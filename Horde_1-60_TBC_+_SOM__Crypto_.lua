GMR.DefineQuester("Horde 1-60 TBC + SOM [Crypto]", function()





local function abandonQuest(questId)
	local questId = questId;
	for i=1,GetNumQuestLogEntries() do
		local _, _, _, _, _, _, _, id = GetQuestLogTitle(i);
		if id == questId then
			SelectQuestLogEntry(i);
			SetAbandonQuest();
			AbandonQuest();
		end
	end
end


local class = UnitClass("player")
local race = UnitRace("player")
local level = UnitLevel("player")







if UnitLevel("player") >= 1 and UnitLevel("player") < 14 then
	if UnitRace("player") == "Tauren" then

----------------------------------------------------------------------------------------------------------------------------------
--  		             START: 1-14 TAUREN Starting Area
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest( "Tauren", nil, 747, "|cFFFFAB48 The Hunt Begins |r", "Custom", -2912.699951, -257.540009, 53.024101, 2980, -2912.699951, -257.540009, 53.024101, 2980, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(747, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 2955, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(747, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 2955, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.SetChecked("Mount", true)',
		'GMR.SetChecked("DisplayCenter", true)',
		'GMR.SetChecked("ClassTrainer", true)',
		'GMR.SetChecked("AutoGear", true)',
		'GMR.DefineProfileCenter(-3025.346, -327.8332, 47.51586,130)',
		'GMR.DefineProfileCenter(-2879.771, -534.1717, 49.06069,130)',
		'GMR.DefineProfileCenter(-3197.676, -507.499, 21.99482,130)',
		'GMR.DefineProfileCenter(-3203.587, -248.2311, 31.82833,130)',
		'GMR.DefineProfileCenter(-3109.681, -710.9704, 33.86679,130)',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)

-- CLASS QUESTS - NOTES - Level 2 Req
-- HUNTER
GMR.DefineQuest("Tauren", "HUNTER", 3092, "|cFFFFAB48 Etched Note |r", "TalkTo", -2912.699951, -257.540009, 53.024101, 2980, -2865.399902, -225.731003, 54.961700, 3061, {},
	{
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	} 
)
-- SHAMAN
GMR.DefineQuest("Tauren", "SHAMAN", 3093, "|cFFFFAB48 Rune-Inscribed Note |r", "TalkTo", -2912.699951, -257.540009, 53.024101, 2980, -2873.879883, -264.709015, 54.007198, 3062, {},
	{
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	} 
)

-- CLASS QUESTS (lvl 2) -- END


GMR.DefineQuest( "Tauren", nil, 752, "|cFFFFAB48 A Humble Task |r", "Custom", -2877.949951, -221.830002, 54.903900, 2981, -3052.540039, -522.497986, 27.014299, 2991, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(752, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-3052.540039, -522.497986, 27.014299, 50)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)

GMR.DefineQuest( "Tauren", nil, 753, "|cFFFFAB48 A Humble Task Part 2 |r", "Custom", -3052.540039, -522.497986, 27.014299, 2991, -2877.949951, -221.830002, 54.903900, 2981, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(753, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-3061.174561, -531.117554, 27.025717, 5) then 
				GMR.MeshTo(-3061.174561, -531.117554, 27.025717)
			else
				local object1 = GMR.GetObjectWithInfo({ id = 2907, rawType = 8 })
				if object1 then
					GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.DefineSettings("Disable", { "Herbs", "Ores", "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(2907)',
		'GMR_DefineCustomObjectID(2907)'
	}
)

GMR.DefineQuest( "Tauren", nil, 750, "|cFFFFAB48 The Hunt Continues |r", "Custom", -2912.699951, -257.540009, 53.024101, 2980, -2912.699951, -257.540009, 53.024101, 2980, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(750, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-3412.962158, -228.277237, 60.784691, 100)',
		'GMR.DefineProfileCenter(-3357.154785, -531.776794, 66.641006, 100)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(2961)'
	}
)

GMR.DefineQuest( "Tauren", nil, 780, "|cFFFFAB48 The Battleboars |r", "Custom", -2912.699951, -257.540009, 53.024101, 2980, -2912.699951, -257.540009, 53.024101, 2980, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(780, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-3254.282715, -906.783142, 60.891441, 100)',
		'GMR.DefineProfileCenter(-3140.008789, -847.213623, 50.176033, 100)',
		'GMR.DefineProfileCenter(-2936.477051, -716.092468, 38.548119, 100)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(2966)'
	}
)

GMR.DefineQuest( "Tauren", nil, 755, "|cFFFFAB48 Rites of the Earthmother |r", "TalkTo", -2877.949951, -221.830002, 54.903900, 2981, -3430.310059, -139.279999, 103.160004, 2982, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-3430.310059, -139.279999, 103.160004, 100)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(2966)'
	}
)

GMR.DefineQuest( "Tauren", nil, 757, "|cFFFFAB48 Rite of Strength |r", "Custom", -3430.310059, -139.279999, 103.160004, 2982, -2877.949951, -221.830002, 54.903900, 2981, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(757, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(-3152.778564, -1055.715088, 57.854565, 50)',
		'GMR.DefineProfileCenter(-3025.980957, -1040.824341, 49.228756, 50)',
		'GMR.DefineProfileCenter(-3072.327393, -1146.556030, 65.395958, 50)',
		'GMR.DefineProfileCenter(-2930.691650, -1101.378418, 56.293694, 50)',
		'GMR.DefineProfileCenter(-2856.688477, -1030.762329, 55.506588, 50)',
		'GMR.DefineProfileCenter(-2937.479492, -991.587402, 55.445560, 50)',
		'GMR.DefineAreaBlacklist(-2935.786133, -1269.726685, 72.204506, 40)',
		'GMR.DefineAreaBlacklist(-2931.841064, -1329.790894, 88.402290, 40)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(2952)',
		'GMR.DefineQuestEnemyId(2953)'
	}
)

GMR.DefineQuest( "Tauren", nil, 781, "|cFFFFAB48 Attack on Camp Narache |r", "Custom", -3105.274, -1201.247, 85.1086, 3076, -2877.949951, -221.830002, 54.903900, 2981, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(781, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		[[if not GMR.Frames.Rand004 then
			GMR.Print("Frame created")
			GMR.Frames.Rand004 = CreateFrame("frame")
			GMR.Frames.Rand004:SetScript("OnUpdate", function(self)
				if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 781 then
					if GMR.IsExecuting() and GetItemCount(4851) >= 1 and not GMR.IsQuestActive(781) then
						local itemName = GetItemInfo(4851)
						GMR.Use(itemName)
						GMR.RunMacroText('/use Dirt-stained Map')
						if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() then
							QuestFrameAcceptButton:Click()
						end
					end
				else 
					self:SetScript("OnUpdate", nil); GMR.Frames.Rand004 = nil; GMR.Print("Frame deleted")
				end
			end)
		end]],
		'GMR.DefineProfileCenter(-3105.274, -1201.247, 85.1086, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(3076)',
		'GMR_DefineCustomObjectID(3076)'
	}
)

GMR.DefineQuest( "Tauren", nil, nil, "|cFFE25FFF TURNIN |r |cFFFFAB48 Rite of Strength |r", "MassTurnIn", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 757, -2877.949951, -221.830002, 54.903900, 2981 }, -- Rite of Strength
	},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.AllowSpeedUp()',
		'GMR.SetChecked("Mount", true)',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		              GRIND: Level 5
----------------------------------------------------------------------------------------------------------------------------------
GMR.DefineQuest("Tauren", nil, nil, "|cFFFFAB48 Grinding: Level 5 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 5 },
	{
		'GMR.DefineProfileCenter(-3160.0334472656, -116.25646972656, 47.096790313721, 80)',
		'GMR.DefineProfileCenter(-3220.4350585938, -228.98822021484, 30.244529724121, 80)',
		'GMR.DefineProfileCenter(-3286.6887207031, -225.42100524902, 40.78702545166, 80)',
		'GMR.DefineProfileCenter(-3313.9775390625, -178.13529968262, 54.330013275146, 80)',
		'GMR.DefineProfileCenter(-3332.0969238281, -136.54806518555, 66.62329864502, 80)',
		'GMR.DefineProfileCenter(-3360.2634277344, -196.50624084473, 63.505905151367, 80)',
		'GMR.DefineProfileCenter(-3324.6398925781, -271.16818237305, 46.196743011475, 80)',
		'GMR.DefineProfileCenter(-3478.6108398438, -225.87693786621, 82.150688171387, 80)',
		'GMR.DefineProfileCenter(-3410.7614746094, -257.9362487793, 70.374313354492, 80)',
		'GMR.DefineProfileCenter(-3407.2856445313, -327.08279418945, 78.759094238281, 80)',
		'GMR.DefineProfileCenter(-3366.0471191406, -344.84872436523, 65.605941772461, 80)',
		'GMR.DefineProfileCenter(-3348.6640625, -424.24569702148, 57.217895507813, 80)',
		'GMR.DefineProfileCenter(-3384.5495605469, -431.23721313477, 74.548294067383, 80)',
		'GMR.DefineProfileCenter(-3428.9907226563, -424.96060180664, 96.145324707031, 80)',
		'GMR.DefineProfileCenter(-3405.5126953125, -485.00830078125, 89.526306152344, 80)',
		'GMR.DefineProfileCenter(-3342.6638183594, -489.51504516602, 57.449436187744, 80)',
		'GMR.DefineProfileCenter(-3337.8347167969, -549.03186035156, 56.677097320557, 80)',
		'GMR.DefineProfileCenter(-3379.4387207031, -575.99194335938, 71.917892456055, 80)',
		'GMR.DefineProfileCenter(-3415.4370117188, -607.52508544922, 89.558723449707, 80)',
		'GMR.DefineProfileCenter(-3353.9846191406, -690.56353759766, 57.757171630859, 80)',
		'GMR.DefineProfileCenter(-3413.8154296875, -710.12225341797, 83.808448791504, 80)',
		'GMR.DefineProfileCenter(-3369.2453613281, -809.03247070313, 55.683689117432, 80)',
		'GMR.DefineProfileCenter(-3428.7956542969, -858.74633789063, 64.580108642578, 80)',
		'GMR.DefineProfileCenter(-3493.0773925781, -848.43090820313, 89.952926635742, 80)',
		'GMR.DefineProfileCenter(-3336.1096191406, -849.46069335938, 53.8434715271, 80)',
		'GMR.DefineProfileCenter(-3273.7004394531, -811.78735351563, 42.237842559814, 80)',
		'GMR.DefineProfileCenter(-3172.3879394531, -805.25225830078, 35.179557800293, 80)',
		'GMR.DefineProfileCenter(-3141.6828613281, -840.01654052734, 49.350639343262, 80)',
		'GMR.DefineProfileCenter(-3216.3999023438, -844.53497314453, 39.737457275391, 80)',
		'GMR.DefineQuestEnemyId(2961)',
		'GMR.DefineQuestEnemyId(2966)',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)
----------------------------------------------------------------------------------------------------------------------------------
--  		             END: 1-5 Tauren Starting Area
----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Shaman ][Level 4][Tauren] - Call of Earth
----------------------------------------------------------------------------------------------------------------------------------
-- SHAMAN Class Quest Here
-- Call of Earth (Tauren)
GMR.DefineQuest("Tauren", "SHAMAN", 1519, "|cFFE25FFF Call of Earth |r", "Custom", -2882.260010, -250.201004, 53.989300, 5888, -2882.260010, -250.201004, 53.989300, 5888,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1519, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineProfileCenter(-2859.325439, -1026.776611, 55.976654, 30)',
		'GMR.DefineQuestEnemyId(2953)' -- Bristleback Shaman
	}
)

-- Call of Earth - Part 2 (Tauren) - 1519 pre-req
GMR.DefineQuest("Tauren", "SHAMAN", 1520, "|cFFE25FFF Call of Earth Part 2 |r", "Custom", -2882.260010, -250.201004, 53.989300, 5888, -3031.360107, -720.833008, 44.890099, 5891,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1520, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-3023.808, -717.1388, 45.71902, 3) then 
				GMR.MeshTo(-3023.808, -717.1388, 45.71902)
			else 
				local itemName = GetItemInfo(6635)
				GMR.Use(itemName)
				GMR.RunMacroText("/use Earth Sapta")
			end
		end
	]]},	
	{
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)

-- Call of Earth - part 3 (Tauren) (1520 Pre-req)
GMR.DefineQuest("Tauren", "SHAMAN", 1521, "|cFFE25FFF Call of Earth Part 3 |r", "Custom", -3031.360107, -720.833008, 44.890099, 5891, -2882.260010, -250.201004, 53.989300, 5888, 
	{},
	{
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	} 
)
----------------------------------------------------------------------------------------------------------------------------------
--  		   END --- [ Shaman ][Level 4][Tauren] - Call of Earth
----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------
--  		              GRIND: Level 7
----------------------------------------------------------------------------------------------------------------------------------
GMR.DefineQuest("Tauren", nil, nil, "|cFFFFAB48 Grinding: Level 7 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 7 },
	{
		'GMR.DefineProfileCenter(-3160.0334472656, -116.25646972656, 47.096790313721, 80)',
		'GMR.DefineProfileCenter(-3220.4350585938, -228.98822021484, 30.244529724121, 80)',
		'GMR.DefineProfileCenter(-3286.6887207031, -225.42100524902, 40.78702545166, 80)',
		'GMR.DefineProfileCenter(-3313.9775390625, -178.13529968262, 54.330013275146, 80)',
		'GMR.DefineProfileCenter(-3332.0969238281, -136.54806518555, 66.62329864502, 80)',
		'GMR.DefineProfileCenter(-3360.2634277344, -196.50624084473, 63.505905151367, 80)',
		'GMR.DefineProfileCenter(-3324.6398925781, -271.16818237305, 46.196743011475, 80)',
		'GMR.DefineProfileCenter(-3478.6108398438, -225.87693786621, 82.150688171387, 80)',
		'GMR.DefineProfileCenter(-3410.7614746094, -257.9362487793, 70.374313354492, 80)',
		'GMR.DefineProfileCenter(-3407.2856445313, -327.08279418945, 78.759094238281, 80)',
		'GMR.DefineProfileCenter(-3366.0471191406, -344.84872436523, 65.605941772461, 80)',
		'GMR.DefineProfileCenter(-3348.6640625, -424.24569702148, 57.217895507813, 80)',
		'GMR.DefineProfileCenter(-3384.5495605469, -431.23721313477, 74.548294067383, 80)',
		'GMR.DefineProfileCenter(-3428.9907226563, -424.96060180664, 96.145324707031, 80)',
		'GMR.DefineProfileCenter(-3405.5126953125, -485.00830078125, 89.526306152344, 80)',
		'GMR.DefineProfileCenter(-3342.6638183594, -489.51504516602, 57.449436187744, 80)',
		'GMR.DefineProfileCenter(-3337.8347167969, -549.03186035156, 56.677097320557, 80)',
		'GMR.DefineProfileCenter(-3379.4387207031, -575.99194335938, 71.917892456055, 80)',
		'GMR.DefineProfileCenter(-3415.4370117188, -607.52508544922, 89.558723449707, 80)',
		'GMR.DefineProfileCenter(-3353.9846191406, -690.56353759766, 57.757171630859, 80)',
		'GMR.DefineProfileCenter(-3413.8154296875, -710.12225341797, 83.808448791504, 80)',
		'GMR.DefineProfileCenter(-3369.2453613281, -809.03247070313, 55.683689117432, 80)',
		'GMR.DefineProfileCenter(-3428.7956542969, -858.74633789063, 64.580108642578, 80)',
		'GMR.DefineProfileCenter(-3493.0773925781, -848.43090820313, 89.952926635742, 80)',
		'GMR.DefineProfileCenter(-3336.1096191406, -849.46069335938, 53.8434715271, 80)',
		'GMR.DefineProfileCenter(-3273.7004394531, -811.78735351563, 42.237842559814, 80)',
		'GMR.DefineProfileCenter(-3172.3879394531, -805.25225830078, 35.179557800293, 80)',
		'GMR.DefineProfileCenter(-3141.6828613281, -840.01654052734, 49.350639343262, 80)',
		'GMR.DefineProfileCenter(-3216.3999023438, -844.53497314453, 39.737457275391, 80)',
		'GMR.DefineQuestEnemyId(2961)',
		'GMR.DefineQuestEnemyId(2966)',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)
----------------------------------------------------------------------------------------------------------------------------------
--  		             END: 1-7 Tauren Starting Area
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--  		             START: 7-14 Tauren - MULGORE
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest( "Tauren", nil, 3376, "|cFFFFAB48 Break Sharptusk |r", "Custom", -2897.610107, -241.814972, 53.928967, 3209, -2897.610107, -241.814972, 53.928967, 3209, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(3376, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2932.870117, -1276.250000, 72.287697, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineQuestEnemyId(8554)'
	}
)

GMR.DefineQuest( "Tauren", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Rites of the Earthmother + Task Unfinished |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 763, -2877.949951, -221.830002, 54.903900, 2981 }, -- Rites of the Earthmother 757 pre-req
		{ 1656, -3066.020020, 68.800201, 79.468803, 6775 }   -- A Task Unfinished
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Tauren Starting Area
		'GMR.DefineSellVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineRepairVendor(-2926.320068, -215.714996, 54.259800, 3073)',
		'GMR.DefineGoodsVendor(-2893.719971, -279.332001, 53.999699, 3072)',
		'GMR.DefineAmmoVendor(-2893.719971, -279.332001, 53.999699, 3072)'
	}
)

GMR.DefineQuest( "Tauren", nil, 763, "|cFFFFAB48 Rites of the Earthmother |r", "TalkTo", -2877.949951, -221.830002, 54.903900, 2981, -2333.540039, -393.072998, -7.929320, 2993, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.DefineProfileCenter(-2333.540039, -393.072998, -7.929320,25)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)

GMR.DefineQuest( "Tauren", nil, 1656, "|cFFFFAB48 A Task Unfinished |r", "TalkTo", -3066.020020, 68.800201, 79.468803, 6775, -2365.370117, -347.309998, -8.873650, 6747, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.DefineProfileCenter(-2365.370117, -347.309998, -8.873650,25)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)

GMR.DefineQuest( "Tauren", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Bloodhoof Village (lefvel 7) Quests |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 767, -2333.540039, -393.072998, -7.929320, 2993 }, -- Rite of Vision 1
		{ 745, -2333.540039, -393.072998, -7.929320, 2993 }, -- Sharing the Land
		{ 748, -2341.399902, -445.449005, -6.417300, 2948 }, -- Poison Water
		{ 761, -2304.790039, -454.808990, -5.355070, 2947 }, -- Swoop Hunting
		{ 743, -2397.070068, -384.830994, -2.194960, 2985 }, -- Dangers of the Windfury
		{ 766, -2227.540039, -365.720001, -9.279410, 3055 }, -- Mazzranache
		{ 749, -2365.459961, -849.280579, -8.620988, 2988 } -- The Ravaged Caravan
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)

GMR.DefineQuest( "Tauren", nil, 767, "|cFFFFAB48 Rite of Vision |r", "TalkTo", -2333.540039, -393.072998, -7.929320, 2993, -2243.479980, -405.842987, -9.341570, 3054, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.DefineProfileCenter(-2243.479980, -405.842987, -9.341570, 25)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)

GMR.DefineQuest( "Tauren", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48  Rite of Vision 2 |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 771, -2243.479980, -405.842987, -9.341570, 3054 } -- Rite of Vision 2
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)

GMR.DefineQuest( "Tauren", nil, 748, "|cFFFFAB48 Poison Water |r", "Custom", -2341.399902, -445.449005, -6.417300, 2948, -2341.399902, -445.449005, -6.417300, 2948, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(748, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 2958, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(748, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 2956, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2603.912598, -54.814518, -2.736796, 80)',
		'GMR.DefineProfileCenter(-2715.682373, 179.095642, 70.046417, 80)',
		'GMR.DefineProfileCenter(-2401.837402, 33.563213, 20.457470, 80)',
		'GMR.DefineProfileCenter(-2144.559082, -2.627638, 20.631237, 80)',
		'GMR.DefineProfileCenter(-2573.074, -520.479, -4.041328, 80)',
		'GMR.DefineProfileCenter(-2629.566, -335.2882, -7.257057, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2958)',
		'GMR.DefineQuestEnemyId(2956)'
	}
)

GMR.DefineQuest( "Tauren", nil, 761, "|cFFFFAB48 Swoop Hunting |r", "Custom", -2304.790039, -454.808990, -5.355070, 2947, -2304.790039, -454.808990, -5.355070, 2947, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(761, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2603.912598, -54.814518, -2.736796, 80)',
		'GMR.DefineProfileCenter(-2715.682373, 179.095642, 70.046417, 80)',
		'GMR.DefineProfileCenter(-2401.837402, 33.563213, 20.457470, 80)',
		'GMR.DefineProfileCenter(-2144.559082, -2.627638, 20.631237, 80)',
		'GMR.DefineProfileCenter(-2673.964844, -247.916794, 1.823853, 80)',
		'GMR.DefineProfileCenter(-2265.809570, 93.624596, 43.731472, 80)',
		'GMR.DefineProfileCenter(-1995.450806, -286.142090, -9.242443, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2970)',
		'GMR.DefineQuestEnemyId(2969)'
	}
)

GMR.DefineQuest( "Tauren", nil, 766, "|cFFFFAB48 Mazzranache |r", "Custom", -2227.540039, -365.720001, -9.279410, 3055, -2227.540039, -365.720001, -9.279410, 3055, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(766, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 2958, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(766, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 3035, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(766, 3) then
			local enemy = GMR.GetObjectWithInfo({ id = 2956, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(766, 4) then
			local enemy = GMR.GetObjectWithInfo({ id = 2969, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2110.617, 18.02, 23.26607, 120)',
		'GMR.DefineProfileCenter(-2127.733887, -3.653963, 20.346233, 120)',
		'GMR.DefineProfileCenter(-2404.879150, 83.083580, 38.793648, 120)',
		'GMR.DefineProfileCenter(-1877.207, 40.08838, 3.749318, 120)',
		'GMR.DefineProfileCenter(-1993.92, -78.51602, -5.298624, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(3035)',
		'GMR.DefineQuestEnemyId(2958)',
		'GMR.DefineQuestEnemyId(2956)',
		'GMR.DefineQuestEnemyId(2969)',
		'GMR.DefineQuestEnemyId(2970)'
	}
)

GMR.DefineQuest( "Tauren", nil, nil, "|cFFFFAB48 Grinding: Mulgore - Level 10 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 10 },
	{},
	{
		'GMR.DefineProfileCenter(-2154.5080566406, -93.69694519043, -8.3515625, 90)',
		'GMR.DefineProfileCenter(-2246.4968261719, -74.943176269531, -4.2369060516357, 90)',
		'GMR.DefineProfileCenter(-2371.953125, -88.782974243164, -5.6990833282471, 90)',
		'GMR.DefineProfileCenter(-2501.5205078125, -60.213027954102, -3.7934322357178, 90)',
		'GMR.DefineProfileCenter(-2529.2592773438, -137.73574829102, -3.4611568450928, 90)',
		'GMR.DefineProfileCenter(-2655.0153808594, -235.68051147461, -2.7849559783936, 90)',
		'GMR.DefineProfileCenter(-2732.34765625, -154.49099731445, 3.6726551055908, 90)',
		'GMR.DefineProfileCenter(-2763.259765625, -108.15797424316, -1.5941381454468, 90)',
		'GMR.DefineProfileCenter(-2656.4855957031, -17.144596099854, 2.1321029663086, 90)',
		'GMR.DefineProfileCenter(-2645.0942382813, 57.05485534668, 25.46325302124, 90)',
		'GMR.DefineProfileCenter(-2652.0480957031, 103.57211303711, 41.234100341797, 90)',
		'GMR.DefineProfileCenter(-2707.8659667969, 150.27407836914, 58.443305969238, 90)',
		'GMR.DefineProfileCenter(-2588.4020996094, 101.17444610596, 45.415496826172, 90)',
		'GMR.DefineProfileCenter(-2556.3630371094, 102.59976959229, 43.285583496094, 90)',
		'GMR.DefineProfileCenter(-2522.4536132813, 53.266567230225, 20.866409301758, 90)',
		'GMR.DefineProfileCenter(-2483.5363769531, 28.144666671753, 13.735383987427, 90)',
		'GMR.DefineProfileCenter(-2412.6257324219, 51.215152740479, 27.166084289551, 90)',
		'GMR.DefineProfileCenter(-2351.7756347656, 97.260643005371, 45.215679168701, 90)',
		'GMR.DefineProfileCenter(-2183.1020507813, 63.422061920166, 35.229595184326, 90)',
		'GMR.DefineProfileCenter(-2187.884765625, 14.027297973633, 23.171659469604, 90)',
		'GMR.DefineProfileCenter(-2068.9660644531, 81.171005249023, 34.302215576172, 90)',
		'GMR.DefineProfileCenter(-1996.2844238281, -46.277267456055, -4.4883842468262, 90)',
		'GMR.DefineProfileCenter(-2024.1834716797, -99.262512207031, -8.6938896179199, 90)',
		'GMR.DefineProfileCenter(-2177.5446777344, -48.49437713623, 0.025783538818359, 90)',
		'GMR.DefineProfileCenter(-2253.3322753906, 14.200880050659, 19.297620773315, 90)',
		'GMR.DefineProfileCenter(-2257.8215332031, -20.63236618042, 9.728832244873, 90)',
		'GMR.DefineProfileCenter(-2282.75390625, -42.59797668457, 1.5401916503906, 90)',
		'GMR.DefineAreaBlacklist(-2545.788, -704.9552, -8.931123, 80)',
		'GMR.DefineQuestEnemyId(3035)',
		'GMR.DefineQuestEnemyId(2958)',
		'GMR.DefineQuestEnemyId(2956)',
		'GMR.DefineQuestEnemyId(2969)',
		'GMR.DefineQuestEnemyId(2970)',
		'GMR.DefineQuestEnemyId(2949)',
		'GMR.DefineQuestEnemyId(2950)',
		'GMR.DefineQuestEnemyId(2951)',
		'GMR.BlacklistId(2974)',
		'GMR.BlacklistId(2972)',
		'GMR.BlacklistId(2973)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)


----------------------------------------------------------------------------------------------------------------------------------
--  		   START (Lvl 10) --- CLASS QUESTS - Tauren
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Hunter ][Level 10][Tauren] - Tame Pet / Summon Pet
----------------------------------------------------------------------------------------------------------------------------------

-- Taming the Beast Part 1 - (6061)
GMR.DefineQuest("Tauren", "HUNTER", 6061, "Taming the Beast", "UseItemOnObjects", -2180.219971, -408.816010, -4.518260, 3065, -2180.219971, -408.816010, -4.518260, 3065, 
	{
		--  itemId, objectId, rawType, distanceToObject, movementFlag
		15914, 2956, 5, 20, 1
	},
	{
		-- Bloodhoof Village - Mulgore (Tauren Area)
		'GMR.DefineSellVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineRepairVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineGoodsVendor(-2365.36, -347.309, -8.95691, 6747)',
		'GMR.DefineAmmoVendor(-2247.81, -308.156, -9.42475, 3076)',
		'GMR.DefineProfileMailbox(-2336.62, -366.484, -8.47472)',

		'PetDismiss()',
		'GMR.DefineProfileCenter(-2139.579590, 16.597075, 24.537880, 100)'
	}
)

-- Taming the Beast Part 2 - (6087) --- Prerequisite (6061)
GMR.DefineQuest("Tauren", "HUNTER", 6087, "Taming the Beast", "UseItemOnObjects", -2180.219971, -408.816010, -4.518260, 3065, -2180.219971, -408.816010, -4.518260, 3065, 
	{
		15915, 2959, 5, 20, 1
	},
	{
		-- Bloodhoof Village - Mulgore (Tauren Area)
		'GMR.DefineSellVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineRepairVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineGoodsVendor(-2365.36, -347.309, -8.95691, 6747)',
		'GMR.DefineAmmoVendor(-2247.81, -308.156, -9.42475, 3076)',
		'GMR.DefineProfileMailbox(-2336.62, -366.484, -8.47472)',
		
		'PetDismiss()',
		'GMR.DefineProfileCenter(-1869.294556, -122.694901, -7.087188, 100)'
	}
)

-- Taming the Beast Part 3 (6087 pre-req)
GMR.DefineQuest("Tauren", "HUNTER", 6088, "Taming the Beast", "UseItemOnObjects", -2180.219971, -408.816010, -4.518260, 3065, -2180.219971, -408.816010, -4.518260, 3065, 
	{
		15920, 2970, 5, 20, nil
	},
	{
		-- Bloodhoof Village - Mulgore (Tauren Area)
		'GMR.DefineSellVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineRepairVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineGoodsVendor(-2365.36, -347.309, -8.95691, 6747)',
		'GMR.DefineAmmoVendor(-2247.81, -308.156, -9.42475, 3076)',
		'GMR.DefineProfileMailbox(-2336.62, -366.484, -8.47472)',

		'PetDismiss()',
		'GMR.DefineProfileCenter(-1959.409546, -320.665222, -1.239008, 100)',
		'GMR.DefineProfileCenter(-1787.130737, -455.367157, -1.584147, 100)',
		'GMR.DefineProfileCenter(-1787.361816, -575.016785, -3.748166, 100)'
	}
)

-- IN THUNDERBLUFF.. Is elevator working????
-- Taming the Beast Part 4
GMR.DefineQuest("Tauren", "HUNTER", 6089, "Training the Beast", "TalkTo", -2180.219971, -408.816010, -4.518260, 3065, -1474.760010, -81.372299, 161.184998, 3039, {},
	{
		-- Bloodhoof Village - Mulgore (Tauren Area)
		'GMR.DefineSellVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineRepairVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineGoodsVendor(-2365.36, -347.309, -8.95691, 6747)',
		'GMR.DefineAmmoVendor(-2247.81, -308.156, -9.42475, 3076)',
		'GMR.DefineProfileMailbox(-2336.62, -366.484, -8.47472)',
		'PetDismiss()'
	}
)

-- Quest Chain Finished - Go TAME an actual pet now.
-- TAME - Prarie Wolf Alpha
GMR.DefineQuest("Tauren", "HUNTER", nil, "Crypto_Tame Praire Wolf Alpha", "TameBeast", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		2960, 5, 28, 1
	},
	{
		'PetDismiss()',
		'GMR_AddPetFoodItem(2672)', -- Stringy Wolf Meat (only shit wolves eat apparently in Mulgore... (all bread vendors))
		'GMR_AddPetFoodItem(7097)', -- Leg Meat
		'GMR_AddPetFoodItem(5467)', -- Kodo Meat
		'GMR.DefineProfileCenter(-1342.940430, -477.003540, -58.498081, 90)',
	
		-- Bloodhoof Village - Mulgore (Tauren Area)
		'GMR.DefineSellVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineRepairVendor(-2283.34, -305.996, -9.42477, 3079)',
		'GMR.DefineGoodsVendor(-2365.36, -347.309, -8.95691, 6747)',
		'GMR.DefineAmmoVendor(-2247.81, -308.156, -9.42475, 3076)',
		'GMR.DefineProfileMailbox(-2336.62, -366.484, -8.47472)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  			 END --- [ Hunter ][Level 10][Orc + Troll] - Tame Pet
----------------------------------------------------------------------------------------------------------------------------------


GMR.DefineQuest( "Tauren", nil, 754, "|cFFFFAB48 Winterhoof Cleansing |r", "Custom", -2341.399902, -445.449005, -6.417300, 2948, -2341.399902, -445.449005, -6.417300, 2948, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(754, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-2538.308, -705.1069, -8.482833, 3) then 
				GMR.MeshTo(-2538.308, -705.1069, -8.482833)
			else
				local itemName = GetItemInfo(5411)
				GMR.Use(itemName)
				GMR.RunMacroText("/use Winterhoof Cleansing Totem")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2538.308, -705.1069, -8.482833, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest( "Scourge", nil, 756, "|cFFFFAB48 Thunderhorn Totem |r", "Custom", -2341.399902, -445.449005, -6.417300, 2948, -2341.399902, -445.449005, -6.417300, 2948, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(756, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 2959, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(756, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 3035, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(-1764.528198, -559.257263, -5.368731, 80)',
		'GMR.DefineProfileCenter(-1568.976318, -772.492615, -18.610395, 80)',
		'GMR.DefineProfileCenter(-1490.808228, -423.939606, -44.892570, 80)',
		'GMR.DefineProfileCenter(-1525.716, -313.4337, -35.88746, 80)',
		'GMR.DefineProfileCenter(-1606.95, -485.4524, -32.54058, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2959)',
		'GMR.DefineQuestEnemyId(3035)'
	}
)

GMR.DefineQuest( "Tauren", nil, 758, "|cFFFFAB48 Thunderhorn Cleansing |r", "Custom", -2341.399902, -445.449005, -6.417300, 2948, -2341.399902, -445.449005, -6.417300, 2948, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(758, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-1830.106079, -239.194534, -9.424853, 3) then 
				GMR.MeshTo(-1830.106079, -239.194534, -9.424853)
			else
				local itemName = GetItemInfo(5415)
				GMR.Use(itemName)
				GMR.RunMacroText("/use Thunderhorn Cleansing Totem")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-1830.106079, -239.194534, -9.424853, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest( "Tauren", nil, 759, "|cFFFFAB48 Wildmane Totem |r", "Custom", -2341.399902, -445.449005, -6.417300, 2948, -2341.399902, -445.449005, -6.417300, 2948, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(759, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-1271.809, -295.7343, -24.51426, 120)',
		'GMR.DefineProfileCenter(-2227.033447, -1146.543091, 3.790687, 80)',
		'GMR.DefineProfileCenter(-2392.629639, -1164.127808, -5.033299, 80)',
		'GMR.DefineProfileCenter(-2552.687012, -1113.044434, -8.189576, 80)',
		'GMR.DefineProfileCenter(-2488.030762, -1293.183472, -5.677210, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2960)'
	}
)

GMR.DefineQuest( "Tauren", nil, 760, "|cFFFFAB48 Wildmane Cleansing (Good Axe Reward) |r", "Custom", -2341.399902, -445.449005, -6.417300, 2948, -2341.399902, -445.449005, -6.417300, 2948, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(760, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-759.175232, -151.060532, -28.475296, 3) then 
				GMR.MeshTo(-759.175232, -151.060532, -28.475296)
			else
				local itemName = GetItemInfo(5416)
				GMR.Use(itemName)
				GMR.RunMacroText("/use Wildmane Cleansing Totem")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-759.175232, -151.060532, -28.475296, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest( "Tauren", nil, 771, "|cFFFFAB48 Rite of Vision |r", "Custom", -2243.479980, -405.842987, -9.341570, 3054, -2243.479980, -405.842987, -9.341570, 3054, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(771, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-2542.016, -686.205, -7.264302, 55) then 
				GMR.MeshTo(-2542.016, -686.205, -7.264302)
			else
				local object = GMR.GetObjectWithInfo({ id = 2910, rawType = 8 })
				if object then 
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
				end
			end
		elseif not GMR.Questing.IsObjectiveCompleted(771, 2) then
			GMR.SetQuestingState(nil);
			local object = GMR.GetObjectWithInfo({ id = 2912, rawType = 8 })
			if object then 
				GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2551.381, -538.968, -4.537463, 120)',
		'GMR.DefineProfileCenter(-2608.394, -404.8862, -7.201443, 120)',
		'GMR.DefineProfileCenter(-2479.6, -456.7796, -6.949165, 120)',
		'GMR.DefineProfileCenter(-2369.05, -625.8557, -6.083608, 120)',
		'GMR.DefineProfileCenter(-2258.88, -701.4191, -4.189744, 120)',
		'GMR.DefineProfileCenter(-2076.522, -836.3208, -6.294233, 120)',
		'GMR.DefineProfileCenter(-2133.915, -987.2304, 19.60836, 120)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2912)',
		'GMR_DefineCustomObjectID(2912)'
	}
)

GMR.DefineQuest( "Tauren", nil, 772, "|cFFFFAB48 Rite of Vision part 2 |r", "Custom", -2243.479980, -405.842987, -9.341570, 3054, -1508.910034, 367.214996, 62.824402, 2984, 
	{[[
		local npc1 = GMR.GetObjectWithInfo({ id = 2983, rawType = 5 })
		local npcToFollow = GMR.FindObject(2983, 5, nil, nil)
		local itemName = GetItemInfo(4823)
		if not GMR.Questing.IsObjectiveCompleted(772, 1) then
			GMR.SetQuestingState(nil);
			if GetItemCount(4823) >= 1 then
				if not GMR.IsPlayerPosition(-2241.277, -404.4802, -9.423854, 2) then 
					GMR.MeshTo(-2241.277, -404.4802, -9.423854)
				else 
					GMR.Use(itemName)
					GMR.RunMacroText("/use Water of the Seers")
				end
			elseif npcToFollow and GetItemCount(4823) < 1 then 
				local x, y, z = GMR.ObjectPosition(npcToFollow)
				if not GMR.IsPlayerPosition(x, y, z) then
					GMR.MeshTo(x, y, z)
				end
			end
		elseif GetItemCount(4823) < 1 and not npcToFollow then
			if not GMR.IsPlayerPosition(-1508.910034, 367.214996, 62.824402, 2) then 
				GMR.MeshTo(-1508.910034, 367.214996, 62.824402)
			else 
				local npc = GMR.GetObjectWithInfo({ id = 2984, rawType = 5, isAlive = true })
				if npc then
					QuestFrameCompleteQuestButton:Click()
					GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-1508.910034, 367.214996, 62.824402, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest( "Tauren", nil, 743, "|cFFFFAB48 Dangers of the Windfury |r", "Custom", -2397.070068, -384.830994, -2.194960, 2985, -2397.070068, -384.830994, -2.194960, 2985, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(743, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-1717.558105, 317.381805, 67.806213, 80)',
		'GMR.DefineProfileCenter(-1682.119507, 423.180573, 97.161179, 80)',
		'GMR.DefineProfileCenter(-1729.475098, 368.225769, 81.605133, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2963)',
		'GMR.DefineQuestEnemyId(2962)',
		'GMR_DefineCustomObjectID(106318)',
	}
)

GMR.DefineQuest("Tauren", nil, 749, "The Ravaged Caravan Part 1", "TalkTo", -2365.459961, -849.280579, -8.620988, 2988, -1923.268555, -713.290222, 4.312356, 2908, {},
	{
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
	}
)

-- The Ravaged Caravan p2
GMR.DefineQuest("Tauren", nil, 751, "The Ravaged Caravan Part 2", "TalkTo", -1923.268555, -713.290222, 4.312356, 2908, -2365.459961, -849.280579, -8.620988, 2988, {},
	{
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
	}
)

GMR.DefineQuest("Tauren", nil, 745, "Sharing the Land", "Custom", -2333.540039, -393.072998, -7.929320, 2993, -2333.540039, -393.072998, -7.929320, 2993,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(745, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-2741.687500, -767.438293, -3.504184, 60)',
		'GMR.DefineProfileCenter(-2767.600342, -687.559875, 6.195585, 60)',
		'GMR.DefineProfileCenter(-2717.230225, -495.081024, 2.884865, 60)',
		'GMR.DefineProfileCenter(-2736.023438, -416.009796, -4.763036, 60)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)',
		'GMR.DefineQuestEnemyId(2950)', -- Skinner
		'GMR.DefineQuestEnemyId(2949)', -- Tanner
		'GMR.DefineQuestEnemyId(2951)', -- Poacher
		'GMR_DefineCustomObjectID(106318)', -- Battered Chest
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		              GRIND: Level 14
----------------------------------------------------------------------------------------------------------------------------------
GMR.DefineQuest( "Tauren", nil, nil, "|cFFFFAB48 Grinding: Mulgore - Level 10 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 10 },
	{},
	{
		'GMR.DefineProfileCenter(-2154.5080566406, -93.69694519043, -8.3515625, 90)',
		'GMR.DefineProfileCenter(-1258.3161621094, -460.04193115234, -53.373764038086, 80)',
		'GMR.DefineProfileCenter(-1198.4289550781, -470.87670898438, -45.632457733154, 80)',
		'GMR.DefineProfileCenter(-1137.666015625, -463.33666992188, -41.69229888916, 80)',
		'GMR.DefineProfileCenter(-1155.9890136719, -528.36578369141, -59.141212463379, 80)',
		'GMR.DefineProfileCenter(-1132.0086669922, -589.27136230469, -55.081825256348, 80)',
		'GMR.DefineProfileCenter(-1040.3309326172, -594.86108398438, -59.648948669434, 80)',
		'GMR.DefineProfileCenter(-1073.9820556641, -694.92449951172, -52.662456512451, 80)',
		'GMR.DefineProfileCenter(-995.93664550781, -720.52673339844, -36.744010925293, 80)',
		'GMR.DefineProfileCenter(-950.27478027344, -591.654296875, -57.71524810791, 80)',
		'GMR.DefineProfileCenter(-875.74908447266, -679.97534179688, -35.814086914063, 80)',
		'GMR.DefineProfileCenter(-838.22155761719, -631.40710449219, -36.141788482666, 80)',
		'GMR.DefineProfileCenter(-755.47265625, -613.13336181641, -26.158538818359, 80)',
		'GMR.DefineProfileCenter(-763.48333740234, -551.01428222656, -27.075479507446, 80)',
		'GMR.DefineProfileCenter(-654.67620849609, -563.48645019531, -9.0659370422363, 80)',
		'GMR.DefineProfileCenter(-692.35443115234, -485.01226806641, -14.173503875732, 80)',
		'GMR.DefineProfileCenter(-777.62060546875, -458.10391235352, -31.051780700684, 80)',
		'GMR.DefineProfileCenter(-765.16748046875, -380.86395263672, -24.385272979736, 80)',
		'GMR.DefineProfileCenter(-765.984375, -248.37226867676, -25.489570617676, 80)',
		'GMR.DefineProfileCenter(-900.18963623047, -194.31297302246, -4.6868629455566, 80)',
		'GMR.DefineProfileCenter(-855.94982910156, -306.150390625, -21.57723236084, 80)',
		'GMR.DefineProfileCenter(-938.14733886719, -415.24243164063, -37.379383087158, 80)',
		'GMR.DefineProfileCenter(-974.52416992188, -521.38317871094, -55.972595214844, 80)',
		'GMR.DefineProfileCenter(-1056.2484130859, -495.3671875, -48.714851379395, 80)',
		'GMR.DefineProfileCenter(-1179.0334472656, -803.53155517578, -33.720489501953, 80)',
		'GMR.DefineProfileCenter(-1332.5335693359, -771.38146972656, -28.783575057983, 80)',
		'GMR.DefineProfileCenter(-1222.0510253906, -588.49078369141, -55.637725830078, 80)',
		'GMR.DefineAreaBlacklist(-2545.788, -704.9552, -8.931123, 80)',
		'GMR.DefineQuestEnemyId(2971)',
		'GMR.DefineQuestEnemyId(3566)',
		'GMR.DefineQuestEnemyId(2957)',
		'GMR.DefineQuestEnemyId(2960)',
		'GMR.DefineQuestEnemyId(2964)',
		'GMR.DefineQuestEnemyId(2965)',
		'GMR_DefineCustomObjectID(106318)', -- Battered Chest
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)
----------------------------------------------------------------------------------------------------------------------------------
--  		             END: 14 Mulgore
----------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Warrior ][Level 10][Tauren] - Defensive Stance
----------------------------------------------------------------------------------------------------------------------------------
-- Defensive Stance - Tauren
-- Vetran Uzzek
GMR.DefineQuest("Tauren", "WARRIOR", 1505, "|cFFFFAB48 Defensive Stance - Vetran Uzzek |r", "TalkTo", -2347.979980, -495.920013, -8.964040, 3063, 185.746002, -3597.209961, 27.338301, 5810, {},
	{
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(-2346.341, -496.4685, -9.080105, 75)',
		'GMR.DefineUnstuck(-2329.477, -506.0047, -9.378332)',
		'GMR.DefineUnstuck(-2321.061, -511.8884, -9.350673)',
		'GMR.DefineUnstuck(-2312.334, -517.9887, -7.813046)',
		'GMR.DefineUnstuck(-2304.75, -525.2286, -6.144685)',
		'GMR.DefineUnstuck(-2301.219, -534.8922, -5.480736)',
		'GMR.DefineUnstuck(-2303.118, -544.9559, -5.01316)',
		'GMR.DefineUnstuck(-2302.403, -555.6172, -5.370659)',
		'GMR.DefineUnstuck(-2303.063, -566.2399, -6.799663)',
		'GMR.DefineUnstuck(-2303.971, -576.5319, -8.579891)',
		'GMR.DefineUnstuck(-2304.875, -586.7611, -9.424641)',
		'GMR.DefineUnstuck(-2305.811, -597.3669, -9.424641)',
		'GMR.DefineUnstuck(-2307.023, -608.0391, -9.424641)',
		'GMR.DefineUnstuck(-2309.785, -618.2563, -9.397708)',
		'GMR.DefineUnstuck(-2313.015, -628.1249, -9.265835)',
		'GMR.DefineUnstuck(-2316.382, -638.1559, -9.18227)',
		'GMR.DefineUnstuck(-2319.67, -647.9507, -9.159492)',
		'GMR.DefineUnstuck(-2323.045, -658.0043, -9.048733)',
		'GMR.DefineUnstuck(-2326.32, -667.7593, -9.050259)',
		'GMR.DefineUnstuck(-2329.715, -677.8727, -9.191029)',
		'GMR.DefineUnstuck(-2332.996, -687.6476, -9.346171)',
		'GMR.DefineUnstuck(-2336.398, -697.7809, -9.398595)',
		'GMR.DefineUnstuck(-2339.092, -707.6566, -9.415686)',
		'GMR.DefineUnstuck(-2340.506, -718.2093, -9.423543)',
		'GMR.DefineUnstuck(-2342.038, -728.4048, -9.423543)',
		'GMR.DefineUnstuck(-2343.825, -738.922, -9.423543)',
		'GMR.DefineUnstuck(-2345.609, -749.4185, -9.423543)',
		'GMR.DefineUnstuck(-2347.333, -759.5631, -9.423543)',
		'GMR.DefineUnstuck(-2350.847, -780.2456, -9.424954)',
		'GMR.DefineUnstuck(-2354.414, -801.2386, -9.424954)',
		'GMR.DefineUnstuck(-2357.869, -821.5692, -9.401217)',
		'GMR.DefineUnstuck(-2361.44, -842.583, -9.127586)',
		'GMR.DefineUnstuck(-2364.951, -863.2448, -9.348041)',
		'GMR.DefineUnstuck(-2368.459, -883.8895, -9.420102)',
		'GMR.DefineUnstuck(-2371.97, -904.5513, -9.424314)',
		'GMR.DefineUnstuck(-2377.277, -924.7363, -9.411241)',
		'GMR.DefineUnstuck(-2382.955, -944.864, -9.367147)',
		'GMR.DefineUnstuck(-2390.506, -964.4144, -9.321777)',
		'GMR.DefineUnstuck(-2398.057, -983.9647, -9.401486)',
		'GMR.DefineUnstuck(-2405.616, -1003.535, -9.410689)',
		'GMR.DefineUnstuck(-2413.296, -1023.418, -9.424421)',
		'GMR.DefineUnstuck(-2420.036, -1041.008, -9.424421)',
		'GMR.DefineUnstuck(-2427.482, -1060.576, -9.423571)',
		'GMR.DefineUnstuck(-2434.118, -1080.378, -9.423571)',
		'GMR.DefineUnstuck(-2436.743, -1101.171, -9.423571)',
		'GMR.DefineUnstuck(-2439.369, -1121.964, -9.325305)',
		'GMR.DefineUnstuck(-2441.997, -1142.778, -9.33598)',
		'GMR.DefineUnstuck(-2443.542, -1163.602, -9.412169)',
		'GMR.DefineUnstuck(-2442.816, -1184.557, -9.361873)',
		'GMR.DefineUnstuck(-2439.86, -1205.558, -9.339412)',
		'GMR.DefineUnstuck(-2432.627, -1225.209, -9.400682)',
		'GMR.DefineUnstuck(-2425.392, -1244.865, -9.423211)',
		'GMR.DefineUnstuck(-2418.152, -1264.533, -9.335959)',
		'GMR.DefineUnstuck(-2411.523, -1284.416, -8.710999)',
		'GMR.DefineUnstuck(-2407.564, -1304.982, -8.165511)',
		'GMR.DefineUnstuck(-2403.666, -1325.574, -5.457558)',
		'GMR.DefineUnstuck(-2399.767, -1346.166, -0.822892)',
		'GMR.DefineUnstuck(-2393.322, -1365.984, 5.502238)',
		'GMR.DefineUnstuck(-2384.965, -1385.592, 11.87622)',
		'GMR.DefineUnstuck(-2376.756, -1404.853, 18.53805)',
		'GMR.DefineUnstuck(-2368.531, -1424.152, 24.38052)',
		'GMR.DefineUnstuck(-2360.349, -1443.469, 29.7144)',
		'GMR.DefineUnstuck(-2354.813, -1463.742, 34.98822)',
		'GMR.DefineUnstuck(-2349.695, -1484.012, 39.57593)',
		'GMR.DefineUnstuck(-2344.565, -1504.33, 44.29585)',
		'GMR.DefineUnstuck(-2344.14, -1525.013, 49.87408)',
		'GMR.DefineUnstuck(-2344.74, -1545.962, 55.8405)',
		'GMR.DefineUnstuck(-2345.341, -1566.912, 61.78883)',
		'GMR.DefineUnstuck(-2345.951, -1588.2, 67.24146)',
		'GMR.DefineUnstuck(-2346.552, -1609.17, 72.44813)',
		'GMR.DefineUnstuck(-2347.152, -1630.12, 77.37295)',
		'GMR.DefineUnstuck(-2347.752, -1651.051, 81.25106)',
		'GMR.DefineUnstuck(-2348.435, -1671.959, 84.03905)',
		'GMR.DefineUnstuck(-2350.26, -1692.837, 85.91085)',
		'GMR.DefineUnstuck(-2351.254, -1713.773, 88.20049)',
		'GMR.DefineUnstuck(-2351.749, -1734.726, 89.98263)',
		'GMR.DefineUnstuck(-2352.244, -1755.678, 90.85183)',
		'GMR.DefineUnstuck(-2352.748, -1777.001, 91.33523)',
		'GMR.DefineUnstuck(-2353.243, -1797.946, 93.60504)',
		'GMR.DefineUnstuck(-2353.827, -1822.642, 96.01868)',
		'GMR.DefineUnstuck(-2353.426, -1839.81, 96.06181)',
		'GMR.DefineUnstuck(-2352.424, -1857.527, 95.95638)',
		'GMR.DefineUnstuck(-2351.45, -1874.754, 95.81538)',
		'GMR.DefineUnstuck(-2352.724, -1891.868, 95.78357)',
		'GMR.DefineUnstuck(-2353.278, -1909.568, 95.78357)',
		'GMR.DefineUnstuck(-2353.12, -1926.774, 95.78642)',
		'GMR.DefineUnstuck(-2351.08, -1935.631, 95.79419)',
		'GMR.DefineUnstuck(-2334.25, -1959.767, 95.95033)',
		'GMR.DefineUnstuck(-2325.783, -1975.401, 95.82619)',
		'GMR.DefineUnstuck(-2323.68, -1992.358, 95.78658)',
		'GMR.DefineUnstuck(-2322.497, -2010.122, 95.79246)',
		'GMR.DefineUnstuck(-2321.359, -2027.199, 95.79305)',
		'GMR.DefineUnstuck(-2320.177, -2044.94, 95.81173)',
		'GMR.DefineUnstuck(-2319.037, -2062.053, 95.79425)',
		'GMR.DefineUnstuck(-2317.859, -2079.735, 95.94518)',
		'GMR.DefineUnstuck(-2316.68, -2097.441, 97.03757)',
		'GMR.DefineUnstuck(-2311.425, -2113.614, 96.1806)',
		'GMR.DefineUnstuck(-2300.854, -2127.897, 96.68309)',
		'GMR.DefineUnstuck(-2290.404, -2141.54, 95.79432)',
		'GMR.DefineUnstuck(-2279.614, -2155.628, 95.79432)',
		'GMR.DefineUnstuck(-2269.103, -2169.221, 96.22282)',
		'GMR.DefineUnstuck(-2252.889, -2175.247, 94.951)',
		'GMR.DefineUnstuck(-2236.303, -2179.872, 94.41177)',
		'GMR.DefineUnstuck(-2219.283, -2184.765, 94.67985)',
		'GMR.DefineUnstuck(-2202.767, -2189.513, 95.55007)',
		'GMR.DefineUnstuck(-2185.712, -2194.416, 95.87889)',
		'GMR.DefineUnstuck(-2168.625, -2199.329, 95.97695)',
		'GMR.DefineUnstuck(-2152.108, -2204.077, 95.78809)',
		'GMR.DefineUnstuck(-2135.249, -2209.511, 96.05093)',
		'GMR.DefineUnstuck(-2119.702, -2216.833, 95.98329)',
		'GMR.DefineUnstuck(-2103.753, -2224.68, 95.78756)',
		'GMR.DefineUnstuck(-2089.231, -2233.79, 95.84248)',
		'GMR.DefineUnstuck(-2074.662, -2243.982, 95.78807)',
		'GMR.DefineUnstuck(-2060.125, -2254.158, 95.78807)',
		'GMR.DefineUnstuck(-2046.047, -2264.014, 95.6196)',
		'GMR.DefineUnstuck(-2031.482, -2274.21, 95.99337)',
		'GMR.DefineUnstuck(-2017.432, -2284.046, 96.14614)',
		'GMR.DefineUnstuck(-2002.867, -2294.242, 94.55312)',
		'GMR.DefineUnstuck(-1988.817, -2304.078, 94.11375)',
		'GMR.DefineUnstuck(-1974.415, -2314.494, 95.36381)',
		'GMR.DefineUnstuck(-1962.052, -2326.327, 95.7857)',
		'GMR.DefineUnstuck(-1949.649, -2339.066, 95.78592)',
		'GMR.DefineUnstuck(-1937.886, -2351.69, 95.78592)',
		'GMR.DefineUnstuck(-1926.294, -2365.019, 95.78592)',
		'GMR.DefineUnstuck(-1916.044, -2378.813, 95.88581)',
		'GMR.DefineUnstuck(-1905.462, -2393.058, 95.89334)',
		'GMR.DefineUnstuck(-1894.86, -2407.331, 95.78794)',
		'GMR.DefineUnstuck(-1884.633, -2421.098, 95.89745)',
		'GMR.DefineUnstuck(-1874.031, -2435.371, 95.47116)',
		'GMR.DefineUnstuck(-1863.277, -2449.528, 94.63368)',
		'GMR.DefineUnstuck(-1852.552, -2462.911, 92.90903)',
		'GMR.DefineUnstuck(-1841.106, -2476.5, 91.88346)',
		'GMR.DefineUnstuck(-1827.565, -2487.012, 91.66683)',
		'GMR.DefineUnstuck(-1813.101, -2497.346, 91.66683)',
		'GMR.DefineUnstuck(-1798.602, -2506.571, 91.6711)',
		'GMR.DefineUnstuck(-1783.62, -2516.081, 91.73967)',
		'GMR.DefineUnstuck(-1769.297, -2525.57, 91.6978)',
		'GMR.DefineUnstuck(-1754.731, -2535.768, 91.89397)',
		'GMR.DefineUnstuck(-1740.71, -2545.583, 91.77264)',
		'GMR.DefineUnstuck(-1726.142, -2555.777, 91.66901)',
		'GMR.DefineUnstuck(-1709.304, -2559.984, 91.6708)',
		'GMR.DefineUnstuck(-1694.083, -2553.056, 91.38154)',
		'GMR.DefineUnstuck(-1679.165, -2543.413, 92.39275)',
		'GMR.DefineUnstuck(-1663.894, -2535.611, 92.76616)',
		'GMR.DefineUnstuck(-1647.947, -2527.749, 91.74962)',
		'GMR.DefineUnstuck(-1632.525, -2520.25, 91.67081)',
		'GMR.DefineUnstuck(-1615.22, -2517.299, 91.7055)',
		'GMR.DefineUnstuck(-1599.931, -2524.41, 91.70592)',
		'GMR.DefineUnstuck(-1587.987, -2537.545, 91.86251)',
		'GMR.DefineUnstuck(-1577.328, -2551.025, 91.79441)',
		'GMR.DefineUnstuck(-1561.947, -2558.925, 92.24903)',
		'GMR.DefineUnstuck(-1544.905, -2558.089, 91.92575)',
		'GMR.DefineUnstuck(-1527.353, -2555.48, 91.82522)',
		'GMR.DefineUnstuck(-1509.713, -2553.26, 91.93179)',
		'GMR.DefineUnstuck(-1492.67, -2551.346, 91.73786)',
		'GMR.DefineUnstuck(-1475.001, -2549.362, 91.6273)',
		'GMR.DefineUnstuck(-1457.924, -2547.445, 91.90913)',
		'GMR.DefineUnstuck(-1440.347, -2544.858, 93.72899)',
		'GMR.DefineUnstuck(-1423.859, -2538.459, 95.27538)',
		'GMR.DefineUnstuck(-1408.554, -2530.721, 95.5085)',
		'GMR.DefineUnstuck(-1392.709, -2522.711, 95.8269)',
		'GMR.DefineUnstuck(-1377.373, -2514.958, 96.60423)',
		'GMR.DefineUnstuck(-1361.505, -2506.936, 95.78819)',
		'GMR.DefineUnstuck(-1346.106, -2499.151, 96.36708)',
		'GMR.DefineUnstuck(-1330.364, -2491.192, 96.26097)',
		'GMR.DefineUnstuck(-1315.027, -2483.439, 95.79759)',
		'GMR.DefineUnstuck(-1299.128, -2475.401, 95.64001)',
		'GMR.DefineUnstuck(-1283.854, -2467.679, 95.38213)',
		'GMR.DefineUnstuck(-1268.018, -2459.673, 94.56471)',
		'GMR.DefineUnstuck(-1252.151, -2451.652, 92.94018)',
		'GMR.DefineUnstuck(-1235.906, -2446.123, 91.75319)',
		'GMR.DefineUnstuck(-1218.271, -2445.005, 92.16798)',
		'GMR.DefineUnstuck(-1201.121, -2445.651, 93.26802)',
		'GMR.DefineUnstuck(-1183.396, -2447.046, 93.93438)',
		'GMR.DefineUnstuck(-1166.223, -2448.442, 94.79327)',
		'GMR.DefineUnstuck(-1148.78, -2451.29, 95.02538)',
		'GMR.DefineUnstuck(-1131.784, -2453.827, 95.02383)',
		'GMR.DefineUnstuck(-1114.222, -2456.37, 94.86257)',
		'GMR.DefineUnstuck(-1097.249, -2458.827, 93.7957)',
		'GMR.DefineUnstuck(-1079.618, -2461.38, 91.83275)',
		'GMR.DefineUnstuck(-1062.091, -2463.918, 91.66756)',
		'GMR.DefineUnstuck(-1045.093, -2466.379, 91.67564)',
		'GMR.DefineUnstuck(-1027.526, -2469.107, 91.66697)',
		'GMR.DefineUnstuck(-1010.947, -2473.471, 91.66697)',
		'GMR.DefineUnstuck(-993.9058, -2478.543, 91.82997)',
		'GMR.DefineUnstuck(-977.4888, -2483.43, 92.6955)',
		'GMR.DefineUnstuck(-961.1028, -2490.224, 93.94011)',
		'GMR.DefineUnstuck(-945.9968, -2498.407, 95.66373)',
		'GMR.DefineUnstuck(-930.8124, -2507.589, 96.18513)',
		'GMR.DefineUnstuck(-915.6101, -2516.81, 96.5279)',
		'GMR.DefineUnstuck(-900.9167, -2525.722, 95.83059)',
		'GMR.DefineUnstuck(-885.7444, -2534.925, 94.31663)',
		'GMR.DefineUnstuck(-871.0509, -2543.837, 92.71811)',
		'GMR.DefineUnstuck(-855.8786, -2553.039, 91.7105)',
		'GMR.DefineUnstuck(-841.1851, -2561.951, 91.70649)',
		'GMR.DefineUnstuck(-825.8281, -2570.79, 91.66671)',
		'GMR.DefineUnstuck(-809.6691, -2576.639, 91.66684)',
		'GMR.DefineUnstuck(-792.9835, -2582.678, 91.67519)',
		'GMR.DefineUnstuck(-776.7034, -2589.709, 92.35994)',
		'GMR.DefineUnstuck(-761.822, -2598.288, 94.69774)',
		'GMR.DefineUnstuck(-746.418, -2607.167, 95.77505)',
		'GMR.DefineUnstuck(-731.5598, -2615.732, 95.78764)',
		'GMR.DefineUnstuck(-716.1548, -2624.558, 95.86154)',
		'GMR.DefineUnstuck(-700.9117, -2632.413, 95.78787)',
		'GMR.DefineUnstuck(-684.7048, -2639.646, 95.78906)',
		'GMR.DefineUnstuck(-668.736, -2645.899, 95.78759)',
		'GMR.DefineUnstuck(-651.9272, -2651.693, 95.78759)',
		'GMR.DefineUnstuck(-634.7488, -2656.146, 95.78759)',
		'GMR.DefineUnstuck(-617.6502, -2657.35, 95.78759)',
		'GMR.DefineUnstuck(-599.8713, -2657.544, 95.78759)',
		'GMR.DefineUnstuck(-582.7503, -2656.702, 95.72477)',
		'GMR.DefineUnstuck(-565.0261, -2655.295, 95.62666)',
		'GMR.DefineUnstuck(-547.9298, -2653.938, 95.53902)',
		'GMR.DefineUnstuck(-530.2045, -2652.545, 95.72519)',
		'GMR.DefineUnstuck(-513.0303, -2652.041, 95.6357)',
		'GMR.DefineUnstuck(-495.2871, -2651.792, 95.71513)',
		'GMR.DefineUnstuck(-478.1061, -2649.891, 95.69365)',
		'GMR.DefineUnstuck(-462.2958, -2642.446, 95.8517)',
		'GMR.DefineUnstuck(-452.442, -2633.092, 95.71399)',
		'GMR.DefineUnstuck(-446.0053, -2621.266, 95.93832)',
		'GMR.DefineUnstuck(-436.7325, -2597.015, 95.78767)',
		'GMR.DefineUnstuck(-437.2129, -2595.791, 95.78767)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
		'GMR.DefineHearthstoneBindLocation(-2365.370117, -347.309998, -8.873650, 6747)'
	}
)

GMR.DefineQuest("Tauren", "WARRIOR", 1498, "Path of Defense", "Grinding", 185.746002, -3597.209961, 27.338301, 5810, 185.746002, -3597.209961, 27.338301, 5810, {},
	{
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'GMR.DefineProfileCenter(729.9128, -4032.058, -6.387927, 30)',
		'GMR.DefineProfileCenter(804.6252, -4035.054, -11.9247, 30)',
		'GMR.DefineProfileCenter(753.4786, -4108.59, -10.18405, 30)',
		'GMR.DefineProfileCenter(919.6711, -4036.738, -13.31126, 30)',
		'GMR.DefineProfileCenter(953.2545, -4135.906, -10.86135, 30)',
		'GMR.DefineProfileCenter(961.916, -4257.97, -8.314043, 30)',
		'GMR.DefineAreaBlacklist(876.708008, -4229.609863, -11.155500, 50)',
		'GMR.DefineQuestEnemyId(3130)',
		'GMR.DefineQuestEnemyId(3131)'
	}
)

GMR.DefineQuest("Tauren", "WARRIOR", 1502, "Thun'grim Firegaze", "TalkTo", 185.746002, -3597.209961, 27.338301, 5810, -437.619995, -3176.260010, 211.384995, 5878, {},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

GMR.DefineQuest( "Tauren", "WARRIOR", 1503, "|cFFFFAB48 Forged Steel - Warrior Weapon |r", "Custom", -437.619995, -3176.260010, 211.384995, 5878, -437.619995, -3176.260010, 211.384995, 5878, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1503, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-2542.016, -686.205, -7.264302, 60) then 
				GMR.MeshTo(-2542.016, -686.205, -7.264302)
			else
				local object = GMR.GetObjectWithInfo({ id = 58369, rawType = 8 })
				if object then 
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-206.9702, -2918.022, 91.66673, 120)',
		'GMR.DefineProfileCenter(-187.4508, -2954.417, 91.78155, 120)',
		'GMR.DefineAreaBlacklist(-141.016388, -3012.611816, 91.666862, 30)',
		'GMR.DefineAreaBlacklist(-218.262482, -3024.780518, 91.666862, 30)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',
		'GMR.DefineQuestEnemyId(58369)',
		'GMR_DefineCustomObjectID(58369)'
	}
)
----------------------------------------------------------------------------------------------------------------------------------
--  			 		END --- [ Warrior ][Level 10][Tauren] - Defensive Stance
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--  		             START --- [ Shaman ][Level 14][Tauren] - Fire Totem
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest("Tauren", "SHAMAN", 2984, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 1 |r", "TalkTo", -2298.959961, -437.742004, -5.355070, 3066, 268.898987, -3055.149902, 97.164200, 5907, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(-2300.412, -439.0248, -5.438341, 150)',
		'GMR.DefineUnstuck(-2309.604, -447.745, -5.438341)',
		'GMR.DefineUnstuck(-2319.745, -457.4068, -5.438341)',
		'GMR.DefineUnstuck(-2329.892, -467.0734, -5.699085)',
		'GMR.DefineUnstuck(-2339.992, -476.6965, -7.901557)',
		'GMR.DefineUnstuck(-2350.297, -486.1712, -8.003516)',
		'GMR.DefineUnstuck(-2365.798, -500.8198, -9.336405)',
		'GMR.DefineUnstuck(-2380.89, -515.4237, -8.861604)',
		'GMR.DefineUnstuck(-2377.332, -536.8203, -7.979251)',
		'GMR.DefineUnstuck(-2368.9, -556.076, -5.759156)',
		'GMR.DefineUnstuck(-2364.102, -566.9736, -5.164005)',
		'GMR.DefineUnstuck(-2359.74, -576.3035, -5.914311)',
		'GMR.DefineUnstuck(-2353.676, -585.1057, -7.665729)',
		'GMR.DefineUnstuck(-2348.027, -593.9562, -8.933048)',
		'GMR.DefineUnstuck(-2342.741, -602.8329, -8.432564)',
		'GMR.DefineUnstuck(-2333.007, -619.6607, -7.901898)',
		'GMR.DefineUnstuck(-2327.219, -639.6141, -9.051932)',
		'GMR.DefineUnstuck(-2331.771, -659.8005, -9.424416)',
		'GMR.DefineUnstuck(-2335.933, -680.5552, -9.422823)',
		'GMR.DefineUnstuck(-2339.727, -701.0407, -9.35604)',
		'GMR.DefineUnstuck(-2342.85, -721.7955, -9.424696)',
		'GMR.DefineUnstuck(-2345.917, -742.4005, -9.424696)',
		'GMR.DefineUnstuck(-2348.936, -763.3521, -9.424696)',
		'GMR.DefineUnstuck(-2351.906, -784.0138, -9.424481)',
		'GMR.DefineUnstuck(-2356.208, -804.7512, -9.424481)',
		'GMR.DefineUnstuck(-2359.594, -821.2064, -9.418588)',
		'GMR.DefineUnstuck(-2361.468, -831.4962, -9.355806)',
		'GMR.DefineUnstuck(-2362.254, -842.1483, -9.20341)',
		'GMR.DefineUnstuck(-2363.277, -852.4929, -9.140267)',
		'GMR.DefineUnstuck(-2364.363, -863.0421, -9.297077)',
		'GMR.DefineUnstuck(-2365.43, -873.4033, -9.42463)',
		'GMR.DefineUnstuck(-2366.499, -883.7855, -9.423903)',
		'GMR.DefineUnstuck(-2368.021, -894.3366, -9.423213)',
		'GMR.DefineUnstuck(-2370.874, -904.3459, -9.424398)',
		'GMR.DefineUnstuck(-2374.112, -914.356, -9.423615)',
		'GMR.DefineUnstuck(-2377.47, -924.2136, -9.411074)',
		'GMR.DefineUnstuck(-2380.856, -934.1525, -9.36812)',
		'GMR.DefineUnstuck(-2384.297, -944.2505, -9.336666)',
		'GMR.DefineUnstuck(-2391.013, -963.9615, -9.302009)',
		'GMR.DefineUnstuck(-2397.792, -983.8593, -9.403821)',
		'GMR.DefineUnstuck(-2404.552, -1003.72, -9.417752)',
		'GMR.DefineUnstuck(-2411.347, -1023.589, -9.424297)',
		'GMR.DefineUnstuck(-2418.227, -1043.453, -9.424297)',
		'GMR.DefineUnstuck(-2424.843, -1063.381, -9.424297)',
		'GMR.DefineUnstuck(-2431.122, -1083.443, -9.424297)',
		'GMR.DefineUnstuck(-2437.02, -1103.585, -9.423802)',
		'GMR.DefineUnstuck(-2440.788, -1124.221, -9.355917)',
		'GMR.DefineUnstuck(-2442.866, -1145.123, -9.362154)',
		'GMR.DefineUnstuck(-2442.413, -1166.067, -9.402051)',
		'GMR.DefineUnstuck(-2440.582, -1186.799, -9.348249)',
		'GMR.DefineUnstuck(-2436.255, -1207.418, -9.417118)',
		'GMR.DefineUnstuck(-2430.528, -1227.634, -9.390582)',
		'GMR.DefineUnstuck(-2425.564, -1248.039, -9.417701)',
		'GMR.DefineUnstuck(-2420.645, -1268.261, -9.216035)',
		'GMR.DefineUnstuck(-2415.007, -1288.442, -8.614432)',
		'GMR.DefineUnstuck(-2407.508, -1308.003, -7.976325)',
		'GMR.DefineUnstuck(-2401.642, -1328.124, -4.876202)',
		'GMR.DefineUnstuck(-2395.76, -1348.292, 0.2544041)',
		'GMR.DefineUnstuck(-2392.404, -1358.412, 3.509296)',
		'GMR.DefineUnstuck(-2384.996, -1377.852, 9.740407)',
		'GMR.DefineUnstuck(-2377.197, -1397.37, 16.25502)',
		'GMR.DefineUnstuck(-2373.165, -1407.065, 19.54491)',
		'GMR.DefineUnstuck(-2365.104, -1426.455, 25.31485)',
		'GMR.DefineUnstuck(-2357.572, -1446.055, 30.6524)',
		'GMR.DefineUnstuck(-2352.574, -1466.38, 35.75291)',
		'GMR.DefineUnstuck(-2349.836, -1487.183, 40.17427)',
		'GMR.DefineUnstuck(-2347.982, -1508.073, 45.25946)',
		'GMR.DefineUnstuck(-2347.67, -1529.091, 51.11731)',
		'GMR.DefineUnstuck(-2347.461, -1545.365, 55.81789)',
		'GMR.DefineUnstuck(-2347.279, -1559.56, 59.88282)',
		'GMR.DefineUnstuck(-2347.01, -1580.538, 65.22916)',
		'GMR.DefineUnstuck(-2347.385, -1601.53, 70.59228)',
		'GMR.DefineUnstuck(-2348.589, -1622.465, 75.73613)',
		'GMR.DefineUnstuck(-2349.794, -1643.431, 80.06741)',
		'GMR.DefineUnstuck(-2350.062, -1664.44, 83.18784)',
		'GMR.DefineUnstuck(-2350.32, -1685.459, 85.31413)',
		'GMR.DefineUnstuck(-2352.324, -1706.212, 87.34789)',
		'GMR.DefineUnstuck(-2357.061, -1726.53, 89.1586)',
		'GMR.DefineUnstuck(-2355.924, -1747.451, 90.75157)',
		'GMR.DefineUnstuck(-2353.238, -1768.278, 90.99692)',
		'GMR.DefineUnstuck(-2350.907, -1789.156, 92.53022)',
		'GMR.DefineUnstuck(-2349.691, -1810.073, 95.4937)',
		'GMR.DefineUnstuck(-2349.148, -1830.898, 95.78258)',
		'GMR.DefineUnstuck(-2350.49, -1851.91, 95.99449)',
		'GMR.DefineUnstuck(-2352.322, -1872.788, 95.85603)',
		'GMR.DefineUnstuck(-2354.174, -1893.896, 95.78316)',
		'GMR.DefineUnstuck(-2355.149, -1914.532, 95.78316)',
		'GMR.DefineUnstuck(-2350.495, -1934.97, 95.79388)',
		'GMR.DefineUnstuck(-2338.777, -1952.32, 95.7946)',
		'GMR.DefineUnstuck(-2326.992, -1969.619, 95.86129)',
		'GMR.DefineUnstuck(-2322.79, -1990.196, 95.79163)',
		'GMR.DefineUnstuck(-2320.976, -2011.075, 95.79257)',
		'GMR.DefineUnstuck(-2317.465, -2031.794, 95.79316)',
		'GMR.DefineUnstuck(-2313.073, -2052.329, 95.79559)',
		'GMR.DefineUnstuck(-2308.298, -2072.775, 95.79405)',
		'GMR.DefineUnstuck(-2302.641, -2093.016, 95.79405)',
		'GMR.DefineUnstuck(-2295.857, -2112.89, 95.99733)',
		'GMR.DefineUnstuck(-2289.067, -2132.784, 95.79393)',
		'GMR.DefineUnstuck(-2281.807, -2152.482, 95.79393)',
		'GMR.DefineUnstuck(-2271.46, -2170.667, 95.85539)',
		'GMR.DefineUnstuck(-2255.053, -2183.086, 95.70628)',
		'GMR.DefineUnstuck(-2234.375, -2185.403, 94.36761)',
		'GMR.DefineUnstuck(-2213.38, -2185.876, 94.97216)',
		'GMR.DefineUnstuck(-2192.553, -2186.345, 95.75027)',
		'GMR.DefineUnstuck(-2171.538, -2186.818, 95.78608)',
		'GMR.DefineUnstuck(-2159.561, -2191.497, 95.78767)',
		'GMR.DefineUnstuck(-2142.308, -2203.339, 95.80241)',
		'GMR.DefineUnstuck(-2123.973, -2213.618, 96.03687)',
		'GMR.DefineUnstuck(-2105.462, -2223.712, 95.8043)',
		'GMR.DefineUnstuck(-2087.228, -2233.655, 95.78765)',
		'GMR.DefineUnstuck(-2068.421, -2243.357, 95.78765)',
		'GMR.DefineUnstuck(-2049.503, -2252.473, 96.00509)',
		'GMR.DefineUnstuck(-2030.588, -2261.595, 95.87489)',
		'GMR.DefineUnstuck(-2013.503, -2273.835, 95.54097)',
		'GMR.DefineUnstuck(-1999.329, -2289.214, 94.33701)',
		'GMR.DefineUnstuck(-1985.878, -2304.882, 94.1442)',
		'GMR.DefineUnstuck(-1974.36, -2318.298, 95.56704)',
		'GMR.DefineUnstuck(-1965.359, -2328.782, 95.78574)',
		'GMR.DefineUnstuck(-1955.049, -2339.253, 95.92216)',
		'GMR.DefineUnstuck(-1942.913, -2356.082, 96.33862)',
		'GMR.DefineUnstuck(-1930.337, -2373.107, 96.3389)',
		'GMR.DefineUnstuck(-1917.398, -2389.434, 96.7374)',
		'GMR.DefineUnstuck(-1904.608, -2406.089, 96.6755)',
		'GMR.DefineUnstuck(-1894.618, -2424.561, 96.79437)',
		'GMR.DefineUnstuck(-1883.285, -2442.34, 95.59605)',
		'GMR.DefineUnstuck(-1870.255, -2458.81, 94.88637)',
		'GMR.DefineUnstuck(-1854.247, -2472.062, 92.4259)',
		'GMR.DefineUnstuck(-1838.59, -2486.37, 91.68344)',
		'GMR.DefineUnstuck(-1823.228, -2500.409, 91.66702)',
		'GMR.DefineUnstuck(-1807.711, -2514.59, 91.66702)',
		'GMR.DefineUnstuck(-1792.209, -2528.757, 91.66946)',
		'GMR.DefineUnstuck(-1776.707, -2542.923, 91.67006)',
		'GMR.DefineUnstuck(-1759.567, -2554.468, 91.6675)',
		'GMR.DefineUnstuck(-1739.68, -2561.202, 91.6675)',
		'GMR.DefineUnstuck(-1718.686, -2560.57, 91.67067)',
		'GMR.DefineUnstuck(-1699.889, -2551.74, 91.45098)',
		'GMR.DefineUnstuck(-1681.129, -2542.312, 92.32235)',
		'GMR.DefineUnstuck(-1661.904, -2533.862, 92.61882)',
		'GMR.DefineUnstuck(-1642.66, -2525.404, 91.54783)',
		'GMR.DefineUnstuck(-1623.025, -2518.421, 91.67059)',
		'GMR.DefineUnstuck(-1604.044, -2523.59, 91.71583)',
		'GMR.DefineUnstuck(-1588.344, -2537.536, 91.87997)',
		'GMR.DefineUnstuck(-1571.532, -2550.04, 91.70902)',
		'GMR.DefineUnstuck(-1550.773, -2552.153, 91.68961)',
		'GMR.DefineUnstuck(-1529.944, -2551.772, 91.70132)',
		'GMR.DefineUnstuck(-1508.948, -2551.389, 91.81796)',
		'GMR.DefineUnstuck(-1487.762, -2551.002, 91.70361)',
		'GMR.DefineUnstuck(-1466.936, -2550.621, 91.6463)',
		'GMR.DefineUnstuck(-1445.898, -2550.237, 93.13027)',
		'GMR.DefineUnstuck(-1425.878, -2544.845, 94.90418)',
		'GMR.DefineUnstuck(-1413.293, -2528.248, 95.53058)',
		'GMR.DefineUnstuck(-1401.991, -2510.566, 95.7861)',
		'GMR.DefineUnstuck(-1386.513, -2496.398, 95.7861)',
		'GMR.DefineUnstuck(-1367.701, -2487.427, 95.7861)',
		'GMR.DefineUnstuck(-1347.443, -2482.048, 95.7861)',
		'GMR.DefineUnstuck(-1327.322, -2476.231, 95.78779)',
		'GMR.DefineUnstuck(-1306.766, -2471.982, 95.83887)',
		'GMR.DefineUnstuck(-1287.009, -2464.865, 95.3571)',
		'GMR.DefineUnstuck(-1267.232, -2457.74, 94.36489)',
		'GMR.DefineUnstuck(-1247.113, -2451.904, 92.38973)',
		'GMR.DefineUnstuck(-1226.591, -2448.451, 91.83871)',
		'GMR.DefineUnstuck(-1205.849, -2445.394, 92.89293)',
		'GMR.DefineUnstuck(-1184.886, -2446.878, 93.91534)',
		'GMR.DefineUnstuck(-1164.053, -2450.579, 94.87491)',
		'GMR.DefineUnstuck(-1143.28, -2451.984, 95.02499)',
		'GMR.DefineUnstuck(-1122.282, -2451.749, 95.01344)',
		'GMR.DefineUnstuck(-1101.425, -2453.46, 94.04797)',
		'GMR.DefineUnstuck(-1082.231, -2461.544, 92.0395)',
		'GMR.DefineUnstuck(-1063.025, -2469.881, 91.82215)',
		'GMR.DefineUnstuck(-1042.781, -2474.977, 92.12329)',
		'GMR.DefineUnstuck(-1022.769, -2481.071, 91.69903)',
		'GMR.DefineUnstuck(-1002.207, -2485.022, 91.70178)',
		'GMR.DefineUnstuck(-981.3221, -2486.543, 92.41056)',
		'GMR.DefineUnstuck(-960.5475, -2489.612, 93.94044)',
		'GMR.DefineUnstuck(-940.1898, -2494.598, 94.81144)',
		'GMR.DefineUnstuck(-921.5842, -2504.376, 95.77431)',
		'GMR.DefineUnstuck(-904.6262, -2516.692, 95.7532)',
		'GMR.DefineUnstuck(-887.8807, -2529.26, 94.86201)',
		'GMR.DefineUnstuck(-871.823, -2542.909, 92.84006)',
		'GMR.DefineUnstuck(-856.0718, -2556.702, 91.68068)',
		'GMR.DefineUnstuck(-840.1941, -2570.606, 91.66708)',
		'GMR.DefineUnstuck(-822.4186, -2581.127, 91.66708)',
		'GMR.DefineUnstuck(-801.9672, -2585.091, 91.66708)',
		'GMR.DefineUnstuck(-781.1731, -2589.121, 92.09915)',
		'GMR.DefineUnstuck(-761.8629, -2596.646, 94.47989)',
		'GMR.DefineUnstuck(-743.9402, -2607.454, 95.78763)',
		'GMR.DefineUnstuck(-726.4202, -2618.976, 95.78759)',
		'GMR.DefineUnstuck(-707.5321, -2628.136, 95.7876)',
		'GMR.DefineUnstuck(-689.9926, -2639.6, 95.7876)',
		'GMR.DefineUnstuck(-670.5285, -2647.382, 95.7876)',
		'GMR.DefineUnstuck(-649.9329, -2650.306, 95.7876)',
		'GMR.DefineUnstuck(-628.7993, -2649.021, 95.8072)',
		'GMR.DefineUnstuck(-607.8394, -2650.318, 95.78782)',
		'GMR.DefineUnstuck(-586.8586, -2651.618, 95.59193)',
		'GMR.DefineUnstuck(-565.8988, -2652.916, 95.57395)',
		'GMR.DefineUnstuck(-545.0703, -2652.871, 95.5621)',
		'GMR.DefineUnstuck(-524.0945, -2655.323, 95.61819)',
		'GMR.DefineUnstuck(-503.8086, -2660.062, 95.98724)',
		'GMR.DefineUnstuck(-483.0671, -2664.371, 97.09891)',
		'GMR.DefineUnstuck(-462.5362, -2668.022, 95.57126)',
		'GMR.DefineUnstuck(-441.8606, -2671.699, 95.97276)',
		'GMR.DefineUnstuck(-421.1643, -2675.379, 95.69243)',
		'GMR.DefineUnstuck(-400.465, -2678.854, 95.63148)',
		'GMR.DefineUnstuck(-379.4603, -2679.681, 95.80126)',
		'GMR.DefineUnstuck(-358.4765, -2680.507, 95.72545)',
		'GMR.DefineUnstuck(-337.4927, -2681.333, 95.73581)',
		'GMR.DefineUnstuck(-316.509, -2682.159, 93.69032)',
		'GMR.DefineUnstuck(-295.5043, -2682.986, 93.00422)',
		'GMR.DefineUnstuck(-274.5408, -2683.811, 93.81226)',
		'GMR.DefineUnstuck(-253.5571, -2684.637, 95.83475)',
		'GMR.DefineUnstuck(-232.5717, -2685.463, 95.90522)',
		'GMR.DefineUnstuck(-211.5978, -2686.288, 95.6741)',
		'GMR.DefineUnstuck(-190.5931, -2687.115, 93.83438)',
		'GMR.DefineUnstuck(-169.7982, -2687.934, 93.17869)',
		'GMR.DefineUnstuck(-148.7934, -2688.76, 93.67503)',
		'GMR.DefineUnstuck(-127.6208, -2689.594, 95.481)',
		'GMR.DefineUnstuck(-106.8049, -2690.413, 95.83288)',
		'GMR.DefineUnstuck(-85.7392, -2692.061, 95.83288)',
		'GMR.DefineUnstuck(-65.47931, -2696.91, 95.94266)',
		'GMR.DefineUnstuck(-45.05604, -2701.798, 95.03088)',
		'GMR.DefineUnstuck(-24.61234, -2706.69, 93.00951)',
		'GMR.DefineUnstuck(-4.604276, -2712.911, 91.69798)',
		'GMR.DefineUnstuck(14.21676, -2722.137, 91.66759)',
		'GMR.DefineUnstuck(30.80549, -2734.896, 92.52334)',
		'GMR.DefineUnstuck(44.81141, -2750.499, 92.58186)',
		'GMR.DefineUnstuck(55.24516, -2768.74, 94.73958)',
		'GMR.DefineUnstuck(65.11897, -2786.052, 95.83884)',
		'GMR.DefineUnstuck(71.69242, -2799.313, 95.8367)',
		'GMR.DefineUnstuck(79.10711, -2818.935, 95.83631)',
		'GMR.DefineUnstuck(82.24281, -2839.679, 95.9592)',
		'GMR.DefineUnstuck(84.2176, -2860.586, 95.86915)',
		'GMR.DefineUnstuck(86.19436, -2881.514, 95.83399)',
		'GMR.DefineUnstuck(87.54907, -2895.856, 95.83455)',
		'GMR.DefineUnstuck(89.51991, -2916.721, 95.93264)',
		'GMR.DefineUnstuck(92.39014, -2937.474, 95.95033)',
		'GMR.DefineUnstuck(97.95971, -2957.529, 95.83396)',
		'GMR.DefineUnstuck(103.6297, -2977.945, 93.97709)',
		'GMR.DefineUnstuck(109.2491, -2998.179, 92.66338)',
		'GMR.DefineUnstuck(114.8741, -3018.434, 91.86377)',
		'GMR.DefineUnstuck(117.4293, -3038.941, 91.89199)',
		'GMR.DefineUnstuck(116.5954, -3058.558, 94.3672)',
		'GMR.DefineUnstuck(115.6964, -3079.707, 95.83218)',
		'GMR.DefineUnstuck(116.178, -3100.638, 95.83218)',
		'GMR.DefineUnstuck(118.1597, -3116.623, 95.83218)',
		'GMR.DefineUnstuck(132.4454, -3113.34, 96.45045)',
		'GMR.DefineUnstuck(150.3721, -3104.683, 96.36921)',
		'GMR.DefineUnstuck(168.7056, -3094.398, 95.8255)',
		'GMR.DefineUnstuck(186.6651, -3083.521, 92.46522)',
		'GMR.DefineUnstuck(205.1027, -3073.469, 91.67282)',
		'GMR.DefineUnstuck(222.9568, -3062.75, 92.3853)',
		'GMR.DefineUnstuck(231.9091, -3056.944, 95.51712)',
		'GMR.DefineUnstuck(246.4859, -3047.584, 96.12763)',
		-- Bloodhoof Village Tauren
		'GMR.DefineSellVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineRepairVendor(-2284.469971, -310.260010, -9.341590, 3080)',
		'GMR.DefineAmmoVendor(-2247.810059, -308.157013, -9.305640, 3076)',
		'GMR.DefineGoodsVendor(-2378.959961, -399.268005, -3.806000, 3884)',
		'GMR.DefineProfileMailbox(-2337.225, -367.654, -8.445905, 143984)',
	}
)


GMR.DefineQuest("Tauren", "SHAMAN", nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Call of Fire (PART 2) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1524, 268.898987, -3055.149902, 97.164200, 5907 } -- Rites of the Earthmother 757 pre-req
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)


GMR.DefineQuest("Tauren", "SHAMAN", 1524, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 2 |r", "TalkTo", 268.898987, -3055.149902, 97.164200, 5907, -269.279296875, -4001.587890625, 168.54284667969, 5900, 
	{},
	{
		-- From Q Pickup in BARRENS to Bottom of Mountain
		'GMR.SkipTurnIn(false)',
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(258.59, -3041.02, 96.27245, 100)',
		'GMR.DefineUnstuck(255.0209, -3047.895, 96.20301)',
		'GMR.DefineUnstuck(249.5511, -3057.054, 96.03181)',
		'GMR.DefineUnstuck(243.1605, -3067.273, 95.20503)',
		'GMR.DefineUnstuck(237.2101, -3076.127, 91.74356)',
		'GMR.DefineUnstuck(231.4548, -3084.657, 91.66515)',
		'GMR.DefineUnstuck(225.4823, -3093.509, 93.4323)',
		'GMR.DefineUnstuck(219.5274, -3102.335, 94.06092)',
		'GMR.DefineUnstuck(213.7721, -3110.865, 93.91298)',
		'GMR.DefineUnstuck(207.8054, -3119.708, 93.20019)',
		'GMR.DefineUnstuck(202.0266, -3128.273, 92.19747)',
		'GMR.DefineUnstuck(197.2555, -3137.603, 91.64236)',
		'GMR.DefineUnstuck(194.7313, -3147.6, 91.33762)',
		'GMR.DefineUnstuck(192.1197, -3157.944, 90.66074)',
		'GMR.DefineUnstuck(189.6006, -3167.921, 89.99816)',
		'GMR.DefineUnstuck(186.989, -3178.264, 88.46404)',
		'GMR.DefineUnstuck(184.4339, -3188.383, 86.27196)',
		'GMR.DefineUnstuck(181.8531, -3198.605, 83.68327)',
		'GMR.DefineUnstuck(179.2518, -3208.907, 81.14092)',
		'GMR.DefineUnstuck(174.1108, -3229.268, 75.81031)',
		'GMR.DefineUnstuck(171.7518, -3249.983, 70.15285)',
		'GMR.DefineUnstuck(171.3713, -3260.624, 67.20923)',
		'GMR.DefineUnstuck(170.9707, -3281.233, 62.23823)',
		'GMR.DefineUnstuck(174.1404, -3302.302, 56.70175)',
		'GMR.DefineUnstuck(177.565, -3322.978, 51.73302)',
		'GMR.DefineUnstuck(180.8963, -3343.67, 46.94912)',
		'GMR.DefineUnstuck(182.8474, -3364.036, 42.24098)',
		'GMR.DefineUnstuck(184.3737, -3384.958, 37.40455)',
		'GMR.DefineUnstuck(193.989, -3403.541, 33.16894)',
		'GMR.DefineUnstuck(203.765, -3422.08, 30.18659)',
		'GMR.DefineUnstuck(215.4052, -3439.406, 29.56329)',
		'GMR.DefineUnstuck(228.5992, -3455.689, 28.38532)',
		'GMR.DefineUnstuck(240.6957, -3473.161, 27.31293)',
		'GMR.DefineUnstuck(248.2084, -3492.726, 27.18479)',
		'GMR.DefineUnstuck(255.6939, -3512.324, 27.16391)',
		'GMR.DefineUnstuck(263.1719, -3531.902, 26.19556)',
		'GMR.DefineUnstuck(265.6788, -3551.797, 26.82313)',
		'GMR.DefineUnstuck(260.1794, -3572.019, 27.20911)',
		'GMR.DefineUnstuck(254.5376, -3592.215, 30.05747)',
		'GMR.DefineUnstuck(248.8, -3612.35, 31.65029)',
		'GMR.DefineUnstuck(241.1766, -3631.784, 30.92603)',
		'GMR.DefineUnstuck(231.7336, -3647.645, 29.45645)',
		'GMR.DefineUnstuck(222.2463, -3658.166, 28.38605)',
		'GMR.DefineUnstuck(205.7688, -3671.116, 29.44319)',
		'GMR.DefineUnstuck(188.8952, -3683.532, 28.49074)',
		'GMR.DefineUnstuck(169.37, -3690.839, 27.12857)',
		'GMR.DefineUnstuck(148.7933, -3694.923, 25.86952)',
		'GMR.DefineUnstuck(140.7835, -3696.417, 26.07366)',
		'GMR.DefineUnstuck(130.3171, -3698.369, 26.84388)',
		'GMR.DefineUnstuck(120.2016, -3700.256, 27.33046)',
		'GMR.DefineUnstuck(109.6571, -3701.697, 27.17219)',
		'GMR.DefineUnstuck(99.39675, -3702.717, 25.79729)',
		'GMR.DefineUnstuck(88.80202, -3703.771, 23.83244)',
		'GMR.DefineUnstuck(78.18639, -3704.827, 24.2003)',
		'GMR.DefineUnstuck(67.95882, -3705.844, 24.87083)',
		'GMR.DefineUnstuck(57.32229, -3706.902, 27.70512)',
		'GMR.DefineUnstuck(47.0828, -3707.92, 28.4149)',
		'GMR.DefineUnstuck(36.53659, -3706.764, 28.24712)',
		'GMR.DefineUnstuck(26.71281, -3704.024, 27.84645)',
		'GMR.DefineUnstuck(16.64107, -3700.834, 28.56208)',
		'GMR.DefineUnstuck(6.498993, -3698.975, 28.81654)',
		'GMR.DefineUnstuck(-4.09821, -3698.925, 27.78838)',
		'GMR.DefineUnstuck(-14.72664, -3699.553, 27.02936)',
		'GMR.DefineUnstuck(-25.01966, -3700.162, 26.35486)',
		'GMR.DefineUnstuck(-35.64809, -3700.79, 25.9634)',
		'GMR.DefineUnstuck(-45.7979, -3702.334, 27.08802)',
		'GMR.DefineUnstuck(-56.11556, -3705.046, 28.27396)',
		'GMR.DefineUnstuck(-66.06763, -3707.661, 29.1044)',
		'GMR.DefineUnstuck(-76.36497, -3710.367, 28.03403)',
		'GMR.DefineUnstuck(-86.37798, -3712.999, 27.12788)',
		'GMR.DefineUnstuck(-94.18307, -3719.122, 25.09065)',
		'GMR.DefineUnstuck(-100.4295, -3724.289, 22.55893)',
		'GMR.DefineUnstuck(-104.549, -3735.972, 19.58081)',
		'GMR.DefineUnstuck(-104.3058, -3744.164, 17.64138)',
		'GMR.DefineUnstuck(-108.4359, -3752.039, 16.34175)',
		'GMR.DefineUnstuck(-111.0229, -3762.976, 16.34175)',
		'GMR.DefineUnstuck(-113.0448, -3771.523, 16.34175)',
		'GMR.DefineUnstuck(-117.9275, -3782.19, 16.34175)',
		'GMR.DefineUnstuck(-125.1925, -3790.661, 16.34175)',
		'GMR.DefineUnstuck(-130.9568, -3797.18, 16.97536)',
		'GMR.DefineUnstuck(-136.5933, -3806.939, 18.9605)',
		'GMR.DefineUnstuck(-140.5615, -3816.413, 20.95662)',
		'GMR.DefineUnstuck(-144.2993, -3826.275, 23.98198)',
		'GMR.DefineUnstuck(-145.9946, -3836.392, 29.00173)',
		'GMR.DefineUnstuck(-148.8529, -3846.539, 33.23373)',
		'GMR.DefineUnstuck(-151.4526, -3856.639, 34.96017)',
		'GMR.DefineUnstuck(-155.5326, -3866.205, 36.53297)',
		'GMR.DefineUnstuck(-160.2907, -3875.498, 38.49665)',
		'GMR.DefineUnstuck(-167.4866, -3882.959, 40.34301)',
		'GMR.DefineUnstuck(-175.618, -3889.614, 42.09916)',
		'GMR.DefineUnstuck(-185.5473, -3893.02, 43.52283)',
		'GMR.DefineUnstuck(-195.0662, -3894.99, 46.08632)',
		'GMR.DefineUnstuck(-199.3801, -3894.128, 47.54789)',

		-- Bottom of the Hill
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(-199.4623, -3894.181, 47.61382, 100)',
		'GMR.DefineUnstuck(-202.7007, -3895.856, 49.94935)',
		'GMR.DefineUnstuck(-207.5063, -3898.153, 53.00512)',
		'GMR.DefineUnstuck(-211.1808, -3899.976, 54.83481)',
		'GMR.DefineUnstuck(-217.0187, -3902.872, 58.64489)',
		'GMR.DefineUnstuck(-220.119, -3904.585, 61.49921)',
		'GMR.DefineUnstuck(-223.0736, -3906.341, 63.95193)',
		'GMR.DefineUnstuck(-226.1475, -3908.105, 66.56731)',
		'GMR.DefineUnstuck(-230.0374, -3907.042, 69.88348)',
		'GMR.DefineUnstuck(-233.9443, -3903.606, 74.20333)',
		'GMR.DefineUnstuck(-235.9505, -3901.594, 76.76254)',
		'GMR.DefineUnstuck(-240.0965, -3898.51, 80.06519)',
		'GMR.DefineUnstuck(-241.8339, -3897.622, 80.91915)',
		'GMR.DefineUnstuck(-245.7246, -3897.939, 81.23082)',
		'GMR.DefineUnstuck(-249.7455, -3898.227, 80.58408)',
		'GMR.DefineUnstuck(-253.1858, -3898.357, 81.33885)',
		'GMR.DefineUnstuck(-255.7887, -3898.384, 82.62493)',
		'GMR.DefineUnstuck(-260.0606, -3898.454, 86.03916)',
		'GMR.DefineUnstuck(-262.191, -3898.672, 88.32788)',
		'GMR.DefineUnstuck(-264.7761, -3899.039, 90.69366)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-269.8214, -3903.371, 93.71248)',
		'GMR.DefineUnstuck(-271.9857, -3906.041, 94.44247)',
		'GMR.DefineUnstuck(-274.2279, -3909.254, 94.85416)',
		'GMR.DefineUnstuck(-272.4892, -3910.983, 96.57925)',
		'GMR.DefineUnstuck(-269.3623, -3912.675, 99.5118)',
		'GMR.DefineUnstuck(-267.3385, -3913.77, 101.4796)',
		'GMR.DefineUnstuck(-265.0544, -3912.314, 102.8668)',
		'GMR.DefineUnstuck(-262.3954, -3911.038, 104.5286)',
		'GMR.DefineUnstuck(-260.1604, -3910.277, 105.8743)',
		'GMR.DefineUnstuck(-257.3842, -3909.249, 106.9683)',
		'GMR.DefineUnstuck(-254.3581, -3908.606, 107.1753)',
		'GMR.DefineUnstuck(-251.652, -3908.49, 107.2679)',
		'GMR.DefineUnstuck(-248.1356, -3908.915, 106.9351)',
		'GMR.DefineUnstuck(-245.5722, -3909.724, 106.1492)',
		'GMR.DefineUnstuck(-243.6041, -3912.237, 104.4027)',
		'GMR.DefineUnstuck(-242.3808, -3914.41, 103.3386)',
		'GMR.DefineUnstuck(-240.897, -3916.677, 102.6752)',
		'GMR.DefineUnstuck(-239.5852, -3920.414, 102.794)',
		'GMR.DefineUnstuck(-239.3499, -3923.955, 102.3959)',
		'GMR.DefineUnstuck(-239.368, -3927.391, 103.7777)',
		'GMR.DefineUnstuck(-239.2873, -3930.225, 105.0105)',
		'GMR.DefineUnstuck(-239.2332, -3935.089, 106.6324)',
		'GMR.DefineUnstuck(-239.1963, -3938.526, 108.1947)',
		'GMR.DefineUnstuck(-239.3192, -3941.252, 109.3255)',
		'GMR.DefineUnstuck(-240.7941, -3944.482, 110.1133)',
		'GMR.DefineUnstuck(-241.6382, -3946.946, 110.1196)',
		'GMR.DefineUnstuck(-242.7522, -3950.197, 110.3971)',
		'GMR.DefineUnstuck(-243.8751, -3953.563, 110.8127)',
		'GMR.DefineUnstuck(-244.5518, -3956.926, 111.6394)',
		'GMR.DefineUnstuck(-244.1009, -3961.622, 114.141)',
		'GMR.DefineUnstuck(-243.7141, -3964.791, 115.6663)',
		'GMR.DefineUnstuck(-243.0102, -3967.404, 117.6225)',
		'GMR.DefineUnstuck(-240.3202, -3968.368, 117.7073)',
		'GMR.DefineUnstuck(-237.2673, -3968.719, 117.5444)',
		'GMR.DefineUnstuck(-233.834, -3968.985, 116.3506)',
		'GMR.DefineUnstuck(-231.4871, -3970.2, 116.3824)',
		'GMR.DefineUnstuck(-229.748, -3971.973, 117.6124)',
		'GMR.DefineUnstuck(-228.0222, -3974.524, 118.8738)',
		'GMR.DefineUnstuck(-227.0834, -3978.285, 120.0969)',
		'GMR.DefineUnstuck(-227.0988, -3980.87, 121.8627)',
		'GMR.DefineUnstuck(-227.1036, -3983.955, 124.4939)',
		'GMR.DefineUnstuck(-227.1822, -3987.496, 127.6713)',
		'GMR.DefineUnstuck(-227.2784, -3990.931, 131.2193)',
		'GMR.DefineUnstuck(-227.2588, -3993.661, 133.9031)',
		'GMR.DefineUnstuck(-227.1564, -3995.443, 135.7632)',
		'GMR.DefineUnstuck(-226.8765, -3997.551, 137.8601)',
		'GMR.DefineUnstuck(-226.0343, -4000.008, 140.1272)',
		'GMR.DefineUnstuck(-224.2299, -4003.055, 143.0167)',
		'GMR.DefineUnstuck(-222.9531, -4005.195, 144.5454)',
		'GMR.DefineUnstuck(-221.6833, -4007.323, 145.8429)',
		'GMR.DefineUnstuck(-220.7245, -4009.599, 146.7871)',
		'GMR.DefineUnstuck(-219.8593, -4013.171, 147.6718)',
		'GMR.DefineUnstuck(-219.6948, -4016.611, 148.7655)',
		'GMR.DefineUnstuck(-219.9563, -4020.261, 150.4539)',
		'GMR.DefineUnstuck(-220.7193, -4022.986, 151.8957)',
		'GMR.DefineUnstuck(-222.3295, -4024.552, 153.2682)',
		'GMR.DefineUnstuck(-225.4425, -4027.475, 155.1708)',
		'GMR.DefineUnstuck(-228.1926, -4030.861, 156.2314)',
		'GMR.DefineUnstuck(-229.4882, -4033.12, 157.2626)',
		'GMR.DefineUnstuck(-232.1257, -4037.305, 157.3341)',
		'GMR.DefineUnstuck(-235.949, -4038.966, 158.5497)',
		'GMR.DefineUnstuck(-238.4073, -4038.621, 159.6674)',
		'GMR.DefineUnstuck(-243.4568, -4037.35, 162.7227)',
		'GMR.DefineUnstuck(-246.7788, -4036.469, 165.6315)',
		'GMR.DefineUnstuck(-250.2322, -4035.651, 168.8637)',
		'GMR.DefineUnstuck(-253.5818, -4034.92, 171.7941)',
		'GMR.DefineUnstuck(-257.0383, -4034.116, 172.7241)',
		'GMR.DefineUnstuck(-260.3021, -4033.049, 172.6628)',
		'GMR.DefineUnstuck(-263.4335, -4031.657, 171.65)',
		'GMR.DefineUnstuck(-264.4315, -4027.819, 170.6964)',
		'GMR.DefineUnstuck(-264.8466, -4023.936, 170.0499)',
		'GMR.DefineUnstuck(-265.0885, -4020.985, 169.8793)',
		'GMR.DefineUnstuck(-265.3019, -4018.382, 169.6561)',
		'GMR.DefineUnstuck(-265.5061, -4015.892, 169.562)',
		'GMR.DefineUnstuck(-265.8044, -4012.362, 169.5828)',
		'GMR.DefineUnstuck(-266.1094, -4008.939, 169.2027)',
		'GMR.DefineUnstuck(-266.5756, -4005.411, 169.2965)',
		'GMR.DefineUnstuck(-267.1466, -4002.643, 169.2112)',
		'GMR.DefineUnstuck(-269.5026, -4001.055, 168.4183)',
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

GMR.DefineQuest("Tauren", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Call of Fire (PART 3) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1525, -269.279296875, -4001.587890625, 168.54284667969, 5900 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

GMR.DefineQuest("Tauren", "SHAMAN", 1525, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 3 |r", "Custom", -269.279296875, -4001.587890625, 168.54284667969, 5900, -269.279296875, -4001.587890625, 168.54284667969, 5900, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1525, 1) then
			GMR.SetQuestingState(nil)
			if not GMR.IsPlayerPosition(-102.434914, -2901.146240, 91.667473, 60) then 
				GMR.MeshTo(-102.434914, -2901.146240, 91.667473)
			else
				local npc1 = GMR.GetObjectWithInfo({ id = 3268, rawType = 5 })
				if npc1 then 
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		elseif not GMR.Questing.IsObjectiveCompleted(1525, 2) then
			GMR.SetQuestingState(nil)
			if not GMR.IsPlayerPosition(929.479309, -4738.714844, 21.402946, 60) then 
				GMR.MeshTo(929.479309, -4738.714844, 21.402946)
			else
				local npc1 = GMR.GetObjectWithInfo({ id = 3199, rawType = 5 })
				if npc1 then 
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				else
					GMR.SetQuestingState("Idle")
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(true)',
		'GMR.CreateTableEntry("Unstuck")',
		-- TOP of the Path (going to bottom)
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072, 80)',
		'GMR.DefineUnstuck(-265.9953, -4003.01, 169.6392)',
		'GMR.DefineUnstuck(-265.5731, -4007.286, 169.5224)',
		'GMR.DefineUnstuck(-265.0865, -4013.539, 169.8328)',
		'GMR.DefineUnstuck(-264.5121, -4020.509, 170.1297)',
		'GMR.DefineUnstuck(-263.6688, -4027.438, 171.0426)',
		'GMR.DefineUnstuck(-261.334, -4031.631, 172.5168)',
		'GMR.DefineUnstuck(-256.1549, -4035.319, 172.6828)',
		'GMR.DefineUnstuck(-252.6155, -4035.652, 170.9231)',
		'GMR.DefineUnstuck(-249.0734, -4035.963, 167.8442)',
		'GMR.DefineUnstuck(-245.6725, -4036.351, 164.5108)',
		'GMR.DefineUnstuck(-242.1395, -4036.754, 161.7505)',
		'GMR.DefineUnstuck(-238.7245, -4037.143, 159.9008)',
		'GMR.DefineUnstuck(-235.1842, -4037.477, 158.1641)',
		'GMR.DefineUnstuck(-231.5566, -4036.843, 157.3718)',
		'GMR.DefineUnstuck(-227.9, -4035.318, 157.1902)',
		'GMR.DefineUnstuck(-226.322, -4031.785, 156.1835)',
		'GMR.DefineUnstuck(-226.6904, -4028.975, 155.5541)',
		'GMR.DefineUnstuck(-225.3974, -4026.745, 155.0093)',
		'GMR.DefineUnstuck(-222.8908, -4024.383, 153.5098)',
		'GMR.DefineUnstuck(-220.3977, -4022.028, 151.4057)',
		'GMR.DefineUnstuck(-219.067, -4019.719, 149.6674)',
		'GMR.DefineUnstuck(-219.1835, -4016.262, 148.3217)',
		'GMR.DefineUnstuck(-220.2606, -4012.618, 147.7169)',
		'GMR.DefineUnstuck(-222.407, -4007.997, 146.4935)',
		'GMR.DefineUnstuck(-223.8766, -4004.003, 143.7523)',
		'GMR.DefineUnstuck(-225.2423, -4000.472, 140.6115)',
		'GMR.DefineUnstuck(-226.108, -3998.383, 138.6592)',
		'GMR.DefineUnstuck(-226.8772, -3996.527, 136.8776)',
		'GMR.DefineUnstuck(-227.6134, -3992.745, 132.9397)',
		'GMR.DefineUnstuck(-227.5912, -3990.134, 130.2498)',
		'GMR.DefineUnstuck(-227.4608, -3986.825, 126.9018)',
		'GMR.DefineUnstuck(-227.3341, -3984.224, 124.7492)',
		'GMR.DefineUnstuck(-227.2997, -3981.727, 122.6494)',
		'GMR.DefineUnstuck(-227.4868, -3979.49, 121.0448)',
		'GMR.DefineUnstuck(-228.0026, -3975.611, 119.4147)',
		'GMR.DefineUnstuck(-228.9861, -3972.956, 118.1752)',
		'GMR.DefineUnstuck(-231.309, -3970.595, 116.6192)',
		'GMR.DefineUnstuck(-233.9447, -3969.288, 116.5102)',
		'GMR.DefineUnstuck(-237.684, -3968.111, 117.4302)',
		'GMR.DefineUnstuck(-240.8389, -3967.107, 117.4534)',
		'GMR.DefineUnstuck(-243.8003, -3965.232, 115.9896)',
		'GMR.DefineUnstuck(-244.49, -3962.244, 114.589)',
		'GMR.DefineUnstuck(-244.8329, -3959.313, 112.8767)',
		'GMR.DefineUnstuck(-244.6241, -3957.085, 111.7018)',
		'GMR.DefineUnstuck(-244.376, -3951.407, 111.0384)',
		'GMR.DefineUnstuck(-244.1539, -3947.621, 110.5382)',
		'GMR.DefineUnstuck(-243.8453, -3945.028, 110.8477)',
		'GMR.DefineUnstuck(-242.4717, -3942.405, 109.7947)',
		'GMR.DefineUnstuck(-241.5451, -3939.616, 108.8344)',
		'GMR.DefineUnstuck(-240.7273, -3934.961, 106.8403)',
		'GMR.DefineUnstuck(-240.4573, -3932.371, 105.8352)',
		'GMR.DefineUnstuck(-240.3791, -3928.824, 104.7793)',
		'GMR.DefineUnstuck(-240.487, -3925.635, 103.1086)',
		'GMR.DefineUnstuck(-240.5185, -3922.912, 102.9576)',
		'GMR.DefineUnstuck(-240.5596, -3919.349, 103.0835)',
		'GMR.DefineUnstuck(-240.7238, -3915.923, 102.5849)',
		'GMR.DefineUnstuck(-240.6834, -3915.648, 102.56)',
		'GMR.DefineUnstuck(-242.3824, -3913.511, 103.37)',
		'GMR.DefineUnstuck(-243.865, -3911.823, 104.7807)',
		'GMR.DefineUnstuck(-245.6009, -3910.217, 106.0888)',
		'GMR.DefineUnstuck(-248.3885, -3908.029, 106.9726)',
		'GMR.DefineUnstuck(-251.8651, -3907.529, 107.3115)',
		'GMR.DefineUnstuck(-255.1949, -3908.342, 107.0193)',
		'GMR.DefineUnstuck(-258.5079, -3909.612, 106.6515)',
		'GMR.DefineUnstuck(-261.7232, -3910.822, 104.9466)',
		'GMR.DefineUnstuck(-265.1378, -3911.815, 102.8035)',
		'GMR.DefineUnstuck(-268.6282, -3912.478, 100.1142)',
		'GMR.DefineUnstuck(-272.0431, -3912.456, 97.57755)',
		'GMR.DefineUnstuck(-274.1382, -3911.253, 95.96122)',
		'GMR.DefineUnstuck(-273.5148, -3907.532, 94.47307)',
		'GMR.DefineUnstuck(-272.0321, -3904.424, 93.64937)',
		'GMR.DefineUnstuck(-270.6601, -3901.686, 93.12472)',
		'GMR.DefineUnstuck(-268.4762, -3899.883, 92.64568)',
		'GMR.DefineUnstuck(-264.9979, -3898.101, 90.40711)',
		'GMR.DefineUnstuck(-262.8027, -3897.241, 88.21679)',
		'GMR.DefineUnstuck(-259.9692, -3897.358, 85.52032)',
		'GMR.DefineUnstuck(-257.8491, -3897.611, 84.00722)',
		'GMR.DefineUnstuck(-255.49, -3897.787, 82.16405)',
		'GMR.DefineUnstuck(-252.8962, -3897.917, 81.03374)',
		'GMR.DefineUnstuck(-250.2877, -3898.031, 80.52705)',
		'GMR.DefineUnstuck(-247.5743, -3898.148, 81.06264)',
		'GMR.DefineUnstuck(-244.735, -3898.272, 81.36361)',
		'GMR.DefineUnstuck(-241.539, -3898.412, 80.69857)',
		'GMR.DefineUnstuck(-238.384, -3899.797, 78.9509)',
		'GMR.DefineUnstuck(-236.525, -3901.272, 77.32093)',
		'GMR.DefineUnstuck(-234.5782, -3902.816, 75.12527)',
		'GMR.DefineUnstuck(-232.5383, -3904.435, 72.6614)',
		'GMR.DefineUnstuck(-230.9534, -3905.692, 70.92281)',
		'GMR.DefineUnstuck(-227.9804, -3908.049, 67.98875)',
		'GMR.DefineUnstuck(-224.6707, -3908.18, 65.50371)',
		'GMR.DefineUnstuck(-222.4453, -3907.357, 64.44076)',
		'GMR.DefineUnstuck(-219.3503, -3905.622, 61.72633)',
		'GMR.DefineUnstuck(-216.3304, -3903.984, 58.855)',
		'GMR.DefineUnstuck(-213.4617, -3901.941, 56.57981)',
		'GMR.DefineUnstuck(-210.9052, -3899.633, 54.56749)',
		'GMR.DefineUnstuck(-208.201, -3897.337, 52.57686)',
		'GMR.DefineUnstuck(-205.4781, -3895.252, 50.61634)',
		'GMR.DefineUnstuck(-202.4935, -3893.324, 48.36252)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',
		'GMR.DefineProfileCenter(-102.434914, -2901.146240, 91.667473, 120)',
		'GMR.DefineProfileCenter(929.479309, -4738.714844, 21.402946, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',
		'GMR.DefineQuestEnemyId(3268)',
		'GMR.DefineQuestEnemyId(3199)'
	}
)

GMR.DefineQuest("Tauren", "SHAMAN", nil, "|cFFE25FFF TURNIN |r |cFFFFAB48 Call of Fire - Part 3 |r", "MassTurnIn", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1525, -269.279296875, -4001.587890625, 168.54284667969, 5900 },
	},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.AllowSpeedUp()',
		'GMR.SetChecked("Mount", true)',

		-- Bottom of the Hill
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(-199.4623, -3894.181, 47.61382, 100)',
		'GMR.DefineUnstuck(-202.7007, -3895.856, 49.94935)',
		'GMR.DefineUnstuck(-207.5063, -3898.153, 53.00512)',
		'GMR.DefineUnstuck(-211.1808, -3899.976, 54.83481)',
		'GMR.DefineUnstuck(-217.0187, -3902.872, 58.64489)',
		'GMR.DefineUnstuck(-220.119, -3904.585, 61.49921)',
		'GMR.DefineUnstuck(-223.0736, -3906.341, 63.95193)',
		'GMR.DefineUnstuck(-226.1475, -3908.105, 66.56731)',
		'GMR.DefineUnstuck(-230.0374, -3907.042, 69.88348)',
		'GMR.DefineUnstuck(-233.9443, -3903.606, 74.20333)',
		'GMR.DefineUnstuck(-235.9505, -3901.594, 76.76254)',
		'GMR.DefineUnstuck(-240.0965, -3898.51, 80.06519)',
		'GMR.DefineUnstuck(-241.8339, -3897.622, 80.91915)',
		'GMR.DefineUnstuck(-245.7246, -3897.939, 81.23082)',
		'GMR.DefineUnstuck(-249.7455, -3898.227, 80.58408)',
		'GMR.DefineUnstuck(-253.1858, -3898.357, 81.33885)',
		'GMR.DefineUnstuck(-255.7887, -3898.384, 82.62493)',
		'GMR.DefineUnstuck(-260.0606, -3898.454, 86.03916)',
		'GMR.DefineUnstuck(-262.191, -3898.672, 88.32788)',
		'GMR.DefineUnstuck(-264.7761, -3899.039, 90.69366)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-269.8214, -3903.371, 93.71248)',
		'GMR.DefineUnstuck(-271.9857, -3906.041, 94.44247)',
		'GMR.DefineUnstuck(-274.2279, -3909.254, 94.85416)',
		'GMR.DefineUnstuck(-272.4892, -3910.983, 96.57925)',
		'GMR.DefineUnstuck(-269.3623, -3912.675, 99.5118)',
		'GMR.DefineUnstuck(-267.3385, -3913.77, 101.4796)',
		'GMR.DefineUnstuck(-265.0544, -3912.314, 102.8668)',
		'GMR.DefineUnstuck(-262.3954, -3911.038, 104.5286)',
		'GMR.DefineUnstuck(-260.1604, -3910.277, 105.8743)',
		'GMR.DefineUnstuck(-257.3842, -3909.249, 106.9683)',
		'GMR.DefineUnstuck(-254.3581, -3908.606, 107.1753)',
		'GMR.DefineUnstuck(-251.652, -3908.49, 107.2679)',
		'GMR.DefineUnstuck(-248.1356, -3908.915, 106.9351)',
		'GMR.DefineUnstuck(-245.5722, -3909.724, 106.1492)',
		'GMR.DefineUnstuck(-243.6041, -3912.237, 104.4027)',
		'GMR.DefineUnstuck(-242.3808, -3914.41, 103.3386)',
		'GMR.DefineUnstuck(-240.897, -3916.677, 102.6752)',
		'GMR.DefineUnstuck(-239.5852, -3920.414, 102.794)',
		'GMR.DefineUnstuck(-239.3499, -3923.955, 102.3959)',
		'GMR.DefineUnstuck(-239.368, -3927.391, 103.7777)',
		'GMR.DefineUnstuck(-239.2873, -3930.225, 105.0105)',
		'GMR.DefineUnstuck(-239.2332, -3935.089, 106.6324)',
		'GMR.DefineUnstuck(-239.1963, -3938.526, 108.1947)',
		'GMR.DefineUnstuck(-239.3192, -3941.252, 109.3255)',
		'GMR.DefineUnstuck(-240.7941, -3944.482, 110.1133)',
		'GMR.DefineUnstuck(-241.6382, -3946.946, 110.1196)',
		'GMR.DefineUnstuck(-242.7522, -3950.197, 110.3971)',
		'GMR.DefineUnstuck(-243.8751, -3953.563, 110.8127)',
		'GMR.DefineUnstuck(-244.5518, -3956.926, 111.6394)',
		'GMR.DefineUnstuck(-244.1009, -3961.622, 114.141)',
		'GMR.DefineUnstuck(-243.7141, -3964.791, 115.6663)',
		'GMR.DefineUnstuck(-243.0102, -3967.404, 117.6225)',
		'GMR.DefineUnstuck(-240.3202, -3968.368, 117.7073)',
		'GMR.DefineUnstuck(-237.2673, -3968.719, 117.5444)',
		'GMR.DefineUnstuck(-233.834, -3968.985, 116.3506)',
		'GMR.DefineUnstuck(-231.4871, -3970.2, 116.3824)',
		'GMR.DefineUnstuck(-229.748, -3971.973, 117.6124)',
		'GMR.DefineUnstuck(-228.0222, -3974.524, 118.8738)',
		'GMR.DefineUnstuck(-227.0834, -3978.285, 120.0969)',
		'GMR.DefineUnstuck(-227.0988, -3980.87, 121.8627)',
		'GMR.DefineUnstuck(-227.1036, -3983.955, 124.4939)',
		'GMR.DefineUnstuck(-227.1822, -3987.496, 127.6713)',
		'GMR.DefineUnstuck(-227.2784, -3990.931, 131.2193)',
		'GMR.DefineUnstuck(-227.2588, -3993.661, 133.9031)',
		'GMR.DefineUnstuck(-227.1564, -3995.443, 135.7632)',
		'GMR.DefineUnstuck(-226.8765, -3997.551, 137.8601)',
		'GMR.DefineUnstuck(-226.0343, -4000.008, 140.1272)',
		'GMR.DefineUnstuck(-224.2299, -4003.055, 143.0167)',
		'GMR.DefineUnstuck(-222.9531, -4005.195, 144.5454)',
		'GMR.DefineUnstuck(-221.6833, -4007.323, 145.8429)',
		'GMR.DefineUnstuck(-220.7245, -4009.599, 146.7871)',
		'GMR.DefineUnstuck(-219.8593, -4013.171, 147.6718)',
		'GMR.DefineUnstuck(-219.6948, -4016.611, 148.7655)',
		'GMR.DefineUnstuck(-219.9563, -4020.261, 150.4539)',
		'GMR.DefineUnstuck(-220.7193, -4022.986, 151.8957)',
		'GMR.DefineUnstuck(-222.3295, -4024.552, 153.2682)',
		'GMR.DefineUnstuck(-225.4425, -4027.475, 155.1708)',
		'GMR.DefineUnstuck(-228.1926, -4030.861, 156.2314)',
		'GMR.DefineUnstuck(-229.4882, -4033.12, 157.2626)',
		'GMR.DefineUnstuck(-232.1257, -4037.305, 157.3341)',
		'GMR.DefineUnstuck(-235.949, -4038.966, 158.5497)',
		'GMR.DefineUnstuck(-238.4073, -4038.621, 159.6674)',
		'GMR.DefineUnstuck(-243.4568, -4037.35, 162.7227)',
		'GMR.DefineUnstuck(-246.7788, -4036.469, 165.6315)',
		'GMR.DefineUnstuck(-250.2322, -4035.651, 168.8637)',
		'GMR.DefineUnstuck(-253.5818, -4034.92, 171.7941)',
		'GMR.DefineUnstuck(-257.0383, -4034.116, 172.7241)',
		'GMR.DefineUnstuck(-260.3021, -4033.049, 172.6628)',
		'GMR.DefineUnstuck(-263.4335, -4031.657, 171.65)',
		'GMR.DefineUnstuck(-264.4315, -4027.819, 170.6964)',
		'GMR.DefineUnstuck(-264.8466, -4023.936, 170.0499)',
		'GMR.DefineUnstuck(-265.0885, -4020.985, 169.8793)',
		'GMR.DefineUnstuck(-265.3019, -4018.382, 169.6561)',
		'GMR.DefineUnstuck(-265.5061, -4015.892, 169.562)',
		'GMR.DefineUnstuck(-265.8044, -4012.362, 169.5828)',
		'GMR.DefineUnstuck(-266.1094, -4008.939, 169.2027)',
		'GMR.DefineUnstuck(-266.5756, -4005.411, 169.2965)',
		'GMR.DefineUnstuck(-267.1466, -4002.643, 169.2112)',
		'GMR.DefineUnstuck(-269.5026, -4001.055, 168.4183)',
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)


GMR.DefineQuest("Tauren", "SHAMAN", 1526, "|cFFFFAB48 Rite of Vision part 4 |r", "Custom", -269.279296875, -4001.587890625, 168.54284667969, 5900, -243.74391174316, -4022.4516601563, 188.00898742676, 61934, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1526, 1) then
			GMR.SetQuestingState(nil);
			local npc1 = GMR.GetObjectWithInfo({ id = 5893, rawType = 5 })
			local fireSaptaBuff = GetSpellInfo(8898)
			if not npc1 and not GMR.HasBuff("player", fireSaptaBuff) then
				if not GMR.IsPlayerPosition(-257.436981, -3978.607178, 168.321869, 2) then 
					GMR.MeshTo(-257.436981, -3978.607178, 168.321869)
				else 
					local itemName = GetItemInfo(6636)
					GMR.Use(itemName)
					GMR.RunMacroText("/use Fire Sapta")
				end
			elseif npc1 or GMR.HasBuff("player, fireSaptaBuff) then
				if not GMR.IsPlayerPosition(-246.257996, -4009.979980, 187.292999, 6) then 
					GMR.MeshTo(-246.257996, -4009.979980, 187.292999)
				else
					GMR.TargetUnit(npc1)
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-246.257996, -4009.979980, 187.292999, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'GMR.DefineQuestEnemyId(5893)'
	}
)

GMR.DefineQuest("Tauren", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Call of Fire (PART 5) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1527, -243.74391174316, -4022.4516601563, 188.00898742676, 61934 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)


GMR.DefineQuest("Tauren", "SHAMAN", 1527, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 5 |r", "TalkTo", -243.74391174316, -4022.4516601563, 188.00898742676, 61934, 268.898987, -3055.149902, 97.164200, 5907, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- From Brazier to Quest NPC
		'GMR.DefineUnstuck(-243.4819, -4020.668, 187.3037, 80)',
		'GMR.DefineUnstuck(-244.7954, -4016.023, 187.3037)',
		'GMR.DefineUnstuck(-246.2919, -4009.803, 187.2939)',
		'GMR.DefineUnstuck(-248.2219, -4001.873, 184.6087)',
		'GMR.DefineUnstuck(-251.9074, -3991.917, 176.1363)',
		'GMR.DefineUnstuck(-255.5767, -3984.81, 169.9798)',
		'GMR.DefineUnstuck(-259.1981, -3986.567, 169.9047)',
		'GMR.DefineUnstuck(-262.6681, -3993.027, 169.5461)',
		'GMR.DefineUnstuck(-265.9148, -3998.033, 169.0287)',
		'GMR.DefineUnstuck(-268.999, -4000.609, 168.5203)',
		
		-- TOP of the Path (going to bottom)
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072, 80)',
		'GMR.DefineUnstuck(-265.9953, -4003.01, 169.6392)',
		'GMR.DefineUnstuck(-265.5731, -4007.286, 169.5224)',
		'GMR.DefineUnstuck(-265.0865, -4013.539, 169.8328)',
		'GMR.DefineUnstuck(-264.5121, -4020.509, 170.1297)',
		'GMR.DefineUnstuck(-263.6688, -4027.438, 171.0426)',
		'GMR.DefineUnstuck(-261.334, -4031.631, 172.5168)',
		'GMR.DefineUnstuck(-256.1549, -4035.319, 172.6828)',
		'GMR.DefineUnstuck(-252.6155, -4035.652, 170.9231)',
		'GMR.DefineUnstuck(-249.0734, -4035.963, 167.8442)',
		'GMR.DefineUnstuck(-245.6725, -4036.351, 164.5108)',
		'GMR.DefineUnstuck(-242.1395, -4036.754, 161.7505)',
		'GMR.DefineUnstuck(-238.7245, -4037.143, 159.9008)',
		'GMR.DefineUnstuck(-235.1842, -4037.477, 158.1641)',
		'GMR.DefineUnstuck(-231.5566, -4036.843, 157.3718)',
		'GMR.DefineUnstuck(-227.9, -4035.318, 157.1902)',
		'GMR.DefineUnstuck(-226.322, -4031.785, 156.1835)',
		'GMR.DefineUnstuck(-226.6904, -4028.975, 155.5541)',
		'GMR.DefineUnstuck(-225.3974, -4026.745, 155.0093)',
		'GMR.DefineUnstuck(-222.8908, -4024.383, 153.5098)',
		'GMR.DefineUnstuck(-220.3977, -4022.028, 151.4057)',
		'GMR.DefineUnstuck(-219.067, -4019.719, 149.6674)',
		'GMR.DefineUnstuck(-219.1835, -4016.262, 148.3217)',
		'GMR.DefineUnstuck(-220.2606, -4012.618, 147.7169)',
		'GMR.DefineUnstuck(-222.407, -4007.997, 146.4935)',
		'GMR.DefineUnstuck(-223.8766, -4004.003, 143.7523)',
		'GMR.DefineUnstuck(-225.2423, -4000.472, 140.6115)',
		'GMR.DefineUnstuck(-226.108, -3998.383, 138.6592)',
		'GMR.DefineUnstuck(-226.8772, -3996.527, 136.8776)',
		'GMR.DefineUnstuck(-227.6134, -3992.745, 132.9397)',
		'GMR.DefineUnstuck(-227.5912, -3990.134, 130.2498)',
		'GMR.DefineUnstuck(-227.4608, -3986.825, 126.9018)',
		'GMR.DefineUnstuck(-227.3341, -3984.224, 124.7492)',
		'GMR.DefineUnstuck(-227.2997, -3981.727, 122.6494)',
		'GMR.DefineUnstuck(-227.4868, -3979.49, 121.0448)',
		'GMR.DefineUnstuck(-228.0026, -3975.611, 119.4147)',
		'GMR.DefineUnstuck(-228.9861, -3972.956, 118.1752)',
		'GMR.DefineUnstuck(-231.309, -3970.595, 116.6192)',
		'GMR.DefineUnstuck(-233.9447, -3969.288, 116.5102)',
		'GMR.DefineUnstuck(-237.684, -3968.111, 117.4302)',
		'GMR.DefineUnstuck(-240.8389, -3967.107, 117.4534)',
		'GMR.DefineUnstuck(-243.8003, -3965.232, 115.9896)',
		'GMR.DefineUnstuck(-244.49, -3962.244, 114.589)',
		'GMR.DefineUnstuck(-244.8329, -3959.313, 112.8767)',
		'GMR.DefineUnstuck(-244.6241, -3957.085, 111.7018)',
		'GMR.DefineUnstuck(-244.376, -3951.407, 111.0384)',
		'GMR.DefineUnstuck(-244.1539, -3947.621, 110.5382)',
		'GMR.DefineUnstuck(-243.8453, -3945.028, 110.8477)',
		'GMR.DefineUnstuck(-242.4717, -3942.405, 109.7947)',
		'GMR.DefineUnstuck(-241.5451, -3939.616, 108.8344)',
		'GMR.DefineUnstuck(-240.7273, -3934.961, 106.8403)',
		'GMR.DefineUnstuck(-240.4573, -3932.371, 105.8352)',
		'GMR.DefineUnstuck(-240.3791, -3928.824, 104.7793)',
		'GMR.DefineUnstuck(-240.487, -3925.635, 103.1086)',
		'GMR.DefineUnstuck(-240.5185, -3922.912, 102.9576)',
		'GMR.DefineUnstuck(-240.5596, -3919.349, 103.0835)',
		'GMR.DefineUnstuck(-240.7238, -3915.923, 102.5849)',
		'GMR.DefineUnstuck(-240.6834, -3915.648, 102.56)',
		'GMR.DefineUnstuck(-242.3824, -3913.511, 103.37)',
		'GMR.DefineUnstuck(-243.865, -3911.823, 104.7807)',
		'GMR.DefineUnstuck(-245.6009, -3910.217, 106.0888)',
		'GMR.DefineUnstuck(-248.3885, -3908.029, 106.9726)',
		'GMR.DefineUnstuck(-251.8651, -3907.529, 107.3115)',
		'GMR.DefineUnstuck(-255.1949, -3908.342, 107.0193)',
		'GMR.DefineUnstuck(-258.5079, -3909.612, 106.6515)',
		'GMR.DefineUnstuck(-261.7232, -3910.822, 104.9466)',
		'GMR.DefineUnstuck(-265.1378, -3911.815, 102.8035)',
		'GMR.DefineUnstuck(-268.6282, -3912.478, 100.1142)',
		'GMR.DefineUnstuck(-272.0431, -3912.456, 97.57755)',
		'GMR.DefineUnstuck(-274.1382, -3911.253, 95.96122)',
		'GMR.DefineUnstuck(-273.5148, -3907.532, 94.47307)',
		'GMR.DefineUnstuck(-272.0321, -3904.424, 93.64937)',
		'GMR.DefineUnstuck(-270.6601, -3901.686, 93.12472)',
		'GMR.DefineUnstuck(-268.4762, -3899.883, 92.64568)',
		'GMR.DefineUnstuck(-264.9979, -3898.101, 90.40711)',
		'GMR.DefineUnstuck(-262.8027, -3897.241, 88.21679)',
		'GMR.DefineUnstuck(-259.9692, -3897.358, 85.52032)',
		'GMR.DefineUnstuck(-257.8491, -3897.611, 84.00722)',
		'GMR.DefineUnstuck(-255.49, -3897.787, 82.16405)',
		'GMR.DefineUnstuck(-252.8962, -3897.917, 81.03374)',
		'GMR.DefineUnstuck(-250.2877, -3898.031, 80.52705)',
		'GMR.DefineUnstuck(-247.5743, -3898.148, 81.06264)',
		'GMR.DefineUnstuck(-244.735, -3898.272, 81.36361)',
		'GMR.DefineUnstuck(-241.539, -3898.412, 80.69857)',
		'GMR.DefineUnstuck(-238.384, -3899.797, 78.9509)',
		'GMR.DefineUnstuck(-236.525, -3901.272, 77.32093)',
		'GMR.DefineUnstuck(-234.5782, -3902.816, 75.12527)',
		'GMR.DefineUnstuck(-232.5383, -3904.435, 72.6614)',
		'GMR.DefineUnstuck(-230.9534, -3905.692, 70.92281)',
		'GMR.DefineUnstuck(-227.9804, -3908.049, 67.98875)',
		'GMR.DefineUnstuck(-224.6707, -3908.18, 65.50371)',
		'GMR.DefineUnstuck(-222.4453, -3907.357, 64.44076)',
		'GMR.DefineUnstuck(-219.3503, -3905.622, 61.72633)',
		'GMR.DefineUnstuck(-216.3304, -3903.984, 58.855)',
		'GMR.DefineUnstuck(-213.4617, -3901.941, 56.57981)',
		'GMR.DefineUnstuck(-210.9052, -3899.633, 54.56749)',
		'GMR.DefineUnstuck(-208.201, -3897.337, 52.57686)',
		'GMR.DefineUnstuck(-205.4781, -3895.252, 50.61634)',
		'GMR.DefineUnstuck(-202.4935, -3893.324, 48.36252)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',

		-- Bottom of Hill to Turnin NPC
		'GMR.DefineUnstuck(-198.136, -3893.104, 46.45814, 30)',
		'GMR.DefineUnstuck(-190.5721, -3888.852, 42.55983)',
		'GMR.DefineUnstuck(-180.0152, -3885.5, 41.1428)',
		'GMR.DefineUnstuck(-170.5749, -3881.455, 40.03998)',
		'GMR.DefineUnstuck(-161.4828, -3876.547, 38.74943)',
		'GMR.DefineUnstuck(-152.1162, -3871.485, 37.55161)',
		'GMR.DefineUnstuck(-143.1477, -3865.721, 36.54287)',
		'GMR.DefineUnstuck(-134.6426, -3859.892, 35.58938)',
		'GMR.DefineUnstuck(-124.9326, -3852.195, 34.12705)',
		'GMR.DefineUnstuck(-117.3372, -3845.222, 32.85403)',
		'GMR.DefineUnstuck(-113.3822, -3828.864, 28.31171)',
		'GMR.DefineUnstuck(-114.7541, -3818.285, 24.51085)',
		'GMR.DefineUnstuck(-115.7247, -3808.003, 20.86743)',
		'GMR.DefineUnstuck(-116.6059, -3797.397, 17.39478)',
		'GMR.DefineUnstuck(-114.9183, -3780.563, 16.34175)',
		'GMR.DefineUnstuck(-113.1018, -3768.708, 16.34175)',
		'GMR.DefineUnstuck(-109.7734, -3753.923, 16.34176)',
		'GMR.DefineUnstuck(-108.132, -3744.38, 17.83697)',
		'GMR.DefineUnstuck(-106.4039, -3735.667, 19.86492)',
		'GMR.DefineUnstuck(-104.106, -3725.25, 22.66694)',
		'GMR.DefineUnstuck(-101.8878, -3715.201, 25.47434)',
		'GMR.DefineUnstuck(-96.14438, -3706.347, 26.78756)',
		'GMR.DefineUnstuck(-87.32219, -3701.694, 27.12251)',
		'GMR.DefineUnstuck(-77.05066, -3698.885, 28.08901)',
		'GMR.DefineUnstuck(-66.76051, -3696.152, 28.8954)',
		'GMR.DefineUnstuck(-56.7951, -3693.505, 28.0468)',
		'GMR.DefineUnstuck(-46.50495, -3690.772, 27.19295)',
		'GMR.DefineUnstuck(-36.53954, -3688.125, 27.18301)',
		'GMR.DefineUnstuck(-26.24939, -3685.391, 27.18367)',
		'GMR.DefineUnstuck(-16.24339, -3682.733, 27.18367)',
		'GMR.DefineUnstuck(-5.97935, -3680.007, 27.42334)',
		'GMR.DefineUnstuck(3.270565, -3674.883, 29.1152)',
		'GMR.DefineUnstuck(11.34515, -3668.539, 29.80562)',
		'GMR.DefineUnstuck(19.06043, -3661.733, 29.93561)',
		'GMR.DefineUnstuck(26.34841, -3653.923, 30.00878)',
		'GMR.DefineUnstuck(30.84596, -3644.323, 29.61634)',
		'GMR.DefineUnstuck(31.85866, -3634.2, 28.69127)',
		'GMR.DefineUnstuck(33.75569, -3623.93, 28.06054)',
		'GMR.DefineUnstuck(39.57153, -3615.391, 28.17637)',
		'GMR.DefineUnstuck(45.60079, -3606.66, 28.37199)',
		'GMR.DefineUnstuck(51.47168, -3598.158, 28.61169)',
		'GMR.DefineUnstuck(57.50439, -3589.422, 28.95725)',
		'GMR.DefineUnstuck(63.38721, -3580.903, 29.55947)',
		'GMR.DefineUnstuck(69.41915, -3572.167, 29.8638)',
		'GMR.DefineUnstuck(75.27811, -3563.683, 29.50505)',
		'GMR.DefineUnstuck(81.33992, -3554.904, 29.13523)',
		'GMR.DefineUnstuck(87.37787, -3546.161, 29.48779)',
		'GMR.DefineUnstuck(93.23682, -3537.676, 30.05953)',
		'GMR.DefineUnstuck(99.30486, -3528.889, 30.41961)',
		'GMR.DefineUnstuck(105.1519, -3520.421, 30.60289)',
		'GMR.DefineUnstuck(111.1898, -3511.677, 30.6422)',
		'GMR.DefineUnstuck(116.8692, -3503.061, 30.4528)',
		'GMR.DefineUnstuck(121.5584, -3493.456, 30.38526)',
		'GMR.DefineUnstuck(126.2168, -3483.905, 30.85957)',
		'GMR.DefineUnstuck(130.9491, -3474.787, 30.96708)',
		'GMR.DefineUnstuck(138.6461, -3467.435, 29.64007)',
		'GMR.DefineUnstuck(146.1448, -3460.357, 29.56522)',
		'GMR.DefineUnstuck(153.5036, -3452.716, 29.32058)',
		'GMR.DefineUnstuck(159.1022, -3444.058, 28.71848)',
		'GMR.DefineUnstuck(164.7224, -3435.001, 28.1831)',
		'GMR.DefineUnstuck(167.2484, -3425, 28.61661)',
		'GMR.DefineUnstuck(169.0656, -3414.573, 30.57157)',
		'GMR.DefineUnstuck(170.8973, -3404.064, 32.98787)',
		'GMR.DefineUnstuck(172.664, -3393.927, 35.33115)',
		'GMR.DefineUnstuck(174.4957, -3383.417, 37.76035)',
		'GMR.DefineUnstuck(176.266, -3373.259, 40.10846)',
		'GMR.DefineUnstuck(178.0516, -3362.764, 42.53461)',
		'GMR.DefineUnstuck(178.9112, -3352.51, 44.90494)',
		'GMR.DefineUnstuck(178.5684, -3341.862, 47.36655)',
		'GMR.DefineUnstuck(177.813, -3331.578, 49.74374)',
		'GMR.DefineUnstuck(177.0331, -3320.96, 52.19994)',
		'GMR.DefineUnstuck(176.2777, -3310.677, 54.61379)',
		'GMR.DefineUnstuck(175.4977, -3300.058, 57.28224)',
		'GMR.DefineUnstuck(174.7147, -3289.398, 60.1383)',
		'GMR.DefineUnstuck(173.9609, -3279.136, 62.74594)',
		'GMR.DefineUnstuck(173.1809, -3268.517, 65.27595)',
		'GMR.DefineUnstuck(172.4255, -3258.234, 67.92759)',
		'GMR.DefineUnstuck(171.6456, -3247.615, 70.77227)',
		'GMR.DefineUnstuck(170.8617, -3237.314, 73.36811)',
		'GMR.DefineUnstuck(169.7871, -3226.722, 75.85859)',
		'GMR.DefineUnstuck(170.2659, -3216.378, 78.34002)',
		'GMR.DefineUnstuck(170.9778, -3205.797, 80.75212)',
		'GMR.DefineUnstuck(171.6925, -3195.174, 83.06865)',
		'GMR.DefineUnstuck(172.3832, -3184.907, 85.43134)',
		'GMR.DefineUnstuck(173.0979, -3174.284, 88.29417)',
		'GMR.DefineUnstuck(175.2186, -3164.302, 90.42546)',
		'GMR.DefineUnstuck(179.0815, -3154.369, 91.21728)',
		'GMR.DefineUnstuck(180.998, -3144.259, 91.4593)',
		'GMR.DefineUnstuck(182.981, -3133.799, 91.65737)',
		'GMR.DefineUnstuck(184.9672, -3123.321, 91.66991)',
		'GMR.DefineUnstuck(188.2947, -3113.675, 91.76788)',
		'GMR.DefineUnstuck(193.4834, -3104.402, 92.25401)',
		'GMR.DefineUnstuck(198.5183, -3095.404, 92.84583)',
		'GMR.DefineUnstuck(203.7274, -3086.094, 92.81425)',
		'GMR.DefineUnstuck(209.6564, -3077.781, 92.24898)',
		'GMR.DefineUnstuck(217.4222, -3070.485, 91.66679)',
		'GMR.DefineUnstuck(225.738, -3064.467, 91.97)',
		'GMR.DefineUnstuck(234.4507, -3058.356, 95.38133)',
		'GMR.DefineUnstuck(243.0827, -3052.088, 95.93181)',
		'GMR.DefineUnstuck(251.6662, -3046.414, 96.07415)',
		'GMR.DefineUnstuck(260.0364, -3041.026, 96.35554)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             END --- [ Shaman ][Level 14][Tauren] - Fire Totem
----------------------------------------------------------------------------------------------------------------------------------

-- END of TAUREN 1-14 (Unstuck should lead us to X-Roads)


























elseif UnitRace("player") == "Scourge" or UnitRace("player") == "Undead" then

----------------------------------------------------------------------------------------------------------------------------------
--  		             START: 1-14 UNDEAD Starting Area
----------------------------------------------------------------------------------------------------------------------------------




GMR.DefineQuest( "Scourge", nil, 363, "|cFFFFAB48 Rude Awakening |r", "TalkTo", 1678.9887695313, 1667.8616943359, 135.7717590332, 1568, 1843.3172607422, 1639.9049072266, 97.627830505371, 1569, 
	{},
	{
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.SetChecked("Mount", true)',
		'GMR.SetChecked("DisplayCenter", true)',
		'GMR.SetChecked("ClassTrainer", true)',
		'GMR.SetChecked("AutoGear", true)',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)'
	}
)

GMR.DefineQuest( "Scourge", nil, 364, "|cFFFFAB48 The Mindless Ones |r", "Custom", 1843.3172607422, 1639.9049072266, 97.627830505371, 1569, 1843.3172607422, 1639.9049072266, 97.627830505371, 1569, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(364, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1501, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(364, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1502, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(1923.8715821, 1592.966064, 84.640579,130)',
		'GMR.DefineProfileCenter(1921.6365966797, 1549.5435791016, 87.026985168457,130)',
		'GMR.DefineProfileCenter(1921.4151611328, 1530.0811767578, 86.939079284668,130)',
		'GMR.DefineProfileCenter(1894.7932128906, 1524.7971191406, 88.077018737793,130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.BlacklistId(1890)',
		'GMR.BlacklistId(1508)',
		'GMR.BlacklistId(1512)',
		'GMR.DefineQuestEnemyId(1501)',
		'GMR.DefineQuestEnemyId(1502)'
	}
)

GMR.DefineQuest( "Scourge", nil, 3901, "|cFFFFAB48 Rattling the Rattlecages |r", "Custom", 1843.3172607422, 1639.9049072266, 97.627830505371, 1569, 1843.3172607422, 1639.9049072266, 97.627830505371, 1569, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(3901, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineProfileCenter(2002.9536132813, 1595.1755371094, 78.17529296875,130)',
		'GMR.DefineProfileCenter(1980.5129394531, 1549.2677001953, 85.747444152832,130)',
		'GMR.DefineProfileCenter(1939.5101318359, 1500.5882568359, 85.721405029297,130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1890)'
	}
)

-- Class Quests - Scrolls
GMR.DefineQuest("Scourge", "PRIEST", 3097, "|cFFFFAB48 Hallowed Scroll |r", "TalkTo", 1843.3172607422, 1639.9049072266, 97.627914428711, 1569, 1848.32421875, 1627.6312255859, 96.933708190918, 2123, {}, 
	{
		'GMR.DefineProfileCenter(1848.32421875, 1627.6312255859, 96.933708190918,130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)'
	} 
)

-- Warlock
GMR.DefineQuest("Scourge", "WARLOCK", 3099, "|cFFFFAB48 Tainted Scroll |r", "TalkTo", 1843.3172607422, 1639.9049072266, 97.627914428711, 1569, 1839.030029, 1636.540039, 97.016899, 2126, {}, 
	{
		'GMR.DefineProfileCenter(1839.030029, 1636.540039, 97.016899,130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)'
	} 
)
-- Warrior
GMR.DefineQuest("Scourge", "WARRIOR", 3095, "|cFFFFAB48 Simple Scroll |r", "TalkTo", 1843.3172607422, 1639.9049072266, 97.627914428711, 1569, 1862.459961, 1556.329956, 94.878998, 2119, {}, 
	{
		'GMR.DefineProfileCenter(1862.459961, 1556.329956, 94.878998,130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)'
	} 
)
-- Mage
GMR.DefineQuest("Scourge", "MAGE", 3098, "|cFFFFAB48 Glyphic Scroll |r", "TalkTo", 1843.3172607422, 1639.9049072266, 97.627914428711, 1569, 1847.390015, 1635.520020, 97.016899, 2124, {}, 
	{
		'GMR.DefineProfileCenter(1847.390015, 1635.520020, 97.016899,130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)'
	} 
)

----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Warlock ][Level 4][Undead] - Summon Imp
----------------------------------------------------------------------------------------------------------------------------------
-- Piercing the Veil
GMR.DefineQuest("Scourge", "WARLOCK", 1470, "|cFFFFAB48 CryptoClassQuest: Piercing the Veil - Summon Imp WARLOCK |r", "Grinding", 1836.939941, 1633.430054, 97.016899, 5667, 1836.939941, 1633.430054, 97.016899, 5667, {},
	{
		'GMR.DefineProfileCenter(1982.681, 1569.13, 78.83903, 80)',
		'GMR.DefineProfileCenter(1977.977905, 1578.657837, 80.980698, 80)',
		'GMR.DefineProfileCenter(1911.692993, 1478.464111, 87.279045, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1890)',
	}
)
----------------------------------------------------------------------------------------------------------------------------------
--  		   END --- [ Warlock ][Level 4][Undead] - Summon Imp
----------------------------------------------------------------------------------------------------------------------------------



GMR.DefineQuest( "Scourge", nil, 376, "|cFFFFAB48 The Damned |r", "Custom", 1847.73046875,1638.6535644531,96.933502197266, 1661, 1847.73046875,1638.6535644531,96.933502197266, 1661, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(376, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1508, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(376, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1512, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(1909.068, 1693.087, 84.62663, 130)',
		'GMR.DefineProfileCenter(1778.957, 1713.792, 119.6821, 130)',
		'GMR.DefineProfileCenter(1969.375, 1673.141, 77.05115, 130)',
		'GMR.DefineProfileCenter(2065.624, 1641.047, 69.64196, 130)',
		'GMR.DefineProfileCenter(2126.827, 1589.101, 73.89836, 130)',
		'GMR.DefineProfileCenter(2069.074, 1517.246, 70.74171, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1508)',
		'GMR.DefineQuestEnemyId(1509)',
		'GMR.DefineQuestEnemyId(1513)',
		'GMR.DefineQuestEnemyId(1512)'
	}
)


GMR.DefineQuest( "Scourge", nil, 6395, "|cFFFFAB48 Marla's Last Wish |r", "Custom", 1847.73046875, 1638.6535644531, 96.933441162109, 1661, 1847.73046875, 1638.6535644531, 96.933441162109, 1661, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(6395, 1) then
			GMR.SetQuestingState(nil);
			if GetItemCount(16333) < 1 then
				if not GMR.IsPlayerPosition(1982.900024, 1376.010010, 63.051498, 3) then 
					GMR.MeshTo(1982.900024, 1376.010010, 63.051498)
				else 
					local npc = GMR.GetObjectWithInfo({ id = 1919, rawType = 5 })
					if npc then
						GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
					end
				end
			elseif GetItemCount(16333) >= 1 then
				if not GMR.IsPlayerPosition(1876.792, 1625.239, 95.33777, 3) then 
					GMR.MeshTo(1876.792, 1625.239, 95.33777)
				else 
					local object = GMR.GetObjectWithInfo({ id = 178090, rawType = 8 })
					if object then
						GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
					end
				end
			end
		end
	]]},
	{
		[[if not GMR.Frames.Rand001 then
			GMR.Print("Frame created")
			GMR.Frames.Rand001 = CreateFrame("frame")
			GMR.Frames.Rand001:SetScript("OnUpdate", function(self)
				if GMR.GetProfileType() == "Questing" then
					if GMR.IsExecuting() and GMR.IsQuestActive(6395) then
						if not GMR.Questing.IsObjectiveCompleted(6395, 1)
							if GetItemCount(16333) >= 1 then
								if not GMR.IsPlayerPosition(1876.792, 1625.239, 95.33777, 3) then 
									GMR.MeshTo(1876.792, 1625.239, 95.33777)
								else 
									local object = GMR.GetObjectWithInfo({ id = 178090, rawType = 8 })
									if object then
										GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
									end
								end
							end
						end
					end
				else 
					self:SetScript("OnUpdate", nil); GMR.Frames.Rand001 = nil; GMR.Print("Frame deleted")
				end
			end)
		end]],
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1982.900024, 1376.010010, 63.051498, 100)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1919)',
		'GMR_DefineCustomObjectID(178090)'
	}
)

GMR.DefineQuest( "Scourge", nil, 3902, "|cFFFFAB48 Scavenging Deathknell |r", "Custom", 1860.4732666016, 1607.4521484375, 95.235260009766, 1740, 1860.4732666016, 1607.4521484375, 95.235260009766, 1740, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(3902, 1) then
			GMR.SetQuestingState("Idle");
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1849.4272460938, 1516.7193603516, 88.079368591309, 50)',
		'GMR.DefineProfileCenter(1859.587890625, 1535.2698974609, 88.979919433594, 50)',
		'GMR.DefineProfileCenter(1868.5517578125, 1541.7933349609, 88.572654724121, 50)',
		'GMR.DefineProfileCenter(1897.4862060547, 1562.6854248047, 88.076942443848, 50)',
		'GMR.DefineProfileCenter(1954.5822753906, 1600.3431396484, 83.526527404785, 50)',
		'GMR.DefineProfileCenter(1971.0888671875, 1597.5864257813, 82.772567749023, 50)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(164662)',
		'GMR_DefineCustomObjectID(164662)'
	}
)

GMR.DefineQuest( "Scourge", nil, 380, "|cFFFFAB48 Night Webs Hollow |r", "Custom", 1848.8227539063, 1580.4724121094, 94.661933898926, 1570, 1848.8227539063, 1580.4724121094, 94.661933898926, 1570, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(380, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1504, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(380, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1505, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(2148.0815429688, 1783.6322021484, 107.00146484375, 50)',
		'GMR.DefineProfileCenter(2082.8798828125, 1750.5493164063, 76.127647399902, 50)',
		'GMR.DefineProfileCenter(2058.6862792969, 1772.4825439453, 88.5859375, 50)',
		'GMR.DefineProfileCenter(2036.8009033203, 1884.3094482422, 102.88173675537, 20)',
		'GMR.DefineProfileCenter(2020.9887695313, 1908.0483398438, 105.40769195557, 30)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1504)',
		'GMR.DefineQuestEnemyId(1505)'
	}
)

GMR.DefineQuest( "Scourge", nil, 381, "|cFFFFAB48 The Scarlet Crusade |r", "Custom", 1848.8227539063, 1580.4724121094, 94.662452697754, 1570, 1848.8227539063, 1580.4724121094, 94.661933898926, 1570, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(381, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineProfileCenter(1839.3432617188, 1382.5003662109, 75.178077697754, 60)',
		'GMR.DefineProfileCenter(1843.3319091797, 1317.3123779297, 83.471740722656, 60)',
		'GMR.DefineProfileCenter(1791.1960449219, 1284.2623291016, 110.01647949219, 60)',
		'GMR.DefineProfileCenter(1734.8381347656, 1327.8264160156, 106.20124816895, 60)',
		'GMR.DefineProfileCenter(1766.6474609375, 1402.2650146484, 95.454399108887, 60)',
		'GMR.DefineAreaBlacklist(1792.9460449219, 1338.9169921875, 89.252830505371, 20)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1506)',
		'GMR.DefineQuestEnemyId(1507)'
	}
)

GMR.DefineQuest( "Scourge", nil, 382, "|cFFFFAB48 The Red Messenger |r", "Custom", 1848.8227539063, 1580.4724121094, 94.662452697754, 1570, 1848.8227539063, 1580.4724121094, 94.661933898926, 1570, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(382, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1772.910034, 1381.260010, 91.035599, 3) then 
				GMR.MeshTo(1772.910034, 1381.260010, 91.035599)
			else 
				local npc1 = GMR.GetObjectWithInfo({ id = 1667, rawType = 5, isAlive = true })
				if npc1 then
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(1772.910034, 1381.260010, 91.035599, 150)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(1667)'
	}
)

GMR.DefineQuest( "Scourge", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Vital Intelligence (End of Starting Zone Quests) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 383, 1848.8227539063, 1580.4724121094, 94.662452697754, 1570 },
		{ 8, 2127.1938476563, 1305.2584228516, 53.919765472412, 6784 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
	}
)

GMR.DefineQuest( "Scourge", nil, nil, "|cFFFFAB48 Grinding: Undead Starting Area - Level 7 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 7 },
	{},
	{
		'GMR.DefineProfileCenter(1994.9067382813, 1646.4929199219, 74.438369750977, 40)',
		'GMR.DefineProfileCenter(1984.8977050781, 1698.1428222656, 79.483322143555, 50)',
		'GMR.DefineProfileCenter(2033.2338867188, 1707.4949951172, 78.199966430664, 50)',
		'GMR.DefineProfileCenter(2085.5546875, 1701.7719726563, 67.920951843262, 50)',
		'GMR.DefineProfileCenter(2080.9650878906, 1740.9365234375, 73.82234954834, 50)',
		'GMR.DefineProfileCenter(2038.3015136719, 1775.1694335938, 97.873832702637, 50)',
		'GMR.DefineProfileCenter(2028.4860839844, 1780.7625732422, 106.49603271484, 50)',
		'GMR.DefineProfileCenter(2053.1940917969, 1806.6491699219, 102.15893554688, 50)',
		'GMR.DefineProfileCenter(2040.8275146484, 1856.7719726563, 103.04599761963, 50)',
		'GMR.DefineProfileCenter(2058.8127441406, 1791.0648193359, 93.024307250977, 50)',
		'GMR.DefineProfileCenter(2117.4387207031, 1771.2185058594, 89.686248779297, 50)',
		'GMR.DefineProfileCenter(2137.5622558594, 1809.4761962891, 108.41210174561, 50)',
		'GMR.DefineProfileCenter(2165.6809082031, 1770.5795898438, 102.5841217041, 50)',
		'GMR.DefineProfileCenter(2128.0688476563, 1728.2691650391, 77.400085449219, 50)',
		'GMR.DefineProfileCenter(2139.30859375, 1700.1552734375, 85.649024963379, 50)',
		'GMR.DefineProfileCenter(2126.1413574219, 1657.7541503906, 77.425247192383, 50)',
		'GMR.DefineProfileCenter(2087.5075683594, 1694.5012207031, 68.094085693359, 50)',
		'GMR.DefineProfileCenter(2041.8790283203, 1705.5439453125, 76.693237304688, 50)',
		'GMR.DefineProfileCenter(2065.9594726563, 1721.8540039063, 89.859100341797, 50)',
		'GMR.DefineProfileCenter(2066.5454101563, 1722.7745361328, 89.906196594238, 50)',
		'GMR.DefineProfileCenter(2066.5454101563, 1722.7745361328, 89.906196594238, 50)',
		'GMR.DefineProfileCenter(2066.5454101563, 1722.7745361328, 89.906196594238, 50)',
		'GMR.DefineQuestEnemyId(1513)',
		'GMR.DefineQuestEnemyId(1509)',
		'GMR.DefineQuestEnemyId(1506)',
		'GMR.DefineQuestEnemyId(1919)',
		'GMR.DefineQuestEnemyId(1918)',
		'GMR.DefineQuestEnemyId(1916)',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)'
	}
)

GMR.DefineQuest( "Scourge", nil, 365, "|cFFFFAB48 Fields of Grief |r", "Custom", 2205.7495117188, 1184.5242919922, 32.144771575928, 1519, 2259.040039, 347.048004, 36.102501, 1518, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(365, 1) then
			GMR.SetQuestingState("Idle");
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2337.7631835938, 1396.7209472656, 33.332527160645, 30)',
		'GMR.DefineProfileCenter(2305.955078125, 1470.3542480469, 33.332660675049, 30)',
		'GMR.DefineProfileCenter(2268.4782714844, 1470.1296386719, 33.334495544434, 30)',
		'GMR.DefineAreaBlacklist(2300.8483886719, 1307.8382568359, 31.394189834595, 65)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Drakenthall Village
		'GMR.DefineSellVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineRepairVendor(1859.3216552734, 1567.9519042969, 94.31356048584, 2113)',
		'GMR.DefineGoodsVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineAmmoVendor(1866.0217285156, 1574.4468994141, 94.313758850098, 2115)',
		'GMR.DefineQuestEnemyId(375)',
		'GMR_DefineCustomObjectID(375)'
	}
)

GMR.DefineQuest( "Scourge", nil, 407, "|cFFFFAB48 Fields of Grief Part 2 |r", "TalkTo", 2259.0368652344, 347.04779052734, 36.019336700439, 1518, 2292.4516601563, 233.12174987793, 27.089242935181, 1931, {}, 
	{
		'GMR.DefineProfileCenter(2292.4516601563, 233.12174987793, 27.089242935181, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)'
	} 
)

GMR.DefineQuest( "Scourge", nil, 383, "|cFFFFAB48 Vital Intelligence |r", "TalkTo", 1848.8227539063, 1580.4724121094, 94.662506103516, 1570, 2278.078125, 295.58682250977, 35.146984100342, 1515, {}, 
	{
		'GMR.DefineProfileCenter(2278.078125, 295.58682250977, 35.146984100342, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)'
	} 
)

GMR.DefineQuest( "Scourge", nil, 8, "|cFFFFAB48 A Rogue's Deal |r", "TalkTo", 2127.1938476563, 1305.2584228516, 53.919765472412, 6784, 2269.510010, 244.944000, 34.340199, 5688, {}, 
	{
		'GMR.DefineProfileCenter(2269.510010, 244.944000, 34.340199, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)'
	} 
)

GMR.DefineQuest( "Scourge", nil, 590, "|cFFFFAB48 A Rogue's Deal Part 2 (DUEL) |r", "Custom", 2126.041015625, 1304.1669921875, 54.054893493652, 6784, 2126.041015625, 1304.1669921875, 54.054893493652, 6784,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(590, 1) then
			GMR.SetQuestingState("Idle");
		end
	]]}, 
	{
		'GMR.DefineProfileCenter(2126.041015625, 1304.1669921875, 54.054893493652, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineQuestEnemyId(6784)'
	} 
)

GMR.DefineQuest( "Scourge", nil, 5481, "|cFFFFAB48 Gordo's Task |r", "Custom", 2198.0612792969, 1057.3499755859, 29.2529296875, 10666, 2393.1, 398.14071655273, 33.889305114746, 10665, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(5481, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2099.5729980469, 476.4758605957, 60.633106231689, 60)',
		'GMR.DefineProfileCenter(2105.9301757813, 612.31103515625, 35.027904510498, 60)',
		'GMR.DefineProfileCenter(2020.1762695313, 717.25122070313, 42.128211975098, 60)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineQuestEnemyId(175566)',
		'GMR_DefineCustomObjectID(175566)'
	}
)

GMR.DefineQuest( "Scourge", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Brill Quests (Level 7) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 5482, 2394.2590332031, 398.65347290039, 33.889305114746, 10665 }, -- Doom Weed (Requires Gordo's Task)
		{ 404, 2287.6625976563, 403.37197875977, 33.921432495117, 1496 },   -- A Putrid Task
		{ 367, 2259.0368652344, 347.04779052734, 36.019397735596, 1518 },   -- A New Plague
		{ 427, 2278.078125, 295.58682250977, 35.146984100342, 1515 }, 		-- At War With The Scarlet Crusade
		{ 358, 2305.9111328125, 265.16384887695, 38.669979095459, 1499 },   -- Graverobbers
		{ 354, 2262.2609863281, 244.25694274902, 33.633995056152, 1500 },   -- Deaths in the Family
		{ 362, 2262.2609863281, 244.25694274902, 33.633995056152, 1500 },	-- The Haunted Mills
		{ 375, 2249.0629882813, 236.78450012207, 41.114723205566, 1521 },	-- The Chill of Death
		{ 356, 2022.3259277344, 74.046005249023, 36.44637298584, 1495 },	-- Rear Guard Patrol
		{ 398, 2286.015, 288.7661, 35.18226, 711 }							-- WANTED: Maggot Eye
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)

GMR.DefineQuest( "Scourge", nil, 367, "|cFFFFAB48 A New Plague |r", "Custom", 2259.0368652344, 347.04779052734, 36.019397735596, 1518, 2259.0368652344, 347.04779052734, 36.019397735596, 1518, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(367, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2156.979, 310.6325, 44.01967, 80)',
		'GMR.DefineProfileCenter(1928.678, 580.7239, 48.00039, 80)',
		'GMR.DefineProfileCenter(2141.583, 845.5686, 35.73692, 80)',
		'GMR.DefineProfileCenter(2048.093, 869.3588, 33.91657, 80)',
		'GMR.DefineProfileCenter(2205.989, 976.9069, 34.38274, 80)',
		'GMR.DefineProfileCenter(2303.767, 943.3101, 57.70234, 80)',
		'GMR.DefineProfileCenter(2369.658, 887.7717, 60.33187, 80)',
		'GMR.DefineProfileCenter(2433.769, 948.8663, 72.24002, 80)',
		'GMR.DefineProfileCenter(2501.05, 1017.049, 76.9332, 80)',
		'GMR.DefineProfileCenter(2550.644, 1095.959, 82.83502, 80)',
		'GMR.DefineProfileCenter(2545.162, 1188.912, 69.5898, 80)',
		'GMR.DefineProfileCenter(2561.514, 1277.77, 49.26146, 80)',
		'GMR.DefineProfileCenter(2362.903, 1154.231, 40.78565, 80)',
		'GMR.DefineProfileCenter(2221.653, 1022.346, 36.60854, 80)',
		'GMR.DefineProfileCenter(2146.448, 1141.902, 33.3686, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1547)',
		'GMR.DefineQuestEnemyId(1548)'
	}
)

GMR.DefineQuest( "Scourge", nil, 404, "|cFFFFAB48 A Putrid Task |r", "Custom", 2287.6625976563, 403.37197875977, 33.921432495117, 1496, 2287.6625976563, 403.37197875977, 33.921432495117, 1496, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(404, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2196.589, 547.9072, 34.00985, 80)',
		'GMR.DefineProfileCenter(2120.595, 526.9319, 50.60775, 80)',
		'GMR.DefineProfileCenter(2039.403, 618.5978, 35.91185, 80)',
		'GMR.DefineProfileCenter(2107.609, 682.6901, 36.73315, 80)',
		'GMR.DefineProfileCenter(2115.883, 623.2257, 34.93325, 80)',
		'GMR.DefineProfileCenter(2221.727, 720.1471, 37.13268, 80)',
		'GMR.DefineProfileCenter(2315.164, 709.6956, 38.92304, 80)',
		'GMR.DefineProfileCenter(2286.273, 581.9818, 33.90998, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1526)',
		'GMR.DefineQuestEnemyId(1525)',
	}
)

GMR.DefineQuest( "Scourge", nil, nil, "|cFFFFAB48 Pickup The Mills Overrun |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 426, 2287.6625976563, 403.37197875977, 33.921745300293, 1496 } -- The Mills Overrun
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)

GMR.DefineQuest( "Scourge", nil, 427, "|cFFFFAB48 At War With the Scarlet Crusade |r", "Custom", 2278.078125, 295.58682250977, 35.146984100342, 1515, 2278.078125, 295.58682250977, 35.146984100342, 1515, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(427, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2402.877, 1399.836, 33.51569, 80)',
		'GMR.DefineProfileCenter(2402.975, 1520.338, 32.39095, 80)',
		'GMR.DefineProfileCenter(2471.744, 1546.077, 34.10182, 80)',
		'GMR.DefineProfileCenter(2314.688, 1574.555, 34.01783, 80)',
		'GMR.DefineProfileCenter(2322.122, 1656.795, 38.22103, 80)',
		'GMR.DefineProfileCenter(2361.778, 1665.156, 33.58817, 80)',
		'GMR.DefineProfileCenter(2414.189, 1645.017, 32.65528, 80)',
		'GMR.DefineProfileCenter(2375.286, 1544.773, 33.78072, 80)',
		'GMR.DefineAreaBlacklist(2445.488, 1596.917, 37.06562,20)',
		'GMR.DefineAreaBlacklist(2452.258, 1589.166, 72.15704,20)',
		'GMR.DefineAreaBlacklist(2453.304, 1602.068, 72.15704,20)',
		'GMR.DefineAreaBlacklist(2438.676, 1594.197, 72.15556,20)',
		'GMR.DefineAreaBlacklist(2443.4, 1600.2, 66.57274,20)',
		'GMR.DefineAreaBlacklist(2439.902, 1599.074, 50.77711,20)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.BlacklistId(1935)',
		'GMR.BlacklistId(1934)',
		'GMR.BlacklistId(1543)',
		'GMR.BlacklistId(1544)',
		'GMR.DefineQuestEnemyId(1535)',
		'GMR.DefineQuestEnemyId(1525)',
	}
)

GMR.DefineQuest( "Scourge", nil, nil, "|cFFFFAB48 Pickup At War With The Scarlet Crusade - part 2 |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 370, 2278.078125, 295.58682250977, 35.146984100342, 1515 } -- At War with the Scarlet Crusade - Part 2
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)

GMR.DefineQuest( "Scourge", nil, 5482, "|cFFFFAB48 Doom Weed |r", "Custom", 2394.2590332031, 398.65347290039, 33.889305114746, 10665, 2394.2590332031, 398.65347290039, 33.889305114746, 10665, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(5482, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2550.307129, 377.935608, 34.553146, 120)',
		'GMR.DefineProfileCenter(2646.625732, 434.298950, 24.713654, 120)',
		'GMR.DefineProfileCenter(2753.867432, 436.623260, 20.205891, 120)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(176753)',
		'GMR.DefineQuestEnemyId(106318)',
		'GMR_DefineCustomObjectID(176753)',
		'GMR_DefineCustomObjectID(106318)'
	}
)

GMR.DefineQuest( "Scourge", nil, 358, "|cFFFFAB48 Graverobbers |r", "Custom", 2305.9111328125, 265.16384887695, 38.669979095459, 1499, 2305.9111328125, 265.16384887695, 38.669979095459, 1499, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(358, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1941, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(358, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1675, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(358, 3) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineProfileCenter(2563.158, 534.6273, 15.22391, 80)',
		'GMR.DefineProfileCenter(2799.44, 357.6886, 24.26473, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1941)',
		'GMR.DefineQuestEnemyId(1674)',
		'GMR.DefineQuestEnemyId(1675)'
	}
)

GMR.DefineQuest( "Scourge", nil, 359, "|cFFFFAB48 Forsaken Duties |r", "TalkTo", 2305.9111328125, 265.16384887695, 38.669979095459, 1499, 2022.3259277344, 74.046005249023, 36.44637298584, 1495, 
	{}, 
	{
		'GMR.DefineProfileCenter(2022.3259277344, 74.046005249023, 36.44637298584, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
	} 
)

GMR.DefineQuest( "Scourge", nil, 360, "|cFFFFAB48 Return to the Magistrate |r", "TalkTo", 2022.3259277344, 74.046005249023, 36.44637298584, 1495, 2305.9111328125, 265.16384887695, 38.669979095459, 1499, 
	{}, 
	{
		'GMR.DefineProfileCenter(2305.9111328125, 265.16384887695, 38.669979095459, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	} 
)

GMR.DefineQuest("Scourge", nil, 375, "The Chill of Death", "GrindAndBuyItem", 2249.0629882813, 236.78450012207, 41.114723205566, 1521, 2249.0629882813, 236.78450012207, 41.114723205566, 1521, 
	{
		2320, 1, 2259.7258300781, 275.57119750977, 34.837749481201, 2118
	},
	{
		-- Brill - Tirisfal Glades Vendors
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2238.305, 255.4581, 34.11528)',

		'GMR.DefineProfileCenter(2145.067871, 395.589874, 54.739853, 100)',
		'GMR.DefineProfileCenter(1952.905151, 541.772339, 49.937157, 100)',
		'GMR.DefineProfileCenter(1963.002319, 962.266907, 35.585918, 100)',
		'GMR.DefineQuestEnemyID(1553)',
		'GMR.DefineQuestEnemyID(1554)',
		'GMR.SkipTurnIn(false)'

	}
)


GMR.DefineQuest( "Scourge", nil, nil, "|cFFFFAB48 Grinding: Around Brill (UNDEAD) - Level 10 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 10 },
	{},
	{
		'GMR.DefineProfileCenter(2150.19, 247.7995, 47.2017, 80)',
		'GMR.DefineProfileCenter(2188.953, 171.9189, 52.31499, 80)',
		'GMR.DefineProfileCenter(2133.943, 70.44136, 34.01689, 80)',
		'GMR.DefineProfileCenter(2185.141, -34.18738, 38.49308, 80)',
		'GMR.DefineProfileCenter(2022.109, -133.1156, 33.90937, 80)',
		'GMR.DefineProfileCenter(2061.168, -237.0187, 38.09074, 80)',
		'GMR.DefineProfileCenter(2202.681, -102.2207, 30.75964, 80)',
		'GMR.DefineProfileCenter(2255.255, 14.37674, 30.29226, 80)',
		'GMR.DefineProfileCenter(2331.621, 104.1143, 34.54053, 80)',
		'GMR.DefineProfileCenter(2405.106, 170.9065, 33.32951, 80)',
		'GMR.DefineProfileCenter(2516.193, 220.9865, 43.15187, 80)',
		'GMR.DefineProfileCenter(2612.764, 252.3105, 38.66068, 80)',
		'GMR.DefineProfileCenter(2744.121, 184.0527, 31.47173, 80)',
		'GMR.DefineProfileCenter(2771.642, 43.64564, 32.93444, 80)',
		'GMR.DefineProfileCenter(2833.113, -89.54623, 33.37553, 80)',
		'GMR.DefineProfileCenter(2766.806, -190.2295, 38.00851, 80)',
		'GMR.DefineProfileCenter(2740.202, -277.574, 64.70099, 80)',
		'GMR.DefineProfileCenter(2801.087, -117.7343, 33.14269, 80)',
		'GMR.DefineProfileCenter(2753.75, -28.47021, 34.74314, 80)',
		'GMR.DefineProfileCenter(2702.596, 81.61485, 34.09962, 80)',
		'GMR.DefineProfileCenter(2589.655, 132.6209, 30.25597, 80)',
		'GMR.DefineProfileCenter(2500.774, 157.3551, 29.31479, 80)',
		'GMR.DefineProfileCenter(2413.219, 168.6181, 33.90602, 80)',
		'GMR.DefineProfileCenter(2345.122, 105.1979, 32.8913, 80)',
		'GMR.DefineProfileCenter(2234.323, 30.34956, 32.41689, 80)',
		'GMR.DefineQuestEnemyId(1547)',
		'GMR.DefineQuestEnemyId(1553)',
		'GMR.DefineQuestEnemyId(1548)',
		'GMR.DefineQuestEnemyId(1554)',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)


----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Warlock ][Level 10][Undead / Scourge] - Summon Voidwalker
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest( "Scourge", "WARLOCK", 1478, "|cFFFFAB48 Halgar's Summons |r", "Custom", 2250.620117, 248.955002, 41.198101, 5724, 1711.589966, 57.167198, -62.205898, 5675, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1478, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(2236.592, 252.3918, 33.93154, 15)',
		'GMR.DefineUnstuck(2226.401, 254.0872, 33.78566)',
		'GMR.DefineUnstuck(2215.687, 255.4095, 34.1702)',
		'GMR.DefineUnstuck(2198.263, 254.1577, 35.71895)',
		'GMR.DefineUnstuck(2187.646, 253.334, 38.22792)',
		'GMR.DefineUnstuck(2177.353, 252.6445, 40.00787)',
		'GMR.DefineUnstuck(2166.855, 251.9413, 41.88131)',
		'GMR.DefineUnstuck(2156.379, 251.2395, 43.92718)',
		'GMR.DefineUnstuck(2145.923, 250.5391, 48.17446)',
		'GMR.DefineUnstuck(2135.384, 249.8331, 54.72035)',
		'GMR.DefineUnstuck(2124.389, 248.5156, 59.97707)',
		'GMR.DefineUnstuck(2114.173, 247.1222, 60.52409)',
		'GMR.DefineUnstuck(2103.743, 245.7362, 59.79256)',
		'GMR.DefineUnstuck(2093.353, 244.3761, 58.99133)',
		'GMR.DefineUnstuck(2082.831, 243.4602, 58.85736)',
		'GMR.DefineUnstuck(2072.434, 242.377, 57.50846)',
		'GMR.DefineUnstuck(2062.436, 239.3395, 55.711)',
		'GMR.DefineUnstuck(2052.248, 236.13, 49.58238)',
		'GMR.DefineUnstuck(2042.115, 237.156, 45.37488)',
		'GMR.DefineUnstuck(2033.004, 242.2939, 46.0036)',
		'GMR.DefineUnstuck(2024.51, 248.43, 48.62227)',
		'GMR.DefineUnstuck(2016.006, 254.589, 47.59326)',
		'GMR.DefineUnstuck(2007.548, 260.8105, 44.98804)',
		'GMR.DefineUnstuck(1999.101, 267.0833, 44.34356)',
		'GMR.DefineUnstuck(1990.861, 273.5426, 45.25836)',
		'GMR.DefineUnstuck(1982.895, 280.3661, 44.23634)',
		'GMR.DefineUnstuck(1974.948, 287.2288, 41.62583)',
		'GMR.DefineUnstuck(1967.154, 294.2333, 40.77947)',
		'GMR.DefineUnstuck(1959.362, 301.2711, 40.41968)',
		'GMR.DefineUnstuck(1951.326, 308.0276, 38.79796)',
		'GMR.DefineUnstuck(1943.2, 314.677, 37.12831)',
		'GMR.DefineUnstuck(1936.843, 322.9557, 36.25694)',
		'GMR.DefineUnstuck(1931.922, 332.2074, 35.59121)',
		'GMR.DefineUnstuck(1926.994, 341.4788, 34.82622)',
		'GMR.DefineUnstuck(1921.436, 350.4057, 34.27668)',
		'GMR.DefineUnstuck(1915.031, 358.6725, 34.02349)',
		'GMR.DefineUnstuck(1908.71, 367.0335, 34.00478)',
		'GMR.DefineUnstuck(1903.725, 376.2675, 34.19984)',
		'GMR.DefineUnstuck(1899.142, 385.6979, 34.62474)',
		'GMR.DefineUnstuck(1896.118, 395.7387, 34.45755)',
		'GMR.DefineUnstuck(1893.693, 405.9457, 34.56581)',
		'GMR.DefineUnstuck(1890.868, 416.0392, 34.61383)',
		'GMR.DefineUnstuck(1887.98, 426.1334, 34.9101)',
		'GMR.DefineUnstuck(1884.327, 435.9771, 34.32167)',
		'GMR.DefineUnstuck(1880.588, 445.9682, 34.50458)',
		'GMR.DefineUnstuck(1876.966, 455.6446, 34.2736)',
		'GMR.DefineUnstuck(1873.285, 465.4777, 34.28709)',
		'GMR.DefineUnstuck(1869.612, 475.293, 34.28693)',
		'GMR.DefineUnstuck(1868.366, 486.143, 35.08328)',
		'GMR.DefineUnstuck(1856.841, 490.1205, 34.7118)',
		'GMR.DefineUnstuck(1846.824, 493.2679, 34.49468)',
		'GMR.DefineUnstuck(1836.756, 496.3198, 34.26251)',
		'GMR.DefineUnstuck(1826.643, 499.1447, 34.33017)',
		'GMR.DefineUnstuck(1816.532, 501.9756, 34.25874)',
		'GMR.DefineUnstuck(1806.543, 505.2006, 34.14602)',
		'GMR.DefineUnstuck(1796.743, 508.9094, 34.27099)',
		'GMR.DefineUnstuck(1789.229, 515.5202, 33.68699)',
		'GMR.DefineUnstuck(1784.846, 525.0829, 33.43451)',
		'GMR.DefineUnstuck(1779.203, 533.9068, 34.61477)',
		'GMR.DefineUnstuck(1774.643, 543.2903, 36.13944)',
		'GMR.DefineUnstuck(1769.35, 552.2892, 37.51299)',
		'GMR.DefineUnstuck(1763.191, 561.0256, 36.86402)',
		'GMR.DefineUnstuck(1757.565, 569.6902, 34.80636)',
		'GMR.DefineUnstuck(1751.823, 578.4812, 34.16957)',
		'GMR.DefineUnstuck(1746.069, 587.2643, 34.33213)',
		'GMR.DefineUnstuck(1740.044, 595.8625, 35.36411)',
		'GMR.DefineUnstuck(1734.354, 604.6862, 35.92915)',
		'GMR.DefineUnstuck(1728.75, 613.5479, 36.28766)',
		'GMR.DefineUnstuck(1722.865, 622.2368, 37.42373)',
		'GMR.DefineUnstuck(1716.619, 630.7293, 40.08358)',
		'GMR.DefineUnstuck(1710.422, 639.1562, 43.20666)',
		'GMR.DefineUnstuck(1704.22, 647.5892, 45.59728)',
		'GMR.DefineUnstuck(1698.343, 656.2866, 48.54382)',
		'GMR.DefineUnstuck(1693.169, 665.3549, 52.80971)',
		'GMR.DefineUnstuck(1692.56, 675.6268, 56.6429)',
		'GMR.DefineUnstuck(1695.402, 685.7217, 59.19622)',
		'GMR.DefineUnstuck(1699.21, 695.5062, 61.74138)',
		'GMR.DefineUnstuck(1703.12, 705.2248, 63.4027)',
		'GMR.DefineUnstuck(1706.596, 716.9417, 62.16449)',
		'GMR.DefineUnstuck(1697.464, 724.6987, 67.33198)',
		'GMR.DefineUnstuck(1683.565, 727.8736, 76.51557)',
		'GMR.DefineUnstuck(1673.153, 729.2241, 77.99963)',
		'GMR.DefineUnstuck(1663.004, 730.3484, 79.59525)',
		'GMR.DefineUnstuck(1652.541, 731.2335, 80.38084)',
		'GMR.DefineUnstuck(1642.066, 732.2177, 78.24287)',
		'GMR.DefineUnstuck(1626.353, 732.453, 74.3444)',
		'GMR.DefineUnstuck(1614.636, 727.4224, 69.07124)',
		'GMR.DefineUnstuck(1603.779, 718.5928, 65.15752)',
		'GMR.DefineUnstuck(1595.083, 706.3583, 60.59822)',
		'GMR.DefineUnstuck(1593.016, 696.0657, 55.93701)',
		'GMR.DefineUnstuck(1591.352, 685.6983, 52.06009)',
		'GMR.DefineUnstuck(1591.012, 675.2861, 49.5619)',
		'GMR.DefineUnstuck(1597.343, 666.9437, 45.56847)',
		'GMR.DefineUnstuck(1603.313, 658.3082, 41.58814)',
		'GMR.DefineUnstuck(1610.284, 650.562, 36.55376)',
		'GMR.DefineUnstuck(1619.549, 645.7024, 35.97715)',
		'GMR.DefineUnstuck(1629.951, 644.7031, 32.19728)',
		'GMR.DefineUnstuck(1640.45, 644.6105, 26.65052)',
		'GMR.DefineUnstuck(1650.95, 644.5178, 23.50147)',
		'GMR.DefineUnstuck(1661.445, 644.2532, 19.0919)',
		'GMR.DefineUnstuck(1671.584, 641.8901, 14.97787)',
		'GMR.DefineUnstuck(1683.755, 631.5815, 9.491672)',
		'GMR.DefineUnstuck(1687.781, 621.8652, 5.155764)',
		'GMR.DefineUnstuck(1690.364, 611.8295, 2.058424)',
		'GMR.DefineUnstuck(1685.14, 602.7971, -2.823752)',
		'GMR.DefineUnstuck(1678.326, 595.0677, -7.323402)',
		'GMR.DefineUnstuck(1670.815, 587.494, -9.625772)',
		'GMR.DefineUnstuck(1664.342, 579.3798, -13.72063)',
		'GMR.DefineUnstuck(1661.169, 569.6526, -16.00454)',
		'GMR.DefineUnstuck(1662.863, 559.2917, -16.27076)',
		'GMR.DefineUnstuck(1663.982, 549.2034, -15.00963)',
		'GMR.DefineUnstuck(1664.688, 536.8452, -11.38674)',
		'GMR.DefineUnstuck(1664.951, 522.5212, -13.07102)',
		'GMR.DefineUnstuck(1664.82, 512.1901, -12.84571)',
		'GMR.DefineUnstuck(1664.661, 501.6933, -12.21246)',
		'GMR.DefineUnstuck(1664.501, 491.0265, -11.6532)',
		'GMR.DefineUnstuck(1664.346, 480.6956, -11.89053)',
		'GMR.DefineUnstuck(1664.643, 478.8625, -11.89053)',
		'GMR.DefineUnstuck(1680.031, 479.2453, -9.129909)',
		'GMR.DefineUnstuck(1690.536, 479.479, -6.185747)',
		'GMR.DefineUnstuck(1701.028, 479.1044, -3.374754)',
		'GMR.DefineUnstuck(1712.231, 478.6169, -1.611948)',
		'GMR.DefineUnstuck(1722.526, 475.304, -2.746873)',
		'GMR.DefineUnstuck(1703.846, 478.3014, -2.729854)',
		'GMR.DefineUnstuck(1693.54, 478.6202, -5.198791)',
		'GMR.DefineUnstuck(1682.874, 478.8319, -8.388901)',
		'GMR.DefineUnstuck(1672.354, 478.9652, -11.30742)',
		'GMR.DefineUnstuck(1661.854, 479.0156, -11.89007)',
		'GMR.DefineUnstuck(1651.354, 479.066, -15.33964)',
		'GMR.DefineUnstuck(1638.587, 479.1273, -20.03626)',
		'GMR.DefineUnstuck(1628.99, 479.1217, -22.8688)',
		'GMR.DefineUnstuck(1628.738, 463.5771, -26.84361)',
		'GMR.DefineUnstuck(1629.141, 453.2531, -30.99132)',
		'GMR.DefineUnstuck(1629.132, 442.7323, -34.2633)',
		'GMR.DefineUnstuck(1629.118, 436.7682, -34.2633)',
		'GMR.DefineUnstuck(1609.615, 438.1479, -41.73772)',
		'GMR.DefineUnstuck(1598.234, 438.0883, -46.33503)',
		'GMR.DefineUnstuck(1595.292, 437.8232, -46.33503)',
		'GMR.DefineUnstuck(1597.05, 427.7943, -46.34811)',
		'GMR.DefineUnstuck(1597.549, 423.6664, -46.37032)',
		'GMR.DefineUnstuck(1611.873, 423.2232, -51.12579)',
		'GMR.DefineUnstuck(1621.635, 423.0699, -57.7126)',
		'GMR.DefineUnstuck(1632.41, 419.3021, -62.17767)',
		'GMR.DefineUnstuck(1641.874, 414.7924, -61.62052)',
		'GMR.DefineUnstuck(1655.832, 409.6123, -62.19259)',
		'GMR.DefineUnstuck(1667.131, 405.8606, -62.17201)',
		'GMR.DefineUnstuck(1682.988, 400.9868, -62.27422)',
		'GMR.DefineUnstuck(1692.366, 396.2717, -62.28307)',
		'GMR.DefineUnstuck(1700.559, 389.7509, -62.26823)',
		'GMR.DefineUnstuck(1708.504, 382.8874, -62.17938)',
		'GMR.DefineUnstuck(1716.34, 375.8985, -61.035)',
		'GMR.DefineUnstuck(1723.634, 368.3832, -60.48622)',
		'GMR.DefineUnstuck(1730.956, 360.8723, -60.48622)',
		'GMR.DefineUnstuck(1738.78, 353.8683, -60.48142)',
		'GMR.DefineUnstuck(1745.337, 345.6785, -60.48199)',
		'GMR.DefineUnstuck(1750.856, 336.7478, -60.48341)',
		'GMR.DefineUnstuck(1756.215, 327.7433, -62.26921)',
		'GMR.DefineUnstuck(1761.553, 318.6768, -62.22971)',
		'GMR.DefineUnstuck(1765.625, 309.0408, -62.12986)',
		'GMR.DefineUnstuck(1768.134, 298.8535, -62.15606)',
		'GMR.DefineUnstuck(1770.175, 288.5608, -61.62032)',
		'GMR.DefineUnstuck(1772.064, 278.2326, -62.17644)',
		'GMR.DefineUnstuck(1773.847, 267.8851, -62.17644)',
		'GMR.DefineUnstuck(1775.633, 257.5168, -62.17644)',
		'GMR.DefineUnstuck(1777.243, 247.1662, -62.17644)',
		'GMR.DefineUnstuck(1776.909, 236.6575, -62.17644)',
		'GMR.DefineUnstuck(1776.12, 226.1873, -62.17644)',
		'GMR.DefineUnstuck(1775.004, 215.747, -62.17644)',
		'GMR.DefineUnstuck(1772.742, 205.5133, -62.17644)',
		'GMR.DefineUnstuck(1770.033, 195.3687, -61.62038)',
		'GMR.DefineUnstuck(1767.325, 185.224, -62.18116)',
		'GMR.DefineUnstuck(1764.616, 175.0794, -62.20003)',
		'GMR.DefineUnstuck(1761.908, 164.9347, -62.26677)',
		'GMR.DefineUnstuck(1758.967, 154.866, -62.28952)',
		'GMR.DefineUnstuck(1754.746, 144.1514, -62.29928)',
		'GMR.DefineUnstuck(1758.206, 134.2271, -62.29824)',
		'GMR.DefineUnstuck(1761.967, 124.4237, -62.2937)',
		'GMR.DefineUnstuck(1765.199, 114.4902, -62.28457)',
		'GMR.DefineUnstuck(1755.501, 99.53073, -62.27688)',
		'GMR.DefineUnstuck(1747.084, 93.2544, -62.27538)',
		'GMR.DefineUnstuck(1735.191, 81.16837, -62.27538)',
		'GMR.DefineUnstuck(1727.932, 73.58132, -62.27846)',
		'GMR.DefineUnstuck(1720.583, 66.31914, -62.28)',
		'GMR.DefineUnstuck(1713.658, 59.40923, -62.28583)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1711.589966, 57.167198, -62.205898, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest("Scourge", "WARLOCK", 1473, "|cFFFFAB48 Creature of the Void |r", "Custom", 1711.589966, 57.167198, -62.205898, 5675, 1711.589966, 57.167198, -62.205898, 5675, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1473, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1801.551, 725.7441, 49.36598, 3) then 
				GMR.MeshTo(1801.551, 725.7441, 49.36598)
			else 
				local warlockChest = GMR.GetObjectWithInfo({ id = 37098, rawType = 8 })
				local Chest = GMR.GetObjectWithInfo({ id = 106318, rawType = 8 })
				if Chest then
					GMR.Questing.InteractWith(Chest, nil, nil, nil, nil, 5)
				elseif warlockChest then
					GMR.Questing.InteractWith(warlockChest, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(true)',
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(1713.027, 58.79336, -62.28737, 60)',
		'GMR.DefineUnstuck(1720.438, 66.69907, -62.28059)',
		'GMR.DefineUnstuck(1731.078, 77.82211, -62.27915)',
		'GMR.DefineUnstuck(1738.335, 85.41023, -62.27658)',
		'GMR.DefineUnstuck(1745.593, 92.99835, -62.27658)',
		'GMR.DefineUnstuck(1752.742, 100.6877, -62.27808)',
		'GMR.DefineUnstuck(1759.821, 108.5556, -62.281)',
		'GMR.DefineUnstuck(1765.713, 117.1768, -62.284)',
		'GMR.DefineUnstuck(1767.562, 127.1452, -62.29151)',
		'GMR.DefineUnstuck(1763.153, 136.6026, -62.29594)',
		'GMR.DefineUnstuck(1757.595, 145.5357, -62.29748)',
		'GMR.DefineUnstuck(1754.172, 155.3549, -62.21753)',
		'GMR.DefineUnstuck(1755.513, 165.5702, -62.1677)',
		'GMR.DefineUnstuck(1760.889, 174.5649, -62.19343)',
		'GMR.DefineUnstuck(1766.016, 183.721, -62.18805)',
		'GMR.DefineUnstuck(1769.899, 193.4951, -61.6205)',
		'GMR.DefineUnstuck(1772.428, 203.6741, -62.17636)',
		'GMR.DefineUnstuck(1773.756, 214.0838, -62.17636)',
		'GMR.DefineUnstuck(1774.147, 224.5531, -62.17636)',
		'GMR.DefineUnstuck(1774.203, 235.0734, -62.17636)',
		'GMR.DefineUnstuck(1774.061, 245.5724, -62.17636)',
		'GMR.DefineUnstuck(1773.96, 256.072, -62.17636)',
		'GMR.DefineUnstuck(1773.776, 266.5697, -62.17636)',
		'GMR.DefineUnstuck(1772.035, 276.9083, -62.17636)',
		'GMR.DefineUnstuck(1769.657, 287.1351, -61.62031)',
		'GMR.DefineUnstuck(1767.178, 297.4031, -62.16741)',
		'GMR.DefineUnstuck(1764.403, 307.4635, -62.13385)',
		'GMR.DefineUnstuck(1761.49, 317.573, -62.22604)',
		'GMR.DefineUnstuck(1757.634, 327.2989, -62.27309)',
		'GMR.DefineUnstuck(1751.47, 335.8199, -60.48415)',
		'GMR.DefineUnstuck(1745.189, 344.2345, -60.48415)',
		'GMR.DefineUnstuck(1739.055, 352.756, -60.48158)',
		'GMR.DefineUnstuck(1732.963, 361.2566, -60.48453)',
		'GMR.DefineUnstuck(1726.045, 369.1052, -60.48453)',
		'GMR.DefineUnstuck(1718.182, 375.8069, -60.48594)',
		'GMR.DefineUnstuck(1709.756, 382.4179, -62.19976)',
		'GMR.DefineUnstuck(1701.431, 388.9191, -62.26366)',
		'GMR.DefineUnstuck(1693.33, 395.2302, -62.27746)',
		'GMR.DefineUnstuck(1684.24, 400.73, -62.27992)',
		'GMR.DefineUnstuck(1674.505, 404.6038, -62.26521)',
		'GMR.DefineUnstuck(1664.694, 408.291, -62.20417)',
		'GMR.DefineUnstuck(1654.77, 411.7221, -62.19234)',
		'GMR.DefineUnstuck(1644.876, 415.2365, -61.62047)',
		'GMR.DefineUnstuck(1635, 418.8627, -62.17598)',
		'GMR.DefineUnstuck(1625.146, 422.4806, -60.0067)',
		'GMR.DefineUnstuck(1616.196, 423.0614, -54.09937)',
		'GMR.DefineUnstuck(1605.699, 422.8046, -46.87791)',
		'GMR.DefineUnstuck(1596.405, 423.3349, -46.37612)',
		'GMR.DefineUnstuck(1596.491, 426.1476, -46.35406)',
		'GMR.DefineUnstuck(1596.681, 435.3436, -46.33564)',
		'GMR.DefineUnstuck(1596.851, 438.1735, -46.33564)',
		'GMR.DefineUnstuck(1614.278, 437.412, -39.63515)',
		'GMR.DefineUnstuck(1624.609, 437.4623, -34.8861)',
		'GMR.DefineUnstuck(1629.921, 437.5893, -34.26337)',
		'GMR.DefineUnstuck(1629.19, 453.3147, -30.96634)',
		'GMR.DefineUnstuck(1629.336, 470.615, -24.02083)',
		'GMR.DefineUnstuck(1629.663, 479.3854, -22.86771)',
		'GMR.DefineUnstuck(1642.383, 478.5519, -18.52233)',
		'GMR.DefineUnstuck(1656.745, 478.7653, -12.83082)',
		'GMR.DefineUnstuck(1664.971, 479.0427, -11.89041)',
		'GMR.DefineUnstuck(1665.28, 493.256, -11.53609)',
		'GMR.DefineUnstuck(1665.105, 505.8544, -12.72852)',
		'GMR.DefineUnstuck(1664.899, 516.3524, -12.91658)',
		'GMR.DefineUnstuck(1664.692, 526.8713, -12.54228)',
		'GMR.DefineUnstuck(1664.264, 538.581, -11.38892)',
		'GMR.DefineUnstuck(1662.588, 550.8851, -15.10547)',
		'GMR.DefineUnstuck(1662.177, 561.3763, -16.40866)',
		'GMR.DefineUnstuck(1662.945, 571.7869, -15.58564)',
		'GMR.DefineUnstuck(1667.383, 581.2916, -12.62303)',
		'GMR.DefineUnstuck(1674.299, 589.1218, -9.526031)',
		'GMR.DefineUnstuck(1682.086, 596.0247, -7.079781)',
		'GMR.DefineUnstuck(1688.561, 604.3261, -1.659621)',
		'GMR.DefineUnstuck(1690.276, 614.5215, 2.856767)',
		'GMR.DefineUnstuck(1688.093, 624.611, 6.061492)',
		'GMR.DefineUnstuck(1682.29, 633.3853, 10.43936)',
		'GMR.DefineUnstuck(1674.627, 640.4525, 13.54236)',
		'GMR.DefineUnstuck(1665.198, 645.0164, 18.11449)',
		'GMR.DefineUnstuck(1655.207, 648.2351, 22.86804)',
		'GMR.DefineUnstuck(1644.772, 648.8699, 25.64111)',
		'GMR.DefineUnstuck(1634.312, 647.9522, 29.62127)',
		'GMR.DefineUnstuck(1623.845, 647.2534, 34.61678)',
		'GMR.DefineUnstuck(1613.801, 650.1248, 36.51628)',
		'GMR.DefineUnstuck(1604.975, 655.5977, 39.71554)',
		'GMR.DefineUnstuck(1598.265, 663.6658, 43.95945)',
		'GMR.DefineUnstuck(1592.687, 672.5284, 48.30902)',
		'GMR.DefineUnstuck(1591.011, 682.7299, 51.38053)',
		'GMR.DefineUnstuck(1592.562, 693.0954, 54.67151)',
		'GMR.DefineUnstuck(1595.744, 702.9152, 58.89167)',
		'GMR.DefineUnstuck(1599.484, 712.9053, 62.4038)',
		'GMR.DefineUnstuck(1605.346, 721.4932, 65.87614)',
		'GMR.DefineUnstuck(1613.771, 725.353, 68.75859)',
		'GMR.DefineUnstuck(1622.887, 730.5231, 72.74375)',
		'GMR.DefineUnstuck(1633.056, 732.3347, 76.20898)',
		'GMR.DefineUnstuck(1643.53, 732.052, 78.69986)',
		'GMR.DefineUnstuck(1654.008, 731.3664, 80.34671)',
		'GMR.DefineUnstuck(1664.488, 730.7297, 79.47942)',
		'GMR.DefineUnstuck(1674.896, 729.4719, 77.72263)',
		'GMR.DefineUnstuck(1683.933, 727.5088, 76.32156)',
		'GMR.DefineUnstuck(1692.756, 727.0591, 70.58398)',
		'GMR.DefineUnstuck(1710.535, 729.276, 61.8245)',
		'GMR.DefineUnstuck(1723.365, 738.3799, 54.27525)',
		'GMR.DefineUnstuck(1732.98, 742.2721, 50.7918)',
		'GMR.DefineUnstuck(1743.29, 744.2601, 50.24187)',
		'GMR.DefineUnstuck(1753.412, 743.2583, 50.82055)',
		'GMR.DefineUnstuck(1761.014, 736.0135, 48.4245)',
		'GMR.DefineUnstuck(1766.5, 727.2908, 46.15947)',
		'GMR.DefineUnstuck(1770.319, 717.5156, 45.22605)',
		'GMR.DefineUnstuck(1773.413, 708.5879, 45.37674)',
		'GMR.DefineUnstuck(1778.693, 698.1272, 45.44062)',
		'GMR.DefineUnstuck(1780.439, 684.3096, 44.29433)',
		'GMR.DefineUnstuck(1779.975, 683.4056, 43.94606)',
		'GMR.DefineProfileCenter(1801.856201, 725.959473, 49.614201, 180)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(37098)',
		'GMR.DefineQuestEnemyId(106318)',
		'GMR_DefineCustomObjectID(37098)',
		'GMR_DefineCustomObjectID(106318)'
	}
)

GMR.DefineQuest("Scourge", "WARLOCK", nil, "|cFFE25FFF TURNIN |r |cFFFFAB48 Creature of the Void |r", "MassTurnIn", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1473, 1711.589966, 57.167198, -62.205898, 5675 }
	},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(1779.975, 683.4056, 43.94606, 80)',
		'GMR.DefineUnstuck(1772.042, 692.9775, 44.82947)',
		'GMR.DefineUnstuck(1765.5, 707.2764, 44.8117)',
		'GMR.DefineUnstuck(1753.079, 717.0735, 46.22178)',
		'GMR.DefineUnstuck(1742.976, 719.8923, 47.60001)',
		'GMR.DefineUnstuck(1732.691, 721.8235, 49.48544)',
		'GMR.DefineUnstuck(1722.302, 723.345, 53.51799)',
		'GMR.DefineUnstuck(1711.876, 724.7233, 59.71112)',
		'GMR.DefineUnstuck(1701.443, 725.9026, 65.47131)',
		'GMR.DefineUnstuck(1691.018, 727.1585, 71.80028)',
		'GMR.DefineUnstuck(1680.595, 728.4266, 77.23187)',
		'GMR.DefineUnstuck(1670.152, 729.3045, 78.47401)',
		'GMR.DefineUnstuck(1659.689, 730.3638, 79.90232)',
		'GMR.DefineUnstuck(1649.381, 732.2319, 79.79242)',
		'GMR.DefineUnstuck(1638.952, 733.3896, 77.45693)',
		'GMR.DefineUnstuck(1628.459, 733.17, 75.04878)',
		'GMR.DefineUnstuck(1614.328, 727.3323, 68.94547)',
		'GMR.DefineUnstuck(1606.146, 720.8052, 66.25578)',
		'GMR.DefineUnstuck(1597.053, 706.0265, 60.14571)',
		'GMR.DefineUnstuck(1593.723, 696.0684, 55.90773)',
		'GMR.DefineUnstuck(1591.284, 685.8771, 52.10796)',
		'GMR.DefineUnstuck(1591.585, 675.4651, 49.47823)',
		'GMR.DefineUnstuck(1596.365, 666.3033, 45.17633)',
		'GMR.DefineUnstuck(1602.607, 657.78, 41.18997)',
		'GMR.DefineUnstuck(1610.549, 650.9011, 36.62398)',
		'GMR.DefineUnstuck(1620.05, 646.6182, 35.89953)',
		'GMR.DefineUnstuck(1630.443, 645.4346, 31.91754)',
		'GMR.DefineUnstuck(1640.929, 644.8964, 26.36551)',
		'GMR.DefineUnstuck(1651.394, 644.3593, 23.36735)',
		'GMR.DefineUnstuck(1661.897, 643.7561, 18.75901)',
		'GMR.DefineUnstuck(1672.008, 641.1358, 14.74057)',
		'GMR.DefineUnstuck(1679.863, 634.6076, 11.0917)',
		'GMR.DefineUnstuck(1686.223, 626.2409, 6.9384)',
		'GMR.DefineUnstuck(1690.766, 616.7755, 3.007479)',
		'GMR.DefineUnstuck(1691.15, 606.2684, 0.01339626)',
		'GMR.DefineUnstuck(1685.385, 597.8129, -5.965946)',
		'GMR.DefineUnstuck(1676.843, 591.7337, -9.007719)',
		'GMR.DefineUnstuck(1669.23, 584.5917, -10.87815)',
		'GMR.DefineUnstuck(1663.305, 575.8989, -14.66465)',
		'GMR.DefineUnstuck(1660.668, 565.7829, -16.54054)',
		'GMR.DefineUnstuck(1662.518, 555.4707, -15.73031)',
		'GMR.DefineUnstuck(1664.62, 545.3751, -13.72596)',
		'GMR.DefineUnstuck(1664.94, 534.7058, -11.38349)',
		'GMR.DefineUnstuck(1664.666, 517.5725, -12.94737)',
		'GMR.DefineUnstuck(1664.8, 506.9054, -12.80318)',
		'GMR.DefineUnstuck(1664.878, 496.3848, -11.55476)',
		'GMR.DefineUnstuck(1664.616, 485.8886, -11.89044)',
		'GMR.DefineUnstuck(1664.035, 476.7031, -11.89044)',
		'GMR.DefineUnstuck(1658.12, 478.3239, -12.18944)',
		'GMR.DefineUnstuck(1645.505, 478.6344, -17.43092)',
		'GMR.DefineUnstuck(1635.009, 478.9134, -21.72544)',
		'GMR.DefineUnstuck(1629.094, 479.1251, -22.8688)',
		'GMR.DefineUnstuck(1629.401, 461.301, -27.75547)',
		'GMR.DefineUnstuck(1629.339, 450.8012, -31.98127)',
		'GMR.DefineUnstuck(1629.277, 440.3014, -34.26329)',
		'GMR.DefineUnstuck(1629.26, 437.3194, -34.26329)',
		'GMR.DefineUnstuck(1610.367, 437.3088, -41.39612)',
		'GMR.DefineUnstuck(1599.867, 437.3156, -46.28862)',
		'GMR.DefineUnstuck(1595.757, 437.3183, -46.33508)',
		'GMR.DefineUnstuck(1596.629, 427.5923, -46.34894)',
		'GMR.DefineUnstuck(1596.926, 424.2876, -46.36306)',
		'GMR.DefineUnstuck(1610.922, 423.4203, -50.4717)',
		'GMR.DefineUnstuck(1629.078, 422.4637, -62.17719)',
		'GMR.DefineUnstuck(1639.023, 416.2701, -62.17719)',
		'GMR.DefineUnstuck(1648.584, 411.9414, -62.17652)',
		'GMR.DefineUnstuck(1658.287, 408.4176, -62.1934)',
		'GMR.DefineUnstuck(1668.493, 405.2478, -62.24202)',
		'GMR.DefineUnstuck(1677.794, 400.4655, -62.24329)',
		'GMR.DefineUnstuck(1687.039, 395.4861, -62.2447)',
		'GMR.DefineUnstuck(1696.308, 390.5085, -62.2492)',
		'GMR.DefineUnstuck(1705.54, 385.5507, -62.22373)',
		'GMR.DefineUnstuck(1713.755, 379.4111, -62.28043)',
		'GMR.DefineUnstuck(1721.312, 371.9521, -60.48624)',
		'GMR.DefineUnstuck(1728.621, 364.5318, -60.48615)',
		'GMR.DefineUnstuck(1735.955, 356.8433, -60.48592)',
		'GMR.DefineUnstuck(1743.145, 349.3066, -60.48025)',
		'GMR.DefineUnstuck(1748.366, 340.1612, -60.48395)',
		'GMR.DefineUnstuck(1753.791, 331.1858, -62.27743)',
		'GMR.DefineUnstuck(1759.167, 322.3862, -62.24387)',
		'GMR.DefineUnstuck(1764.01, 313.0743, -62.22815)',
		'GMR.DefineUnstuck(1767.066, 303.0399, -62.14096)',
		'GMR.DefineUnstuck(1768.64, 292.7012, -62.17621)',
		'GMR.DefineUnstuck(1770.374, 282.3456, -61.62038)',
		'GMR.DefineUnstuck(1772.148, 271.9965, -62.17634)',
		'GMR.DefineUnstuck(1773.954, 261.4611, -62.17634)',
		'GMR.DefineUnstuck(1775.696, 251.2983, -62.17634)',
		'GMR.DefineUnstuck(1775.645, 240.8035, -62.17634)',
		'GMR.DefineUnstuck(1775.118, 230.3171, -62.17634)',
		'GMR.DefineUnstuck(1774.142, 219.8628, -62.17634)',
		'GMR.DefineUnstuck(1772.951, 209.4312, -62.17634)',
		'GMR.DefineUnstuck(1771.459, 199.0413, -62.17634)',
		'GMR.DefineUnstuck(1768.31, 189.0289, -61.63406)',
		'GMR.DefineUnstuck(1765.037, 179.052, -62.19368)',
		'GMR.DefineUnstuck(1761.762, 169.0756, -62.15347)',
		'GMR.DefineUnstuck(1758.338, 159.1274, -62.24272)',
		'GMR.DefineUnstuck(1754.923, 149.2207, -62.27914)',
		'GMR.DefineUnstuck(1756.454, 139.6524, -62.2991)',
		'GMR.DefineUnstuck(1762.481, 131.0541, -62.2966)',
		'GMR.DefineUnstuck(1768.489, 122.4432, -62.29188)',
		'GMR.DefineUnstuck(1771.74, 112.8522, -62.2822)',
		'GMR.DefineUnstuck(1762.42, 103.6993, -62.27831)',
		'GMR.DefineUnstuck(1753.178, 98.81606, -62.27674)',
		'GMR.DefineUnstuck(1743.955, 93.76054, -62.27674)',
		'GMR.DefineUnstuck(1734.447, 82.76222, -62.27674)',
		'GMR.DefineUnstuck(1728.363, 74.20457, -62.27823)',
		'GMR.DefineUnstuck(1722.583, 66.07481, -62.27823)',
		'GMR.DefineUnstuck(1713.244, 58.2615, -62.28593)',
		'GMR.DefineUnstuck(1712.896, 58.00428, -62.28765)',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)

GMR.DefineQuest("Scourge", "WARLOCK", 1471, "|cFFFFAB48 The Binding |r", "Custom", 1711.589966, 57.167198, -62.205898, 5675, 1711.589966, 57.167198, -62.205898, 5675, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1471, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1705.527, 42.8714, -63.84325, 3) then 
				GMR.MeshTo(1705.527, 42.8714, -63.84325)
			else 
				local npc1 = GMR.GetObjectWithInfo({ id = 5676, rawType = 5, isAlive = true })
				local itemName = GetItemInfo(6284)
				if npc1 then
					if not GMR.InCombat() then
						GMR.TargetUnit(npc1)
						GMR.RunMacroText("/startattack")
						GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
					else
						GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
					end
				else
					GMR.Use(itemName)
					GMR.RunMacroText("/use Runes of Summoning")
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(1705.527, 42.8714, -63.84325, 150)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(5676)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		   END --- [ Warlock ][Level 10][Undead / Scourge] - Summon Voidwalker
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Warrior ][Level 10][Undead / Scourge] - Defensive Stance
----------------------------------------------------------------------------------------------------------------------------------

-- Defensive Stance 1 - Speak with Dillinger
GMR.DefineQuest("Scourge", "WARRIOR", 1818, "|cFFFFAB48 Speak with Dillinger |r", "TalkTo", 2254.760010, 238.445007, 33.716999, 2131, 2287.659912, 403.372009, 34.004799, 1496, 
	{}, 
	{
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
	} 
)

-- Defensive Stance 2 - Gather Key and Kill mob... ask marty
GMR.DefineQuest("Scourge", "WARRIOR", 1819, "|cFFFFAB48 Ulag the Cleaver |r", "Custom", 2287.659912, 403.372009, 34.004799, 1496, 2287.659912, 403.372009, 34.004799, 1496, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1819, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		'GMR.DefineQuestEnemyId(104593)',
		'GMR.DefineQuestEnemyId(176594)',
		'GMR_DefineCustomObjectID(104593)', -- Mausoleum Trigger
		'GMR_DefineCustomObjectID(176594)', -- Doors
		'GMR.DefineProfileCenter(2376.111084, 360.184937, 38.224148, 80)'
	}
)
-- Defensive Stance 3 - Speak with Coleman
GMR.DefineQuest("Scourge", "WARRIOR", 1820, "|cFFFFAB48 Speak with Coleman |r", "TalkTo", 2287.659912, 403.372009, 34.004799, 1496, 2262.260010, 244.257004, 33.716999, 1500, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1820, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	} 
)
----------------------------------------------------------------------------------------------------------------------------------
--  		   END --- [ Warrior ][Level 10][Undead / Scourge] - Defensive Stance
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Mage ][Level 10][Undead / Scourge] - STAFF
----------------------------------------------------------------------------------------------------------------------------------

-- Mage Staff 1 - Speak with Anastasia
GMR.DefineQuest("Scourge", "MAGE", 1881, "|cFFFFAB48 Speak with Anastasia |r", "TalkTo", 2256.810059, 233.218994, 41.198101, 2128, 1813.739990, 56.413799, -47.303799, 4568, 
	{}, 
	{
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	} 
)
-- Mage Staff 2 - 
GMR.DefineQuest("Scourge", "MAGE", 1882, "|cFFFFAB48 The Balnir Farmstead |r", "Gathering", 1813.739990, 56.413799, -47.303799, 4568, 1813.739990, 56.413799, -47.303799, 4568, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1882, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineCustomObjectID(102985)', -- Balnir Snapdragons
		'GMR.DefineProfileCenter(1969.574219, -463.934204, 34.626118, 30)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)
----------------------------------------------------------------------------------------------------------------------------------
--  		   END --- [ Mage ][Level 10][Undead / Scourge] - STAFF
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest( "Scourge", nil, 370, "|cFFFFAB48 At War With the Scarlet Crusade (PART 2) |r", "Custom", 2278.078125, 295.58682250977, 35.146984100342, 1515, 2278.078125, 295.58682250977, 35.146984100342, 1515, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(370, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1662, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(370, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1537, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(370, 3) then
			local enemy = GMR.GetObjectWithInfo({ id = 1536, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(1713.027, 58.79336, -62.28737, 50)',
		'GMR.DefineUnstuck(1720.438, 66.69907, -62.28059)',
		'GMR.DefineUnstuck(1731.078, 77.82211, -62.27915)',
		'GMR.DefineUnstuck(1738.335, 85.41023, -62.27658)',
		'GMR.DefineUnstuck(1745.593, 92.99835, -62.27658)',
		'GMR.DefineUnstuck(1752.742, 100.6877, -62.27808)',
		'GMR.DefineUnstuck(1759.821, 108.5556, -62.281)',
		'GMR.DefineUnstuck(1765.713, 117.1768, -62.284)',
		'GMR.DefineUnstuck(1767.562, 127.1452, -62.29151)',
		'GMR.DefineUnstuck(1763.153, 136.6026, -62.29594)',
		'GMR.DefineUnstuck(1757.595, 145.5357, -62.29748)',
		'GMR.DefineUnstuck(1754.172, 155.3549, -62.21753)',
		'GMR.DefineUnstuck(1755.513, 165.5702, -62.1677)',
		'GMR.DefineUnstuck(1760.889, 174.5649, -62.19343)',
		'GMR.DefineUnstuck(1766.016, 183.721, -62.18805)',
		'GMR.DefineUnstuck(1769.899, 193.4951, -61.6205)',
		'GMR.DefineUnstuck(1772.428, 203.6741, -62.17636)',
		'GMR.DefineUnstuck(1773.756, 214.0838, -62.17636)',
		'GMR.DefineUnstuck(1774.147, 224.5531, -62.17636)',
		'GMR.DefineUnstuck(1774.203, 235.0734, -62.17636)',
		'GMR.DefineUnstuck(1774.061, 245.5724, -62.17636)',
		'GMR.DefineUnstuck(1773.96, 256.072, -62.17636)',
		'GMR.DefineUnstuck(1773.776, 266.5697, -62.17636)',
		'GMR.DefineUnstuck(1772.035, 276.9083, -62.17636)',
		'GMR.DefineUnstuck(1769.657, 287.1351, -61.62031)',
		'GMR.DefineUnstuck(1767.178, 297.4031, -62.16741)',
		'GMR.DefineUnstuck(1764.403, 307.4635, -62.13385)',
		'GMR.DefineUnstuck(1761.49, 317.573, -62.22604)',
		'GMR.DefineUnstuck(1757.634, 327.2989, -62.27309)',
		'GMR.DefineUnstuck(1751.47, 335.8199, -60.48415)',
		'GMR.DefineUnstuck(1745.189, 344.2345, -60.48415)',
		'GMR.DefineUnstuck(1739.055, 352.756, -60.48158)',
		'GMR.DefineUnstuck(1732.963, 361.2566, -60.48453)',
		'GMR.DefineUnstuck(1726.045, 369.1052, -60.48453)',
		'GMR.DefineUnstuck(1718.182, 375.8069, -60.48594)',
		'GMR.DefineUnstuck(1709.756, 382.4179, -62.19976)',
		'GMR.DefineUnstuck(1701.431, 388.9191, -62.26366)',
		'GMR.DefineUnstuck(1693.33, 395.2302, -62.27746)',
		'GMR.DefineUnstuck(1684.24, 400.73, -62.27992)',
		'GMR.DefineUnstuck(1674.505, 404.6038, -62.26521)',
		'GMR.DefineUnstuck(1664.694, 408.291, -62.20417)',
		'GMR.DefineUnstuck(1654.77, 411.7221, -62.19234)',
		'GMR.DefineUnstuck(1644.876, 415.2365, -61.62047)',
		'GMR.DefineUnstuck(1635, 418.8627, -62.17598)',
		'GMR.DefineUnstuck(1625.146, 422.4806, -60.0067)',
		'GMR.DefineUnstuck(1616.196, 423.0614, -54.09937)',
		'GMR.DefineUnstuck(1605.699, 422.8046, -46.87791)',
		'GMR.DefineUnstuck(1596.405, 423.3349, -46.37612)',
		'GMR.DefineUnstuck(1596.491, 426.1476, -46.35406)',
		'GMR.DefineUnstuck(1596.681, 435.3436, -46.33564)',
		'GMR.DefineUnstuck(1596.851, 438.1735, -46.33564)',
		'GMR.DefineUnstuck(1614.278, 437.412, -39.63515)',
		'GMR.DefineUnstuck(1624.609, 437.4623, -34.8861)',
		'GMR.DefineUnstuck(1629.921, 437.5893, -34.26337)',
		'GMR.DefineUnstuck(1629.19, 453.3147, -30.96634)',
		'GMR.DefineUnstuck(1629.336, 470.615, -24.02083)',
		'GMR.DefineUnstuck(1629.663, 479.3854, -22.86771)',
		'GMR.DefineUnstuck(1642.383, 478.5519, -18.52233)',
		'GMR.DefineUnstuck(1656.745, 478.7653, -12.83082)',
		'GMR.DefineUnstuck(1664.971, 479.0427, -11.89041)',
		'GMR.DefineUnstuck(1665.28, 493.256, -11.53609)',
		'GMR.DefineUnstuck(1665.105, 505.8544, -12.72852)',
		'GMR.DefineUnstuck(1664.899, 516.3524, -12.91658)',
		'GMR.DefineUnstuck(1664.692, 526.8713, -12.54228)',
		'GMR.DefineUnstuck(1664.264, 538.581, -11.38892)',
		'GMR.DefineUnstuck(1662.588, 550.8851, -15.10547)',
		'GMR.DefineUnstuck(1662.177, 561.3763, -16.40866)',
		'GMR.DefineUnstuck(1662.945, 571.7869, -15.58564)',
		'GMR.DefineUnstuck(1667.383, 581.2916, -12.62303)',
		'GMR.DefineUnstuck(1674.299, 589.1218, -9.526031)',
		'GMR.DefineUnstuck(1682.086, 596.0247, -7.079781)',
		'GMR.DefineUnstuck(1688.561, 604.3261, -1.659621)',
		'GMR.DefineUnstuck(1690.276, 614.5215, 2.856767)',
		'GMR.DefineUnstuck(1688.093, 624.611, 6.061492)',
		'GMR.DefineUnstuck(1682.29, 633.3853, 10.43936)',
		'GMR.DefineUnstuck(1674.627, 640.4525, 13.54236)',
		'GMR.DefineUnstuck(1665.198, 645.0164, 18.11449)',
		'GMR.DefineUnstuck(1655.207, 648.2351, 22.86804)',
		'GMR.DefineUnstuck(1644.772, 648.8699, 25.64111)',
		'GMR.DefineUnstuck(1634.312, 647.9522, 29.62127)',
		'GMR.DefineUnstuck(1623.845, 647.2534, 34.61678)',
		'GMR.DefineUnstuck(1613.801, 650.1248, 36.51628)',
		'GMR.DefineUnstuck(1604.975, 655.5977, 39.71554)',
		'GMR.DefineUnstuck(1598.265, 663.6658, 43.95945)',
		'GMR.DefineUnstuck(1592.687, 672.5284, 48.30902)',
		'GMR.DefineUnstuck(1591.011, 682.7299, 51.38053)',
		'GMR.DefineUnstuck(1592.562, 693.0954, 54.67151)',
		'GMR.DefineUnstuck(1595.744, 702.9152, 58.89167)',
		'GMR.DefineUnstuck(1599.484, 712.9053, 62.4038)',
		'GMR.DefineUnstuck(1605.346, 721.4932, 65.87614)',
		'GMR.DefineUnstuck(1613.771, 725.353, 68.75859)',
		'GMR.DefineUnstuck(1622.887, 730.5231, 72.74375)',
		'GMR.DefineUnstuck(1633.056, 732.3347, 76.20898)',
		'GMR.DefineUnstuck(1643.53, 732.052, 78.69986)',
		'GMR.DefineUnstuck(1654.008, 731.3664, 80.34671)',
		'GMR.DefineUnstuck(1664.488, 730.7297, 79.47942)',
		'GMR.DefineUnstuck(1674.896, 729.4719, 77.72263)',
		'GMR.DefineUnstuck(1683.933, 727.5088, 76.32156)',
		'GMR.DefineUnstuck(1692.756, 727.0591, 70.58398)',
		'GMR.DefineUnstuck(1710.535, 729.276, 61.8245)',
		'GMR.DefineUnstuck(1723.365, 738.3799, 54.27525)',
		'GMR.DefineUnstuck(1732.98, 742.2721, 50.7918)',
		'GMR.DefineUnstuck(1743.29, 744.2601, 50.24187)',
		'GMR.DefineUnstuck(1753.412, 743.2583, 50.82055)',
		'GMR.DefineUnstuck(1761.014, 736.0135, 48.4245)',
		'GMR.DefineUnstuck(1766.5, 727.2908, 46.15947)',
		'GMR.DefineUnstuck(1770.319, 717.5156, 45.22605)',
		'GMR.DefineUnstuck(1773.413, 708.5879, 45.37674)',
		'GMR.DefineUnstuck(1778.693, 698.1272, 45.44062)',
		'GMR.DefineUnstuck(1780.439, 684.3096, 44.29433)',
		'GMR.DefineUnstuck(1779.975, 683.4056, 43.94606)',
		'GMR.DefineProfileCenter(1842.212, 820.9021, 26.49645, 80)',
		'GMR.DefineProfileCenter(1851.293, 708.8365, 39.52066, 80)',
		'GMR.DefineProfileCenter(1786.498, 670.7971, 43.28187, 80)',
		'GMR.DefineProfileCenter(1786.5, 597.616, 39.724, 80)',
		'GMR.DefineProfileCenter(1821.265, 658.2141, 41.60354, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1537)',
		'GMR.DefineQuestEnemyId(1536)',
		'GMR.DefineQuestEnemyId(1662)'
	}
)

GMR.DefineQuest( "Scourge", nil, 374, "|cFFFFAB48 Proof of Demise |r", "Custom", 2270.7014160156, 279.99826049805, 35.139709472656, 1652, 2270.7014160156, 279.99826049805, 35.139709472656, 1652, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(374, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1795.826294, 673.497986, 41.720463, 120)',
		'GMR.DefineProfileCenter(1817.275146, 812.070801, 32.623741, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1536)',
		'GMR.DefineQuestEnemyId(1537)',
		'GMR.DefineQuestEnemyId(1662)'
	}
)

GMR.DefineQuest( "Scourge", nil, 426, "|cFFFFAB48 The Mills Overrun |r", "Custom", 2287.6625976563, 403.37197875977, 33.921745300293, 1496, 2287.6625976563, 403.37197875977, 33.921745300293, 1496, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(426, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1522, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(426, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1520, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		[[if not GMR.Frames.Rand002 then
			GMR.Print("Frame created")
			GMR.Frames.Rand002 = CreateFrame("frame")
			GMR.Frames.Rand002:SetScript("OnUpdate", function(self)
				if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 426 then
					if GMR.IsExecuting() and GetItemCount(2839) >= 1 and not GMR.IsQuestActive(361) then
						GMR.Use(2839)
						GMR.RunMacroText("/use A Letter to Yvette")
						if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
							if QuestFrameAcceptButton:IsShown() then
								QuestFrameAcceptButton:Click()
							else
								QuestFrameAcceptButton:Click()
							end
						end
					end
				else 
					self:SetScript("OnUpdate", nil); GMR.Frames.Rand002 = nil; GMR.Print("Frame deleted")
				end
			end)
		end]],
		'GMR.DefineProfileCenter(2604.746, 893.2764, 106.9091, 80)',
		'GMR.DefineProfileCenter(2709.957, 870.9642, 112.0549, 80)',
		'GMR.DefineProfileCenter(2777.39, 884.0813, 112.2406, 80)',
		'GMR.DefineProfileCenter(2800.812, 991.0462, 114.596, 80)',
		'GMR.DefineProfileCenter(2703.126, 1017.271, 110.4341, 80)',
		'GMR.DefineProfileCenter(2808.644, 1036.429, 111.2991, 80)',
		'GMR.DefineProfileCenter(2880.22, 1077.03, 109.177, 80)',
		'GMR.DefineProfileCenter(2898.156, 1001.858, 109.1991, 80)',
		'GMR.DefineProfileCenter(2873.403, 916.9211, 118.1423, 80)',
		'GMR.DefineProfileCenter(2959.921, 921.0939, 113.5265, 80)',
		'GMR.DefineProfileCenter(2961.673, 983.6396, 116.0472, 80)',
		'GMR.DefineProfileCenter(2825.534, 973.403, 117.1827, 80)',
		'GMR.DefineProfileCenter(2685.066, 936.6003, 111.1764, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1522)',
		'GMR.DefineQuestEnemyId(1520)'
	}
)

GMR.DefineQuest( "Scourge", nil, 361, "|cFFFFAB48 A Letter Undelivered - FARMITEM |r - FarmItemAndAcceptQuest", "Grinding", 2657.735, 948.2593, 113.6442, { 1522, 2839 }, 2252.830078, 250.671005, 34.343700, 1560, 
	{},
	{
		[[if not GMR.Frames.Rand003 then
			GMR.Print("Frame created")
			GMR.Frames.Rand003 = CreateFrame("frame")
			GMR.Frames.Rand003:SetScript("OnUpdate", function(self)
				if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 361 then
					if GMR.IsExecuting() and GetItemCount(2839) >= 1 and not GMR.IsQuestActive(361) then
						GMR.Use(2839)
						GMR.RunMacroText("/use A Letter to Yvette")
						if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
							if QuestFrameAcceptButton:IsShown() then
								QuestFrameAcceptButton:Click()
							else
								QuestFrameAcceptButton:Click()
							end
						end
					end
				else 
					self:SetScript("OnUpdate", nil); GMR.Frames.Rand003 = nil; GMR.Print("Frame deleted")
				end
			end)
		end]],
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2657.735, 948.2593, 113.6442, 80)',
		'GMR.DefineProfileCenter(2604.746, 893.2764, 106.9091, 80)',
		'GMR.DefineProfileCenter(2709.957, 870.9642, 112.0549, 80)',
		'GMR.DefineProfileCenter(2777.39, 884.0813, 112.2406, 80)',
		'GMR.DefineProfileCenter(2800.812, 991.0462, 114.596, 80)',
		'GMR.DefineProfileCenter(2703.126, 1017.271, 110.4341, 80)',
		'GMR.DefineProfileCenter(2808.644, 1036.429, 111.2991, 80)',
		'GMR.DefineProfileCenter(2880.22, 1077.03, 109.177, 80)',
		'GMR.DefineProfileCenter(2898.156, 1001.858, 109.1991, 80)',
		'GMR.DefineProfileCenter(2873.403, 916.9211, 118.1423, 80)',
		'GMR.DefineProfileCenter(2959.921, 921.0939, 113.5265, 80)',
		'GMR.DefineProfileCenter(2961.673, 983.6396, 116.0472, 80)',
		'GMR.DefineProfileCenter(2825.534, 973.403, 117.1827, 80)',
		'GMR.DefineProfileCenter(2685.066, 936.6003, 111.1764, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1522)',
		'GMR.DefineQuestEnemyId(1520)'
	}
)

GMR.DefineQuest( "Scourge", nil, 362, "|cFFFFAB48 The Haunted Mills |r", "Custom", 2262.2609863281, 244.25694274902, 33.633995056152, 1500, 2262.2609863281, 244.25694274902, 33.633995056152, 1500, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(362, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2609.080078, 894.034973, 107.834000, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1657)'
	}
)

GMR.DefineQuest( "Scourge", nil, 398, "|cFFFFAB48 WANTED: Maggot Eye |r", "Custom", 2286.015, 288.7661, 35.18226, 711, 2278.080078, 295.587006, 35.330101, 1515, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(398, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2910.376, 381.9073, 31.48436, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1753)'
	}
)

GMR.DefineQuest( "Scourge", nil, 354, "|cFFFFAB48 Deaths in the Family |r", "Custom", 2262.2609863281, 244.25694274902, 33.633995056152, 1500, 2262.2609863281, 244.25694274902, 33.633995056152, 1500, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(354, 1) then
			GMR.SetQuestingState(nil);
			local npc = GMR.GetObjectWithInfo({ id = 1654, rawType = 5 })
			if npc then
				GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
			else
				GMR.SetQuestingState("Idle")
			end

		elseif not GMR.Questing.IsObjectiveCompleted(354, 2) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(2751.108, 799.7998, 114.3675, 15) then 
				GMR.MeshTo(2751.108, 799.7998, 114.3675)
			else 
				local object = GMR.GetObjectWithInfo({ id = 1655, rawType = 5 })
				if object then
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
				end
			end
		elseif not GMR.Questing.IsObjectiveCompleted(354, 3) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(2825.789, 1053.666, 110.481, 35) then 
				GMR.MeshTo(2825.789, 1053.666, 110.481)
			else 
				local object = GMR.GetObjectWithInfo({ id = 1656, rawType = 5 })
				if object then
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2948.186, 996.6116, 110.7314, 200)',
		'GMR.DefineProfileCenter(2751.108, 799.7998, 114.3675, 120)',
		'GMR.DefineProfileCenter(2825.789, 1053.666, 110.481, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1654)',
		'GMR.DefineQuestEnemyId(1655)',
		'GMR.DefineQuestEnemyId(1656)'
	}
)

GMR.DefineQuest( "Scourge", nil, 355, "|cFFFFAB48 Speak with Sevren |r", "TalkTo", 2262.2609863281, 244.25694274902, 33.633995056152, 1500, 2305.9111328125, 265.16384887695, 38.669979095459, 1499, 
	{}, 
	{
		'GMR.DefineProfileCenter(2305.9111328125, 265.16384887695, 38.669979095459, 130)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	} 
)


GMR.DefineQuest( "Scourge", nil, 356, "|cFFFFAB48 Rear Guard Patrol |r", "Custom", 2022.3259277344, 74.046005249023, 36.44637298584, 1495, 2022.3259277344, 74.046005249023, 36.44637298584, 1495, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(356, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 1529, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(356, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 1532, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2008.301636, -357.398254, 35.453186, 80)',
		'GMR.DefineProfileCenter(2012.174561, -466.312195, 34.716946, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)',
		'GMR.DefineQuestEnemyId(1532)',
		'GMR.DefineQuestEnemyId(1529)'
	}
)

GMR.DefineQuest( "Scourge", nil, nil, "|cFFFFAB48 Grinding: Tirisfal Glades - Level 14 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 14 },
	{},
	{
		'GMR.DefineProfileCenter(2368.3471679688, -602.12536621094, 73.41040802002, 80)',
		'GMR.DefineProfileCenter(2304.46875, -649.97552490234, 75.942199707031, 80)',
		'GMR.DefineProfileCenter(2260.6694335938, -720.70617675781, 67.278289794922, 80)',
		'GMR.DefineProfileCenter(2177.0317382813, -752.23889160156, 74.741188049316, 80)',
		'GMR.DefineProfileCenter(2208.5407714844, -830.09094238281, 80.322883605957, 80)',
		'GMR.DefineProfileCenter(2255.48828125, -854.0263671875, 75.058532714844, 80)',
		'GMR.DefineProfileCenter(2251.9091796875, -923.93829345703, 75.76001739502, 80)',
		'GMR.DefineProfileCenter(2165.1728515625, -924.42376708984, 87.947570800781, 80)',
		'GMR.DefineProfileCenter(2110.2526855469, -888.74096679688, 108.94683837891, 80)',
		'GMR.DefineProfileCenter(2262.0251464844, -965.58227539063, 78.336135864258, 80)',
		'GMR.DefineProfileCenter(2329.296875, -920.037109375, 72.756759643555, 80)',
		'GMR.DefineProfileCenter(2337.7199707031, -1005.4998779297, 74.222763061523, 80)',
		'GMR.DefineProfileCenter(2330.302734375, -1070.5314941406, 83.633193969727, 80)',
		'GMR.DefineProfileCenter(2399.3647460938, -1035.5718994141, 84.536056518555, 80)',
		'GMR.DefineProfileCenter(2455.3776855469, -1014.178527832, 82.984451293945, 80)',
		'GMR.DefineProfileCenter(2539.8022460938, -1045.6501464844, 99.115539550781, 80)',
		'GMR.DefineProfileCenter(2397.0378417969, -1007.3502197266, 80.20467376709, 80)',
		'GMR.DefineProfileCenter(2364.2314453125, -910.83966064453, 68.852432250977, 80)',
		'GMR.DefineProfileCenter(2367.7265625, -828.73413085938, 72.199974060059, 80)',
		'GMR.DefineProfileCenter(2413.0356445313, -783.99560546875, 68.617950439453, 80)',
		'GMR.DefineProfileCenter(2437.646484375, -716.24133300781, 67.796165466309, 80)',
		'GMR.DefineProfileCenter(2495.8041992188, -707.66387939453, 66.743316650391, 80)',
		'GMR.DefineProfileCenter(2571.0476074219, -733.95806884766, 66.546562194824, 80)',
		'GMR.DefineProfileCenter(2458.1940917969, -655.24957275391, 71.194725036621, 80)',
		'GMR.DefineProfileCenter(2380.7514648438, -611.58331298828, 71.655548095703, 80)',
		'GMR.DefineAreaBlacklist(2524.279, -897.9366, 56.0958, 75)',
		'GMR.DefineAreaBlacklist(2551.068, -898.0433, 56.49329, 75)',
		'GMR.DefineAreaBlacklist(2589.468, -903.402, 60.28486, 75)',
		'GMR.DefineAreaBlacklist(2552.836, -912.4724, 56.21835, 75)',
		'GMR.DefineQuestEnemyId(1555)',
		'GMR.DefineQuestEnemyId(1554)',
		'GMR.DefineQuestEnemyId(1549)',
		-- Brill
		'GMR.DefineSellVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineRepairVendor(2246.331, 308.2395, 35.18927, 2137)',
		'GMR.DefineGoodsVendor(2269.51, 244.9443, 34.25709, 5688)',
		'GMR.DefineAmmoVendor(2253.34, 270.2204, 34.26433, 2134)',
		'GMR.DefineProfileMailbox(2237.687, 254.8723, 34.11674, 143990)',
		'GMR.DefineHearthstoneBindLocation(2269.510010, 244.944000, 34.340199, 5688)'
	}
)






























elseif UnitRace("player") == "Orc" or UnitRace("player") == "Troll" then



----------------------------------------------------------------------------------------------------------------------------------
--  		             START: 1-14 ORC/TROLL Starting Area
----------------------------------------------------------------------------------------------------------------------------------


GMR.DefineQuest({ "Orc", "Troll" }, nil, 4641, "|cFFFFAB48 Your Place In The World |r", "Custom", -607.43438720703, -4251.326171875, 38.956035614014, 10176, -600.13208007813, -4186.1938476563, 41.089191436768, 3143, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(4641, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineProfileCenter(-600.13208007813, -4186.1938476563, 41.089191436768, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 788, "|cFFFFAB48 Cutting Teeth |r", "Custom", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -600.13208007813, -4186.1938476563, 41.089191436768, 3143, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(788, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-435.49945068359, -4285.9077148438, 42.358001708984, 150)',
		'GMR.DefineProfileCenter(-405.07507324219, -4373.703125, 39.862857818604, 150)',
		'GMR.DefineProfileCenter(-321.002, -4275.086, 58.63735, 150)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(3098)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- Level 2 --- Class Quests
----------------------------------------------------------------------------------------------------------------------------------
-- Class Quests

-- Rogue
GMR.DefineQuest("Orc", "ROGUE", 3088, "|cFFFFAB48 Encrypted Parchment |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -588.703003, -4144.939941, 41.103298, 3155, {}, 
	{
		'GMR.DefineProfileCenter(-588.703003, -4144.939941, 41.103298, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)
GMR.DefineQuest("Troll", "ROGUE", 3083, "|cFFFFAB48 Encrypted Tablet |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -588.703003, -4144.939941, 41.103298, 3155, {}, 
	{
		'GMR.DefineProfileCenter(-588.703003, -4144.939941, 41.103298, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)

-- Hunter
GMR.DefineQuest("Orc", "HUNTER", 3087, "|cFFFFAB48 Etched Parchment |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -635.461975, -4227.520020, 38.370998, 3154, {}, 
	{
		'GMR.DefineProfileCenter(-635.461975, -4227.520020, 38.370998, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)
GMR.DefineQuest("Troll", "HUNTER", 3082, "|cFFFFAB48 Etched Tablet", "TalkTo |r", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -635.461975, -4227.520020, 38.370998, 3154, {}, 
	{
		'GMR.DefineProfileCenter(-635.461975, -4227.520020, 38.370998, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)

-- Mage
GMR.DefineQuest("Troll", "MAGE", 3086, "|cFFFFAB48 Glyphic Tablet |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -625.296021, -4210.169922, 38.296600, 5884, {}, 
	{
		'GMR.DefineProfileCenter(-625.296021, -4210.169922, 38.296600, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)

-- Priest
GMR.DefineQuest("Troll", "PRIEST", 3085, "|cFFFFAB48 Hallowed Tablet", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -617.395020, -4202.399902, 38.296600, 3707, {}, 
	{
		'GMR.DefineProfileCenter(-617.395020, -4202.399902, 38.296600, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)

-- Shaman
GMR.DefineQuest("Orc", "SHAMAN", 3089, "|cFFFFAB48 Rune-Inscribed Parchment |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -623.939026, -4203.879883, 38.428501, 3157, {}, 
	{
		'GMR.DefineProfileCenter(-623.939026, -4203.879883, 38.428501, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)
GMR.DefineQuest("Troll", "SHAMAN", 3084, "|cFFFFAB48 Rune-Inscribed Parchment |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -623.939026, -4203.879883, 38.428501, 3157, {}, 
	{
		'GMR.DefineProfileCenter(-623.939026, -4203.879883, 38.428501, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	} 
)

-- Warrior
GMR.DefineQuest("Orc", "WARRIOR", 2383, "|cFFFFAB48 Simple Parchment |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -639.343994, -4230.189941, 38.560501, 3153, {}, 
	{
		'GMR.DefineProfileCenter(-639.343994, -4230.189941, 38.560501, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	} 
)
GMR.DefineQuest("Troll", "WARRIOR", 3065, "|cFFFFAB48 Simple Tablet |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -639.343994, -4230.189941, 38.560501, 3153, {}, 
	{
		'GMR.DefineProfileCenter(-639.343994, -4230.189941, 38.560501, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	} 
)

-- Warlock
GMR.DefineQuest("Orc", "WARLOCK", 3090, "|cFFFFAB48 Tainted Parchment |r", "TalkTo", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -606.874023, -4111.870117, 43.028000, 3156, {}, 
	{
		'GMR.DefineProfileCenter(-606.874023, -4111.870117, 43.028000, 150)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	} 
)
----------------------------------------------------------------------------------------------------------------------------------
--  		   END - Level 2 Class Quests
----------------------------------------------------------------------------------------------------------------------------------


GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFE25FFF PICKUP |r |cFF1E90FE Accept All ORC/TROLL Starting Quests |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 789, -600.13208007813, -4186.1938476563, 41.089191436768, 3143 },
		{ 4402, -561.62847900391, -4221.798828125, 41.590442657471, 9796 },
		{ 790, -397.76095581055, -4108.986328125, 50.20450592041, 3287 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 789, "|cFFFFAB48 Sting of the Scorpid", "Custom", -600.13208007813, -4186.1938476563, 41.089191436768, 3143, -600.13208007813, -4186.1938476563, 41.089191436768, 3143,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(789, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineQuestEnemyId(3124)',
		'GMR.DefineProfileCenter(-388.1318359375, -4112.0122070313, 50.13232421875, 150)',
		'GMR.DefineProfileCenter(-308.18420410156, -4167.4169921875, 53.210670471191, 100)',
		'GMR.DefineProfileCenter(-275.52014160156, -4189.814453125, 52.801456451416, 50)',
		'GMR.DefineProfileCenter(-564.70910644531, -4106.791015625, 71.968170166016, 100)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 790, "|cFFFFAB48 Sarkoth |r", "Custom", -397.76095581055, -4108.986328125, 50.20450592041, 3287, -397.76095581055, -4108.986328125, 50.20450592041, 3287, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(790, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineQuestEnemyId(3281)',
		'GMR.DefineProfileCenter(-559.05737304688, -4102.177734375, 72.355026245117, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
			-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 804, "|cFFFFAB48 Sarkoth Part 2 |r", "TalkTo", -397.76095581055, -4108.986328125, 50.20450592041, 3287, -600.13208007813, -4186.1938476563, 41.089191436768, 3143, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.SetChecked("Mount", true)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 4402, "|cFFFFAB48 Cactus Apple Surprise |r", "Custom", -561.62847900391, -4221.798828125, 41.590507507324, 9796, -561.62847900391, -4221.798828125, 41.590507507324, 9796,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(4402, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-557.399841, -4289.202637, 37.470306, 50)',
		'GMR.DefineProfileCenter(-487.868225, -4291.974121, 43.158340, 50)',
		'GMR.DefineProfileCenter(-406.954803, -4280.130859, 45.829395, 50)',
		'GMR.DefineProfileCenter(-299.333038, -4332.531250, 55.674145, 50)',
		'GMR.DefineProfileCenter(-325.371155, -4394.829102, 58.277267, 50)',
		'GMR.DefineProfileCenter(-489.205078, -4462.878906, 51.504410, 50)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(171938)',
		'GMR_DefineCustomObjectID(171938)'
	}
)
GMR_DefineQuest({ "Orc", "Troll" }, nil, 5441, "Lazy Peons", "UseItemOnObjects", -611.58654785156, -4322.08203125, 40.009532928467, 11378, -611.58654785156, -4322.08203125, 40.009532928467, 11378,
	{
		16114, 10556, 5, 3, 1024
	},
	{
		'GMR_DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR_DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR_DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR_DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',

		'GMR_DefineCenter(-628.14660644531, -4339.578125, 41.70202255249, 50)',
		'GMR_DefineCenter(-762.30438232422, -4322.177734375, 46.15796661377, 50)',
		'GMR_DefineCenter(-771.30871582031, -4202.9389648438, 44.650329589844, 50)',
		'GMR_DefineCenter(-754.10675048828, -4144.8388671875, 38.310493469238, 50)',
		'GMR_DefineCenter(-506.46942138672, -4466.548828125, 54.233722686768, 50)',
		'GMR_DefineCenter(-330.55725097656, -4435.21484375, 54.811470031738, 50)',
		'GMR_DefineCenter(-320.77935791016, -4126.3071289063, 51.678733825684, 50)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Shaman ][Level 4][Orc + Troll] - Earth Totem
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1516, "|cFFFFAB48 Call of Earth Part 1 (Orc + Troll) |r", "Custom", -630.01971435547, -4204.5517578125, 38.133892059326, 5887, -630.01971435547, -4204.5517578125, 38.133892059326, 5887, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1516, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-559.05737304688, -4102.177734375, 72.355026245117, 60)',
		'GMR.DefineProfileCenter(-142.91697692871, -4365.8168945313, 67.17163848877, 30)',
		'GMR.DefineProfileCenter(-131.43783569336, -4327.3779296875, 65.778747558594, 30)',
		'GMR.DefineProfileCenter(-103.20240020752, -4300.28515625, 61.52156829834, 30)',
		'GMR.DefineProfileCenter(-52.084041595459, -4334.314453125, 68.202857971191, 30)',
		'GMR.DefineProfileCenter(-43.695167541504, -4274.4790039063, 68.317543029785, 30)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
			-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(3102)',
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1517, "|cFFE25FFF Call of Earth Part 2 (Orc + Troll) |r", "Custom", -630.01971435547, -4204.5517578125, 38.134212493896, 5887, -877.78985595703, -4290.3642578125, 72.623100280762, 5891,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1517, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-887.736023, -4302.861328, 73.804642, 3) then 
				GMR.MeshTo(-887.736023, -4302.861328, 73.804642)
			else 
				local itemName = GetItemInfo(6635)
				GMR.Use(itemName)
				GMR.RunMacroText("/use Earth Sapta")
			end
		end
	]]},	
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-887.736023, -4302.861328, 73.804642, 60)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1518, "|cFFE25FFF Call of Earth Part 3 (Orc + Troll) |r", "Custom", -877.78985595703, -4290.3642578125, 72.623100280762, 5891, -630.01971435547, -4204.5517578125, 38.134143829346, 5887, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	} 
)
----------------------------------------------------------------------------------------------------------------------------------
--  			 END --- [ Shaman ][Level 4][Orc + Troll] - Earth Totem
----------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Warlock ][Level 4][Orc] - Summon Imp
----------------------------------------------------------------------------------------------------------------------------------
GMR.DefineQuest("Orc", "WARLOCK", 1485, "|cFFFFAB48 Vile Familiars - Summon Imp WARLOCK |r", "Custom", -624.023987, -4214.410156, 38.217300, 5765, -624.023987, -4214.410156, 38.217300, 5765, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1485, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineProfileCenter(-216.229019, -4343.848145, 65.238342, 50)',
		'GMR.DefineProfileCenter(-214.007217, -4350.105469, 64.876472, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(3101)'
	}
)

GMR.DefineQuest("Orc", "WARLOCK", 1499, "|cFFFFAB48 Vile Familiars - Summon Imp WARLOCK Part 2 |r", "TalkTo", -624.023987, -4214.410156, 38.217300, 5765, -629.052002, -4228.060059, 38.233398, 3145, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1499, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.DefineProfileCenter(-216.229019, -4343.848145, 65.238342, 50)',
		'GMR.DefineProfileCenter(-214.007217, -4350.105469, 64.876472, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(3101)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  			 END --- [ Warlock ][Level 4][Orc] - Summon Imp
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest("Orc", "WARLOCK", nil, 6394, "|cFFFFAB48 Thazz'ril's Pick |r", "Custom", -611.58654785156, -4322.08203125, 40.009471893311, 11378, -611.58654785156, -4322.08203125, 40.009471893311, 11378, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(6394, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(-134.925232, -4347.992676, 68.033661, 10) then 
				GMR.MeshTo(-134.925232, -4347.992676, 68.033661)
			else
				local object = GMR.GetObjectWithInfo({ id = 178087, rawType = 8 })
				if object then 
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
				else
					GMR.SetQuestingState("Idle")
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-134.925232, -4347.992676, 68.033661, 120)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(178087)',
		'GMR_DefineCustomObjectID(178087)'
	}
)

if select(2, UnitClass("player")) ~= "WARLOCK" then
GMR.DefineQuest({ "Orc", "Troll" }, nil, 792, "Vile Familiars", "Custom", -629.052002, -4228.060059, 38.233398, 3145, -629.052002, -4228.060059, 38.233398, 3145, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(792, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-225.83293151855, -4328.4345703125, 65.140754699707, 60)',
		'GMR.DefineProfileCenter(-217.93112182617, -4376.2509765625, 63.370704650879, 60)',
		'GMR.DefineProfileCenter(-207.10675048828, -4405.6220703125, 64.797996520996, 60)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(3101)'
	}
)
end

GMR.DefineQuest({ "Orc", "Troll" }, nil, 794, "|cFFFFAB48 Burning Blade Medallion |r", "Custom", -629.05236816406, -4228.0581054688, 38.150356292725, 3145, -629.05236816406, -4228.0581054688, 38.150356292725, 3145,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(794, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-58.192409515381, -4219.0952148438, 62.317226409912, 50)',
		'GMR.DefineAreaBlacklist(-65.556587219238, -4318.1752929688, 62.888019561768, 3)',
		'GMR.DefineAreaBlacklist(-73.161933898926, -4320.1625976563, 64.95972442627, 10)',
		'GMR.DefineAreaBlacklist(-13.888278961182, -4244.7407226563, 64.328605651855, 5)',
		'GMR.DefineAreaBlacklist(-26.787548065186, -4216.2387695313, 61.870399475098, 10)',
		'GMR.DefineAreaBlacklist(-105.74465942383, -4214.6293945313, 54.474857330322, 10)',
		'GMR.DefineAreaBlacklist(-102.12308502197, -4230.3232421875, 47.827144622803, 20)',
		'GMR.DefineAreaBlacklist(-128.53616333008, -4229.7177734375, 57.305107116699, 10)',
		'GMR.DefineAreaBlacklist(-146.71231079102, -4245.9262695313, 58.741107940674, 10)',
		'GMR.DefineAreaBlacklist(-148.13395690918, -4260.83984375, 60.811092376709, 10)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(3183)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 805, "|cFFFFAB48 Report to Sen'jin Village |r", "Custom", -629.05236816406, -4228.0581054688, 38.150291442871, 3145, -825.63604736328, -4920.7573242188, 19.359380722046, 3188,
	{[[
		if not GMR.Questing.IsObjectiveCompleted(805, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-825.63604736328, -4920.7573242188, 19.359380722046, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             GRIND: Level 7
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFFFAB48 Grinding: Level 7 - Durotar |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 7 },
	{
		'GMR.DefineProfileCenter(-424.78457641602, -4091.224609375, 49.355751037598, 50)',
		'GMR.DefineProfileCenter(-364.03665161133, -4096.4282226563, 50.766654968262, 50)',
		'GMR.DefineProfileCenter(-312.17037963867, -4155.4536132813, 53.244571685791, 50)',
		'GMR.DefineProfileCenter(-242.92324829102, -4202.4067382813, 61.334133148193, 50)',
		'GMR.DefineProfileCenter(-237.72958374023, -4280.6015625, 62.751251220703, 50)',
		'GMR.DefineProfileCenter(-577.73229980469, -4103.47265625, 71.962905883789, 60)',
		'GMR.DefineQuestEnemyId(3124)',
		'GMR.DefineQuestEnemyId(3281)',
		'GMR.DefineQuestEnemyId(3101)',
		-- Orc/Troll Starting Area Vendors
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             END: 1-7 Troll / Orc Starting Area
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "Senjin Village - Pickup ALL Quests", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 823, -825.63604736328, -4920.7573242188, 19.359380722046, 3188 }, -- Report to Orgnil (Requires Report to Senjin Village)
		-- { 818, -813.994140625, -4920.5659179688, 19.353393554688, 3304 },   -- A Solvent Spirit
		{ 817, -797.52288818359, -4921.1704101563, 22.87388420105, 3194 },  -- Practical Prey
		{ 786, -842.18194580078, -4845.75, 21.831750869751, 3140 },   		-- Thwarting Kolkar Aggression
		{ 2161, -599.44940185547, -4715.318359375, 35.146488189697, 6786 }  -- A Peon's Burden
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.SkipTurnIn(false)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 823, "|cFFFFAB48 Report to Orgnil |r", "TalkTo", -825.63604736328, -4920.7573242188, 19.359380722046, 3188, 287.26541137695, -4724.8837890625, 13.131202697754, 3142, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 2161, "|cFFFFAB48 A Peon's Burden |r", "TalkTo", -599.44940185547, -4715.318359375, 35.146488189697, 6786, 340.36209106445, -4686.2875976563, 16.458362579346, 6928, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFFFAB48 Razor Hill - Mass Pickup ALL Quests |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 784, 274.98779296875, -4709.3022460938, 17.489046096802, 3139 }, 	-- Vanquish the Betrayers
		{ 837, 274.98779296875, -4709.3022460938, 17.489046096802, 3139 }, 	-- Encroachment
		{ 815, 312.09875488281, -4664.9516601563, 16.35396194458, 3191 },   -- Break a Few Eggs
		{ 791, 384.74282836914, -4600.1323242188, 76.088539123535, 3147 }  -- Carry Your Weight
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFFFAB48 Grinding: Level 10 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 10 },
	{
		'GMR.DefineProfileCenter(73.26016998291, -4694.4184570313, 33.896457672119, 80)',
		'GMR.DefineProfileCenter(101.46932983398, -4813.0361328125, 15.177879333496, 80)',
		'GMR.DefineProfileCenter(47.536350250244, -4862.1293945313, 17.780578613281, 80)',
		'GMR.DefineProfileCenter(125.72813415527, -4903.5678710938, 14.652051925659, 80)',
		'GMR.DefineProfileCenter(203.33804321289, -4904.28125, 16.08678817749, 80)',
		'GMR.DefineProfileCenter(260.80133056641, -4925.4956054688, 20.071517944336, 80)',
		'GMR.DefineProfileCenter(408.38317871094, -4892.2817382813, 20.70272064209, 80)',
		'GMR.DefineProfileCenter(477.94277954102, -4837.5009765625, 29.95373916626, 80)',
		'GMR.DefineProfileCenter(580.86016845703, -4810.69921875, 25.654579162598, 80)',
		'GMR.DefineProfileCenter(660.50622558594, -4823.6440429688, 26.609203338623, 80)',
		'GMR.DefineProfileCenter(656.96704101563, -4873.5795898438, 25.387760162354, 80)',
		'GMR.DefineProfileCenter(565.55047607422, -4884.3471679688, 26.210723876953, 80)',
		'GMR.DefineProfileCenter(448.62475585938, -4918.1625976563, 22.134880065918, 80)',
		'GMR.DefineProfileCenter(355.35845947266, -4912.9038085938, 22.970380783081, 80)',
		'GMR.DefineProfileCenter(313.45776367188, -4967.8413085938, 19.097560882568, 80)',
		'GMR.DefineProfileCenter(329.70858764648, -5034.0385742188, 15.095987319946, 80)',
		'GMR.DefineProfileCenter(261.3747253418, -5035.1918945313, 14.488904953003, 80)',
		'GMR.DefineProfileCenter(170.13674926758, -5037.5180664063, 11.173412322998, 80)',
		'GMR.DefineProfileCenter(54.177642822266, -5027.4658203125, 10.670310974121, 80)',
		'GMR.DefineProfileCenter(-11.863121032715, -5064.271484375, 9.4496803283691, 80)',
		'GMR.DefineProfileCenter(-84.120491027832, -5111.2348632813, 18.455577850342, 80)',
		'GMR.DefineProfileCenter(47.951969146729, -4985.8706054688, 13.36088180542, 80)',
		'GMR.DefineProfileCenter(16.657531738281, -4860.9213867188, 19.93098449707, 80)',
		'GMR.DefineProfileCenter(-79.939277648926, -4857.3125, 21.136817932129, 80)',
		'GMR.DefineProfileCenter(-167.58369445801, -4876.9052734375, 19.251857757568, 80)',
		'GMR.DefineProfileCenter(-190.12934875488, -4851.6977539063, 25.011615753174, 80)',
		'GMR.DefineProfileCenter(-268.025390625, -4892.5205078125, 29.610879898071, 80)',
		'GMR.DefineProfileCenter(-277.08294677734, -4773.6630859375, 32.334285736084, 80)',
		'GMR.DefineProfileCenter(-235.95220947266, -4723.87109375, 31.775899887085, 80)',
		'GMR.DefineProfileCenter(-170.26571655273, -4680.09765625, 33.015365600586, 80)',
		'GMR.DefineProfileCenter(-106.5814743042, -4651.7602539063, 34.271827697754, 80)',
		'GMR.DefineProfileCenter(-54.363075256348, -4727.1015625, 24.842422485352, 80)',
		'GMR.DefineProfileCenter(36.43480682373, -4698.2836914063, 33.20556640625, 80)',
		'GMR.DefineQuestEnemyId(3099)',
		'GMR.DefineQuestEnemyId(3125)',
		'GMR.DefineQuestEnemyId(3128)',
		'GMR.DefineQuestEnemyId(3129)',
		'GMR.DefineQuestEnemyId(3122)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             END: Grind to 10 (Troll/Orc)
----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Hunter ][Level 10][Orc + Troll] - Tame Pet / Summon Pet
----------------------------------------------------------------------------------------------------------------------------------

-- Taming the Beast Part 1 - (6062)
GMR.DefineQuest({ "Orc", "Troll" }, "HUNTER", 6062, "Taming the Beast", "UseItemOnObjects", 275.34072875977, -4703.9990234375, 11.625365257263, 3171, 275.34072875977, -4703.9990234375, 11.625365257263, 3171, 
	{
		15917, 3099, 5, 20, 1
	},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',
		'PetDismiss()',
		'GMR.DefineProfileCenter(4710.2089, -1767.1914, 31.9666, 120)'
	}
)

-- Taming the Beast Part 2 - (6083) --- Prerequisite (6062)
GMR.DefineQuest({ "Orc", "Troll" }, "HUNTER", 6083, "Taming the Beast", "UseItemOnObjects", 275.34072875977, -4703.9990234375, 11.625365257263, 3171, 275.34072875977, -4703.9990234375, 11.625365257263, 3171, 
	{
		15919, 3107, 5, 20, 1
	},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',
		'PetDismiss()',
		'GMR.DefineProfileCenter(985.39270019531, -5122.8798828125, 0.098619446158409, 15)'
	}
)

-- Taming the Beast Part 3 - (6082) --- Prerequisite (6083) --- Ask marty for HELP - Mob is Agro (Armored Scorpion) - Bot just refuses to run up and use the item on the mob
-- Mob is Aggressiove and all  he will do is attack, not use item.
GMR.DefineQuest({ "Orc", "Troll" }, "HUNTER", 6082, "Taming the Beast", "UseItemOnObjects", 275.34072875977, -4703.9990234375, 11.625365257263, 3171, 275.34072875977, -4703.9990234375, 11.625365257263, 3171, 
	{
		15920, 3126, 5, 20, nil
	},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',

		'PetDismiss()',
		'GMR.DefineProfileCenter(791.38751220703, -4971.4545898438, 15.323130607605, 60)'
	}
)


-- Taming the Beast Part 4 - 6081 -- Prerequisite (6082)
GMR.DefineQuest({ "Orc", "Troll" }, "HUNTER", 6081, "Training the Beast", "TalkTo", 275.34072875977, -4703.9990234375, 11.625366210938, 3171, 2100.58, -4606.96, 58.7034, 3352, {},
	{
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'PetDismiss()'
	}
)

-- Quest Chain Finished - Go TAME an actual pet now.
-- TAME - Dire Molted Boar (Black Boar, best pet to get)
GMR.DefineQuest({ "Orc", "Troll" }, "HUNTER", nil, "Tame Boar by Orgrimmar", "TameBeast", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		3100, 5, 29, 1
	},
	{
		'PetDismiss()',
		'GMR_AddPetFoodItem(117)',
		'GMR_AddPetFoodItem(2287)',
		'GMR_AddPetFoodItem(3770)',
		'GMR_AddPetFoodItem(3771)',
		'GMR_AddPetFoodItem(4599)',
		'GMR_AddPetFoodItem(9852)',
		'GMR_AddPetFoodItem(769)',
		'GMR.DefineProfileCenter(1061.5466308594, -4211.8774414063, 18.707204818726, 90)',
		'GMR.DefineProfileCenter(1049.2331542969, -4053.5478515625, 17.080318450928, 90)',
		'GMR.DefineProfileCenter(982.42956542969, -3957.5593261719, 22.301876068115, 90)',
	
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  			 END --- [ Hunter ][Level 10][Orc + Troll] - Tame Pet
----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------
--  		   START --- [ Warrior ][Level 10][Orc + Troll] - Defensive Stance
----------------------------------------------------------------------------------------------------------------------------------

-- Quest 1 - Defensive Stance
GMR.DefineQuest({ "Orc", "Troll" }, "WARRIOR", 1505, "Veteran Uzzek", "TalkTo", 311.351990, -4827.790039, 9.662940, 3169, 185.746002, -3597.209961, 27.338301, 5810, {},
	{
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)

-- Quest 2 - Defensive Stance - Path of Defense
GMR.DefineQuest({ "Orc", "Troll" }, "WARRIOR", 1498, "Path of Defense", "Grinding", 185.746002, -3597.209961, 27.338301, 5810, 185.746002, -3597.209961, 27.338301, 5810, {},
	{
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',

		'GMR.DefineProfileCenter(729.9128, -4032.058, -6.387927, 30)',
		'GMR.DefineProfileCenter(804.6252, -4035.054, -11.9247, 30)',
		'GMR.DefineProfileCenter(753.4786, -4108.59, -10.18405, 30)',
		'GMR.DefineProfileCenter(919.6711, -4036.738, -13.31126, 30)',
		'GMR.DefineProfileCenter(953.2545, -4135.906, -10.86135, 30)',
		'GMR.DefineProfileCenter(961.916, -4257.97, -8.314043, 30)',
		'GMR.DefineAreaBlacklist(876.708008, -4229.609863, -11.155500, 30)',
		'GMR.DefineQuestEnemyID(3130)',
		'GMR.DefineQuestEnemyID(3131)'
	}
)


-- Warrior Weapon - Maybe move to Level 13 Class Quests
-- Quest 3 - Warrior Weapon
GMR.DefineQuest({ "Orc", "Troll" }, "WARRIOR", 1502, "Thun'grim Firegaze", "TalkTo", 185.746002, -3597.209961, 27.338301, 5810, -437.619995, -3176.260010, 211.384995, 5878, {},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

-- Quest 4 - Warrior Weapon - Forged Steel
GMR.DefineQuest({ "Orc", "Troll" }, "WARRIOR", 1503, "Forged Steel", "Gathering", -437.619995, -3176.260010, 211.384995, 5878, -437.619995, -3176.260010, 211.384995, 5878, {},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',

		'GMR_DefineCustomObjectID(58369)', -- Stolen Iron Chest
		'GMR.DefineProfileCenter(-206.9702, -2918.022, 91.66673, 30)',
		'GMR.DefineProfileCenter(-187.4508, -2954.417, 91.78155, 30)',
		'GMR.DefineAreaBlacklist(-141.016388, -3012.611816, 91.666862, 30)',
		'GMR.DefineAreaBlacklist(-218.262482, -3024.780518, 91.666862, 30)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		   END --- [ Warrior ][Level 10][Orc + Troll] - Defensive Stance
----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------
--  		             START --- [ Warlock ][Level 13][Orc + Troll] - Summon Voidwalker
----------------------------------------------------------------------------------------------------------------------------------

-- Quest 1 - Voidwalker - Gan'Ruls Summons
GMR.DefineQuest({ "Orc", "Troll" }, "WARLOCK", 1506, "Gan'rul's Summons", "TalkTo", 352.947998, -4837.450195, 10.890600, 3294, 1850.310059, -4357.299805, -14.798200, 5875, {},
	{
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'GMR.SkipTurnIn(false)'
	}
)

-- Quest 2 - Voidwalker - Creature of the Void
GMR.DefineQuest({ "Orc", "Troll" }, "WARLOCK", 1501, "Creature of the Void", "Gathering", 1850.310059, -4357.299805, -14.798200, 5875, 1850.310059, -4357.299805, -14.798200, 5875, {},
	{
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',

		'GMR_DefineCustomObjectID(58595)',
		'GMR.DefineProfileCenter(1465.078, -4691.495, 6.628387, 20)',
		'GMR.DefineAreaBlacklist(1424.017, -4774.607, 5.164882, 30)',
		'GMR.DefineAreaBlacklist(1520.866089, -4774.905273, 9.305311, 3)',
		'GMR.DefineAreaBlacklist(1526.342, -4768.835, 8.263624, 5)',
		'GMR.DefineAreaBlacklist(1522.672852, -4682.168945, 9.000220, 5)',
		'GMR.DefineAreaBlacklist(1492.125, -4669.805, 7.29536, 5)',
		'GMR.SkipTurnIn(false)'

	}
)

-- Quest 3 - Voidwalker - The Binding
GMR.DefineQuest({ "Orc", "Troll" }, "WARLOCK", 1504, "|cFFFFAB48 The Binding |r", "Custom", 1850.310059, -4357.299805, -14.798200, 5875, 1850.310059, -4357.299805, -14.798200, 5875, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1504, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1805.804077, -4374.026855, -17.494928, 3) then 
				GMR.MeshTo(1805.804077, -4374.026855, -17.494928)
			else 
				local npc1 = GMR.GetObjectWithInfo({ id = 5676, rawType = 5, isAlive = true })
				local itemName = GetItemInfo(7464)
				if npc1 then
					if not GMR.InCombat() then
						GMR.TargetUnit(npc1)
						GMR.RunMacroText("/startattack")
						GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
					else
						GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
					end
				else
					GMR.Use(itemName)
					GMR.RunMacroText("/use Glyphs of Summoning")
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(1805.804077, -4374.026855, -17.494928, 150)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineQuestEnemyId(5676)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             END --- [ Warlock ][Level 13][Orc + Troll] - Summon Voidwalker
----------------------------------------------------------------------------------------------------------------------------------







GMR.DefineQuest({ "Orc", "Troll" }, nil, 784, "|cFFFFAB48 Vanquish the Betrayers |r", "Custom", 274.98779296875, -4709.3022460938, 17.489046096802, 3139, 274.98779296875, -4709.3022460938, 17.489046096802, 3139, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(784, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 3128, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(784, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 3129, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(784, 3) then
			local enemy = GMR.GetObjectWithInfo({ id = 3192, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(-244.5311, -5095.672, 41.34754, 50)',
		'GMR.DefineProfileCenter(-244.4739074707, -5116.8247070313, 42.640808105469, 50)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3128)',
		'GMR.DefineQuestEnemyId(3129)',
		'GMR.DefineQuestEnemyId(3192)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 830, "|cFFFFAB48 The Admiral's Orders |r", "Custom", -224.327835, -5096.153809, 49.529667, 3239, 274.98779296875, -4709.3022460938, 17.488767623901, 3139, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(830, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		[[if not GMR.Frames.Rand004 then
			GMR.Print("Frame created")
			GMR.Frames.Rand004 = CreateFrame("frame")
			GMR.Frames.Rand004:SetScript("OnUpdate", function(self)
				if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 830 then
					if GMR.IsExecuting() and GetItemCount(4881) >= 1 and not GMR.IsQuestActive(830) then
						local itemName = GetItemInfo(4881)
						GMR.Use(itemName)
						if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
							if QuestFrameAcceptButton:IsShown() then
								QuestFrameAcceptButton:Click()
							else
								QuestFrameAcceptButton:Click()
							end
						end
					end
				else 
					self:SetScript("OnUpdate", nil); GMR.Frames.Rand004 = nil; GMR.Print("Frame deleted")
				end
			end)
		end]],
		'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(-224.0952, -5095.855, 49.9767, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(99999)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFE25FFF TURNIN |r |cFFFFAB48 Vanquish the Betrayers / Admirals Orders |r", "MassTurnIn", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 784, 274.98779296875, -4709.3022460938, 17.488767623901, 3139 },
		{ 830, 274.98779296875, -4709.3022460938, 17.488767623901, 3139 }
	},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.AllowSpeedUp()',
		'GMR.SetChecked("Mount", true)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Admirals Orders Part 2 |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 831, 274.98779296875, -4709.3022460938, 17.488767623901, 3139 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 791, "|cFFFFAB48 Carry Your Weight |r", "Custom", 384.74282836914, -4600.1323242188, 76.088539123535, 3147, 384.74282836914, -4600.1323242188, 76.088539123535, 3147, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(791, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-917.8056640625, -4482.3364257813, 29.581489562988, 80)',
		'GMR.DefineProfileCenter(-974.41925048828, -4407.2412109375, 29.264513015747, 60)',
		'GMR.DefineProfileCenter(-931.01123046875, -4505.9072265625, 29.583276748657, 60)',
		'GMR.DefineProfileCenter(-1056.8665771484, -4596.42578125, 25.82195854187, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3128)',
		'GMR.DefineQuestEnemyId(3192)',
		'GMR.DefineQuestEnemyId(3129)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 815, "|cFFFFAB48 Break a Few Eggs |r", "Custom", 312.09875488281, -4664.9516601563, 16.35396194458, 3191, 312.09875488281, -4664.9516601563, 16.35396194458, 3191, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(815, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-1112.33, -5151.885, 0.9379244, 50)',
		'GMR.DefineProfileCenter(-1143.9565429688, -5108.5, 4.6817836761475, 30)',
		'GMR.DefineProfileCenter(-1352.3143310547, -5123.408203125, 1.8300317525864, 30)',
		'GMR.DefineProfileCenter(-1255.9052734375, -5338.9897460938, 5.4289474487305, 30)',
		'GMR.DefineProfileCenter(-1107.2957763672, -5394.1147460938, 10.499494552612, 30)',
		'GMR.DefineProfileCenter(-949.97052001953, -5203.9956054688, 3.111049413681, 30)',
		'GMR.DefineProfileCenter(-968.79406738281, -5180.1196289063, 2.0751116275787, 30)',
		'GMR.DefineProfileCenter(-774.21490478516, -5373.6845703125, 3.7147183418274, 30)',
		'GMR.DefineProfileCenter(-679.74041748047, -5522.0297851563, 6.1756219863892, 30)',
		'GMR.DefineProfileCenter(-655.740234375, -5547.3427734375, 4.0719041824341, 30)',
		'GMR.DefineProfileCenter(-682.46069335938, -5631.7568359375, 16.239351272583, 100)',
		'GMR.DefineProfileCenter(-677.88061523438, -5647.134765625, 10.83514881134, 30)',
		'GMR.DefineProfileCenter(-817.78271484375, -5593.38671875, 3.9629893302917, 30)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3240)',
		'GMR_DefineCustomObjectID(3240)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 817, "|cFFFFAB48 Practical Prey |r", "Custom", -797.52288818359, -4921.1704101563, 22.87388420105, 3194, -797.52288818359, -4921.1704101563, 22.87388420105, 3194, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(817, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(676.15881347656, -5547.2729492188, 8.5392732620239, 60)',
		'GMR.DefineProfileCenter(726.24938964844, -5608.5263671875, 25.169721603394, 60)',
		'GMR.DefineProfileCenter(782.73181152344, -5656.8564453125, 10.896883964539, 60)',
		'GMR.DefineProfileCenter(807.09594726563, -5529.1083984375, 0.70331192016602, 60)',
		'GMR.DefineProfileCenter(1030.5660400391, -5432.8852539063, 3.4216494560242, 60)',
		'GMR.DefineProfileCenter(1018.9437255859, -5383.09375, 5.6077275276184, 60)',
		'GMR.DefineProfileCenter(1130.2084960938, -5422.412109375, 11.648834228516, 60)',
		'GMR.DefineProfileCenter(1207.2869873047, -5378.1362304688, 8.1022939682007, 60)',
		'GMR.DefineProfileCenter(1305.2762451172, -5400.8168945313, 3.6345081329346, 60)',
		'GMR.DefineProfileCenter(1550.3559570313, -5338.4125976563, 5.9342083930969, 60)',
		'GMR.DefineProfileCenter(1592.326171875, -5329.1479492188, 6.5765390396118, 60)',
		'GMR.DefineProfileCenter(1609.9184570313, -5285.5375976563, 7.4622678756714, 60)',
		'GMR.DefineProfileCenter(1544.6513671875, -5254.9506835938, 6.8929967880249, 60)',
		'GMR.DefineProfileCenter(1421.7646484375, -5214.0346679688, 3.3820910453796, 60)',
		'GMR.DefineProfileCenter(1387.630859375, -5172.2006835938, 2.590790271759, 60)',
		'GMR.DefineProfileCenter(1365.7603759766, -5104.552734375, 0.30608797073364, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3121)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 786, "|cFFFFAB48 Thwarting Kolkar Aggression |r", "Gathering", -842.18194580078, -4845.75, 21.831750869751, 3140, -842.18194580078, -4845.75, 21.831750869751, 3140, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-1057.065, -4596.437, 25.82222, 5)',
		'GMR.DefineProfileCenter(-917.6598, -4482.283, 29.58254, 5)',
		'GMR.DefineProfileCenter(-974.2922, -4406.859, 29.26438, 5)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3189)',
		'GMR.DefineQuestEnemyId(3190)',
		'GMR.DefineQuestEnemyId(3192)',
		'GMR_DefineCustomObjectID(3189)',
		'GMR_DefineCustomObjectID(3190)',
		'GMR_DefineCustomObjectID(3192)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 837, "Encroachment", "Grinding", 274.98779296875, -4709.3022460938, 17.489046096802, 3139, 274.98779296875, -4709.3022460938, 17.489046096802, 3139, {},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',

		'GMR.DefineProfileCenter(91.886924743652, -4447.0869140625, 41.147144317627, 30)',
		'GMR.DefineProfileCenter(364.1164855957, -4306.4067382813, 25.376361846924, 30)',
		'GMR.DefineProfileCenter(485.8385925293, -4309.0434570313, 22.330936431885, 30)',
		'GMR.DefineAreaBlacklist(458.81094360352, -4237.3505859375, 24.979314804077, 10)',
		'GMR.DefineAreaBlacklist(432.39343261719, -4229.2866210938, 25.060152053833, 20)',
		'GMR.DefineAreaBlacklist(413.44635009766, -4234.41015625, 26.231554031372, 10)',
		'GMR.DefineAreaBlacklist(430.633636, -4226.846680, 24.954456, 20)',
		'GMR.DefineQuestEnemyId(3113)',
		'GMR.DefineQuestEnemyId(3114)',
		'GMR.DefineQuestEnemyId(3111)',
		'GMR.DefineQuestEnemyId(3112)',
		'GMR.SkipTurnIn(false)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 816, "Lost But Not Forgotten", "Grinding", 742.385010, -4241.649902, 18.761999, 3193, 742.385010, -4241.649902, 18.761999, 3193, {},
	{
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',

		'GMR.DefineProfileCenter(425.0439, -3814.493, 19.20117, 30)',
		'GMR.DefineProfileCenter(498.7545, -3802.046, 17.95413, 30)',
		'GMR.DefineProfileCenter(426.0063, -3814.745, 19.19055, 30)',
		'GMR.DefineProfileCenter(259.9775, -3790.82, 24.50972, 30)',
		'GMR.DefineProfileCenter(198.121, -3773.521, 18.55823, 30)',
		'GMR.DefineProfileCenter(128.0404, -3761.23, 18.04934, 30)',
		'GMR.DefineProfileCenter(57.73216, -3771.53, 17.83179, 30)',
		'GMR.DefineProfileCenter(-7.371057, -3732.038, 17.43111, 30)',
		'GMR.DefineProfileCenter(-41.17341, -3788.267, 19.85365, 30)',
		'GMR.DefineProfileCenter(-128.8107, -3804.188, 18.69778, 30)',
		'GMR.DefineProfileCenter(-175.5372, -3827.468, 21.13475, 30)',
		'GMR.DefineAreaBlacklist(448.4715, -4236.79, 25.77106, 50)',
		'GMR.DefineQuestEnemyId(3110)',
		'GMR.SkipTurnIn(false)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 834, "|cFFFFAB48 Winds in the Desert |r", "Gathering", 999.763, -4414.4, 14.38843, 3293, 999.763, -4414.4, 14.38843, 3293, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(834, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1013.018, -4558.711, 19.49151, 15)',
		'GMR.DefineProfileCenter(894.0166, -4650.524, 18.57743, 15)',
		'GMR.DefineProfileCenter(861.2977, -4599.819, 9.413081, 15)',
		'GMR.DefineProfileCenter(830.3309, -4694.842, 12.05367, 15)',
		'GMR.DefineProfileCenter(673.6964, -4588.714, 1.032063, 15)',
		'GMR.DefineProfileCenter(641.1426, -4559.641, 7.508011, 15)',
		'GMR.DefineProfileCenter(597.2432, -4505.117, 12.84119, 15)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3290)',
		'GMR_DefineCustomObjectID(3290)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 835, "|cFFFFAB48 Securing the Lines |r", "Custom", 999.763, -4414.4, 14.38843, 3293, 999.763, -4414.4, 14.38843, 3293, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(835, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = 3117, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(835, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = 3118, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(786.1123, -4573.615, 3.959859, 20)',
		'GMR.DefineProfileCenter(826.6373, -4818.174, 11.1625, 20)',
		'GMR.DefineProfileCenter(874.1946, -4820.68, 14.10484, 20)',
		'GMR.DefineProfileCenter(914.4683, -4791.959, 18.90134, 20)',
		'GMR.DefineProfileCenter(974.3276, -4823.456, 17.43384, 20)',
		'GMR.DefineProfileCenter(1008.054, -4815.13, 16.32497, 20)',
		'GMR.DefineProfileCenter(1036.342, -4802.117, 13.42201, 20)',
		'GMR.DefineProfileCenter(1053.1, -4755.493, 16.0609, 20)',
		'GMR.DefineProfileCenter(1071.072, -4732.667, 16.6285, 20)',
		'GMR.DefineProfileCenter(1061.344, -4690.297, 13.78766, 20)',
		'GMR.DefineProfileCenter(1092.514, -4682.279, 16.18264, 20)',
		'GMR.DefineProfileCenter(1142.895, -4682.209, 17.67068, 20)',
		'GMR.DefineProfileCenter(986.2607, -4813.653, 16.82832, 20)',
		'GMR.DefineProfileCenter(838.3339, -4818.579, 11.16211, 20)',
		'GMR.DefineAreaBlacklist(972.3451, -4783.308, 42.6867, 5)',
		'GMR.DefineAreaBlacklist(968.1453, -4764.871, 39.08746, 5)',
		'GMR.DefineAreaBlacklist(979.1228, -4705.535, 47.41284, 5)',
		'GMR.DefineAreaBlacklist(995.1911, -4678.6, 46.77747, 5)',
		'GMR.DefineAreaBlacklist(979.8286, -4706.024, 46.89437, 5)',
		'GMR.DefineAreaBlacklist(448.4715, -4236.79, 25.77106, 10)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineHearthstoneBindLocation(340.362000, -4686.290039, 16.541100, 6928)',
		'GMR.DefineQuestEnemyId(3118)',
		'GMR.DefineQuestEnemyId(3117)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFFFAB48 Grinding: Level 14 |r", "GrindTo", nil, nil, nil, nil, nil, nil, nil, nil, { 14 },
	{
		'GMR.DefineProfileCenter(1209.7626953125, -4315.3872070313, 22.656204223633, 80)',
		'GMR.DefineProfileCenter(1163.7670898438, -4258.416015625, 22.898790359497, 80)',
		'GMR.DefineProfileCenter(1162.0692138672, -4198.1860351563, 22.904590606689, 80)',
		'GMR.DefineProfileCenter(1073.8911132813, -4278.587890625, 18.716270446777, 80)',
		'GMR.DefineProfileCenter(1069.0638427734, -4193.8188476563, 20.547128677368, 80)',
		'GMR.DefineProfileCenter(1076.7243652344, -4110.0610351563, 18.323877334595, 80)',
		'GMR.DefineProfileCenter(1063.4938964844, -4065.3334960938, 15.547847747803, 80)',
		'GMR.DefineProfileCenter(1132.4083251953, -4058.6193847656, 14.016008377075, 80)',
		'GMR.DefineProfileCenter(1189.1466064453, -4087.8188476563, 18.631860733032, 80)',
		'GMR.DefineProfileCenter(1218.9377441406, -4106.5546875, 23.677265167236, 80)',
		'GMR.DefineProfileCenter(1245.9875488281, -4035.5041503906, 22.242565155029, 80)',
		'GMR.DefineProfileCenter(1130.3098144531, -3973.4296875, 14.278873443604, 80)',
		'GMR.DefineProfileCenter(1010.6015014648, -3953.0139160156, 19.187255859375, 80)',
		'GMR.DefineProfileCenter(956.23596191406, -3903.7229003906, 17.844142913818, 80)',
		'GMR.DefineProfileCenter(848.04864501953, -3908.390625, 19.117794036865, 80)',
		'GMR.DefineProfileCenter(803.5576171875, -3918.857421875, 20.064994812012, 80)',
		'GMR.DefineProfileCenter(709.60028076172, -3841.9328613281, 20.412528991699, 80)',
		'GMR.DefineProfileCenter(734.48760986328, -3923.9887695313, 14.622121810913, 80)',
		'GMR.DefineProfileCenter(1033.9995117188, -3964.8747558594, 17.730363845825, 80)',
		'GMR.DefineProfileCenter(1124.7436523438, -4161.0219726563, 19.470144271851, 80)',
		'GMR.DefineProfileCenter(1655.1296386719, -4408.3408203125, 17.313020706177, 150)',
		'GMR.DefineAreaBlacklist(888.21636962891, -4039.4477539063, -13.428733825684, 45)',
		'GMR.DefineAreaBlacklist(927.89855957031, -4020.7448730469, 24.582958221436, 45)',
		'GMR.DefineAreaBlacklist(824.11578369141, -4056.9248046875, -13.012672424316, 45)',
		'GMR.DefineAreaBlacklist(809.37463378906, -4094.2775878906, -12.923156738281, 45)',
		'GMR.DefineAreaBlacklist(770.71716308594, -4037.6440429688, -6.4796295166016, 45)',
		'GMR.DefineAreaBlacklist(695.95947265625, -4027.3432617188, -6.260814666748, 45)',
		'GMR.DefineAreaBlacklist(919.55596923828, -4044.6354980469, -13.338790893555, 45)',
		'GMR.DefineAreaBlacklist(886.31243896484, -4059.7685546875, 31.353824615479, 45)',
		'GMR.DefineAreaBlacklist(870.62109375, -4030.3679199219, 32.428894042969, 45)',
		'GMR.DefineAreaBlacklist(904.29058837891, -4030.201171875, 33.375587463379, 45)',
		'GMR.DefineAreaBlacklist(954.69970703125, -4130.1396484375, -11.223377227783, 55)',
		'GMR.DefineAreaBlacklist(947.03735351563, -4216.61328125, -6.4382705688477, 55)',
		'GMR.DefineAreaBlacklist(956.43658447266, -4277.828125, -6.3946304321289, 55)',
		'GMR.DefineAreaBlacklist(861.36596679688, -4180.7651367188, -14.110263824463, 55)',
		'GMR.DefineAreaBlacklist(955.56585693359, -4085.5395507813, -11.418724060059, 55)',
		'GMR.DefineAreaBlacklist(946.384765625, -4054.1572265625, 3.0340957641602, 55)',
		'GMR.DefineAreaBlacklist(865.13562011719, -4189.8823242188, -14.245635986328, 55)',
		'GMR.DefineAreaBlacklist(952.24932861328, -4237.5473632813, -6.4055137634277, 55)',
		'GMR.DefineAreaBlacklist(942.6181640625, -4216.0698242188, -6.3532791137695, 55)',
		'GMR.DefineAreaBlacklist(825.28509521484, -4243.1591796875, 22.232164382935, 55)',
		'GMR.DefineAreaBlacklist(779.53137207031, -4193.357421875, 21.296989440918, 55)',
		'GMR.DefineAreaBlacklist(919.15612792969, -4259.9047851563, 26.042501449585, 55)',
		'GMR.DefineAreaBlacklist(935.54028320313, -4287.5546875, 25.332153320313, 55)',
		'GMR.DefineQuestEnemyId(3099)',
		'GMR.DefineQuestEnemyId(3125)',
		'GMR.DefineQuestEnemyId(3128)',
		'GMR.DefineQuestEnemyId(3129)',
		'GMR.DefineQuestEnemyId(3122)',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "PICKUP Hidden Enemies and Spirits of Stonetalon", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 5726, 1920.130005, -4126.459961, 42.910000, 4949 }, -- Hidden Enemies
		{ 1061, 1914.650024, -4226.580078, 42.178902, 4047 }  -- Spirits of Stonetalon
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)



----------------------------------------------------------------------------------------------------------------------------------
--  		             START --- [ Shaman ][Level 14][Orc] - Fire Totem
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 2983, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 1 |r", "TalkTo", 307.114014, -4839.910156, 10.607500, 3173, 268.898987, -3055.149902, 97.164200, 5907, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Call of Fire (PART 2) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1524, 268.898987, -3055.149902, 97.164200, 5907 } -- Rites of the Earthmother 757 pre-req
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)'
	}
)


GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1524, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 2 |r", "TalkTo", 268.898987, -3055.149902, 97.164200, 5907, -269.279296875, -4001.587890625, 168.54284667969, 5900, 
	{},
	{
		-- From Q Pickup in BARRENS to Bottom of Mountain
		'GMR.SkipTurnIn(false)',
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(258.59, -3041.02, 96.27245, 100)',
		'GMR.DefineUnstuck(255.0209, -3047.895, 96.20301)',
		'GMR.DefineUnstuck(249.5511, -3057.054, 96.03181)',
		'GMR.DefineUnstuck(243.1605, -3067.273, 95.20503)',
		'GMR.DefineUnstuck(237.2101, -3076.127, 91.74356)',
		'GMR.DefineUnstuck(231.4548, -3084.657, 91.66515)',
		'GMR.DefineUnstuck(225.4823, -3093.509, 93.4323)',
		'GMR.DefineUnstuck(219.5274, -3102.335, 94.06092)',
		'GMR.DefineUnstuck(213.7721, -3110.865, 93.91298)',
		'GMR.DefineUnstuck(207.8054, -3119.708, 93.20019)',
		'GMR.DefineUnstuck(202.0266, -3128.273, 92.19747)',
		'GMR.DefineUnstuck(197.2555, -3137.603, 91.64236)',
		'GMR.DefineUnstuck(194.7313, -3147.6, 91.33762)',
		'GMR.DefineUnstuck(192.1197, -3157.944, 90.66074)',
		'GMR.DefineUnstuck(189.6006, -3167.921, 89.99816)',
		'GMR.DefineUnstuck(186.989, -3178.264, 88.46404)',
		'GMR.DefineUnstuck(184.4339, -3188.383, 86.27196)',
		'GMR.DefineUnstuck(181.8531, -3198.605, 83.68327)',
		'GMR.DefineUnstuck(179.2518, -3208.907, 81.14092)',
		'GMR.DefineUnstuck(174.1108, -3229.268, 75.81031)',
		'GMR.DefineUnstuck(171.7518, -3249.983, 70.15285)',
		'GMR.DefineUnstuck(171.3713, -3260.624, 67.20923)',
		'GMR.DefineUnstuck(170.9707, -3281.233, 62.23823)',
		'GMR.DefineUnstuck(174.1404, -3302.302, 56.70175)',
		'GMR.DefineUnstuck(177.565, -3322.978, 51.73302)',
		'GMR.DefineUnstuck(180.8963, -3343.67, 46.94912)',
		'GMR.DefineUnstuck(182.8474, -3364.036, 42.24098)',
		'GMR.DefineUnstuck(184.3737, -3384.958, 37.40455)',
		'GMR.DefineUnstuck(193.989, -3403.541, 33.16894)',
		'GMR.DefineUnstuck(203.765, -3422.08, 30.18659)',
		'GMR.DefineUnstuck(215.4052, -3439.406, 29.56329)',
		'GMR.DefineUnstuck(228.5992, -3455.689, 28.38532)',
		'GMR.DefineUnstuck(240.6957, -3473.161, 27.31293)',
		'GMR.DefineUnstuck(248.2084, -3492.726, 27.18479)',
		'GMR.DefineUnstuck(255.6939, -3512.324, 27.16391)',
		'GMR.DefineUnstuck(263.1719, -3531.902, 26.19556)',
		'GMR.DefineUnstuck(265.6788, -3551.797, 26.82313)',
		'GMR.DefineUnstuck(260.1794, -3572.019, 27.20911)',
		'GMR.DefineUnstuck(254.5376, -3592.215, 30.05747)',
		'GMR.DefineUnstuck(248.8, -3612.35, 31.65029)',
		'GMR.DefineUnstuck(241.1766, -3631.784, 30.92603)',
		'GMR.DefineUnstuck(231.7336, -3647.645, 29.45645)',
		'GMR.DefineUnstuck(222.2463, -3658.166, 28.38605)',
		'GMR.DefineUnstuck(205.7688, -3671.116, 29.44319)',
		'GMR.DefineUnstuck(188.8952, -3683.532, 28.49074)',
		'GMR.DefineUnstuck(169.37, -3690.839, 27.12857)',
		'GMR.DefineUnstuck(148.7933, -3694.923, 25.86952)',
		'GMR.DefineUnstuck(140.7835, -3696.417, 26.07366)',
		'GMR.DefineUnstuck(130.3171, -3698.369, 26.84388)',
		'GMR.DefineUnstuck(120.2016, -3700.256, 27.33046)',
		'GMR.DefineUnstuck(109.6571, -3701.697, 27.17219)',
		'GMR.DefineUnstuck(99.39675, -3702.717, 25.79729)',
		'GMR.DefineUnstuck(88.80202, -3703.771, 23.83244)',
		'GMR.DefineUnstuck(78.18639, -3704.827, 24.2003)',
		'GMR.DefineUnstuck(67.95882, -3705.844, 24.87083)',
		'GMR.DefineUnstuck(57.32229, -3706.902, 27.70512)',
		'GMR.DefineUnstuck(47.0828, -3707.92, 28.4149)',
		'GMR.DefineUnstuck(36.53659, -3706.764, 28.24712)',
		'GMR.DefineUnstuck(26.71281, -3704.024, 27.84645)',
		'GMR.DefineUnstuck(16.64107, -3700.834, 28.56208)',
		'GMR.DefineUnstuck(6.498993, -3698.975, 28.81654)',
		'GMR.DefineUnstuck(-4.09821, -3698.925, 27.78838)',
		'GMR.DefineUnstuck(-14.72664, -3699.553, 27.02936)',
		'GMR.DefineUnstuck(-25.01966, -3700.162, 26.35486)',
		'GMR.DefineUnstuck(-35.64809, -3700.79, 25.9634)',
		'GMR.DefineUnstuck(-45.7979, -3702.334, 27.08802)',
		'GMR.DefineUnstuck(-56.11556, -3705.046, 28.27396)',
		'GMR.DefineUnstuck(-66.06763, -3707.661, 29.1044)',
		'GMR.DefineUnstuck(-76.36497, -3710.367, 28.03403)',
		'GMR.DefineUnstuck(-86.37798, -3712.999, 27.12788)',
		'GMR.DefineUnstuck(-94.18307, -3719.122, 25.09065)',
		'GMR.DefineUnstuck(-100.4295, -3724.289, 22.55893)',
		'GMR.DefineUnstuck(-104.549, -3735.972, 19.58081)',
		'GMR.DefineUnstuck(-104.3058, -3744.164, 17.64138)',
		'GMR.DefineUnstuck(-108.4359, -3752.039, 16.34175)',
		'GMR.DefineUnstuck(-111.0229, -3762.976, 16.34175)',
		'GMR.DefineUnstuck(-113.0448, -3771.523, 16.34175)',
		'GMR.DefineUnstuck(-117.9275, -3782.19, 16.34175)',
		'GMR.DefineUnstuck(-125.1925, -3790.661, 16.34175)',
		'GMR.DefineUnstuck(-130.9568, -3797.18, 16.97536)',
		'GMR.DefineUnstuck(-136.5933, -3806.939, 18.9605)',
		'GMR.DefineUnstuck(-140.5615, -3816.413, 20.95662)',
		'GMR.DefineUnstuck(-144.2993, -3826.275, 23.98198)',
		'GMR.DefineUnstuck(-145.9946, -3836.392, 29.00173)',
		'GMR.DefineUnstuck(-148.8529, -3846.539, 33.23373)',
		'GMR.DefineUnstuck(-151.4526, -3856.639, 34.96017)',
		'GMR.DefineUnstuck(-155.5326, -3866.205, 36.53297)',
		'GMR.DefineUnstuck(-160.2907, -3875.498, 38.49665)',
		'GMR.DefineUnstuck(-167.4866, -3882.959, 40.34301)',
		'GMR.DefineUnstuck(-175.618, -3889.614, 42.09916)',
		'GMR.DefineUnstuck(-185.5473, -3893.02, 43.52283)',
		'GMR.DefineUnstuck(-195.0662, -3894.99, 46.08632)',
		'GMR.DefineUnstuck(-199.3801, -3894.128, 47.54789)',

		-- Bottom of the Hill
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(-199.4623, -3894.181, 47.61382, 100)',
		'GMR.DefineUnstuck(-202.7007, -3895.856, 49.94935)',
		'GMR.DefineUnstuck(-207.5063, -3898.153, 53.00512)',
		'GMR.DefineUnstuck(-211.1808, -3899.976, 54.83481)',
		'GMR.DefineUnstuck(-217.0187, -3902.872, 58.64489)',
		'GMR.DefineUnstuck(-220.119, -3904.585, 61.49921)',
		'GMR.DefineUnstuck(-223.0736, -3906.341, 63.95193)',
		'GMR.DefineUnstuck(-226.1475, -3908.105, 66.56731)',
		'GMR.DefineUnstuck(-230.0374, -3907.042, 69.88348)',
		'GMR.DefineUnstuck(-233.9443, -3903.606, 74.20333)',
		'GMR.DefineUnstuck(-235.9505, -3901.594, 76.76254)',
		'GMR.DefineUnstuck(-240.0965, -3898.51, 80.06519)',
		'GMR.DefineUnstuck(-241.8339, -3897.622, 80.91915)',
		'GMR.DefineUnstuck(-245.7246, -3897.939, 81.23082)',
		'GMR.DefineUnstuck(-249.7455, -3898.227, 80.58408)',
		'GMR.DefineUnstuck(-253.1858, -3898.357, 81.33885)',
		'GMR.DefineUnstuck(-255.7887, -3898.384, 82.62493)',
		'GMR.DefineUnstuck(-260.0606, -3898.454, 86.03916)',
		'GMR.DefineUnstuck(-262.191, -3898.672, 88.32788)',
		'GMR.DefineUnstuck(-264.7761, -3899.039, 90.69366)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-269.8214, -3903.371, 93.71248)',
		'GMR.DefineUnstuck(-271.9857, -3906.041, 94.44247)',
		'GMR.DefineUnstuck(-274.2279, -3909.254, 94.85416)',
		'GMR.DefineUnstuck(-272.4892, -3910.983, 96.57925)',
		'GMR.DefineUnstuck(-269.3623, -3912.675, 99.5118)',
		'GMR.DefineUnstuck(-267.3385, -3913.77, 101.4796)',
		'GMR.DefineUnstuck(-265.0544, -3912.314, 102.8668)',
		'GMR.DefineUnstuck(-262.3954, -3911.038, 104.5286)',
		'GMR.DefineUnstuck(-260.1604, -3910.277, 105.8743)',
		'GMR.DefineUnstuck(-257.3842, -3909.249, 106.9683)',
		'GMR.DefineUnstuck(-254.3581, -3908.606, 107.1753)',
		'GMR.DefineUnstuck(-251.652, -3908.49, 107.2679)',
		'GMR.DefineUnstuck(-248.1356, -3908.915, 106.9351)',
		'GMR.DefineUnstuck(-245.5722, -3909.724, 106.1492)',
		'GMR.DefineUnstuck(-243.6041, -3912.237, 104.4027)',
		'GMR.DefineUnstuck(-242.3808, -3914.41, 103.3386)',
		'GMR.DefineUnstuck(-240.897, -3916.677, 102.6752)',
		'GMR.DefineUnstuck(-239.5852, -3920.414, 102.794)',
		'GMR.DefineUnstuck(-239.3499, -3923.955, 102.3959)',
		'GMR.DefineUnstuck(-239.368, -3927.391, 103.7777)',
		'GMR.DefineUnstuck(-239.2873, -3930.225, 105.0105)',
		'GMR.DefineUnstuck(-239.2332, -3935.089, 106.6324)',
		'GMR.DefineUnstuck(-239.1963, -3938.526, 108.1947)',
		'GMR.DefineUnstuck(-239.3192, -3941.252, 109.3255)',
		'GMR.DefineUnstuck(-240.7941, -3944.482, 110.1133)',
		'GMR.DefineUnstuck(-241.6382, -3946.946, 110.1196)',
		'GMR.DefineUnstuck(-242.7522, -3950.197, 110.3971)',
		'GMR.DefineUnstuck(-243.8751, -3953.563, 110.8127)',
		'GMR.DefineUnstuck(-244.5518, -3956.926, 111.6394)',
		'GMR.DefineUnstuck(-244.1009, -3961.622, 114.141)',
		'GMR.DefineUnstuck(-243.7141, -3964.791, 115.6663)',
		'GMR.DefineUnstuck(-243.0102, -3967.404, 117.6225)',
		'GMR.DefineUnstuck(-240.3202, -3968.368, 117.7073)',
		'GMR.DefineUnstuck(-237.2673, -3968.719, 117.5444)',
		'GMR.DefineUnstuck(-233.834, -3968.985, 116.3506)',
		'GMR.DefineUnstuck(-231.4871, -3970.2, 116.3824)',
		'GMR.DefineUnstuck(-229.748, -3971.973, 117.6124)',
		'GMR.DefineUnstuck(-228.0222, -3974.524, 118.8738)',
		'GMR.DefineUnstuck(-227.0834, -3978.285, 120.0969)',
		'GMR.DefineUnstuck(-227.0988, -3980.87, 121.8627)',
		'GMR.DefineUnstuck(-227.1036, -3983.955, 124.4939)',
		'GMR.DefineUnstuck(-227.1822, -3987.496, 127.6713)',
		'GMR.DefineUnstuck(-227.2784, -3990.931, 131.2193)',
		'GMR.DefineUnstuck(-227.2588, -3993.661, 133.9031)',
		'GMR.DefineUnstuck(-227.1564, -3995.443, 135.7632)',
		'GMR.DefineUnstuck(-226.8765, -3997.551, 137.8601)',
		'GMR.DefineUnstuck(-226.0343, -4000.008, 140.1272)',
		'GMR.DefineUnstuck(-224.2299, -4003.055, 143.0167)',
		'GMR.DefineUnstuck(-222.9531, -4005.195, 144.5454)',
		'GMR.DefineUnstuck(-221.6833, -4007.323, 145.8429)',
		'GMR.DefineUnstuck(-220.7245, -4009.599, 146.7871)',
		'GMR.DefineUnstuck(-219.8593, -4013.171, 147.6718)',
		'GMR.DefineUnstuck(-219.6948, -4016.611, 148.7655)',
		'GMR.DefineUnstuck(-219.9563, -4020.261, 150.4539)',
		'GMR.DefineUnstuck(-220.7193, -4022.986, 151.8957)',
		'GMR.DefineUnstuck(-222.3295, -4024.552, 153.2682)',
		'GMR.DefineUnstuck(-225.4425, -4027.475, 155.1708)',
		'GMR.DefineUnstuck(-228.1926, -4030.861, 156.2314)',
		'GMR.DefineUnstuck(-229.4882, -4033.12, 157.2626)',
		'GMR.DefineUnstuck(-232.1257, -4037.305, 157.3341)',
		'GMR.DefineUnstuck(-235.949, -4038.966, 158.5497)',
		'GMR.DefineUnstuck(-238.4073, -4038.621, 159.6674)',
		'GMR.DefineUnstuck(-243.4568, -4037.35, 162.7227)',
		'GMR.DefineUnstuck(-246.7788, -4036.469, 165.6315)',
		'GMR.DefineUnstuck(-250.2322, -4035.651, 168.8637)',
		'GMR.DefineUnstuck(-253.5818, -4034.92, 171.7941)',
		'GMR.DefineUnstuck(-257.0383, -4034.116, 172.7241)',
		'GMR.DefineUnstuck(-260.3021, -4033.049, 172.6628)',
		'GMR.DefineUnstuck(-263.4335, -4031.657, 171.65)',
		'GMR.DefineUnstuck(-264.4315, -4027.819, 170.6964)',
		'GMR.DefineUnstuck(-264.8466, -4023.936, 170.0499)',
		'GMR.DefineUnstuck(-265.0885, -4020.985, 169.8793)',
		'GMR.DefineUnstuck(-265.3019, -4018.382, 169.6561)',
		'GMR.DefineUnstuck(-265.5061, -4015.892, 169.562)',
		'GMR.DefineUnstuck(-265.8044, -4012.362, 169.5828)',
		'GMR.DefineUnstuck(-266.1094, -4008.939, 169.2027)',
		'GMR.DefineUnstuck(-266.5756, -4005.411, 169.2965)',
		'GMR.DefineUnstuck(-267.1466, -4002.643, 169.2112)',
		'GMR.DefineUnstuck(-269.5026, -4001.055, 168.4183)',
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Call of Fire (PART 3) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1525, -269.279296875, -4001.587890625, 168.54284667969, 5900 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1525, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 3 |r", "Custom", -269.279296875, -4001.587890625, 168.54284667969, 5900, -269.279296875, -4001.587890625, 168.54284667969, 5900, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1525, 1) then
			GMR.SetQuestingState(nil)
			if not GMR.IsPlayerPosition(-102.434914, -2901.146240, 91.667473, 60) then 
				GMR.MeshTo(-102.434914, -2901.146240, 91.667473)
			else
				local npc1 = GMR.GetObjectWithInfo({ id = 3268, rawType = 5 })
				if npc1 then 
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		elseif not GMR.Questing.IsObjectiveCompleted(1525, 2) then
			GMR.SetQuestingState(nil)
			if not GMR.IsPlayerPosition(929.479309, -4738.714844, 21.402946, 60) then 
				GMR.MeshTo(929.479309, -4738.714844, 21.402946)
			else
				local npc1 = GMR.GetObjectWithInfo({ id = 3199, rawType = 5 })
				if npc1 then 
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				else
					GMR.SetQuestingState("Idle")
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(true)',
		'GMR.CreateTableEntry("Unstuck")',
		-- TOP of the Path (going to bottom)
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072, 80)',
		'GMR.DefineUnstuck(-265.9953, -4003.01, 169.6392)',
		'GMR.DefineUnstuck(-265.5731, -4007.286, 169.5224)',
		'GMR.DefineUnstuck(-265.0865, -4013.539, 169.8328)',
		'GMR.DefineUnstuck(-264.5121, -4020.509, 170.1297)',
		'GMR.DefineUnstuck(-263.6688, -4027.438, 171.0426)',
		'GMR.DefineUnstuck(-261.334, -4031.631, 172.5168)',
		'GMR.DefineUnstuck(-256.1549, -4035.319, 172.6828)',
		'GMR.DefineUnstuck(-252.6155, -4035.652, 170.9231)',
		'GMR.DefineUnstuck(-249.0734, -4035.963, 167.8442)',
		'GMR.DefineUnstuck(-245.6725, -4036.351, 164.5108)',
		'GMR.DefineUnstuck(-242.1395, -4036.754, 161.7505)',
		'GMR.DefineUnstuck(-238.7245, -4037.143, 159.9008)',
		'GMR.DefineUnstuck(-235.1842, -4037.477, 158.1641)',
		'GMR.DefineUnstuck(-231.5566, -4036.843, 157.3718)',
		'GMR.DefineUnstuck(-227.9, -4035.318, 157.1902)',
		'GMR.DefineUnstuck(-226.322, -4031.785, 156.1835)',
		'GMR.DefineUnstuck(-226.6904, -4028.975, 155.5541)',
		'GMR.DefineUnstuck(-225.3974, -4026.745, 155.0093)',
		'GMR.DefineUnstuck(-222.8908, -4024.383, 153.5098)',
		'GMR.DefineUnstuck(-220.3977, -4022.028, 151.4057)',
		'GMR.DefineUnstuck(-219.067, -4019.719, 149.6674)',
		'GMR.DefineUnstuck(-219.1835, -4016.262, 148.3217)',
		'GMR.DefineUnstuck(-220.2606, -4012.618, 147.7169)',
		'GMR.DefineUnstuck(-222.407, -4007.997, 146.4935)',
		'GMR.DefineUnstuck(-223.8766, -4004.003, 143.7523)',
		'GMR.DefineUnstuck(-225.2423, -4000.472, 140.6115)',
		'GMR.DefineUnstuck(-226.108, -3998.383, 138.6592)',
		'GMR.DefineUnstuck(-226.8772, -3996.527, 136.8776)',
		'GMR.DefineUnstuck(-227.6134, -3992.745, 132.9397)',
		'GMR.DefineUnstuck(-227.5912, -3990.134, 130.2498)',
		'GMR.DefineUnstuck(-227.4608, -3986.825, 126.9018)',
		'GMR.DefineUnstuck(-227.3341, -3984.224, 124.7492)',
		'GMR.DefineUnstuck(-227.2997, -3981.727, 122.6494)',
		'GMR.DefineUnstuck(-227.4868, -3979.49, 121.0448)',
		'GMR.DefineUnstuck(-228.0026, -3975.611, 119.4147)',
		'GMR.DefineUnstuck(-228.9861, -3972.956, 118.1752)',
		'GMR.DefineUnstuck(-231.309, -3970.595, 116.6192)',
		'GMR.DefineUnstuck(-233.9447, -3969.288, 116.5102)',
		'GMR.DefineUnstuck(-237.684, -3968.111, 117.4302)',
		'GMR.DefineUnstuck(-240.8389, -3967.107, 117.4534)',
		'GMR.DefineUnstuck(-243.8003, -3965.232, 115.9896)',
		'GMR.DefineUnstuck(-244.49, -3962.244, 114.589)',
		'GMR.DefineUnstuck(-244.8329, -3959.313, 112.8767)',
		'GMR.DefineUnstuck(-244.6241, -3957.085, 111.7018)',
		'GMR.DefineUnstuck(-244.376, -3951.407, 111.0384)',
		'GMR.DefineUnstuck(-244.1539, -3947.621, 110.5382)',
		'GMR.DefineUnstuck(-243.8453, -3945.028, 110.8477)',
		'GMR.DefineUnstuck(-242.4717, -3942.405, 109.7947)',
		'GMR.DefineUnstuck(-241.5451, -3939.616, 108.8344)',
		'GMR.DefineUnstuck(-240.7273, -3934.961, 106.8403)',
		'GMR.DefineUnstuck(-240.4573, -3932.371, 105.8352)',
		'GMR.DefineUnstuck(-240.3791, -3928.824, 104.7793)',
		'GMR.DefineUnstuck(-240.487, -3925.635, 103.1086)',
		'GMR.DefineUnstuck(-240.5185, -3922.912, 102.9576)',
		'GMR.DefineUnstuck(-240.5596, -3919.349, 103.0835)',
		'GMR.DefineUnstuck(-240.7238, -3915.923, 102.5849)',
		'GMR.DefineUnstuck(-240.6834, -3915.648, 102.56)',
		'GMR.DefineUnstuck(-242.3824, -3913.511, 103.37)',
		'GMR.DefineUnstuck(-243.865, -3911.823, 104.7807)',
		'GMR.DefineUnstuck(-245.6009, -3910.217, 106.0888)',
		'GMR.DefineUnstuck(-248.3885, -3908.029, 106.9726)',
		'GMR.DefineUnstuck(-251.8651, -3907.529, 107.3115)',
		'GMR.DefineUnstuck(-255.1949, -3908.342, 107.0193)',
		'GMR.DefineUnstuck(-258.5079, -3909.612, 106.6515)',
		'GMR.DefineUnstuck(-261.7232, -3910.822, 104.9466)',
		'GMR.DefineUnstuck(-265.1378, -3911.815, 102.8035)',
		'GMR.DefineUnstuck(-268.6282, -3912.478, 100.1142)',
		'GMR.DefineUnstuck(-272.0431, -3912.456, 97.57755)',
		'GMR.DefineUnstuck(-274.1382, -3911.253, 95.96122)',
		'GMR.DefineUnstuck(-273.5148, -3907.532, 94.47307)',
		'GMR.DefineUnstuck(-272.0321, -3904.424, 93.64937)',
		'GMR.DefineUnstuck(-270.6601, -3901.686, 93.12472)',
		'GMR.DefineUnstuck(-268.4762, -3899.883, 92.64568)',
		'GMR.DefineUnstuck(-264.9979, -3898.101, 90.40711)',
		'GMR.DefineUnstuck(-262.8027, -3897.241, 88.21679)',
		'GMR.DefineUnstuck(-259.9692, -3897.358, 85.52032)',
		'GMR.DefineUnstuck(-257.8491, -3897.611, 84.00722)',
		'GMR.DefineUnstuck(-255.49, -3897.787, 82.16405)',
		'GMR.DefineUnstuck(-252.8962, -3897.917, 81.03374)',
		'GMR.DefineUnstuck(-250.2877, -3898.031, 80.52705)',
		'GMR.DefineUnstuck(-247.5743, -3898.148, 81.06264)',
		'GMR.DefineUnstuck(-244.735, -3898.272, 81.36361)',
		'GMR.DefineUnstuck(-241.539, -3898.412, 80.69857)',
		'GMR.DefineUnstuck(-238.384, -3899.797, 78.9509)',
		'GMR.DefineUnstuck(-236.525, -3901.272, 77.32093)',
		'GMR.DefineUnstuck(-234.5782, -3902.816, 75.12527)',
		'GMR.DefineUnstuck(-232.5383, -3904.435, 72.6614)',
		'GMR.DefineUnstuck(-230.9534, -3905.692, 70.92281)',
		'GMR.DefineUnstuck(-227.9804, -3908.049, 67.98875)',
		'GMR.DefineUnstuck(-224.6707, -3908.18, 65.50371)',
		'GMR.DefineUnstuck(-222.4453, -3907.357, 64.44076)',
		'GMR.DefineUnstuck(-219.3503, -3905.622, 61.72633)',
		'GMR.DefineUnstuck(-216.3304, -3903.984, 58.855)',
		'GMR.DefineUnstuck(-213.4617, -3901.941, 56.57981)',
		'GMR.DefineUnstuck(-210.9052, -3899.633, 54.56749)',
		'GMR.DefineUnstuck(-208.201, -3897.337, 52.57686)',
		'GMR.DefineUnstuck(-205.4781, -3895.252, 50.61634)',
		'GMR.DefineUnstuck(-202.4935, -3893.324, 48.36252)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',
		'GMR.DefineProfileCenter(-102.434914, -2901.146240, 91.667473, 120)',
		'GMR.DefineProfileCenter(929.479309, -4738.714844, 21.402946, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)',
		'GMR.DefineQuestEnemyId(3268)',
		'GMR.DefineQuestEnemyId(3199)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", nil, "|cFFE25FFF TURNIN |r |cFFFFAB48 Call of Fire - Part 3 |r", "MassTurnIn", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1525, -269.279296875, -4001.587890625, 168.54284667969, 5900 },
	},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.SetChecked("GryphonMasters", true)',
		'GMR.AllowSpeedUp()',
		'GMR.SetChecked("Mount", true)',

		-- Bottom of the Hill
		'GMR.CreateTableEntry("Unstuck")',
		'GMR.DefineUnstuck(-199.4623, -3894.181, 47.61382, 100)',
		'GMR.DefineUnstuck(-202.7007, -3895.856, 49.94935)',
		'GMR.DefineUnstuck(-207.5063, -3898.153, 53.00512)',
		'GMR.DefineUnstuck(-211.1808, -3899.976, 54.83481)',
		'GMR.DefineUnstuck(-217.0187, -3902.872, 58.64489)',
		'GMR.DefineUnstuck(-220.119, -3904.585, 61.49921)',
		'GMR.DefineUnstuck(-223.0736, -3906.341, 63.95193)',
		'GMR.DefineUnstuck(-226.1475, -3908.105, 66.56731)',
		'GMR.DefineUnstuck(-230.0374, -3907.042, 69.88348)',
		'GMR.DefineUnstuck(-233.9443, -3903.606, 74.20333)',
		'GMR.DefineUnstuck(-235.9505, -3901.594, 76.76254)',
		'GMR.DefineUnstuck(-240.0965, -3898.51, 80.06519)',
		'GMR.DefineUnstuck(-241.8339, -3897.622, 80.91915)',
		'GMR.DefineUnstuck(-245.7246, -3897.939, 81.23082)',
		'GMR.DefineUnstuck(-249.7455, -3898.227, 80.58408)',
		'GMR.DefineUnstuck(-253.1858, -3898.357, 81.33885)',
		'GMR.DefineUnstuck(-255.7887, -3898.384, 82.62493)',
		'GMR.DefineUnstuck(-260.0606, -3898.454, 86.03916)',
		'GMR.DefineUnstuck(-262.191, -3898.672, 88.32788)',
		'GMR.DefineUnstuck(-264.7761, -3899.039, 90.69366)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-266.6161, -3899.418, 92.41206)',
		'GMR.DefineUnstuck(-269.8214, -3903.371, 93.71248)',
		'GMR.DefineUnstuck(-271.9857, -3906.041, 94.44247)',
		'GMR.DefineUnstuck(-274.2279, -3909.254, 94.85416)',
		'GMR.DefineUnstuck(-272.4892, -3910.983, 96.57925)',
		'GMR.DefineUnstuck(-269.3623, -3912.675, 99.5118)',
		'GMR.DefineUnstuck(-267.3385, -3913.77, 101.4796)',
		'GMR.DefineUnstuck(-265.0544, -3912.314, 102.8668)',
		'GMR.DefineUnstuck(-262.3954, -3911.038, 104.5286)',
		'GMR.DefineUnstuck(-260.1604, -3910.277, 105.8743)',
		'GMR.DefineUnstuck(-257.3842, -3909.249, 106.9683)',
		'GMR.DefineUnstuck(-254.3581, -3908.606, 107.1753)',
		'GMR.DefineUnstuck(-251.652, -3908.49, 107.2679)',
		'GMR.DefineUnstuck(-248.1356, -3908.915, 106.9351)',
		'GMR.DefineUnstuck(-245.5722, -3909.724, 106.1492)',
		'GMR.DefineUnstuck(-243.6041, -3912.237, 104.4027)',
		'GMR.DefineUnstuck(-242.3808, -3914.41, 103.3386)',
		'GMR.DefineUnstuck(-240.897, -3916.677, 102.6752)',
		'GMR.DefineUnstuck(-239.5852, -3920.414, 102.794)',
		'GMR.DefineUnstuck(-239.3499, -3923.955, 102.3959)',
		'GMR.DefineUnstuck(-239.368, -3927.391, 103.7777)',
		'GMR.DefineUnstuck(-239.2873, -3930.225, 105.0105)',
		'GMR.DefineUnstuck(-239.2332, -3935.089, 106.6324)',
		'GMR.DefineUnstuck(-239.1963, -3938.526, 108.1947)',
		'GMR.DefineUnstuck(-239.3192, -3941.252, 109.3255)',
		'GMR.DefineUnstuck(-240.7941, -3944.482, 110.1133)',
		'GMR.DefineUnstuck(-241.6382, -3946.946, 110.1196)',
		'GMR.DefineUnstuck(-242.7522, -3950.197, 110.3971)',
		'GMR.DefineUnstuck(-243.8751, -3953.563, 110.8127)',
		'GMR.DefineUnstuck(-244.5518, -3956.926, 111.6394)',
		'GMR.DefineUnstuck(-244.1009, -3961.622, 114.141)',
		'GMR.DefineUnstuck(-243.7141, -3964.791, 115.6663)',
		'GMR.DefineUnstuck(-243.0102, -3967.404, 117.6225)',
		'GMR.DefineUnstuck(-240.3202, -3968.368, 117.7073)',
		'GMR.DefineUnstuck(-237.2673, -3968.719, 117.5444)',
		'GMR.DefineUnstuck(-233.834, -3968.985, 116.3506)',
		'GMR.DefineUnstuck(-231.4871, -3970.2, 116.3824)',
		'GMR.DefineUnstuck(-229.748, -3971.973, 117.6124)',
		'GMR.DefineUnstuck(-228.0222, -3974.524, 118.8738)',
		'GMR.DefineUnstuck(-227.0834, -3978.285, 120.0969)',
		'GMR.DefineUnstuck(-227.0988, -3980.87, 121.8627)',
		'GMR.DefineUnstuck(-227.1036, -3983.955, 124.4939)',
		'GMR.DefineUnstuck(-227.1822, -3987.496, 127.6713)',
		'GMR.DefineUnstuck(-227.2784, -3990.931, 131.2193)',
		'GMR.DefineUnstuck(-227.2588, -3993.661, 133.9031)',
		'GMR.DefineUnstuck(-227.1564, -3995.443, 135.7632)',
		'GMR.DefineUnstuck(-226.8765, -3997.551, 137.8601)',
		'GMR.DefineUnstuck(-226.0343, -4000.008, 140.1272)',
		'GMR.DefineUnstuck(-224.2299, -4003.055, 143.0167)',
		'GMR.DefineUnstuck(-222.9531, -4005.195, 144.5454)',
		'GMR.DefineUnstuck(-221.6833, -4007.323, 145.8429)',
		'GMR.DefineUnstuck(-220.7245, -4009.599, 146.7871)',
		'GMR.DefineUnstuck(-219.8593, -4013.171, 147.6718)',
		'GMR.DefineUnstuck(-219.6948, -4016.611, 148.7655)',
		'GMR.DefineUnstuck(-219.9563, -4020.261, 150.4539)',
		'GMR.DefineUnstuck(-220.7193, -4022.986, 151.8957)',
		'GMR.DefineUnstuck(-222.3295, -4024.552, 153.2682)',
		'GMR.DefineUnstuck(-225.4425, -4027.475, 155.1708)',
		'GMR.DefineUnstuck(-228.1926, -4030.861, 156.2314)',
		'GMR.DefineUnstuck(-229.4882, -4033.12, 157.2626)',
		'GMR.DefineUnstuck(-232.1257, -4037.305, 157.3341)',
		'GMR.DefineUnstuck(-235.949, -4038.966, 158.5497)',
		'GMR.DefineUnstuck(-238.4073, -4038.621, 159.6674)',
		'GMR.DefineUnstuck(-243.4568, -4037.35, 162.7227)',
		'GMR.DefineUnstuck(-246.7788, -4036.469, 165.6315)',
		'GMR.DefineUnstuck(-250.2322, -4035.651, 168.8637)',
		'GMR.DefineUnstuck(-253.5818, -4034.92, 171.7941)',
		'GMR.DefineUnstuck(-257.0383, -4034.116, 172.7241)',
		'GMR.DefineUnstuck(-260.3021, -4033.049, 172.6628)',
		'GMR.DefineUnstuck(-263.4335, -4031.657, 171.65)',
		'GMR.DefineUnstuck(-264.4315, -4027.819, 170.6964)',
		'GMR.DefineUnstuck(-264.8466, -4023.936, 170.0499)',
		'GMR.DefineUnstuck(-265.0885, -4020.985, 169.8793)',
		'GMR.DefineUnstuck(-265.3019, -4018.382, 169.6561)',
		'GMR.DefineUnstuck(-265.5061, -4015.892, 169.562)',
		'GMR.DefineUnstuck(-265.8044, -4012.362, 169.5828)',
		'GMR.DefineUnstuck(-266.1094, -4008.939, 169.2027)',
		'GMR.DefineUnstuck(-266.5756, -4005.411, 169.2965)',
		'GMR.DefineUnstuck(-267.1466, -4002.643, 169.2112)',
		'GMR.DefineUnstuck(-269.5026, -4001.055, 168.4183)',
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)


GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1526, "|cFFFFAB48 Rite of Vision part 4 |r", "Custom", -269.279296875, -4001.587890625, 168.54284667969, 5900, -243.74391174316, -4022.4516601563, 188.00898742676, 61934, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(1526, 1) then
			GMR.SetQuestingState(nil);
			local npc1 = GMR.GetObjectWithInfo({ id = 5893, rawType = 5 })
			local fireSaptaBuff = GetSpellInfo(8898)
			if not npc1 and not GMR.HasBuff("player", fireSaptaBuff) then
				if not GMR.IsPlayerPosition(-257.436981, -3978.607178, 168.321869, 2) then 
					GMR.MeshTo(-257.436981, -3978.607178, 168.321869)
				else 
					local itemName = GetItemInfo(6636)
					GMR.Use(itemName)
					GMR.RunMacroText("/use Fire Sapta")
				end
			elseif npc1 or GMR.HasBuff("player, fireSaptaBuff) then
				if not GMR.IsPlayerPosition(-246.257996, -4009.979980, 187.292999, 6) then 
					GMR.MeshTo(-246.257996, -4009.979980, 187.292999)
				else
					GMR.TargetUnit(npc1)
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-246.257996, -4009.979980, 187.292999, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'GMR.DefineQuestEnemyId(5893)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 Call of Fire (PART 5) |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 1527, -243.74391174316, -4022.4516601563, 188.00898742676, 61934 }
	},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)


GMR.DefineQuest({ "Orc", "Troll" }, "SHAMAN", 1527, "|cFF59E2FD (Fire Totem) |r |cFFFFAB48 Call of Fire - Part 5 |r", "TalkTo", -243.74391174316, -4022.4516601563, 188.00898742676, 61934, 268.898987, -3055.149902, 97.164200, 5907, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- From Brazier to Quest NPC
		'GMR.DefineUnstuck(-243.4819, -4020.668, 187.3037, 80)',
		'GMR.DefineUnstuck(-244.7954, -4016.023, 187.3037)',
		'GMR.DefineUnstuck(-246.2919, -4009.803, 187.2939)',
		'GMR.DefineUnstuck(-248.2219, -4001.873, 184.6087)',
		'GMR.DefineUnstuck(-251.9074, -3991.917, 176.1363)',
		'GMR.DefineUnstuck(-255.5767, -3984.81, 169.9798)',
		'GMR.DefineUnstuck(-259.1981, -3986.567, 169.9047)',
		'GMR.DefineUnstuck(-262.6681, -3993.027, 169.5461)',
		'GMR.DefineUnstuck(-265.9148, -3998.033, 169.0287)',
		'GMR.DefineUnstuck(-268.999, -4000.609, 168.5203)',

		-- TOP of the Path (going to bottom)
		'GMR.DefineUnstuck(-269.7977, -4000.854, 168.3072, 80)',
		'GMR.DefineUnstuck(-265.9953, -4003.01, 169.6392)',
		'GMR.DefineUnstuck(-265.5731, -4007.286, 169.5224)',
		'GMR.DefineUnstuck(-265.0865, -4013.539, 169.8328)',
		'GMR.DefineUnstuck(-264.5121, -4020.509, 170.1297)',
		'GMR.DefineUnstuck(-263.6688, -4027.438, 171.0426)',
		'GMR.DefineUnstuck(-261.334, -4031.631, 172.5168)',
		'GMR.DefineUnstuck(-256.1549, -4035.319, 172.6828)',
		'GMR.DefineUnstuck(-252.6155, -4035.652, 170.9231)',
		'GMR.DefineUnstuck(-249.0734, -4035.963, 167.8442)',
		'GMR.DefineUnstuck(-245.6725, -4036.351, 164.5108)',
		'GMR.DefineUnstuck(-242.1395, -4036.754, 161.7505)',
		'GMR.DefineUnstuck(-238.7245, -4037.143, 159.9008)',
		'GMR.DefineUnstuck(-235.1842, -4037.477, 158.1641)',
		'GMR.DefineUnstuck(-231.5566, -4036.843, 157.3718)',
		'GMR.DefineUnstuck(-227.9, -4035.318, 157.1902)',
		'GMR.DefineUnstuck(-226.322, -4031.785, 156.1835)',
		'GMR.DefineUnstuck(-226.6904, -4028.975, 155.5541)',
		'GMR.DefineUnstuck(-225.3974, -4026.745, 155.0093)',
		'GMR.DefineUnstuck(-222.8908, -4024.383, 153.5098)',
		'GMR.DefineUnstuck(-220.3977, -4022.028, 151.4057)',
		'GMR.DefineUnstuck(-219.067, -4019.719, 149.6674)',
		'GMR.DefineUnstuck(-219.1835, -4016.262, 148.3217)',
		'GMR.DefineUnstuck(-220.2606, -4012.618, 147.7169)',
		'GMR.DefineUnstuck(-222.407, -4007.997, 146.4935)',
		'GMR.DefineUnstuck(-223.8766, -4004.003, 143.7523)',
		'GMR.DefineUnstuck(-225.2423, -4000.472, 140.6115)',
		'GMR.DefineUnstuck(-226.108, -3998.383, 138.6592)',
		'GMR.DefineUnstuck(-226.8772, -3996.527, 136.8776)',
		'GMR.DefineUnstuck(-227.6134, -3992.745, 132.9397)',
		'GMR.DefineUnstuck(-227.5912, -3990.134, 130.2498)',
		'GMR.DefineUnstuck(-227.4608, -3986.825, 126.9018)',
		'GMR.DefineUnstuck(-227.3341, -3984.224, 124.7492)',
		'GMR.DefineUnstuck(-227.2997, -3981.727, 122.6494)',
		'GMR.DefineUnstuck(-227.4868, -3979.49, 121.0448)',
		'GMR.DefineUnstuck(-228.0026, -3975.611, 119.4147)',
		'GMR.DefineUnstuck(-228.9861, -3972.956, 118.1752)',
		'GMR.DefineUnstuck(-231.309, -3970.595, 116.6192)',
		'GMR.DefineUnstuck(-233.9447, -3969.288, 116.5102)',
		'GMR.DefineUnstuck(-237.684, -3968.111, 117.4302)',
		'GMR.DefineUnstuck(-240.8389, -3967.107, 117.4534)',
		'GMR.DefineUnstuck(-243.8003, -3965.232, 115.9896)',
		'GMR.DefineUnstuck(-244.49, -3962.244, 114.589)',
		'GMR.DefineUnstuck(-244.8329, -3959.313, 112.8767)',
		'GMR.DefineUnstuck(-244.6241, -3957.085, 111.7018)',
		'GMR.DefineUnstuck(-244.376, -3951.407, 111.0384)',
		'GMR.DefineUnstuck(-244.1539, -3947.621, 110.5382)',
		'GMR.DefineUnstuck(-243.8453, -3945.028, 110.8477)',
		'GMR.DefineUnstuck(-242.4717, -3942.405, 109.7947)',
		'GMR.DefineUnstuck(-241.5451, -3939.616, 108.8344)',
		'GMR.DefineUnstuck(-240.7273, -3934.961, 106.8403)',
		'GMR.DefineUnstuck(-240.4573, -3932.371, 105.8352)',
		'GMR.DefineUnstuck(-240.3791, -3928.824, 104.7793)',
		'GMR.DefineUnstuck(-240.487, -3925.635, 103.1086)',
		'GMR.DefineUnstuck(-240.5185, -3922.912, 102.9576)',
		'GMR.DefineUnstuck(-240.5596, -3919.349, 103.0835)',
		'GMR.DefineUnstuck(-240.7238, -3915.923, 102.5849)',
		'GMR.DefineUnstuck(-240.6834, -3915.648, 102.56)',
		'GMR.DefineUnstuck(-242.3824, -3913.511, 103.37)',
		'GMR.DefineUnstuck(-243.865, -3911.823, 104.7807)',
		'GMR.DefineUnstuck(-245.6009, -3910.217, 106.0888)',
		'GMR.DefineUnstuck(-248.3885, -3908.029, 106.9726)',
		'GMR.DefineUnstuck(-251.8651, -3907.529, 107.3115)',
		'GMR.DefineUnstuck(-255.1949, -3908.342, 107.0193)',
		'GMR.DefineUnstuck(-258.5079, -3909.612, 106.6515)',
		'GMR.DefineUnstuck(-261.7232, -3910.822, 104.9466)',
		'GMR.DefineUnstuck(-265.1378, -3911.815, 102.8035)',
		'GMR.DefineUnstuck(-268.6282, -3912.478, 100.1142)',
		'GMR.DefineUnstuck(-272.0431, -3912.456, 97.57755)',
		'GMR.DefineUnstuck(-274.1382, -3911.253, 95.96122)',
		'GMR.DefineUnstuck(-273.5148, -3907.532, 94.47307)',
		'GMR.DefineUnstuck(-272.0321, -3904.424, 93.64937)',
		'GMR.DefineUnstuck(-270.6601, -3901.686, 93.12472)',
		'GMR.DefineUnstuck(-268.4762, -3899.883, 92.64568)',
		'GMR.DefineUnstuck(-264.9979, -3898.101, 90.40711)',
		'GMR.DefineUnstuck(-262.8027, -3897.241, 88.21679)',
		'GMR.DefineUnstuck(-259.9692, -3897.358, 85.52032)',
		'GMR.DefineUnstuck(-257.8491, -3897.611, 84.00722)',
		'GMR.DefineUnstuck(-255.49, -3897.787, 82.16405)',
		'GMR.DefineUnstuck(-252.8962, -3897.917, 81.03374)',
		'GMR.DefineUnstuck(-250.2877, -3898.031, 80.52705)',
		'GMR.DefineUnstuck(-247.5743, -3898.148, 81.06264)',
		'GMR.DefineUnstuck(-244.735, -3898.272, 81.36361)',
		'GMR.DefineUnstuck(-241.539, -3898.412, 80.69857)',
		'GMR.DefineUnstuck(-238.384, -3899.797, 78.9509)',
		'GMR.DefineUnstuck(-236.525, -3901.272, 77.32093)',
		'GMR.DefineUnstuck(-234.5782, -3902.816, 75.12527)',
		'GMR.DefineUnstuck(-232.5383, -3904.435, 72.6614)',
		'GMR.DefineUnstuck(-230.9534, -3905.692, 70.92281)',
		'GMR.DefineUnstuck(-227.9804, -3908.049, 67.98875)',
		'GMR.DefineUnstuck(-224.6707, -3908.18, 65.50371)',
		'GMR.DefineUnstuck(-222.4453, -3907.357, 64.44076)',
		'GMR.DefineUnstuck(-219.3503, -3905.622, 61.72633)',
		'GMR.DefineUnstuck(-216.3304, -3903.984, 58.855)',
		'GMR.DefineUnstuck(-213.4617, -3901.941, 56.57981)',
		'GMR.DefineUnstuck(-210.9052, -3899.633, 54.56749)',
		'GMR.DefineUnstuck(-208.201, -3897.337, 52.57686)',
		'GMR.DefineUnstuck(-205.4781, -3895.252, 50.61634)',
		'GMR.DefineUnstuck(-202.4935, -3893.324, 48.36252)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',
		'GMR.DefineUnstuck(-200.5301, -3892.231, 46.96742)',

		-- Bottom of Hill to Turnin NPC
		'GMR.DefineUnstuck(-198.136, -3893.104, 46.45814, 30)',
		'GMR.DefineUnstuck(-190.5721, -3888.852, 42.55983)',
		'GMR.DefineUnstuck(-180.0152, -3885.5, 41.1428)',
		'GMR.DefineUnstuck(-170.5749, -3881.455, 40.03998)',
		'GMR.DefineUnstuck(-161.4828, -3876.547, 38.74943)',
		'GMR.DefineUnstuck(-152.1162, -3871.485, 37.55161)',
		'GMR.DefineUnstuck(-143.1477, -3865.721, 36.54287)',
		'GMR.DefineUnstuck(-134.6426, -3859.892, 35.58938)',
		'GMR.DefineUnstuck(-124.9326, -3852.195, 34.12705)',
		'GMR.DefineUnstuck(-117.3372, -3845.222, 32.85403)',
		'GMR.DefineUnstuck(-113.3822, -3828.864, 28.31171)',
		'GMR.DefineUnstuck(-114.7541, -3818.285, 24.51085)',
		'GMR.DefineUnstuck(-115.7247, -3808.003, 20.86743)',
		'GMR.DefineUnstuck(-116.6059, -3797.397, 17.39478)',
		'GMR.DefineUnstuck(-114.9183, -3780.563, 16.34175)',
		'GMR.DefineUnstuck(-113.1018, -3768.708, 16.34175)',
		'GMR.DefineUnstuck(-109.7734, -3753.923, 16.34176)',
		'GMR.DefineUnstuck(-108.132, -3744.38, 17.83697)',
		'GMR.DefineUnstuck(-106.4039, -3735.667, 19.86492)',
		'GMR.DefineUnstuck(-104.106, -3725.25, 22.66694)',
		'GMR.DefineUnstuck(-101.8878, -3715.201, 25.47434)',
		'GMR.DefineUnstuck(-96.14438, -3706.347, 26.78756)',
		'GMR.DefineUnstuck(-87.32219, -3701.694, 27.12251)',
		'GMR.DefineUnstuck(-77.05066, -3698.885, 28.08901)',
		'GMR.DefineUnstuck(-66.76051, -3696.152, 28.8954)',
		'GMR.DefineUnstuck(-56.7951, -3693.505, 28.0468)',
		'GMR.DefineUnstuck(-46.50495, -3690.772, 27.19295)',
		'GMR.DefineUnstuck(-36.53954, -3688.125, 27.18301)',
		'GMR.DefineUnstuck(-26.24939, -3685.391, 27.18367)',
		'GMR.DefineUnstuck(-16.24339, -3682.733, 27.18367)',
		'GMR.DefineUnstuck(-5.97935, -3680.007, 27.42334)',
		'GMR.DefineUnstuck(3.270565, -3674.883, 29.1152)',
		'GMR.DefineUnstuck(11.34515, -3668.539, 29.80562)',
		'GMR.DefineUnstuck(19.06043, -3661.733, 29.93561)',
		'GMR.DefineUnstuck(26.34841, -3653.923, 30.00878)',
		'GMR.DefineUnstuck(30.84596, -3644.323, 29.61634)',
		'GMR.DefineUnstuck(31.85866, -3634.2, 28.69127)',
		'GMR.DefineUnstuck(33.75569, -3623.93, 28.06054)',
		'GMR.DefineUnstuck(39.57153, -3615.391, 28.17637)',
		'GMR.DefineUnstuck(45.60079, -3606.66, 28.37199)',
		'GMR.DefineUnstuck(51.47168, -3598.158, 28.61169)',
		'GMR.DefineUnstuck(57.50439, -3589.422, 28.95725)',
		'GMR.DefineUnstuck(63.38721, -3580.903, 29.55947)',
		'GMR.DefineUnstuck(69.41915, -3572.167, 29.8638)',
		'GMR.DefineUnstuck(75.27811, -3563.683, 29.50505)',
		'GMR.DefineUnstuck(81.33992, -3554.904, 29.13523)',
		'GMR.DefineUnstuck(87.37787, -3546.161, 29.48779)',
		'GMR.DefineUnstuck(93.23682, -3537.676, 30.05953)',
		'GMR.DefineUnstuck(99.30486, -3528.889, 30.41961)',
		'GMR.DefineUnstuck(105.1519, -3520.421, 30.60289)',
		'GMR.DefineUnstuck(111.1898, -3511.677, 30.6422)',
		'GMR.DefineUnstuck(116.8692, -3503.061, 30.4528)',
		'GMR.DefineUnstuck(121.5584, -3493.456, 30.38526)',
		'GMR.DefineUnstuck(126.2168, -3483.905, 30.85957)',
		'GMR.DefineUnstuck(130.9491, -3474.787, 30.96708)',
		'GMR.DefineUnstuck(138.6461, -3467.435, 29.64007)',
		'GMR.DefineUnstuck(146.1448, -3460.357, 29.56522)',
		'GMR.DefineUnstuck(153.5036, -3452.716, 29.32058)',
		'GMR.DefineUnstuck(159.1022, -3444.058, 28.71848)',
		'GMR.DefineUnstuck(164.7224, -3435.001, 28.1831)',
		'GMR.DefineUnstuck(167.2484, -3425, 28.61661)',
		'GMR.DefineUnstuck(169.0656, -3414.573, 30.57157)',
		'GMR.DefineUnstuck(170.8973, -3404.064, 32.98787)',
		'GMR.DefineUnstuck(172.664, -3393.927, 35.33115)',
		'GMR.DefineUnstuck(174.4957, -3383.417, 37.76035)',
		'GMR.DefineUnstuck(176.266, -3373.259, 40.10846)',
		'GMR.DefineUnstuck(178.0516, -3362.764, 42.53461)',
		'GMR.DefineUnstuck(178.9112, -3352.51, 44.90494)',
		'GMR.DefineUnstuck(178.5684, -3341.862, 47.36655)',
		'GMR.DefineUnstuck(177.813, -3331.578, 49.74374)',
		'GMR.DefineUnstuck(177.0331, -3320.96, 52.19994)',
		'GMR.DefineUnstuck(176.2777, -3310.677, 54.61379)',
		'GMR.DefineUnstuck(175.4977, -3300.058, 57.28224)',
		'GMR.DefineUnstuck(174.7147, -3289.398, 60.1383)',
		'GMR.DefineUnstuck(173.9609, -3279.136, 62.74594)',
		'GMR.DefineUnstuck(173.1809, -3268.517, 65.27595)',
		'GMR.DefineUnstuck(172.4255, -3258.234, 67.92759)',
		'GMR.DefineUnstuck(171.6456, -3247.615, 70.77227)',
		'GMR.DefineUnstuck(170.8617, -3237.314, 73.36811)',
		'GMR.DefineUnstuck(169.7871, -3226.722, 75.85859)',
		'GMR.DefineUnstuck(170.2659, -3216.378, 78.34002)',
		'GMR.DefineUnstuck(170.9778, -3205.797, 80.75212)',
		'GMR.DefineUnstuck(171.6925, -3195.174, 83.06865)',
		'GMR.DefineUnstuck(172.3832, -3184.907, 85.43134)',
		'GMR.DefineUnstuck(173.0979, -3174.284, 88.29417)',
		'GMR.DefineUnstuck(175.2186, -3164.302, 90.42546)',
		'GMR.DefineUnstuck(179.0815, -3154.369, 91.21728)',
		'GMR.DefineUnstuck(180.998, -3144.259, 91.4593)',
		'GMR.DefineUnstuck(182.981, -3133.799, 91.65737)',
		'GMR.DefineUnstuck(184.9672, -3123.321, 91.66991)',
		'GMR.DefineUnstuck(188.2947, -3113.675, 91.76788)',
		'GMR.DefineUnstuck(193.4834, -3104.402, 92.25401)',
		'GMR.DefineUnstuck(198.5183, -3095.404, 92.84583)',
		'GMR.DefineUnstuck(203.7274, -3086.094, 92.81425)',
		'GMR.DefineUnstuck(209.6564, -3077.781, 92.24898)',
		'GMR.DefineUnstuck(217.4222, -3070.485, 91.66679)',
		'GMR.DefineUnstuck(225.738, -3064.467, 91.97)',
		'GMR.DefineUnstuck(234.4507, -3058.356, 95.38133)',
		'GMR.DefineUnstuck(243.0827, -3052.088, 95.93181)',
		'GMR.DefineUnstuck(251.6662, -3046.414, 96.07415)',
		'GMR.DefineUnstuck(260.0364, -3041.026, 96.35554)',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             END --- [ Shaman ][Level 14][ORC] - Fire Totem
----------------------------------------------------------------------------------------------------------------------------------









GMR.DefineQuest({ "Orc", "Troll" }, nil, 831, "|cFFFFAB48 The Admirals Orders part 2 |r", "TalkTo", 274.98779296875, -4709.3022460938, 17.488767623901, 3139, 1934.060059, -4162.259766, 40.994999, 10540, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(831, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1934.060059, -4162.259766, 40.994999, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Ogrimmar Vendors
		'GMR.DefineSellVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineRepairVendor(1589.021, -4469.47, 7.665683, 3321)',
		'GMR.DefineGoodsVendor(1640.825, -4446.98, 15.40686, 5611)',
		'GMR.DefineAmmoVendor(1520.536, -4355.58, 18.76784, 3313)',
		'GMR.DefineProfileMailbox(1615.674, -4392.68, 10.21776)',
		'GMR.DefineQuestEnemyId(999999)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 5726, "|cFFFFAB48 Hidden Enemies |r", "Custom", 1920.0699462891, -4125.6850585938, 42.997577667236, 4949, 1920.0699462891, -4125.6850585938, 42.997577667236, 4949, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(5726, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1453.1999511719, -4877.3559570313, 11.899484634399, 60)',
		'GMR.DefineProfileCenter(1493.6730957031, -4824.1630859375, 9.1997737884521, 60)',
		'GMR.DefineProfileCenter(1429.6931152344, -4784.2158203125, 6.9113583564758, 60)',
		'GMR.DefineProfileCenter(1483.10546875, -4780.388671875, 6.0290551185608, 60)',
		'GMR.DefineProfileCenter(1525.6534423828, -4745.1123046875, 15.510608673096, 60)',
		'GMR.DefineProfileCenter(1460.8394775391, -4871.8930664063, 12.674479484558, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineQuestEnemyId(3197)',
		'GMR.DefineQuestEnemyId(3198)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 5727, "|cFFFFAB48 Hidden Enemies Part 2|r", "Custom", 1920.0699462891, -4125.6850585938, 42.997577667236, 4949, 1920.0699462891, -4125.6850585938, 42.997577667236, 4949, 
	{[[
		GMR.SetQuestingState(nil);
		if not GMR.Questing.IsObjectiveCompleted(5727, 1) then
			GMR.Questing.GossipWith(1800.660034, -4374.509766, -17.347500, 3216)
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1453.1999511719, -4877.3559570313, 11.899484634399, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Razor Hill Vendors
		'GMR.DefineSellVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineRepairVendor(358.12835693359, -4706.7265625, 14.39377784729, 3167)',
		'GMR.DefineGoodsVendor(305.72152709961, -4665.869140625, 16.444164276123, 3881)',
		'GMR.DefineAmmoVendor(321.5481262207, -4838.3989257813, 10.524939537048, 3164)',
		'GMR.DefineProfileMailbox(321.62692260742, -4707.6401367188, 14.571348190308, 143981)',
		'GMR.DefineQuestEnemyId(3197)',
		'GMR.DefineQuestEnemyId(3198)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 840, "|cFFFFAB48 Conscript of the Horde |r", "TalkTo", 271.804993, -4650.830078, 11.789400, 3336, 303.432007, -3686.159912, 27.150200, 3337, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

GMR.DefineQuest({ "Orc", "Troll" }, nil, 842, "|cFFFFAB48 Crossroads Conscription |r", "TalkTo", 303.432007, -3686.159912, 27.150200, 3337, -482.475006, -2670.189941, 97.522003, 3338, 
	{},
	{
		'GMR.SkipTurnIn(false)',
		-- Razor Hill Vendors
		'GMR.DefineRepairVendor(-581.73370361328, -4109.5063476563, 43.5244140625, 3160)',
		'GMR.DefineAmmoVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineSellVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)',
		'GMR.DefineGoodsVendor(-565.42803955078, -4214.19921875, 41.590438842773, 3158)'
	}
)

----------------------------------------------------------------------------------------------------------------------------------
--  		             END -- 7-13 Durotar Questing + Grinding
----------------------------------------------------------------------------------------------------------------------------------


















	end -- Detect Race - if UnitRace("player") == "Tauren" then
elseif UnitLevel("player") >= 14 and UnitLevel("player") < 60 then





----------------------------------------------------------------------------------------------------------------------------------
--  		             START -- 13-25 - The Barrens - Questing + Grinding
----------------------------------------------------------------------------------------------------------------------------------

GMR.DefineQuest( "Horde", nil, nil, "|cFFE25FFF PICKUP |r |cFFFFAB48 All Crossroads Quests - Level 14 |r", "MassPickUp", nil, nil, nil, nil, nil, nil, nil, nil,
	{
		{ 844, -482.475006, -2670.189941, 97.522003, 3338 },  -- Plainstrider Menace
		{ 869, -435.951996, -2639.209961, 96.358803, 3464 },  -- Raptor Thieves // To Track a Thief
		{ 870, -544.992004, -2672.770020, 95.870903, 3448 },  -- The Forgotten Pools
		{ 867, -474.894012, -2607.739990, 127.885002, 3449 }, -- Harpy Raiders
		{ 871, -473.201996, -2595.699951, 103.806000, 3429 }, -- Disrupt Their Attacks 1 / In Defense of Far Watch 1
		{ 848, -424.541992, -2589.879883, 95.905197, 3390 },  -- Fungal Spores
		{ 850, -307.135986, -1971.949951, 96.480003, 3389 },  -- Kolkar Leaders
		{ 855, -307.135986, -1971.949951, 96.480003, 3389 }  -- Centaur Bracers
	},
	{
		'GMR.SkipTurnIn(false)',
		-- The Barrens (Crossroads) Vendors
		'GMR.DefineSellVendor(-351.184, -2556.51, 95.78766, 3488)',
		'GMR.DefineRepairVendor(-351.184, -2556.51, 95.78766, 3488)',
		'GMR.DefineGoodsVendor(-407.122, -2645.22, 96.22272, 3934)',
		'GMR.DefineAmmoVendor(-410.836, -2612.9, 95.73892, 3481)',
		'GMR.DefineProfileMailbox(-442.97, -2648.11, 95.90372, 143982)',
		'GMR.DefineHearthstoneBindLocation(-407.122, -2645.22, 96.22272, 3934)'
	}
)

GMR.DefineQuest( "Horde", nil, 844, "|cFFFFAB48 Plainstrider Menace |r", "Custom", -482.475006, -2670.189941, 97.522003, 3338, -482.475006, -2670.189941, 97.522003, 3338, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(844, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-591.7014, -2752.299, 91.66708, 80)',
		'GMR.DefineProfileCenter(-647.934875, -2768.818115, 96.262848, 80)',
		'GMR.DefineProfileCenter(-798.158813, -2653.147949, 94.601501, 80)',
		'GMR.DefineProfileCenter(-835.701721, -2418.067383, 92.221519, 80)',
		'GMR.DefineProfileCenter(-447.580933, -2432.654541, 91.769165, 80)',
		'GMR.DefineProfileCenter(-154.199615, -2076.177002, 93.709488, 80)',
		'GMR.DefineProfileCenter(-124.237465, -2268.277100, 92.927689, 80)',
		'GMR.DefineProfileCenter(-252.042435, -2615.837158, 95.149345, 80)',
		'GMR.DefineProfileCenter(-212.388596, -2703.825684, 96.181038, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- The Barrens: Crossroads Vendors
		'GMR.DefineSellVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineRepairVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineGoodsVendor(-407.122986, -2645.219971, 96.306297, 3934)',
		'GMR.DefineAmmoVendor(-410.837006, -2612.909912, 95.821899, 3481)',
		'GMR.DefineProfileMailbox(-444.1595, -2648.366, 95.81458, 143982)',
		'GMR.DefineQuestEnemyId(3254)',
		'GMR.DefineQuestEnemyId(3244)',
		'GMR.DefineQuestEnemyId(3246)',
		'GMR.DefineQuestEnemyId(3255)'
	}
)

GMR.DefineQuest( "Horde", nil, 869, "|cFFFFAB48 Raptor Thieves |r", "Custom", -435.951996, -2639.209961, 96.358803, 3464, -435.951996, -2639.209961, 96.358803, 3464, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(869, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-513.31323242188, -2781.5209960938, 92.027069091797, 150)',
		'GMR.DefineProfileCenter(-781.56207275391, -2803.6818847656, 91.952445983887, 150)',
		'GMR.DefineProfileCenter(-940.01879882813, -2799.1384277344, 93.197357177734, 150)',
		'GMR.DefineProfileCenter(-702.877197, -3080.668701, 91.783241, 150)',
		'GMR.DefineProfileCenter(-881.164063, -3224.079102, 92.752090, 150)',
		'GMR.DefineProfileCenter(-948.915771, -3040.536133, 93.322250, 150)',
		'GMR.DefineProfileCenter(-664.002625, -2768.100342, 96.766418, 150)',
		'GMR.DefineProfileCenter(-1067.072998, -2465.810303, 91.666710, 150)',
		'GMR.DefineProfileCenter(-124.237465, -2268.277100, 92.927689, 150)',
		'GMR.DefineProfileCenter(-252.042435, -2615.837158, 95.149345, 150)',
		'GMR.DefineProfileCenter(-212.388596, -2703.825684, 96.181038, 150)',
		'GMR.DefineProfileCenter(-649.507019, -3319.038574, 94.478058, 150)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- The Barrens: Crossroads Vendors
		'GMR.DefineSellVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineRepairVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineGoodsVendor(-407.122986, -2645.219971, 96.306297, 3934)',
		'GMR.DefineAmmoVendor(-410.837006, -2612.909912, 95.821899, 3481)',
		'GMR.DefineProfileMailbox(-444.1595, -2648.366, 95.81458, 143982)',
		'GMR.DefineQuestEnemyId(3255)',
		'GMR.DefineQuestEnemyId(3254)'
	}
)

-- (844 Pre-req - Plainstrider Menace)
GMR.DefineQuest( "Horde", nil, 845, "|cFFFFAB48 The Zhevra |r", "Custom", -482.475006, -2670.189941, 97.522003, 3338, -482.475006, -2670.189941, 97.522003, 3338, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(845, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(-647.934875, -2768.818115, 96.262848, 80)',
		'GMR.DefineProfileCenter(-798.158813, -2653.147949, 94.601501, 80)',
		'GMR.DefineProfileCenter(-835.701721, -2418.067383, 92.221519, 80)',
		'GMR.DefineProfileCenter(-447.580933, -2432.654541, 91.769165, 80)',
		'GMR.DefineProfileCenter(-154.199615, -2076.177002, 93.709488, 80)',
		'GMR.DefineProfileCenter(-124.237465, -2268.277100, 92.927689, 80)',
		'GMR.DefineProfileCenter(-252.042435, -2615.837158, 95.149345, 80)',
		'GMR.DefineProfileCenter(-212.388596, -2703.825684, 96.181038, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- The Barrens: Crossroads Vendors
		'GMR.DefineSellVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineRepairVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineGoodsVendor(-407.122986, -2645.219971, 96.306297, 3934)',
		'GMR.DefineAmmoVendor(-410.837006, -2612.909912, 95.821899, 3481)',
		'GMR.DefineProfileMailbox(-444.1595, -2648.366, 95.81458, 143982)',
		'GMR.DefineQuestEnemyId(3242)'
	}
)

-- (845 Pre-req - The Zhevra)
GMR.DefineQuest( "Horde", nil, 903, "|cFFFFAB48 Prowlers of the Barrens |r", "Custom", -482.475006, -2670.189941, 97.522003, 3338, -482.475006, -2670.189941, 97.522003, 3338, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(903, 1) then
			GMR.SetQuestingState("Idle")
		end
	]]},
	{
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(542.802979, -2834.048096, 93.507729, 80)',
		'GMR.DefineProfileCenter(589.355652, -2965.114746, 91.667648, 80)',
		'GMR.DefineAreaBlacklist(-175.263000, -3008.489990, 91.833702, 90)',
		'GMR.DefineAreaBlacklist(-134.263794, -2887.378662, 93.703552, 90)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- The Barrens: Crossroads Vendors
		'GMR.DefineSellVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineRepairVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineGoodsVendor(-407.122986, -2645.219971, 96.306297, 3934)',
		'GMR.DefineAmmoVendor(-410.837006, -2612.909912, 95.821899, 3481)',
		'GMR.DefineProfileMailbox(-444.1595, -2648.366, 95.81458, 143982)',
		'GMR.DefineQuestEnemyId(3425)'
	}
)

-- (903 Pre-req - The Zhevra)
GMR.DefineQuest( "Horde", nil, 881, "|cFFFFAB48 Echeyakee |r", "Custom", -482.475006, -2670.189941, 97.522003, 3338, -482.475006, -2670.189941, 97.522003, 3338, 
	{[[
		if not GMR.Questing.IsObjectiveCompleted(881, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(452.287933, -3038.994629, 91.762222, 3) then 
				GMR.MeshTo(452.287933, -3038.994629, 91.762222)
			else 
				local npc1 = GMR.GetObjectWithInfo({ id = 3475, rawType = 5, isAlive = true })
				local itemName = GetItemInfo(10327)
				if npc1 then
					GMR.TargetUnit(npc1)
					GMR.RunMacroText("/startattack")
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				else
					GMR.Use(itemName)
					GMR.RunMacroText("/use Horn of Echeyakee")
					GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
				end
			end
		end
	]]},
	{
		'GMR.DefineProfileCenter(452.287933, -3038.994629, 91.762222, 150)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- The Barrens: Crossroads Vendors
		'GMR.DefineSellVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineRepairVendor(-356.996002, -2568.860107, 95.870796, 3479)',
		'GMR.DefineGoodsVendor(-407.122986, -2645.219971, 96.306297, 3934)',
		'GMR.DefineAmmoVendor(-410.837006, -2612.909912, 95.821899, 3481)',
		'GMR.DefineProfileMailbox(-444.1595, -2648.366, 95.81458, 143982)',
		'GMR.DefineQuestEnemyId(3475)'
	}
)




























end -- Level Detection

end)