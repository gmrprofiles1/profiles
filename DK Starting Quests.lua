GMR.DefineQuester("[WOTLK] DK Starting Area - Questing", function()



    if not timer then
        timer = GetTime()+85555666666
        local playerClass, englishClass = UnitClass("player")
            GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFF00FF00Thank you for Supporting me! Looking for more custom paid profiles? Dungeons/Grinding/Gathering/Questing/Fishing (All Expansions) Trolol#2955 |r")
            GMR.Print("|cFF00FF00 Death Knight Starting Zone Questing Profile! Looking to become a #Lifetime Crypto User? send me a message on gitlab ;) |r")
            GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFFFF2CE6 Loading...")
            GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFFFF2CE6 Detecting Zone...")
            if UnitClass("player") == "Death Knight" then
                GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFFFF2CE6 Detecting Class |r |cFFC41E3A" ..playerClass)
                GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFFFF2CE6 Death Knight Detected! Enjoy!|r")
            else
                GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFFFF2CE6 Detecting Class |r" ..playerClass)
                GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFFFF9734 Class Not Supported! Please create a Death Knight.|r")
            end
    elseif timer < GetTime() then 
        timer = nil;
    end




GMR.DefineQuest({
    QuestName = "|cFFFFAB48 In Service Of The Lich King |r",
    QuestID = 12593,
    QuestType = "TalkTo",
    PickUp = { x = 2345.2990722656, y = -5671.6787109375, z = 426.0302734375, id = 25462 },
    TurnIn = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 99553311064,
    Profile = {
        'GMR.SkipTurnIn(false)',
        'GMR.DefineProfileCenter(25462, 2486.320801, -5570.602051, 420.322113, 60)',
        'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
        -- Acherous the Ebon Hold
        'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
        'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
        'GMR.DefineQuestEnemyId(999999)'
    }
  })


GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Emblazoned Runeblade |r",
    QuestID = 12619,
    QuestType = "Custom",
    PickUp = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    TurnIn = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12593,
    Lua = [[
        if not GMR.Questing.IsObjectiveCompleted(12619, 1) then
            local itemName = GetItemInfo(38607)
            local object = GMR.GetObjectWithInfo({ id = 190584, rawType = 8, isInteractable = true })
            local object2 = GMR.GetObjectWithInfo({ id = { 28481, 28483 }, rawType = 5 })
            GMR.SetQuestingState(nil);
            if GetItemCount(itemName) < 1 then
                GMR.SetQuestingState(nil);
                if not GMR.GetDelay("CustomQuest") and object then 
                    GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
                else
                    GMR.SetQuestingState("Idle")
                end
            elseif GetItemCount(itemName) >= 1 then
                local x, y, z = GMR.ObjectPosition(object2)
                if not GMR.IsPlayerPosition(x, y, z, 6) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    if GetItemCooldown(38607) == 0 then
                        GMR.Use(38607)
                    end
                end
            end
        end
    ]],
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2502.6880, -5561.2360, 420.6452, 120)',
		'GMR.DefineProfileCenter(2439.9700, -5671.8900, 420.6008, 120)',
		'GMR.DefineProfileCenter(2487.5100, -5639.0280, 420.6454, 120)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
		'GMR.DefineQuestEnemyId(190584)',
        'GMR.DefineCustomObjectId(190584)'
    }
  })

GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Runeforging: Preparation For Battle |r",
    QuestID = 12842,
    QuestType = "Custom",
    PickUp = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    TurnIn = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12619,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12842, 1) then
            if not GMR.IsPlayerPosition(2506.9700, -5561.9250, 420.6474, 3) then 
                GMR.MeshTo(2506.9700, -5561.9250, 420.6474)
            else 
                local Runeforging = GetSpellInfo(53428)
                local itemName = GetItemInfo(38607)
                local RunedSoulblade = GetItemInfo(38707)
                GMR.Cast(53428)
                if TradeSkillFrame:IsShown() then
                    GMR.Cast(Runeforging)
                    GMR.RunMacroText("/click TradeSkillCreateButton")
                    GMR.Use(RunedSoulblade)
                end
            end
		end
    ]],
    Profile = {
		'GMR.DefineProfileCenter(2506.9700, -5561.9250, 420.6474, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
		'GMR.DefineQuestEnemyId(9999999)',
        'GMR.DefineCustomObjectId(9999999)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Endless Hunger |r",
    QuestID = 12848,
    QuestType = "Custom",
    PickUp = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    TurnIn = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12842,
    Lua = [[
        if GMR.IsQuestActive(12848) then
            if GetItemCount(38707) >= 1 and not IsEquippedItem(38707) then
                GMR.RunMacroText("/equip Runed Soulblade")
            elseif not GMR.Questing.IsObjectiveCompleted(12848, 1) then
                GMR.SetQuestingState(nil)
                local object = GMR.GetObjectWithInfo({ id = { 191584, 191583, 191582, 191581, 191585, 191580, 191577, 191589, 191590, 191588, 191587, 191586 }, rawType = 8, isInteractable = true })
                local enemy = GMR.GetObjectWithInfo({ id = { 29519, 29567, 29566, 29565, 29520, 29519 }, rawType = 5, canAttack = true, isAlive = true })
                if enemy then
                    GMR.SetQuestingState(nil)
                    GMR.Questing.KillEnemy(enemy)
                elseif object then 
                    GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
                else
                    GMR.SetQuestingState("Idle")
                end
            end
        end
    ]],
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2460.4290, -5616.8000, 415.6596, 80)',
		'GMR.DefineProfileCenter(2483.7340, -5593.4130, 415.6636, 80)',
		'GMR.DefineProfileCenter(2463.1250, -5570.1210, 415.6639, 80)',
		'GMR.DefineProfileCenter(2438.8910, -5584.4450, 415.6640, 80)',
		'GMR.DefineProfileCenter(2438.4310, -5601.9210, 415.6638, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
		'GMR.DefineQuestEnemyId(29519)',
		'GMR.DefineQuestEnemyId(29567)',
		'GMR.DefineQuestEnemyId(29566)',
		'GMR.DefineQuestEnemyId(29565)',
		'GMR.DefineQuestEnemyId(29520)',
		'GMR.DefineQuestEnemyId(29519)'
    }
  })


GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Eye Of Acherus |r",
    QuestID = 12636,
    QuestType = "TalkTo",
    PickUp = { x = 2496.87890625, y = -5587.4125976562, z = 420.41665649414, id = 28357 },
    TurnIn = { x = 2345.3000, y = -5671.6800, z = 426.0307, id = 25462 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 38707,
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2345.300049, -5671.680176, 426.114014, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
		'GMR.DefineQuestEnemyId(25462)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Death Comes From On High |r",
    QuestID = 12641,
    QuestType = "Custom",
    PickUp = { x = 2345.300049, y = -5671.680176, z = 426.114014, id = 25462 },
    TurnIn = { x = 2345.300049, y = -5671.680176, z = 426.114014, id = 25462 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12636,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12641, 1) then
			GMR.SetQuestingState(nil);
            local theBuff = GetSpellInfo(51852)
			if not GMR.HasBuff("player", theBuff) then
				if not GMR.IsPlayerPosition(2345.498535, -5696.931641, 426.029602, 3) then 
					GMR.MeshTo(2345.498535, -5696.931641, 426.029602)
				else 
					local object = GMR.GetObjectWithInfo({ id = 191609, rawType = 8 })
					if object then
						GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
					end
				end
			else
                GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
		[[
        if not GMR.Frames.Rand001 then
			GMR.Print("Frame created")
			GMR.Frames.Rand001 = CreateFrame("frame")
			GMR.Frames.Rand001:SetScript("OnUpdate", function(self)
				if GMR.GetProfileType() == "Questing" then
                    if not GMR.IsQuestCompletable(12641) then
                        if GMR.IsExecuting() and GMR.IsQuestActive(12641) then
                            local theBuff = GetSpellInfo(51852)
                            if GMR.HasBuff("player", theBuff) then
                                local SiphonOfArcheus = GetSpellInfo(51859)
                                local summonGhouls = GetSpellInfo(51904)
                                local shroud = GetSpellInfo(52006)
                                local RecallEXIT = GetSpellInfo(52694)
                                local npc1 = GMR.GetObjectWithInfo({ id = { 28660, 28529, 28530, 28594, 28558, 28548 }, rawType = 5, isAlive = true })
                                local eyeHP = GMR.GetObjectWithInfo({ id = 28511, rawType = 5, isAlive = true })
                                if GetSpellCooldown(shroud) == 0 then
                                    GMR.Cast(shroud)
                                else
                                    if not GMR.Questing.IsObjectiveCompleted(12641, 1) then
                                        if not GMR.IsPlayerPosition(1810.9990, -5991.1930, 171.3509, 10) then
                                            if UnitChannelInfo("pet") ~= "Siphon of Acherus" then
                                                GMR.MoveTo(1810.9990, -5991.1930, 171.3509)
                                                GMR.Cast(SiphonOfArcheus)
                                            end
                                        end
                                    elseif not GMR.Questing.IsObjectiveCompleted(12641, 2) then
                                        if not GMR.IsPlayerPosition(1598.7790, -5743.9200, 178.5091, 3) then
                                            if UnitChannelInfo("pet") ~= "Siphon of Acherus" then
                                                GMR.MoveTo(1598.7790, -5743.9200, 178.5091)
                                                GMR.Cast(SiphonOfArcheus)
                                            end
                                        end
                                    elseif not GMR.Questing.IsObjectiveCompleted(12641, 3) then
                                        if not GMR.IsPlayerPosition(1652.7570, -5984.3750, 184.4390, 3) then
                                            if UnitChannelInfo("pet") ~= "Siphon of Acherus" then
                                                GMR.MoveTo(1652.7570, -5984.3750, 184.4390)
                                                GMR.Cast(SiphonOfArcheus)
                                            end
                                        end
                                    elseif not GMR.Questing.IsObjectiveCompleted(12641, 4) then
                                        if not GMR.IsPlayerPosition(1393.4120, -5705.7430, 187.3752, 3) then
                                            if UnitChannelInfo("pet") ~= "Siphon of Acherus" then
                                                GMR.MoveTo(1393.4120, -5705.7430, 187.3752)
                                                GMR.Cast(SiphonOfArcheus)
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    elseif GMR.IsQuestCompletable(12641) then
                        GMR.Cast(RecallEXIT)
                        OverrideActionBarButton5:Click()
                        ActionButton5:Click()
                    end
				else 
					self:SetScript("OnUpdate", nil); GMR.Frames.Rand001 = nil; GMR.Print("Frame deleted")
				end
			end)
		end]],
		'GMR.SkipTurnIn(false)',
        'GMR.SetChecked("ClickToMove", true)',
		'GMR.SetChecked("Mount", false)',
		'GMR.SetChecked("FlyingMount", false)',
		'GMR.DenySpeedUp()',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
    }
  })



  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Might Of The Scourge |r",
    QuestID = 12657,
    QuestType = "Custom",
    PickUp = { x = 2345.300049, y = -5671.680176, z = 426.114014, id = 25462 },
    TurnIn = { x = 2460.500000, y = -5593.470215, z = 367.476013, id = 28444 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12641,
    Lua = [[
        if not GMR.IsQuestCompletable(12657) then
            GMR.SetQuestingState(nil);
			if GMR.IsPlayerPosition(2345.300049, -5671.680176, 426.114014, 55) or GMR.IsPlayerPosition(2377.563232, -5648.110840, 424.843842, 15) then 
                GMR.MeshTo(2345.498535, -5696.931641, 426.029602)
            else
                GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("ClickToMove", false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(2374.9641113281, -5651.6782226562, 426.12817382812, 20)',
        'GMR.DefineUnstuck(2385.2607421875, -5653.47265625, 423.25637817383)',
        'GMR.DefineUnstuck(2393.791015625, -5646.5849609375, 420.75717163086)',
        'GMR.DefineUnstuck(2391.6186523438, -5640.0844726562, 420.85217285156)',
        'GMR.DefineUnstuck(2383.2446289062, -5645.197265625, 421.70391845703)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(2390.2009277344, -5640.6352539062, 378.0407409668, 20)',
        'GMR.DefineUnstuck(2395.3662109375, -5637.5927734375, 377.08197021484)',
        'GMR.DefineUnstuck(2404.880859375, -5631.1108398438, 376.94207763672)',
        'GMR.DefineUnstuck(2414.1186523438, -5624.7250976562, 377.02676391602)',
        'GMR.DefineUnstuck(2422.6118164062, -5618.4438476562, 372.23068237305)',
        'GMR.DefineUnstuck(2432.015625, -5613.2578125, 367.34252929688)',
        'GMR.DefineUnstuck(2439.9379882812, -5608.08984375, 366.81848144531)',
        'GMR.DefineUnstuck(2445.9592285156, -5603.7797851562, 367.49612426758)',
        'GMR.DefineUnstuck(2453.6606445312, -5598.2670898438, 367.40591430664)',
        'GMR.DefineUnstuck(2459.9106445312, -5593.7939453125, 367.39370727539)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2460.500000, -5593.470215, 367.476013, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
		'GMR.DefineQuestEnemyId(25462)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Report To Scourge Commander Thalanor |r",
    QuestID = 12850,
    QuestType = "Custom",
    PickUp = { x = 2460.500000, y = -5593.470215, z = 367.476013, id = 28444 },
    TurnIn = { x = 2357.362061, y = -5684.257324, z = 382.240356, id = 28510 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12641,
    Lua = [[
        if GetItemCount(38662) >= 1 and not IsEquippedItem(38662) then
            GMR.RunMacroText("/equip Bladed Ebon Amulet")
        elseif GetItemCount(38707) >= 1 and not IsEquippedItem(38707) then
            GMR.RunMacroText("/equip Runed Soulblade")

		elseif GMR.IsQuestActive(12850) then
            GMR.SetQuestingState("Idle")
		end
    ]],
    Profile = {
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2357.362061, -5684.257324, 382.240356, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Acherous the Ebon Hold
		'GMR.DefineGoodsVendor(2435.71875, -5638.244140625, 420.64447021484, 28500)',
		'GMR.DefineAmmoVendor(2388.310059, -5579.209961, 420.735992, 29207)',
		'GMR.DefineQuestEnemyId(25462)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Scarlet Harvest |r",
    QuestID = 12670,
    QuestType = "Custom",
    PickUp = { x = 2357.362061, y = -5684.257324, z = 382.240356, id = 28510 },
    TurnIn = { x = 2371.370117, y = -5701.040039, z = 154.005005, id = 28377 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12850,
    Lua = [[
		if GMR.IsQuestActive(12670) and GMR.IsPlayerPosition(2357.362061, -5684.257324, 382.240356, 100) then
            GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(2348.547363, -5695.462891, 382.949402, 5) then 
                GMR.MeshTo(2348.547363, -5695.462891, 382.949402)
            else
                local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                else
                    GMR.SetQuestingState("Idle")
                end
            end
		end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand00155 then
                GMR.Print("Frame created")
                GMR.Frames.Rand00155 = CreateFrame("frame")
                GMR.Frames.Rand00155:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" then
                        if GMR.IsQuestCompletable(12670) then
                            if GMR.IsExecuting() and GMR.IsQuestActive(12670) then
                                if GMR.IsQuestActive(12670) and GMR.IsPlayerPosition(2357.362061, -5684.257324, 382.240356, 100) then
                                    GMR.SetQuestingState(nil);
                                    if not GMR.IsPlayerPosition(2348.547363, -5695.462891, 382.949402, 5) then 
                                        GMR.MeshTo(2348.547363, -5695.462891, 382.949402)
                                    else
                                        local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5 })
                                        if npc then
                                            GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                        end
                                    end
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand00155 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end]],
		'GMR.SkipTurnIn(false)',
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.DefineProfileCenter(2348.547363, -5695.462891, 382.949402, 60)',
		'GMR.DefineProfileCenter(2371.370117, -5701.040039, 154.005005, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28377)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 If Chaos Drives / Grand Theft Palomino / Death's Challenge / Tonight We Dine /  |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 12678, x = 2371.370117, y = -5701.040039, z = 154.005005, id = 28377 },
      { questId = 12680, x = 2378.479248, y = -5726.925293, z = 153.924072, id = 28653 },
      { questId = 12733, x = 2380.684326, y = -5786.438965, z = 151.706314, id = 29047 },
      { questId = 12679, x = 2301.387207, y = -5719.591309, z = 153.891571, id = 28647 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12670,
    Profile = {
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.SkipTurnIn(true)',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Tonight We Dine In Havenshire |r",
    QuestID = 12679,
    QuestType = "Custom",
    PickUp = { x = 2301.387207, y = -5719.591309, z = 153.891571, id = 28647 },
    TurnIn = { x = 2301.387207, y = -5719.591309, z = 153.891571, id = 28647 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12670,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12679, 1) then
			GMR.SetQuestingState(nil);
			local object = GMR.GetObjectWithInfo({ id = 190691, rawType = 8, isInteractable = true})
			if not GMR.GetDelay("CustomQuest") and object then 
				GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
			else
				GMR.SetQuestingState("Idle")
			end
		end
    ]],
    Profile = {
		'GMR.SkipTurnIn(false)',
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.DefineProfileCenter(2056.2290, -5755.4130, 98.0604, 130)',
		'GMR.DefineProfileCenter(1998.1060, -5817.1740, 100.6272, 130)',
		'GMR.DefineProfileCenter(2124.3980, -5852.8640, 102.3126, 130)',
		'GMR.DefineProfileCenter(1874.2090, -5877.8720, 102.9978, 130)',
		'GMR.DefineProfileCenter(1903.3290, -5808.5000, 100.8494, 130)',
		'GMR.DefineProfileCenter(1740.4490, -5827.8630, 116.1650, 130)',
		'GMR.DefineProfileCenter(1720.2280, -5900.1930, 116.1481, 130)',
		'GMR.DefineProfileCenter(2010.7590, -5894.7020, 103.3427, 130)',
		'GMR.DefineProfileCenter(2084.9950, -5843.9340, 103.7149, 130)',
        'GMR.DefineAreaBlacklist(2115.7802734375, -5802.8149414062, 100.83236694336, 17)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(190691)',
		'GMR.DefineCustomObjectId(190691)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 If Chaos Drives, Let Suffering Hold The Reins |r",
    QuestID = 12678,
    QuestType = "Custom",
    PickUp = { x = 2371.370117, y = -5701.040039, z = 154.005005, id = 28377 },
    TurnIn = { x = 2371.370117, y = -5701.040039, z = 154.005005, id = 28377 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12679,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12678, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = { 28557, 28609, 28608, 28611, 28841 }, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(12678, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = { 28576, 28577 }, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
    ]],
    Profile = {
        [[
        if not GMR.Frames.Rand004 then
            GMR.Print("Frame created")
            GMR.Frames.Rand004 = CreateFrame("frame")
            GMR.Frames.Rand004:SetScript("OnUpdate", function(self)
                if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12678 then
                    if GMR.IsExecuting() then
                        if GMR.IsQuestActive(12678) then
                            local cloak = GetItemInfo(39320)
                            local neck = GetItemInfo(38662)
                            if GMR.IsQuestActive(12678) then
                                if GetItemInfo(GetInventoryItemID("player", 15)) == "Sky Darkener's Shroud of Blood" == false then
                                    GMR.Print("Equipping Sky Darkener's Shroud of Blood")
                                    GMR.RunMacroText("/equip Sky Darkener's Shroud of Blood")
                                elseif GetItemInfo(GetInventoryItemID("player", 2)) == "Bladed Ebon Amulet" == false then
                                    GMR.Print("Equipping Bladed Ebon Amulet")
                                    GMR.RunMacroText("/equip Bladed Ebon Amulet")
                                end
                            elseif GMR.IsPlayerPosition(2357.362061, -5684.257324, 382.240356, 100) or GMR.IsPlayerPosition(28472, 2530.439941, -5554.250000, 377.054443, 100)
                            or GMR.IsPlayerPosition(2348.547363, -5695.462891, 382.949402, 30) then
                                if not GMR.IsPlayerPosition(2348.547363, -5695.462891, 382.949402, 3) then 
                                    GMR.MeshTo(2348.547363, -5695.462891, 382.949402)
                                else
                                    local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5, distance = 20 })
                                    if npc then
                                        GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                    end
                                end
                            end
                        end
                    end
                else 
                    self:SetScript("OnUpdate", nil); GMR.Frames.Rand004 = nil; GMR.Print("Frame deleted")
                end
            end)
        end
        ]],
        'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2117.590088, -5731.640137, 100.163002, 80)',
		'GMR.DefineProfileCenter(2073.017822, -5860.476074, 103.636299, 80)',
		'GMR.DefineProfileCenter(1811.505859, -5934.935059, 115.132690, 80)',
		'GMR.DefineProfileCenter(2118.224609, -5829.645508, 101.917809, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28557)',
		'GMR.DefineQuestEnemyId(28609)',
		'GMR.DefineQuestEnemyId(28608)',
		'GMR.DefineQuestEnemyId(28611)',
		'GMR.DefineQuestEnemyId(28841)',
		'GMR.DefineQuestEnemyId(28576)',
		'GMR.DefineQuestEnemyId(28577)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Death's Challenge |r",
    QuestID = 12733,
    QuestType = "Custom",
    PickUp = { x = 2380.684326, y = -5786.438965, z = 151.706314, id = 29047 },
    TurnIn = { x = 2380.684326, y = -5786.438965, z = 151.706314, id = 29047 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12678,
    Lua = [[
		GMR.SetQuestingState(nil);
		if GMR.Variables.Enemy then
			if UnitCanAttack("player", GMR.Variables.Enemy) then
				GMR.TargetUnit(GMR.Variables.Enemy); GMR.EnemyMovement(); GMR.CombatRotation()
			else
				GMR.Questing.GossipWith(GMR.Variables.Enemy, nil, nil, nil, 5);
				if not ObjectExists(GMR.Variables.Enemy) then
					GMR.Variables.Enemy = nil
				end
			end
		elseif not GMR.Variables.Enemy then
			local npc1 = GMR.GetObjectWithInfo({ id = 28406, rawType = 5, isAlive = true })
			if npc1 and GMR.GetHealth(npc1) > 15 then
				GMR.Questing.GossipWith(npc1, nil, nil, nil, 5); GMR.Variables.Enemy = npc1
            else
				GMR.SetQuestingState("Idle")
			end
		end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand004123 then
                GMR.Print("Frame created")
                GMR.Frames.Rand004123 = CreateFrame("frame")
                GMR.Frames.Rand004123:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12733 then
                        if GMR.IsExecuting() then
                            local ring = GetItemInfo(38671)
                            if GMR.IsQuestActive(12733) then
                                if GetItemInfo(GetInventoryItemID("player", 12)) == "Valanar's Signet Ring" == false then
                                    GMR.RunMacroText("/equip Valanar's Signet Ring")
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand004123 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end
            ]],
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2347.199951, -5680.350098, 154.003006, 80)',
		'GMR.DefineProfileCenter(2381.649902, -5662.370117, 154.169998, 80)',
		'GMR.DefineProfileCenter(2439.020020, -5753.129883, 153.679001, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(9999999)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Abandoned Mail |r",
    QuestID = 12711,
    QuestType = "TalkTo",
    PickUp = { x = 2114.3360, y = -5795.3070, z = 99.7090, id = 190917 },
    TurnIn = { x = 2114.3360, y = -5795.3070, z = 99.7090, id = 190917 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12733,
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2114.3360, -5795.3070, 99.7090, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(25462)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Grand Theft Palomino (Manual Completeion Required) |r",
    QuestID = 12680,
    QuestType = "Custom",
    PickUp = { x = 2378.479248, y = -5726.925293, z = 153.924072, id = 28653 },
    TurnIn = { x = 2378.479248, y = -5726.925293, z = 153.924072, id = 28653 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12711,
    Lua = [[
        if not GMR.Questing.IsObjectiveCompleted(12680, 1) then
            local Gallop = GetSpellInfo(52268)
            local DeliverHorse = GetSpellInfo(52264)
            local horseBUFF = GetSpellInfo(52263)
            local npc1 = GMR.GetObjectWithInfo({ id = { 28605, 28606, 28607 }, rawType = 5, isAlive = true })
            if GMR.InCombat() and GMR.HasBuff("player", horseBUFF) then
                GMR.Cast(Gallop)
            elseif not GMR.HasBuff("player", horseBUFF) then
                if npc1 then
                    GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
                end
            elseif GMR.IsInVehicle() and GMR.HasBuff("player", horseBUFF) then
                if not GMR.IsPlayerPosition(2352.2930, -5702.7870, 153.9229, 20) then
                    GMR.MoveTo(2371.7260, -5720.2910, 153.9201)
                else
                    local npc = GMR.GetObjectWithInfo({ id = 28653, rawType = 5 })
                    local x, y, z = GMR.ObjectPosition(npc)
                    if not GMR.IsPlayerPosition(x, y, z, 5) then
                        GMR.Mesh(x, y, z)
                    elseif GMR.IsMoving() then 
                        GMR.StopMoving()
                    else
                        GMR.Cast(DeliverHorse)
                        GMR.RunMacroText("/click VehicleMenuBarActionButton1")
                    end
                end
            else
				GMR.SetQuestingState("Idle")
			end
        end
    ]],
    Profile = {
        [[
        if not GMR.Frames.Rand023 then
            GMR.Print("Frame created")
            GMR.Frames.Rand023 = CreateFrame("frame")
            GMR.Frames.Rand023:SetScript("OnUpdate", function(self)
                if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12680 then
                    if GMR.IsExecuting() and not GMR.Questing.IsObjectiveCompleted(12680, 1) then
                        local Gallop = GetSpellInfo(52268)
                        local DeliverHorse = GetSpellInfo(52264)
                        local horseBUFF = GetSpellInfo(52263)
                        local npc1 = GMR.GetObjectWithInfo({ id = { 28605, 28606, 28607 }, rawType = 5, isAlive = true })
                        if GMR.IsInVehicle() then
                            GMR.Cast(DeliverHorse)
                            OverrideActionBarButton1:Click()
                            if not GMR.IsPlayerPosition(2352.2930, -5702.7870, 153.9229, 30) 
                            and not GMR.IsPlayerPosition(2206.4680175781, -5640.4077148438, 127.87729644775, 30)
                            and not GMR.IsPlayerPosition(2206.4680175781, -5640.4077148438, 127.87729644775, 30) 
                            and not GMR.IsPlayerPosition(2231.7700195312, -5649.7309570312, 130.83636474609, 30)
                            and not GMR.IsPlayerPosition(2262.1767578125, -5655.8930664062, 139.14996337891, 30)
                            and not GMR.IsPlayerPosition(2293.4709472656, -5666.0048828125, 150.5630645752, 30)
                            and not GMR.IsPlayerPosition(2325.6040039062, -5684.3291015625, 155.04125976562, 30) then
                                GMR.MoveTo(2206.4680175781, -5640.4077148438, 127.87729644775)
                            else
                                GMR.MoveTo(2337.5493164062, -5686.859375, 155.78608703613)
                                local npc = GMR.GetObjectWithInfo({ id = 28653, rawType = 5 })
                                local x, y, z = GMR.ObjectPosition(npc)
                                if not GMR.IsPlayerPosition(x, y, z, 5) then
                                    GMR.Mesh(x, y, z)
                                elseif GMR.IsMoving() then 
                                    GMR.StopMoving()
                                else
                                    GMR.Cast(DeliverHorse)
                                    GMR.RunMacroText("/click VehicleMenuBarActionButton1")
                                end
                            end
                        end
                    end
                else 
                    self:SetScript("OnUpdate", nil); GMR.Frames.Rand023 = nil; GMR.Print("Frame deleted")
                end
            end)
        end
        ]],
        'GMR.SetChecked("ClickToMove", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2209.9700, -5824.1540, 101.3565, 60)',
		'GMR.DefineProfileCenter(2200.4280, -5827.3260, 101.6395, 60)',
		'GMR.DefineProfileCenter(2368.3750, -5719.3150, 153.9225, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(999999)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Into the Realm of Shadows (GZ ON MOUNT) |r",
    QuestID = 12687,
    QuestType = "Custom",
    PickUp = { x = 2378.479248, y = -5726.925293, z = 153.924072, id = 28653 },
    TurnIn = { x = 2378.479248, y = -5726.925293, z = 153.924072, id = 28653 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12680,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12687, 1) then
            if not UnitInVehicle("player") then
                GMR.SetQuestingState(nil);
                local npc = GMR.GetObjectWithInfo({ id = 28782, rawType = 5 })
                if npc then 
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 3)
                else
                    GMR.SetQuestingState("Idle")
                end
            else
                local horsemansCall = GetSpellInfo(52362)
                if UnitInVehicle("player") then
                    GMR.Cast(horsemansCall)
                    if not GMR.IsPlayerPosition(2327.1610, -5669.1980, 153.9205, 7) then
                        if not GMR.IsPlayerPosition(2327.1610, -5669.1980, 153.9205) then
                            if GMR.GetVehicleSpeed() == 0 then 
                                GMR.MoveTo(2327.1610, -5669.1980, 153.9205)
                            end
                        elseif GMR.GetVehicleSpeed() ~= 0 then 
                            GMR.MoveForwardStart(); GMR.MoveForwardStop()()
                        end
                    elseif GMR.IsPlayerPosition(2327.1610, -5669.1980, 153.9205, 7) then
                        GMR.Cast(horsemansCall)
                    end
                end
            end
        end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand025 then
                GMR.Print("Frame created")
                GMR.Frames.Rand025 = CreateFrame("frame")
                GMR.Frames.Rand025:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12687 then
                        if GMR.IsExecuting() and not GMR.Questing.IsObjectiveCompleted(12687, 1) then
                            local horsemansCall = GetSpellInfo(52362)
                            if UnitInVehicle("player") then
                                if GMR.IsPlayerPosition(2272.7868652344, -5756.1435546875, 103.93466949463, 10) then
                                    GMR.Cast(horsemansCall)
                                else
                                    if not GMR.IsPlayerPosition(2272.7868652344, -5756.1435546875, 103.93466949463, 7) then
                                        if not GMR.IsPlayerPosition(2272.7868652344, -5756.1435546875, 103.93466949463) then
                                            if GMR.GetVehicleSpeed() == 0 then 
                                                GMR.MoveTo(2272.7868652344, -5756.1435546875, 103.93466949463)
                                            end
                                        elseif GMR.GetVehicleSpeed() ~= 0 then 
                                            GMR.MoveForwardStart(); GMR.MoveForwardStop()()
                                        end
                                    end
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand025 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end
            ]],
        'GMR.SetChecked("Mount", true)',
        'GMR.SetChecked("ClickToMove", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2211.4910, -5774.6270, 101.5713, 60)',
		'GMR.DefineProfileCenter(2241.3300, -5715.7520, 108.7173, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Gothik the Harvester |r",
    QuestID = 12697,
    QuestType = "TalkTo",
    PickUp = { x = 2371.370117, y = -5701.040039, z = 154.005005, id = 28377 },
    TurnIn = { x = 2348.7700, y = -5758.2800, z = 153.9221, id = 28658 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12687,
    Profile = {
        'GMR.SkipTurnIn(false)',
        'GMR.DefineProfileCenter(2348.7700, -5758.2800, 153.9222, 60)',
        'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
    }
  })



  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Gift That Keeps On Giving |r",
    QuestID = 12698,
    QuestType = "Custom",
    PickUp = { x = 2348.7700, y = -5758.2800, z = 153.9222, id = 28658 },
    TurnIn = { x = 2348.7700, y = -5758.2800, z = 153.9222, id = 28658 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12687,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12698, 1) then
			GMR.SetQuestingState(nil);
            local itemName = GetItemInfo(39253)
			local enemy = GMR.GetObjectWithInfo({ id = 28819, rawType = 5, isAlive = true, canAttack = true })
			local GHOUL = GMR.GetObjectWithInfo({ id = 28845, rawType = 5, isAlive = true })
            local x1, y1, z1 = GMR.ObjectPosition(enemy)
            if GHOUL then
                if GMR.ObjectExists(GHOUL) then
                    if GMR.GetDistance("player", npc2, "<", 10) then
                        if not GMR.IsPlayerPosition(2348.7020, -5757.9360, 153.9229, 3) then
                            GMR.MeshTo(2348.7020, -5757.9360, 153.9229)
                        end
                    end
                end
			elseif enemy then
                if GMR.InLoS(enemy) then
                    if GMR.GetDistance("player", enemy, "<", 25) then
                        GMR.Use(itemName)
                        GMR.ClickPosition(x1, y1, z1)
                    end
                else
                    if not GMR.IsPlayerPosition(x1, y1, z1, 13) then
                        GMR.Mesh(x1, y1, z1)
                    else
                        GMR.Use(itemName)
                        GMR.ClickPosition(x1, y1, z1)
                    end
                end
            else
                GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2440.8500, -5910.8870, 102.2988,50)',
		'GMR.DefineProfileCenter(2426.0920, -5938.9060, 95.2391,50)',
		'GMR.DefineProfileCenter(2435.3670, -5971.4490, 95.7675,50)',
		'GMR.DefineProfileCenter(2489.9250, -5977.8020, 95.0542,50)',
		'GMR.DefineProfileCenter(2520.3760, -5996.4420, 102.9741,50)',
		'GMR.DefineProfileCenter(2518.9740, -5942.3390, 110.2863,50)',
		'GMR.DefineProfileCenter(2470.9480, -5941.4120, 114.7902,50)',
		'GMR.DefineProfileCenter(2408.5950, -5918.6410, 110.5943,50)',
		'GMR.DefineProfileCenter(2441.2270, -5912.5140, 102.3153,50)',
		'GMR.DefineSettings("Enable", { "Herbs", "Ores", "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28819)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 An Attack Of Opportunity |r",
    QuestID = 12700,
    QuestType = "TalkTo",
    PickUp = { x = 2348.7700, y = -5758.2800, z = 153.9222, id = 28658 },
    TurnIn = { x = 2371.3700, y = -5701.0400, z = 153.9218, id = 28377 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12698,
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2371.3700, -5701.0400, 153.9220, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28377)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Massacre At Light's Point (Manual Completion Required) |r",
    QuestID = 12701,
    QuestType = "Custom",
    PickUp = { x = 2371.3700, y = -5701.0400, z = 153.9218, id = 28377 },
    TurnIn = { x = 2371.3700, y = -5701.0400, z = 153.9218, id = 28377 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12700,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12701, 1) then
			GMR.SetQuestingState("Idle")
		end
    ]],
    Profile = {
        [[if not GMR.Frames.Rand008 then
            GMR.Print("Frame created")
            GMR.Frames.Rand008 = CreateFrame("frame")
            GMR.Frames.Rand008:SetScript("OnUpdate", function(self)
                if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12701 then
                    if GMR.IsExecuting() then
                        local scarletCanon = GetSpellInfo(52435)
                        local magnetPulse = GetSpellInfo(52576)
                        local gryphonESCAPE = GetSpellInfo(52588)

                        if GMR.IsQuestActive(12701) and not GMR.Questing.IsObjectiveCompleted(12701, 1) then
                            if not GMR.IsPlayerPosition(2263.6010, -6199.1410, 13.1270, 120) and not GMR.IsPlayerPosition(2260.0080, -6181.6000, -0.9033, 120) then
                                if not GMR.IsInVehicle() then
                                    if not GMR.IsPlayerPosition(2390.5920, -5898.8180, 109.0345, 3) then
                                        GMR.MeshTo(2390.5920, -5898.8180, 109.0345)
                                    else
                                        local Cart = GMR.GetObjectWithInfo({ id = 190767, rawType = 8 })
                                        if Cart then
                                            GMR.Questing.InteractWith(Cart, nil, nil, nil, nil, 5)
                                        end
                                    end
                                end
                            elseif GMR.IsPlayerPosition(2263.6010, -6199.1410, 13.1270, 120) or GMR.IsPlayerPosition(2260.0080, -6181.6000, -0.9033, 120) then
                                if not GMR.IsInVehicle() then
                                    local cannon = GMR.GetObjectWithInfo({ id = 28833, rawType = 5 })
                                    if cannon then
                                        GMR.Questing.InteractWith(cannon, nil, nil, nil, nil, 3)
                                    end
                                elseif UnitInVehicle("player") then
                                    GMR.Cast(scarletCanon)
                                    OverrideActionBarButton1:Click()
                                    local enemyCLOSE = GMR.GetObjectWithInfo({ id = { 28834, 28850 }, rawType = 5, isAlive = true, distance < 15 })
                                    local npc1 = GMR.GetObjectWithInfo({ id = { 28834, 28850 }, rawType = 5, isAlive = true })
                                    if GetSpellCooldown(52435) == 0 and UnitExists("target") then
                                        GMR.Cast(scarletCanon)
                                    elseif enemyCLOSE then
                                        GMR.Cast(magnetPulse)
                                        OverrideActionBarButton2:Click()
                                        VehicleMenuBarActionButton2:Click()
                                    elseif npc1 then
                                        GMR.TargetUnit(npc1)
                                        GMR.Cast(scarletCanon)
                                        OverrideActionBarButton1:Click()
                                    end
                                end
                            end
                        elseif GMR.Questing.IsObjectiveCompleted(12701, 1) and UnitInVehicle("player") then
                            if GMR.IsPlayerPosition(2263.6010, -6199.1410, 13.1270, 60) or GMR.IsPlayerPosition(2260.0080, -6181.6000, -0.9033, 60) then
                                GMR.Cast(gryphonESCAPE)
                                OverrideActionBarButton5:Click()
                                VehicleMenuBarActionButton5:Click()
                            end
                        end
                    end
                else 
                    self:SetScript("OnUpdate", nil); GMR.Frames.Rand008 = nil; GMR.Print("Frame deleted")
                end
            end)
        end]],
        'GMR.SetChecked("ClickToMove", true)',
		'GMR.SetChecked("Mount", false)',
		'GMR.SetChecked("FlyingMount", false)',
        'GMR.DenySpeedUp()',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(2440.8500, -5910.8870, 102.2988,50)',
		'GMR.DefineProfileCenter(2426.0920, -5938.9060, 95.2391,50)',
		'GMR.DefineProfileCenter(2435.3670, -5971.4490, 95.7675,50)',
		'GMR.DefineProfileCenter(2489.9250, -5977.8020, 95.0542,50)',
		'GMR.DefineProfileCenter(2520.3760, -5996.4420, 102.9741,50)',
		'GMR.DefineProfileCenter(2518.9740, -5942.3390, 110.2863,50)',
		'GMR.DefineProfileCenter(2470.9480, -5941.4120, 114.7902,50)',
		'GMR.DefineProfileCenter(2408.5950, -5918.6410, 110.5943,50)',
		'GMR.DefineProfileCenter(2441.2270, -5912.5140, 102.3153,50)',
		'GMR.DefineSettings("Enable", { "Herbs", "Ores", "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId()',
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Victory At Death's Breach! |r",
    QuestID = 12706,
    QuestType = "Custom",
    PickUp = { x = 2371.3700, y = -5701.0400, z = 153.9218, id = 28377 },
    TurnIn = { x = 2460.5000, y = -5593.4700, z = 367.3937, id = 28444 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12701,
    Lua = [[
        if GMR.IsQuestActive(12706) and not GMR.IsPlayerPosition(2412.5860, -5626.7970, 376.9059, 100) then
            if not GMR.IsPlayerPosition(2403.0620, -5727.3450, 153.9255, 3) then 
                GMR.MeshTo(2403.0620, -5727.3450, 153.9255)
            else
                local npc = GMR.GetObjectWithInfo({ id = 29501, rawType = 5, distance = 10 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
            end
        elseif GMR.IsQuestActive(12706) and GMR.IsPlayerPosition(2412.5860, -5626.7970, 376.9059, 100) then
            if not GMR.IsPlayerPosition(2460.5000, -5593.4700, 367.3937, 3) then 
                GMR.MeshTo(2460.5000, -5593.4700, 367.3937)
            else
                local npc = GMR.GetObjectWithInfo({ id = 28444, rawType = 5 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
            end
        end
    ]],
    Profile = {
    [[
        if not GMR.Frames.Rand009 then
            GMR.Print("Frame created")
            GMR.Frames.Rand009 = CreateFrame("frame")
            GMR.Frames.Rand009:SetScript("OnUpdate", function(self)
                if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12706 then
                    if GMR.IsExecuting() then
                        if GMR.IsQuestActive(12706) and not GMR.IsPlayerPosition(2412.5860, -5626.7970, 376.9059, 100) then
                            if not GMR.IsPlayerPosition(2403.0620, -5727.3450, 153.9255, 3) then 
                                GMR.MeshTo(2403.0620, -5727.3450, 153.9255)
                            else
                                local npc = GMR.GetObjectWithInfo({ id = 29501, rawType = 5, distance = 10 })
                                if npc then
                                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                end
                            end
                        elseif GMR.IsQuestActive(12706) and GMR.IsPlayerPosition(2412.5860, -5626.7970, 376.9059, 100) then
                            if not GMR.IsPlayerPosition(2460.5000, -5593.4700, 367.3937, 3) then 
                                GMR.MeshTo(2460.5000, -5593.4700, 367.3937)
                            else
                                local npc = GMR.GetObjectWithInfo({ id = 28444, rawType = 5 })
                                if npc then
                                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                end
                            end
                        end
                    end
                else 
                    self:SetScript("OnUpdate", nil); GMR.Frames.Rand009 = nil; GMR.Print("Frame deleted")
                end
            end)
        end
        ]],
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.SetChecked("Mount", true)',
        'GMR.AllowSpeedUp()',
		'GMR.SkipTurnIn(false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(2374.1310, -5652.2270, 382.4217, 20)',
        'GMR.DefineUnstuck(2387.5480, -5650.0370, 381.2832)',
        'GMR.DefineUnstuck(2399.0440, -5645.2300, 377.1093)',
        'GMR.DefineUnstuck(2408.8870, -5633.7890, 376.8947)',
        'GMR.DefineUnstuck(2416.5710, -5624.0870, 376.2773)',
        'GMR.DefineUnstuck(2426.7390, -5616.8870, 369.1700)',
        'GMR.DefineUnstuck(2432.1390, -5613.0780, 367.2783)',
        'GMR.DefineUnstuck(2440.8330, -5606.9450, 366.8185)',
        'GMR.DefineUnstuck(2450.5580, -5600.0860, 367.4255)',
		'GMR.DefineProfileCenter(2460.5000, -5593.4700, 367.3937, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28444)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Will Of The Lich King |r",
    QuestID = 12714,
    QuestType = "Custom",
    PickUp = { x = 2460.5000, y = -5593.4700, z = 367.3937, id = 28444 },
    TurnIn = { x = 2316.6500, y = -5738.6100, z = 156.0240, id = 28907 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12706,
    Lua = [[
		if GMR.IsQuestActive(12714) and GMR.IsPlayerPosition(2414.9710, -5624.9050, 376.8325, 100) then
            GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(2348.7400, -5694.9400, 384.0880, 3) then 
                GMR.MeshTo(2348.7400, -5694.9400, 384.0880)
            else
                local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5, distance = 15 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
            end
		end
    ]],
    Profile = {
        [[
        if not GMR.Frames.Rand010 then
            GMR.Print("Frame created")
            GMR.Frames.Rand010 = CreateFrame("frame")
            GMR.Frames.Rand010:SetScript("OnUpdate", function(self)
                if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12714 then
                    if GMR.IsExecuting() then
                        if GMR.IsQuestActive(12714) and GMR.IsPlayerPosition(2414.9710, -5624.9050, 376.8325, 100) then
                            if not GMR.IsPlayerPosition(2348.7400, -5694.9400, 384.0880, 3) then 
                                GMR.MeshTo(2348.7400, -5694.9400, 384.0880)
                            else
                                local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5, distance = 15 })
                                if npc then
                                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                end
                            end
                        end
                    end
                else 
                    self:SetScript("OnUpdate", nil); GMR.Frames.Rand010 = nil; GMR.Print("Frame deleted")
                end
            end)
        end
        ]],
		'GMR.SkipTurnIn(false)',
        -- In the Sky (Lower level) > To BIRD (GO DOWN)
		'GMR.DefineProfileCenter(2316.6500, -5738.6100, 156.0240, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28907)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 The Crypt of Remembrance / The Plaguebringers Request |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 12715, x = 2316.6500, y = -5738.6100, z = 156.0240, id = 28907 },
      { questId = 12716, x = 1982.7400, y = -5815.5400, z = 100.8918, id = 28919 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12714,
    Profile = {
		'GMR.SkipTurnIn(true)',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
    }
  })



  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Crypt of Remembrance |r",
    QuestID = 12715,
    QuestType = "TalkTo",
    PickUp = { x = 2316.6500, y = -5738.6100, z = 156.0240, id = 28907 },
    TurnIn = { x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12714,
    Profile = {
		'GMR.SkipTurnIn(false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(1981.8780, -5815.3030, 100.9228, 30)',
        'GMR.DefineUnstuck(1965.1690, -5811.7620, 101.1262)',
        'GMR.DefineUnstuck(1948.4050, -5808.2090, 100.8217)',
        'GMR.DefineUnstuck(1931.1330, -5807.8740, 100.2347)',
        'GMR.DefineUnstuck(1913.5610, -5807.5850, 100.5350)',
        'GMR.DefineUnstuck(1893.5770, -5809.1880, 101.3409)',
        'GMR.DefineUnstuck(1875.1030, -5810.7020, 101.3160)',
        'GMR.DefineUnstuck(1860.9270, -5811.8640, 100.3821)',
        'GMR.DefineUnstuck(1862.0660, -5792.5600, 101.9361)',
        'GMR.DefineUnstuck(1862.6790, -5777.2270, 103.8325)',
        'GMR.DefineUnstuck(1861.1480, -5762.5370, 105.0629)',
        'GMR.DefineUnstuck(1861.2950, -5753.0420, 100.3470)',
        'GMR.DefineUnstuck(1861.6420, -5743.9710, 95.5280)',
        'GMR.DefineUnstuck(1864.6880, -5742.5640, 95.5179)',
        'GMR.DefineUnstuck(1870.4030, -5742.7780, 92.0250)',
        'GMR.DefineUnstuck(1877.0410, -5743.0170, 89.9820)',
        'GMR.DefineUnstuck(1877.1540, -5749.4950, 88.1483)',
        'GMR.DefineUnstuck(1878.1370, -5757.3200, 83.8950)',
        'GMR.DefineUnstuck(1879.2860, -5764.3360, 83.7670)',
		'GMR.DefineProfileCenter(1879.2860, -5764.3360, 83.7670, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28911)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 Nowhere to Run and Nowhere to Hide / Lambs to the Slaughter |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 12719, x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
      { questId = 12722, x = 1876.4400, y = -5777.1400, z = 85.0192, id = 28910 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12715,
    Profile = {
		'GMR.SkipTurnIn(true)',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Plaguebringer's Request |r",
    QuestID = 12716,
    QuestType = "Custom",
    PickUp = { x = 1982.7400, y = -5815.5400, z = 100.8918, id = 28919 },
    TurnIn = { x = 1982.7400, y = -5815.5400, z = 100.8918, id = 28919 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12715,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12716, 1) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1784.1500, -5876.0430, 109.4964, 3) then 
				GMR.MeshTo(1784.1500, -5876.0430, 109.4964)
			else 
				local object = GMR.GetObjectWithInfo({ id = 190937, rawType = 8 })
				if not GMR.GetDelay("CustomQuest") and object then 
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
				end 
			end
		elseif not GMR.Questing.IsObjectiveCompleted(12716, 2) then
			GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1817.0960, -6011.0860, 117.6584, 3) then 
				GMR.MeshTo(1817.0960, -6011.0860, 117.6584)
			else 
				local object = GMR.GetObjectWithInfo({ id = 190938, rawType = 8 })
				if not GMR.GetDelay("CustomQuest") and object then 
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
				end 
			end
        elseif not GMR.Questing.IsObjectiveCompleted(12716, 3) then
			local enemy = GMR.GetObjectWithInfo({ id = { 28610, 28936, 28939, 28940 }, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
    ]],
    Profile = {
		'GMR.SkipTurnIn(false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(1875.7000, -5767.1500, 83.7903, 20)',
        'GMR.DefineUnstuck(1876.3780, -5754.7010, 84.6989)',
        'GMR.DefineUnstuck(1876.5700, -5743.5610, 89.9881)',
        'GMR.DefineUnstuck(1862.4660, -5743.6260, 95.5264)',
        'GMR.DefineUnstuck(1862.1970, -5754.1130, 101.1294)',
        'GMR.DefineUnstuck(1862.1350, -5764.2000, 105.0768)',
        'GMR.DefineUnstuck(1861.9640, -5781.2510, 103.3247)',
        'GMR.DefineUnstuck(1861.3990, -5812.6620, 100.3570)',
		'GMR.DefineProfileCenter(1824.6390, -5932.3450, 112.8276,80)',
		'GMR.DefineProfileCenter(1815.6340, -5823.0970, 104.7429,80)',
		'GMR.DefineProfileCenter(1692.1420, -5780.0380, 113.8851,80)',
		'GMR.DefineProfileCenter(1551.3590, -5885.2500, 125.9852,80)',
		'GMR.DefineProfileCenter(1457.9250, -5852.6920, 131.2484,80)',
		'GMR.DefineProfileCenter(1656.1700, -5944.3700, 125.1105,60)',
        'GMR.DefineAreaBlacklist(1565.8930, -5754.0160, 120.1137, 60)',
        'GMR.DefineAreaBlacklist(1584.3570, -5718.2650, 121.6801, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
        -- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28610)',
		'GMR.DefineQuestEnemyId(28936)',
		'GMR.DefineQuestEnemyId(28939)',
		'GMR.DefineQuestEnemyId(28940)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Noth's Special Brew |r",
    QuestID = 12717,
    QuestType = "TalkTo",
    PickUp = { x = 1982.7400, y = -5815.5400, z = 100.8918, id = 28919 },
    TurnIn = { x = 1990.4680, y = -5824.2850, z = 104.2896, id = 190936 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12716,
    Lua = [[
		if GMR.IsQuestActive(12717) and GMR.IsQuestCompletable(12717) then
            GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1990.4680, -5824.2850, 104.2896, 3) then 
                GMR.MeshTo(1990.4680, -5824.2850, 104.2896)
            else
                local object = GMR.GetObjectWithInfo({ id = 190936, rawType = 8 })
                if object then
                    GMR.Questing.InteractWith(object, nil, nil, nil, nil, 8)
                end
            end
		end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand011 then
                GMR.Print("Frame created")
                GMR.Frames.Rand011 = CreateFrame("frame")
                GMR.Frames.Rand011:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12717 then
                        if GMR.IsExecuting() then
                            if GMR.IsQuestActive(12717) and GMR.IsQuestCompletable(12717) then
                                if QuestFrameProgressPanel:IsShown() then
                                    QuestFrameCompleteButton:Click()
                                elseif not GMR.IsPlayerPosition(1990.4680, -5824.2850, 104.2896, 3) then 
                                    GMR.MeshTo(1990.4680, -5824.2850, 104.2896)
                                else
                                    local object = GMR.GetObjectWithInfo({ id = 190936, rawType = 8 })
                                    if object then
                                        GMR.Questing.InteractWith(object, nil, nil, nil, nil, 8)
                                    end
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand011 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end
            ]],
            'GMR.SkipTurnIn(false)',
            'GMR.DefineProfileCenter(1990.4680, -5824.2850, 104.2896, 60)',
            'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
            -- Deaths Reach
            'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
            'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
            'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
            'GMR.DefineQuestEnemyId(190936)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Nowhere To Run And Nowhere To Hide |r",
    QuestID = 12719,
    QuestType = "Custom",
    PickUp = { x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    TurnIn = { x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12717,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12719, 1) then
            local enemy = GMR.GetObjectWithInfo({ id = 28945, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(12719, 2) then
            if not GMR.IsPlayerPosition(1590.0590, -5706.1830, 123.0406, 3) then 
                GMR.MeshTo(1590.0590, -5706.1830, 123.0406)
            else
                local object = GMR.GetObjectWithInfo({ id = 190947, rawType = 8, isInteractable = true })
                if object then
                    GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
                end
            end
		end
    ]],
    Profile = {
        'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1576.7070, -5703.4780, 121.6812, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "Skinning", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28945)',
        'GMR.DefineCustomObjectId(190947)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 How To Win Friends And Influence Enemies |r",
    QuestID = 12720,
    QuestType = "Custom",
    PickUp = { x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    TurnIn = { x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12717,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12720, 1) then
            GMR.SetQuestingState(nil);
            local equipSWORDS = GetItemInfo(38607)
            local itemName = GetItemInfo(38607)
            local openBOX = GetItemInfo(39418)
            local interrogatedDEBUFF = GetSpellInfo(52781)
            if GMR.HasDebuff("target", interrogatedDEBUFF) then
                if not GMR.GetDelay("CustomQuest") then
                    GMR.RunMacroText("/stopattack")
                    GMR.SetDelay("CustomQuest", 15)
                    GMR.SetDelay("execute", 15)
                end
            else
                if GetItemCount(openBOX) >= 1 then
                    GMR.Use(openBOX)
                elseif GetItemInfo(GetInventoryItemID("player", 16) or 0) == "Keleseth's Persuader" == false then
                    if GMR.IsQuestActive(12720) and not GMR.Questing.IsObjectiveCompleted(12720, 1) then
                        if GetItemCount(39371) >= 1 then
                            GMR.RunMacroText("/equipslot 16 Keleseth's Persuader")
                        end
                    end
                elseif GetItemInfo(GetInventoryItemID("player", 17) or 0) == "Keleseth's Persuader" == false then
                    if GMR.IsQuestActive(12720) and not GMR.Questing.IsObjectiveCompleted(12720, 1) then
                        if GetItemCount(39371) >= 1 then
                            GMR.RunMacroText("/equipslot 17 Keleseth's Persuader")
                        end
                    end
                else
                    GMR.SetQuestingState("Idle")
                end
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Combat", false)',
        'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(1815.6340, -5823.0970, 104.7429,80)',
		'GMR.DefineProfileCenter(1692.1420, -5780.0380, 113.8851,80)',
		'GMR.DefineProfileCenter(1551.3590, -5885.2500, 125.9852,80)',
		'GMR.DefineProfileCenter(1457.9250, -5852.6920, 131.2484,80)',
		'GMR.DefineProfileCenter(1656.1700, -5944.3700, 125.1105,60)',
		'GMR.DefineProfileCenter(1676.6690, -5840.7800, 116.1542,60)',
        'GMR.DefineAreaBlacklist(1565.8930, -5754.0160, 120.1137, 60)',
        'GMR.DefineAreaBlacklist(1584.3570, -5718.2650, 121.6801, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
        -- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28610)',
		'GMR.DefineQuestEnemyId(28936)',
		'GMR.DefineQuestEnemyId(28939)',
		'GMR.DefineQuestEnemyId(28940)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Lambs to the Slaughter |r",
    QuestID = 12722,
    QuestType = "Custom",
    PickUp = { x = 1876.4400, y = -5777.1400, z = 85.0192, id = 28910 },
    TurnIn = { x = 1876.4400, y = -5777.1400, z = 85.0192, id = 28910 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12717,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12722, 1) then
			local enemy = GMR.GetObjectWithInfo({ id = { 28939, 28610, 28940, 28936 }, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(12722, 2) then
			local enemy = GMR.GetObjectWithInfo({ id = { 28942, 28941 }, canAttack = true, isAlive = true })
			if enemy then
				GMR.SetQuestingState(nil)
				GMR.Questing.KillEnemy(enemy)
			else
				GMR.SetQuestingState("Idle")
			end
		end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand013 then
                GMR.Print("Frame created")
                GMR.Frames.Rand013 = CreateFrame("frame")
                GMR.Frames.Rand013:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12722 then
                        if GMR.IsExecuting() then
                            local equipSWORDS = GetItemInfo(38607)
                            local itemName = GetItemInfo(38607)
                            local interrogatedDEBUFF = GetSpellInfo(52781)
                            if GMR.IsQuestActive(12722) then
                                if GetItemCount(38707) >= 1 then
                                    if GetItemInfo(GetInventoryItemID("player", 16) or 0) == "Runed Soulblade" == false then
                                        GMR.Print("Equipping our Main Weapon")
                                        GMR.Use(equipSWORDS)
                                        GMR.RunMacroText("/equipslot 16 Runed Soulblade")
                                    end
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand013 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end
        ]],
        'GMR.SetChecked("Combat", true)',
        'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(1824.6390, -5932.3450, 112.8276,80)',
		'GMR.DefineProfileCenter(1815.6340, -5823.0970, 104.7429,80)',
		'GMR.DefineProfileCenter(1692.1420, -5780.0380, 113.8851,80)',
		'GMR.DefineProfileCenter(1551.3590, -5885.2500, 125.9852,80)',
		'GMR.DefineProfileCenter(1457.9250, -5852.6920, 131.2484,80)',
		'GMR.DefineProfileCenter(1656.1700, -5944.3700, 125.1105,60)',
		'GMR.DefineProfileCenter(1676.6690, -5840.7800, 116.1542,60)',
        'GMR.DefineAreaBlacklist(1565.8930, -5754.0160, 120.1137, 60)',
        'GMR.DefineAreaBlacklist(1584.3570, -5718.2650, 121.6801, 80)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
        -- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28941)',
		'GMR.DefineQuestEnemyId(28942)',
		'GMR.DefineQuestEnemyId(28610)',
		'GMR.DefineQuestEnemyId(28936)',
		'GMR.DefineQuestEnemyId(28939)',
		'GMR.DefineQuestEnemyId(28940)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFFFFAB48 How To Win Friends And Influence Enemies / Lambs to the Slaughter |r",
    QuestType = "MassTurnIn",
    MassPickUp = {
      { questId = 12722, x = 1876.4400, y = -5777.1400, z = 85.0192, id = 28910 },
      { questId = 12720, x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12717,
    Profile = {
		'GMR.SkipTurnIn(false)',
        -- Deaths Reach
        'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
        'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
        'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Behind Scarlet Lines |r",
    QuestID = 12723,
    QuestType = "TalkTo",
    PickUp = { x = 1879.4000, y = -5765.1000, z = 83.7753, id = 28911 },
    TurnIn = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12722,
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1403.9400, -5826.6200, 137.1914, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28914)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 Brothers In Death / The Path Of The Righteous Crusader |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 12724, x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
      { questId = 12725, x = 1397.5100, y = -5827.4200, z = 137.1880, id = 28913 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12723,
    Profile = {
		'GMR.SkipTurnIn(true)',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Brothers In Death |r",
    QuestID = 12725,
    QuestType = "TalkTo",
    PickUp = { x = 1397.5100, y = -5827.4200, z = 137.1880, id = 28913 },
    TurnIn = { x = 1655.9700, y = -6038.7600, z = 128.6392, id = 28912 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12723,
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1655.9700, -6038.7600, 128.6392, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28914)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Bloody Breakout |r",
    QuestID = 12727,
    QuestType = "Custom",
    PickUp = { x = 1655.9700, y = -6038.7600, z = 128.6392, id = 28912 },
    TurnIn = { x = 1655.9700, y = -6038.7600, z = 128.6392, id = 28912 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12725,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12727, 1) then
			GMR.SetQuestingState(nil);
            local magicShield = GetSpellInfo(52894)
            local Remains = GMR.GetObjectWithInfo({ id = 191092, rawType = 8 })
			local npc2 = GMR.GetObjectWithInfo({ id = 29001, rawType = 5 })

            if Remains then
                GMR.Questing.InteractWith(Remains, nil, nil, nil, nil, 5)
            else
                if not GMR.HasBuff("player", magicShield) then
                    if not GMR.IsPlayerPosition(1653.2110, -6035.7700, 127.5840, 2) then 
                        GMR.MeshTo(1653.2110, -6035.7700, 127.5840)
                    elseif GMR.IsMoving() then 
                        GMR.StopMoving()
                    else
                        if npc2 then
                            if GMR.GetDistance("player", npc2, "<", 10) then
                                GMR.SetQuestingState(nil)
                                GMR.Questing.KillEnemy(npc2)
                            end
                        else
                            GMR.SetQuestingState("Idle")
                        end
                    end
                end
            end
		end
    ]],
    Profile = {
		'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(1651.3730, -6037.3610, 127.5835, 20)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29001)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Path Of The Righteous Crusader |r",
    QuestID = 12724,
    QuestType = "Custom",
    PickUp = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    TurnIn = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12725,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12724, 1) then
            GMR.SetQuestingState(nil);
			if not GMR.IsPlayerPosition(1647.4600, -6037.7690, 149.9563, 3) then 
                GMR.MeshTo(1647.4600, -6037.7690, 149.9563)
            else
                local object = GMR.GetObjectWithInfo({ id = 191084, rawType = 8, isInteractable = true })
                if object then
                    GMR.Questing.InteractWith(object, nil, nil, nil, nil, 8)
                end
            end
		end
    ]],
    Profile = {
		'GMR.SkipTurnIn(true)',
		'GMR.DefineProfileCenter(1647.4760, -6037.5810, 149.9563, 60)',
		'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(191084)',
        'GMR.DefineCustomObjectId(191084)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFFFFAB48 The Path Of The Righteous Crusader / Bloody Breakout |r",
    QuestType = "MassTurnIn",
    MassPickUp = {
      { questId = 12724, x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
      { questId = 12727, x = 1397.5100, y = -5827.4200, z = 137.1880, id = 28913 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12725,
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
        -- Deaths Reach
        'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
        'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
        'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Cry For Vengeance! |r",
    QuestID = 12738,
    QuestType = "TalkTo",
    PickUp = { x = 1397.5100, y = -5827.4200, z = 137.1880, id = 28913 },
    TurnIn = { x = 1359.8130, y = -5724.2370, z = 136.4147, id = 29053 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12727,
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1374.9580, -5717.4260, 136.4599, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28914)'
    }
  })




  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (HUMAN) |r",
    QuestID = 12742,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Human" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12742, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29061, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1320.9715576172, -5740.9619140625, 137.47155761719, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29061)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (UNDEAD) |r",
    QuestID = 12750,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Scourge" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12750, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29071, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1331.3032226562, -5740.9721679688, 137.57695007324, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29071)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (Draenei) |r",
    QuestID = 12746,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Draenei" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12746, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29070, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1328.6749267578, -5773.0366210938, 137.76490783691, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29070)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (ORC) |r",
    QuestID = 12748,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Orc" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12748, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29072, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1331.8696289062, -5748.4365234375, 137.40211486816, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29072)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (GNOME) |r",
    QuestID = 12745,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Gnome" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12745, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29068, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1320.4420166016, -5753.361328125, 137.39694213867, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29068)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (DWARF) |r",
    QuestID = 12744,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Dwarf" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12744, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29067, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1331.7293701172, -5756.236328125, 137.49369812012, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29067)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (BloodElf) |r",
    QuestID = 12747,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "BloodElf" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12747, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29074, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1331.4768066406, -5764.6650390625, 137.82130432129, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29074)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (TROLL) |r",
    QuestID = 12749,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Troll" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12749, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29073, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1321.2545166016, -5749.3510742188, 137.37097167969, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29073)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (NIGHTELF) |r",
    QuestID = 12743,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "NightElf" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12743, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29065, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1318.3688964844, -5763.4770507812, 137.80867004394, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29065)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Special Surprise (TAUREN) |r",
    QuestID = 12739,
    QuestType = "Custom",
    PickUp = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    TurnIn = { x = 1348.9093017578, y = -5717.3046875, z = 136.41529846191, id = 29053 },
    Faction = { "Tauren" },
    Class = "DEATHKNIGHT",
    PreQuest = 12738,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12739, 1) then
            local npc = GMR.GetObjectWithInfo({ id = 29032, rawType = 5, isAlive = true })
			local x, y, z = GMR.ObjectPosition(npc)
			if npc then
                if not GMR.IsPlayerPosition(x, y, z, 3) then
                    GMR.Mesh(x, y, z)
                elseif GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
			else
				GMR.SetQuestingState("Idle")
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1319.3231201172, -5771.8408203125, 137.74580383301, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29032)'
    }
  })


  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Sort Of Homecoming |r",
    QuestID = 12751,
    QuestType = "TalkTo",
    PickUp = { x = 1363.1300, y = -5723.8160, z = 136.4153, id = 29053 },
    TurnIn = { x = 1397.5100, y = -5827.4200, z = 137.1880, id = 28913 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12742,
    Profile = {
        'GMR.SetChecked("Mount", true)',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1397.5100, -5827.4200, 137.1894, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(28914)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Ambush At The Overlook |r",
    QuestID = 12754,
    QuestType = "Custom",
    PickUp = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    TurnIn = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12751,
    Lua = [[
		if not GMR.Questing.IsObjectiveCompleted(12754, 1) then
            GMR.SetQuestingState(nil);
            if not GMR.IsPlayerPosition(1504.0490, -6010.9440, 117.1591, 3) then 
                GMR.MeshTo(1504.0490, -6010.9440, 117.1591)
            else
                local itemname = GetItemInfo(39645)
                local treeBuff = GetSpellInfo(53061)
                if not GMR.HasBuff("player", treeBuff) then
                    GMR.Use(itemname)
                elseif GMR.HasBuff("player", treeBuff) then
                    local npc1 = GMR.GetObjectWithInfo({ id = 29076, rawType = 5, isAlive = true })
                    if npc1 then
                        GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
                    else
                        GMR.SetQuestingState("Idle")
                    end
                end
            end
		end
    ]],
    Profile = {
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.SetChecked("Mount", true)',
        'GMR.DenySpeedUp()',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1504.0490, -6010.9440, 117.1591, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(29076)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Meeting With Fate |r",
    QuestID = 12755,
    QuestType = "TalkTo",
    PickUp = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    TurnIn = { x = 1320.4100, y = -6124.4100, z = 14.6751, id = 29077 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12754,
    Profile = {
        'GMR.SetChecked("ClickToMove", false)',
		'GMR.SetChecked("Mount", true)',
        'GMR.AllowSpeedUp()',
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1320.4100, -6124.4100, 14.6751, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(99999)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Scarlet Onslaught Emerges |r",
    QuestID = 12756,
    QuestType = "TalkTo",
    PickUp = { x = 1320.4100, y = -6124.4100, z = 14.6751, id = 29077 },
    TurnIn = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12755,
    Profile = {
		'GMR.SkipTurnIn(false)',
		'GMR.DefineProfileCenter(1403.9400, -5826.6200, 137.1910, 60)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineQuestEnemyId(99999)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Scarlet Armies Approach... |r",
    QuestID = 12757,
    QuestType = "Custom",
    PickUp = { x = 1403.9400, y = -5826.6200, z = 137.1914, id = 28914 },
    TurnIn = { x = 2460.5000, y = -5593.4700, z = 367.3937, id = 28444 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12756,
    Lua = [[
        if GMR.IsQuestActive(12757) and not GMR.IsPlayerPosition(2399.0700, -5635.1500, 377.0350, 200) then
            GMR.SetQuestingState(nil);
            local object = GMR.GetObjectWithInfo({ id = 191155, rawType = 8 })
            local npc = GMR.GetObjectWithInfo({ id = 28914, rawType = 5 })
            if not GMR.IsPlayerPosition(1401.0900, -5824.4540, 137.1877, 10) then 
                GMR.MeshTo(1401.0900, -5824.4540, 137.1877)
            else
                if not object then
                    GossipTitleButton1:Click()
                    GMR.Questing.GossipWith(npc, nil, nil, nil, nil, 5)
                elseif object then
                    GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
                else
                    GMR.SetQuestingState("Idle")
                end
            end
        elseif GMR.IsQuestActive(12757) and GMR.IsPlayerPosition(2399.0700, -5635.1500, 377.0350, 200) then
            GMR.SetQuestingState(nil);
            if not GMR.IsPlayerPosition(2460.5000, -5593.4700, 367.3937, 3) then 
                GMR.MeshTo(2460.5000, -5593.4700, 367.3937)
            else
                local npc = GMR.GetObjectWithInfo({ id = 28444, rawType = 5 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
            end
        end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand014 then
                GMR.Print("Frame created")
                GMR.Frames.Rand014 = CreateFrame("frame")
                GMR.Frames.Rand014:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12757 then
                        if GMR.IsExecuting() then
                            if GMR.IsQuestActive(12757) and not GMR.IsPlayerPosition(2399.0700, -5635.1500, 377.0350, 200) then
                                local object = GMR.GetObjectWithInfo({ id = 191155, rawType = 8 })
                                local npc = GMR.GetObjectWithInfo({ id = 28914, rawType = 5 })
                                if not GMR.IsPlayerPosition(1401.0900, -5824.4540, 137.1877, 10) then 
                                    GMR.MeshTo(1401.0900, -5824.4540, 137.1877)
                                else
                                    if not object then
                                        GMR.Questing.GossipWith(npc, nil, nil, nil, nil, 5)
                                    elseif object then
                                        GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
                                    end
                                end
                            elseif GMR.IsQuestActive(12757) and GMR.IsPlayerPosition(2399.0700, -5635.1500, 377.0350, 200) then
                                if not GMR.IsPlayerPosition(2460.5000, -5593.4700, 367.3937, 3) then 
                                    GMR.MeshTo(2460.5000, -5593.4700, 367.3937)
                                else
                                    local npc = GMR.GetObjectWithInfo({ id = 28444, rawType = 5 })
                                    if npc then
                                        GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                    end
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand014 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end
            ]],
            'GMR.SkipTurnIn(false)',
            'GMR.DefineProfileCenter(1401.0900, -5824.4540, 137.1877, 60)',
            'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
            -- Deaths Reach
            'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
            'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
            'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)',
            'GMR.DefineQuestEnemyId(99999)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Scarlet Apocalypse |r",
    QuestID = 12778,
    QuestType = "Custom",
    PickUp = { x = 2460.5000, y = -5593.4700, z = 367.3944, id = 28444 },
    TurnIn = { x = 2310.2668457031, y = -5742.2124023438, z = 161.00743103027, id = 29110 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12757,
    Lua = [[
        if GMR.IsQuestActive(12778) and GMR.IsPlayerPosition(2414.9710, -5624.9050, 376.8325, 100) then
            if not GMR.IsPlayerPosition(2348.7400, -5694.9400, 384.0880, 3) then 
                GMR.MeshTo(2348.7400, -5694.9400, 384.0880)
            else
                local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5, distance = 15 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
            end
        elseif not GMR.Questing.IsObjectiveCompleted(12778, 1) then
            if not GMR.IsPlayerPosition(2310.2668457031, -5742.2124023438, 161.00743103027, 3) then 
                GMR.MeshTo(2310.2668457031, -5742.2124023438, 161.00743103027)
            else
                local npc = GMR.GetObjectWithInfo({ id = 29110, rawType = 5 })
                if npc then
                    GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                end
            end
        end
    ]],
    Profile = {
        [[
            if not GMR.Frames.Rand014 then
                GMR.Print("Frame created")
                GMR.Frames.Rand014 = CreateFrame("frame")
                GMR.Frames.Rand014:SetScript("OnUpdate", function(self)
                    if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12778 then
                        if GMR.IsExecuting() then
                            if GMR.IsQuestActive(12778) and GMR.IsPlayerPosition(2414.9710, -5624.9050, 376.8325, 150) then
                                if not GMR.IsPlayerPosition(2348.7400, -5694.9400, 384.0880, 3) then 
                                    GMR.MeshTo(2348.7400, -5694.9400, 384.0880)
                                else
                                    local npc = GMR.GetObjectWithInfo({ id = 29488, rawType = 5 })
                                    if npc then
                                        GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
                                    end
                                end
                            end
                        end
                    else 
                        self:SetScript("OnUpdate", nil); GMR.Frames.Rand014 = nil; GMR.Print("Frame deleted")
                    end
                end)
            end
            ]],
            'GMR.SkipTurnIn(false)',
            'GMR.CreateTableEntry("Unstuck")',
            'GMR.DefineUnstuck(2405.8530, -5733.6720, 153.9352, 20)',
            'GMR.DefineUnstuck(2391.0720, -5736.5030, 153.9491)',
            'GMR.DefineUnstuck(2375.1900, -5726.9030, 153.9218)',
            'GMR.DefineUnstuck(2365.3680, -5720.7980, 153.9219)',
            'GMR.DefineUnstuck(2353.7130, -5729.0000, 153.9221)',
            'GMR.DefineUnstuck(2343.6460, -5738.7290, 153.9221)',
            'GMR.DefineUnstuck(2332.3580, -5731.5900, 153.9221)',
            'GMR.DefineUnstuck(2323.8580, -5730.5120, 153.9206)',
            'GMR.DefineUnstuck(2317.6360, -5735.7280, 156.6861)',
            'GMR.DefineUnstuck(2313.2770, -5739.6750, 159.8685)',
            'GMR.DefineUnstuck(2310.9300, -5741.7180, 161.1480)',
            'GMR.DefineProfileCenter(2460.5000, -5593.4700, 367.3944, 60)',
            'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
            'GMR.DefineQuestEnemyId(99999)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 An End to All Things (MANUAL COMPLETION REQUIRED!) |r",
    QuestID = 12779,
    QuestType = "Custom",
    PickUp = { x = 2310.2668457031, y = -5742.2124023438, z = 161.00743103027, id = 29110 },
    TurnIn = { x = 2310.2668457031, y = -5742.2124023438, z = 161.00743103027, id = 29110 },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12778,
    Lua = [[
		if GMR.Questing.IsObjectiveCompleted(12779, 1) and GMR.Questing.IsObjectiveCompleted(12779, 2) then
			if GMR.IsInVehicle() then
				GMR.RunMacroText("/leavevehicle")
			end
		elseif not GMR.Questing.IsObjectiveCompleted(12779, 1) then
			GMR.SetQuestingState(nil);
			local singleTarget = GetSpellInfo(53114)
			local eatShit = GetSpellInfo(53110)
			local enemy = GMR.GetObjectWithInfo({ id = { 29102, 29103 }, rawType = 5, isAlive = true })
            local itemName = GetItemInfo(39700)
            if not GMR.IsInVehicle() then
                if GMR.IsMoving() then 
                    GMR.StopMoving()
                else
                    if GetItemCooldown(39700) == 0 then
                        GMR.Use(39700)
                    end
                end
            elseif GMR.IsInVehicle() then
                if enemy then
                    GMR.Cast(singleTarget)
					local x, y, z = GMR.ObjectPosition(enemy)
					if not GMR.IsPlayerPosition(x, y, z, 6) then
                        if GMR.GetVehicleSpeed() == 0 then
                            GMR.MoveTo(x, y, z)
                        else 
                            GMR.Questing.InteractWith(enemy, nil, nil, nil, nil, 5)
                        end
                    else
                        if UnitPower("vehicle" , mana) <= 41000 then
                            if GMR.GetSpellCooldown(eatShit) == 0 then
                                GMR.Cast(eatShit)
                                GMR.RunMacroText("/click OverrideActionBarButton3")
                                GMR.RunMacroText("/click VehicleMenuBarActionButton3")
                            end
                        elseif GMR.GetSpellCooldown(singleTarget) == 0 then
                            GMR.Cast(singleTarget)
                            GMR.RunMacroText("/click VehicleMenuBarActionButton1")
                            local x1, y1, z1 = GMR.ObjectPosition(enemy)
                            GMR.ClickPosition(x1, y1, z1)
                        end
                    end
                else 
                    GMR.SetQuestingState("Idle")
                end
			end
		end
    ]],
    Profile = {
        'GMR.SetChecked("ClickToMove", true)',
		'GMR.SetChecked("Mount", false)',
		'GMR.SetChecked("FlyingMount", false)',
        'GMR.DenySpeedUp()',
		'GMR.DefineProfileCenter(2183.3230, -5728.1900, 105.1636, 120)',
		'GMR.DefineProfileCenter(2103.8150, -5727.9240, 113.7049, 120)',
		'GMR.DefineProfileCenter(1981.0120, -5792.0210, 100.8947, 120)',
		'GMR.DefineProfileCenter(2011.4950, -5827.6440, 112.2205, 120)',
		'GMR.DefineProfileCenter(2059.2750, -5903.2580, 105.6034, 120)',
		'GMR.DefineProfileCenter(1852.2810, -6002.4290, 151.2272, 120)',
		'GMR.DefineProfileCenter(1853.4450, -5957.8860, 151.2272, 120)',
		'GMR.DefineProfileCenter(1819.7230, -5898.7660, 149.6703, 120)',
		'GMR.DefineProfileCenter(1813.9300, -5917.2920, 114.6218, 120)',
		'GMR.DefineProfileCenter(1965.5460, -5800.4600, 100.9255, 120)',
		'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
		'GMR.DefineQuestEnemyId(29102)',
		'GMR.DefineQuestEnemyId(29103)',
		'GMR.DefineQuestEnemyId(29104)'
    }
  })

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 The Lich King's Command -- END OF PROFILE! More quests to be added soon |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 12800, x = 2310.2700, y = -5742.2100, z = 161.1478, id = 29110 },
    },
    Faction = { "Alliance", "Horde" },
    Class = "DEATHKNIGHT",
    PreQuest = 12723,
    Profile = {
        [[
        if not GMR.Frames.Rand015 then
            GMR.Print("Frame created")
            GMR.Frames.Rand015 = CreateFrame("frame")
            GMR.Frames.Rand015:SetScript("OnUpdate", function(self)
                if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 12800 then
                    if GMR.IsExecuting() then
                        if not timer then
                            timer = GetTime()+40
                            local playerClass, englishClass = UnitClass("player")
                            GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFF00FF00END OF QUEST PROFILE. DK Start Zone is now done. |r")
                            GMR.Print("|cFFFF2CE6 CryptoQuester:|r  |cFF00FF00Thanks for using my profile! More quests will come in time when things slow down with WoTLK. Crypto#6212 |r")
                        elseif timer < GetTime() then 
                            timer = nil;
                        end
                    end
                else 
                    self:SetScript("OnUpdate", nil); GMR.Frames.Rand015 = nil; GMR.Print("Frame deleted")
                end
            end)
        end
        ]],
		'GMR.SkipTurnIn(true)',
		-- Deaths Reach
		'GMR.DefineSellVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineRepairVendor(2345.149902, -5720.770020, 153.921997, 28760)',
		'GMR.DefineGoodsVendor(2345.149902, -5720.770020, 153.921997, 28760)'
    }
  })



end)

GMR.LoadQuester("[WOTLK] DK Starting Area - Questing")
