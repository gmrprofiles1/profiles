GMR.DefineProfileName("REPUTATION - Netherstorm - Fel Armaments + Mark of Sargeras [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Outland")
GMR.DefineProfileCenter(2799.206, 3382.134, 142.8158, 80)
GMR.DefineProfileCenter(2750.305, 3287.26, 134.7795, 80)
GMR.DefineProfileCenter(2689.121, 3247.809, 132.596, 80)
GMR.DefineProfileCenter(2720.358, 3197.765, 147.5962, 80)
GMR.DefineProfileCenter(2753.316, 3228.654, 147.6003, 80)
GMR.DefineProfileCenter(2766.637, 3141.164, 150.7482, 80)
GMR.DefineProfileCenter(2690.422, 3133.257, 136.1719, 80)
GMR.DefineProfileCenter(2755.394, 3181.182, 148.9333, 80)
GMR.DefineQuestEnemyId(19852)
GMR.DefineQuestEnemyId(19853)
GMR.BlacklistId(21124)
GMR.BlacklistId(20998)
GMR.BlacklistId(21123)
GMR.BlacklistId(21300)
GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000,20194)
GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)
GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000,20194)
GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996,19572)
GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)
GMR.DefineBlacklistItem("Mark of Sargeras")
GMR.DefineBlacklistItem("Fel Armament")
GMR.DefineBlacklistItem("Mote of Shadow")
