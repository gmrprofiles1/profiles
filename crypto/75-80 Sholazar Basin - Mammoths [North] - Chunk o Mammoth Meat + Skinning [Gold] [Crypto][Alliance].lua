GMR.DefineProfileName("75-80 Sholazar Basin - Mammoths (North) - Chunk o Mammoth Meat + Skinning (Gold) [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(6395.8100, 5348.7100, -44.2662, 80)
GMR.DefineProfileCenter(6449.7830, 5289.6620, -48.0832, 80)
GMR.DefineProfileCenter(6461.4700, 5219.8290, -59.0336, 80)
GMR.DefineProfileCenter(6377.6450, 5111.5390, -73.6279, 80)
GMR.DefineProfileCenter(6375.3140, 4993.8900, -76.0434, 80)
GMR.DefineProfileCenter(6392.2360, 4788.9560, -81.3706, 80)
GMR.DefineProfileCenter(6410.1600, 4671.6270, -73.0203, 80)
GMR.DefineProfileCenter(6610.6460, 4683.9530, -20.8050, 80)
GMR.DefineProfileCenter(6358.0630, 4595.5130, -69.9531, 80)
GMR.DefineProfileCenter(6273.5830, 4515.8890, -79.1203, 80)
GMR.DefineProfileCenter(6038.2020, 4704.1100, -94.5167, 80)
GMR.DefineProfileCenter(6254.7410, 4543.7300, -81.4584, 80)
GMR.DefineProfileCenter(6302.2380, 4283.7740, -45.2391, 80)
GMR.DefineProfileCenter(6368.9010, 4640.4480, -74.6272, 80)
GMR.DefineProfileCenter(6369.0270, 5039.1920, -78.2225, 80)
GMR.DefineAreaBlacklist(6089.6320, 4516.6250, -81.3400, 30)
GMR.DefineAreaBlacklist(6100.7840, 4442.9050, -85.0411, 30)
GMR.DefineAreaBlacklist(6025.2170, 4474.7950, -87.6906, 30)
GMR.DefineAreaBlacklist(6039.8960, 4518.0790, -87.6536, 30)
GMR.DefineAreaBlacklist(6529.7920, 4432.7840, -47.6047, 30)
GMR.DefineAreaBlacklist(6495.7030, 4355.9090, -48.4912, 30)
GMR.DefineAreaBlacklist(6593.1340, 4524.4610, -44.7954, 20)
GMR.DefineAreaBlacklist(6267.9300, 4988.3420, -94.6071, 30)
GMR.DefineAreaBlacklist(6610.1820, 5072.4100, -41.4679, 30)
GMR.DefineAreaBlacklist(6569.6220, 5120.9780, -43.4805, 30)
GMR.DefineAreaBlacklist(6559.7790, 5190.0240, -51.2771, 30)
GMR.DefineAreaBlacklist(6653.3180, 5130.6510, -37.2988, 50)
GMR.DefineMeshAreaBlacklist(6089.6320, 4516.6250, -81.3400, 32)
GMR.DefineMeshAreaBlacklist(6100.7840, 4442.9050, -85.0411, 32)
GMR.DefineMeshAreaBlacklist(6025.2170, 4474.7950, -87.6906, 32)
GMR.DefineMeshAreaBlacklist(6039.8960, 4518.0790, -87.6536, 32)
GMR.DefineMeshAreaBlacklist(6529.7920, 4432.7840, -47.6047, 32)
GMR.DefineMeshAreaBlacklist(6495.7030, 4355.9090, -48.4912, 32)
GMR.DefineMeshAreaBlacklist(6593.1340, 4524.4610, -44.7954, 22)
GMR.DefineMeshAreaBlacklist(6267.9300, 4988.3420, -94.6071, 32)
GMR.DefineMeshAreaBlacklist(6610.1820, 5072.4100, -41.4679, 32)
GMR.DefineMeshAreaBlacklist(6569.6220, 5120.9780, -43.4805, 32)
GMR.DefineMeshAreaBlacklist(6559.7790, 5190.0240, -51.2771, 32)
GMR.DefineMeshAreaBlacklist(6653.3180, 5130.6510, -37.2988, 50)
GMR.DefineQuestEnemyId(28380)
GMR.DefineQuestEnemyId(28379)
GMR.DefineQuestEnemyId(28381)
GMR.BlacklistId(28101)
GMR.BlacklistId(28373)
GMR.BlacklistId(28109)
GMR.BlacklistId(28110)
GMR.DefineSellVendor(5552.2600, 5738.8800, -76.2168, 28040)
GMR.DefineRepairVendor(5552.2600, 5738.8800, -76.2168, 28040)
GMR.DefineAmmoVendor(5552.2600, 5738.8800, -76.2168, 28040)
GMR.DefineGoodsVendor(5566.2500, 5763.7100, -75.2256, 28038)
GMR.DefineProfileMailbox(5563.2230, 5758.7600, -75.2246, 192952)
GMR.DefineVendorPath("Sell", 5764.7340, 5732.9110, -73.8948)
GMR.DefineVendorPath("Sell", 5739.5420, 5732.2840, -76.7182)
GMR.DefineVendorPath("Sell", 5707.8050, 5733.9940, -73.4065)
GMR.DefineVendorPath("Sell", 5686.6900, 5736.3100, -73.1946)
GMR.DefineVendorPath("Sell", 5666.0580, 5737.4290, -78.2361)
GMR.DefineVendorPath("Sell", 5640.4440, 5743.7550, -77.1276)
GMR.DefineVendorPath("Sell", 5621.4840, 5751.4250, -72.0742)
GMR.DefineVendorPath("Sell", 5599.7600, 5758.9910, -70.7285)
GMR.DefineVendorPath("Sell", 5585.6780, 5762.5120, -73.7420)
GMR.DefineVendorPath("Sell", 5575.8260, 5749.9750, -74.4472)
GMR.DefineVendorPath("Sell", 5562.3150, 5747.5540, -76.1395)
GMR.DefineVendorPath("Sell", 5552.2600, 5738.8800, -76.2168)
GMR.DefineVendorPath("Repair", 5772.3760, 5729.7530, -73.9964)
GMR.DefineVendorPath("Repair", 5751.7930, 5730.6890, -75.6165)
GMR.DefineVendorPath("Repair", 5731.5780, 5732.3610, -77.2305)
GMR.DefineVendorPath("Repair", 5709.7480, 5731.7350, -73.9982)
GMR.DefineVendorPath("Repair", 5684.7010, 5733.9760, -74.1573)
GMR.DefineVendorPath("Repair", 5661.9020, 5736.4440, -79.8900)
GMR.DefineVendorPath("Repair", 5638.1050, 5742.2340, -76.6222)
GMR.DefineVendorPath("Repair", 5614.6180, 5756.6370, -70.7531)
GMR.DefineVendorPath("Repair", 5599.6950, 5765.5260, -70.6530)
GMR.DefineVendorPath("Repair", 5587.7050, 5764.1360, -73.2244)
GMR.DefineVendorPath("Repair", 5579.0620, 5754.4020, -74.6074)
GMR.DefineVendorPath("Repair", 5568.1120, 5750.8780, -75.4142)
GMR.DefineVendorPath("Repair", 5556.8630, 5746.5690, -76.7714)
GMR.DefineVendorPath("Repair", 5552.2600, 5738.8800, -76.2167)
GMR.DefineVendorPath("Goods", 5761.6050, 5733.3130, -74.0509)
GMR.DefineVendorPath("Goods", 5740.5670, 5733.2900, -76.4054)
GMR.DefineVendorPath("Goods", 5719.4930, 5732.2580, -75.5851)
GMR.DefineVendorPath("Goods", 5696.4460, 5733.0590, -73.0466)
GMR.DefineVendorPath("Goods", 5676.8570, 5735.6730, -74.4405)
GMR.DefineVendorPath("Goods", 5653.9710, 5739.8710, -80.6919)
GMR.DefineVendorPath("Goods", 5635.4800, 5744.8330, -75.6734)
GMR.DefineVendorPath("Goods", 5617.4020, 5754.0460, -70.6337)
GMR.DefineVendorPath("Goods", 5599.2320, 5763.1250, -70.5692)
GMR.DefineVendorPath("Goods", 5584.9980, 5758.0820, -73.1284)
GMR.DefineVendorPath("Goods", 5572.0350, 5750.8020, -75.0492)
GMR.DefineVendorPath("Goods", 5570.0360, 5764.7860, -75.2255)
GMR.DefineVendorPath("Goods", 5566.2500, 5763.7100, -75.2241)
GMR.DefineVendorPath("Ammo", 5780.3370, 5733.6070, -72.8830)
GMR.DefineVendorPath("Ammo", 5760.8080, 5733.5780, -74.0437)
GMR.DefineVendorPath("Ammo", 5741.0260, 5733.5480, -76.2689)
GMR.DefineVendorPath("Ammo", 5720.9230, 5733.5180, -75.9009)
GMR.DefineVendorPath("Ammo", 5696.2070, 5735.3810, -72.7993)
GMR.DefineVendorPath("Ammo", 5676.8880, 5737.1570, -73.9497)
GMR.DefineVendorPath("Ammo", 5653.4010, 5740.8940, -80.5001)
GMR.DefineVendorPath("Ammo", 5635.7160, 5748.5160, -75.5427)
GMR.DefineVendorPath("Ammo", 5617.1270, 5757.7380, -70.5873)
GMR.DefineVendorPath("Ammo", 5597.7740, 5765.9780, -70.9113)
GMR.DefineVendorPath("Ammo", 5586.1270, 5760.3310, -73.1664)
GMR.DefineVendorPath("Ammo", 5577.1470, 5752.7710, -74.5826)
GMR.DefineVendorPath("Ammo", 5561.6870, 5747.9220, -76.2628)
GMR.DefineVendorPath("Ammo", 5552.2600, 5738.8800, -76.2167)
GMR.DefineMailboxPath(5781.7490, 5739.1030, -70.9794)
GMR.DefineMailboxPath(5764.7870, 5738.6410, -71.9165)
GMR.DefineMailboxPath(5747.6810, 5738.0910, -73.9312)
GMR.DefineMailboxPath(5728.3310, 5735.3630, -77.3052)
GMR.DefineMailboxPath(5711.0940, 5734.6740, -73.9030)
GMR.DefineMailboxPath(5690.3010, 5733.8420, -73.7204)
GMR.DefineMailboxPath(5665.2990, 5736.2290, -78.8767)
GMR.DefineMailboxPath(5649.3690, 5743.4780, -79.5962)
GMR.DefineMailboxPath(5627.6530, 5753.9180, -73.7506)
GMR.DefineMailboxPath(5610.1450, 5761.9870, -71.0874)
GMR.DefineMailboxPath(5593.7450, 5766.3800, -71.5141)
GMR.DefineMailboxPath(5584.8110, 5760.2620, -73.6766)
GMR.DefineMailboxPath(5574.1340, 5745.5550, -74.6733)
GMR.DefineMailboxPath(5562.9610, 5752.7580, -75.9423)
GMR.DefineMailboxPath(5563.2230, 5758.7600, -75.2246)