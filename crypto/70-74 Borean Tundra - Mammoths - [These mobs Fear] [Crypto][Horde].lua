GMR.DefineProfileName("70-74 Borean Tundra - Mammoths - (These mobs Fear) [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(3299.5860, 5399.5730, 47.7618, 80)
GMR.DefineProfileCenter(3374.3800, 5455.5140, 60.7151, 80)
GMR.DefineProfileCenter(3331.6220, 5271.6280, 36.0379, 80)
GMR.DefineProfileCenter(3544.5640, 5284.6710, 30.4330, 80)
GMR.DefineProfileCenter(3604.3860, 5208.7160, 25.8880, 80)
GMR.DefineProfileCenter(3715.3330, 5226.2610, 31.0422, 80)
GMR.DefineProfileCenter(3809.0060, 5393.9810, 32.6430, 80)
GMR.DefineProfileCenter(3691.7170, 5374.0070, 35.7970, 80)
GMR.DefineProfileCenter(3590.4530, 5202.7600, 27.4783, 80)
GMR.DefineProfileCenter(3396.9590, 5431.3190, 56.6230, 80)
GMR.DefineProfileCenter(3087.2510, 5448.8980, 58.4003, 80)
GMR.DefineAreaBlacklist(3557.1340, 5431.7950, 38.7902, 40)
GMR.DefineAreaBlacklist(3646.2260, 5603.4910, 33.1862, 60)
GMR.DefineAreaBlacklist(3710.7320, 5627.2170, 33.4510, 25)
GMR.DefineAreaBlacklist(3722.5570, 5579.9500, 36.5520, 25)
GMR.DefineAreaBlacklist(3640.4410, 5540.0000, 32.4227, 25)
GMR.DefineAreaBlacklist(3587.6090, 5506.9790, 27.9453, 35)
GMR.DefineAreaBlacklist(3519.1540, 5561.8950, 64.3772, 33)
GMR.DefineAreaBlacklist(3526.5950, 5668.3050, 61.5449, 25)
GMR.DefineAreaBlacklist(3606.7530, 5692.2200, 67.4599, 25)
GMR.DefineAreaBlacklist(3690.4320, 5710.4930, 64.2417, 25)
GMR.DefineAreaBlacklist(3975.6580, 5469.3280, 27.9809, 40)
GMR.DefineMeshAreaBlacklist(3557.1340, 5431.7950, 38.7902, 40)
GMR.DefineMeshAreaBlacklist(3646.2260, 5603.4910, 33.1862, 60)
GMR.DefineMeshAreaBlacklist(3710.7320, 5627.2170, 33.4510, 25)
GMR.DefineMeshAreaBlacklist(3722.5570, 5579.9500, 36.5520, 25)
GMR.DefineMeshAreaBlacklist(3640.4410, 5540.0000, 32.4227, 25)
GMR.DefineMeshAreaBlacklist(3587.6090, 5506.9790, 27.9453, 35)
GMR.DefineMeshAreaBlacklist(3519.1540, 5561.8950, 64.3772, 25)
GMR.DefineMeshAreaBlacklist(3526.5950, 5668.3050, 61.5449, 25)
GMR.DefineMeshAreaBlacklist(3606.7530, 5692.2200, 67.4599, 25)
GMR.DefineMeshAreaBlacklist(3690.4320, 5710.4930, 64.2417, 25)
GMR.DefineMeshAreaBlacklist(3975.6580, 5469.3280, 27.9809, 40)
GMR.DefineQuestEnemyId(24614)
GMR.DefineQuestEnemyId(24613)
GMR.DefineQuestEnemyId(25743)
GMR.DefineQuestEnemyId(25680)
GMR.BlacklistId(25850)
GMR.BlacklistId(25979)
GMR.BlacklistId(25353)
GMR.BlacklistId(25355)
GMR.BlacklistId(25791)
GMR.DefineSellVendor(4510.5200, 5703.3500, 81.5405, 27067)
GMR.DefineRepairVendor(4510.5200, 5703.3500, 81.5405, 27067)
GMR.DefineAmmoVendor(4502.2400, 5757.5600, 81.5369, 27058)
GMR.DefineGoodsVendor(4506.3600, 5707.3200, 81.5201, 27069)
GMR.DefineProfileMailbox(4502.1790, 5717.2480, 81.4825, 189329)
GMR.DefineVendorPath("Sell", 3168.4160, 5380.1230, 56.6358)
GMR.DefineVendorPath("Sell", 3251.1410, 5319.5760, 42.4219)
GMR.DefineVendorPath("Sell", 3360.3930, 5314.3170, 37.8737)
GMR.DefineVendorPath("Sell", 3444.2780, 5355.6100, 44.5309)
GMR.DefineVendorPath("Sell", 3526.1010, 5355.4580, 39.7701)
GMR.DefineVendorPath("Sell", 3597.2100, 5388.8050, 39.4079)
GMR.DefineVendorPath("Sell", 3672.0460, 5413.0500, 40.6367)
GMR.DefineVendorPath("Sell", 3821.7530, 5416.7030, 33.6697)
GMR.DefineVendorPath("Sell", 3853.8990, 5463.8050, 35.7251)
GMR.DefineVendorPath("Sell", 3913.9830, 5545.0100, 36.9414)
GMR.DefineVendorPath("Sell", 3991.8540, 5558.8930, 37.4764)
GMR.DefineVendorPath("Sell", 4042.0130, 5605.6330, 41.0241)
GMR.DefineVendorPath("Sell", 4058.9410, 5610.7420, 41.0758)
GMR.DefineVendorPath("Sell", 4145.3930, 5611.3860, 41.3177)
GMR.DefineVendorPath("Sell", 4195.5990, 5600.3390, 41.7802)
GMR.DefineVendorPath("Sell", 4279.6170, 5579.1490, 47.0520)
GMR.DefineVendorPath("Sell", 4355.3320, 5585.0800, 44.1405)
GMR.DefineVendorPath("Sell", 4396.8720, 5622.0290, 53.3256)
GMR.DefineVendorPath("Sell", 4430.1470, 5651.9420, 59.7012)
GMR.DefineVendorPath("Sell", 4450.7440, 5678.0420, 69.7699)
GMR.DefineVendorPath("Sell", 4474.2000, 5690.2440, 80.1667)
GMR.DefineVendorPath("Sell", 4481.0780, 5709.6500, 81.3205)
GMR.DefineVendorPath("Sell", 4490.2090, 5734.8660, 80.1843)
GMR.DefineVendorPath("Sell", 4503.3010, 5721.2060, 81.4647)
GMR.DefineVendorPath("Sell", 4508.2120, 5714.4490, 81.5300)
GMR.DefineVendorPath("Sell", 4508.2400, 5707.9630, 81.5289)
GMR.DefineVendorPath("Sell", 4510.5200, 5703.3500, 81.5405)
GMR.DefineVendorPath("Repair", 3168.4160, 5380.1230, 56.6358)
GMR.DefineVendorPath("Repair", 3251.1410, 5319.5760, 42.4219)
GMR.DefineVendorPath("Repair", 3360.3930, 5314.3170, 37.8737)
GMR.DefineVendorPath("Repair", 3444.2780, 5355.6100, 44.5309)
GMR.DefineVendorPath("Repair", 3526.1010, 5355.4580, 39.7701)
GMR.DefineVendorPath("Repair", 3597.2100, 5388.8050, 39.4079)
GMR.DefineVendorPath("Repair", 3672.0460, 5413.0500, 40.6367)
GMR.DefineVendorPath("Repair", 3821.7530, 5416.7030, 33.6697)
GMR.DefineVendorPath("Repair", 3853.8990, 5463.8050, 35.7251)
GMR.DefineVendorPath("Repair", 3913.9830, 5545.0100, 36.9414)
GMR.DefineVendorPath("Repair", 3991.8540, 5558.8930, 37.4764)
GMR.DefineVendorPath("Repair", 4042.0130, 5605.6330, 41.0241)
GMR.DefineVendorPath("Repair", 4058.9410, 5610.7420, 41.0758)
GMR.DefineVendorPath("Repair", 4145.3930, 5611.3860, 41.3177)
GMR.DefineVendorPath("Repair", 4195.5990, 5600.3390, 41.7802)
GMR.DefineVendorPath("Repair", 4279.6170, 5579.1490, 47.0520)
GMR.DefineVendorPath("Repair", 4355.3320, 5585.0800, 44.1405)
GMR.DefineVendorPath("Repair", 4396.8720, 5622.0290, 53.3256)
GMR.DefineVendorPath("Repair", 4430.1470, 5651.9420, 59.7012)
GMR.DefineVendorPath("Repair", 4450.7440, 5678.0420, 69.7699)
GMR.DefineVendorPath("Repair", 4474.2000, 5690.2440, 80.1667)
GMR.DefineVendorPath("Repair", 4481.0780, 5709.6500, 81.3205)
GMR.DefineVendorPath("Repair", 4490.2090, 5734.8660, 80.1843)
GMR.DefineVendorPath("Repair", 4503.3010, 5721.2060, 81.4647)
GMR.DefineVendorPath("Repair", 4508.2120, 5714.4490, 81.5300)
GMR.DefineVendorPath("Repair", 4508.2400, 5707.9630, 81.5289)
GMR.DefineVendorPath("Repair", 4510.5200, 5703.3500, 81.5405)
GMR.DefineVendorPath("Goods", 3168.4160, 5380.1230, 56.6358)
GMR.DefineVendorPath("Goods", 3251.1410, 5319.5760, 42.4219)
GMR.DefineVendorPath("Goods", 3360.3930, 5314.3170, 37.8737)
GMR.DefineVendorPath("Goods", 3444.2780, 5355.6100, 44.5309)
GMR.DefineVendorPath("Goods", 3526.1010, 5355.4580, 39.7701)
GMR.DefineVendorPath("Goods", 3597.2100, 5388.8050, 39.4079)
GMR.DefineVendorPath("Goods", 3672.0460, 5413.0500, 40.6367)
GMR.DefineVendorPath("Goods", 3821.7530, 5416.7030, 33.6697)
GMR.DefineVendorPath("Goods", 3853.8990, 5463.8050, 35.7251)
GMR.DefineVendorPath("Goods", 3913.9830, 5545.0100, 36.9414)
GMR.DefineVendorPath("Goods", 3991.8540, 5558.8930, 37.4764)
GMR.DefineVendorPath("Goods", 4042.0130, 5605.6330, 41.0241)
GMR.DefineVendorPath("Goods", 4058.9410, 5610.7420, 41.0758)
GMR.DefineVendorPath("Goods", 4145.3930, 5611.3860, 41.3177)
GMR.DefineVendorPath("Goods", 4195.5990, 5600.3390, 41.7802)
GMR.DefineVendorPath("Goods", 4279.6170, 5579.1490, 47.0520)
GMR.DefineVendorPath("Goods", 4355.3320, 5585.0800, 44.1405)
GMR.DefineVendorPath("Goods", 4396.8720, 5622.0290, 53.3256)
GMR.DefineVendorPath("Goods", 4430.1470, 5651.9420, 59.7012)
GMR.DefineVendorPath("Goods", 4450.7440, 5678.0420, 69.7699)
GMR.DefineVendorPath("Goods", 4474.2000, 5690.2440, 80.1667)
GMR.DefineVendorPath("Goods", 4481.0780, 5709.6500, 81.3205)
GMR.DefineVendorPath("Goods", 4490.2090, 5734.8660, 80.1843)
GMR.DefineVendorPath("Goods", 4499.0260, 5725.3250, 81.0917)
GMR.DefineVendorPath("Goods", 4508.2560, 5714.7000, 81.5299)
GMR.DefineVendorPath("Goods", 4508.7430, 5709.4720, 81.5305)
GMR.DefineVendorPath("Goods", 4506.3600, 5707.3200, 81.5201)
GMR.DefineVendorPath("Ammo", 3168.4160, 5380.1230, 56.6358)
GMR.DefineVendorPath("Ammo", 3251.1410, 5319.5760, 42.4219)
GMR.DefineVendorPath("Ammo", 3360.3930, 5314.3170, 37.8737)
GMR.DefineVendorPath("Ammo", 3444.2780, 5355.6100, 44.5309)
GMR.DefineVendorPath("Ammo", 3526.1010, 5355.4580, 39.7701)
GMR.DefineVendorPath("Ammo", 3597.2100, 5388.8050, 39.4079)
GMR.DefineVendorPath("Ammo", 3672.0460, 5413.0500, 40.6367)
GMR.DefineVendorPath("Ammo", 3821.7530, 5416.7030, 33.6697)
GMR.DefineVendorPath("Ammo", 3853.8990, 5463.8050, 35.7251)
GMR.DefineVendorPath("Ammo", 3913.9830, 5545.0100, 36.9414)
GMR.DefineVendorPath("Ammo", 3991.8540, 5558.8930, 37.4764)
GMR.DefineVendorPath("Ammo", 4042.0130, 5605.6330, 41.0241)
GMR.DefineVendorPath("Ammo", 4058.9410, 5610.7420, 41.0758)
GMR.DefineVendorPath("Ammo", 4145.3930, 5611.3860, 41.3177)
GMR.DefineVendorPath("Ammo", 4195.5990, 5600.3390, 41.7802)
GMR.DefineVendorPath("Ammo", 4279.6170, 5579.1490, 47.0520)
GMR.DefineVendorPath("Ammo", 4355.3320, 5585.0800, 44.1405)
GMR.DefineVendorPath("Ammo", 4396.8720, 5622.0290, 53.3256)
GMR.DefineVendorPath("Ammo", 4430.1470, 5651.9420, 59.7012)
GMR.DefineVendorPath("Ammo", 4450.7440, 5678.0420, 69.7699)
GMR.DefineVendorPath("Ammo", 4474.2000, 5690.2440, 80.1667)
GMR.DefineVendorPath("Ammo", 4481.0780, 5709.6500, 81.3205)
GMR.DefineVendorPath("Ammo", 4490.2090, 5734.8660, 80.1843)
GMR.DefineVendorPath("Ammo", 4493.1360, 5753.8910, 81.3466)
GMR.DefineVendorPath("Ammo", 4502.2400, 5757.5600, 81.5369)
GMR.DefineMailboxPath(3168.4160, 5380.1230, 56.6358)
GMR.DefineMailboxPath(3251.1410, 5319.5760, 42.4219)
GMR.DefineMailboxPath(3360.3930, 5314.3170, 37.8737)
GMR.DefineMailboxPath(3444.2780, 5355.6100, 44.5309)
GMR.DefineMailboxPath(3526.1010, 5355.4580, 39.7701)
GMR.DefineMailboxPath(3597.2100, 5388.8050, 39.4079)
GMR.DefineMailboxPath(3672.0460, 5413.0500, 40.6367)
GMR.DefineMailboxPath(3821.7530, 5416.7030, 33.6697)
GMR.DefineMailboxPath(3853.8990, 5463.8050, 35.7251)
GMR.DefineMailboxPath(3913.9830, 5545.0100, 36.9414)
GMR.DefineMailboxPath(3991.8540, 5558.8930, 37.4764)
GMR.DefineMailboxPath(4042.0130, 5605.6330, 41.0241)
GMR.DefineMailboxPath(4058.9410, 5610.7420, 41.0758)
GMR.DefineMailboxPath(4145.3930, 5611.3860, 41.3177)
GMR.DefineMailboxPath(4195.5990, 5600.3390, 41.7802)
GMR.DefineMailboxPath(4279.6170, 5579.1490, 47.0520)
GMR.DefineMailboxPath(4355.3320, 5585.0800, 44.1405)
GMR.DefineMailboxPath(4396.8720, 5622.0290, 53.3256)
GMR.DefineMailboxPath(4430.1470, 5651.9420, 59.7012)
GMR.DefineMailboxPath(4450.7440, 5678.0420, 69.7699)
GMR.DefineMailboxPath(4474.2000, 5690.2440, 80.1667)
GMR.DefineMailboxPath(4481.0780, 5709.6500, 81.3205)
GMR.DefineMailboxPath(4490.2090, 5734.8660, 80.1843)
GMR.DefineMailboxPath(4499.9220, 5724.3220, 81.2080)
GMR.DefineMailboxPath(4502.1790, 5717.2480, 81.4825)