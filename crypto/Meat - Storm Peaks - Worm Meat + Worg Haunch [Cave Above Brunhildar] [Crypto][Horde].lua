GMR.DefineProfileName("Meat - Storm Peaks - Worm Meat + Worg Haunch (Cave Above Brunhildar) [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(7560.5730, -1469.5970, 972.7325, 80)
GMR.DefineProfileCenter(7618.7760, -1539.8720, 972.8475, 80)
GMR.DefineProfileCenter(7664.5830, -1608.1500, 964.6168, 80)
GMR.DefineProfileCenter(7739.9350, -1624.5730, 950.3891, 80)
GMR.DefineProfileCenter(7890.6260, -1528.9230, 921.1073, 80)
GMR.DefineProfileCenter(7917.4130, -1617.6420, 910.5573, 80)
GMR.DefineProfileCenter(7952.4510, -1606.2960, 908.4918, 80)
GMR.DefineProfileCenter(8021.0490, -1593.8510, 904.3027, 80)
GMR.DefineProfileCenter(7987.8860, -1554.7690, 905.2123, 80)
GMR.DefineProfileCenter(7859.7640, -1527.8890, 927.0709, 80)
GMR.DefineProfileCenter(7652.6020, -1588.4150, 967.9547, 80)
GMR.DefineQuestEnemyId(30148)
GMR.DefineQuestEnemyId(30164)
GMR.DefineSellVendor(6097.5600, -1071.2600, 404.4130, 29907)
GMR.DefineRepairVendor(6097.5600, -1071.2600, 404.4130, 29907)
GMR.DefineAmmoVendor(6149.1600, -1055.3000, 402.8568, 29908)
GMR.DefineGoodsVendor(6124.4200, -1080.9600, 402.5410, 29904)
GMR.DefineProfileMailbox(6126.4450, -1070.1070, 403.1782, 191521)
GMR.DefineVendorPath("Sell", 6122.5730, -1004.3740, 406.7550)
GMR.DefineVendorPath("Sell", 6120.2880, -1028.0740, 407.8150)
GMR.DefineVendorPath("Sell", 6121.0980, -1058.6970, 402.6789)
GMR.DefineVendorPath("Sell", 6108.9160, -1068.0960, 402.9505)
GMR.DefineVendorPath("Sell", 6097.5600, -1071.2600, 404.4130)
GMR.DefineVendorPath("Repair", 6124.0080, -996.8227, 405.1455)
GMR.DefineVendorPath("Repair", 6122.1690, -1020.9720, 409.0563)
GMR.DefineVendorPath("Repair", 6119.9540, -1047.0580, 403.5760)
GMR.DefineVendorPath("Repair", 6111.2030, -1063.6520, 402.7378)
GMR.DefineVendorPath("Repair", 6097.5600, -1071.2600, 404.4130)
GMR.DefineVendorPath("Goods", 6125.6780, -991.5635, 403.7322)
GMR.DefineVendorPath("Goods", 6121.3330, -1015.5930, 408.6011)
GMR.DefineVendorPath("Goods", 6121.2200, -1045.3700, 403.7219)
GMR.DefineVendorPath("Goods", 6122.2950, -1069.1590, 403.2240)
GMR.DefineVendorPath("Goods", 6122.5960, -1079.1970, 402.5612)
GMR.DefineVendorPath("Goods", 6118.5900, -1085.8550, 402.6865)
GMR.DefineVendorPath("Goods", 6115.9390, -1092.9630, 402.8130)
GMR.DefineVendorPath("Goods", 6121.1000, -1095.9200, 402.7538)
GMR.DefineVendorPath("Ammo", 6125.1860, -991.8169, 403.8012)
GMR.DefineVendorPath("Ammo", 6120.8670, -1014.1820, 408.4061)
GMR.DefineVendorPath("Ammo", 6120.8560, -1034.3000, 406.3181)
GMR.DefineVendorPath("Ammo", 6121.1980, -1055.6670, 402.8696)
GMR.DefineVendorPath("Ammo", 6133.0240, -1062.8900, 402.6282)
GMR.DefineVendorPath("Ammo", 6143.2340, -1059.4010, 403.3754)
GMR.DefineVendorPath("Ammo", 6149.1600, -1055.3000, 402.8568)
GMR.DefineMailboxPath(6128.4960, -986.1156, 402.7598)
GMR.DefineMailboxPath(6122.2490, -1008.8260, 407.5920)
GMR.DefineMailboxPath(6120.5720, -1028.9520, 407.6218)
GMR.DefineMailboxPath(6120.9960, -1052.7830, 403.0959)
GMR.DefineMailboxPath(6125.7820, -1064.4740, 402.6499)
GMR.DefineMailboxPath(6126.4380, -1070.1920, 403.1883)