GMR.DefineProfileName("Eternals - Grizzly Hills (Southeast) - Eternal Air [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(3393.7990, -4913.7200, 285.1108, 60)
GMR.DefineProfileCenter(3489.6980, -4939.8290, 227.7815, 60)
GMR.DefineProfileCenter(3606.8490, -4971.9180, 182.7741, 60)
GMR.DefineProfileCenter(3656.3240, -5072.5700, 168.5883, 60)
GMR.DefineProfileCenter(3677.6170, -5181.3970, 135.3009, 60)
GMR.DefineProfileCenter(3601.1870, -5198.6950, 160.6274, 60)
GMR.DefineProfileCenter(3544.6980, -5282.5350, 156.9486, 60)
GMR.DefineProfileCenter(3608.7280, -5199.4440, 158.2950, 60)
GMR.DefineProfileCenter(3623.7390, -4972.8810, 178.9321, 60)
GMR.DefineProfileCenter(3510.7770, -4959.3420, 218.4895, 60)
GMR.DefineProfileCenter(3392.3380, -4915.0570, 285.3434, 60)
GMR.DefineQuestEnemyId(26407)
GMR.DefineQuestEnemyId(26347)
GMR.DefineSellVendor(3836.0000, -4542.0800, 209.2770, 26707)
GMR.DefineRepairVendor(3836.0000, -4542.0800, 209.2770, 26707)
GMR.DefineAmmoVendor(3843.4200, -4550.3200, 209.7358, 26936)
GMR.DefineGoodsVendor(3875.4600, -4544.9900, 209.2655, 26680)
GMR.DefineProfileMailbox(3861.4390, -4549.1670, 209.6440, 188256)
GMR.DefineVendorPath("Sell", 3838.8060, -4493.0860, 204.2388)
GMR.DefineVendorPath("Sell", 3854.0040, -4506.3860, 208.8525)
GMR.DefineVendorPath("Sell", 3851.4270, -4521.0230, 209.4854)
GMR.DefineVendorPath("Sell", 3842.2160, -4533.6810, 209.7137)
GMR.DefineVendorPath("Sell", 3836.0000, -4542.0800, 209.2775)
GMR.DefineVendorPath("Repair", 3839.9930, -4494.7220, 204.5555)
GMR.DefineVendorPath("Repair", 3855.1700, -4510.6650, 209.2641)
GMR.DefineVendorPath("Repair", 3845.9540, -4525.7300, 209.7737)
GMR.DefineVendorPath("Repair", 3836.0000, -4542.0800, 209.2775)
GMR.DefineVendorPath("Goods", 3834.7720, -4490.4030, 203.4267)
GMR.DefineVendorPath("Goods", 3853.8730, -4506.9810, 208.8720)
GMR.DefineVendorPath("Goods", 3857.5190, -4530.4430, 209.2700)
GMR.DefineVendorPath("Goods", 3863.8300, -4543.2660, 209.2664)
GMR.DefineVendorPath("Goods", 3875.4600, -4544.9900, 209.2663)
GMR.DefineVendorPath("Ammo", 3834.0800, -4488.9390, 203.2303)
GMR.DefineVendorPath("Ammo", 3850.0710, -4503.9440, 207.7320)
GMR.DefineVendorPath("Ammo", 3853.3520, -4517.0210, 209.4176)
GMR.DefineVendorPath("Ammo", 3840.9940, -4534.4580, 209.7391)
GMR.DefineVendorPath("Ammo", 3847.9790, -4547.3130, 209.3930)
GMR.DefineVendorPath("Ammo", 3843.4200, -4550.3200, 209.7358)
GMR.DefineMailboxPath(3833.0350, -4489.9030, 203.1475)
GMR.DefineMailboxPath(3853.8690, -4506.4180, 208.8305)
GMR.DefineMailboxPath(3857.0100, -4527.5110, 209.3938)
GMR.DefineMailboxPath(3858.9310, -4542.7810, 209.3115)
GMR.DefineMailboxPath(3861.5320, -4548.0050, 209.5340)