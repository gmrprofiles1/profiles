GMR.DefineProfileName("Skinning - Borean Tundra - Caribou (Taunkale Village) [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(3657.3890, 4153.3770, 26.7907, 80)
GMR.DefineProfileCenter(3547.8330, 4092.3500, 22.4477, 80)
GMR.DefineProfileCenter(3482.4520, 4019.4090, 25.8089, 80)
GMR.DefineProfileCenter(3307.3450, 3929.3590, 26.2274, 80)
GMR.DefineProfileCenter(3355.1560, 4121.7130, 25.3859, 80)
GMR.DefineProfileCenter(3176.2980, 4359.9690, 28.1373, 80)
GMR.DefineProfileCenter(3307.5350, 3950.2750, 26.6294, 80)
GMR.DefineProfileCenter(3419.5730, 4017.2300, 25.0062, 80)
GMR.DefineProfileCenter(3560.6680, 4039.6430, 21.6877, 80)
GMR.DefineProfileCenter(3685.3190, 4168.3180, 25.2283, 80)
GMR.DefineAreaBlacklist(3768.0630, 4066.0940, 27.9868, 30)
GMR.DefineAreaBlacklist(3824.1180, 4028.4660, 25.1810, 30)
GMR.DefineAreaBlacklist(3750.5360, 4012.3210, 27.5582, 30)
GMR.DefineMeshAreaBlacklist(3768.0630, 4066.0940, 27.9868, 32)
GMR.DefineMeshAreaBlacklist(3824.1180, 4028.4660, 25.1810, 32)
GMR.DefineMeshAreaBlacklist(3750.5360, 4012.3210, 27.5582, 30)
GMR.DefineQuestEnemyId(25680)
GMR.BlacklistId(25514)
GMR.BlacklistId(26202)
GMR.BlacklistId(25699)
GMR.BlacklistId(25701)
GMR.DefineSellVendor(3468.8300, 4154.3000, 17.1931, 26697)
GMR.DefineRepairVendor(3468.8300, 4154.3000, 17.1931, 26697)
GMR.DefineAmmoVendor(3430.1500, 4137.8100, 16.8692, 26718)
GMR.DefineGoodsVendor(3468.6400, 4175.0400, 17.1942, 26709)
GMR.DefineProfileMailbox(3458.7600, 4131.6550, 15.2298, 187322)
GMR.DefineVendorPath("Sell", 3594.4940, 4018.1360, 22.2999)
GMR.DefineVendorPath("Sell", 3537.4940, 4036.2460, 22.0970)
GMR.DefineVendorPath("Sell", 3475.1880, 4064.5160, 18.7099)
GMR.DefineVendorPath("Sell", 3461.3140, 4106.7060, 16.0229)
GMR.DefineVendorPath("Sell", 3462.5150, 4140.0470, 15.9306)
GMR.DefineVendorPath("Sell", 3465.7050, 4147.8590, 17.1932)
GMR.DefineVendorPath("Sell", 3468.8300, 4154.3000, 17.1930)
GMR.DefineVendorPath("Repair", 3594.4940, 4018.1360, 22.2999)
GMR.DefineVendorPath("Repair", 3537.4940, 4036.2460, 22.0970)
GMR.DefineVendorPath("Repair", 3475.1880, 4064.5160, 18.7099)
GMR.DefineVendorPath("Repair", 3461.3140, 4106.7060, 16.0229)
GMR.DefineVendorPath("Repair", 3462.5150, 4140.0470, 15.9306)
GMR.DefineVendorPath("Repair", 3465.7050, 4147.8590, 17.1932)
GMR.DefineVendorPath("Repair", 3468.8300, 4154.3000, 17.1930)
GMR.DefineVendorPath("Goods", 3622.3130, 4013.9450, 25.5138)
GMR.DefineVendorPath("Goods", 3558.2210, 4029.0180, 22.0244)
GMR.DefineVendorPath("Goods", 3493.1180, 4052.7680, 19.2751)
GMR.DefineVendorPath("Goods", 3456.2740, 4093.3450, 16.5054)
GMR.DefineVendorPath("Goods", 3462.4280, 4120.8740, 16.0635)
GMR.DefineVendorPath("Goods", 3463.0890, 4144.5270, 17.1929)
GMR.DefineVendorPath("Goods", 3464.1470, 4166.0200, 17.1929)
GMR.DefineVendorPath("Goods", 3468.6400, 4175.0400, 17.1938)
GMR.DefineVendorPath("Ammo", 3495.4180, 4050.0330, 19.3753)
GMR.DefineVendorPath("Ammo", 3469.3140, 4078.2970, 17.7537)
GMR.DefineVendorPath("Ammo", 3438.4860, 4113.7490, 15.8971)
GMR.DefineVendorPath("Ammo", 3433.1000, 4132.7210, 15.8607)
GMR.DefineVendorPath("Ammo", 3430.1500, 4137.8100, 16.8695)