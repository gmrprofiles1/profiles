GMR.DefineProfileName("18-25 Draenei - Bloodmyst Isle - Small [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Outland")
GMR.DefineProfileCenter(-1621.105, -11102.59, 68.62511, 80)
GMR.DefineProfileCenter(-1612.933, -11006.31, 63.14269, 60)
GMR.DefineProfileCenter(-1542.991, -10964.68, 61.86495, 60)
GMR.DefineProfileCenter(-1517.456, -11100.54, 75.79701, 60)
GMR.DefineProfileCenter(-1576.586, -10964.35, 59.02934, 60)
GMR.DefineProfileCenter(-1682.894, -10851.02, 58.60995, 60)
GMR.DefineProfileCenter(-1729.32, -10910.44, 63.75991, 60)
GMR.DefineProfileCenter(-1805.198, -10890.81, 67.01472, 60)
GMR.DefineProfileCenter(-1832.317, -10984.49, 66.76692, 60)
GMR.DefineProfileCenter(-1940.349, -11006.26, 59.44644, 60)
GMR.DefineProfileCenter(-1976.216, -11106.21, 56.03507, 60)
GMR.DefineProfileCenter(-1942.791, -11171.72, 59.95145, 60)
GMR.DefineProfileCenter(-2028.159, -11166.96, 61.85518, 60)
GMR.DefineProfileCenter(-2154.046, -11136.9, 56.74143, 60)
GMR.DefineProfileCenter(-2270.264, -11046.38, 7.586513, 60)
GMR.DefineProfileCenter(-2154.135, -11135.53, 56.44253, 60)
GMR.DefineProfileCenter(-2028.859, -11166.73, 61.82865, 60)
GMR.DefineProfileCenter(-1977.107, -11077.2, 57.29395, 60)
GMR.DefineProfileCenter(-1883.73, -11173.21, 62.0778, 60)
GMR.DefineProfileCenter(-1779.469, -11117.1, 66.31611, 60)
GMR.DefineProfileCenter(-1611.667, -11119.37, 68.66452, 60)
GMR.DefineAreaBlacklist(-1901.357, -10952.1, 61.3618,25)
GMR.DefineAreaBlacklist(-2102.138, -10850.85, 70.31364,20)
GMR.DefineAreaBlacklist(-2099.731, -10772.96, 67.99112,30)
GMR.DefineAreaBlacklist(-2097.111, -10721.48, 67.94263,30)
GMR.DefineAreaBlacklist(-2068.204, -10812.39, 68.41188,20)
GMR.DefineAreaBlacklist(-2094.716, -10890.61, 71.48605,15)
GMR.DefineAreaBlacklist(-2049.369, -10862.3, 65.78916,15)
GMR.DefineAreaBlacklist(-1560.643, -10794.65, 51.98116,20)
GMR.DefineQuestEnemyId(17358)
GMR.DefineQuestEnemyId(17342)
GMR.DefineQuestEnemyId(17340)
GMR.DefineQuestEnemyId(17522)
GMR.DefineQuestEnemyId(17353)
GMR.DefineQuestEnemyId(17527)
GMR.DefineQuestEnemyId(17346)
GMR.DefineQuestEnemyId(17523)
GMR.BlacklistId(17660)
GMR.BlacklistId(17606)
GMR.BlacklistId(17664)
GMR.DefineSellVendor(-1990.959961, -11814.500000, 54.583698, 17667)
GMR.DefineRepairVendor(-1990.959961, -11814.500000, 54.583698, 17667)
GMR.DefineAmmoVendor(-2023.560059, -11878.000000, 45.474899, 18811)
GMR.DefineGoodsVendor(-2059.199951, -11897.000000, 45.741501, 17553)
GMR.DefineHearthstoneBindLocation(-2059.199951, -11897.000000, 45.741501, 17553)
