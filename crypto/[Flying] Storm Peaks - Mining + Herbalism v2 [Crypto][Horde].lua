GMR.DefineProfileName("[Flying] Storm Peaks - Mining + Herbalism v2 [Crypto][Horde]")
GMR.DefineProfileType("Gathering")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(6574.258, -1823.43, 988.079350, 300)
GMR.DefineProfileCenter(6550.257, -1713.16, 843.8047350, 300)
GMR.DefineProfileCenter(6513.335, -1594.88, 720.9691350, 300)
GMR.DefineProfileCenter(6503.519, -1436.342, 649.2556350, 300)
GMR.DefineProfileCenter(6502.169, -1417.491, 640.7621350, 300)
GMR.DefineProfileCenter(6500.821, -1229.438, 593.5769350, 300)
GMR.DefineProfileCenter(6473.559, -1051.568, 565.0311350, 300)
GMR.DefineProfileCenter(6593.718, -930.4932, 680.0869350, 300)
GMR.DefineProfileCenter(6431.465, -868.575, 561.2204350, 300)
GMR.DefineProfileCenter(6286.728, -948.7196, 478.4705350, 300)
GMR.DefineProfileCenter(6083.572, -997.1887, 482.4465350, 300)
GMR.DefineProfileCenter(5904.672, -978.7485, 532.0575350, 300)
GMR.DefineProfileCenter(5806.201, -882.8904, 392.9406350, 300)
GMR.DefineProfileCenter(5640.161, -806.9379, 322.5244350, 300)
GMR.DefineProfileCenter(5815.679, -815.0114, 377.2189350, 300)
GMR.DefineProfileCenter(5971.678, -750.4229, 446.5224350, 300)
GMR.DefineProfileCenter(6000.946, -563.6541, 505.3311350, 300)
GMR.DefineProfileCenter(5866.945, -454.4272, 384.7316350, 300)
GMR.DefineProfileCenter(5692.776, -496.2449, 294.6739350, 300)
GMR.DefineProfileCenter(5866.572, -462.2479, 405.9002350, 300)
GMR.DefineProfileCenter(6051.368, -447.2581, 478.6096350, 300)
GMR.DefineProfileCenter(6169.547, -306.6486, 553.1350, 300)
GMR.DefineProfileCenter(6313.606, -413.0952, 624.7986350, 300)
GMR.DefineProfileCenter(6326.342, -611.1881, 581.5763350, 300)
GMR.DefineProfileCenter(6454.458, -709.7217, 684.8882350, 300)
GMR.DefineProfileCenter(6597.213, -834.4784, 746.8808350, 300)
GMR.DefineProfileCenter(6676.121, -978.2055, 849.5403350, 300)
GMR.DefineProfileCenter(6682.263, -1184.741, 829.9066350, 300)
GMR.DefineProfileCenter(6700.316, -1363.931, 820.9733350, 300)
GMR.DefineProfileCenter(6703.123, -1540.492, 880.3336350, 300)
GMR.DefineProfileCenter(6761.86, -1710.971, 917.2092350, 300)
GMR.DefineProfileCenter(6887.64, -1573.824, 878.0493350, 300)
GMR.DefineProfileCenter(7065.555, -1533.331, 949.9111350, 300)
GMR.DefineProfileCenter(7219.96, -1580.68, 1068.306350, 300)
GMR.DefineProfileCenter(7298.493, -1705.89, 1216.402350, 300)
GMR.DefineProfileCenter(7363.372, -1831.33, 1364.144350, 300)
GMR.DefineProfileCenter(7401.014, -1694.027, 1226.22350, 300)
GMR.DefineProfileCenter(7446.838, -1539.219, 1132.332350, 300)
GMR.DefineProfileCenter(7477.221, -1398.199, 1022.898350, 300)
GMR.DefineProfileCenter(7615.161, -1275.428, 1021.262350, 300)
GMR.DefineProfileCenter(7734.361, -1107.112, 996.6498350, 300)
GMR.DefineProfileCenter(7843.391, -1024.7, 1055.346350, 300)
GMR.DefineProfileCenter(7685.57, -931.9965, 1006.626350, 300)
GMR.DefineProfileCenter(7500.973, -843.2722, 1005.866350, 300)
GMR.DefineProfileCenter(7321.643, -805.7969, 1005.185350, 300)
GMR.DefineProfileCenter(7155.14, -763.4377, 947.4742350, 300)
GMR.DefineProfileCenter(6957.92, -723.8434, 892.1231350, 300)
GMR.DefineProfileCenter(6758.865, -756.6216, 843.9441350, 300)
GMR.DefineProfileCenter(6564.395, -796.5075, 790.7165350, 300)
GMR.DefineProfileCenter(6704.467, -703.3533, 899.4317350, 300)
GMR.DefineProfileCenter(6811.116, -633.4082, 1057.293350, 300)
GMR.DefineProfileCenter(6962.118, -558.1517, 983.1917350, 300)
GMR.DefineProfileCenter(7068.916, -423.3998, 920.4697350, 300)
GMR.DefineProfileCenter(6947.845, -283.0123, 968.3387350, 300)
GMR.DefineProfileCenter(6814.713, -365.5067, 1087.758350, 300)
GMR.DefineProfileCenter(6816.514, -228.9466, 962.2131350, 300)
GMR.DefineProfileCenter(6759.196, -48.87444, 872.6779350, 300)
GMR.DefineProfileCenter(6755.787, 57.5104, 825.6678350, 300)
GMR.DefineProfileCenter(6882.409, -108.5881, 854.4424350, 300)
GMR.DefineProfileCenter(7015.623, -265.3293, 842.1517350, 300)
GMR.DefineProfileCenter(7120.433, -371.0049, 919.4741350, 300)
GMR.DefineProfileCenter(7187.043, -204.3168, 890.7305350, 300)
GMR.DefineProfileCenter(7353.887, -115.005, 882.001350, 300)
GMR.DefineProfileCenter(7541.745, -107.0175, 934.5081350, 300)
GMR.DefineProfileCenter(7396.068, 25.26676, 882.7806350, 300)
GMR.DefineProfileCenter(7247.338, 123.0436, 912.1499350, 300)
GMR.DefineProfileCenter(7192.44, 247.2679, 862.3643350, 300)
GMR.DefineProfileCenter(7398.214, 238.7866, 876.2106350, 300)
GMR.DefineProfileCenter(7586.231, 218.8727, 916.1074350, 300)
GMR.DefineProfileCenter(7510.461, 21.71321, 898.7565350, 300)
GMR.DefineProfileCenter(7654.328, -121.982, 939.404350, 300)
GMR.DefineProfileCenter(7784.931, -280.3823, 962.4941350, 300)
GMR.DefineProfileCenter(7834.81, -477.3965, 995.603350, 300)
GMR.DefineProfileCenter(7839.751, -271.9169, 991.6856350, 300)
GMR.DefineProfileCenter(7832.184, -82.10206, 1025.837350, 300)
GMR.DefineProfileCenter(7928.869, 67.01112, 1096.737350, 300)
GMR.DefineProfileCenter(8121.708, 82.95632, 1023.433350, 300)
GMR.DefineProfileCenter(8248.548, 74.74854, 939.7356350, 300)
GMR.DefineProfileCenter(8256.68, 266.123, 927.9464350, 300)
GMR.DefineProfileCenter(8422.276, 245.2324, 853.7153350, 300)
GMR.DefineProfileCenter(8491.122, -16.74867, 854.6159350, 300)
GMR.DefineProfileCenter(8472.983, -211.7667, 900.6945350, 300)
GMR.DefineProfileCenter(8303.887, -239.5985, 1000.256350, 300)
GMR.DefineProfileCenter(8146.161, -149.0813, 938.1986350, 300)
GMR.DefineProfileCenter(7972.376, -257.0612, 962.0155350, 300)
GMR.DefineProfileCenter(7940.31, -454.6939, 961.1298350, 300)
GMR.DefineProfileCenter(7937.23, -602.0327, 1108.682350, 300)
GMR.DefineProfileCenter(7951.687, -695.8759, 1128.843350, 300)
GMR.DefineProfileCenter(8108.18, -581.14, 1047.088350, 300)
GMR.DefineProfileCenter(8289.886, -612.2508, 1036.602350, 300)
GMR.DefineProfileCenter(8355.964, -784.2608, 1009.266350, 300)
GMR.DefineProfileCenter(8214.893, -910.8698, 1007.161350, 300)
GMR.DefineProfileCenter(8086.559, -1023.729, 1104.825350, 300)
GMR.DefineProfileCenter(8120.625, -1218.853, 1096.207350, 300)
GMR.DefineProfileCenter(8299.269, -1298.845, 1061.873350, 300)
GMR.DefineProfileCenter(8477.498, -1400.46, 1054.829350, 300)
GMR.DefineProfileCenter(8502.719, -1597.398, 1063.755350, 300)
GMR.DefineProfileCenter(8404.163, -1533.823, 1227.649350, 300)
GMR.DefineProfileCenter(8174.55, -1593.455, 1281.709350, 300)
GMR.DefineProfileCenter(7855.289, -1720.769, 1330.635350, 300)
GMR.DefineProfileCenter(7834.78, -1828.336, 1319.005350, 300)
GMR.DefineProfileCenter(7688.688, -1933.448, 1331.748350, 300)
GMR.DefineProfileCenter(7705.976, -2119.592, 1269.345350, 300)
GMR.DefineProfileCenter(7879.805, -2195.404, 1283.961350, 300)
GMR.DefineProfileCenter(8073.636, -2260.662, 1267.084350, 300)
GMR.DefineProfileCenter(8213.404, -2266.023, 1236.717350, 300)
GMR.DefineProfileCenter(8234.479, -2473.573, 1262.749350, 300)
GMR.DefineProfileCenter(8109.638, -2480.496, 1245.693350, 300)
GMR.DefineProfileCenter(7939.739, -2370.204, 1255.509350, 300)
GMR.DefineProfileCenter(7802.391, -2490.014, 1229.958350, 300)
GMR.DefineProfileCenter(7958.68, -2587.984, 1252.511350, 300)
GMR.DefineProfileCenter(8100.092, -2727.448, 1235.226350, 300)
GMR.DefineProfileCenter(7987.425, -2879.404, 1249.828350, 300)
GMR.DefineProfileCenter(7833.279, -2773.972, 1238.869350, 300)
GMR.DefineProfileCenter(7896.214, -2947.667, 1238.51350, 300)
GMR.DefineProfileCenter(7970.24, -3117.712, 1158.745350, 300)
GMR.DefineProfileCenter(8002.736, -3267.763, 1018.748350, 300)
GMR.DefineProfileCenter(7841.018, -3365.972, 953.4933350, 300)
GMR.DefineProfileCenter(7652.528, -3317.32, 949.0635350, 300)
GMR.DefineProfileCenter(7786.245, -3182.111, 940.5895350, 300)
GMR.DefineProfileCenter(7650.85, -3056.483, 972.5842350, 300)
GMR.DefineProfileCenter(7561.985, -3231.635, 936.47350, 300)
GMR.DefineProfileCenter(7387.271, -3297.03, 933.6709350, 300)
GMR.DefineProfileCenter(7184.416, -3316.275, 933.5439350, 300)
GMR.DefineProfileCenter(7206.668, -3131.654, 918.055350, 300)
GMR.DefineProfileCenter(7152.953, -2965.047, 983.6035350, 300)
GMR.DefineProfileCenter(7134.501, -2775.62, 974.1706350, 300)
GMR.DefineProfileCenter(6971.553, -2749.465, 852.0516350, 300)
GMR.DefineProfileCenter(6829.93, -2749.587, 696.726350, 300)
GMR.DefineProfileCenter(6687.222, -2811.125, 519.0998350, 300)
GMR.DefineProfileCenter(6807.295, -2874.968, 662.0988350, 300)
GMR.DefineProfileCenter(6892.986, -2980.528, 800.4417350, 300)
GMR.DefineProfileCenter(6836.455, -3138.205, 676.7752350, 300)
GMR.DefineProfileCenter(6845.821, -3328.932, 684.2274350, 300)
GMR.DefineProfileCenter(6825.214, -3521.64, 714.1238350, 300)
GMR.DefineProfileCenter(6829.327, -3705.225, 751.2226350, 300)
GMR.DefineProfileCenter(6864.966, -3894.639, 707.8796350, 300)
GMR.DefineProfileCenter(6845.825, -4003.32, 655.8345350, 300)
GMR.DefineProfileCenter(6660.944, -4000.091, 589.2098350, 300)
GMR.DefineProfileCenter(6713.978, -3815.161, 650.4695350, 300)
GMR.DefineProfileCenter(6737.732, -3622.455, 680.5406350, 300)
GMR.DefineProfileCenter(6701.977, -3428.182, 618.9272350, 300)
GMR.DefineProfileCenter(6695.036, -3223.628, 574.4489350, 300)
GMR.DefineProfileCenter(6797.494, -3071.995, 642.3231350, 300)
GMR.DefineProfileCenter(6886.203, -2900.385, 717.9998350, 300)
GMR.DefineProfileCenter(6770.653, -2757.024, 640.7251350, 300)
GMR.DefineProfileCenter(6622.694, -2753.533, 498.0523350, 300)
GMR.DefineProfileCenter(6629.917, -2544.25, 479.4268350, 300)
GMR.DefineProfileCenter(6689.797, -2394.907, 592.9084350, 300)
GMR.DefineProfileCenter(6789.889, -2286.975, 741.5274350, 300)
GMR.DefineProfileCenter(6920.638, -2174.303, 813.3458350, 300)
GMR.DefineProfileCenter(6829.906, -2028.827, 924.5223350, 300)
GMR.DefineProfileCenter(6905.884, -1855.057, 969.908350, 300)
GMR.DefineProfileCenter(6764.826, -1731.061, 945.1166350, 300)
GMR.DefineProfileCenter(6571.27, -1773.673, 890.8461350, 300)
GMR.DefineProfileCenter(6513.094, -1683.957, 728.3383350, 300)
GMR.DefineProfileCenter(6668.934, -1639.031, 811.4333350, 300)
GMR.DefineProfileCenter(6853.869, -1671.226, 868.5029350, 300)
GMR.DefineProfileCenter(6789.469, -1497.708, 863.5779350, 300)
GMR.DefineProfileCenter(6710.675, -1336.261, 816.9906350, 300)
GMR.DefineProfileCenter(6701.865, -1150.246, 839.3129350, 300)
GMR.DefineProfileCenter(6675.695, -941.4441, 837.9758350, 300)
GMR.DefineProfileCenter(6736.836, -752.4547, 828.7621350, 300)
GMR.DefineProfileCenter(6613.6, -890.3649, 750.8132350, 300)
GMR.DefineProfileCenter(6529.782, -1032.627, 624.5796350, 300)
GMR.DefineProfileCenter(6469.046, -1178.541, 528.3465350, 300)
GMR.DefineProfileCenter(6525.549, -1339.742, 617.3757350, 300)
GMR.DefineProfileCenter(6495.722, -1536.693, 640.935350, 300)
GMR.DefineAreaBlacklist(7871.514, -3179.168, 857.5828, 30)
GMR.DefineAreaBlacklist(6897.924, -602.1816, 924.7097, 30)
GMR.DefineAreaBlacklist(7583.5390, -1490.1670, 977.7716, 30)
GMR.DefineAreaBlacklist(7635.0530, -1556.1210, 973.0212, 30)
GMR.DefineAreaBlacklist(7703.0940, -1659.7730, 953.9519, 30)
GMR.DefineAreaBlacklist(7747.1150, -1599.4560, 945.7670, 30)
GMR.DefineAreaBlacklist(7825.3880, -1548.9910, 933.7877, 30)
GMR.DefineAreaBlacklist(7897.4610, -1512.6010, 920.2105, 30)
GMR.DefineAreaBlacklist(7228.2010, -1130.6070, 939.0313, 30)
GMR.DefineAreaBlacklist(7170.7740, -1147.0950, 936.5327, 30)
GMR.DefineAreaBlacklist(7142.9180, -1218.9850, 923.8766, 30)
GMR.DefineAreaBlacklist(7095.0990, -1133.4020, 921.8155, 45)
GMR.DefineAreaBlacklist(7035.0880, -1195.6100, 928.7196, 30)
GMR.DefineAreaBlacklist(6984.3130, -1238.4340, 930.5374, 30)
GMR.DefineAreaBlacklist(7033.9450, -1299.3500, 939.7466, 30)
GMR.DefineAreaBlacklist(7041.7930, -1285.4910, 919.5781, 30)
GMR.DefineAreaBlacklist(7124.3330, -1314.0210, 916.3643, 50)
GMR.DefineAreaBlacklist(6985.3460, -1216.7740, 927.8894, 30)
GMR.DefineAreaBlacklist(7112.9800, -1107.3820, 924.0865, 30)
GMR.DefineAreaBlacklist(7140.1200, -1150.1910, 931.4960, 30)
GMR.DefineAreaBlacklist(6506.1520, -1032.8920, 435.7122, 30)
GMR.DefineAreaBlacklist(6567.7390, -1024.9770, 433.4644, 60)
GMR.DefineAreaBlacklist(6677.0830, -1009.8670, 414.8397, 30)
GMR.DefineAreaBlacklist(6647.6170, -1083.5200, 401.9590, 60)
GMR.DefineAreaBlacklist(6635.5930, -1112.7940, 426.6802, 30)
GMR.DefineAreaBlacklist(6685.8730, -1106.9430, 396.8469, 30)
GMR.DefineAreaBlacklist(6660.7410, -1190.5330, 398.7615, 60)
GMR.DefineAreaBlacklist(6680.7840, -1276.5800, 396.2646, 30)
GMR.DefineAreaBlacklist(6731.5270, -1318.0480, 395.8602, 30)
GMR.DefineAreaBlacklist(6685.2420, -1370.4150, 391.0064, 30)
GMR.DefineAreaBlacklist(6699.5610, -1441.9990, 385.2171, 30)
GMR.DefineAreaBlacklist(6728.5890, -1535.5280, 367.7092, 50)
GMR.DefineAreaBlacklist(6894.3430, -1522.0430, 358.3529, 30)
GMR.DefineAreaBlacklist(6817.8680, -1599.3550, 351.9640, 55)
GMR.DefineAreaBlacklist(6728.8220, -1537.2530, 367.4264, 30)
GMR.DefineAreaBlacklist(6701.5460, -1344.2680, 394.7059, 30)
GMR.DefineAreaBlacklist(6543.2680, -1260.9330, 400.7551, 30)
GMR.DefineAreaBlacklist(6581.9930, -1240.9330, 399.1515, 30)
GMR.DefineAreaBlacklist(6596.4680, -1336.3070, 404.4000, 30)
GMR.DefineAreaBlacklist(7174.3520, -2125.5850, 763.0121, 30)
GMR.DefineAreaBlacklist(7202.5430, -2106.7280, 767.9936, 30)
GMR.DefineAreaBlacklist(7247.3560, -2123.0880, 778.3644, 30)
GMR.DefineAreaBlacklist(7309.4660, -2104.9090, 773.8260, 30)
GMR.DefineAreaBlacklist(7333.7090, -2041.4540, 762.9391, 30)
GMR.DefineAreaBlacklist(7293.8250, -2065.1880, 761.9807, 30)
GMR.DefineAreaBlacklist(7287.3680, -2008.1560, 764.6655, 30)
GMR.DefineAreaBlacklist(7247.3110, -1974.1390, 770.7268, 30)
GMR.DefineAreaBlacklist(7210.3700, -1994.2820, 767.7876, 30)
GMR.DefineAreaBlacklist(7195.7260, -2060.4720, 764.6873, 30)
GMR.DefineAreaBlacklist(7202.9390, -2113.8920, 768.7082, 30)
GMR.DefineAreaBlacklist(7251.2920, -2118.7020, 778.8754, 30)
GMR.DefineAreaBlacklist(7310.3160, -2104.6930, 773.8583, 30)
GMR.DefineAreaBlacklist(7238.6620, -2077.5390, 763.3433, 30)
GMR.DefineAreaBlacklist(7289.2570, -2056.0320, 761.9616, 30)
GMR.DefineAreaBlacklist(7386.1810, -2666.7670, 748.2072, 30)
GMR.DefineAreaBlacklist(7401.2710, -2770.8630, 770.9587, 30)
GMR.DefineAreaBlacklist(7251.8760, -2845.7440, 777.7433, 30)
GMR.DefineAreaBlacklist(7196.4250, -2651.1860, 813.4915, 30)
GMR.DefineAreaBlacklist(7306.8430, -2613.5160, 814.8335, 30)
GMR.DefineAreaBlacklist(7454.4660, -2669.0200, 808.3264, 30)
GMR.DefineAreaBlacklist(8117.0060, -2981.2750, 1127.1780, 70)
GMR.DefineAreaBlacklist(8235.5160, -3105.7830, 1122.8530, 30)
GMR.DefineAreaBlacklist(8286.9130, -2957.3880, 1070.8570, 50)
GMR.DefineAreaBlacklist(8166.7680, -2928.3610, 1048.5370, 55)
GMR.DefineAreaBlacklist(8298.3750, -2792.7400, 1042.3890, 60)
GMR.DefineAreaBlacklist(8145.1530, -2726.9530, 1028.8050, 60)
GMR.DefineAreaBlacklist(8122.6160, -2817.4180, 1045.7870, 30)
GMR.DefineAreaBlacklist(8262.8040, -2588.4740, 1148.3220, 30)
GMR.DefineAreaBlacklist(8296.1120, -2564.2230, 1147.5900, 30)
GMR.DefineAreaBlacklist(8312.7190, -2536.8350, 1150.8360, 30)
GMR.DefineAreaBlacklist(8334.2720, -2521.4270, 1138.8220, 30)
GMR.DefineAreaBlacklist(8378.4120, -2537.1100, 1131.2300, 30)
GMR.DefineAreaBlacklist(8232.8860, -2600.7220, 1150.6270, 30)
GMR.DefineAreaBlacklist(6903.2320, -1150.3110, 804.6217, 60)
GMR.DefineAreaBlacklist(6911.9420, -1198.0080, 807.3357, 60)
GMR.DefineAreaBlacklist(6921.8020, -1221.6620, 808.4130, 60)
GMR.DefineAreaBlacklist(6977.3350, -1217.2660, 808.4078, 60)
GMR.DefineAreaBlacklist(6878.0570, -1245.3640, 809.8047, 60)
GMR.DefineAreaBlacklist(6850.9670, -1241.0650, 805.8053, 60)
GMR.DefineAreaBlacklist(6880.5160, -1295.2620, 819.3554, 60)
GMR.DefineAreaBlacklist(6846.9960, -1316.2100, 819.2816, 60)
GMR.DefineAreaBlacklist(6818.5640, -1350.4770, 819.2200, 60)
GMR.DefineAreaBlacklist(6909.4930, -1294.8810, 817.3395, 80)
GMR.DefineAreaBlacklist(6981.7290, -1298.8870, 819.8673, 60)
GMR.DefineAreaBlacklist(7006.6200, -1307.7380, 818.8802, 60)
GMR.DefineAreaBlacklist(7035.3750, -1307.4180, 819.4522, 60)
GMR.DefineAreaBlacklist(6958.5640, -1312.6930, 821.6508, 60)
GMR.DefineAreaBlacklist(6961.7140, -1350.0540, 830.6466, 60)
GMR.DefineAreaBlacklist(6991.2560, -1350.1100, 830.1817, 60)
GMR.DefineAreaBlacklist(7021.1760, -1356.4760, 833.0851, 22)
GMR.DefineAreaBlacklist(6865.3580, -1353.6270, 831.4898, 60)
GMR.DefineAreaBlacklist(6831.3560, -1319.3530, 833.4532, 60)
GMR.DefineAreaBlacklist(6845.9370, -1324.9970, 819.3679, 60)
GMR.DefineAreaBlacklist(6890.8650, -1357.1580, 831.9649, 60)
GMR.DefineAreaBlacklist(6961.0870, -1350.4390, 830.6202, 60)
GMR.DefineAreaBlacklist(6941.0760, -1390.9820, 830.2859, 60)
GMR.DefineAreaBlacklist(6950.3790, -1454.2340, 841.2368, 60)
GMR.DefineAreaBlacklist(7241.5030, -1124.5170, 940.8931, 60)
GMR.DefineAreaBlacklist(7206.1850, -1147.2730, 938.1954, 60)
GMR.DefineAreaBlacklist(7167.6170, -1115.6680, 933.2911, 60)
GMR.DefineAreaBlacklist(7172.6520, -1145.5950, 936.1915, 60)
GMR.DefineAreaBlacklist(7171.0410, -1187.3170, 934.5790, 60)
GMR.DefineAreaBlacklist(7145.2420, -1216.8290, 924.3041, 60)
GMR.DefineAreaBlacklist(7111.9490, -1227.9480, 923.8115, 60)
GMR.DefineAreaBlacklist(7155.2940, -1212.4190, 926.4653, 60)
GMR.DefineAreaBlacklist(7169.9340, -1168.6070, 937.9244, 60)
GMR.DefineAreaBlacklist(7076.1060, -1199.9820, 923.9446, 60)
GMR.DefineAreaBlacklist(7031.5000, -1195.0750, 928.7473, 60)
GMR.DefineAreaBlacklist(6983.1970, -1212.6030, 928.2952, 60)
GMR.DefineAreaBlacklist(6984.9030, -1247.7600, 930.5071, 60)
GMR.DefineAreaBlacklist(7020.9210, -1295.9500, 936.8399, 60)
GMR.DefineAreaBlacklist(7040.3950, -1330.3420, 936.9823, 60)
GMR.DefineAreaBlacklist(7052.4650, -1258.0670, 942.2608, 60)
GMR.DefineAreaBlacklist(7016.9690, -1278.7550, 926.6236, 60)
GMR.DefineAreaBlacklist(7060.2890, -1281.9700, 919.1013, 60)
GMR.DefineAreaBlacklist(7114.4030, -1290.9490, 916.3641, 80)
GMR.DefineAreaBlacklist(7134.8090, -1332.0010, 916.3641, 60)
GMR.DefineAreaBlacklist(7081.6040, -1334.7730, 919.4312, 60)
GMR.DefineAreaBlacklist(6983.4950, -1242.0080, 930.4236, 80)
GMR.DefineAreaBlacklist(7095.1200, -1139.4890, 921.7855, 82)
GMR.DefineAreaBlacklist(7059.7850, -1111.8410, 932.0320, 60)
GMR.DefineAreaBlacklist(7086.7530, -1098.4480, 930.7043, 60)
GMR.DefineAreaBlacklist(7117.5510, -1102.8410, 925.2039, 60)
GMR.DefineAreaBlacklist(7134.9740, -1150.2990, 929.6840, 60)
GMR.DefineAreaBlacklist(7094.6910, -1234.5450, 923.8138, 60)
GMR.DefineAreaBlacklist(7166.7070, -1192.2130, 933.6357, 60)
GMR.DefineAreaBlacklist(7169.4720, -1111.9200, 933.8060, 60)
GMR.DefineAreaBlacklist(7221.0100, -1158.5610, 936.5198, 60)
GMR.DefineAreaBlacklist(7232.2540, -1135.8180, 938.3804, 30)
GMR.DefineAreaBlacklist(7236.0010, -1118.6640, 941.4749, 30)
GMR.DefineAreaBlacklist(8084.2630, -2989.0310, 1133.9700, 60)
GMR.DefineAreaBlacklist(8132.8760, -2990.1550, 1121.8340, 60)
GMR.DefineAreaBlacklist(8193.0740, -3091.1940, 1117.6310, 60)
GMR.DefineAreaBlacklist(8256.9830, -3113.1390, 1123.9780, 60)
GMR.DefineAreaBlacklist(8195.1930, -2995.8670, 1100.3180, 60)
GMR.DefineAreaBlacklist(8199.6460, -3054.5470, 1104.4070, 60)
GMR.DefineAreaBlacklist(8263.8860, -3040.5260, 1078.0860, 60)
GMR.DefineAreaBlacklist(8298.2690, -2880.7260, 1058.3890, 60)
GMR.DefineAreaBlacklist(8283.0920, -2872.5150, 1116.0980, 60)
GMR.DefineAreaBlacklist(8330.7690, -2842.0240, 1049.2200, 60)
GMR.DefineAreaBlacklist(8317.6510, -2846.5840, 1080.8240, 60)
GMR.DefineAreaBlacklist(8340.9060, -2765.8200, 1068.3460, 60)
GMR.DefineAreaBlacklist(8341.3280, -2770.9940, 1038.6470, 60)
GMR.DefineAreaBlacklist(8334.9390, -2724.1860, 1041.7830, 60)
GMR.DefineAreaBlacklist(8291.9540, -2687.8340, 1037.5800, 60)
GMR.DefineAreaBlacklist(8269.4550, -2724.6980, 1035.9820, 60)
GMR.DefineAreaBlacklist(8230.0660, -2762.7070, 1040.0810, 60)
GMR.DefineAreaBlacklist(8187.0980, -2726.4500, 1033.6680, 60)
GMR.DefineAreaBlacklist(8108.6190, -2689.5520, 1032.8830, 60)
GMR.DefineAreaBlacklist(8084.7750, -2733.7720, 1028.5220, 60)
GMR.DefineAreaBlacklist(8067.7920, -2777.6860, 1050.8420, 60)
GMR.DefineAreaBlacklist(8096.2530, -2831.4410, 1045.4770, 60)
GMR.DefineAreaBlacklist(8169.4550, -2851.8440, 1046.6440, 30)
GMR.DefineAreaBlacklist(8241.7310, -2831.7180, 1050.6810, 30)
GMR.DefineAreaBlacklist(8157.7130, -2907.0060, 1050.9410, 30)
GMR.DefineAreaBlacklist(8086.4150, -2933.2070, 1135.2700, 30)
GMR.DefineAreaBlacklist(8161.8410, -2848.1270, 1139.1430, 30)
GMR.DefineAreaBlacklist(8185.3380, -2876.2200, 1137.9450, 30)
GMR.DefineAreaBlacklist(8233.0560, -2600.3650, 1150.5790, 30)
GMR.DefineMeshAreaBlacklist(8169.4550, -2851.8440, 1046.6440, 30)
GMR.DefineMeshAreaBlacklist(8241.7310, -2831.7180, 1050.6810, 30)
GMR.DefineMeshAreaBlacklist(8157.7130, -2907.0060, 1050.9410, 30)
GMR.DefineMeshAreaBlacklist(8086.4150, -2933.2070, 1135.2700, 30)
GMR.DefineMeshAreaBlacklist(8161.8410, -2848.1270, 1139.1430, 30)
GMR.DefineMeshAreaBlacklist(8185.3380, -2876.2200, 1137.9450, 30)
GMR.DefineMeshAreaBlacklist(8233.0560, -2600.3650, 1150.5790, 30)
GMR.DefineMeshAreaBlacklist(8084.2630, -2989.0310, 1133.9700, 60)
GMR.DefineMeshAreaBlacklist(8132.8760, -2990.1550, 1121.8340, 60)
GMR.DefineMeshAreaBlacklist(8193.0740, -3091.1940, 1117.6310, 60)
GMR.DefineMeshAreaBlacklist(8256.9830, -3113.1390, 1123.9780, 60)
GMR.DefineMeshAreaBlacklist(8195.1930, -2995.8670, 1100.3180, 60)
GMR.DefineMeshAreaBlacklist(8199.6460, -3054.5470, 1104.4070, 60)
GMR.DefineMeshAreaBlacklist(8263.8860, -3040.5260, 1078.0860, 60)
GMR.DefineMeshAreaBlacklist(8298.2690, -2880.7260, 1058.3890, 60)
GMR.DefineMeshAreaBlacklist(8283.0920, -2872.5150, 1116.0980, 60)
GMR.DefineMeshAreaBlacklist(8330.7690, -2842.0240, 1049.2200, 60)
GMR.DefineMeshAreaBlacklist(8317.6510, -2846.5840, 1080.8240, 60)
GMR.DefineMeshAreaBlacklist(8340.9060, -2765.8200, 1068.3460, 60)
GMR.DefineMeshAreaBlacklist(8341.3280, -2770.9940, 1038.6470, 60)
GMR.DefineMeshAreaBlacklist(8334.9390, -2724.1860, 1041.7830, 60)
GMR.DefineMeshAreaBlacklist(8291.9540, -2687.8340, 1037.5800, 60)
GMR.DefineMeshAreaBlacklist(8269.4550, -2724.6980, 1035.9820, 60)
GMR.DefineMeshAreaBlacklist(8230.0660, -2762.7070, 1040.0810, 60)
GMR.DefineMeshAreaBlacklist(8187.0980, -2726.4500, 1033.6680, 60)
GMR.DefineMeshAreaBlacklist(8108.6190, -2689.5520, 1032.8830, 60)
GMR.DefineMeshAreaBlacklist(8084.7750, -2733.7720, 1028.5220, 60)
GMR.DefineMeshAreaBlacklist(8067.7920, -2777.6860, 1050.8420, 60)
GMR.DefineMeshAreaBlacklist(8096.2530, -2831.4410, 1045.4770, 60)
GMR.DefineMeshAreaBlacklist(6903.2320, -1150.3110, 804.6217, 60)
GMR.DefineMeshAreaBlacklist(6911.9420, -1198.0080, 807.3357, 60)
GMR.DefineMeshAreaBlacklist(6921.8020, -1221.6620, 808.4130, 60)
GMR.DefineMeshAreaBlacklist(6977.3350, -1217.2660, 808.4078, 60)
GMR.DefineMeshAreaBlacklist(6878.0570, -1245.3640, 809.8047, 60)
GMR.DefineMeshAreaBlacklist(6850.9670, -1241.0650, 805.8053, 60)
GMR.DefineMeshAreaBlacklist(6880.5160, -1295.2620, 819.3554, 60)
GMR.DefineMeshAreaBlacklist(6846.9960, -1316.2100, 819.2816, 60)
GMR.DefineMeshAreaBlacklist(6818.5640, -1350.4770, 819.2200, 60)
GMR.DefineMeshAreaBlacklist(6909.4930, -1294.8810, 817.3395, 80)
GMR.DefineMeshAreaBlacklist(6981.7290, -1298.8870, 819.8673, 60)
GMR.DefineMeshAreaBlacklist(7006.6200, -1307.7380, 818.8802, 60)
GMR.DefineMeshAreaBlacklist(7035.3750, -1307.4180, 819.4522, 60)
GMR.DefineMeshAreaBlacklist(6958.5640, -1312.6930, 821.6508, 60)
GMR.DefineMeshAreaBlacklist(6961.7140, -1350.0540, 830.6466, 60)
GMR.DefineMeshAreaBlacklist(6991.2560, -1350.1100, 830.1817, 60)
GMR.DefineMeshAreaBlacklist(7021.1760, -1356.4760, 833.0851, 22)
GMR.DefineMeshAreaBlacklist(6865.3580, -1353.6270, 831.4898, 60)
GMR.DefineMeshAreaBlacklist(6831.3560, -1319.3530, 833.4532, 60)
GMR.DefineMeshAreaBlacklist(6845.9370, -1324.9970, 819.3679, 60)
GMR.DefineMeshAreaBlacklist(6890.8650, -1357.1580, 831.9649, 60)
GMR.DefineMeshAreaBlacklist(6961.0870, -1350.4390, 830.6202, 60)
GMR.DefineMeshAreaBlacklist(6941.0760, -1390.9820, 830.2859, 60)
GMR.DefineMeshAreaBlacklist(6950.3790, -1454.2340, 841.2368, 60)
GMR.DefineMeshAreaBlacklist(7241.5030, -1124.5170, 940.8931, 60)
GMR.DefineMeshAreaBlacklist(7206.1850, -1147.2730, 938.1954, 60)
GMR.DefineMeshAreaBlacklist(7167.6170, -1115.6680, 933.2911, 60)
GMR.DefineMeshAreaBlacklist(7172.6520, -1145.5950, 936.1915, 60)
GMR.DefineMeshAreaBlacklist(7171.0410, -1187.3170, 934.5790, 60)
GMR.DefineMeshAreaBlacklist(7145.2420, -1216.8290, 924.3041, 60)
GMR.DefineMeshAreaBlacklist(7111.9490, -1227.9480, 923.8115, 60)
GMR.DefineMeshAreaBlacklist(7155.2940, -1212.4190, 926.4653, 60)
GMR.DefineMeshAreaBlacklist(7169.9340, -1168.6070, 937.9244, 60)
GMR.DefineMeshAreaBlacklist(7076.1060, -1199.9820, 923.9446, 60)
GMR.DefineMeshAreaBlacklist(7031.5000, -1195.0750, 928.7473, 60)
GMR.DefineMeshAreaBlacklist(6983.1970, -1212.6030, 928.2952, 60)
GMR.DefineMeshAreaBlacklist(6984.9030, -1247.7600, 930.5071, 60)
GMR.DefineMeshAreaBlacklist(7020.9210, -1295.9500, 936.8399, 60)
GMR.DefineMeshAreaBlacklist(7040.3950, -1330.3420, 936.9823, 60)
GMR.DefineMeshAreaBlacklist(7052.4650, -1258.0670, 942.2608, 60)
GMR.DefineMeshAreaBlacklist(7016.9690, -1278.7550, 926.6236, 60)
GMR.DefineMeshAreaBlacklist(7060.2890, -1281.9700, 919.1013, 60)
GMR.DefineMeshAreaBlacklist(7114.4030, -1290.9490, 916.3641, 60)
GMR.DefineMeshAreaBlacklist(7134.8090, -1332.0010, 916.3641, 60)
GMR.DefineMeshAreaBlacklist(7081.6040, -1334.7730, 919.4312, 60)
GMR.DefineMeshAreaBlacklist(6983.4950, -1242.0080, 930.4236, 80)
GMR.DefineMeshAreaBlacklist(7095.1200, -1139.4890, 921.7855, 80)
GMR.DefineMeshAreaBlacklist(7059.7850, -1111.8410, 932.0320, 60)
GMR.DefineMeshAreaBlacklist(7086.7530, -1098.4480, 930.7043, 60)
GMR.DefineMeshAreaBlacklist(7117.5510, -1102.8410, 925.2039, 60)
GMR.DefineMeshAreaBlacklist(7134.9740, -1150.2990, 929.6840, 60)
GMR.DefineMeshAreaBlacklist(7094.6910, -1234.5450, 923.8138, 60)
GMR.DefineMeshAreaBlacklist(7166.7070, -1192.2130, 933.6357, 60)
GMR.DefineMeshAreaBlacklist(7169.4720, -1111.9200, 933.8060, 60)
GMR.DefineMeshAreaBlacklist(7221.0100, -1158.5610, 936.5198, 60)
GMR.DefineMeshAreaBlacklist(7232.2540, -1135.8180, 938.3804, 30)
GMR.DefineMeshAreaBlacklist(7236.0010, -1118.6640, 941.4749, 30)
GMR.DefineMeshAreaBlacklist(8117.0060, -2981.2750, 1127.1780, 70)
GMR.DefineMeshAreaBlacklist(8235.5160, -3105.7830, 1122.8530, 30)
GMR.DefineMeshAreaBlacklist(8286.9130, -2957.3880, 1070.8570, 50)
GMR.DefineMeshAreaBlacklist(8166.7680, -2928.3610, 1048.5370, 55)
GMR.DefineMeshAreaBlacklist(8298.3750, -2792.7400, 1042.3890, 60)
GMR.DefineMeshAreaBlacklist(8145.1530, -2726.9530, 1028.8050, 60)
GMR.DefineMeshAreaBlacklist(8122.6160, -2817.4180, 1045.7870, 30)
GMR.DefineMeshAreaBlacklist(8262.8040, -2588.4740, 1148.3220, 30)
GMR.DefineMeshAreaBlacklist(8296.1120, -2564.2230, 1147.5900, 30)
GMR.DefineMeshAreaBlacklist(8312.7190, -2536.8350, 1150.8360, 30)
GMR.DefineMeshAreaBlacklist(8334.2720, -2521.4270, 1138.8220, 30)
GMR.DefineMeshAreaBlacklist(8378.4120, -2537.1100, 1131.2300, 30)
GMR.DefineMeshAreaBlacklist(8232.8860, -2600.7220, 1150.6270, 30)
GMR.DefineMeshAreaBlacklist(6543.2680, -1260.9330, 400.7551, 30)
GMR.DefineMeshAreaBlacklist(6581.9930, -1240.9330, 399.1515, 30)
GMR.DefineMeshAreaBlacklist(6596.4680, -1336.3070, 404.4000, 30)
GMR.DefineMeshAreaBlacklist(7174.3520, -2125.5850, 763.0121, 30)
GMR.DefineMeshAreaBlacklist(7202.5430, -2106.7280, 767.9936, 30)
GMR.DefineMeshAreaBlacklist(7247.3560, -2123.0880, 778.3644, 30)
GMR.DefineMeshAreaBlacklist(7309.4660, -2104.9090, 773.8260, 30)
GMR.DefineMeshAreaBlacklist(7333.7090, -2041.4540, 762.9391, 30)
GMR.DefineMeshAreaBlacklist(7293.8250, -2065.1880, 761.9807, 30)
GMR.DefineMeshAreaBlacklist(7287.3680, -2008.1560, 764.6655, 30)
GMR.DefineMeshAreaBlacklist(7247.3110, -1974.1390, 770.7268, 30)
GMR.DefineMeshAreaBlacklist(7210.3700, -1994.2820, 767.7876, 30)
GMR.DefineMeshAreaBlacklist(7195.7260, -2060.4720, 764.6873, 30)
GMR.DefineMeshAreaBlacklist(7202.9390, -2113.8920, 768.7082, 30)
GMR.DefineMeshAreaBlacklist(7251.2920, -2118.7020, 778.8754, 30)
GMR.DefineMeshAreaBlacklist(7310.3160, -2104.6930, 773.8583, 30)
GMR.DefineMeshAreaBlacklist(7238.6620, -2077.5390, 763.3433, 30)
GMR.DefineMeshAreaBlacklist(7583.5390, -1490.1670, 977.7716, 30)
GMR.DefineMeshAreaBlacklist(7635.0530, -1556.1210, 973.0212, 30)
GMR.DefineMeshAreaBlacklist(7703.0940, -1659.7730, 953.9519, 30)
GMR.DefineMeshAreaBlacklist(7747.1150, -1599.4560, 945.7670, 30)
GMR.DefineMeshAreaBlacklist(7825.3880, -1548.9910, 933.7877, 30)
GMR.DefineMeshAreaBlacklist(7897.4610, -1512.6010, 920.2105, 30)
GMR.DefineMeshAreaBlacklist(7228.2010, -1130.6070, 939.0313, 30)
GMR.DefineMeshAreaBlacklist(7170.7740, -1147.0950, 936.5327, 30)
GMR.DefineMeshAreaBlacklist(7142.9180, -1218.9850, 923.8766, 30)
GMR.DefineMeshAreaBlacklist(7095.0990, -1133.4020, 921.8155, 44)
GMR.DefineMeshAreaBlacklist(7035.0880, -1195.6100, 928.7196, 30)
GMR.DefineMeshAreaBlacklist(6984.3130, -1238.4340, 930.5374, 30)
GMR.DefineMeshAreaBlacklist(7033.9450, -1299.3500, 939.7466, 30)
GMR.DefineMeshAreaBlacklist(7041.7930, -1285.4910, 919.5781, 30)
GMR.DefineMeshAreaBlacklist(7124.3330, -1314.0210, 916.3643, 45)
GMR.DefineMeshAreaBlacklist(6985.3460, -1216.7740, 927.8894, 30)
GMR.DefineMeshAreaBlacklist(7112.9800, -1107.3820, 924.0865, 30)
GMR.DefineMeshAreaBlacklist(7140.1200, -1150.1910, 931.4960, 30)
GMR.DefineMeshAreaBlacklist(6506.1520, -1032.8920, 435.7122, 30)
GMR.DefineMeshAreaBlacklist(6567.7390, -1024.9770, 433.4644, 63)
GMR.DefineMeshAreaBlacklist(6677.0830, -1009.8670, 414.8397, 30)
GMR.DefineMeshAreaBlacklist(6647.6170, -1083.5200, 401.9590, 60)
GMR.DefineMeshAreaBlacklist(6635.5930, -1112.7940, 426.6802, 30)
GMR.DefineMeshAreaBlacklist(6685.8730, -1106.9430, 396.8469, 30)
GMR.DefineMeshAreaBlacklist(6660.7410, -1190.5330, 398.7615, 60)
GMR.DefineMeshAreaBlacklist(6680.7840, -1276.5800, 396.2646, 30)
GMR.DefineMeshAreaBlacklist(6731.5270, -1318.0480, 395.8602, 30)
GMR.DefineMeshAreaBlacklist(6685.2420, -1370.4150, 391.0064, 30)
GMR.DefineMeshAreaBlacklist(6699.5610, -1441.9990, 385.2171, 30)
GMR.DefineMeshAreaBlacklist(6728.5890, -1535.5280, 367.7092, 50)
GMR.DefineMeshAreaBlacklist(6894.3430, -1522.0430, 358.3529, 30)
GMR.DefineMeshAreaBlacklist(6817.8680, -1599.3550, 351.9640, 55)
GMR.DefineMeshAreaBlacklist(6728.8220, -1537.2530, 367.4264, 30)
GMR.DefineMeshAreaBlacklist(6701.5460, -1344.2680, 394.7059, 30)
GMR.DefineQuestEnemyId(32500)
GMR.DefineQuestEnemyId(35189)
GMR.DefineQuestEnemyId(32630)
GMR.DefineQuestEnemyId(32491)
GMR.DefineSellVendor(8383.4500, -390.1660, 903.1190, 29964)
GMR.DefineRepairVendor(8383.4500, -390.1660, 903.1190, 29964)
GMR.DefineAmmoVendor(8409.7900, -393.8690, 903.1189, 29962)
GMR.DefineGoodsVendor(8426.4100, -355.5840, 906.4021, 29963)
GMR.DefineProfileMailbox(8441.4080, -328.6534, 906.6083, 193972)
GMR.DefineVendorPath("Sell", 8321.9850, -146.4632, 838.7548)
GMR.DefineVendorPath("Sell", 8347.8010, -172.4623, 839.6221)
GMR.DefineVendorPath("Sell", 8377.7270, -182.2339, 831.7011)
GMR.DefineVendorPath("Sell", 8398.6860, -207.6445, 832.4556)
GMR.DefineVendorPath("Sell", 8421.7080, -226.4664, 831.3672)
GMR.DefineVendorPath("Sell", 8451.0890, -243.3136, 838.3688)
GMR.DefineVendorPath("Sell", 8474.4690, -263.7405, 842.1360)
GMR.DefineVendorPath("Sell", 8491.9950, -271.8022, 844.9027)
GMR.DefineVendorPath("Sell", 8473.4410, -279.2617, 854.3558)
GMR.DefineVendorPath("Sell", 8448.0970, -275.6898, 865.1257)
GMR.DefineVendorPath("Sell", 8425.4060, -271.4563, 875.4626)
GMR.DefineVendorPath("Sell", 8408.3500, -268.6902, 882.8370)
GMR.DefineVendorPath("Sell", 8390.5590, -269.9288, 885.3040)
GMR.DefineVendorPath("Sell", 8404.3980, -285.6296, 896.1627)
GMR.DefineVendorPath("Sell", 8425.0690, -301.6198, 905.2186)
GMR.DefineVendorPath("Sell", 8443.3960, -324.9996, 906.6083)
GMR.DefineVendorPath("Sell", 8442.7870, -336.7228, 906.6083)
GMR.DefineVendorPath("Sell", 8426.3180, -355.5426, 906.4023)
GMR.DefineVendorPath("Sell", 8410.0810, -373.6576, 903.5703)
GMR.DefineVendorPath("Sell", 8396.1000, -384.0745, 903.0033)
GMR.DefineVendorPath("Sell", 8383.4500, -390.1660, 903.1190)
GMR.DefineVendorPath("Repair", 8401.6360, -223.4002, 836.3298)
GMR.DefineVendorPath("Repair", 8425.6390, -235.9678, 836.0665)
GMR.DefineVendorPath("Repair", 8445.0500, -243.4258, 839.5514)
GMR.DefineVendorPath("Repair", 8465.9500, -251.9038, 837.8793)
GMR.DefineVendorPath("Repair", 8487.8030, -261.8965, 839.6171)
GMR.DefineVendorPath("Repair", 8486.3490, -278.3753, 849.8098)
GMR.DefineVendorPath("Repair", 8464.7690, -277.7582, 856.7622)
GMR.DefineVendorPath("Repair", 8443.4310, -274.5038, 867.3959)
GMR.DefineVendorPath("Repair", 8423.0730, -271.4348, 876.6315)
GMR.DefineVendorPath("Repair", 8403.6260, -267.9735, 884.4666)
GMR.DefineVendorPath("Repair", 8392.0270, -271.3465, 885.4426)
GMR.DefineVendorPath("Repair", 8402.3890, -283.8090, 894.3664)
GMR.DefineVendorPath("Repair", 8418.2450, -295.6371, 903.0416)
GMR.DefineVendorPath("Repair", 8436.3730, -310.0653, 906.1971)
GMR.DefineVendorPath("Repair", 8451.6050, -322.9362, 906.8499)
GMR.DefineVendorPath("Repair", 8443.8160, -335.7912, 906.6083)
GMR.DefineVendorPath("Repair", 8428.4020, -353.2852, 906.4022)
GMR.DefineVendorPath("Repair", 8413.0700, -369.0712, 905.5019)
GMR.DefineVendorPath("Repair", 8397.1330, -382.9527, 903.0042)
GMR.DefineVendorPath("Repair", 8383.4500, -390.1660, 903.1204)
GMR.DefineVendorPath("Goods", 8382.0640, -185.4152, 831.2933)
GMR.DefineVendorPath("Goods", 8397.1200, -205.6085, 832.6025)
GMR.DefineVendorPath("Goods", 8417.2420, -218.2080, 829.3217)
GMR.DefineVendorPath("Goods", 8437.1740, -229.8120, 829.8748)
GMR.DefineVendorPath("Goods", 8458.3140, -242.3827, 835.3203)
GMR.DefineVendorPath("Goods", 8479.1920, -256.4336, 835.3506)
GMR.DefineVendorPath("Goods", 8497.7670, -268.4667, 841.4008)
GMR.DefineVendorPath("Goods", 8493.1440, -279.7730, 848.7173)
GMR.DefineVendorPath("Goods", 8465.0570, -277.5034, 856.6355)
GMR.DefineVendorPath("Goods", 8438.7470, -273.7011, 869.7695)
GMR.DefineVendorPath("Goods", 8414.7200, -269.3181, 880.2919)
GMR.DefineVendorPath("Goods", 8391.7790, -268.8078, 885.2992)
GMR.DefineVendorPath("Goods", 8399.2690, -281.6437, 891.7500)
GMR.DefineVendorPath("Goods", 8418.4730, -296.2518, 903.2263)
GMR.DefineVendorPath("Goods", 8439.2640, -312.9861, 906.4109)
GMR.DefineVendorPath("Goods", 8446.3840, -330.6708, 906.6082)
GMR.DefineVendorPath("Goods", 8439.6240, -340.8784, 906.6082)
GMR.DefineVendorPath("Goods", 8426.4100, -355.5840, 906.4020)
GMR.DefineVendorPath("Ammo", 8379.7050, -192.9379, 834.6889)
GMR.DefineVendorPath("Ammo", 8400.1290, -206.7685, 831.8224)
GMR.DefineVendorPath("Ammo", 8423.4060, -219.2171, 828.6506)
GMR.DefineVendorPath("Ammo", 8451.2430, -233.7055, 832.6269)
GMR.DefineVendorPath("Ammo", 8471.5400, -249.9974, 835.3045)
GMR.DefineVendorPath("Ammo", 8486.0090, -269.0477, 845.0137)
GMR.DefineVendorPath("Ammo", 8472.8890, -278.6158, 854.3690)
GMR.DefineVendorPath("Ammo", 8446.1230, -274.5900, 866.0352)
GMR.DefineVendorPath("Ammo", 8421.8450, -271.4050, 877.2626)
GMR.DefineVendorPath("Ammo", 8402.6890, -267.3521, 884.5610)
GMR.DefineVendorPath("Ammo", 8392.3670, -270.0877, 885.2988)
GMR.DefineVendorPath("Ammo", 8401.1980, -282.8623, 893.2987)
GMR.DefineVendorPath("Ammo", 8418.2960, -297.3117, 903.4863)
GMR.DefineVendorPath("Ammo", 8439.1010, -312.0799, 906.3844)
GMR.DefineVendorPath("Ammo", 8445.5190, -332.9526, 906.6080)
GMR.DefineVendorPath("Ammo", 8434.1520, -347.1390, 906.6080)
GMR.DefineVendorPath("Ammo", 8417.2510, -365.5303, 906.4549)
GMR.DefineVendorPath("Ammo", 8408.4770, -383.6574, 903.0483)
GMR.DefineVendorPath("Ammo", 8409.7900, -393.8690, 903.1189)
GMR.DefineMailboxPath(8375.5490, -186.8937, 832.5340)
GMR.DefineMailboxPath(8392.4850, -201.4814, 833.0284)
GMR.DefineMailboxPath(8413.1880, -214.8545, 829.6998)
GMR.DefineMailboxPath(8437.7950, -227.7585, 828.7980)
GMR.DefineMailboxPath(8460.7960, -244.0806, 835.7833)
GMR.DefineMailboxPath(8480.0340, -258.3452, 836.7357)
GMR.DefineMailboxPath(8490.0380, -271.3682, 845.2632)
GMR.DefineMailboxPath(8476.5620, -278.7448, 853.2691)
GMR.DefineMailboxPath(8452.6850, -275.2734, 862.4200)
GMR.DefineMailboxPath(8432.2880, -272.6682, 872.6153)
GMR.DefineMailboxPath(8414.3540, -269.9633, 880.4308)
GMR.DefineMailboxPath(8391.4870, -268.7057, 885.3000)
GMR.DefineMailboxPath(8397.9680, -281.5177, 891.1547)
GMR.DefineMailboxPath(8417.5260, -296.0015, 903.0648)
GMR.DefineMailboxPath(8435.9160, -310.2255, 906.1747)
GMR.DefineMailboxPath(8446.9590, -319.1088, 906.8428)
GMR.DefineMailboxPath(8441.4080, -328.6534, 906.6083)