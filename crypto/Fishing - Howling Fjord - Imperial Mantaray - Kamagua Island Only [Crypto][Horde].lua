GMR.DefineProfileName("Fishing - Howling Fjord - Imperial Mantaray - Kamagua Island Only [Crypto][Horde]")
GMR.DefineProfileType("Fishing")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(891.3804, -2876.0830, 0.9932, 300)
GMR.DefineProfileCenter(878.3463, -2862.7200, 0.9921, 300)
GMR.DefineProfileCenter(733.7876, -2846.7990, 1.0892, 300)
GMR.DefineProfileCenter(703.0352, -2839.7830, 1.0892, 300)
GMR.DefineProfileCenter(660.7856, -2832.7190, 1.0892, 300)
GMR.DefineProfileCenter(875.6613, -3255.3580, 2.8887, 300)
GMR.DefineProfileCenter(842.3484, -3310.5030, 2.0853, 300)
GMR.DefineProfileCenter(266.8282, -3143.8170, 0.5961, 300)
GMR.DefineProfileCenter(234.8610, -3168.7390, 0.0948, 300)
GMR.DefineProfileCenter(275.4368, -3340.9740, 0.1364, 300)
GMR.DefineProfileCenter(336.9461, -3359.5890, 0.2852, 300)
GMR.DefineProfileCenter(502.2092, -3480.7100, 0.5987, 300)
GMR.DefineProfileCenter(594.7786, -3505.6550, 0.7653, 300)
GMR.DefineProfileCenter(125.4078, -3350.3390, 0.3291, 300)
GMR.DefineProfileCenter(99.2268, -3275.9540, -0.3645, 300)
GMR.DefineProfileCenter(15.9105, -3282.3600, 0.0024, 300)
GMR.DefineProfileCenter(-11.8212, -3315.1820, 0.1711, 300)
GMR.DefineProfileCenter(-229.8597, -3490.2680, 0.1952, 300)
GMR.DefineProfileCenter(-240.5647, -3506.1650, 1.0246, 300)
GMR.DefineProfileCenter(-237.2090, -3536.3360, 0.4402, 300)
GMR.DefineProfileCenter(-146.4765, -3588.7570, 0.6195, 300)
GMR.DefineProfileCenter(-170.8643, -3642.8760, 2.0116, 300)
GMR.DefineProfileCenter(-151.5967, -3648.3120, 0.3934, 300)
GMR.DefineProfileCenter(-29.4729, -3645.2100, 0.8589, 300)
GMR.DefineProfileCenter(-7.6440, -3637.2020, -0.5912, 300)
GMR.DefineQuestEnemyId(32398)
GMR.DefineQuestEnemyId(32386)
GMR.DefineQuestEnemyId(32377)
GMR.DefineSellVendor(801.2080, -2948.5700, 6.6619, 27151)
GMR.DefineRepairVendor(801.2080, -2948.5700, 6.6619, 27151)
GMR.DefineAmmoVendor(790.4500, -2901.3300, 7.1532, 27145)
GMR.DefineGoodsVendor(704.0290, -2932.8800, -3.0684, 27148)
GMR.DefineProfileMailbox(738.0306, -2921.1140, 7.1903, 189328)
GMR.DefineVendorPath("Sell", 727.0018, -3058.3480, 26.1660)
GMR.DefineVendorPath("Sell", 750.4119, -3031.4000, 23.8239)
GMR.DefineVendorPath("Sell", 778.0685, -3001.1850, 22.0988)
GMR.DefineVendorPath("Sell", 780.1513, -2968.5330, 10.1816)
GMR.DefineVendorPath("Sell", 780.2324, -2951.5030, 7.6844)
GMR.DefineVendorPath("Sell", 793.2127, -2943.4560, 7.2376)
GMR.DefineVendorPath("Sell", 799.1193, -2944.0670, 6.9270)
GMR.DefineVendorPath("Sell", 801.2080, -2948.5700, 6.6609)
GMR.DefineVendorPath("Repair", 739.7429, -3049.3130, 25.2023)
GMR.DefineVendorPath("Repair", 759.1530, -3026.8540, 23.9630)
GMR.DefineVendorPath("Repair", 777.6824, -2999.2470, 21.6971)
GMR.DefineVendorPath("Repair", 777.2733, -2968.0930, 10.0833)
GMR.DefineVendorPath("Repair", 781.9066, -2948.3450, 7.6348)
GMR.DefineVendorPath("Repair", 795.2293, -2939.8070, 7.1595)
GMR.DefineVendorPath("Repair", 801.2080, -2948.5700, 6.6619)
GMR.DefineVendorPath("Goods", 707.0715, -3075.5930, 26.8124)
GMR.DefineVendorPath("Goods", 739.8367, -3044.5710, 25.0235)
GMR.DefineVendorPath("Goods", 769.5427, -3007.3300, 23.4866)
GMR.DefineVendorPath("Goods", 778.1049, -2966.0600, 9.6319)
GMR.DefineVendorPath("Goods", 762.4965, -2938.7860, 7.2524)
GMR.DefineVendorPath("Goods", 743.9208, -2926.3610, 7.1619)
GMR.DefineVendorPath("Goods", 735.3371, -2927.6940, 7.1675)
GMR.DefineVendorPath("Goods", 726.2051, -2929.3170, 4.2745)
GMR.DefineVendorPath("Goods", 715.6595, -2931.2620, -2.0167)
GMR.DefineVendorPath("Goods", 710.2795, -2932.1680, -3.0686)
GMR.DefineVendorPath("Goods", 704.0290, -2932.8800, -3.0684)
GMR.DefineVendorPath("Ammo", 708.7606, -3072.0150, 26.7607)
GMR.DefineVendorPath("Ammo", 746.0539, -3038.9000, 24.1733)
GMR.DefineVendorPath("Ammo", 774.3484, -3006.7420, 23.0497)
GMR.DefineVendorPath("Ammo", 778.3042, -2970.4950, 10.6881)
GMR.DefineVendorPath("Ammo", 782.2216, -2939.1130, 7.2946)
GMR.DefineVendorPath("Ammo", 785.4589, -2914.9350, 7.1622)
GMR.DefineVendorPath("Ammo", 790.4500, -2901.3300, 7.1519)
GMR.DefineMailboxPath(709.0513, -3071.3100, 26.7609)
GMR.DefineMailboxPath(739.1938, -3045.5060, 25.1303)
GMR.DefineMailboxPath(767.4360, -3014.4200, 23.5755)
GMR.DefineMailboxPath(782.2574, -2984.1520, 15.5921)
GMR.DefineMailboxPath(773.8539, -2955.3620, 8.2139)
GMR.DefineMailboxPath(757.5815, -2934.8240, 7.1672)
GMR.DefineMailboxPath(747.7509, -2920.6480, 7.1626)
GMR.DefineMailboxPath(738.0306, -2921.1140, 7.1903)