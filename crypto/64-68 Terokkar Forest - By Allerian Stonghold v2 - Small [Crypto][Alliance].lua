GMR.DefineProfileName("64-68 Terokkar Forest - By Allerian Stonghold v2 - Small [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Outland")
GMR.DefineProfileCenter(-2771.049, 3854.49, -11.35501, 80)
GMR.DefineProfileCenter(-2815.557, 3773.216, 0.5834111, 80)
GMR.DefineProfileCenter(-2714.803, 3738.345, -0.7928813, 80)
GMR.DefineProfileCenter(-2685.386, 3570.435, -2.988367, 80)
GMR.DefineProfileCenter(-2557.224, 3470.746, -1.488072, 80)
GMR.DefineProfileCenter(-2427.9, 3409.018, -19.78393, 80)
GMR.DefineProfileCenter(-2318.587, 3594.467, -7.854971, 80)
GMR.DefineProfileCenter(-2204.848, 3694.051, -24.28693, 80)
GMR.DefineProfileCenter(-2244.92, 3817.436, 1.737215, 80)
GMR.DefineProfileCenter(-2448.445, 3801.975, 0.32543, 80)
GMR.DefineProfileCenter(-2506.787, 3907.183, 5.516074, 80)
GMR.DefineProfileCenter(-2480.784, 4006.896, -0.6518404, 80)
GMR.DefineProfileCenter(-2372.814, 4050.143, -9.049672, 80)
GMR.DefineProfileCenter(-2314.105, 4120.898, -17.75971, 80)
GMR.DefineProfileCenter(-2254.086, 4037.498, -26.7936, 80)
GMR.DefineProfileCenter(-2141.977, 3919.544, 0.5098275, 80)
GMR.DefineProfileCenter(-2166.072, 3842.54, -1.480403, 80)
GMR.DefineProfileCenter(-2421.282, 3906.438, 7.601208, 80)
GMR.DefineProfileCenter(-2639.443, 3869.645, -16.86149, 80)
GMR.DefineProfileCenter(-2756.085, 3764.875, -0.002831154, 80)
GMR.DefineProfileCenter(-2603.368, 3723.924, -0.6596406, 80)
GMR.DefineAreaBlacklist(-2628.695, 4412.642, 34.71798, 80)
GMR.DefineAreaBlacklist(-2909.116, 3511.988, -24.50016, 80)
GMR.DefineAreaBlacklist(-2986.972, 3541.644, -3.837799, 80)
GMR.DefineAreaBlacklist(-2907.203, 3651.01, -14.18148, 30)
GMR.DefineAreaBlacklist(-2353.069, 3241.824, -3.375561, 80)
GMR.DefineAreaBlacklist(-2354.785, 3302.63, -0.9883045, 50)
GMR.DefineAreaBlacklist(-2053.458, 3812.553, 2.111515, 30)
GMR.DefineQuestEnemyId(18477)
GMR.DefineQuestEnemyId(18464)
GMR.DefineQuestEnemyId(18469)
GMR.DefineQuestEnemyId(18563)
GMR.DefineQuestEnemyId(18463)
GMR.BlacklistId(18595)
GMR.BlacklistId(16772)
GMR.BlacklistId(16810)
GMR.BlacklistId(5355)
GMR.BlacklistId(16769)
GMR.BlacklistId(1410)
GMR.BlacklistId(18648)
GMR.BlacklistId(18449)
GMR.BlacklistId(18450)
GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)
GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)
GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)
GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)
GMR.DefineProfileMailbox(-2930.272, 4008.43, -1.359393, 182939)
