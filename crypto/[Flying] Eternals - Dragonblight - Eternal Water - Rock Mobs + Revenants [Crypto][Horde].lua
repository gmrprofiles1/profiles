GMR.DefineProfileName("[Flying] Eternals - Dragonblight - Eternal Water - Rock Mobs + Revenants [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(4880.5840, 927.9002, 246.6455, 80)
GMR.DefineProfileCenter(4911.3240, 775.3119, 232.2253, 80)
GMR.DefineProfileCenter(4834.2740, 649.9263, 217.2453, 80)
GMR.DefineProfileCenter(4867.2180, 553.7869, 216.4903, 80)
GMR.DefineProfileCenter(4942.2420, 388.4932, 241.5767, 80)
GMR.DefineProfileCenter(3662.1160, -163.5276, 91.4445, 80)
GMR.DefineProfileCenter(3508.3440, -181.3316, 87.0006, 80)
GMR.DefineProfileCenter(3531.2280, -110.9521, 80.8284, 80)
GMR.DefineProfileCenter(3639.8140, -144.5663, 86.7573, 80)
GMR.DefineQuestEnemyId(26316)
GMR.DefineQuestEnemyId(26283)
GMR.BlacklistId(26291)
GMR.DefineSellVendor(3845.4900, 1572.7200, 86.6987, 27019)
GMR.DefineRepairVendor(3845.4900, 1572.7200, 86.6987, 27019)
GMR.DefineAmmoVendor(3818.2000, 1594.3300, 86.6987, 27022)
GMR.DefineGoodsVendor(3814.9900, 1585.3600, 86.6991, 26567)
GMR.DefineProfileMailbox(3828.0530, 1534.5670, 89.7255, 188355)
GMR.DefineVendorPath("Sell", 3748.5590, 1520.8420, 89.1970)
GMR.DefineVendorPath("Sell", 3782.5580, 1534.4190, 87.0815)
GMR.DefineVendorPath("Sell", 3834.8480, 1545.0850, 89.7248)
GMR.DefineVendorPath("Sell", 3834.9670, 1570.2640, 86.6984)
GMR.DefineVendorPath("Sell", 3845.4900, 1572.7200, 86.6986)
GMR.DefineVendorPath("Sell", 3845.4900, 1572.7200, 86.6986)
GMR.DefineVendorPath("Repair", 3741.5640, 1513.7900, 90.6969)
GMR.DefineVendorPath("Repair", 3774.6770, 1532.0190, 87.0539)
GMR.DefineVendorPath("Repair", 3807.3900, 1538.7330, 89.7252)
GMR.DefineVendorPath("Repair", 3833.2410, 1544.0390, 89.7252)
GMR.DefineVendorPath("Repair", 3835.0360, 1551.0940, 89.5134)
GMR.DefineVendorPath("Repair", 3838.5310, 1572.1530, 86.6988)
GMR.DefineVendorPath("Repair", 3845.4900, 1572.7200, 86.6987)
GMR.DefineVendorPath("Goods", 3736.1250, 1512.6190, 91.3033)
GMR.DefineVendorPath("Goods", 3771.7430, 1530.8890, 86.9645)
GMR.DefineVendorPath("Goods", 3809.9550, 1537.1390, 89.7233)
GMR.DefineVendorPath("Goods", 3832.2860, 1543.7170, 89.7233)
GMR.DefineVendorPath("Goods", 3835.5580, 1549.4730, 89.6740)
GMR.DefineVendorPath("Goods", 3832.7250, 1568.2290, 86.6985)
GMR.DefineVendorPath("Goods", 3823.2240, 1575.2720, 86.6985)
GMR.DefineVendorPath("Goods", 3814.9900, 1585.3600, 86.6989)
GMR.DefineVendorPath("Ammo", 3730.6300, 1510.9810, 91.8763)
GMR.DefineVendorPath("Ammo", 3770.1770, 1529.6660, 86.9223)
GMR.DefineVendorPath("Ammo", 3805.2890, 1536.5230, 89.7245)
GMR.DefineVendorPath("Ammo", 3834.4410, 1553.9930, 89.2595)
GMR.DefineVendorPath("Ammo", 3831.6650, 1568.3710, 86.6989)
GMR.DefineVendorPath("Ammo", 3820.8740, 1583.6470, 86.6989)
GMR.DefineVendorPath("Ammo", 3818.2000, 1594.3300, 86.6987)
GMR.DefineMailboxPath(3740.2080, 1515.5620, 90.6327)
GMR.DefineMailboxPath(3772.5310, 1530.9490, 86.9810)
GMR.DefineMailboxPath(3795.7490, 1535.9430, 88.2192)
GMR.DefineMailboxPath(3819.7950, 1541.2950, 89.7257)
GMR.DefineMailboxPath(3828.0700, 1534.5710, 89.7257)