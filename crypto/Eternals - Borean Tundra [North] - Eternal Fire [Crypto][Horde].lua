GMR.DefineProfileName("Eternals - Borean Tundra (North) - Eternal Fire [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(4245.9810, 5748.7520, 65.7252, 60)
GMR.DefineProfileCenter(4313.7540, 5872.8080, 57.4439, 60)
GMR.DefineProfileCenter(4351.6840, 5748.4830, 82.1169, 60)
GMR.DefineProfileCenter(4417.4230, 5797.7850, 75.8602, 60)
GMR.DefineAreaBlacklist(4539.1250, 5887.2630, 62.3881, 25)
GMR.DefineAreaBlacklist(4383.9490, 5923.6620, 55.6825, 20)
GMR.DefineAreaBlacklist(4309.6900, 5633.3700, 94.7915, 20)
GMR.DefineMeshAreaBlacklist(4539.1250, 5887.2630, 62.3881, 20)
GMR.DefineMeshAreaBlacklist(4383.9490, 5923.6620, 55.6825, 21)
GMR.DefineMeshAreaBlacklist(4309.6900, 5633.3700, 94.7915, 25)
GMR.DefineQuestEnemyId(25417)
GMR.BlacklistId(25376)
GMR.BlacklistId(25418)
GMR.BlacklistId(25416)
GMR.DefineSellVendor(4510.5200, 5703.3500, 81.5398, 27067)
GMR.DefineRepairVendor(4510.5200, 5703.3500, 81.5398, 27067)
GMR.DefineAmmoVendor(4502.2400, 5757.5600, 81.5365, 27058)
GMR.DefineGoodsVendor(4506.3600, 5707.3200, 81.5200, 27069)
GMR.DefineProfileMailbox(4502.1520, 5717.2430, 81.4823, 189329)
GMR.DefineVendorPath("Sell", 4487.8840, 5737.7190, 80.0330)
GMR.DefineVendorPath("Sell", 4504.1900, 5719.8770, 81.4935)
GMR.DefineVendorPath("Sell", 4509.8530, 5712.8440, 81.5446)
GMR.DefineVendorPath("Sell", 4510.5210, 5703.4230, 81.5404)
GMR.DefineVendorPath("Repair", 4487.8840, 5737.7190, 80.0330)
GMR.DefineVendorPath("Repair", 4504.1900, 5719.8770, 81.4935)
GMR.DefineVendorPath("Repair", 4509.8530, 5712.8440, 81.5446)
GMR.DefineVendorPath("Repair", 4510.5210, 5703.4230, 81.5404)
GMR.DefineVendorPath("Goods", 4493.3900, 5730.7190, 80.4479)
GMR.DefineVendorPath("Goods", 4500.2420, 5723.6500, 81.2609)
GMR.DefineVendorPath("Goods", 4507.7220, 5715.2690, 81.5265)
GMR.DefineVendorPath("Goods", 4509.0250, 5709.7340, 81.5327)
GMR.DefineVendorPath("Goods", 4506.3600, 5707.3200, 81.5200)