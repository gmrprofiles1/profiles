<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=0.75, maximum-scale=1.0, user-scalable=yes" />
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-S72LBY47R8"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', "G-S72LBY47R8");
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pastebin.com - Not Found (#404)</title>
    <link rel="shortcut icon" href="/favicon.ico" />
    <meta name="description" content="Pastebin.com is the number one paste tool since 2002. Pastebin is a website where you can store text online for a set period of time." />
    <meta property="og:description" content="Pastebin.com is the number one paste tool since 2002. Pastebin is a website where you can store text online for a set period of time." />
            <meta property="fb:app_id" content="231493360234820" />
    <meta property="og:title" content="Pastebin.com - Not Found (#404)" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://pastebin.com/raw/04TNWgeN%20" />
    <meta property="og:image" content="https://pastebin.com/i/facebook.png" />
    <meta property="og:site_name" content="Pastebin" />
    <meta name="google-site-verification" content="jkUAIOE8owUXu8UXIhRLB9oHJsWBfOgJbZzncqHoF4A" />
    <link rel="canonical" href="https://pastebin.com/raw/04TNWgeN%20" />
        <meta name="csrf-param" content="_csrf-frontend">
<meta name="csrf-token" content="UY1rOXZWWqd-X5cLIlSP665Rg-Oce555jBnDZOt6RDBk_iFsHxI3_TId-kd1MPba6gfUge8ozzb9XptduCICew==">

<link href="/assets/c80611c4/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/a25015e7/dist/bootstrap-tagsinput.css" rel="stylesheet">        
<link href="/themes/pastebin/css/vendors.bundle.css?3bd1f2bdbc3844dd5aad" rel="stylesheet">
<link href="/themes/pastebin/css/app.bundle.css?3bd1f2bdbc3844dd5aad" rel="stylesheet">
    </head>
<body class="night-auto " data-pr="x2xy94pJ" data-pa="" data-sar="1" data-abd="1">


<div class="wrap">

        
        
<div class="header">
    <div class="container">
        <div class="header__container">

                        <div class="header__left">
                <a class="header__logo" href="/">
                    Pastebin                </a>

                <div class="header__links h_1024">
                    
                                        <a href="/doc_api">API</a>
                    <a href="/tools">tools</a>
                    <a href="/faq">faq</a>
                                    </div>

                <a class="header__btn" href="/">
                    paste                </a>

                
                <div class="header__search">
                                            <form id="w0" class="search_form" action="https://pastebin.com/search" method="get">
                            
<input type="text" id="q" class="search_input" name="q" maxlength="128" placeholder="Search...">



                            <button type="submit" class="search_btn"></button>
                        </form>                                    </div>

            </div>

                        <div class="header__right">

                                    <div class="header_sign">
                        <a href="/login" class="btn-sign sign-in">Login</a>
                        <a href="/signup" class="btn-sign sign-up">Sign up</a>
                    </div>
                
            </div>

        </div>
    </div>

</div>
        

    <div class="container">
        <div class="content">

                        
                        
                                    
            
            
<div class="page -top -right">

    <div class="content__title">Not Found (#404)</div>
    <div class="content__text">
        <div class="notice -no-margin">
            Page not found.        </div>
    </div>

</div>
            <div style="clear: both;"></div>

                                </div>

        <div class="sidebar h_1024">
            



                
    <div class="sidebar__title">
        <a href="/archive">Public Pastes</a>
    </div>
    <ul class="sidebar__menu">

                    <li>
                <a href="/j6kZdKeD">ML - Lab 6 - PCA</a>
                <div class="details">
                                            Python |
                    
                    1 min ago
                    | 4.00 KB                </div>
            </li>
                    <li>
                <a href="/50KxiAFu">Untitled</a>
                <div class="details">
                                            Java |
                    
                    13 min ago
                    | 0.20 KB                </div>
            </li>
                    <li>
                <a href="/BhqBqb2P">Untitled</a>
                <div class="details">
                                            C# |
                    
                    40 min ago
                    | 1.42 KB                </div>
            </li>
                    <li>
                <a href="/NZejzVTL">Untitled</a>
                <div class="details">
                                            JavaScript |
                    
                    40 min ago
                    | 7.76 KB                </div>
            </li>
                    <li>
                <a href="/8bzDSDZ6">ENG</a>
                <div class="details">
                                            HTML |
                    
                    43 min ago
                    | 1.99 KB                </div>
            </li>
                    <li>
                <a href="/Tva07T1J">Untitled</a>
                <div class="details">
                                            Java |
                    
                    57 min ago
                    | 1.13 KB                </div>
            </li>
                    <li>
                <a href="/GzUC4tb4">Untitled</a>
                <div class="details">
                                            C++ |
                    
                    1 hour ago
                    | 0.02 KB                </div>
            </li>
                    <li>
                <a href="/iPLW94pv">邀您一起看：人鱼陷落</a>
                <div class="details">
                                            JavaScript |
                    
                    1 hour ago
                    | 43.24 KB                </div>
            </li>
        
    </ul>
            

    <div class="sidebar__sticky -on">
                    </div>
        </div>
    </div>
</div>


    
<div class="top-footer">
    <a class="icon-link -size-24-24 -chrome" href="/tools#chrome" title="Google Chrome Extension"></a>
    <a class="icon-link -size-24-24 -firefox" href="/tools#firefox" title="Firefox Extension"></a>
    <a class="icon-link -size-24-24 -iphone" href="/tools#iphone" title="iPhone/iPad Application"></a>
    <a class="icon-link -size-24-24 -windows" href="/tools#windows" title="Windows Desktop Application"></a>
    <a class="icon-link -size-24-24 -android" href="/tools#android" title="Android Application"></a>
    <a class="icon-link -size-24-24 -macos" href="/tools#macos" title="MacOS X Widget"></a>
    <a class="icon-link -size-24-24 -opera" href="/tools#opera" title="Opera Extension"></a>
    <a class="icon-link -size-24-24 -unix" href="/tools#pastebincl" title="Linux Application"></a>
</div>

<footer class="footer">
    <div class="container">
        <div class="footer__container">

            <div class="footer__left">
                <a href="/">create new paste</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                                <a href="/languages">syntax languages</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/archive">archive</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/faq">faq</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/tools">tools</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/night_mode">night mode</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/doc_api">api</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/doc_scraping_api">scraping api</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/news">news</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/pro" class="pro">pro</a>

                <br>
                <a href="/doc_privacy_statement">privacy statement</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/doc_cookies_policy">cookies policy</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/doc_terms_of_service">terms of service</a><sup style="color:#999">updated</sup> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/doc_security_disclosure">security disclosure</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/dmca">dmca</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/report-abuse">report abuse</a> <span class="footer__devider">&nbsp;/&nbsp;</span>
                <a href="/contact">contact</a>

                <br>

                                
                <br>

                
<span class="footer__bottom h_800">
    By using Pastebin.com you agree to our <a href="/doc_cookies_policy">cookies policy</a> to enhance your experience.
    <br>
    Site design &amp; logo &copy; 2022 Pastebin</span>
            </div>

            <div class="footer__right h_1024">
                                    <a class="icon-link -size-40-40 -facebook-circle" href="https://facebook.com/pastebin" rel="nofollow" title="Like us on Facebook" target="_blank"></a>
                    <a class="icon-link -size-40-40 -twitter-circle" href="https://twitter.com/pastebin" rel="nofollow" title="Follow us on Twitter" target="_blank"></a>
                            </div>

        </div>
    </div>
</footer>
    


    
<div class="popup-container">

                <div class="popup-box -cookies" data-name="l2c_1">
            We use cookies for various purposes including analytics. By continuing to use Pastebin, you agree to our use of cookies as described in the <a href="/doc_cookies_policy">Cookies Policy</a>.            &nbsp;<span class="cookie-button js-close-cookies">OK, I Understand</span>
        </div>
    
                <div class="popup-box -pro" data-name="l2c_2_pg">
            <div class="pro-promo-img">
                <a href="/signup">
                    <img src="/themes/pastebin/img/hello.png" alt=""/>
                </a>
            </div>
            <div class="pro-promo-text">
                Not a member of Pastebin yet?<br/>
                <a href="/signup"><b>Sign Up</b></a>, it unlocks many cool features!            </div>
            <div class="close js-close-pro-guest" title="Close Me">&nbsp;</div>
        </div>
    
    
    
</div>
    

<span class="cd-top"></span>

<script src="/assets/9ce1885/jquery.min.js"></script>
<script src="/assets/f04f76b8/yii.js"></script>
<script src="/assets/a25015e7/dist/bootstrap-tagsinput.js"></script>
<script>
    const POST_EXPIRATION_NEVER = 'N';
    const POST_EXPIRATION_BURN = 'B';
    const POST_STATUS_PUBLIC = '0';
    const POST_STATUS_UNLISTED = '1';
</script>
<script src="/themes/pastebin/js/vendors.bundle.js?3bd1f2bdbc3844dd5aad"></script>
<script src="/themes/pastebin/js/app.bundle.js?3bd1f2bdbc3844dd5aad"></script>

</body>
</html>
