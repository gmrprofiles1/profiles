GMR.DefineProfileName("75-80 Zul Drak - Spiders + Bats v4 (MaxEXP) [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(6105.0150, -2331.5790, 290.3641, 300)
GMR.DefineProfileCenter(6025.8570, -2425.2130, 289.5766, 300)
GMR.DefineProfileCenter(5912.9000, -2438.4950, 292.3786, 300)
GMR.DefineProfileCenter(5818.6500, -2366.5860, 291.6740, 300)
GMR.DefineProfileCenter(5833.6110, -2507.4600, 293.3651, 300)
GMR.DefineProfileCenter(5895.3650, -2568.1170, 292.6001, 300)
GMR.DefineProfileCenter(5943.8160, -2661.2830, 291.1166, 300)
GMR.DefineProfileCenter(5996.9170, -2754.2920, 290.9411, 300)
GMR.DefineProfileCenter(6052.4690, -2806.7440, 291.1913, 300)
GMR.DefineProfileCenter(6166.1470, -2841.4560, 296.9335, 300)
GMR.DefineProfileCenter(6291.2940, -2860.7420, 293.2216, 300)
GMR.DefineProfileCenter(6377.3610, -2831.4930, 289.7916, 300)
GMR.DefineProfileCenter(6458.8090, -2852.9220, 292.7126, 300)
GMR.DefineProfileCenter(6287.3710, -2874.1860, 297.1609, 300)
GMR.DefineProfileCenter(6064.9740, -2885.5070, 296.6671, 300)
GMR.DefineProfileCenter(5904.9650, -2540.5060, 293.5762, 300)
GMR.DefineQuestEnemyId(28221)
GMR.DefineQuestEnemyId(28233)
GMR.BlacklistId(29129)
GMR.BlacklistId(29133)
GMR.DefineSellVendor(5499.7700, -2651.8700, 303.9554, 28800)
GMR.DefineRepairVendor(5499.7700, -2651.8700, 303.9554, 28800)
GMR.DefineAmmoVendor(5499.7700, -2651.8700, 303.9554, 28800)
GMR.DefineGoodsVendor(5462.9700, -2639.0500, 306.5510, 28791)
GMR.DefineProfileMailbox(5483.9750, -2644.6910, 306.9655, 193045)
GMR.DefineVendorPath("Sell", 6011.0870, -2618.5430, 291.5695)
GMR.DefineVendorPath("Sell", 5987.4250, -2618.3860, 292.2716)
GMR.DefineVendorPath("Sell", 5958.0940, -2617.9310, 292.4190)
GMR.DefineVendorPath("Sell", 5922.6300, -2617.8480, 292.4114)
GMR.DefineVendorPath("Sell", 5892.8800, -2617.7770, 292.4179)
GMR.DefineVendorPath("Sell", 5863.6900, -2617.7080, 292.4179)
GMR.DefineVendorPath("Sell", 5833.9400, -2617.6380, 292.4179)
GMR.DefineVendorPath("Sell", 5797.2610, -2617.5510, 292.4179)
GMR.DefineVendorPath("Sell", 5771.0190, -2618.2110, 292.4179)
GMR.DefineVendorPath("Sell", 5739.1250, -2619.0120, 292.4195)
GMR.DefineVendorPath("Sell", 5705.3330, -2618.9790, 292.4129)
GMR.DefineVendorPath("Sell", 5666.2310, -2618.3780, 292.4172)
GMR.DefineVendorPath("Sell", 5628.0850, -2617.8380, 292.4187)
GMR.DefineVendorPath("Sell", 5591.8990, -2617.3260, 292.4186)
GMR.DefineVendorPath("Sell", 5563.2720, -2616.9210, 292.4186)
GMR.DefineVendorPath("Sell", 5548.9600, -2616.8250, 297.9510)
GMR.DefineVendorPath("Sell", 5535.6670, -2617.0200, 303.8464)
GMR.DefineVendorPath("Sell", 5523.0950, -2625.7910, 303.9553)
GMR.DefineVendorPath("Sell", 5510.7960, -2639.1820, 303.9553)
GMR.DefineVendorPath("Sell", 5505.0300, -2646.4020, 303.9553)
GMR.DefineVendorPath("Sell", 5499.7700, -2651.8700, 303.9545)
GMR.DefineVendorPath("Repair", 6012.6140, -2619.2630, 291.6148)
GMR.DefineVendorPath("Repair", 5994.0690, -2618.4170, 290.5267)
GMR.DefineVendorPath("Repair", 5968.5330, -2617.6260, 292.3931)
GMR.DefineVendorPath("Repair", 5944.3980, -2618.0240, 292.3506)
GMR.DefineVendorPath("Repair", 5922.3560, -2618.3870, 292.4130)
GMR.DefineVendorPath("Repair", 5894.8950, -2618.7290, 292.4183)
GMR.DefineVendorPath("Repair", 5868.8340, -2618.9540, 292.4183)
GMR.DefineVendorPath("Repair", 5844.2650, -2619.1200, 292.4183)
GMR.DefineVendorPath("Repair", 5822.2160, -2619.1730, 292.4183)
GMR.DefineVendorPath("Repair", 5798.9480, -2619.1000, 292.4183)
GMR.DefineVendorPath("Repair", 5771.6690, -2619.0140, 292.4183)
GMR.DefineVendorPath("Repair", 5744.7960, -2618.4710, 292.4259)
GMR.DefineVendorPath("Repair", 5716.0000, -2617.8960, 292.4135)
GMR.DefineVendorPath("Repair", 5688.7980, -2617.2160, 292.4171)
GMR.DefineVendorPath("Repair", 5661.6170, -2617.8040, 292.4171)
GMR.DefineVendorPath("Repair", 5634.7020, -2617.9400, 292.2136)
GMR.DefineVendorPath("Repair", 5613.0310, -2617.7480, 292.1634)
GMR.DefineVendorPath("Repair", 5588.8610, -2617.4820, 292.4186)
GMR.DefineVendorPath("Repair", 5567.0460, -2617.5980, 292.4186)
GMR.DefineVendorPath("Repair", 5547.8390, -2618.7500, 298.5632)
GMR.DefineVendorPath("Repair", 5535.4420, -2621.3900, 303.7344)
GMR.DefineVendorPath("Repair", 5518.4940, -2630.5690, 303.9527)
GMR.DefineVendorPath("Repair", 5508.0630, -2642.4400, 303.9527)
GMR.DefineVendorPath("Repair", 5499.7700, -2651.8700, 303.9542)
GMR.DefineVendorPath("Goods", 6010.7510, -2622.5060, 291.2607)
GMR.DefineVendorPath("Goods", 5987.0770, -2620.1110, 292.2149)
GMR.DefineVendorPath("Goods", 5962.1240, -2619.4110, 292.4178)
GMR.DefineVendorPath("Goods", 5939.9320, -2619.0510, 292.3366)
GMR.DefineVendorPath("Goods", 5913.5320, -2618.7710, 292.4173)
GMR.DefineVendorPath("Goods", 5888.8580, -2618.6290, 292.4183)
GMR.DefineVendorPath("Goods", 5868.2780, -2618.5640, 292.4183)
GMR.DefineVendorPath("Goods", 5842.7640, -2619.1830, 292.4183)
GMR.DefineVendorPath("Goods", 5814.6100, -2618.9970, 292.4183)
GMR.DefineVendorPath("Goods", 5786.4360, -2618.6990, 292.4183)
GMR.DefineVendorPath("Goods", 5762.3580, -2618.1150, 292.3180)
GMR.DefineVendorPath("Goods", 5736.8220, -2618.2180, 292.4194)
GMR.DefineVendorPath("Goods", 5708.7510, -2617.7840, 292.4167)
GMR.DefineVendorPath("Goods", 5685.3610, -2617.3440, 292.4174)
GMR.DefineVendorPath("Goods", 5658.6120, -2617.3500, 292.4174)
GMR.DefineVendorPath("Goods", 5632.3660, -2617.7600, 292.4133)
GMR.DefineVendorPath("Goods", 5607.5240, -2617.6780, 292.4186)
GMR.DefineVendorPath("Goods", 5582.8720, -2617.4070, 292.4186)
GMR.DefineVendorPath("Goods", 5561.1800, -2617.4410, 292.3862)
GMR.DefineVendorPath("Goods", 5548.0550, -2617.5550, 298.4492)
GMR.DefineVendorPath("Goods", 5537.0220, -2621.2840, 303.9680)
GMR.DefineVendorPath("Goods", 5524.3900, -2625.5540, 303.9524)
GMR.DefineVendorPath("Goods", 5508.4200, -2629.2400, 303.9524)
GMR.DefineVendorPath("Goods", 5491.8120, -2632.8160, 305.7886)
GMR.DefineVendorPath("Goods", 5477.3500, -2635.2890, 306.9283)
GMR.DefineVendorPath("Goods", 5462.9700, -2639.0500, 306.5509)
GMR.DefineVendorPath("Ammo", 6014.8630, -2618.3770, 291.8065)
GMR.DefineVendorPath("Ammo", 5996.2080, -2617.7040, 290.5258)
GMR.DefineVendorPath("Ammo", 5970.4450, -2617.5710, 292.3782)
GMR.DefineVendorPath("Ammo", 5949.4690, -2617.6910, 292.4156)
GMR.DefineVendorPath("Ammo", 5924.4440, -2617.6650, 292.4088)
GMR.DefineVendorPath("Ammo", 5899.9480, -2618.3770, 292.4179)
GMR.DefineVendorPath("Ammo", 5878.0900, -2618.4150, 292.4179)
GMR.DefineVendorPath("Ammo", 5855.6630, -2618.5390, 292.4179)
GMR.DefineVendorPath("Ammo", 5831.3480, -2618.8350, 292.4179)
GMR.DefineVendorPath("Ammo", 5808.5470, -2619.2210, 292.4179)
GMR.DefineVendorPath("Ammo", 5785.6370, -2619.0880, 292.4179)
GMR.DefineVendorPath("Ammo", 5762.3940, -2618.6690, 292.2880)
GMR.DefineVendorPath("Ammo", 5740.7460, -2618.3050, 292.4194)
GMR.DefineVendorPath("Ammo", 5717.4170, -2618.0670, 292.4126)
GMR.DefineVendorPath("Ammo", 5697.7540, -2617.6890, 292.4172)
GMR.DefineVendorPath("Ammo", 5675.2270, -2617.1760, 292.4172)
GMR.DefineVendorPath("Ammo", 5658.6830, -2616.8260, 292.4172)
GMR.DefineVendorPath("Ammo", 5637.5410, -2616.8760, 292.4172)
GMR.DefineVendorPath("Ammo", 5617.2720, -2617.3330, 292.4187)
GMR.DefineVendorPath("Ammo", 5597.8560, -2616.7380, 292.4188)
GMR.DefineVendorPath("Ammo", 5575.6390, -2616.7180, 292.4188)
GMR.DefineVendorPath("Ammo", 5560.6240, -2616.7300, 292.3099)
GMR.DefineVendorPath("Ammo", 5549.0630, -2616.7390, 297.8946)
GMR.DefineVendorPath("Ammo", 5535.6860, -2616.7500, 303.8566)
GMR.DefineVendorPath("Ammo", 5525.1910, -2620.3830, 303.9541)
GMR.DefineVendorPath("Ammo", 5516.8650, -2632.0690, 303.9541)
GMR.DefineVendorPath("Ammo", 5507.9120, -2643.4090, 303.9541)
GMR.DefineVendorPath("Ammo", 5499.7700, -2651.8700, 303.9546)
GMR.DefineMailboxPath(6006.8990, -2618.0620, 290.6062)
GMR.DefineMailboxPath(5990.7430, -2617.2750, 291.2048)
GMR.DefineMailboxPath(5972.0400, -2617.1590, 292.3724)
GMR.DefineMailboxPath(5954.5590, -2617.2760, 292.4171)
GMR.DefineMailboxPath(5933.5390, -2617.3610, 292.3930)
GMR.DefineMailboxPath(5915.9010, -2618.1010, 292.4170)
GMR.DefineMailboxPath(5897.0370, -2618.4200, 292.4182)
GMR.DefineMailboxPath(5879.0650, -2618.8110, 292.4182)
GMR.DefineMailboxPath(5862.0990, -2619.0380, 292.4182)
GMR.DefineMailboxPath(5843.2220, -2619.2900, 292.4182)
GMR.DefineMailboxPath(5821.7200, -2619.5780, 292.4182)
GMR.DefineMailboxPath(5800.2500, -2619.2020, 292.4182)
GMR.DefineMailboxPath(5777.3460, -2618.6580, 292.4182)
GMR.DefineMailboxPath(5756.5390, -2618.2500, 291.6852)
GMR.DefineMailboxPath(5737.4540, -2617.8750, 292.4188)
GMR.DefineMailboxPath(5713.9380, -2617.4200, 292.4155)
GMR.DefineMailboxPath(5690.3160, -2617.0490, 292.4173)
GMR.DefineMailboxPath(5669.4840, -2616.8860, 292.4173)
GMR.DefineMailboxPath(5651.6350, -2616.7460, 292.4173)
GMR.DefineMailboxPath(5631.4550, -2616.5880, 292.4173)
GMR.DefineMailboxPath(5609.0550, -2617.0750, 292.3871)
GMR.DefineMailboxPath(5585.2680, -2617.5760, 292.4192)
GMR.DefineMailboxPath(5565.4880, -2617.4250, 292.4192)
GMR.DefineMailboxPath(5548.6700, -2617.0940, 298.1099)
GMR.DefineMailboxPath(5534.8650, -2620.4900, 303.7839)
GMR.DefineMailboxPath(5519.5910, -2624.2460, 303.9542)
GMR.DefineMailboxPath(5508.2020, -2632.5070, 303.9542)
GMR.DefineMailboxPath(5500.6010, -2641.6790, 303.9542)
GMR.DefineMailboxPath(5489.9090, -2643.9800, 305.9570)
GMR.DefineMailboxPath(5483.9750, -2644.6910, 306.9655)