GMR.DefineProfileName("Cloth - Dragonblight - Forgotten Shore - Humanoids [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(2987.4390, -665.1202, 43.8231, 80)
GMR.DefineProfileCenter(3053.0040, -798.0436, 14.8919, 80)
GMR.DefineProfileCenter(3132.4930, -877.2451, 29.7868, 80)
GMR.DefineProfileCenter(3157.0050, -1005.4610, 38.4718, 80)
GMR.DefineProfileCenter(3029.6430, -941.7005, 15.2448, 80)
GMR.DefineProfileCenter(2916.6400, -875.0048, 10.1787, 80)
GMR.DefineProfileCenter(2850.2500, -801.1996, 18.0678, 80)
GMR.DefineProfileCenter(2744.0340, -733.5048, 14.9652, 80)
GMR.DefineAreaBlacklist(3005.5980, -538.5674, 109.8819, 25)
GMR.DefineAreaBlacklist(2834.1230, -914.9827, 3.2185, 30)
GMR.DefineAreaBlacklist(2850.8780, -957.4891, 0.4350, 11)
GMR.DefineAreaBlacklist(2833.5220, -966.0832, -10.2759, 22)
GMR.DefineAreaBlacklist(2853.9870, -1009.9940, -15.7273, 11)
GMR.DefineAreaBlacklist(2843.4130, -1025.4010, -3.1757, 30)
GMR.DefineAreaBlacklist(2739.8160, -839.8170, -18.3437, 30)
GMR.DefineAreaBlacklist(2811.6770, -871.6033, -15.0055, 30)
GMR.DefineAreaBlacklist(2823.1660, -909.9440, -5.5120, 30)
GMR.DefineAreaBlacklist(2810.8600, -950.5042, -18.2960, 30)
GMR.DefineAreaBlacklist(2842.7000, -1027.4790, -13.8771, 30)
GMR.DefineAreaBlacklist(2841.4000, -1073.7040, -12.7768, 30)
GMR.DefineAreaBlacklist(2968.9110, -1064.5150, 4.6569, 30)
GMR.DefineMeshAreaBlacklist(3005.5980, -538.5674, 109.8819, 26)
GMR.DefineMeshAreaBlacklist(2834.1230, -914.9827, 3.2185, 32)
GMR.DefineMeshAreaBlacklist(2850.8780, -957.4891, 0.4350, 11)
GMR.DefineMeshAreaBlacklist(2833.5220, -966.0832, -10.2759, 22)
GMR.DefineMeshAreaBlacklist(2853.9870, -1009.9940, -15.7273, 11)
GMR.DefineMeshAreaBlacklist(2843.4130, -1025.4010, -3.1757, 32)
GMR.DefineMeshAreaBlacklist(2739.8160, -839.8170, -18.3437, 32)
GMR.DefineMeshAreaBlacklist(2811.6770, -871.6033, -15.0055, 32)
GMR.DefineMeshAreaBlacklist(2823.1660, -909.9440, -5.5120, 32)
GMR.DefineMeshAreaBlacklist(2810.8600, -950.5042, -18.2960, 32)
GMR.DefineMeshAreaBlacklist(2842.7000, -1027.4790, -13.8771, 32)
GMR.DefineMeshAreaBlacklist(2841.4000, -1073.7040, -12.7768, 32)
GMR.DefineMeshAreaBlacklist(2968.9110, -1064.5150, 4.6569, 32)
GMR.DefineQuestEnemyId(27229)
GMR.DefineQuestEnemyId(27225)
GMR.DefineQuestEnemyId(27224)
GMR.DefineSellVendor(3209.3500, -633.5820, 160.1684, 27267)
GMR.DefineRepairVendor(3209.3500, -633.5820, 160.1684, 27267)
GMR.DefineAmmoVendor(3255.7700, -700.2330, 167.2520, 27025)
GMR.DefineGoodsVendor(3214.1700, -684.0830, 167.5368, 27027)
GMR.DefineProfileMailbox(3225.9650, -690.8445, 167.1395, 188531)
GMR.DefineVendorPath("Sell", 3156.4320, -697.5669, 110.9450)
GMR.DefineVendorPath("Sell", 3181.9380, -619.7256, 132.3127)
GMR.DefineVendorPath("Sell", 3217.6050, -590.8616, 150.5460)
GMR.DefineVendorPath("Sell", 3225.8060, -620.2481, 160.6440)
GMR.DefineVendorPath("Sell", 3209.3500, -633.5820, 160.1685)
GMR.DefineVendorPath("Repair", 3156.4320, -697.5669, 110.9450)
GMR.DefineVendorPath("Repair", 3181.9380, -619.7256, 132.3127)
GMR.DefineVendorPath("Repair", 3217.6050, -590.8616, 150.5460)
GMR.DefineVendorPath("Repair", 3225.8060, -620.2481, 160.6440)
GMR.DefineVendorPath("Repair", 3209.3500, -633.5820, 160.1685)
GMR.DefineVendorPath("Goods", 3241.9990, -605.2327, 159.2374)
GMR.DefineVendorPath("Goods", 3234.7730, -636.3834, 164.7299)
GMR.DefineVendorPath("Goods", 3236.2570, -688.2825, 166.8978)
GMR.DefineVendorPath("Goods", 3226.2180, -687.7791, 167.1471)
GMR.DefineVendorPath("Goods", 3217.3190, -683.3751, 167.5354)
GMR.DefineVendorPath("Goods", 3214.1700, -684.0830, 167.5369)
GMR.DefineVendorPath("Ammo", 3155.4510, -699.9825, 110.9746)
GMR.DefineVendorPath("Ammo", 3174.4500, -651.0916, 126.5485)
GMR.DefineVendorPath("Ammo", 3189.4130, -608.7767, 135.0499)
GMR.DefineVendorPath("Ammo", 3224.9710, -590.9908, 152.6898)
GMR.DefineVendorPath("Ammo", 3233.1710, -652.1066, 165.8905)
GMR.DefineVendorPath("Ammo", 3243.7340, -688.3300, 166.8122)
GMR.DefineVendorPath("Ammo", 3255.7700, -700.2330, 167.2521)
GMR.DefineMailboxPath(3240.7570, -604.6458, 159.1132)
GMR.DefineMailboxPath(3235.5300, -642.2814, 165.0519)
GMR.DefineMailboxPath(3234.1380, -663.1171, 166.7673)
GMR.DefineMailboxPath(3232.9080, -686.3442, 167.0550)
GMR.DefineMailboxPath(3225.9690, -690.7654, 167.1400)