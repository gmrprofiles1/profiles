GMR.DefineProfileName("16-22 Draenei - Bloodmyst v2 [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Outland")
GMR.DefineProfileCenter(-1722.355, -11143.7, 70.01852, 80)
GMR.DefineProfileCenter(-1727.803, -11233.26, 60.64254, 80)
GMR.DefineProfileCenter(-1613.319, -11251.92, 69.35548, 80)
GMR.DefineProfileCenter(-1705.83, -11281.85, 60.69883, 60)
GMR.DefineProfileCenter(-1824.185, -11241.03, 57.41825, 80)
GMR.DefineProfileCenter(-1940.602, -11102.89, 58.32285, 80)
GMR.DefineProfileCenter(-1709.893, -11132.56, 71.31308, 80)
GMR.DefineProfileCenter(-1715.03, -11289.14, 60.71505, 80)
GMR.DefineProfileCenter(-1484.694, -11589.57, 21.59632, 80)
GMR.DefineProfileCenter(-1523.777, -11438.25, 63.74134, 80)
GMR.DefineProfileCenter(-1480.477, -11415.26, 68.05864, 80)
GMR.DefineProfileCenter(-1583.962, -11372.42, 60.49344, 30)
GMR.DefineProfileCenter(-1558.402, -11598.42, 27.77113, 80)
GMR.DefineProfileCenter(-1419.544, -11592.98, 13.51445, 80)
GMR.DefineProfileCenter(-1311.375, -11516.2, 8.883119, 20)
GMR.DefineProfileCenter(-1327.763, -11413.24, 19.25471, 30)
GMR.DefineProfileCenter(-1300.191, -11641.27, 2.093126, 80)
GMR.DefineProfileCenter(-1287.913, -11776.75, 7.615673, 80)
GMR.DefineProfileCenter(-1220.872, -11770.82, 6.43976, 80)
GMR.DefineProfileCenter(-1343.092, -11934.07, 15.29094, 80)
GMR.DefineProfileCenter(-1451.409, -12009.06, 10.26712, 80)
GMR.DefineProfileCenter(-1282.232, -11776.42, 6.897182, 80)
GMR.DefineProfileCenter(-1294.607, -11642.69, 2.591054, 80)
GMR.DefineProfileCenter(-1379.29, -11567.87, 8.703784, 80)
GMR.DefineProfileCenter(-1481.326, -11623.06, 19.81586, 80)
GMR.DefineProfileCenter(-1520.333, -11486.38, 59.99312, 80)
GMR.DefineProfileCenter(-1801.139, -11429.48, 46.13634, 80)
GMR.DefineProfileCenter(-1908.806, -11531.2, 52.46934, 80)
GMR.DefineProfileCenter(-1981.054, -11536.91, 57.60432, 80)
GMR.DefineProfileCenter(-2082.719, -11502.21, 59.39231, 30)
GMR.DefineProfileCenter(-1968.13, -11546.63, 56.99587, 80)
GMR.DefineProfileCenter(-1942.125, -11438.04, 65.11853, 80)
GMR.DefineProfileCenter(-1926.401, -11204.53, 64.22558, 80)
GMR.DefineProfileCenter(-1819.349, -11062.52, 66.00397, 80)
GMR.DefineProfileCenter(-1702.946, -11127.7, 71.93453, 80)
GMR.DefineProfileCenter(-1831.275, -11468.33, 46.69798, 80)
GMR.DefineAreaBlacklist(-1395.859, -11967.05, 16.50877,15)
GMR.DefineAreaBlacklist(-1560.292, -11316.3, 66.89428,20)
GMR.DefineAreaBlacklist(-1578.7, -11316.47, 64.46667,20)
GMR.DefineAreaBlacklist(-1438.953, -11840.34, 19.27396,20)
GMR.DefineAreaBlacklist(-1533.874, -12030.31, 11.32009,20)
GMR.DefineAreaBlacklist(-1537.807, -11829.21, 23.73433,20)
GMR.DefineAreaBlacklist(-1630.265, -11905.98, 9.803871,30)
GMR.DefineAreaBlacklist(-1432.637, -11841.17, 19.35571,20)
GMR.DefineAreaBlacklist(-1547.708, -11299.42, 67.82945,20)
GMR.DefineAreaBlacklist(-1565.658, -11219.77, 68.06197,20)
GMR.DefineQuestEnemyId(17350)
GMR.DefineQuestEnemyId(17527)
GMR.DefineQuestEnemyId(17348)
GMR.DefineQuestEnemyId(17344)
GMR.DefineQuestEnemyId(17550)
GMR.DefineQuestEnemyId(17322)
GMR.DefineQuestEnemyId(17323)
GMR.DefineQuestEnemyId(17340)
GMR.DefineQuestEnemyId(17342)
GMR.DefineQuestEnemyId(17341)
GMR.DefineQuestEnemyId(17346)
GMR.BlacklistId(17358)
GMR.BlacklistId(17346)
GMR.BlacklistId(17607)
GMR.BlacklistId(17608)
GMR.BlacklistId(17353)
GMR.BlacklistId(17328)
GMR.BlacklistId(17330)
GMR.BlacklistId(17329)
GMR.BlacklistId(17606)
GMR.BlacklistId(17604)
GMR.BlacklistId(17343)
GMR.DefineSellVendor(-1990.959961, -11814.500000, 54.583698, 17667)
GMR.DefineRepairVendor(-1990.959961, -11814.500000, 54.583698, 17667)
GMR.DefineAmmoVendor(-2023.560059, -11878.000000, 45.474899, 18811)
GMR.DefineGoodsVendor(-2059.199951, -11897.000000, 45.741501, 17553)
GMR.DefineHearthstoneBindLocation(-2059.199951, -11897.000000, 45.741501, 17553)
