GMR.DefineProfileName("70-74 Dragonblight - By Agmars Hammer v2 - Elks + Bears (Small) [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(4013.0020, 1601.4270, 111.7997, 80)
GMR.DefineProfileCenter(3986.7790, 1514.2490, 118.6628, 80)
GMR.DefineProfileCenter(4090.0520, 1409.5040, 136.8009, 80)
GMR.DefineProfileCenter(4209.7190, 1265.0540, 140.8692, 80)
GMR.DefineProfileCenter(4074.2140, 1353.9060, 134.1647, 80)
GMR.DefineProfileCenter(4011.9780, 1308.2750, 134.0343, 80)
GMR.DefineProfileCenter(3882.0190, 1321.6400, 122.8896, 80)
GMR.DefineProfileCenter(3777.9000, 1277.8620, 132.0089, 80)
GMR.DefineProfileCenter(3667.5330, 1273.4440, 132.4403, 80)
GMR.DefineProfileCenter(3550.3470, 1197.8060, 140.8386, 80)
GMR.DefineProfileCenter(3664.3790, 1290.2770, 127.7741, 80)
GMR.DefineProfileCenter(3698.6080, 1490.0570, 96.0717, 80)
GMR.DefineProfileCenter(3887.2500, 1319.6260, 124.0357, 80)
GMR.DefineProfileCenter(4018.8030, 1309.9290, 134.0452, 80)
GMR.DefineProfileCenter(4176.8810, 1348.0260, 140.3719, 80)
GMR.DefineAreaBlacklist(3801.1950, 1515.1530, 127.7247, 15)
GMR.DefineAreaBlacklist(3804.6520, 1444.1960, 128.8242, 15)
GMR.DefineAreaBlacklist(3868.7980, 1521.4230, 128.5643, 10)
GMR.DefineAreaBlacklist(3913.5740, 1299.1080, 159.0833, 15)
GMR.DefineAreaBlacklist(3892.1070, 1580.6650, 151.1291, 15)
GMR.DefineAreaBlacklist(3906.4570, 1610.7110, 151.1047, 15)
GMR.DefineAreaBlacklist(3914.3860, 1591.9890, 158.4724, 20)
GMR.DefineAreaBlacklist(3786.9570, 1606.3600, 145.8362, 15)
GMR.DefineAreaBlacklist(3764.0940, 1614.5600, 154.8641, 20)
GMR.DefineAreaBlacklist(3922.5880, 1454.2280, 175.0309, 15)
GMR.DefineAreaBlacklist(3953.1880, 1479.9030, 165.4692, 10)
GMR.DefineAreaBlacklist(4104.1880, 1398.2300, 157.5619, 10)
GMR.DefineAreaBlacklist(4122.9970, 1304.5160, 158.1125, 10)
GMR.DefineAreaBlacklist(3691.4870, 1349.6710, 127.6378, 10)
GMR.DefineAreaBlacklist(3709.4710, 1367.2770, 116.1830, 5)
GMR.DefineMeshAreaBlacklist(3691.4870, 1349.6710, 127.6378, 10)
GMR.DefineMeshAreaBlacklist(3709.4710, 1367.2770, 116.1830, 5)
GMR.DefineMeshAreaBlacklist(3801.1950, 1515.1530, 127.7247, 16)
GMR.DefineMeshAreaBlacklist(3804.6520, 1444.1960, 128.8242, 16)
GMR.DefineMeshAreaBlacklist(4122.9970, 1304.5160, 158.1125, 11)
GMR.DefineQuestEnemyId(26615)
GMR.DefineQuestEnemyId(27294)
GMR.DefineQuestEnemyId(26482)
GMR.BlacklistId(27685)
GMR.DefineSellVendor(3845.4900, 1572.7200, 86.6987, 27019)
GMR.DefineRepairVendor(3845.4900, 1572.7200, 86.6987, 27019)
GMR.DefineAmmoVendor(3818.2000, 1594.3300, 86.6987, 27022)
GMR.DefineGoodsVendor(3814.9900, 1585.3600, 86.6991, 26567)
GMR.DefineProfileMailbox(3828.0530, 1534.5670, 89.7255, 188355)
GMR.DefineVendorPath("Sell", 3748.5590, 1520.8420, 89.1970)
GMR.DefineVendorPath("Sell", 3782.5580, 1534.4190, 87.0815)
GMR.DefineVendorPath("Sell", 3834.8480, 1545.0850, 89.7248)
GMR.DefineVendorPath("Sell", 3834.9670, 1570.2640, 86.6984)
GMR.DefineVendorPath("Sell", 3845.4900, 1572.7200, 86.6986)
GMR.DefineVendorPath("Sell", 3845.4900, 1572.7200, 86.6986)
GMR.DefineVendorPath("Repair", 3741.5640, 1513.7900, 90.6969)
GMR.DefineVendorPath("Repair", 3774.6770, 1532.0190, 87.0539)
GMR.DefineVendorPath("Repair", 3807.3900, 1538.7330, 89.7252)
GMR.DefineVendorPath("Repair", 3833.2410, 1544.0390, 89.7252)
GMR.DefineVendorPath("Repair", 3835.0360, 1551.0940, 89.5134)
GMR.DefineVendorPath("Repair", 3838.5310, 1572.1530, 86.6988)
GMR.DefineVendorPath("Repair", 3845.4900, 1572.7200, 86.6987)
GMR.DefineVendorPath("Goods", 3736.1250, 1512.6190, 91.3033)
GMR.DefineVendorPath("Goods", 3771.7430, 1530.8890, 86.9645)
GMR.DefineVendorPath("Goods", 3809.9550, 1537.1390, 89.7233)
GMR.DefineVendorPath("Goods", 3832.2860, 1543.7170, 89.7233)
GMR.DefineVendorPath("Goods", 3835.5580, 1549.4730, 89.6740)
GMR.DefineVendorPath("Goods", 3832.7250, 1568.2290, 86.6985)
GMR.DefineVendorPath("Goods", 3823.2240, 1575.2720, 86.6985)
GMR.DefineVendorPath("Goods", 3814.9900, 1585.3600, 86.6989)
GMR.DefineVendorPath("Ammo", 3730.6300, 1510.9810, 91.8763)
GMR.DefineVendorPath("Ammo", 3770.1770, 1529.6660, 86.9223)
GMR.DefineVendorPath("Ammo", 3805.2890, 1536.5230, 89.7245)
GMR.DefineVendorPath("Ammo", 3834.4410, 1553.9930, 89.2595)
GMR.DefineVendorPath("Ammo", 3831.6650, 1568.3710, 86.6989)
GMR.DefineVendorPath("Ammo", 3820.8740, 1583.6470, 86.6989)
GMR.DefineVendorPath("Ammo", 3818.2000, 1594.3300, 86.6987)
GMR.DefineMailboxPath(3740.2080, 1515.5620, 90.6327)
GMR.DefineMailboxPath(3772.5310, 1530.9490, 86.9810)
GMR.DefineMailboxPath(3795.7490, 1535.9430, 88.2192)
GMR.DefineMailboxPath(3819.7950, 1541.2950, 89.7257)
GMR.DefineMailboxPath(3828.0700, 1534.5710, 89.7257)