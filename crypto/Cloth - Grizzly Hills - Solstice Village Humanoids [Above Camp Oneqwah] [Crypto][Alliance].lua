GMR.DefineProfileName("Cloth - Grizzly Hills - Solstice Village Humanoids (Above Camp Oneqwah) [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(4128.5970, -4248.2250, 249.3849, 80)
GMR.DefineProfileCenter(4115.9740, -4280.0780, 251.4234, 80)
GMR.DefineProfileCenter(4033.4570, -4307.4310, 252.0309, 80)
GMR.DefineProfileCenter(3990.3360, -4356.2860, 252.4532, 80)
GMR.DefineProfileCenter(3984.3390, -4463.5040, 268.1429, 80)
GMR.DefineProfileCenter(4039.8500, -4454.2250, 267.6866, 80)
GMR.DefineProfileCenter(4095.8350, -4422.1540, 261.1422, 80)
GMR.DefineProfileCenter(4114.5700, -4380.9690, 266.1830, 80)
GMR.DefineProfileCenter(4076.8700, -4299.1220, 251.6795, 80)
GMR.DefineQuestEnemyId(27408)
GMR.DefineQuestEnemyId(26389)
GMR.DefineQuestEnemyId(26416)
GMR.BlacklistId(27626)
GMR.DefineSellVendor(3836.0000, -4542.0800, 209.2768, 26707)
GMR.DefineRepairVendor(3836.0000, -4542.0800, 209.2768, 26707)
GMR.DefineAmmoVendor(3843.4200, -4550.3200, 209.7359, 26936)
GMR.DefineGoodsVendor(3875.4600, -4544.9900, 209.2663, 26680)
GMR.DefineProfileMailbox(3861.2500, -4548.6440, 209.6146, 188256)
GMR.DefineVendorPath("Sell", 3475.3490, -4352.8190, 230.8575)
GMR.DefineVendorPath("Sell", 3501.4120, -4365.2050, 235.6560)
GMR.DefineVendorPath("Sell", 3531.6050, -4386.7920, 234.7290)
GMR.DefineVendorPath("Sell", 3540.8340, -4418.5320, 224.0899)
GMR.DefineVendorPath("Sell", 3546.7510, -4431.7570, 218.1537)
GMR.DefineVendorPath("Sell", 3578.2340, -4428.4200, 208.7335)
GMR.DefineVendorPath("Sell", 3603.7770, -4425.2290, 198.8451)
GMR.DefineVendorPath("Sell", 3621.7040, -4434.2700, 188.3620)
GMR.DefineVendorPath("Sell", 3646.1380, -4448.9600, 184.9052)
GMR.DefineVendorPath("Sell", 3665.8680, -4461.4830, 185.4187)
GMR.DefineVendorPath("Sell", 3676.4680, -4467.8300, 187.6503)
GMR.DefineVendorPath("Sell", 3695.0440, -4475.5100, 186.7457)
GMR.DefineVendorPath("Sell", 3726.9830, -4468.8770, 186.8093)
GMR.DefineVendorPath("Sell", 3766.2080, -4460.4960, 189.0493)
GMR.DefineVendorPath("Sell", 3797.7940, -4449.5410, 190.3299)
GMR.DefineVendorPath("Sell", 3822.5490, -4454.9710, 198.8727)
GMR.DefineVendorPath("Sell", 3831.7830, -4479.8380, 202.6442)
GMR.DefineVendorPath("Sell", 3843.2680, -4496.6820, 205.3002)
GMR.DefineVendorPath("Sell", 3854.7940, -4511.0660, 209.2528)
GMR.DefineVendorPath("Sell", 3844.3970, -4528.4250, 209.8234)
GMR.DefineVendorPath("Sell", 3836.0000, -4542.0800, 209.2770)
GMR.DefineVendorPath("Repair", 3475.3490, -4352.8190, 230.8575)
GMR.DefineVendorPath("Repair", 3501.4120, -4365.2050, 235.6560)
GMR.DefineVendorPath("Repair", 3531.6050, -4386.7920, 234.7290)
GMR.DefineVendorPath("Repair", 3540.8340, -4418.5320, 224.0899)
GMR.DefineVendorPath("Repair", 3546.7510, -4431.7570, 218.1537)
GMR.DefineVendorPath("Repair", 3578.2340, -4428.4200, 208.7335)
GMR.DefineVendorPath("Repair", 3603.7770, -4425.2290, 198.8451)
GMR.DefineVendorPath("Repair", 3621.7040, -4434.2700, 188.3620)
GMR.DefineVendorPath("Repair", 3646.1380, -4448.9600, 184.9052)
GMR.DefineVendorPath("Repair", 3665.8680, -4461.4830, 185.4187)
GMR.DefineVendorPath("Repair", 3676.4680, -4467.8300, 187.6503)
GMR.DefineVendorPath("Repair", 3695.0440, -4475.5100, 186.7457)
GMR.DefineVendorPath("Repair", 3726.9830, -4468.8770, 186.8093)
GMR.DefineVendorPath("Repair", 3766.2080, -4460.4960, 189.0493)
GMR.DefineVendorPath("Repair", 3797.7940, -4449.5410, 190.3299)
GMR.DefineVendorPath("Repair", 3822.5490, -4454.9710, 198.8727)
GMR.DefineVendorPath("Repair", 3831.7830, -4479.8380, 202.6442)
GMR.DefineVendorPath("Repair", 3843.2680, -4496.6820, 205.3002)
GMR.DefineVendorPath("Repair", 3854.7940, -4511.0660, 209.2528)
GMR.DefineVendorPath("Repair", 3844.3970, -4528.4250, 209.8234)
GMR.DefineVendorPath("Repair", 3836.0000, -4542.0800, 209.2770)
GMR.DefineVendorPath("Goods", 3464.7620, -4380.7340, 231.4319)
GMR.DefineVendorPath("Goods", 3489.0990, -4388.5380, 237.5683)
GMR.DefineVendorPath("Goods", 3527.5460, -4380.4070, 236.1484)
GMR.DefineVendorPath("Goods", 3557.8580, -4378.4580, 223.7860)
GMR.DefineVendorPath("Goods", 3580.7150, -4393.4620, 215.4353)
GMR.DefineVendorPath("Goods", 3605.3100, -4399.4890, 206.2779)
GMR.DefineVendorPath("Goods", 3633.9050, -4409.6710, 189.7840)
GMR.DefineVendorPath("Goods", 3658.5380, -4426.3230, 186.2898)
GMR.DefineVendorPath("Goods", 3669.0440, -4443.4160, 184.1654)
GMR.DefineVendorPath("Goods", 3692.9860, -4451.5180, 181.6837)
GMR.DefineVendorPath("Goods", 3726.0700, -4454.2290, 185.0391)
GMR.DefineVendorPath("Goods", 3758.5810, -4455.7610, 188.6885)
GMR.DefineVendorPath("Goods", 3775.9940, -4456.6000, 189.0624)
GMR.DefineVendorPath("Goods", 3797.6930, -4448.5940, 190.3507)
GMR.DefineVendorPath("Goods", 3821.9720, -4451.3120, 197.6615)
GMR.DefineVendorPath("Goods", 3830.1450, -4477.5070, 202.5102)
GMR.DefineVendorPath("Goods", 3842.3670, -4496.7060, 205.1387)
GMR.DefineVendorPath("Goods", 3854.3240, -4508.3920, 209.0447)
GMR.DefineVendorPath("Goods", 3856.9030, -4524.4860, 209.2770)
GMR.DefineVendorPath("Goods", 3860.5160, -4540.7710, 209.2658)
GMR.DefineVendorPath("Goods", 3865.9260, -4543.6930, 209.2658)
GMR.DefineVendorPath("Goods", 3875.4600, -4544.9900, 209.2661)
GMR.DefineVendorPath("Ammo", 3475.3490, -4352.8190, 230.8575)
GMR.DefineVendorPath("Ammo", 3501.4120, -4365.2050, 235.6560)
GMR.DefineVendorPath("Ammo", 3531.6050, -4386.7920, 234.7290)
GMR.DefineVendorPath("Ammo", 3540.8340, -4418.5320, 224.0899)
GMR.DefineVendorPath("Ammo", 3546.7510, -4431.7570, 218.1537)
GMR.DefineVendorPath("Ammo", 3578.2340, -4428.4200, 208.7335)
GMR.DefineVendorPath("Ammo", 3603.7770, -4425.2290, 198.8451)
GMR.DefineVendorPath("Ammo", 3621.7040, -4434.2700, 188.3620)
GMR.DefineVendorPath("Ammo", 3646.1380, -4448.9600, 184.9052)
GMR.DefineVendorPath("Ammo", 3665.8680, -4461.4830, 185.4187)
GMR.DefineVendorPath("Ammo", 3676.4680, -4467.8300, 187.6503)
GMR.DefineVendorPath("Ammo", 3695.0440, -4475.5100, 186.7457)
GMR.DefineVendorPath("Ammo", 3726.9830, -4468.8770, 186.8093)
GMR.DefineVendorPath("Ammo", 3766.2080, -4460.4960, 189.0493)
GMR.DefineVendorPath("Ammo", 3797.7940, -4449.5410, 190.3299)
GMR.DefineVendorPath("Ammo", 3822.5490, -4454.9710, 198.8727)
GMR.DefineVendorPath("Ammo", 3831.7830, -4479.8380, 202.6442)
GMR.DefineVendorPath("Ammo", 3843.2680, -4496.6820, 205.3002)
GMR.DefineVendorPath("Ammo", 3854.7940, -4511.0660, 209.2528)
GMR.DefineVendorPath("Ammo", 3844.3970, -4528.4250, 209.8234)
GMR.DefineVendorPath("Ammo", 3842.5780, -4539.2290, 209.3851)
GMR.DefineVendorPath("Ammo", 3847.3090, -4546.4320, 209.3525)
GMR.DefineVendorPath("Ammo", 3843.4200, -4550.3200, 209.7360)
GMR.DefineMailboxPath(3482.1960, -4352.1450, 230.9866)
GMR.DefineMailboxPath(3503.1820, -4366.6490, 235.8394)
GMR.DefineMailboxPath(3542.5520, -4374.1820, 232.3587)
GMR.DefineMailboxPath(3567.3610, -4388.9010, 219.3406)
GMR.DefineMailboxPath(3589.5110, -4406.3190, 211.3023)
GMR.DefineMailboxPath(3608.0450, -4421.5050, 196.8556)
GMR.DefineMailboxPath(3626.5290, -4437.7810, 187.9876)
GMR.DefineMailboxPath(3647.9280, -4451.1160, 184.7188)
GMR.DefineMailboxPath(3664.7330, -4460.7760, 185.3295)
GMR.DefineMailboxPath(3693.3980, -4475.7850, 186.7018)
GMR.DefineMailboxPath(3723.0560, -4466.9690, 186.3041)
GMR.DefineMailboxPath(3753.6610, -4457.6590, 189.0963)
GMR.DefineMailboxPath(3783.3220, -4457.7400, 189.3793)
GMR.DefineMailboxPath(3807.0900, -4449.5810, 191.9993)
GMR.DefineMailboxPath(3823.7020, -4453.5870, 198.6429)
GMR.DefineMailboxPath(3830.2730, -4476.5420, 202.5524)
GMR.DefineMailboxPath(3842.2930, -4496.2620, 205.0841)
GMR.DefineMailboxPath(3853.5880, -4506.7510, 208.8037)
GMR.DefineMailboxPath(3858.1790, -4526.9030, 209.3132)
GMR.DefineMailboxPath(3859.0390, -4542.3220, 209.2662)
GMR.DefineMailboxPath(3861.4060, -4548.5140, 209.5918)