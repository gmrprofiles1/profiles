GMR.DefineProfileName("Eternals - Storm Peaks (North) - Eternal Air + Worm Meat [Crypto][Horde]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(8256.718, -88.35273, 839.6473, 80)
GMR.DefineProfileCenter(8294.854, -76.88042, 830.7534, 80)
GMR.DefineProfileCenter(8259.106, -15.97819, 835.2863, 80)
GMR.DefineProfileCenter(8363.819, 40.06788, 806.7826, 80)
GMR.DefineProfileCenter(8294.617, 128.9422, 830.2887, 80)
GMR.DefineProfileCenter(8483.46, 112.1987, 783.5273, 80)
GMR.DefineProfileCenter(8392.037, 159.0385, 808.7905, 80)
GMR.DefineProfileCenter(8270.608, -46.16165, 832.2665, 80)
GMR.DefineProfileCenter(8234.337, -159.941, 854.0504, 80)
GMR.DefineProfileCenter(8172.109, -70.33862, 851.3676, 80)
GMR.DefineProfileCenter(8136.472, -48.6445, 857.394, 80)
GMR.DefineProfileCenter(8047.25, -38.41647, 875.0049, 80)
GMR.DefineProfileCenter(8054.441, -116.3225, 861.9158, 80)
GMR.DefineProfileCenter(7990.755, -101.215, 870.452, 80)
GMR.DefineProfileCenter(7925.353, -136.5637, 874.4055, 80)
GMR.DefineProfileCenter(7870.347, -122.9031, 878.0053, 80)
GMR.DefineProfileCenter(7843.58, -140.1435, 878.6022, 80)
GMR.DefineProfileCenter(7801.938, -168.9299, 880.0634, 80)
GMR.DefineProfileCenter(7893.337, -263.195, 876.6198, 80)
GMR.DefineProfileCenter(7825.253, -319.9658, 894.1681, 80)
GMR.DefineProfileCenter(7837.25, -396.352, 899.5978, 80)
GMR.DefineProfileCenter(7910.557, -419.361, 889.3987, 80)
GMR.DefineProfileCenter(7923.922, -532.1282, 912.8791, 80)
GMR.DefineProfileCenter(7951.842, -521.2007, 912.0123, 80)
GMR.DefineProfileCenter(7845.888, -449.6959, 903.6011, 80)
GMR.DefineProfileCenter(7830.236, -398.3031, 901.1796, 80)
GMR.DefineProfileCenter(7830.103, -338.1613, 894.4334, 80)
GMR.DefineProfileCenter(7828.788, -259.9711, 890.8393, 80)
GMR.DefineProfileCenter(7894.874, -210.2513, 870.2484, 80)
GMR.DefineProfileCenter(7823.81, -87.12099, 881.1526, 80)
GMR.DefineProfileCenter(7985.775, -102.7613, 872.0284, 80)
GMR.DefineProfileCenter(8125.182, -82.80488, 856.1624, 80)
GMR.DefineProfileCenter(8154.57, -25.9218, 856.6701, 80)
GMR.DefineProfileCenter(8278.378, -88.009, 835.4875, 80)
GMR.DefineAreaBlacklist(8082.7360, -214.1171, 849.1771, 30)
GMR.DefineAreaBlacklist(8034.7720, -247.6104, 847.2096, 30)
GMR.DefineAreaBlacklist(8011.3210, -296.4593, 848.2976, 30)
GMR.DefineAreaBlacklist(8010.1770, -350.0557, 856.1047, 30)
GMR.DefineAreaBlacklist(8051.4650, -295.2616, 868.6008, 30)
GMR.DefineAreaBlacklist(8089.5340, -319.8997, 865.9276, 30)
GMR.DefineAreaBlacklist(8108.8690, -272.5297, 901.8051, 30)
GMR.DefineAreaBlacklist(8045.7020, -308.4721, 902.6870, 30)
GMR.DefineAreaBlacklist(8134.1310, -262.6718, 926.1215, 30)
GMR.DefineAreaBlacklist(8039.4140, -347.3780, 925.6670, 30)
GMR.DefineAreaBlacklist(8079.0110, -408.4577, 984.9507, 30)
GMR.DefineAreaBlacklist(8117.0330, -386.0871, 981.6255, 30)
GMR.DefineAreaBlacklist(8164.8380, -339.8222, 965.0143, 30)
GMR.DefineAreaBlacklist(8199.9970, -312.7350, 968.4967, 30)
GMR.DefineAreaBlacklist(8241.3680, -251.0803, 952.7894, 30)
GMR.DefineAreaBlacklist(8282.9290, -232.7119, 919.8608, 20)
GMR.DefineAreaBlacklist(8180.5980, -222.3200, 879.0634, 20)
GMR.DefineAreaBlacklist(7967.3600, -263.4087, 853.7609, 20)
GMR.DefineMeshAreaBlacklist(8082.7360, -214.1171, 849.1771, 32)
GMR.DefineMeshAreaBlacklist(8034.7720, -247.6104, 847.2096, 32)
GMR.DefineMeshAreaBlacklist(8011.3210, -296.4593, 848.2976, 32)
GMR.DefineMeshAreaBlacklist(8010.1770, -350.0557, 856.1047, 32)
GMR.DefineMeshAreaBlacklist(8051.4650, -295.2616, 868.6008, 32)
GMR.DefineMeshAreaBlacklist(8089.5340, -319.8997, 865.9276, 32)
GMR.DefineMeshAreaBlacklist(8108.8690, -272.5297, 901.8051, 32)
GMR.DefineMeshAreaBlacklist(8045.7020, -308.4721, 902.6870, 32)
GMR.DefineMeshAreaBlacklist(8134.1310, -262.6718, 926.1215, 32)
GMR.DefineMeshAreaBlacklist(8039.4140, -347.3780, 925.6670, 32)
GMR.DefineMeshAreaBlacklist(8079.0110, -408.4577, 984.9507, 32)
GMR.DefineMeshAreaBlacklist(8117.0330, -386.0871, 981.6255, 32)
GMR.DefineMeshAreaBlacklist(8164.8380, -339.8222, 965.0143, 32)
GMR.DefineMeshAreaBlacklist(8199.9970, -312.7350, 968.4967, 32)
GMR.DefineMeshAreaBlacklist(8241.3680, -251.0803, 952.7894, 32)
GMR.DefineMeshAreaBlacklist(8282.9290, -232.7119, 919.8608, 20)
GMR.DefineMeshAreaBlacklist(8180.5980, -222.3200, 879.0634, 20)
GMR.DefineMeshAreaBlacklist(7967.3600, -263.4087, 853.7609, 20)
GMR.DefineQuestEnemyId(29624)
GMR.DefineQuestEnemyId(29390)
GMR.BlacklistId(29377)
GMR.DefineSellVendor(8383.4500, -390.1660, 903.1190, 29964)
GMR.DefineRepairVendor(8383.4500, -390.1660, 903.1190, 29964)
GMR.DefineAmmoVendor(8409.7900, -393.8690, 903.1189, 29962)
GMR.DefineGoodsVendor(8426.4100, -355.5840, 906.4021, 29963)
GMR.DefineProfileMailbox(8441.4080, -328.6534, 906.6083, 193972)
GMR.DefineVendorPath("Sell", 8321.9850, -146.4632, 838.7548)
GMR.DefineVendorPath("Sell", 8347.8010, -172.4623, 839.6221)
GMR.DefineVendorPath("Sell", 8377.7270, -182.2339, 831.7011)
GMR.DefineVendorPath("Sell", 8398.6860, -207.6445, 832.4556)
GMR.DefineVendorPath("Sell", 8421.7080, -226.4664, 831.3672)
GMR.DefineVendorPath("Sell", 8451.0890, -243.3136, 838.3688)
GMR.DefineVendorPath("Sell", 8474.4690, -263.7405, 842.1360)
GMR.DefineVendorPath("Sell", 8491.9950, -271.8022, 844.9027)
GMR.DefineVendorPath("Sell", 8473.4410, -279.2617, 854.3558)
GMR.DefineVendorPath("Sell", 8448.0970, -275.6898, 865.1257)
GMR.DefineVendorPath("Sell", 8425.4060, -271.4563, 875.4626)
GMR.DefineVendorPath("Sell", 8408.3500, -268.6902, 882.8370)
GMR.DefineVendorPath("Sell", 8390.5590, -269.9288, 885.3040)
GMR.DefineVendorPath("Sell", 8404.3980, -285.6296, 896.1627)
GMR.DefineVendorPath("Sell", 8425.0690, -301.6198, 905.2186)
GMR.DefineVendorPath("Sell", 8443.3960, -324.9996, 906.6083)
GMR.DefineVendorPath("Sell", 8442.7870, -336.7228, 906.6083)
GMR.DefineVendorPath("Sell", 8426.3180, -355.5426, 906.4023)
GMR.DefineVendorPath("Sell", 8410.0810, -373.6576, 903.5703)
GMR.DefineVendorPath("Sell", 8396.1000, -384.0745, 903.0033)
GMR.DefineVendorPath("Sell", 8383.4500, -390.1660, 903.1190)
GMR.DefineVendorPath("Repair", 8401.6360, -223.4002, 836.3298)
GMR.DefineVendorPath("Repair", 8425.6390, -235.9678, 836.0665)
GMR.DefineVendorPath("Repair", 8445.0500, -243.4258, 839.5514)
GMR.DefineVendorPath("Repair", 8465.9500, -251.9038, 837.8793)
GMR.DefineVendorPath("Repair", 8487.8030, -261.8965, 839.6171)
GMR.DefineVendorPath("Repair", 8486.3490, -278.3753, 849.8098)
GMR.DefineVendorPath("Repair", 8464.7690, -277.7582, 856.7622)
GMR.DefineVendorPath("Repair", 8443.4310, -274.5038, 867.3959)
GMR.DefineVendorPath("Repair", 8423.0730, -271.4348, 876.6315)
GMR.DefineVendorPath("Repair", 8403.6260, -267.9735, 884.4666)
GMR.DefineVendorPath("Repair", 8392.0270, -271.3465, 885.4426)
GMR.DefineVendorPath("Repair", 8402.3890, -283.8090, 894.3664)
GMR.DefineVendorPath("Repair", 8418.2450, -295.6371, 903.0416)
GMR.DefineVendorPath("Repair", 8436.3730, -310.0653, 906.1971)
GMR.DefineVendorPath("Repair", 8451.6050, -322.9362, 906.8499)
GMR.DefineVendorPath("Repair", 8443.8160, -335.7912, 906.6083)
GMR.DefineVendorPath("Repair", 8428.4020, -353.2852, 906.4022)
GMR.DefineVendorPath("Repair", 8413.0700, -369.0712, 905.5019)
GMR.DefineVendorPath("Repair", 8397.1330, -382.9527, 903.0042)
GMR.DefineVendorPath("Repair", 8383.4500, -390.1660, 903.1204)
GMR.DefineVendorPath("Goods", 8382.0640, -185.4152, 831.2933)
GMR.DefineVendorPath("Goods", 8397.1200, -205.6085, 832.6025)
GMR.DefineVendorPath("Goods", 8417.2420, -218.2080, 829.3217)
GMR.DefineVendorPath("Goods", 8437.1740, -229.8120, 829.8748)
GMR.DefineVendorPath("Goods", 8458.3140, -242.3827, 835.3203)
GMR.DefineVendorPath("Goods", 8479.1920, -256.4336, 835.3506)
GMR.DefineVendorPath("Goods", 8497.7670, -268.4667, 841.4008)
GMR.DefineVendorPath("Goods", 8493.1440, -279.7730, 848.7173)
GMR.DefineVendorPath("Goods", 8465.0570, -277.5034, 856.6355)
GMR.DefineVendorPath("Goods", 8438.7470, -273.7011, 869.7695)
GMR.DefineVendorPath("Goods", 8414.7200, -269.3181, 880.2919)
GMR.DefineVendorPath("Goods", 8391.7790, -268.8078, 885.2992)
GMR.DefineVendorPath("Goods", 8399.2690, -281.6437, 891.7500)
GMR.DefineVendorPath("Goods", 8418.4730, -296.2518, 903.2263)
GMR.DefineVendorPath("Goods", 8439.2640, -312.9861, 906.4109)
GMR.DefineVendorPath("Goods", 8446.3840, -330.6708, 906.6082)
GMR.DefineVendorPath("Goods", 8439.6240, -340.8784, 906.6082)
GMR.DefineVendorPath("Goods", 8426.4100, -355.5840, 906.4020)
GMR.DefineVendorPath("Ammo", 8379.7050, -192.9379, 834.6889)
GMR.DefineVendorPath("Ammo", 8400.1290, -206.7685, 831.8224)
GMR.DefineVendorPath("Ammo", 8423.4060, -219.2171, 828.6506)
GMR.DefineVendorPath("Ammo", 8451.2430, -233.7055, 832.6269)
GMR.DefineVendorPath("Ammo", 8471.5400, -249.9974, 835.3045)
GMR.DefineVendorPath("Ammo", 8486.0090, -269.0477, 845.0137)
GMR.DefineVendorPath("Ammo", 8472.8890, -278.6158, 854.3690)
GMR.DefineVendorPath("Ammo", 8446.1230, -274.5900, 866.0352)
GMR.DefineVendorPath("Ammo", 8421.8450, -271.4050, 877.2626)
GMR.DefineVendorPath("Ammo", 8402.6890, -267.3521, 884.5610)
GMR.DefineVendorPath("Ammo", 8392.3670, -270.0877, 885.2988)
GMR.DefineVendorPath("Ammo", 8401.1980, -282.8623, 893.2987)
GMR.DefineVendorPath("Ammo", 8418.2960, -297.3117, 903.4863)
GMR.DefineVendorPath("Ammo", 8439.1010, -312.0799, 906.3844)
GMR.DefineVendorPath("Ammo", 8445.5190, -332.9526, 906.6080)
GMR.DefineVendorPath("Ammo", 8434.1520, -347.1390, 906.6080)
GMR.DefineVendorPath("Ammo", 8417.2510, -365.5303, 906.4549)
GMR.DefineVendorPath("Ammo", 8408.4770, -383.6574, 903.0483)
GMR.DefineVendorPath("Ammo", 8409.7900, -393.8690, 903.1189)
GMR.DefineMailboxPath(8375.5490, -186.8937, 832.5340)
GMR.DefineMailboxPath(8392.4850, -201.4814, 833.0284)
GMR.DefineMailboxPath(8413.1880, -214.8545, 829.6998)
GMR.DefineMailboxPath(8437.7950, -227.7585, 828.7980)
GMR.DefineMailboxPath(8460.7960, -244.0806, 835.7833)
GMR.DefineMailboxPath(8480.0340, -258.3452, 836.7357)
GMR.DefineMailboxPath(8490.0380, -271.3682, 845.2632)
GMR.DefineMailboxPath(8476.5620, -278.7448, 853.2691)
GMR.DefineMailboxPath(8452.6850, -275.2734, 862.4200)
GMR.DefineMailboxPath(8432.2880, -272.6682, 872.6153)
GMR.DefineMailboxPath(8414.3540, -269.9633, 880.4308)
GMR.DefineMailboxPath(8391.4870, -268.7057, 885.3000)
GMR.DefineMailboxPath(8397.9680, -281.5177, 891.1547)
GMR.DefineMailboxPath(8417.5260, -296.0015, 903.0648)
GMR.DefineMailboxPath(8435.9160, -310.2255, 906.1747)
GMR.DefineMailboxPath(8446.9590, -319.1088, 906.8428)
GMR.DefineMailboxPath(8441.4080, -328.6534, 906.6083)