GMR.DefineProfileName("[Flying] Eternals - Grizzly Hills - Eternal Air v2 (DunArgol Only) [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(3356.3570, -4909.5180, 332.8695, 80)
GMR.DefineProfileCenter(3482.2490, -4946.9580, 267.6623, 80)
GMR.DefineProfileCenter(3626.7690, -4981.4480, 206.1874, 80)
GMR.DefineProfileCenter(3724.6240, -5068.0980, 165.0398, 80)
GMR.DefineProfileCenter(3665.6000, -5154.7640, 169.3132, 80)
GMR.DefineProfileCenter(3576.1620, -5225.4510, 197.0508, 80)
GMR.DefineProfileCenter(3524.2400, -5322.7450, 191.3235, 80)
GMR.DefineProfileCenter(3656.8140, -5129.7380, 206.5225, 80)
GMR.DefineProfileCenter(3585.1640, -4998.4680, 227.8582, 80)
GMR.DefineProfileCenter(3485.3070, -4959.0710, 277.4922, 80)
GMR.DefineProfileCenter(3365.3740, -4935.5080, 324.9474, 80)
GMR.DefineQuestEnemyId(26407)
GMR.DefineQuestEnemyId(24229)
GMR.DefineSellVendor(2428.3200, -5086.5200, 273.2877, 24052)
GMR.DefineRepairVendor(2428.3200, -5086.5200, 273.2877, 24052)
GMR.DefineAmmoVendor(2422.0200, -5176.1300, 277.7525, 24053)
GMR.DefineGoodsVendor(2477.3900, -5054.4900, 284.3919, 24057)
GMR.DefineProfileMailbox(2475.6720, -5075.6310, 282.9850, 186506)
GMR.DefineVendorPath("Sell", 2570.9330, -5009.4060, 415.0504)
GMR.DefineVendorPath("Sell", 2530.6250, -5019.0850, 371.0143)
GMR.DefineVendorPath("Sell", 2481.0460, -5030.4330, 328.2525)
GMR.DefineVendorPath("Sell", 2457.9210, -5051.7140, 300.4960)
GMR.DefineVendorPath("Sell", 2439.8650, -5076.7170, 273.1764)
GMR.DefineVendorPath("Sell", 2428.3200, -5086.5200, 273.2878)
GMR.DefineVendorPath("Repair", 2570.9330, -5009.4060, 415.0504)
GMR.DefineVendorPath("Repair", 2530.6250, -5019.0850, 371.0143)
GMR.DefineVendorPath("Repair", 2481.0460, -5030.4330, 328.2525)
GMR.DefineVendorPath("Repair", 2457.9210, -5051.7140, 300.4960)
GMR.DefineVendorPath("Repair", 2439.8650, -5076.7170, 273.1764)
GMR.DefineVendorPath("Repair", 2428.3200, -5086.5200, 273.2878)
GMR.DefineVendorPath("Goods", 2510.8900, -5145.2240, 366.5068)
GMR.DefineVendorPath("Goods", 2483.2380, -5118.8400, 323.0129)
GMR.DefineVendorPath("Goods", 2469.7830, -5088.6220, 294.1276)
GMR.DefineVendorPath("Goods", 2472.7600, -5068.7970, 284.7856)
GMR.DefineVendorPath("Goods", 2475.9570, -5062.5110, 284.9393)
GMR.DefineVendorPath("Goods", 2477.3900, -5054.4900, 284.3919)
GMR.DefineVendorPath("Ammo", 2498.1290, -5099.2810, 352.0800)
GMR.DefineVendorPath("Ammo", 2469.1510, -5120.9450, 323.1325)
GMR.DefineVendorPath("Ammo", 2441.5190, -5144.9350, 295.0775)
GMR.DefineVendorPath("Ammo", 2426.1260, -5165.9010, 276.8569)
GMR.DefineVendorPath("Ammo", 2422.0200, -5176.1300, 277.7525)
GMR.DefineMailboxPath(2487.2680, -5145.1250, 351.7820)
GMR.DefineMailboxPath(2471.1930, -5116.9330, 320.1091)
GMR.DefineMailboxPath(2468.0710, -5096.0560, 300.4585)
GMR.DefineMailboxPath(2470.7280, -5082.0290, 289.5311)
GMR.DefineMailboxPath(2475.6720, -5075.6310, 282.9850)