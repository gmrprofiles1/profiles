GMR.DefineProfileName("64-70 Nagrand - Around Ring of Trials v2 - Telaar Vendors [Crypto][Alliance]")
GMR.DefineProfileType("Grinding")
GMR.DefineProfileContinent("Outland")
GMR.DefineProfileCenter(-1429.361, 6591.828, 35.40303, 80)
GMR.DefineProfileCenter(-1532.797, 6777.391, 13.77376, 80)
GMR.DefineProfileCenter(-1585.703, 6869.497, -11.2667, 80)
GMR.DefineProfileCenter(-1562.262, 7003.144, 0.8189923, 80)
GMR.DefineProfileCenter(-1621.28, 7164.765, 2.886883, 80)
GMR.DefineProfileCenter(-1651.351, 7392.816, -0.7571673, 80)
GMR.DefineProfileCenter(-1759.905, 7522.404, -8.044071, 80)
GMR.DefineProfileCenter(-1868.573, 7505.46, -7.685871, 80)
GMR.DefineProfileCenter(-1897.23, 7339.741, -20.85601, 80)
GMR.DefineProfileCenter(-2030.114, 7360.323, -32.95383, 80)
GMR.DefineProfileCenter(-1635.145, 7106.332, 4.890052, 80)
GMR.DefineProfileCenter(-1758.936, 6819.458, -28.50351, 80)
GMR.DefineProfileCenter(-1721.51, 6678.866, -17.42623, 80)
GMR.DefineProfileCenter(-1829.358, 6612.281, -2.68595, 80)
GMR.DefineProfileCenter(-1865.269, 6478.015, 23.64894, 80)
GMR.DefineProfileCenter(-1700.874, 6381.083, 42.27262, 120)
GMR.DefineProfileCenter(-2087.211, 6429.951, 22.83161, 120)
GMR.DefineProfileCenter(-2261.05, 6536.031, 8.711577, 80)
GMR.DefineProfileCenter(-2343.52, 6648.872, 1.044387, 80)
GMR.DefineProfileCenter(-2563.307, 6602.932, 4.366075, 80)
GMR.DefineProfileCenter(-2694.205, 6562.12, 26.17357, 80)
GMR.DefineProfileCenter(-2648.344, 6482.102, 28.55662, 80)
GMR.DefineProfileCenter(-2578.549, 6424.449, 25.51998, 80)
GMR.DefineProfileCenter(-2392.057, 6409.718, 16.66008, 80)
GMR.DefineQuestEnemyId(17130)
GMR.DefineQuestEnemyId(17158)
GMR.DefineQuestEnemyId(18205)
GMR.DefineQuestEnemyId(17128)
GMR.DefineQuestEnemyId(18334)
GMR.DefineQuestEnemyId(17132)
GMR.DefineQuestEnemyId(17131)
GMR.DefineQuestEnemyId(17159)
GMR.BlacklistId(17141)
GMR.BlacklistId(17139)
GMR.BlacklistId(18489)
GMR.DefineSellVendor(-1480.119995, 6357.979980, 34.347698, 18278)
GMR.DefineRepairVendor(-1480.119995, 6357.979980, 34.347698, 18278)
GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)
GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)
GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)
