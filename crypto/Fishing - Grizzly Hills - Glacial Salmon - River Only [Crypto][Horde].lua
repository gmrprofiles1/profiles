GMR.DefineProfileName("Fishing - Grizzly Hills - Glacial Salmon - River Only [Crypto][Horde]")
GMR.DefineProfileType("Fishing")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(4197.7450, -1599.4530, 170.3316, 300)
GMR.DefineProfileCenter(4083.5680, -1630.1930, 178.6334, 300)
GMR.DefineProfileCenter(4143.7600, -1817.0220, 199.6549, 300)
GMR.DefineProfileCenter(4176.3500, -1823.3470, 200.2268, 300)
GMR.DefineProfileCenter(4198.8890, -1913.5710, 200.9960, 300)
GMR.DefineProfileCenter(4153.5830, -1935.6130, 203.0017, 300)
GMR.DefineProfileCenter(4090.3910, -1825.0270, 205.2570, 300)
GMR.DefineProfileCenter(4037.6860, -2316.8220, 219.2361, 300)
GMR.DefineProfileCenter(4129.2320, -2544.8800, 217.2358, 300)
GMR.DefineProfileCenter(4109.3690, -2593.5770, 221.1334, 300)
GMR.DefineProfileCenter(4154.2010, -2704.4450, 228.7203, 300)
GMR.DefineProfileCenter(3911.9140, -2752.0950, 219.4531, 300)
GMR.DefineProfileCenter(3728.3910, -2635.8930, 164.5817, 300)
GMR.DefineProfileCenter(3679.4510, -2652.0320, 156.8961, 300)
GMR.DefineProfileCenter(3630.3170, -2640.9970, 136.2619, 300)
GMR.DefineProfileCenter(3598.3230, -2641.9470, 129.2557, 300)
GMR.DefineProfileCenter(3557.4700, -2639.4670, 113.4724, 300)
GMR.DefineProfileCenter(3411.5900, -2624.9630, 51.3164, 300)
GMR.DefineProfileCenter(3395.7690, -2601.6130, 51.6466, 300)
GMR.DefineProfileCenter(3392.8770, -2576.6710, 51.6939, 300)
GMR.DefineProfileCenter(3392.2420, -2645.9550, 50.9288, 300)
GMR.DefineProfileCenter(3343.2360, -2638.0610, 51.9495, 300)
GMR.DefineProfileCenter(3234.0180, -2587.8750, 50.0990, 300)
GMR.DefineProfileCenter(3262.1300, -2528.1450, 50.1778, 300)
GMR.DefineProfileCenter(3131.6900, -2498.3310, 57.5607, 300)
GMR.DefineProfileCenter(3083.3080, -2473.8020, 54.6102, 300)
GMR.DefineProfileCenter(2835.7450, -2384.1430, 20.8247, 300)
GMR.DefineQuestEnemyId(32429)
GMR.DefineQuestEnemyId(32422)
GMR.DefineQuestEnemyId(32438)
GMR.DefineQuestEnemyId(38453)
GMR.DefineSellVendor(3836.0000, -4542.0800, 209.2768, 26707)
GMR.DefineRepairVendor(3836.0000, -4542.0800, 209.2768, 26707)
GMR.DefineAmmoVendor(3843.4200, -4550.3200, 209.7359, 26936)
GMR.DefineGoodsVendor(3875.4600, -4544.9900, 209.2663, 26680)
GMR.DefineProfileMailbox(3861.2500, -4548.6440, 209.6146, 188256)
GMR.DefineVendorPath("Sell", 3475.3490, -4352.8190, 230.8575)
GMR.DefineVendorPath("Sell", 3501.4120, -4365.2050, 235.6560)
GMR.DefineVendorPath("Sell", 3531.6050, -4386.7920, 234.7290)
GMR.DefineVendorPath("Sell", 3540.8340, -4418.5320, 224.0899)
GMR.DefineVendorPath("Sell", 3546.7510, -4431.7570, 218.1537)
GMR.DefineVendorPath("Sell", 3578.2340, -4428.4200, 208.7335)
GMR.DefineVendorPath("Sell", 3603.7770, -4425.2290, 198.8451)
GMR.DefineVendorPath("Sell", 3621.7040, -4434.2700, 188.3620)
GMR.DefineVendorPath("Sell", 3646.1380, -4448.9600, 184.9052)
GMR.DefineVendorPath("Sell", 3665.8680, -4461.4830, 185.4187)
GMR.DefineVendorPath("Sell", 3676.4680, -4467.8300, 187.6503)
GMR.DefineVendorPath("Sell", 3695.0440, -4475.5100, 186.7457)
GMR.DefineVendorPath("Sell", 3726.9830, -4468.8770, 186.8093)
GMR.DefineVendorPath("Sell", 3766.2080, -4460.4960, 189.0493)
GMR.DefineVendorPath("Sell", 3797.7940, -4449.5410, 190.3299)
GMR.DefineVendorPath("Sell", 3822.5490, -4454.9710, 198.8727)
GMR.DefineVendorPath("Sell", 3831.7830, -4479.8380, 202.6442)
GMR.DefineVendorPath("Sell", 3843.2680, -4496.6820, 205.3002)
GMR.DefineVendorPath("Sell", 3854.7940, -4511.0660, 209.2528)
GMR.DefineVendorPath("Sell", 3844.3970, -4528.4250, 209.8234)
GMR.DefineVendorPath("Sell", 3836.0000, -4542.0800, 209.2770)
GMR.DefineVendorPath("Repair", 3475.3490, -4352.8190, 230.8575)
GMR.DefineVendorPath("Repair", 3501.4120, -4365.2050, 235.6560)
GMR.DefineVendorPath("Repair", 3531.6050, -4386.7920, 234.7290)
GMR.DefineVendorPath("Repair", 3540.8340, -4418.5320, 224.0899)
GMR.DefineVendorPath("Repair", 3546.7510, -4431.7570, 218.1537)
GMR.DefineVendorPath("Repair", 3578.2340, -4428.4200, 208.7335)
GMR.DefineVendorPath("Repair", 3603.7770, -4425.2290, 198.8451)
GMR.DefineVendorPath("Repair", 3621.7040, -4434.2700, 188.3620)
GMR.DefineVendorPath("Repair", 3646.1380, -4448.9600, 184.9052)
GMR.DefineVendorPath("Repair", 3665.8680, -4461.4830, 185.4187)
GMR.DefineVendorPath("Repair", 3676.4680, -4467.8300, 187.6503)
GMR.DefineVendorPath("Repair", 3695.0440, -4475.5100, 186.7457)
GMR.DefineVendorPath("Repair", 3726.9830, -4468.8770, 186.8093)
GMR.DefineVendorPath("Repair", 3766.2080, -4460.4960, 189.0493)
GMR.DefineVendorPath("Repair", 3797.7940, -4449.5410, 190.3299)
GMR.DefineVendorPath("Repair", 3822.5490, -4454.9710, 198.8727)
GMR.DefineVendorPath("Repair", 3831.7830, -4479.8380, 202.6442)
GMR.DefineVendorPath("Repair", 3843.2680, -4496.6820, 205.3002)
GMR.DefineVendorPath("Repair", 3854.7940, -4511.0660, 209.2528)
GMR.DefineVendorPath("Repair", 3844.3970, -4528.4250, 209.8234)
GMR.DefineVendorPath("Repair", 3836.0000, -4542.0800, 209.2770)
GMR.DefineVendorPath("Goods", 3464.7620, -4380.7340, 231.4319)
GMR.DefineVendorPath("Goods", 3489.0990, -4388.5380, 237.5683)
GMR.DefineVendorPath("Goods", 3527.5460, -4380.4070, 236.1484)
GMR.DefineVendorPath("Goods", 3557.8580, -4378.4580, 223.7860)
GMR.DefineVendorPath("Goods", 3580.7150, -4393.4620, 215.4353)
GMR.DefineVendorPath("Goods", 3605.3100, -4399.4890, 206.2779)
GMR.DefineVendorPath("Goods", 3633.9050, -4409.6710, 189.7840)
GMR.DefineVendorPath("Goods", 3658.5380, -4426.3230, 186.2898)
GMR.DefineVendorPath("Goods", 3669.0440, -4443.4160, 184.1654)
GMR.DefineVendorPath("Goods", 3692.9860, -4451.5180, 181.6837)
GMR.DefineVendorPath("Goods", 3726.0700, -4454.2290, 185.0391)
GMR.DefineVendorPath("Goods", 3758.5810, -4455.7610, 188.6885)
GMR.DefineVendorPath("Goods", 3775.9940, -4456.6000, 189.0624)
GMR.DefineVendorPath("Goods", 3797.6930, -4448.5940, 190.3507)
GMR.DefineVendorPath("Goods", 3821.9720, -4451.3120, 197.6615)
GMR.DefineVendorPath("Goods", 3830.1450, -4477.5070, 202.5102)
GMR.DefineVendorPath("Goods", 3842.3670, -4496.7060, 205.1387)
GMR.DefineVendorPath("Goods", 3854.3240, -4508.3920, 209.0447)
GMR.DefineVendorPath("Goods", 3856.9030, -4524.4860, 209.2770)
GMR.DefineVendorPath("Goods", 3860.5160, -4540.7710, 209.2658)
GMR.DefineVendorPath("Goods", 3865.9260, -4543.6930, 209.2658)
GMR.DefineVendorPath("Goods", 3875.4600, -4544.9900, 209.2661)
GMR.DefineVendorPath("Ammo", 3475.3490, -4352.8190, 230.8575)
GMR.DefineVendorPath("Ammo", 3501.4120, -4365.2050, 235.6560)
GMR.DefineVendorPath("Ammo", 3531.6050, -4386.7920, 234.7290)
GMR.DefineVendorPath("Ammo", 3540.8340, -4418.5320, 224.0899)
GMR.DefineVendorPath("Ammo", 3546.7510, -4431.7570, 218.1537)
GMR.DefineVendorPath("Ammo", 3578.2340, -4428.4200, 208.7335)
GMR.DefineVendorPath("Ammo", 3603.7770, -4425.2290, 198.8451)
GMR.DefineVendorPath("Ammo", 3621.7040, -4434.2700, 188.3620)
GMR.DefineVendorPath("Ammo", 3646.1380, -4448.9600, 184.9052)
GMR.DefineVendorPath("Ammo", 3665.8680, -4461.4830, 185.4187)
GMR.DefineVendorPath("Ammo", 3676.4680, -4467.8300, 187.6503)
GMR.DefineVendorPath("Ammo", 3695.0440, -4475.5100, 186.7457)
GMR.DefineVendorPath("Ammo", 3726.9830, -4468.8770, 186.8093)
GMR.DefineVendorPath("Ammo", 3766.2080, -4460.4960, 189.0493)
GMR.DefineVendorPath("Ammo", 3797.7940, -4449.5410, 190.3299)
GMR.DefineVendorPath("Ammo", 3822.5490, -4454.9710, 198.8727)
GMR.DefineVendorPath("Ammo", 3831.7830, -4479.8380, 202.6442)
GMR.DefineVendorPath("Ammo", 3843.2680, -4496.6820, 205.3002)
GMR.DefineVendorPath("Ammo", 3854.7940, -4511.0660, 209.2528)
GMR.DefineVendorPath("Ammo", 3844.3970, -4528.4250, 209.8234)
GMR.DefineVendorPath("Ammo", 3842.5780, -4539.2290, 209.3851)
GMR.DefineVendorPath("Ammo", 3847.3090, -4546.4320, 209.3525)
GMR.DefineVendorPath("Ammo", 3843.4200, -4550.3200, 209.7360)
GMR.DefineMailboxPath(3482.1960, -4352.1450, 230.9866)
GMR.DefineMailboxPath(3503.1820, -4366.6490, 235.8394)
GMR.DefineMailboxPath(3542.5520, -4374.1820, 232.3587)
GMR.DefineMailboxPath(3567.3610, -4388.9010, 219.3406)
GMR.DefineMailboxPath(3589.5110, -4406.3190, 211.3023)
GMR.DefineMailboxPath(3608.0450, -4421.5050, 196.8556)
GMR.DefineMailboxPath(3626.5290, -4437.7810, 187.9876)
GMR.DefineMailboxPath(3647.9280, -4451.1160, 184.7188)
GMR.DefineMailboxPath(3664.7330, -4460.7760, 185.3295)
GMR.DefineMailboxPath(3693.3980, -4475.7850, 186.7018)
GMR.DefineMailboxPath(3723.0560, -4466.9690, 186.3041)
GMR.DefineMailboxPath(3753.6610, -4457.6590, 189.0963)
GMR.DefineMailboxPath(3783.3220, -4457.7400, 189.3793)
GMR.DefineMailboxPath(3807.0900, -4449.5810, 191.9993)
GMR.DefineMailboxPath(3823.7020, -4453.5870, 198.6429)
GMR.DefineMailboxPath(3830.2730, -4476.5420, 202.5524)
GMR.DefineMailboxPath(3842.2930, -4496.2620, 205.0841)
GMR.DefineMailboxPath(3853.5880, -4506.7510, 208.8037)
GMR.DefineMailboxPath(3858.1790, -4526.9030, 209.3132)
GMR.DefineMailboxPath(3859.0390, -4542.3220, 209.2662)
GMR.DefineMailboxPath(3861.4060, -4548.5140, 209.5918)