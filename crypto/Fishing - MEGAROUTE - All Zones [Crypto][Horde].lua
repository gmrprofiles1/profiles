GMR.DefineProfileName("Fishing - MEGAROUTE - All Zones [Crypto][Horde]")
GMR.DefineProfileType("Fishing")
GMR.DefineProfileContinent("Northrend")
GMR.DefineProfileCenter(3096.8100, 3543.5270, 0.1763, 300)
GMR.DefineProfileCenter(3122.7840, 3567.6830, 0.1763, 300)
GMR.DefineProfileCenter(3057.1690, 3608.8590, 0.1768, 300)
GMR.DefineProfileCenter(3034.1220, 3626.9720, 0.1768, 300)
GMR.DefineProfileCenter(3041.0700, 3674.1510, 0.1768, 300)
GMR.DefineProfileCenter(3044.2970, 3730.3370, 0.1768, 300)
GMR.DefineProfileCenter(3024.0510, 3799.8830, 0.1768, 300)
GMR.DefineProfileCenter(2924.7860, 4007.5000, 0.0106, 300)
GMR.DefineProfileCenter(2894.8200, 4023.2040, 0.1349, 300)
GMR.DefineProfileCenter(2872.9030, 4093.4720, 1.5652, 300)
GMR.DefineProfileCenter(2846.8920, 4270.5830, 0.3721, 300)
GMR.DefineProfileCenter(2950.5320, 4541.0010, 0.3975, 300)
GMR.DefineProfileCenter(3024.6790, 4813.8500, 0.3960, 300)
GMR.DefineProfileCenter(2971.8650, 4834.0300, 0.3967, 300)
GMR.DefineProfileCenter(2506.9060, 5024.2600, 0.3794, 300)
GMR.DefineProfileCenter(2409.3780, 5039.5400, 0.3538, 300)
GMR.DefineProfileCenter(2407.4470, 5095.6920, 0.6351, 300)
GMR.DefineProfileCenter(1915.0520, 6240.6010, 0.2074, 300)
GMR.DefineProfileCenter(1961.7150, 6271.0630, 0.4730, 300)
GMR.DefineProfileCenter(2747.1570, 6934.3590, 2.6609, 300)
GMR.DefineProfileCenter(3557.0090, 6181.5200, 3.4746, 300)
GMR.DefineProfileCenter(3622.7170, 6064.4460, 3.6833, 300)
GMR.DefineProfileCenter(4255.1290, 6029.8910, 0.2323, 300)
GMR.DefineProfileCenter(4314.9650, 6060.6740, 0.9780, 300)
GMR.DefineProfileCenter(4377.0080, 6117.3690, 2.5288, 300)
GMR.DefineProfileCenter(4386.4130, 6141.9680, 0.1069, 300)
GMR.DefineProfileCenter(4407.8750, 6165.2620, 0.2845, 300)
GMR.DefineProfileCenter(4316.2740, 6213.6600, 0.4049, 300)
GMR.DefineProfileCenter(4204.5930, 6165.5320, 0.7168, 300)
GMR.DefineProfileCenter(3046.2270, 1277.3250, 148.8458, 300)
GMR.DefineProfileCenter(3012.0000, 1278.4430, 148.8458, 300)
GMR.DefineProfileCenter(3003.3270, 1268.6310, 148.8458, 300)
GMR.DefineProfileCenter(2983.4250, 1303.9570, 150.0670, 300)
GMR.DefineProfileCenter(2976.8510, 1366.2160, 149.6186, 300)
GMR.DefineProfileCenter(2962.2940, 1421.6290, 150.2748, 300)
GMR.DefineProfileCenter(3026.0020, 1463.4730, 149.7632, 300)
GMR.DefineProfileCenter(3020.2090, 1525.1670, 149.2742, 300)
GMR.DefineProfileCenter(3076.9710, 1471.0160, 149.2717, 300)
GMR.DefineProfileCenter(3082.1160, 1465.9060, 148.8629, 300)
GMR.DefineProfileCenter(3130.4120, 1394.3280, 148.8540, 300)
GMR.DefineProfileCenter(3127.3750, 1356.7640, 150.3780, 300)
GMR.DefineProfileCenter(3044.6880, 1275.4890, 148.8457, 300)
GMR.DefineProfileCenter(4171.4990, -1401.0460, 146.3831, 300)
GMR.DefineProfileCenter(4264.2940, -1418.4600, 149.9698, 300)
GMR.DefineProfileCenter(3443.3670, -1579.8260, 82.2602, 300)
GMR.DefineProfileCenter(3391.2480, -1575.7380, 82.3578, 300)
GMR.DefineProfileCenter(3090.3060, -1591.3710, 14.2746, 300)
GMR.DefineProfileCenter(3054.6780, -1583.0940, 15.1715, 300)
GMR.DefineProfileCenter(4008.9450, -2853.3180, 274.4042, 300)
GMR.DefineProfileCenter(4031.7700, -2884.4250, 276.2774, 300)
GMR.DefineProfileCenter(4058.9890, -2949.8440, 273.9180, 300)
GMR.DefineProfileCenter(4092.0100, -2944.2290, 276.3922, 300)
GMR.DefineProfileCenter(4135.4260, -2827.1620, 279.6393, 300)
GMR.DefineProfileCenter(4138.6370, -2862.0120, 279.8697, 300)
GMR.DefineProfileCenter(4187.9550, -2865.6250, 278.8019, 300)
GMR.DefineProfileCenter(4184.9600, -2911.3530, 280.5727, 300)
GMR.DefineProfileCenter(4252.6550, -2926.2500, 277.9511, 300)
GMR.DefineProfileCenter(4407.5760, -2990.5410, 308.9938, 300)
GMR.DefineProfileCenter(4409.1390, -2989.4990, 309.2656, 300)
GMR.DefineProfileCenter(4416.0540, -3013.9970, 308.6200, 300)
GMR.DefineProfileCenter(4411.5130, -3103.4830, 312.9208, 300)
GMR.DefineProfileCenter(4407.0290, -3140.2230, 309.6910, 300)
GMR.DefineProfileCenter(4432.1850, -3209.3140, 310.2703, 300)
GMR.DefineProfileCenter(4307.5040, -3317.1080, 309.2019, 300)
GMR.DefineProfileCenter(4269.0140, -3291.5500, 308.8979, 300)
GMR.DefineProfileCenter(4304.3150, -3235.3090, 307.9393, 300)
GMR.DefineProfileCenter(4304.1620, -3194.6000, 308.4596, 300)
GMR.DefineProfileCenter(4320.1140, -3127.2360, 307.3362, 300)
GMR.DefineProfileCenter(4177.2970, -3114.9920, 275.9969, 300)
GMR.DefineProfileCenter(4051.8770, -3107.9060, 274.0023, 300)
GMR.DefineProfileCenter(3985.9520, -2947.3870, 275.8232, 300)
GMR.DefineProfileCenter(5486.7410, 70.4300, 148.9666, 300)
GMR.DefineProfileCenter(5424.6560, 119.8286, 148.3750, 300)
GMR.DefineProfileCenter(5472.7420, 301.8727, 147.9650, 300)
GMR.DefineProfileCenter(5526.8910, 346.8297, 148.3191, 300)
GMR.DefineProfileCenter(5518.6800, 402.0509, 148.6419, 300)
GMR.DefineProfileCenter(5545.5730, 426.4706, 148.8943, 300)
GMR.DefineProfileCenter(5561.3960, 449.3590, 148.8459, 300)
GMR.DefineProfileCenter(5642.1210, 516.6893, 147.5986, 300)
GMR.DefineProfileCenter(5664.7640, 552.6688, 147.9444, 300)
GMR.DefineProfileCenter(5675.2900, 643.7007, 148.5190, 300)
GMR.DefineProfileCenter(5712.1760, 751.3743, 147.4439, 300)
GMR.DefineProfileCenter(5736.2080, 779.1025, 149.0898, 300)
GMR.DefineProfileCenter(5859.2720, 793.4253, 149.6082, 300)
GMR.DefineProfileCenter(2077.2620, -3231.1180, 56.2805, 300)
GMR.DefineProfileCenter(2059.1430, -3233.2760, 56.2806, 300)
GMR.DefineProfileCenter(2074.4070, -3437.0240, 138.3386, 300)
GMR.DefineProfileCenter(2214.4450, -3563.5780, 151.3878, 300)
GMR.DefineProfileCenter(2204.0700, -3625.8130, 161.8125, 300)
GMR.DefineProfileCenter(2203.4350, -3832.5920, 221.1000, 300)
GMR.DefineProfileCenter(2287.2920, -3805.8220, 218.6309, 300)
GMR.DefineProfileCenter(2329.9920, -3759.2540, 218.9863, 300)
GMR.DefineProfileCenter(2390.2370, -3810.0610, 218.9421, 300)
GMR.DefineProfileCenter(2408.3430, -3862.2370, 218.6545, 300)
GMR.DefineProfileCenter(2424.2870, -3996.3460, 225.3960, 300)
GMR.DefineProfileCenter(2395.2470, -4095.5030, 218.1759, 300)
GMR.DefineProfileCenter(2345.7620, -4200.1590, 218.3797, 300)
GMR.DefineProfileCenter(2239.8790, -4154.1760, 219.2735, 300)
GMR.DefineProfileCenter(2177.1180, -4072.1660, 219.5487, 300)
GMR.DefineProfileCenter(2180.6870, -3879.6220, 218.8590, 300)
GMR.DefineProfileCenter(2207.1150, -3834.8220, 220.7555, 300)
GMR.DefineProfileCenter(1598.6100, -3877.8750, 77.5649, 300)
GMR.DefineProfileCenter(1658.2730, -3852.4370, 76.6102, 300)
GMR.DefineProfileCenter(1635.0670, -3682.2430, 79.8964, 300)
GMR.DefineProfileCenter(1626.9710, -3634.4420, 80.3946, 300)
GMR.DefineProfileCenter(1493.6510, -4379.4380, 115.3733, 300)
GMR.DefineProfileCenter(1723.7560, -4812.7920, 115.9837, 300)
GMR.DefineProfileCenter(1671.5800, -5009.2460, 116.9893, 300)
GMR.DefineProfileCenter(1655.6000, -5014.9550, 117.5327, 300)
GMR.DefineProfileCenter(1552.8830, -5040.4320, 119.0859, 300)
GMR.DefineProfileCenter(1515.6690, -5054.5500, 119.1485, 300)
GMR.DefineProfileCenter(1484.1980, -5023.2060, 119.0118, 300)
GMR.DefineProfileCenter(1461.2730, -4989.4740, 116.5810, 300)
GMR.DefineProfileCenter(1486.5540, -4932.4260, 117.8286, 300)
GMR.DefineProfileCenter(933.2936, -5009.8480, -0.1625, 300)
GMR.DefineProfileCenter(843.0325, -5018.9340, 2.4551, 300)
GMR.DefineProfileCenter(680.1900, -5071.8010, 0.1460, 300)
GMR.DefineProfileCenter(665.9614, -5074.4790, 0.4630, 300)
GMR.DefineProfileCenter(822.1141, -5525.1200, 0.5957, 300)
GMR.DefineProfileCenter(841.5529, -5521.3100, -0.0030, 300)
GMR.DefineProfileCenter(5375.1920, 4831.6000, -198.6656, 300)
GMR.DefineProfileCenter(5383.5950, 4838.4500, -199.8268, 300)
GMR.DefineProfileCenter(5392.2740, 4867.9010, -199.6474, 300)
GMR.DefineProfileCenter(5456.1980, 4904.1940, -199.6310, 300)
GMR.DefineProfileCenter(5501.4260, 4870.7500, -197.3599, 300)
GMR.DefineProfileCenter(5521.8000, 4836.9180, -200.2841, 300)
GMR.DefineProfileCenter(5500.6680, 4776.3530, -199.9039, 300)
GMR.DefineProfileCenter(5405.8930, 4754.6660, -199.1722, 300)
GMR.DefineProfileCenter(5972.2300, 4914.2740, -100.4798, 300)
GMR.DefineProfileCenter(6098.8290, 5056.7680, -101.2331, 300)
GMR.DefineProfileCenter(6143.5010, 5142.1640, -100.9681, 300)
GMR.DefineProfileCenter(6202.1510, 5184.4490, -100.9533, 300)
GMR.DefineProfileCenter(6297.9250, 5226.1100, -100.1214, 300)
GMR.DefineProfileCenter(6103.2550, 5385.9790, -99.0378, 300)
GMR.DefineProfileCenter(6113.1720, 5305.5420, -100.4118, 300)
GMR.DefineProfileCenter(6087.3470, 5286.4260, -99.9901, 300)
GMR.DefineProfileCenter(5266.5060, 4819.8850, -137.3546, 300)
GMR.DefineProfileCenter(5211.8540, 4788.9060, -134.5159, 300)
GMR.DefineProfileCenter(5177.4350, 4863.0600, -128.1311, 300)
GMR.DefineProfileCenter(5116.3790, 4900.8800, -134.7375, 300)
GMR.DefineProfileCenter(5126.0320, 4969.7820, -135.2709, 300)
GMR.DefineProfileCenter(4934.3800, 4994.3210, -109.8104, 300)
GMR.DefineProfileCenter(4934.3890, 4893.4040, -103.2775, 300)
GMR.DefineProfileCenter(4774.2670, 4734.7700, -72.5888, 300)
GMR.DefineProfileCenter(4823.4960, 4672.0360, -67.1456, 300)
GMR.DefineProfileCenter(5426.7100, 3781.4760, -69.6209, 300)
GMR.DefineProfileCenter(5453.5540, 3749.7860, -69.6078, 300)
GMR.DefineProfileCenter(5616.2830, 4450.5020, -143.5649, 300)
GMR.DefineProfileCenter(5621.3200, 4385.0890, -143.2076, 300)
GMR.DefineProfileCenter(5676.9650, 4421.5430, -145.0256, 300)
GMR.DefineProfileCenter(5752.0760, 4305.8850, -113.4957, 300)
GMR.DefineProfileCenter(5911.0950, 4000.5920, -70.5520, 300)
GMR.DefineProfileCenter(5944.3210, 3974.1170, -71.3033, 300)
GMR.DefineProfileCenter(6042.2020, 3915.8970, -49.8667, 300)
GMR.DefineProfileCenter(5479.7280, 5018.0290, -130.2391, 300)
GMR.DefineProfileCenter(5534.5760, 5075.9170, -132.1886, 300)
GMR.DefineProfileCenter(5447.5030, 5272.3190, -131.8687, 300)
GMR.DefineProfileCenter(5477.6410, 5292.1530, -131.0916, 300)
GMR.DefineProfileCenter(5410.6020, 5549.1260, -101.2536, 300)
GMR.DefineProfileCenter(5488.9060, 5655.8380, -101.2908, 300)
GMR.DefineProfileCenter(5535.5120, 5829.8750, -73.0484, 300)
GMR.DefineProfileCenter(5529.2260, 5857.1630, -65.0230, 300)
GMR.DefineProfileCenter(5543.6910, 5929.3470, -49.6467, 300)
GMR.DefineProfileCenter(5538.3210, 5970.9490, -44.2954, 300)
GMR.DefineProfileCenter(5510.2580, 5280.9110, -131.9440, 300)
GMR.DefineProfileCenter(5534.3880, 5079.1320, -131.7941, 300)
GMR.DefineProfileCenter(5804.5960, 4852.0650, -135.5423, 300)
GMR.DefineProfileCenter(5857.7510, 4937.1000, -134.7802, 300)
GMR.DefineProfileCenter(5385.5040, 4843.4580, -200.0725, 300)
GMR.DefineProfileCenter(4851.9350, 2165.7380, 354.9546, 300)
GMR.DefineProfileCenter(4770.7970, 3579.1280, 352.3854, 300)
GMR.DefineProfileCenter(4127.9780, 2747.7860, 354.2231, 300)
GMR.DefineProfileCenter(4639.3910, 3367.8930, 352.3037, 300)
GMR.DefineProfileCenter(4767.9290, 3345.0140, 351.9432, 300)
GMR.DefineProfileCenter(4772.7120, 3477.0800, 352.3847, 300)
GMR.DefineProfileCenter(4744.2950, 3546.7860, 352.3353 , 300)
GMR.DefineQuestEnemyId(32429)
GMR.DefineQuestEnemyId(32422)
GMR.DefineQuestEnemyId(32438)
GMR.DefineQuestEnemyId(38453)
GMR.DefineQuestEnemyId(32398)
GMR.DefineQuestEnemyId(32386)
GMR.DefineQuestEnemyId(32377)
GMR.DefineQuestEnemyId(32361)
GMR.DefineQuestEnemyId(32357)
GMR.DefineQuestEnemyId(32358)
GMR.DefineQuestEnemyId(32481)
GMR.DefineQuestEnemyId(32517)
GMR.DefineQuestEnemyId(32485)
GMR.DefineQuestEnemyId(32471)
GMR.DefineQuestEnemyId(32447)
GMR.DefineQuestEnemyId(32475)
GMR.DefineQuestEnemyId(33776)
GMR.DefineQuestEnemyId(32409)
GMR.DefineQuestEnemyId(32417)
GMR.DefineQuestEnemyId(32400)
GMR.DefineSellVendor(5552.2600, 5738.8800, -76.2168, 28040)
GMR.DefineRepairVendor(5552.2600, 5738.8800, -76.2168, 28040)
GMR.DefineAmmoVendor(5552.2600, 5738.8800, -76.2168, 28040)
GMR.DefineGoodsVendor(5566.2500, 5763.7100, -75.2256, 28038)
GMR.DefineProfileMailbox(5563.2230, 5758.7600, -75.2246, 192952)
GMR.DefineVendorPath("Sell", 5764.7340, 5732.9110, -73.8948)
GMR.DefineVendorPath("Sell", 5739.5420, 5732.2840, -76.7182)
GMR.DefineVendorPath("Sell", 5707.8050, 5733.9940, -73.4065)
GMR.DefineVendorPath("Sell", 5686.6900, 5736.3100, -73.1946)
GMR.DefineVendorPath("Sell", 5666.0580, 5737.4290, -78.2361)
GMR.DefineVendorPath("Sell", 5640.4440, 5743.7550, -77.1276)
GMR.DefineVendorPath("Sell", 5621.4840, 5751.4250, -72.0742)
GMR.DefineVendorPath("Sell", 5599.7600, 5758.9910, -70.7285)
GMR.DefineVendorPath("Sell", 5585.6780, 5762.5120, -73.7420)
GMR.DefineVendorPath("Sell", 5575.8260, 5749.9750, -74.4472)
GMR.DefineVendorPath("Sell", 5562.3150, 5747.5540, -76.1395)
GMR.DefineVendorPath("Sell", 5552.2600, 5738.8800, -76.2168)
GMR.DefineVendorPath("Repair", 5772.3760, 5729.7530, -73.9964)
GMR.DefineVendorPath("Repair", 5751.7930, 5730.6890, -75.6165)
GMR.DefineVendorPath("Repair", 5731.5780, 5732.3610, -77.2305)
GMR.DefineVendorPath("Repair", 5709.7480, 5731.7350, -73.9982)
GMR.DefineVendorPath("Repair", 5684.7010, 5733.9760, -74.1573)
GMR.DefineVendorPath("Repair", 5661.9020, 5736.4440, -79.8900)
GMR.DefineVendorPath("Repair", 5638.1050, 5742.2340, -76.6222)
GMR.DefineVendorPath("Repair", 5614.6180, 5756.6370, -70.7531)
GMR.DefineVendorPath("Repair", 5599.6950, 5765.5260, -70.6530)
GMR.DefineVendorPath("Repair", 5587.7050, 5764.1360, -73.2244)
GMR.DefineVendorPath("Repair", 5579.0620, 5754.4020, -74.6074)
GMR.DefineVendorPath("Repair", 5568.1120, 5750.8780, -75.4142)
GMR.DefineVendorPath("Repair", 5556.8630, 5746.5690, -76.7714)
GMR.DefineVendorPath("Repair", 5552.2600, 5738.8800, -76.2167)
GMR.DefineVendorPath("Goods", 5761.6050, 5733.3130, -74.0509)
GMR.DefineVendorPath("Goods", 5740.5670, 5733.2900, -76.4054)
GMR.DefineVendorPath("Goods", 5719.4930, 5732.2580, -75.5851)
GMR.DefineVendorPath("Goods", 5696.4460, 5733.0590, -73.0466)
GMR.DefineVendorPath("Goods", 5676.8570, 5735.6730, -74.4405)
GMR.DefineVendorPath("Goods", 5653.9710, 5739.8710, -80.6919)
GMR.DefineVendorPath("Goods", 5635.4800, 5744.8330, -75.6734)
GMR.DefineVendorPath("Goods", 5617.4020, 5754.0460, -70.6337)
GMR.DefineVendorPath("Goods", 5599.2320, 5763.1250, -70.5692)
GMR.DefineVendorPath("Goods", 5584.9980, 5758.0820, -73.1284)
GMR.DefineVendorPath("Goods", 5572.0350, 5750.8020, -75.0492)
GMR.DefineVendorPath("Goods", 5570.0360, 5764.7860, -75.2255)
GMR.DefineVendorPath("Goods", 5566.2500, 5763.7100, -75.2241)
GMR.DefineVendorPath("Ammo", 5780.3370, 5733.6070, -72.8830)
GMR.DefineVendorPath("Ammo", 5760.8080, 5733.5780, -74.0437)
GMR.DefineVendorPath("Ammo", 5741.0260, 5733.5480, -76.2689)
GMR.DefineVendorPath("Ammo", 5720.9230, 5733.5180, -75.9009)
GMR.DefineVendorPath("Ammo", 5696.2070, 5735.3810, -72.7993)
GMR.DefineVendorPath("Ammo", 5676.8880, 5737.1570, -73.9497)
GMR.DefineVendorPath("Ammo", 5653.4010, 5740.8940, -80.5001)
GMR.DefineVendorPath("Ammo", 5635.7160, 5748.5160, -75.5427)
GMR.DefineVendorPath("Ammo", 5617.1270, 5757.7380, -70.5873)
GMR.DefineVendorPath("Ammo", 5597.7740, 5765.9780, -70.9113)
GMR.DefineVendorPath("Ammo", 5586.1270, 5760.3310, -73.1664)
GMR.DefineVendorPath("Ammo", 5577.1470, 5752.7710, -74.5826)
GMR.DefineVendorPath("Ammo", 5561.6870, 5747.9220, -76.2628)
GMR.DefineVendorPath("Ammo", 5552.2600, 5738.8800, -76.2167)
GMR.DefineMailboxPath(5781.7490, 5739.1030, -70.9794)
GMR.DefineMailboxPath(5764.7870, 5738.6410, -71.9165)
GMR.DefineMailboxPath(5747.6810, 5738.0910, -73.9312)
GMR.DefineMailboxPath(5728.3310, 5735.3630, -77.3052)
GMR.DefineMailboxPath(5711.0940, 5734.6740, -73.9030)
GMR.DefineMailboxPath(5690.3010, 5733.8420, -73.7204)
GMR.DefineMailboxPath(5665.2990, 5736.2290, -78.8767)
GMR.DefineMailboxPath(5649.3690, 5743.4780, -79.5962)
GMR.DefineMailboxPath(5627.6530, 5753.9180, -73.7506)
GMR.DefineMailboxPath(5610.1450, 5761.9870, -71.0874)
GMR.DefineMailboxPath(5593.7450, 5766.3800, -71.5141)
GMR.DefineMailboxPath(5584.8110, 5760.2620, -73.6766)
GMR.DefineMailboxPath(5574.1340, 5745.5550, -74.6733)
GMR.DefineMailboxPath(5562.9610, 5752.7580, -75.9423)
GMR.DefineMailboxPath(5563.2230, 5758.7600, -75.2246)