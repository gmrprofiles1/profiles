GMR.DefineQuester("Alliance 60-70 [Crypto]", function()




  local level = UnitLevel("player")
  local faction = UnitFactionGroup("player")
  local _, race = UnitRace("player")
  
  
  

  
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Grinding: Eastern Plaguelands by FP (Boosted Chars) - Level 60 |r",
    QuestType = "GrindTo",
    Level = 60,
    Faction = "Alliance",
    Profile = {
      'GMR.DefineProfileCenter(2119.6088867188, -5210.5859375, 82.588401794434, 70)',
      'GMR.DefineProfileCenter(2035.4390869141, -5235.208984375, 85.136413574219, 70)',
      'GMR.DefineProfileCenter(1972.1302490234, -5238.0395507813, 83.784072875977, 70)',
      'GMR.DefineProfileCenter(1986.0489501953, -5196.8823242188, 81.370414733887, 70)',
      'GMR.DefineProfileCenter(2039.9678955078, -5150.7153320313, 80.311614990234, 70)',
      'GMR.DefineProfileCenter(2031.2258300781, -5122.4838867188, 85.937728881836, 70)',
      'GMR.DefineProfileCenter(1958.6190185547, -5141.4418945313, 73.932083129883, 70)',
      'GMR.DefineProfileCenter(1948.5581054688, -5179.7348632813, 74.965171813965, 70)',
      'GMR.DefineProfileCenter(1940.8375244141, -5127.37109375, 75.328727722168, 70)',
      'GMR.DefineProfileCenter(1946.2587890625, -5090.7607421875, 87.274040222168, 70)',
      'GMR.DefineProfileCenter(1939.2185058594, -5028.990234375, 75.092483520508, 70)',
      'GMR.DefineProfileCenter(1927.8046875, -5003.580078125, 74.693321228027, 70)',
      'GMR.DefineProfileCenter(1927.3243408203, -4997.2045898438, 74.736633300781, 70)',
      'GMR.DefineProfileCenter(1895.0323486328, -4971.0815429688, 78.27254486084, 70)',
      'GMR.DefineProfileCenter(1846.0034179688, -4918.361328125, 77.506011962891, 70)',
      'GMR.DefineProfileCenter(1879.9016113281, -4882.7490234375, 97.553520202637, 70)',
      'GMR.DefineProfileCenter(1882.2431640625, -4848.330078125, 107.10688781738, 70)',
      'GMR.DefineProfileCenter(1935.361328125, -4848.2192382813, 99.967193603516, 70)',
      'GMR.DefineProfileCenter(1991.3726806641, -4867.0786132813, 83.901321411133, 70)',
      'GMR.DefineProfileCenter(1978.1951904297, -4962.232421875, 74.390686035156, 70)',
      'GMR.DefineProfileCenter(1937.62890625, -4905.412109375, 86.822212219238, 70)',
      'GMR.DefineProfileCenter(1978.7033691406, -4826.435546875, 93.820999145508, 70)',
      'GMR.DefineProfileCenter(2024.2412109375, -4799.3818359375, 81.088668823242, 70)',
      'GMR.DefineProfileCenter(1990.5372314453, -4756.7412109375, 95.319190979004, 70)',
      'GMR.DefineProfileCenter(2009.9151611328, -4714.7387695313, 95.777793884277, 70)',
      'GMR.DefineProfileCenter(2075.369140625, -4714.0717773438, 75.729347229004, 70)',
      'GMR.DefineProfileCenter(2148.109375, -4723.1450195313, 73.612571716309, 70)',
      'GMR.DefineProfileCenter(2168.3564453125, -4772.0561523438, 86.535789489746, 70)',
      'GMR.DefineProfileCenter(2236.265625, -4859.7475585938, 90.588859558105, 70)',
      'GMR.DefineProfileCenter(2214.7377929688, -4907.9702148438, 78.653465270996, 70)',
      'GMR.DefineProfileCenter(2170.8601074219, -4928.4833984375, 85.711158752441, 70)',
      'GMR.DefineProfileCenter(2110.5227050781, -4909.7309570313, 77.577735900879, 70)',
      'GMR.DefineProfileCenter(2116.3625488281, -4887.9790039063, 76.389991760254, 70)',
      'GMR.DefineProfileCenter(2111.6125488281, -4855.9233398438, 74.318000793457, 70)',
      'GMR.DefineProfileCenter(2052.1755371094, -4854.529296875, 74.171577453613, 70)',
      'GMR.DefineProfileCenter(1984.7437744141, -4891.806640625, 77.716384887695, 70)',
      'GMR.DefineProfileCenter(1932.6904296875, -4894.7612304688, 89.149696350098, 70)',
      'GMR.DefineProfileCenter(1897.9604492188, -4979.6577148438, 79.090682983398, 70)',
      'GMR.DefineProfileCenter(1890.9388427734, -5069.7836914063, 83.617126464844, 70)',
      'GMR.DefineSellVendor(2151.2978, -5263.596, 95.682304, 11038)',
      'GMR.DefineRepairVendor(949.88458, -1435.649, 62.589923, 12942)',
      'GMR.DefineGoodsVendor(2151.2978, -5263.596, 95.682304, 11038)',
      'GMR.DefineAmmoVendor(2151.2978, -5263.596, 95.682304, 11038)',
      'GMR.DefineProfileMailbox(2288.603, -5317.84, 88.82285)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Through the Dark Portal |r",
    QuestID = 10119,
    QuestType = "Custom",
    PickUp = { x = -11815.200195, y = -3195.540039, z = -30.9174, id = 16841 },
    TurnIn = { x = -266.36865234375, y = 1028.8601074219, z = 54.32336807251, id = 19229 },
    Faction = "Alliance",
    Profile = {
      [[
          if not GMR.Frames.Rand1 then
        GMR.Print("Frame created")
        GMR.Frames.Rand1 = CreateFrame("frame")
        GMR.Frames.Rand1:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10119 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10119) then
              if C_Map.GetBestMapForUnit('player') == 1419 then
                if not GMR.IsPlayerPosition(-11907.986328125, -3208.4077148438, -14.831151008606, 1) then 
                  GMR.MeshTo(-11907.986328125, -3208.4077148438, -14.831151008606)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand1 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineProfileCenter(-11906.90234375, -3208.353515625, -14.860760688782, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.AllowSpeedUp()',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Arrival in Outland |r",
    QuestID = 10288,
    QuestType = "TalkTo",
    PickUp = { x = -266.368988, y = 1028.859985, z = 54.4067, id = 19229 },
    TurnIn = { x = -323.809998, y = 1027.609985, z = 54.239899, id = 18931 },
    Faction = { "Alliance" },
    PreQuest = 10119,
    Profile = {
      'GMR.DefineProfileCenter(-323.809998, 1027.609985, 54.239899,120)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Journey to Honor Hold |r",
    QuestID = 10140,
    QuestType = "Custom",
    PickUp = { x = -323.809998, y = 1027.609985, z = 54.239899, id = 18931 },
    TurnIn = { x = -680.833435, y = 2717.996826, z = 94.83432, id = 19308 },
    Faction = { "Alliance" },
    PreQuest = 10288,
    Profile = {
      [[
    if not GMR.Frames.Rand002 then
      GMR.Print("Frame created")
      GMR.Frames.Rand002 = CreateFrame("frame")
      GMR.Frames.Rand002:SetScript("OnUpdate", function(self)
        if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10140 then
          if GMR.IsExecuting() and GMR.IsQuestActive(10140) then
            if GMR.IsPlayerPosition(-322.00305175781, 1026.1351318359, 54.173706054688, 150) then
              local npc1 = GMR.GetObjectWithInfo({ id = 18931, rawType = 5, isAlive = true })
              if npc1 then
                TakeTaxiNode(1)
                GossipTitleButton1:Click()
                GMR.Questing.GossipWith(npc1, nil, nil, nil, nil, 5)
              end
            end
          end
        else 
          self:SetScript("OnUpdate", nil); GMR.Frames.Rand002 = nil; GMR.Print("Frame deleted")
        end
      end)
    end
      ]],
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.DefineProfileCenter(-322.76049804688, 1027.2615966797, 54.160144805908,3)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineQuestEnemyId(18931)',
      'GMR.DefineCustomObjectId(18931)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Force Commander Danath |r",
    QuestID = 10254,
    QuestType = "TalkTo",
    PickUp = { x = -680.833435, y = 2717.996826, z = 94.83432, id = 19308 },
    TurnIn = { x = -815.609009, y = 2614.199951, z = 124.472, id = 16819 },
    Faction = "Alliance",
    PreQuest = 10140,
    Profile = {
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Honor Hold - Initial Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10141, x = -815.609009, y = 2614.199951, z = 124.472, id = 16819 },
      { questId = 10160, x = -815.609009, y = 2614.199951, z = 124.472, id = 16819 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.AllowSpeedUp()',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Legion Reborn |r",
    QuestID = 10141,
    QuestType = "TalkTo",
    PickUp = { x = -815.609009, y = 2614.199951, z = 124.472, id = 16819 },
    TurnIn = { x = -616.502014, y = 2352.870117, z = 36.612099, id = 19309 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Path of Anguish |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10142, x = -616.502014, y = 2352.870117, z = 36.612099, id = 19309 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Know Your Enemy |r",
    QuestID = 10160,
    QuestType = "TalkTo",
    PickUp = { x = -815.609009, y = 2614.199951, z = 124.472, id = 16819 },
    TurnIn = { x = -591.54303, y = 2910.219971, z = 59.295601, id = 16820 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Fel Orc Scavengers / Waste Not Want Not |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10482, x = -591.54296875, y = 2910.2238769531, z = 59.212322235107, id = 16820 },
      { questId = 10055, x = -595.8095703125, y = 2890.7438964844, z = 59.204360961914, id = 21209 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Grinding: Hellfire Penninsula Honor Hold - Level 62 |r",
    QuestType = "GrindTo",
    Level = 62,
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineProfileCenter(-935.80682373047, 2838.3701171875, 2.059051990509, 80)',
      'GMR.DefineProfileCenter(-966.66711425781, 2901.279296875, 3.4042391777039, 80)',
      'GMR.DefineProfileCenter(-1047.4464111328, 2901.9138183594, -0.21641859412193, 80)',
      'GMR.DefineProfileCenter(-1116.0823974609, 2922.9418945312, -1.7161306142807, 80)',
      'GMR.DefineProfileCenter(-1207.2751464844, 2944.0224609375, 3.338522195816, 80)',
      'GMR.DefineProfileCenter(-1320.6322021484, 3050.0947265625, 18.821342468262, 80)',
      'GMR.DefineProfileCenter(-1247.7850341797, 3063.890625, 27.60634803772, 80)',
      'GMR.DefineProfileCenter(-1181.7453613281, 3046.9816894531, 21.282604217529, 80)',
      'GMR.DefineProfileCenter(-1056.4880371094, 3065.1096191406, 22.212581634521, 80)',
      'GMR.DefineProfileCenter(-978.30834960938, 3134.1440429688, 28.177421569824, 80)',
      'GMR.DefineProfileCenter(-894.49450683594, 3133.9604492188, 16.268154144287, 80)',
      'GMR.DefineProfileCenter(-981.5576171875, 3217.5559082031, 43.758411407471, 80)',
      'GMR.DefineProfileCenter(-838.25982666016, 3120.2163085938, 9.1887102127075, 80)',
      'GMR.DefineProfileCenter(-792.01531982422, 3113.1906738281, -10.578170776367, 80)',
      'GMR.DefineProfileCenter(-769.82019042969, 3058.5942382812, 8.7652645111084, 80)',
      'GMR.DefineProfileCenter(-751.80700683594, 2993.7260742188, 15.161187171936, 80)',
      'GMR.DefineProfileCenter(-705.13269042969, 3022.1428222656, 13.724836349487, 80)',
      'GMR.DefineProfileCenter(-692.60662841797, 3046.0207519531, 8.9818143844604, 80)',
      'GMR.DefineProfileCenter(-707.70098876953, 2953.2109375, 29.843919754028, 80)',
      'GMR.DefineProfileCenter(-754.94549560547, 2914.9479980469, 21.647886276245, 80)',
      'GMR.DefineProfileCenter(-811.88421630859, 2922.3039550781, 11.908421516418, 80)',
      'GMR.DefineProfileCenter(-897.513671875, 2977.9672851562, 10.475419998169, 80)',
      'GMR.DefineProfileCenter(-874.69512939453, 2903.4438476562, 7.400806427002, 80)',
      'GMR.DefineProfileCenter(-883.58746337891, 2858.8420410156, 9.3059015274048, 80)',
      'GMR.DefineProfileCenter(-948.64599609375, 2802.8083496094, 3.870044708252, 80)',
      'GMR.DefineQuestEnemyId(16863)',
      'GMR.DefineQuestEnemyId(16857)',
      'GMR.BlacklistId(19414)',
      'GMR.BlacklistId(19295)',
      'GMR.BlacklistId(16878)',
      'GMR.BlacklistId(19413)',
      'GMR.BlacklistId(19515)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Waste Not, Want Not |r",
    QuestID = 10055,
    QuestType = "Custom",
    PickUp = { x = -595.426025, y = 2899.23999, z = 59.205399, id = 21209 },
    TurnIn = { x = -595.426025, y = 2899.23999, z = 59.205399, id = 21209 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10055, 1) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = { 182798, 182797 }, rawType = 8, isInteractable = true })
				if not GMR.GetDelay("CustomQuest") and object then
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 2)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10055, 2) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = 182799, rawType = 8, isInteractable = true })
				if not GMR.GetDelay("CustomQuest") and object then
					GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 2)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-380.0488, 2753.134, 27.3257, 120)',
      'GMR.DefineProfileCenter(-346.6085, 2587.622, 46.74556, 120)',
      'GMR.DefineProfileCenter(-284.8584, 2466.682, 41.06714, 120)',
      'GMR.DefineProfileCenter(-322.2322, 2404.393, 44.12713, 120)',
      'GMR.DefineProfileCenter(-275.5236, 2345.813, 53.29506, 120)',
      'GMR.DefineProfileCenter(-359.4168, 2324.586, 48.19233, 120)',
      'GMR.DefineProfileCenter(-280.4356, 2252.611, 63.6048, 120)',
      'GMR.DefineProfileCenter(-167.0144, 2339.49, 60.44089, 120)',
      'GMR.DefineProfileCenter(-316.9514, 2514.145, 41.72683, 120)',
      'GMR.DefineProfileCenter(-347.8003, 2584.961, 47.02792, 120)',
      'GMR.DefineProfileCenter(-366.4443, 2675.661, 40.94158, 120)',
      'GMR.DefineProfileCenter(-364.1339, 2740.013, 26.15858, 120)',
      'GMR.DefineProfileCenter(-488.2604, 2888.509, 34.15775, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(182798)',
      'GMR.DefineQuestEnemyId(182797)',
      'GMR.DefineQuestEnemyId(182799)',
      'GMR.DefineCustomObjectId(182798)',
      'GMR.DefineCustomObjectId(182797)',
      'GMR.DefineCustomObjectId(182799)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Laying Waste to the Unwanted |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10078, x = -595.426025, y = 2899.23999, z = 59.205399, id = 21209 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Laying Waste to the Unwanted |r",
    QuestID = 10078,
    QuestType = "Custom",
    PickUp = { x = -595.426025, y = 2899.23999, z = 59.205399, id = 21209 },
    TurnIn = { x = -595.426025, y = 2899.23999, z = 59.205399, id = 21209 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10078, 1) and not GMR.InCombat() then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-158.4289, 2522.761, 43.08107, 5) then 
              GMR.MeshTo(-158.4289, 2522.761, 43.08107)
          else 
              local itemName = GetItemInfo(26002)
              GMR.Use(itemName)
          end
      elseif not GMR.Questing.IsObjectiveCompleted(10078, 2) and not GMR.InCombat() then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-137.7705, 2651.053, 47.41739, 5) then 
              GMR.MeshTo(-137.7705, 2651.053, 47.41739)
          else 
              local itemName = GetItemInfo(26002)
              GMR.Use(itemName)
          end
      elseif not GMR.Questing.IsObjectiveCompleted(10078, 3) and not GMR.InCombat() then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-167.6738, 2769.229, 34.97382, 5) then 
              GMR.MeshTo(-167.6738, 2769.229, 34.97382)
          else 
              local itemName = GetItemInfo(26002)
              GMR.Use(itemName)
          end
      elseif not GMR.Questing.IsObjectiveCompleted(10078, 4) and not GMR.InCombat() then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-155.58799743652, 2806.8696289062, 33.954250335693, 5) then 
              GMR.MeshTo(-155.58799743652, 2806.8696289062, 33.954250335693)
          else 
              local itemName = GetItemInfo(26002)
              GMR.Use(itemName)
          end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Fel Orc Scavengers |r",
    QuestID = 10482,
    QuestType = "Custom",
    PickUp = { x = -591.54303, y = 2910.219971, z = 59.295601, id = 16820 },
    TurnIn = { x = -591.54303, y = 2910.219971, z = 59.295601, id = 16820 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10482, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-466.4889, 2907.433, 27.54113, 60)',
      'GMR.DefineProfileCenter(-494.8383, 2894.607, 34.02743, 60)',
      'GMR.DefineProfileCenter(-459.4119, 2914.18, 25.09476, 60)',
      'GMR.DefineProfileCenter(-450.9622, 2949.096, 15.25964, 60)',
      'GMR.DefineProfileCenter(-423.9727, 2894.863, 20.44556, 60)',
      'GMR.DefineProfileCenter(-379.9469, 2722.711, 32.88358, 60)',
      'GMR.DefineProfileCenter(-327.593, 2722.686, 24.21149, 60)',
      'GMR.DefineProfileCenter(-352.6655, 2609.725, 46.35963, 60)',
      'GMR.DefineProfileCenter(-373.1852, 2315.256, 48.13853, 60)',
      'GMR.DefineProfileCenter(-323.3282, 2414.492, 43.24156, 60)',
      'GMR.DefineProfileCenter(-300.9901, 2353.234, 49.75561, 60)',
      'GMR.DefineProfileCenter(-283.098, 2264.5, 62.06885, 60)',
      'GMR.DefineProfileCenter(-294.6266, 2325.743, 52.31622, 60)',
      'GMR.DefineProfileCenter(-291.4462, 2371.714, 49.00684, 60)',
      'GMR.DefineProfileCenter(-323.856, 2427.735, 42.02336, 60)',
      'GMR.DefineProfileCenter(-334.2835, 2512.671, 41.05806, 60)',
      'GMR.DefineProfileCenter(-343.4322, 2572.307, 46.98271, 60)',
      'GMR.DefineProfileCenter(-373.4251, 2668.884, 43.44143, 60)',
      'GMR.DefineProfileCenter(-483.7918, 2879.551, 34.92595, 60)',
      'GMR.DefineAreaBlacklist(-302.0939, 2531.107, 41.86152, 15)',
      'GMR.BlacklistId(16870)',
      'GMR.BlacklistId(19411)',
      'GMR.BlacklistId(19410)',
      'GMR.BlacklistId(168678)',
      'GMR.BlacklistId(16878)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(16876)',
      'GMR.DefineQuestEnemyId(18952)',
      'GMR.DefineQuestEnemyId(16925)',
      'GMR.DefineQuestEnemyId(19701)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Ill Omens |r",
    QuestID = 10483,
    QuestType = "Custom",
    PickUp = { x = -591.54303, y = 2910.219971, z = 59.295601, id = 16820 },
    TurnIn = { x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10483, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1060.2429199219, 2470.2158203125, 16.760665893555, 100)',
      'GMR.DefineProfileCenter(-1149.869140625, 2429.4694824219, 33.765823364258, 25)',
      'GMR.DefineProfileCenter(-1108.6304931641, 2347.1247558594, 24.651287078857, 100)',
      'GMR.DefineProfileCenter(-1070.0222167969, 2286.5339355469, 24.932203292847, 100)',
      'GMR.DefineProfileCenter(-980.576171875, 2224.3056640625, 9.2066278457642, 100)',
      'GMR.DefineProfileCenter(-922.92596435547, 2181.078125, 11.655283927917, 100)',
      'GMR.DefineProfileCenter(-893.03900146484, 2166.1608886719, 11.374631881714, 100)',
      'GMR.DefineProfileCenter(-827.00018310547, 2060.1000976562, 31.531047821045, 100)',
      'GMR.DefineProfileCenter(-918.68334960938, 2022.3254394531, 60.166728973389, 100)',
      'GMR.DefineProfileCenter(-985.54406738281, 2040.3059082031, 67.609039306641, 100)',
      'GMR.DefineProfileCenter(-1041.3947753906, 2098.2590332031, 61.205711364746, 100)',
      'GMR.DefineProfileCenter(-1062.8898925781, 2254.0883789062, 24.750995635986, 100)',
      'GMR.BlacklistId(16870)',
      'GMR.BlacklistId(19411)',
      'GMR.BlacklistId(19410)',
      'GMR.BlacklistId(16878)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(16907)',
      'GMR.DefineQuestEnemyId(16871)',
      'GMR.DefineQuestEnemyId(19424)',
      'GMR.DefineQuestEnemyId(19422)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Cursed Talismans |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10484, x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Path of Anguish |r",
    QuestID = 10142,
    QuestType = "Custom",
    PickUp = { x = -616.502014, y = 2352.870117, z = 36.612099, id = 19309 },
    TurnIn = { x = -616.502014, y = 2352.870117, z = 36.612099, id = 19309 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10142, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 19434, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10142, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 19136, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10142, 3) then
        local enemy = GMR.GetObjectWithInfo({ id = 19261, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-565.4496, 2025.425, 95.40997, 120)',
      'GMR.DefineProfileCenter(-564.6198, 2130.499, 85.56831, 120)',
      'GMR.DefineProfileCenter(-496.8573, 2139.903, 72.37421, 120)',
      'GMR.DefineProfileCenter(-422.7042, 2117.753, 87.30258, 120)',
      'GMR.DefineProfileCenter(-446.8613, 2025.432, 92.1493, 120)',
      'GMR.DefineProfileCenter(-525.1763, 1978.812, 82.55491, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(19434)',
      'GMR.DefineQuestEnemyId(19261)',
      'GMR.DefineQuestEnemyId(19136)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Expedition Point |r",
    QuestID = 10143,
    QuestType = "TalkTo",
    PickUp = { x = -616.502014, y = 2352.870117, z = 36.612099, id = 19309 },
    TurnIn = { x = -680.346008, y = 1855.050049, z = 66.922501, id = 19310 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineRepairVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineGoodsVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineAmmoVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Disrupt Their Reinforcements |r",
    QuestID = 10144,
    QuestType = "Custom",
    PickUp = { x = -680.346008, y = 1855.050049, z = 66.922501, id = 19310 },
    TurnIn = { x = -680.346008, y = 1855.050049, z = 66.922501, id = 19310 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10144, 1) and GetItemCount(28513) >= 4 then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-417.67135620117, 1859.1119384766, 84.439651489258, 3) then 
          GMR.MeshTo(-417.67135620117, 1859.1119384766, 84.439651489258)
        else
          local npc1 = GMR.GetObjectWithInfo({ id = 184414, rawType = 8 })
          if npc1 then
            GMR.Interact(npc1)
          end
        end
      elseif GMR.Questing.IsObjectiveCompleted(10144, 1) and not GMR.Questing.IsObjectiveCompleted(10144, 2) and GetItemCount(28513) >= 4 then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-534.3427734375, 1789.4445800781, 56.546459197998, 5) then 
          GMR.MeshTo(-534.3427734375, 1789.4445800781, 56.546459197998)
        else
          local npc2 = GMR.GetObjectWithInfo({ id = 184415, rawType = 8 })
          if npc2 then
            GMR.Interact(npc2)
          end
        end
      else
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-577.3325, 1844.913, 72.07056,130)',
      'GMR.DefineProfileCenter(-475.8587, 1785.178, 49.39034,130)',
      'GMR.DefineProfileCenter(-341.1373, 1770.048, 66.01037,130)',
      'GMR.DefineProfileCenter(-392.3997, 1860.435, 89.04066,130)',
      'GMR.DefineProfileCenter(-506.6153, 1870.118, 77.36893,130)',
      'GMR.DefineProfileCenter(-540.7584, 1812.516, 62.94191,130)',
      'GMR.BlacklistId(18977)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Skinning", "CustomObjects" })',
      'GMR.DefineSellVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineRepairVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineGoodsVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineAmmoVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(19190)',
      'GMR.DefineQuestEnemyId(16950)',
      'GMR.DefineQuestEnemyId(18975)',
      'GMR.DefineQuestEnemyId(18981)',
      'GMR.DefineQuestEnemyId(19282)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Mission: Gateways Murketh and Shadraz |r",
    QuestID = 10146,
    QuestType = "Custom",
    PickUp = { x = -680.346008, y = 1855.050049, z = 66.922501, id = 19310 },
    TurnIn = { x = -680.346008, y = 1855.050049, z = 66.922501, id = 19310 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand34004 then
        GMR.Print("Frame created")
        GMR.Frames.Rand34004 = CreateFrame("frame")
        GMR.Frames.Rand34004:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10146 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10146) then
              if StaticPopup1:IsShown() then
                StaticPopup1Button2:Click()
              elseif not UnitOnTaxi("player") and GMR.IsQuestActive(10146) then
                if not GMR.Questing.IsObjectiveCompleted(10146, 1) then
                  GMR.SetQuestingState(nil);
                  GossipTitleButton1:Click()
                  GMR.Questing.GossipWith(-670.427002, 1851.839966, 66.922501, 19409)
                elseif GMR.Questing.IsObjectiveCompleted(10146, 1) and not GMR.Questing.IsObjectiveCompleted(10146, 2) then
                  GMR.SetQuestingState(nil);
                  GossipTitleButton1:Click()
                  GMR.Questing.GossipWith(-670.427002, 1851.839966, 66.922501, 19409)
                end
  
              elseif UnitOnTaxi("player") then
                local itemName = GetItemInfo(28038)
                if not GMR.Questing.IsObjectiveCompleted(10146, 2) and GMR.IsPlayerPosition(-302.81112670898, 1525.1038818359, 37.766208648682, 90) then
                  GMR.Use(itemName)
                  local x1, y1, z1 = -302.81112670898, 1525.1038818359, 37.766208648682
                  GMR.ClickPosition(x1, y1, z1)
                elseif not GMR.Questing.IsObjectiveCompleted(10146, 1) and GMR.Questing.IsObjectiveCompleted(10146, 2) 
                and GMR.IsPlayerPosition(-146.99546813965, 1513.1086425781, 33.624019622803, 80) then
                  GMR.Use(itemName)
                  local x1, y1, z1 = -146.99546813965, 1513.1086425781, 33.624019622803
                  GMR.ClickPosition(x1, y1, z1)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand34004 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineRepairVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineGoodsVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineAmmoVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Shatter Point |r",
    QuestID = 10340,
    QuestType = "Custom",
    PickUp = { x = -680.346008, y = 1855.050049, z = 66.922501, id = 19310 },
    TurnIn = { x = 279.397003, y = 1489.76001, z = -15.4411, id = 20234 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.IsPlayerPosition(274.103, 1485.097, -15.28971, 200) then
          if and GMR.IsQuestActive(10340) then
              GMR.SetQuestingState(nil);
              GMR.RunMacroText("/run SelectGossipOption(1)")
              GMR.Questing.GossipWith(-670.427002, 1851.839966, 66.922501, 19409)
          end
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand005 then
        GMR.Print("Frame created")
        GMR.Frames.Rand005 = CreateFrame("frame")
        GMR.Frames.Rand005:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10340 then
            if GMR.IsExecuting() and not UnitOnTaxi("player") and GMR.IsQuestActive(10340) then
              if not GMR.IsPlayerPosition(274.103, 1485.097, -15.28971, 200) then
                GMR.RunMacroText("/run SelectGossipOption(1)")
                GMR.Questing.GossipWith(-670.427002, 1851.839966, 66.922501, 19409)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand005 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-577.3325, 1844.913, 72.07056,130)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineRepairVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineGoodsVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineAmmoVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Wing Commander Gryphngar |r",
    QuestID = 10344,
    QuestType = "TalkTo",
    PickUp = { x = 279.397003, y = 1489.76001, z = -15.4411, id = 20234 },
    TurnIn = { x = 315.337006, y = 1442.099976, z = 0.981787, id = 20232 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineRepairVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineGoodsVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineAmmoVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Mission: The Abyssal Shelf |r",
    QuestID = 10163,
    QuestType = "Custom",
    PickUp = { x = 315.337006, y = 1442.099976, z = 0.981787, id = 20232 },
    TurnIn = { x = 315.337006, y = 1442.099976, z = 0.981787, id = 20232 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand005 then
        GMR.Print("Frame created")
        GMR.Frames.Rand005 = CreateFrame("frame")
        GMR.Frames.Rand005:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10163 then
            if GMR.IsExecuting() then
              if StaticPopup1:IsShown() then
                StaticPopup1Button2:Click()
              
              elseif not UnitOnTaxi("player") and GMR.IsQuestActive(10163) then
                if GMR.IsPlayerPosition(294.68835449219, 1498.0620117188, -14.598951339722, 150) then
                  if not GMR.Questing.IsObjectiveCompleted(10163, 1) then
                    GossipTitleButton2:Click()
                    GMR.Questing.GossipWith(294.68835449219, 1498.0620117188, -14.598951339722, 20235)
                  elseif GMR.Questing.IsObjectiveCompleted(10163, 1) and not GMR.Questing.IsObjectiveCompleted(10163, 2) then
                    GossipTitleButton2:Click()
                    GMR.Questing.GossipWith(294.68835449219, 1498.0620117188, -14.598951339722, 20235)
                  elseif GMR.Questing.IsObjectiveCompleted(10163, 1) and GMR.Questing.IsObjectiveCompleted(10163, 2) and not GMR.Questing.IsObjectiveCompleted(10163, 3) then
                    GossipTitleButton2:Click()
                    GMR.Questing.GossipWith(294.68835449219, 1498.0620117188, -14.598951339722, 20235)
                  end
                end
              elseif UnitOnTaxi("player") then
                local itemName = GetItemInfo(28132)
                if UnitExists("target") and GMR.GetDistanceBetweenObjects("player", "target") < 80 then
                  GMR.Use(itemName)
                  local x1, y1, z1 = GMR.ObjectPosition("target")
                  GMR.ClickPosition(x1, y1, z1)
                elseif UnitExists("target") and GMR.GetDistanceBetweenObjects("player", "target") > 90 then
                  C_Timer.After(1.1, function() print("|cFF1E90FE CryptoGMR |r Target is to far. Clearing Target.") end)
                  GMR.RunMacroText("/cleartarget")
                  GMR.Mount()
                elseif not GMR.Questing.IsObjectiveCompleted(10163, 1) then
                  local npc1 = GMR.GetObjectWithInfo({ id = 19398, rawType = 5 })
                  if npc1 then
                    GMR.TargetUnit(npc1)
                  end
                elseif not GMR.Questing.IsObjectiveCompleted(10163, 2) then
                  local npc1 = GMR.GetObjectWithInfo({ id = 19397, rawType = 5 })
                  if npc1 then
                    GMR.TargetUnit(npc1)
                  end
                elseif not GMR.Questing.IsObjectiveCompleted(10163, 3) then
                  local npc1 = GMR.GetObjectWithInfo({ id = 19399, rawType = 5 })
                  if npc1 then
                    GMR.TargetUnit(npc1)
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand005 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineProfileCenter(-24.103500, 2125.860107, 112.693001,130)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(179.787003, 2605.399902, 87.366898, 22225)',
      'GMR.DefineRepairVendor(179.787003, 2605.399902, 87.366898, 22225)',
      'GMR.DefineGoodsVendor(190.876007, 2610.929932, 87.366898, 16602)',
      'GMR.DefineAmmoVendor(190.876007, 2610.929932, 87.366898, 16602)',
      'GMR.DefineProfileMailbox(172.493149, 2623.889648, 87.104507, 181381)',
      'GMR.DefineQuestEnemyId(19398)',
      'GMR.DefineQuestEnemyId(19397)',
      'GMR.DefineQuestEnemyId(19399)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Go to the Front |r",
    QuestID = 10382,
    QuestType = "Custom",
    PickUp = { x = 315.337006, y = 1442.099976, z = 0.981787, id = 20232 },
    TurnIn = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10382, 1) and GMR.IsPlayerPosition(293.331024, 1465.033936, -8.438472, 80) then
        GMR.SetQuestingState(nil);
        GMR.RunMacroText("/run SelectGossipOption(1)")
        GMR.Questing.GossipWith(294.687988, 1498.060059, -14.472900, 20235)
      elseif GMR.IsPlayerPosition(293.331024, 1465.033936, -8.438472, 80) then
        GMR.SetQuestingState(nil);
        GMR.RunMacroText("/run SelectGossipOption(1)")
        GMR.Questing.GossipWith(294.687988, 1498.060059, -14.472900, 20235)
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand007 then
        GMR.Print("Frame created")
        GMR.Frames.Rand007 = CreateFrame("frame")
        GMR.Frames.Rand007:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10382 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10382) then
              if GMR.IsPlayerPosition(293.331024, 1465.033936, -8.438472, 60) then
                GMR.RunMacroText("/run SelectGossipOption(1)")
                GMR.Questing.GossipWith(294.687988, 1498.060059, -14.472900, 20235)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand007 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineProfileCenter(294.687988, 1498.060059, -14.472900,130)',
      'GMR.BlacklistId(19400)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Disruption - Forge Camp: Mageddon |r",
    QuestID = 10394,
    QuestType = "Custom",
    PickUp = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    TurnIn = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10394, 1) then
        GMR.SetQuestingState("Idle")
        
      elseif not GMR.Questing.IsObjectiveCompleted(10394, 2) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(382.348999, 2191.939941, 118.392998, 45) then 
                  GMR.MeshTo(382.348999, 2191.939941, 118.392998)
              else 
          local object = GMR.GetObjectWithInfo({ id = 20798, rawType = 5 })
                  if object then 
                      GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
                  end 
              end
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand06 then
        GMR.Print("Frame created")
        GMR.Frames.Rand06 = CreateFrame("frame")
        GMR.Frames.Rand06:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10394 then
            if GMR.IsExecuting() and GetItemCount(29590) >= 1 and not GMR.IsQuestActive(10395) then
              local itemName = GetItemInfo(29590)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() then
                QuestFrameAcceptButton:Click()
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand06 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(407.4813, 2247.379, 115.8862,100)',
      'GMR.DefineProfileCenter(381.3682, 2192.309, 118.3161,100)',
      'GMR.DefineProfileCenter(387.4156, 2121.266, 116.5944,100)',
      'GMR.DefineProfileCenter(468.8453, 2123.454, 118.878,100)',
      'GMR.DefineProfileCenter(350.383, 2468.361, 159.9869,50)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(16947)',
      'GMR.DefineQuestEnemyId(20798)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Enemy of My Enemy... |r",
    QuestID = 10396,
    QuestType = "Custom",
    PickUp = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    TurnIn = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10396, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(415.923370, 2257.349854, 116.518715, 80)',
      'GMR.DefineProfileCenter(411.816986, 2259.340088, 116.848000, 80)',
      'GMR.DefineProfileCenter(333.877869, 2227.980469, 118.074883, 80)',
      'GMR.DefineProfileCenter(361.027802, 2147.812988, 119.632103, 80)',
      'GMR.DefineProfileCenter(346.234192, 2497.704590, 160.096786, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(22461)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Invasion Point: Annihilation |r",
    QuestID = 10397,
    QuestType = "Custom",
    PickUp = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    TurnIn = { x = 497.947998, y = 2012.609985, z = 112.777, id = 20793 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10397, 1) then
        GMR.SetQuestingState("Idle")
        
      elseif not GMR.Questing.IsObjectiveCompleted(10397, 2) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(526.484375, 2800.3610839844, 217.01509094238, 25) then 
            GMR.MeshTo(526.484375, 2800.3610839844, 217.01509094238)
          else
              local doorwayAbyss = GMR.GetObjectWithInfo({ id = 182935, rawType = 8 })
              if doorwayAbyss then
                  GMR.Questing.InteractWith(doorwayAbyss, nil, nil, nil, nil, 8)
              end
          end
      elseif not GMR.Questing.IsObjectiveCompleted(10397, 3) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand01238 then
        GMR.Print("Frame created")
        GMR.Frames.Rand01238 = CreateFrame("frame")
        GMR.Frames.Rand01238:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10397 then
            if GMR.IsExecuting() and GetItemCount(29588) >= 1 and not GMR.IsQuestActive(10395) then
              GMR.Use(29588)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand01238 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(569.778992, 2797.709961, 218.619995,100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(19298)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Dark Missive - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 10395,
    QuestType = "Grinding",
    PickUp = { x = 569.778992, y = 2797.709961, z = 218.619995, id = { 19298, 29588 } },
    TurnIn = { x = -809.405029, y = 2611.590088, z = 124.472, id = 16839 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand008 then
        GMR.Print("Frame created")
        GMR.Frames.Rand008 = CreateFrame("frame")
        GMR.Frames.Rand008:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10395 then
            if GMR.IsExecuting() and GetItemCount(29588) >= 1 and not GMR.IsQuestActive(10395) then
              GMR.Use(29588)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand008 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(386.33142089844, 2071.1110839844, 75.355545043945, 30)',
      'GMR.DefineUnstuck(367.91024780273, 2071.5363769531, 66.773811340332)',
      'GMR.DefineUnstuck(353.73675537109, 2074.4916992188, 61.528644561768)',
      'GMR.DefineUnstuck(341.8059387207, 2082.1086425781, 57.891353607178)',
      'GMR.DefineUnstuck(331.06329345703, 2094.2211914062, 55.880546569824)',
      'GMR.DefineUnstuck(323.65524291992, 2105.4406738281, 55.937652587891)',
      'GMR.DefineUnstuck(315.75701904297, 2116.8198242188, 55.133926391602)',
      'GMR.DefineUnstuck(308.20980834961, 2128.9235839844, 54.853080749512)',
      'GMR.DefineUnstuck(301.53399658203, 2139.556640625, 55.766799926758)',
      'GMR.DefineUnstuck(294.58843994141, 2149.5632324219, 55.950386047363)',
      'GMR.DefineUnstuck(289.82147216797, 2160.1059570312, 55.78426361084)',
      'GMR.DefineUnstuck(289.20034790039, 2172.6462402344, 56.290351867676)',
      'GMR.DefineUnstuck(292.84457397461, 2183.2453613281, 57.887939453125)',
      'GMR.DefineUnstuck(296.02349853516, 2189.8315429688, 59.356704711914)',
      'GMR.DefineUnstuck(296.04037475586, 2204.4812011719, 60.663692474365)',
      'GMR.DefineUnstuck(295.54986572266, 2219.1560058594, 62.341617584229)',
      'GMR.DefineUnstuck(294.77801513672, 2242.2482910156, 63.250022888184)',
      'GMR.DefineUnstuck(298.26165771484, 2258.0651855469, 65.071174621582)',
      'GMR.DefineUnstuck(304.51052856445, 2273.6525878906, 67.487937927246)',
      'GMR.DefineUnstuck(310.60531616211, 2288.85546875, 69.914886474609)',
      'GMR.DefineUnstuck(315.86782836914, 2304.3049316406, 72.092155456543)',
      'GMR.DefineUnstuck(319.01983642578, 2316.4016113281, 73.659271240234)',
      'GMR.DefineUnstuck(320.60311889648, 2331.0102539062, 75.557273864746)',
      'GMR.DefineUnstuck(315.91485595703, 2345.0380859375, 76.38404083252)',
      'GMR.DefineUnstuck(307.94427490234, 2356.7866210938, 76.625205993652)',
      'GMR.DefineUnstuck(301.3229675293, 2370.0212402344, 77.727668762207)',
      'GMR.DefineUnstuck(288.37353515625, 2375.8159179688, 75.064300537109)',
      'GMR.DefineUnstuck(276.00604248047, 2382.7563476562, 72.624862670898)',
      'GMR.DefineUnstuck(261.62841796875, 2386.5183105469, 68.879943847656)',
      'GMR.DefineUnstuck(247.29750061035, 2388.0803222656, 65.133850097656)',
      'GMR.DefineUnstuck(234.6208190918, 2390.931640625, 59.216758728027)',
      'GMR.DefineUnstuck(225.95030212402, 2400.6975097656, 54.464405059814)',
      'GMR.DefineUnstuck(217.19486999512, 2414.6899414062, 54.011306762695)',
      'GMR.DefineUnstuck(208.08627319336, 2425.1442871094, 55.279205322266)',
      'GMR.DefineUnstuck(196.36233520508, 2434.3552246094, 56.311351776123)',
      'GMR.DefineUnstuck(182.79808044434, 2441.9020996094, 57.079807281494)',
      'GMR.DefineUnstuck(169.94509887695, 2449.0532226562, 56.626266479492)',
      'GMR.DefineUnstuck(155.30871582031, 2452.3801269531, 54.820686340332)',
      'GMR.DefineUnstuck(141.11274719238, 2452.9631347656, 54.568515777588)',
      'GMR.DefineUnstuck(126.40727233887, 2452.2517089844, 54.288951873779)',
      'GMR.DefineUnstuck(110.36023712158, 2450.1088867188, 54.323795318604)',
      'GMR.DefineUnstuck(99.169731140137, 2446.552734375, 54.147148132324)',
      'GMR.DefineUnstuck(83.733245849609, 2442.4787597656, 54.434753417969)',
      'GMR.DefineUnstuck(66.695129394531, 2437.9819335938, 54.419059753418)',
      'GMR.DefineUnstuck(47.572120666504, 2432.9350585938, 52.870361328125)',
      'GMR.DefineUnstuck(29.691761016846, 2428.2160644531, 51.289249420166)',
      'GMR.DefineUnstuck(13.119623184204, 2422.5402832031, 52.643531799316)',
      'GMR.DefineUnstuck(-3.746600151062, 2410.9169921875, 51.676212310791)',
      'GMR.DefineUnstuck(-20.47793006897, 2399.9868164062, 52.564594268799)',
      'GMR.DefineUnstuck(-34.255760192871, 2389.9116210938, 53.20964050293)',
      'GMR.DefineUnstuck(-50.34016418457, 2371.2312011719, 54.685394287109)',
      'GMR.DefineUnstuck(-59.045101165771, 2362.3059082031, 55.861911773682)',
      'GMR.DefineUnstuck(-74.050262451172, 2351.6833496094, 56.715663909912)',
      'GMR.DefineUnstuck(-93.713768005371, 2334.0590820312, 58.852710723877)',
      'GMR.DefineUnstuck(-110.21019744873, 2318.5275878906, 63.571151733398)',
      'GMR.DefineUnstuck(-126.10717773438, 2303.560546875, 67.169845581055)',
      'GMR.DefineUnstuck(-142.30743408203, 2288.3078613281, 73.140419006348)',
      'GMR.DefineUnstuck(-158.49778747559, 2276.5563964844, 73.636421203613)',
      'GMR.DefineUnstuck(-176.91816711426, 2269.7121582031, 69.699020385742)',
      'GMR.DefineUnstuck(-195.53953552246, 2264.560546875, 63.1819190979)',
      'GMR.DefineUnstuck(-215.3582611084, 2259.0776367188, 60.808029174805)',
      'GMR.DefineUnstuck(-231.43383789062, 2256.4699707031, 61.668186187744)',
      'GMR.DefineUnstuck(-245.63299560547, 2256.9379882812, 65.236473083496)',
      'GMR.DefineUnstuck(-251.96435546875, 2258.9604492188, 65.487236022949)',
      'GMR.DefineUnstuck(-262.96530151367, 2267.8923339844, 63.46305847168)',
      'GMR.DefineUnstuck(-274.97848510742, 2276.9533691406, 61.108818054199)',
      'GMR.DefineUnstuck(-293.01473999023, 2283.5727539062, 58.593616485596)',
      'GMR.DefineUnstuck(-308.21463012695, 2292.7214355469, 56.323375701904)',
      'GMR.DefineUnstuck(-318.74499511719, 2305.1020507812, 53.933208465576)',
      'GMR.DefineUnstuck(-326.87545776367, 2321.1257324219, 51.991497039795)',
      'GMR.DefineUnstuck(-333.57186889648, 2339.14453125, 48.805484771729)',
      'GMR.DefineUnstuck(-343.20260620117, 2355.7155761719, 46.086708068848)',
      'GMR.DefineUnstuck(-358.0915222168, 2369.5134277344, 44.155529022217)',
      'GMR.DefineUnstuck(-374.78561401367, 2379.3525390625, 42.73413848877)',
      'GMR.DefineUnstuck(-388.34683227539, 2390.8686523438, 41.48509979248)',
      'GMR.DefineUnstuck(-401.25607299805, 2403.4841308594, 40.3955078125)',
      'GMR.DefineUnstuck(-415.47039794922, 2413.0107421875, 39.605792999268)',
      'GMR.DefineUnstuck(-432.81591796875, 2422.3935546875, 36.477348327637)',
      'GMR.DefineUnstuck(-450.53826904297, 2431.9802246094, 38.51651763916)',
      'GMR.DefineUnstuck(-467.74185180664, 2443.2419433594, 38.908290863037)',
      'GMR.DefineUnstuck(-482.89309692383, 2456.9733886719, 40.74499130249)',
      'GMR.DefineUnstuck(-498.03121948242, 2473.857421875, 44.520294189453)',
      'GMR.DefineUnstuck(-516.80535888672, 2491.7856445312, 49.536464691162)',
      'GMR.DefineUnstuck(-538.01654052734, 2503.3864746094, 54.900218963623)',
      'GMR.DefineUnstuck(-559.69842529297, 2511.4113769531, 62.339569091797)',
      'GMR.DefineProfileCenter(569.778992, 2797.709961, 218.619995, 70)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(19298)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Heart of Darkness / The Path to Glory / The Longbeards / An Old Gift / Unyeilding Souls |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10399, x = -809.40454101562, y = 2611.5910644531, z = 124.38919067383, id = 16839 },
      { questId = 10047, x = -809.40454101562, y = 2611.5910644531, z = 124.38919067383, id = 16839 },
      { questId = 10058, x = -708.29669189453, y = 2735.7436523438, z = 94.733764648438, id = 16825 },
      { questId = 9558, x = -708.87133789062, y = 2739.130859375, z = 94.733749389648, id = 16826 },
      { questId = 10050, x = -597.18621826172, y = 2912.0349121094, z = 59.213386535645, id = 16827 },
      { questId = 9355, x = -673.34484863281, y = 2853.2170410156, z = 49.378269195557, id = 16837 },
      { questId = 10079, x = -673.34484863281, y = 2853.2170410156, z = 49.378269195557, id = 16837 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE When This Mine's a-Rockin' |r",
    QuestID = 10079,
    QuestType = "Custom",
    PickUp = { x = -673.344971, y = 2853.219971, z = 49.461498, id = 16837 },
    TurnIn = { x = -673.344971, y = 2853.219971, z = 49.461498, id = 16837 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10079, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-668.0736, 2814.805, 47.28006,100)',
      'GMR.DefineProfileCenter(-674.9313, 2757.926, 40.76197,100)',
      'GMR.DefineProfileCenter(-713.1915, 2733.722, 33.64737,100)',
      'GMR.DefineProfileCenter(-747.6174, 2732.878, 21.9957,100)',
      'GMR.DefineProfileCenter(-719.7755, 2744.296, 16.14655,100)',
      'GMR.DefineProfileCenter(-691.3367, 2710.648, 4.654569,100)',
      'GMR.DefineProfileCenter(-680.0317, 2658.655, 1.056471,100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(18827)',
      'GMR.DefineQuestEnemyId(20798)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Mastermind |r",
    QuestID = 10099,
    QuestType = "Custom",
    PickUp = { x = -673.344971, y = 2853.219971, z = 49.461498, id = 16837 },
    TurnIn = { x = -673.344971, y = 2853.219971, z = 49.461498, id = 16837 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10099, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-806.17407226562, 2704.5854492188, 107.41032409668, 60)',
      'GMR.DefineUnstuck(-784.17626953125, 2701.0151367188, 105.98710632324)',
      'GMR.DefineUnstuck(-760.71649169922, 2698.171875, 105.74020385742)',
      'GMR.DefineUnstuck(-740.11791992188, 2689.8493652344, 102.73526763916)',
      'GMR.DefineUnstuck(-722.45263671875, 2680.5231933594, 96.942489624023)',
      'GMR.DefineUnstuck(-703.06567382812, 2682.6701660156, 93.842277526855)',
      'GMR.DefineUnstuck(-681.67840576172, 2685.8364257812, 92.922088623047)',
      'GMR.DefineUnstuck(-661.05194091797, 2688.8898925781, 88.705749511719)',
      'GMR.DefineUnstuck(-648.49224853516, 2706.2360839844, 87.238746643066)',
      'GMR.DefineUnstuck(-634.59576416016, 2724.7341308594, 85.851722717285)',
      'GMR.DefineUnstuck(-620.02685546875, 2743.3989257812, 84.220916748047)',
      'GMR.DefineUnstuck(-608.03533935547, 2756.3679199219, 80.922920227051)',
      'GMR.DefineUnstuck(-600.25787353516, 2760.6821289062, 79.108192443848)',
      'GMR.DefineUnstuck(-574.43280029297, 2768.0485839844, 73.151596069336)',
      'GMR.DefineUnstuck(-571.66345214844, 2790.7893066406, 66.605697631836)',
      'GMR.DefineUnstuck(-589.00439453125, 2817.4577636719, 62.553638458252)',
      'GMR.DefineUnstuck(-596.1357421875, 2831.1564941406, 60.46654510498)',
      'GMR.DefineUnstuck(-603.72045898438, 2843.0258789062, 58.791763305664)',
      'GMR.DefineUnstuck(-618.4931640625, 2854.8369140625, 56.06761932373)',
      'GMR.DefineUnstuck(-634.87237548828, 2863.2946777344, 50.730754852295)',
      'GMR.DefineUnstuck(-647.65020751953, 2865.2927246094, 49.296775817871)',
      'GMR.DefineUnstuck(-659.13732910156, 2861.2316894531, 49.081127166748)',
      'GMR.DefineUnstuck(-664.26776123047, 2853.935546875, 49.368541717529)',
      'GMR.DefineUnstuck(-665.30712890625, 2843.2319335938, 50.51342010498)',
      'GMR.DefineUnstuck(-665.88482666016, 2827.5451660156, 50.078674316406)',
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-648.0015, 2651.462, -3.032806,100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(18974)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Path of Glory |r",
    QuestID = 10047,
    QuestType = "Custom",
    PickUp = { x = -809.405029, y = 2611.590088, z = 124.472, id = 16839 },
    TurnIn = { x = -809.405029, y = 2611.590088, z = 124.472, id = 16839 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10047, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-231.3827, 2231.086, 66.45734, 100)',
      'GMR.DefineProfileCenter(-206.3274, 2237.479, 65.60208, 100)',
      'GMR.DefineProfileCenter(-203.9629, 2196.157, 73.33993, 100)',
      'GMR.DefineProfileCenter(-198.1169, 2163.315, 78.91178, 100)',
      'GMR.DefineProfileCenter(-191.2462, 2127.36, 85.17311, 100)',
      'GMR.DefineProfileCenter(-205.0212, 2071.635, 92.86361, 100)',
      'GMR.DefineProfileCenter(-213.4579, 2008.725, 97.72665, 100)',
      'GMR.DefineProfileCenter(-225.3234, 2349.699, 40.26536, 100)',
      'GMR.DefineProfileCenter(-232.4758, 2185.564, 73.50227, 100)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(182789)',
      'GMR.DefineCustomObjectId(182789)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Temple of Telhamat |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10093, x = -809.405029, y = 2611.590088, z = 124.472, id = 16839 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Job for an Intelligent Man (Ring Upgrade for Casters) |r",
    QuestID = 9355,
    QuestType = "Custom",
    PickUp = { x = -673.344971, y = 2853.219971, z = 49.461498, id = 16837 },
    TurnIn = { x = -673.344971, y = 2853.219971, z = 49.461498, id = 16837 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9355, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand011 then
        GMR.Print("Frame created")
        GMR.Frames.Rand011 = CreateFrame("frame")
        GMR.Frames.Rand011:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9355 and not GMR.Questing.IsObjectiveCompleted(9355, 1) then
            if GMR.IsExecuting() and GetItemCount(23338) >= 1 and not GMR.IsQuestActive(9373) then
              local itemName = GetItemInfo(23338)
              GMR.Use(itemName)
              QuestFrameAcceptButton:Click()
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand011 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-849.27593994141, 2965.2331542969, 8.6600894927979, 100)',
      'GMR.DefineProfileCenter(-897.68664550781, 3022.4541015625, 10.156967163086, 100)',
      'GMR.DefineProfileCenter(-764.99829101562, 3047.6474609375, 6.2585868835449, 100)',
      'GMR.DefineProfileCenter(-744.20257568359, 3076.5275878906, 5.9045453071594, 100)',
      'GMR.DefineProfileCenter(-734.5048828125, 3098.8728027344, 3.0389223098755, 100)',
      'GMR.DefineProfileCenter(-735.87219238281, 3206.7424316406, -1.5952764749527, 100)',
      'GMR.DefineProfileCenter(-802.91870117188, 3165.9958496094, 3.2061269283295, 100)',
      'GMR.DefineProfileCenter(-856.44915771484, 3183.0908203125, 5.9140048027039, 100)',
      'GMR.DefineProfileCenter(-784.78961181641, 3104.7385253906, -10.908683776855, 100)',
      'GMR.DefineProfileCenter(-791.32525634766, 3098.1091308594, -8.4497385025024, 100)',
      'GMR.DefineProfileCenter(-707.8383, 3018.741, 14.36597, 120)',
      'GMR.DefineProfileCenter(-680.0767, 3125.421, -3.051707, 120)',
      'GMR.DefineProfileCenter(-718.6839, 3178.425, -20.30642, 120)',
      'GMR.DefineProfileCenter(-830.619, 3224.669, 20.11537, 120)',
      'GMR.DefineProfileCenter(-850.0051, 3064.873, 11.10627, 120)',
      'GMR.DefineProfileCenter(-869.9362, 2959.165, 9.218779, 120)',
      'GMR.DefineProfileCenter(-776.7478, 2910.372, 17.81917, 120)',
      'GMR.DefineProfileCenter(-708.7668, 3028.377, 11.82657, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(16857)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Unyielding Souls |r",
    QuestID = 10050,
    QuestType = "Custom",
    PickUp = { x = -597.185974, y = 2912.030029, z = 59.2966, id = 16827 },
    TurnIn = { x = -597.185974, y = 2912.030029, z = 59.2966, id = 16827 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10050, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 16904, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10050, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 16905, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10050, 3) then
        local enemy = GMR.GetObjectWithInfo({ id = 16906, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1252.823, 2509.128, 41.1351, 70)',
      'GMR.DefineProfileCenter(-1199.96, 2626.393, 18.4107, 70)',
      'GMR.DefineProfileCenter(-1195.638, 2686.61, 2.418364, 70)',
      'GMR.DefineProfileCenter(-1265.104, 2712.731, -9.73418, 70)',
      'GMR.DefineProfileCenter(-1382.134, 2668.466, -21.14911, 70)',
      'GMR.DefineProfileCenter(-1378.148, 2732.652, -29.90999, 70)',
      'GMR.DefineProfileCenter(-1387.664, 2829.671, -40.3022, 70)',
      'GMR.DefineProfileCenter(-1461.693, 2723.725, -46.60286, 70)',
      'GMR.DefineProfileCenter(-1310.958, 2697.819, -10.41644, 70)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(16904)',
      'GMR.DefineQuestEnemyId(16906)',
      'GMR.DefineQuestEnemyId(16905)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Looking to the Leadership |r",
    QuestID = 10057,
    QuestType = "Custom",
    PickUp = { x = -597.185974, y = 2912.030029, z = 59.2966, id = 16827 },
    TurnIn = { x = -597.185974, y = 2912.030029, z = 59.2966, id = 16827 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
    if not GMR.Questing.IsObjectiveCompleted(10057, 1) then
      GMR.SetQuestingState(nil);
      if not GMR.IsPlayerPosition(-1311.579956, 2767.750000, -27.080099, 3) then
        GMR.MeshTo(-1311.579956, 2767.750000, -27.080099)
      else
        local npc = GMR.GetObjectWithInfo({ id = 16977, rawType = 5 })
        if npc then
          GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
        end
      end
    elseif not GMR.Questing.IsObjectiveCompleted(10057, 2) then
      GMR.SetQuestingState(nil);
      if not GMR.IsPlayerPosition(-1400.459961, 2711.330078, -27.780899, 3) then
        GMR.MeshTo(-1400.459961, 2711.330078, -27.780899)
      else
        local npc = GMR.GetObjectWithInfo({ id = 16978, rawType = 5 })
        if npc then
          GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
        end
      end
    end
  ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-1311.579956, 2767.750000, -27.080099, 70)',
      'GMR.DefineProfileCenter(-1400.459961, 2711.330078, -27.780899, 70)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(16978)',
      'GMR.DefineQuestEnemyId(16977)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE An Old Gift |r",
    QuestID = 10058,
    QuestType = "Custom",
    PickUp = { x = -708.296997, y = 2735.73999, z = 94.816803, id = 16825 },
    TurnIn = { x = -708.296997, y = 2735.73999, z = 94.816803, id = 16825 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
    if not GMR.Questing.IsObjectiveCompleted(10058, 1) then
      GMR.SetQuestingState(nil);
      if not GMR.IsPlayerPosition(-1508.454224, 2701.108887, -62.576046, 3) then
        GMR.MeshTo(-1508.454224, 2701.108887, -62.576046)
      else
        local object = GMR.GetObjectWithInfo({ id = 182804, rawType = 8 })
        if not GMR.GetDelay("CustomQuest") and object then
          GMR.Interact(object); GMR.SetDelay("CustomQuest", 3)
        end
      end
    end
  ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-1508.454224, 2701.108887, -62.576046, 70)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(182804)',
      'GMR.DefineCustomObjectId(182804)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Looking for Leadership / An Old Gift|r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10057, x = -597.185974, y = 2912.030029, z = 59.2966, id = 16827 },
      { questId = 10058, x = -708.296997, y = 2735.73999, z = 94.816803, id = 16825 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(323.088013, 7839.830078, 22.167400, 19383)',
      'GMR.DefineRepairVendor(323.088013, 7839.830078, 22.167400, 19383)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(228.162994, 7933.879883, 25.161100, 18245)',
      'GMR.DefineProfileMailbox(258.980835, 7869.787598, 23.053631, 182567)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE In Case of Emergency... |r",
    QuestID = 10161,
    QuestType = "Custom",
    PickUp = { x = -1096.859985, y = 3001.189941, z = 8.27807, id = 19367 },
    TurnIn = { x = -1096.859985, y = 3001.189941, z = 8.27807, id = 19367 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10161, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 40)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-1093.582,2923.063,-1.606655, 70)',
      'GMR.DefineProfileCenter(-1047.433,2778.977,-9.029743, 70)',
      'GMR.DefineProfileCenter(-1030.038940,2627.676758,-11.013138, 70)',
      'GMR.DefineProfileCenter(-1078.111,2443.336,20.5518, 70)',
      'GMR.DefineProfileCenter(-1047.639,2314.632,14.02283, 70)',
      'GMR.DefineProfileCenter(-891.3585,2270.583,0.8596322, 70)',
      'GMR.DefineProfileCenter(-797.7929,2463.744,29.62946, 70)',
      'GMR.DefineProfileCenter(-805.9025,2161.988,11.77662, 70)',
      'GMR.DefineProfileCenter(-895.8868,2204.125,9.97627, 70)',
      'GMR.DefineProfileCenter(-1033.091,2352.461,12.48151, 70)',
      'GMR.DefineProfileCenter(-1015.249,2651.909,3.913574, 70)',
      'GMR.DefineProfileCenter(-1065.134,2827.632,-19.90529, 70)',
      'GMR.DefineProfileCenter(-1049.034,2899.116,-0.2953456, 70)',
      'GMR.DefineProfileCenter(-907.155457,2456.305176,1.726288, 70)',
      'GMR.DefineAreaBlacklist(-1053.3332519531, 2640.1293945312, 3.9874315261841, 25)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(183397)',
      'GMR.DefineQuestEnemyId(183394)',
      'GMR.DefineQuestEnemyId(183396)',
      'GMR.DefineQuestEnemyId(183395)',
      'GMR.DefineCustomObjectId(183397)',
      'GMR.DefineCustomObjectId(183394)',
      'GMR.DefineCustomObjectId(183396)',
      'GMR.DefineCustomObjectId(183395)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Voidwalkers Gone Wild |r",
    QuestID = 9351,
    QuestType = "Custom",
    PickUp = { x = -1096.859985, y = 3001.189941, z = 8.27807, id = 19367 },
    TurnIn = { x = -1096.859985, y = 3001.189941, z = 8.27807, id = 19367 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9351, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 40)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-1372.880615, 3145.235596, 26.791048, 70)',
      'GMR.DefineProfileCenter(-1390.465332, 3040.505859, 4.156014, 70)',
      'GMR.DefineProfileCenter(-1353.761230, 2959.628906, -3.704804, 70)',
      'GMR.DefineProfileCenter(-1404.937866, 2931.530029, -26.646973, 70)',
      'GMR.DefineProfileCenter(-1413.048584, 3097.252930, 6.985355, 70)',
      'GMR.DefineProfileCenter(-1430.156860, 3279.379395, 24.980282, 70)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(16975)',
      'GMR.DefineQuestEnemyId(16974)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Ravager Egg Roundup |r",
    QuestID = 9349,
    QuestType = "Custom",
    PickUp = { x = -1095.949951, y = 2996.350098, z = 8.27364, id = 19344 },
    TurnIn = { x = -1095.949951, y = 2996.350098, z = 8.27364, id = 19344 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9349, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 40)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-1420.171, 3425.188, 41.59656, 60)',
      'GMR.DefineProfileCenter(-1454.052, 3411.541, 33.9743, 60)',
      'GMR.DefineProfileCenter(-1488.032, 3486.563, 35.89564, 60)',
      'GMR.DefineProfileCenter(-1549.218, 3483.503, 22.53659, 60)',
      'GMR.DefineProfileCenter(-1520.568, 3578.584, 41.86139, 60)',
      'GMR.DefineProfileCenter(-1576.781, 3557.437, 27.72922, 60)',
      'GMR.DefineProfileCenter(-1570.502, 3607.362, 34.45533, 60)',
      'GMR.DefineProfileCenter(-1665.111, 3633.725, 31.73647, 60)',
      'GMR.DefineProfileCenter(-1569.837, 3609.654, 34.50581, 60)',
      'GMR.DefineProfileCenter(-1583.523, 3695.073, 38.52938, 60)',
      'GMR.DefineProfileCenter(-1672.075, 3722.497, 40.38662, 60)',
      'GMR.DefineProfileCenter(-1679.658, 3765.051, 36.2511, 60)',
      'GMR.DefineProfileCenter(-1677.345, 3783.735, 36.2515, 60)',
      'GMR.DefineProfileCenter(-1611.714, 3791.644, 42.12037, 60)',
      'GMR.DefineProfileCenter(-1714.629, 3822.721, 45.32983, 60)',
      'GMR.DefineProfileCenter(-1704.229, 3841.994, 40.53221, 60)',
      'GMR.DefineProfileCenter(-1732.683, 3856.676, 43.61577, 60)',
      'GMR.DefineProfileCenter(-1619.033, 3820.451, 43.32684, 60)',
      'GMR.DefineProfileCenter(-1604.725, 3747.161, 41.00349, 60)',
      'GMR.DefineProfileCenter(-1570.524, 3609.343, 34.45152, 60)',
      'GMR.DefineProfileCenter(-1551.592, 3546.341, 32.11758, 60)',
      'GMR.DefineProfileCenter(-1487.595, 3476.134, 32.65432, 60)',
      'GMR.DefineProfileCenter(-1445.515, 3425.393, 36.21096, 60)',
      'GMR.DefineAreaBlacklist(-1420.380005,3462.121826,46.759861,15)',
      'GMR.DefineAreaBlacklist(-1563.212769,3514.184570,15.076518,10)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(181385)',
      'GMR.DefineCustomObjectId(181385)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Helboar, the Other White Meat |r",
    QuestID = 9361,
    QuestType = "Custom",
    PickUp = { x = -1095.949951, y = 2996.350098, z = 8.27364, id = 19344 },
    TurnIn = { x = -1095.949951, y = 2996.350098, z = 8.27364, id = 19344 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9361, 1) and GetItemCount(23270) < 1  then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand010 then
        GMR.Print("Frame created")
        GMR.Frames.Rand010 = CreateFrame("frame")
        GMR.Frames.Rand010:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9361 then
            if GMR.IsExecuting() and GMR.IsQuestActive(9361) then
              local itemName = GetItemInfo(23268)
              if not GMR.Questing.IsObjectiveCompleted(9361, 1) and GetItemCount(23270) >= 2 then
                GMR.Use(itemName)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand010 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 10)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-1077.572, 2921.525, -1.186113, 120)',
      'GMR.DefineProfileCenter(-952.134, 2795.023, 4.232949, 120)',
      'GMR.DefineProfileCenter(-841.653, 2911.051, 9.335446, 120)',
      'GMR.DefineProfileCenter(-922.3448, 3081.768, 16.22847, 120)',
      'GMR.DefineProfileCenter(-735.1697, 3021.665, 12.05203, 120)',
      'GMR.DefineProfileCenter(-1179.858, 3062.121, 25.3441, 120)',
      'GMR.DefineProfileCenter(-917.0084, 2914.575, 5.343684, 120)',
      'GMR.DefineProfileCenter(-1232.243, 2977.758, 9.826759, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(16863)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Smooth as Butter |r",
    QuestID = 9356,
    QuestType = "Custom",
    PickUp = { x = -1095.949951, y = 2996.350098, z = 8.27364, id = 19344 },
    TurnIn = { x = -1095.949951, y = 2996.350098, z = 8.27364, id = 19344 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9356, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 40)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-900.7607, 2574.444, 20.38859, 120)',
      'GMR.DefineProfileCenter(-847.8049, 2400.488, 3.853766, 120)',
      'GMR.DefineProfileCenter(-774.7745, 2327.815, 12.52186, 120)',
      'GMR.DefineProfileCenter(-678.6805, 2215.164, 25.76592, 120)',
      'GMR.DefineProfileCenter(-887.4419, 2260.966, 2.741399, 120)',
      'GMR.DefineProfileCenter(-941.5074, 2409.585, 0.3780828, 120)',
      'GMR.DefineProfileCenter(-721.837097, 2184.110352, 20.561342, 120)',
      'GMR.DefineProfileCenter(-786.351379, 2335.689453, 10.955239, 120)',
      'GMR.DefineAreaBlacklist(-1420.380005,3462.121826,46.759861,15)',
      'GMR.DefineAreaBlacklist(-1563.212769,3514.184570,15.076518,10)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(16972)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Cursed Talismans |r",
    QuestID = 10484,
    QuestType = "Custom",
    PickUp = { x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 },
    TurnIn = { x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10484, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 40)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-1103.678, 2339.985, 23.72057, 120)',
      'GMR.DefineProfileCenter(-1172.109, 2270.422, 41.44325, 60)',
      'GMR.DefineProfileCenter(-1079.066, 2274.968, 24.84718, 60)',
      'GMR.DefineProfileCenter(-1034.586, 2212.488, 16.56769, 60)',
      'GMR.DefineProfileCenter(-960.9215, 2214.594, 9.552672, 60)',
      'GMR.DefineProfileCenter(-873.65, 2150.883, 13.37324, 60)',
      'GMR.DefineProfileCenter(-922.7961, 2122.376, 20.52276, 60)',
      'GMR.DefineProfileCenter(-1055.392, 2148.162, 38.45712, 60)',
      'GMR.DefineProfileCenter(-1120.849, 2107.997, 68.21753, 60)',
      'GMR.DefineProfileCenter(-1130.052, 2070.625, 66.9397, 60)',
      'GMR.DefineProfileCenter(-977.4161, 2055.552, 66.99088, 60)',
      'GMR.DefineProfileCenter(-937.9418, 2007.991, 65.98048, 60)',
      'GMR.DefineProfileCenter(-921.1581, 1972.412, 67.03526, 60)',
      'GMR.DefineProfileCenter(-884.7187, 2050.25, 37.75677, 60)',
      'GMR.DefineProfileCenter(-895.216, 2105.163, 22.18787, 60)',
      'GMR.DefineProfileCenter(-930.958, 2132.42, 19.02489, 60)',
      'GMR.DefineProfileCenter(-967.6158, 2189.736, 13.06963, 60)',
      'GMR.DefineProfileCenter(-985.1539, 2181.982, 15.77969, 60)',
      'GMR.DefineProfileCenter(-1049.727, 2253.034, 23.52975, 60)',
      'GMR.DefineAreaBlacklist(-977.0522, 2103.354, 21.68662, 15)',
      'GMR.DefineAreaBlacklist(-1135.986, 2004.605, 69.9808, 15)',
      'GMR.DefineAreaBlacklist(-1134.736, 1971.614, 73.54317, 25)',
      'GMR.DefineAreaBlacklist(-1105.831, 1940.946, 73.9445, 20)',
      'GMR.DefineAreaBlacklist(-1181.937, 1991.852, 74.63454, 15)',
      'GMR.DefineAreaBlacklist(-1054.094, 2002.422, 66.99904, 15)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(16907)',
      'GMR.DefineQuestEnemyId(19424)',
      'GMR.DefineQuestEnemyId(19422)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Warlord of the Bleeding Hollow |r",
    QuestID = 10485,
    QuestType = "Custom",
    PickUp = { x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 },
    TurnIn = { x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10485, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-808.03985595703, 2707.263671875, 108.12638092041, 40)',
      'GMR.DefineUnstuck(-811.66912841797, 2696.7272949219, 105.83232879639)',
      'GMR.DefineUnstuck(-820.65008544922, 2684.3413085938, 102.72332000732)',
      'GMR.DefineUnstuck(-833.44122314453, 2677.7141113281, 98.429206848145)',
      'GMR.DefineUnstuck(-846.16052246094, 2677.3513183594, 94.087707519531)',
      'GMR.DefineUnstuck(-857.07458496094, 2686.1882324219, 88.443977355957)',
      'GMR.DefineUnstuck(-865.62774658203, 2695.66796875, 81.492218017578)',
      'GMR.DefineUnstuck(-867.51989746094, 2707.4333496094, 74.326553344727)',
      'GMR.DefineUnstuck(-867.09790039062, 2723.0656738281, 66.290222167969)',
      'GMR.DefineUnstuck(-865.38983154297, 2734.4182128906, 59.638481140137)',
      'GMR.DefineUnstuck(-863.36578369141, 2750.2075195312, 50.483173370361)',
      'GMR.DefineUnstuck(-864.44525146484, 2763.455078125, 43.036838531494)',
      'GMR.DefineUnstuck(-876.16284179688, 2769.181640625, 34.63399887085)',
      'GMR.DefineUnstuck(-890.08227539062, 2768.7001953125, 25.287696838379)',
      'GMR.DefineUnstuck(-904.77062988281, 2766.6845703125, 18.866128921509)',
      'GMR.DefineUnstuck(-916.51995849609, 2762.056640625, 17.471797943115)',
      'GMR.DefineUnstuck(-933.60571289062, 2746.9899902344, 17.047988891602)',
      'GMR.DefineProfileCenter(-1173.459961 ,1920.359985, 81.602997, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineRepairVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineGoodsVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineAmmoVendor(-696.838989, 1875.520020, 63.509899, 19314)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineQuestEnemyId(16964)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Return to Honor Hold (10485 - Followup) |r",
    QuestID = 10903,
    QuestType = "TalkTo",
    PickUp = { x = -701.020996, y = 1874.660034, z = 63.457401, id = 21133 },
    TurnIn = { x = -710.679016, y = 2734.280029, z = 101.675003, id = 22430 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Digging for Prayer Beads |r",
    QuestID = 10916,
    QuestType = "Custom",
    PickUp = { x = -710.679016, y = 2734.280029, z = 101.675003, id = 22430 },
    TurnIn = { x = -710.679016, y = 2734.280029, z = 101.675003, id = 22430 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10916, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-699.40509033203, 2742.669921875, 94.419998168945, 20) then 
          GMR.MeshTo(-699.40509033203, 2742.669921875, 94.419998168945)
        else
          local object = GMR.GetObjectWithInfo({ id = 185302, rawType = 8 })
          if not GMR.GetDelay("CustomQuest") and object then
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-699.40509033203, 2742.669921875, 94.419998168945, 100)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineCustomObjectId(185302)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Fel Spirits |r",
    QuestID = 10909,
    QuestType = "Custom",
    PickUp = { x = -710.67932128906, y = 2734.2795410156, z = 101.5913772583, id = 22430 },
    TurnIn = { x = -710.67932128906, y = 2734.2795410156, z = 101.5913772583, id = 22430 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10909, 1) then
      GMR.SetQuestingState(nil);
      local itemName = GetItemInfo(31772)
      local object = GMR.GetObjectWithInfo({ id = 185298, rawType = 8 })
      local npc = GMR.GetObjectWithInfo({ id = 16878, rawType = 5, isAlive = true })
      local npcSpirit = GMR.GetObjectWithInfo({ id = 22454, rawType = 5, isAlive = true })
      if GetItemCooldown(31772) == 0 then
        if not object and not GMR.InCombat() then
          if not GMR.IsPlayerPosition(-1121.6766357422, 3224.1533203125, 67.370582580566, 3) then 
            GMR.MeshTo(-1121.6766357422, 3224.1533203125, 67.370582580566)
          else
            GMR.Use(itemName)
          end
        end
      elseif object and npcSpirit then
        GMR.TargetUnit(npcSpirit)
        GMR.Questing.InteractWith(npcSpirit, nil, nil, nil, nil, 5)
      elseif not npcSpirit and object then
        if npc then
          GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 5)
        end
      end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1121.6766357422, 3224.1533203125, 67.370582580566, 25)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(22454)',
      'GMR.DefineQuestEnemyId(16878)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Exorcism of Colonel Jules (10909 Followup) |r",
    QuestID = 10935,
    QuestType = "Custom",
    PickUp = { x = -710.67932128906, y = 2734.2795410156, z = 101.59134674072, id = 22430 },
    TurnIn = { x = -710.679016, y = 2734.280029, z = 101.675003, id = 22430 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10935, 1) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = 22507, canAttack = true, isAlive = true })
        local object2 = GMR.GetObjectWithInfo({ id = 22506, canAttack = true, isAlive = true })
        local npc1 = GMR.GetObjectWithInfo({ id = 22431, rawType = 5, isAlive = true })
        local npc2 = GMR.GetObjectWithInfo({ id = 22432, rawType = 5, isAlive = true })
        local itemName = GetItemInfo(31828)
        if GetItemCooldown(31828) == 0 then
          if object then
            GMR.TargetUnit(object)
            GMR.Use(itemName)
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 8)
          elseif object2 then
            GMR.TargetUnit(object2)
            GMR.Use(itemName)
            GMR.Questing.InteractWith(object2, nil, nil, nil, nil, 8)
          elseif not object and not object2 then
              C_Timer.After(4, function() GMR.Questing.GossipWith(npc2, nil, nil, nil, nil, 5) end)
              C_Timer.After(4, function() GMR.Questing.GossipWith(npc1, nil, nil, nil, nil, 5) end)
          end
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SetQuestNpcInteractRange(159762, 10)',
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-708.55706787109, 2750.8151855469, 101.59143829346, 150)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineQuestEnemyId(22507)',
      'GMR.DefineQuestEnemyId(22506)'
    }
  })
  
  GMR.DefineQuest({ 
    QuestName = "|cFF1E90FE Trollbane Is Looking For You (10935 - Followup) |r",
    QuestID = 10936,
    QuestType = "TalkTo",
    PickUp = { x = -710.67932128906, y = 2734.2795410156, z = 101.59134674072, id = 22430 },
    TurnIn = { x = -815.609375, y = 2614.2043457031, z = 124.38929748535, id = 16819 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineRepairVendor(-717.31719970703, 2607.5817871094, 91.011131286621, 16823)',
      'GMR.DefineGoodsVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(-706.6766, 2700.012, 94.47553, 181380)',
      'GMR.DefineHearthstoneBindLocation(-708.870972, 2739.129883, 94.816803, 16826)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Temple of Telmamat (10047 Followup) |r",
    QuestID = 10093,
    QuestType = "TalkTo",
    PickUp = { x = -809.405029, y = 2611.590088, z = 124.472, id = 16839 },
    TurnIn = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-579.060546875, 2806.5876464844, 65.149482727051, 80)',
      'GMR.DefineUnstuck(-585.65216064453, 2823.8247070312, 61.421852111816)',
      'GMR.DefineUnstuck(-594.23095703125, 2838.3395996094, 58.933479309082)',
      'GMR.DefineUnstuck(-604.22772216797, 2850.8610839844, 56.634765625)',
      'GMR.DefineUnstuck(-615.68939208984, 2862.0561523438, 53.659420013428)',
      'GMR.DefineUnstuck(-628.61193847656, 2871.7172851562, 51.433193206787)',
      'GMR.DefineUnstuck(-643.83703613281, 2881.5346679688, 50.330959320068)',
      'GMR.DefineUnstuck(-654.55828857422, 2892.666015625, 47.444042205811)',
      'GMR.DefineUnstuck(-660.33001708984, 2906.4831542969, 44.19616317749)',
      'GMR.DefineUnstuck(-662.68566894531, 2921.9084472656, 40.942432403564)',
      'GMR.DefineUnstuck(-664.33654785156, 2938.3034667969, 37.333122253418)',
      'GMR.DefineUnstuck(-664.09759521484, 2958.3728027344, 32.986736297607)',
      'GMR.DefineUnstuck(-659.21221923828, 2975.2404785156, 29.676647186279)',
      'GMR.DefineUnstuck(-651.70837402344, 2992.1804199219, 25.915035247803)',
      'GMR.DefineUnstuck(-644.78356933594, 3006.6530761719, 24.128105163574)',
      'GMR.DefineUnstuck(-637.85272216797, 3021.1384277344, 21.180866241455)',
      'GMR.DefineUnstuck(-630.20886230469, 3037.1137695312, 16.578907012939)',
      'GMR.DefineUnstuck(-622.56494140625, 3053.0893554688, 11.896987915039)',
      'GMR.DefineUnstuck(-615.92059326172, 3068.560546875, 7.4878101348877)',
      'GMR.DefineUnstuck(-610.80969238281, 3084.6669921875, 5.1964302062988)',
      'GMR.DefineUnstuck(-606.07995605469, 3099.5725097656, 5.0141730308533)',
      'GMR.DefineUnstuck(-601.09613037109, 3115.2788085938, 4.9974026679993)',
      'GMR.DefineUnstuck(-598.34979248047, 3128.509765625, 4.9978065490723)',
      'GMR.DefineUnstuck(-596.75177001953, 3144.0798339844, 4.9978065490723)',
      'GMR.DefineUnstuck(-595.32238769531, 3158.0068359375, 4.9978065490723)',
      'GMR.DefineUnstuck(-595.39569091797, 3172.3896484375, 5.0153198242188)',
      'GMR.DefineUnstuck(-595.15155029297, 3185.9514160156, 4.5486764907837)',
      'GMR.DefineUnstuck(-592.11383056641, 3197.8083496094, 4.8152918815613)',
      'GMR.DefineUnstuck(-586.15368652344, 3209.7268066406, 4.403470993042)',
      'GMR.DefineUnstuck(-575.791015625, 3226.0107421875, 4.9991979598999)',
      'GMR.DefineUnstuck(-565.46990966797, 3239.3549804688, 5.3469748497009)',
      'GMR.DefineUnstuck(-555.90252685547, 3251.7248535156, 6.3157825469971)',
      'GMR.DefineUnstuck(-546.23742675781, 3266.9260253906, 7.408917427063)',
      'GMR.DefineUnstuck(-540.29364013672, 3281.0358886719, 9.5502071380615)',
      'GMR.DefineUnstuck(-537.87585449219, 3295.1237792969, 12.769607543945)',
      'GMR.DefineUnstuck(-537.32348632812, 3310.3315429688, 15.085195541382)',
      'GMR.DefineUnstuck(-537.30743408203, 3324.3237304688, 18.174312591553)',
      'GMR.DefineUnstuck(-538.41912841797, 3339.5061035156, 21.982055664062)',
      'GMR.DefineUnstuck(-539.93927001953, 3354.25390625, 25.58963394165)',
      'GMR.DefineUnstuck(-541.98980712891, 3370.171875, 29.923185348511)',
      'GMR.DefineUnstuck(-544.79449462891, 3386.4089355469, 34.235908508301)',
      'GMR.DefineUnstuck(-547.75457763672, 3403.5461425781, 38.506008148193)',
      'GMR.DefineUnstuck(-550.82879638672, 3421.3439941406, 41.739566802979)',
      'GMR.DefineUnstuck(-551.49261474609, 3436.9069824219, 43.543758392334)',
      'GMR.DefineUnstuck(-549.744140625, 3453.6459960938, 44.392658233643)',
      'GMR.DefineUnstuck(-546.31408691406, 3469.7341308594, 44.408885955811)',
      'GMR.DefineUnstuck(-540.55114746094, 3485.9836425781, 44.845100402832)',
      'GMR.DefineUnstuck(-533.51196289062, 3500.4321289062, 44.630565643311)',
      'GMR.DefineUnstuck(-526.12322998047, 3515.5979003906, 44.56714630127)',
      'GMR.DefineUnstuck(-518.18884277344, 3531.8840332031, 44.752632141113)',
      'GMR.DefineUnstuck(-511.14962768555, 3546.3325195312, 44.823253631592)',
      'GMR.DefineUnstuck(-503.68121337891, 3561.6618652344, 44.366779327393)',
      'GMR.DefineUnstuck(-496.68493652344, 3576.0222167969, 45.294429779053)',
      'GMR.DefineUnstuck(-489.65185546875, 3590.4582519531, 46.22900390625)',
      'GMR.DefineUnstuck(-482.44097900391, 3605.2590332031, 45.350563049316)',
      'GMR.DefineUnstuck(-473.78912353516, 3623.017578125, 44.759967803955)',
      'GMR.DefineUnstuck(-466.26440429688, 3637.208984375, 45.132095336914)',
      'GMR.DefineUnstuck(-456.82748413086, 3653.6267089844, 45.765693664551)',
      'GMR.DefineUnstuck(-448.00601196289, 3668.9738769531, 46.204078674316)',
      'GMR.DefineUnstuck(-441.70056152344, 3682.7556152344, 45.694664001465)',
      'GMR.DefineUnstuck(-437.1494140625, 3698.9665527344, 46.245666503906)',
      'GMR.DefineUnstuck(-433.95364379883, 3713.8649902344, 46.97416305542)',
      'GMR.DefineUnstuck(-429.98178100586, 3729.8425292969, 46.040554046631)',
      'GMR.DefineUnstuck(-425.90859985352, 3746.2280273438, 44.926971435547)',
      'GMR.DefineUnstuck(-421.63616943359, 3763.4147949219, 47.56217956543)',
      'GMR.DefineUnstuck(-416.07702636719, 3778.8354492188, 50.062072753906)',
      'GMR.DefineUnstuck(-408.68218994141, 3794.0139160156, 52.121196746826)',
      'GMR.DefineUnstuck(-399.07019042969, 3807.8330078125, 53.961681365967)',
      'GMR.DefineUnstuck(-387.45471191406, 3822.7780761719, 56.354930877686)',
      'GMR.DefineUnstuck(-376.84539794922, 3836.4284667969, 58.730133056641)',
      'GMR.DefineUnstuck(-361.93084716797, 3855.6179199219, 62.200908660889)',
      'GMR.DefineUnstuck(-350.56454467773, 3870.2424316406, 64.9814453125)',
      'GMR.DefineUnstuck(-337.93533325195, 3886.4916992188, 67.993476867676)',
      'GMR.DefineUnstuck(-325.04833984375, 3903.0725097656, 71.639060974121)',
      'GMR.DefineUnstuck(-312.85491943359, 3919.6391601562, 76.611541748047)',
      'GMR.DefineUnstuck(-302.65829467773, 3936.5122070312, 81.547241210938)',
      'GMR.DefineUnstuck(-294.38809204102, 3951.6550292969, 86.026214599609)',
      'GMR.DefineUnstuck(-282.06246948242, 3970.1574707031, 91.146621704102)',
      'GMR.DefineUnstuck(-271.78594970703, 3985.583984375, 94.600616455078)',
      'GMR.DefineUnstuck(-260.83416748047, 4002.0241699219, 98.205543518066)',
      'GMR.DefineUnstuck(-253.57008361816, 4018.8957519531, 99.622764587402)',
      'GMR.DefineUnstuck(-247.71823120117, 4037.3374023438, 99.619606018066)',
      'GMR.DefineUnstuck(-242.10772705078, 4055.0187988281, 99.620185852051)',
      'GMR.DefineUnstuck(-232.74101257324, 4069.6350097656, 99.441398620605)',
      'GMR.DefineUnstuck(-219.59161376953, 4081.2797851562, 99.444877624512)',
      'GMR.DefineUnstuck(-205.0492401123, 4088.921875, 99.155601501465)',
      'GMR.DefineUnstuck(-189.22770690918, 4092.2006835938, 99.378974914551)',
      'GMR.DefineUnstuck(-170.31288146973, 4091.1862792969, 99.211570739746)',
      'GMR.DefineUnstuck(-148.93760681152, 4090.0397949219, 99.007995605469)',
      'GMR.DefineUnstuck(-131.25302124023, 4089.0913085938, 97.90177154541)',
      'GMR.DefineUnstuck(-112.29626464844, 4088.0747070312, 95.972480773926)',
      'GMR.DefineUnstuck(-94.234214782715, 4087.1059570312, 96.519821166992)',
      'GMR.DefineUnstuck(-75.333389282227, 4086.0922851562, 91.999778747559)',
      'GMR.DefineUnstuck(-58.753574371338, 4093.4331054688, 85.241790771484)',
      'GMR.DefineUnstuck(-47.128620147705, 4106.4506835938, 80.134437561035)',
      'GMR.DefineUnstuck(-44.097721099854, 4123.4560546875, 80.675102233887)',
      'GMR.DefineUnstuck(-43.335021972656, 4144.4560546875, 81.119064331055)',
      'GMR.DefineUnstuck(-41.549098968506, 4162.4067382812, 79.80924987793)',
      'GMR.DefineUnstuck(-35.948154449463, 4180.0078125, 80.292366027832)',
      'GMR.DefineUnstuck(-27.936794281006, 4197.1411132812, 81.035675048828)',
      'GMR.DefineUnstuck(-18.419769287109, 4213.97265625, 82.137130737305)',
      'GMR.DefineUnstuck(-13.246383666992, 4230.658203125, 83.896507263184)',
      'GMR.DefineUnstuck(-11.832708358765, 4247.8383789062, 84.606246948242)',
      'GMR.DefineUnstuck(-9.642749786377, 4263.2563476562, 84.919494628906)',
      'GMR.DefineUnstuck(-4.526002407074, 4280.5849609375, 84.33683013916)',
      'GMR.DefineUnstuck(4.6998739242554, 4297.2260742188, 85.810165405273)',
      'GMR.DefineUnstuck(17.552124023438, 4310.0131835938, 88.051620483398)',
      'GMR.DefineUnstuck(31.461095809937, 4322.119140625, 90.10400390625)',
      'GMR.DefineUnstuck(46.986431121826, 4332.19140625, 93.241973876953)',
      'GMR.DefineUnstuck(62.151596069336, 4334.0380859375, 96.723655700684)',
      'GMR.DefineUnstuck(76.955337524414, 4333.3837890625, 101.53699493408)',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE In Search of Sedai |r",
    QuestID = 9390,
    QuestType = "TalkTo",
    PickUp = { x = 90.928749084473, y = 4351.6879882812, z = 103.18335723877, id = 16834 },
    TurnIn = { x = 192.145996, y = 4150.549805, z = 73.661102, id = 16852 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Return to Obadei |r",
    QuestID = 9423,
    QuestType = "TalkTo",
    PickUp = { x = 192.145996, y = 4150.549805, z = 73.661102, id = 16852 },
    TurnIn = { x = 90.928749084473, y = 4351.6879882812, z = 103.18335723877, id = 16834 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Makuru's Vengeance |r",
    QuestID = 9424,
    QuestType = "Custom",
    PickUp = { x = 98.088501, y = 4344.580078, z = 101.817001, id = 16833 },
    TurnIn = { x = 98.088501, y = 4344.580078, z = 101.817001, id = 16833 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9424, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(369.4458, 3873.263, 144.3707, 50)',
      'GMR.DefineProfileCenter(262.9749, 3679.685, 185.4746, 50)',
      'GMR.DefineProfileCenter(318.2233, 3673.515, 183.7813, 50)',
      'GMR.DefineProfileCenter(339.4046, 3781.839, 198.4192, 50)',
      'GMR.DefineProfileCenter(383.231, 3766.489, 196.8515, 50)',
      'GMR.DefineProfileCenter(467.8823, 3785.912, 186.2942, 50)',
      'GMR.DefineProfileCenter(512.7664, 3690.256, 187.4875, 50)',
      'GMR.DefineProfileCenter(455.8156, 3769.525, 186.445, 50)',
      'GMR.DefineProfileCenter(398.1936, 3766.099, 191.4869, 50)',
      'GMR.DefineProfileCenter(331.6683, 3725.279, 184.009, 50)',
      'GMR.DefineProfileCenter(322.4864, 3672.789, 183.8534, 50)',
      'GMR.DefineProfileCenter(278.6811, 3667.058, 184.4736, 50)',
      'GMR.DefineProfileCenter(360.1667, 3871.961, 144.6465, 50)',
      'GMR.DefineAreaBlacklist(503.0016, 3849.921, 194.7134, 30)',
      'GMR.DefineAreaBlacklist(526.2589, 3890.169, 189.3598, 45)',
      'GMR.DefineAreaBlacklist(504.5215, 3859.038, 207.8834, 30)',
      'GMR.DefineAreaBlacklist(564.5906, 3683.344, 195.8522, 30)',
      'GMR.DefineAreaBlacklist(575.7244, 3640.054, 201.6471, 30)',
      'GMR.DefineAreaBlacklist(311.67321777344, 3653.3186035156, 191.5281829834, 25)',
      'GMR.BlacklistId(16911)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16846)',
      'GMR.DefineQuestEnemyId(16847)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Atonement |r",
    QuestID = 9543,
    QuestType = "TalkTo",
    PickUp = { x = 90.838402, y = 4351.72998, z = 103.178001, id = 16834 },
    TurnIn = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Temple of Telhamat - Initial Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9399, x = 96.144096, y = 4347.310059, z = 102.357002, id = 16799 },
      { questId = 9398, x = 171.785004, y = 4332.990234, z = 113.765999, id = 16797 },
      { questId = 9426, x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
      { questId = 9430, x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })

  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Demonic Contamination |r",
    QuestID = 9372,
    QuestType = "Custom",
    PickUp = { x = -312.484985, y = 4728.77002, z = 17.3123, id = 16991 },
    TurnIn = { x = -312.484985, y = 4728.77002, z = 17.3123, id = 16991 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9372, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-435.0956, 4070.934, 76.78642, 80)',
      'GMR.DefineProfileCenter(-311.649200, 4660.610840, 23.663984, 120)',
      'GMR.DefineProfileCenter(-355.162231, 4641.011230, 29.725866, 120)',
      'GMR.DefineProfileCenter(-388.028870, 4629.812012, 31.933458, 120)',
      'GMR.DefineProfileCenter(-373.424011, 4592.170410, 35.137798, 120)',
      'GMR.DefineProfileCenter(-413.756958, 4554.815918, 40.615093, 120)',
      'GMR.DefineProfileCenter(-482.641479, 4492.309570, 53.474277, 120)',
      'GMR.DefineProfileCenter(-442.838257, 4385.738770, 46.698326, 120)',
      'GMR.DefineProfileCenter(-389.541565, 4285.426758, 72.819641, 120)',
      'GMR.DefineProfileCenter(-585.158386, 4314.430664, 48.572002, 120)',
      'GMR.DefineProfileCenter(-714.519958, 4227.532227, 51.100941, 120)',
      'GMR.DefineProfileCenter(-823.304993, 4181.667480, 48.885960, 120)',
      'GMR.DefineAreaBlacklist(-320.998810, 4476.799805, 58.842533,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16880)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Testing the Antidote |r",
    QuestID = 10255,
    QuestType = "Custom",
    PickUp = { x = -312.484985, y = 4728.77002, z = 17.3123, id = 16991 },
    TurnIn = { x = -312.484985, y = 4728.77002, z = 17.3123, id = 16991 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if UnitExists("target") and UnitName("target") == "Hulking Helboar" and not UnitIsDead("target") then
        local itemName = GetItemInfo(23337)
        GMR.Use(itemName)
      elseif not GMR.Questing.IsObjectiveCompleted(10255, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand013 then
        GMR.Print("Frame created")
        GMR.Frames.Rand013 = CreateFrame("frame")
        GMR.Frames.Rand013:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10255 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10255)  then
              if not GMR.Questing.IsObjectiveCompleted(10255, 1) then
                if UnitExists("target") and UnitName("target") == "Hulking Helboar" and not UnitIsDead("target") then
                  local itemName = GetItemInfo(23337)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand013 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-311.649200, 4660.610840, 23.663984, 120)',
      'GMR.DefineProfileCenter(-355.162231, 4641.011230, 29.725866, 120)',
      'GMR.DefineProfileCenter(-388.028870, 4629.812012, 31.933458, 120)',
      'GMR.DefineProfileCenter(-373.424011, 4592.170410, 35.137798, 120)',
      'GMR.DefineProfileCenter(-413.756958, 4554.815918, 40.615093, 120)',
      'GMR.DefineProfileCenter(-482.641479, 4492.309570, 53.474277, 120)',
      'GMR.DefineProfileCenter(-442.838257, 4385.738770, 46.698326, 120)',
      'GMR.DefineProfileCenter(-389.541565, 4285.426758, 72.819641, 120)',
      'GMR.DefineProfileCenter(-585.158386, 4314.430664, 48.572002, 120)',
      'GMR.DefineProfileCenter(-714.519958, 4227.532227, 51.100941, 120)',
      'GMR.DefineProfileCenter(-823.304993, 4181.667480, 48.885960, 120)',
      'GMR.DefineAreaBlacklist(-320.998810, 4476.799805, 58.842533,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16880)',
      'GMR.DefineQuestEnemyId(16992)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Keep Thornfang Hill Clear! |r",
    QuestID = 10159,
    QuestType = "Custom",
    PickUp = { x = -313.528015, y = 4731.669922, z = 17.082399, id = 16888 },
    TurnIn = { x = -313.528015, y = 4731.669922, z = 17.082399, id = 16888 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10159, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 19349, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10159, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 19350, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-313.4189, 4872.531, 31.25887, 80)',
      'GMR.DefineProfileCenter(-402.1064, 4969.633, 39.37047, 80)',
      'GMR.DefineProfileCenter(-356.9586, 5039.476, 63.97596, 80)',
      'GMR.DefineProfileCenter(-253.1909, 5099.839, 79.26114, 80)',
      'GMR.DefineProfileCenter(-179.7714, 4956.793, 58.49525, 80)',
      'GMR.DefineProfileCenter(-360.8733, 5010.974, 53.13129, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(19349)',
      'GMR.DefineQuestEnemyId(19350)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Naladu |r",
    QuestID = 10403,
    QuestType = "TalkTo",
    PickUp = { x = -541.786987, y = 4734.319824, z = 38.796101, id = 20678 },
    TurnIn = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-760.262024, 4699.160156, 67.259300, 10)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineCustomObjectId(19361)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Traitor Among Us |r",
    QuestID = 10367,
    QuestType = "Custom",
    PickUp = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    TurnIn = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10367, 1) then
        GMR.SetQuestingState(nil);
        GMR.Questing.InteractWith(-706.264160, 4798.960449, 49.059639, 184466)
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-706.264160, 4798.960449, 49.059639, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(184466)',
      'GMR.DefineCustomObjectId(184466)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Dreghood Elders |r",
    QuestID = 10368,
    QuestType = "Custom",
    PickUp = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    TurnIn = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10368, 1) then
        GMR.SetQuestingState(nil);
        GMR.Questing.GossipWith(-620.888000, 4861.629883, 41.500999, 20677)
      elseif not GMR.Questing.IsObjectiveCompleted(10368, 2) then
        GMR.SetQuestingState(nil);
        GMR.Questing.GossipWith(-541.786987, 4734.319824, 38.796101, 20678)
      elseif not GMR.Questing.IsObjectiveCompleted(10368, 3) then
        GMR.SetQuestingState(nil);
        GMR.Questing.GossipWith(-530.682983, 4867.859863, 33.729099, 20679)
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-706.264160, 4798.960449, 49.059639, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineCustomObjectId(181800)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Arzeth's Demise |r",
    QuestID = 10369,
    QuestType = "Custom",
    PickUp = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    TurnIn = { x = -760.262024, y = 4699.160156, z = 67.2593, id = 19361 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10369, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand023 then
        GMR.Print("Frame created")
        GMR.Frames.Rand023 = CreateFrame("frame")
        GMR.Frames.Rand023:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10369 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10369) then
              if not GMR.Questing.IsObjectiveCompleted(10369, 1) then
                local itemName = GetItemInfo(29513)
                local arzethELIETE = GMR.GetObjectWithInfo({ id = 19354, rawType = 5 })
                if arzethELIETE and UnitExists("target") and UnitClassification("target") == "elite" then
                  GMR.TargetUnit(arzethELIETE)
                  GMR.Use(itemName)
                elseif arzethELIETE then
                  GMR.TargetUnit(arzethELIETE)
                  GMR.Questing.InteractWith(arzethELIETE, nil, nil, nil, nil, 5)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand023 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineProfileCenter(-658.366821, 4800.104004, 49.067436, 120)',
      'GMR.DefineProfileCenter(-530.23626708984, 4806.9921875, 33.706157684326, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(19354)',
      'GMR.DefineQuestEnemyId(20680)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Cenarion Expedition |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9912, x = -314.819, y = 4711.439941, z = 18.420401, id = 16885 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.AllowSpeedUp()',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Sha'naar Relics |r",
    QuestID = 9430,
    QuestType = "Custom",
    PickUp = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    TurnIn = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9430, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-456.3977, 4678.874, 30.30479, 80)',
      'GMR.DefineProfileCenter(-508.9279, 4726.61, 32.59045, 80)',
      'GMR.DefineProfileCenter(-558.6781, 4744.349, 36.7476, 80)',
      'GMR.DefineProfileCenter(-525.6581, 4848.321, 35.63411, 80)',
      'GMR.DefineProfileCenter(-533.8954, 4858.07, 35.78514, 80)',
      'GMR.DefineProfileCenter(-535.6934, 4903.376, 38.25726, 80)',
      'GMR.DefineProfileCenter(-569.1011, 4849.133, 39.31391, 80)',
      'GMR.DefineProfileCenter(-604.7742, 4865.076, 39.5036, 80)',
      'GMR.DefineProfileCenter(-622.1879, 4826.762, 40.29654, 80)',
      'GMR.DefineProfileCenter(-618.7794, 4785.512, 39.82763, 80)',
      'GMR.DefineProfileCenter(-673.6985, 4753.471, 49.564, 80)',
      'GMR.DefineProfileCenter(-692.3516, 4785.728, 49.01603, 80)',
      'GMR.DefineProfileCenter(-642.5126, 4865.964, 53.34941, 80)',
      'GMR.DefineAreaBlacklist(-704.4334, 4781.68, 48.77222,15)',
      'GMR.DefineAreaBlacklist(-29.49377, 4595.726, 42.42378,45)',
      'GMR.DefineAreaBlacklist(80.8956, 4590.005, 66.26076,45)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(181637)',
      'GMR.DefineCustomObjectId(181637)',
      'GMR.DefineCustomObjectId(181800)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Cruel Taskmasters |r",
    QuestID = 9399,
    QuestType = "Custom",
    PickUp = { x = 96.144104003906, y = 4347.3134765625, z = 102.27414703369, id = 16799 },
    TurnIn = { x = 96.144104003906, y = 4347.3134765625, z = 102.27414703369, id = 16799 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9399, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      [[
          if not GMR.Frames.Rand023314 then
        GMR.Print("Frame created")
        GMR.Frames.Rand023314 = CreateFrame("frame")
        GMR.Frames.Rand023314:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9399 then
            if GMR.IsExecuting() and GMR.IsQuestActive(9399) then
              if GMR.Questing.IsObjectiveCompleted(9399, 1) then
                GossipTitleButton3:Click()
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand023314 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineProfileCenter(-451.1664, 4670.116, 31.34988, 60)',
      'GMR.DefineProfileCenter(-445.6966, 4752.076, 20.00525, 60)',
      'GMR.DefineProfileCenter(-439.8025, 4845.512, 27.43234, 60)',
      'GMR.DefineProfileCenter(-503.0187, 4850.53, 31.35462, 60)',
      'GMR.DefineProfileCenter(-558.7057, 4756.314, 35.79194, 60)',
      'GMR.DefineProfileCenter(-572.1396, 4853.187, 38.5723, 60)',
      'GMR.DefineAreaBlacklist(-661.8085, 4847.366, 50.46773,15)',
      'GMR.DefineAreaBlacklist(-677.1371, 4756.49, 49.47829,15)',
      'GMR.DefineAreaBlacklist(-704.4334, 4781.68, 48.77222,15)',
      'GMR.DefineAreaBlacklist(-29.49377, 4595.726, 42.42378,45)',
      'GMR.DefineAreaBlacklist(80.8956, 4590.005, 66.26076,45)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(17058)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Seer's Relic |r",
    QuestID = 9545,
    QuestType = "Custom",
    PickUp = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    TurnIn = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9545, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(191.6242, 4151.343, 73.51359, 10) then 
          GMR.MeshTo(191.6242, 4151.343, 73.51359)
        else
          local npc1 = GMR.GetObjectWithInfo({ id = 16852, rawType = 5 })
          local itemName = GetItemInfo(23645)
          if npc1 then
            GMR.TargetUnit(npc1)
            GMR.Use(itemName)
          end
        end
      end
    ]],
    Profile = {
    'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
    'GMR.DefineUnstuck(201.63229370117, 4248.154296875, 121.31951904297, 25)',
    'GMR.DefineUnstuck(197.94917297363, 4240.4306640625, 121.80669403076)',
    'GMR.DefineUnstuck(191.35754394531, 4230.1806640625, 120.53025054932)',
    'GMR.DefineUnstuck(189.478515625, 4196.1181640625, 80.033645629883)',
    'GMR.DefineUnstuck(191.85081481934, 4182.2700195312, 78.128707885742)',
    'GMR.DefineUnstuck(192.79547119141, 4169.529296875, 76.114753723145)',
    'GMR.DefineUnstuck(192.60023498535, 4154.7216796875, 74.659339904785)',
      'GMR.DefineProfileCenter(191.6242, 4151.343, 73.51359, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(19354)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Deadly Predators |r",
    QuestID = 9398,
    QuestType = "Custom",
    PickUp = { x = 171.785004, y = 4332.990234, z = 113.765999, id = 16797 },
    TurnIn = { x = 171.785004, y = 4332.990234, z = 113.765999, id = 16797 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9398, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 16929, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9398, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 16927, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-570.976929, 3864.632813, 31.862654, 80)',
      'GMR.DefineProfileCenter(-579.1493, 3883.134, 39.10646, 80)',
      'GMR.DefineProfileCenter(-725.005371, 3508.117432, 97.568375, 80)',
      'GMR.DefineProfileCenter(-746.9059, 4010.466, 30.39127, 80)',
      'GMR.DefineProfileCenter(-720.5511, 3945.784, 29.44855, 80)',
      'GMR.DefineProfileCenter(-616.912, 3932.303, 28.99596, 80)',
      'GMR.DefineProfileCenter(-581.1861, 3839.612, 28.99555, 80)',
      'GMR.DefineProfileCenter(-542.3076, 3619.121, 28.99579, 80)',
      'GMR.DefineProfileCenter(-697.9468, 3593.517, 29.17842, 80)',
      'GMR.DefineAreaBlacklist(-754.4313, 3812.172, 19.05555,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16927)',
      'GMR.DefineQuestEnemyId(16929)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Longbeards |r",
    QuestID = 9558,
    QuestType = "TalkTo",
    PickUp = { x = -708.870972, y = 2739.129883, z = 94.816803, id = 16826 },
    TurnIn = { x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      [[
      if not GMR.Frames.Rand03432 then
          GMR.Print("Frame created")
          GMR.Frames.Rand03432 = CreateFrame("frame")
          GMR.Frames.Rand03432:SetScript("OnUpdate", function(self)
              if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9558 then
                  if GMR.IsExecuting() then
                      if GossipFrameGreetingPanel:IsShown() then
                          GossipTitleButton3:Click()
                      end
                  end
              else 
                  self:SetScript("OnUpdate", nil); GMR.Frames.Rand03432 = nil; GMR.Print("Frame deleted")
              end
          end)
      end
      ]],
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(59.657073974609, 4332.1040039062, 95.92407989502, 30)',
      'GMR.DefineUnstuck(45.144069671631, 4329.005859375, 92.713302612305)',
      'GMR.DefineUnstuck(27.85980796814, 4326.669921875, 89.936264038086)',
      'GMR.DefineUnstuck(13.389314651489, 4322.7138671875, 88.409629821777)',
      'GMR.DefineUnstuck(4.5116295814514, 4309.0068359375, 86.435356140137)',
      'GMR.DefineUnstuck(-4.0126800537109, 4291.56640625, 84.497833251953)',
      'GMR.DefineUnstuck(-10.691184043884, 4274.4575195312, 84.415885925293)',
      'GMR.DefineUnstuck(-15.595476150513, 4259.7763671875, 85.072273254395)',
      'GMR.DefineUnstuck(-20.290397644043, 4245.7221679688, 85.398681640625)',
      'GMR.DefineUnstuck(-25.200839996338, 4231.0229492188, 85.54460144043)',
      'GMR.DefineUnstuck(-29.89575958252, 4216.96875, 84.023231506348)',
      'GMR.DefineUnstuck(-36.643920898438, 4205.1889648438, 83.49772644043)',
      'GMR.DefineUnstuck(-45.010448455811, 4193.1489257812, 83.340103149414)',
      'GMR.DefineUnstuck(-49.218116760254, 4180.017578125, 82.268203735352)',
      'GMR.DefineUnstuck(-51.514957427979, 4166.447265625, 80.963653564453)',
      'GMR.DefineUnstuck(-52.146953582764, 4152.9326171875, 80.917190551758)',
      'GMR.DefineUnstuck(-53.364414215088, 4138.5708007812, 81.578186035156)',
      'GMR.DefineUnstuck(-60.599925994873, 4127.9545898438, 82.59489440918)',
      'GMR.DefineUnstuck(-72.06665802002, 4121.185546875, 83.643852233887)',
      'GMR.DefineUnstuck(-84.789985656738, 4116.6328125, 86.50431060791)',
      'GMR.DefineUnstuck(-96.874885559082, 4112.30859375, 88.673942565918)',
      'GMR.DefineUnstuck(-109.64254760742, 4109.3256835938, 93.359870910645)',
      'GMR.DefineUnstuck(-124.06533050537, 4108.4594726562, 93.951141357422)',
      'GMR.DefineUnstuck(-137.90835571289, 4108.5302734375, 96.970016479492)',
      'GMR.DefineUnstuck(-152.06497192383, 4108.6020507812, 99.250854492188)',
      'GMR.DefineUnstuck(-167.7043762207, 4113.2260742188, 98.708961486816)',
      'GMR.DefineUnstuck(-185.69467163086, 4125.1879882812, 97.619132995605)',
      'GMR.DefineUnstuck(-200.64309692383, 4141.912109375, 96.913192749023)',
      'GMR.DefineUnstuck(-209.72410583496, 4160.2114257812, 96.697738647461)',
      'GMR.DefineUnstuck(-218.43170166016, 4177.7587890625, 97.140319824219)',
      'GMR.DefineUnstuck(-228.24456787109, 4197.533203125, 96.769721984863)',
      'GMR.DefineUnstuck(-234.63021850586, 4216.1279296875, 95.665657043457)',
      'GMR.DefineUnstuck(-239.33695983887, 4234.3110351562, 94.673835754395)',
      'GMR.DefineUnstuck(-251.2333984375, 4250.0512695312, 95.662712097168)',
      'GMR.DefineUnstuck(-267.35342407227, 4257.0034179688, 92.750213623047)',
      'GMR.DefineUnstuck(-284.23342895508, 4264.4418945312, 89.24471282959)',
      'GMR.DefineUnstuck(-300.38006591797, 4272.5854492188, 86.255920410156)',
      'GMR.DefineUnstuck(-316.19519042969, 4281.9223632812, 81.873390197754)',
      'GMR.DefineUnstuck(-330.1530456543, 4293.27734375, 76.659400939941)',
      'GMR.DefineUnstuck(-346.67755126953, 4303.501953125, 72.213981628418)',
      'GMR.DefineUnstuck(-364.99847412109, 4306.3935546875, 69.937103271484)',
      'GMR.DefineUnstuck(-382.8717956543, 4303.6450195312, 68.844924926758)',
      'GMR.DefineUnstuck(-399.31842041016, 4300.2729492188, 68.133804321289)',
      'GMR.DefineUnstuck(-415.11511230469, 4302.259765625, 64.85994720459)',
      'GMR.DefineUnstuck(-430.93438720703, 4306.8217773438, 61.558429718018)',
      'GMR.DefineUnstuck(-447.10876464844, 4311.486328125, 56.146785736084)',
      'GMR.DefineUnstuck(-462.21127319336, 4314.654296875, 51.155689239502)',
      'GMR.DefineUnstuck(-478.18856811523, 4316.9345703125, 40.16556930542)',
      'GMR.DefineUnstuck(-491.3122253418, 4317.2548828125, 41.616371154785)',
      'GMR.DefineUnstuck(-507.08044433594, 4318.3403320312, 42.944301605225)',
      'GMR.DefineUnstuck(-522.1982421875, 4319.380859375, 43.770114898682)',
      'GMR.DefineUnstuck(-537.64013671875, 4320.4438476562, 45.077865600586)',
      'GMR.DefineUnstuck(-553.67718505859, 4320.3100585938, 46.196769714355)',
      'GMR.DefineUnstuck(-569.95489501953, 4318.2084960938, 46.849430084229)',
      'GMR.DefineUnstuck(-586.05841064453, 4317.619140625, 48.950817108154)',
      'GMR.DefineUnstuck(-602.49652099609, 4317.7661132812, 50.82332611084)',
      'GMR.DefineUnstuck(-617.87365722656, 4319.296875, 52.10391998291)',
      'GMR.DefineUnstuck(-635.29376220703, 4319.31640625, 53.309772491455)',
      'GMR.DefineUnstuck(-657.85345458984, 4314.9624023438, 52.829822540283)',
      'GMR.DefineUnstuck(-678.15240478516, 4306.5263671875, 50.688945770264)',
      'GMR.DefineUnstuck(-698.15063476562, 4298.1215820312, 50.614082336426)',
      'GMR.DefineUnstuck(-716.45721435547, 4287.8876953125, 50.612503051758)',
      'GMR.DefineUnstuck(-732.4404296875, 4273.0361328125, 48.97579574585)',
      'GMR.DefineUnstuck(-739.24877929688, 4255.2626953125, 49.01615524292)',
      'GMR.DefineUnstuck(-752.31127929688, 4235.8310546875, 48.665138244629)',
      'GMR.DefineUnstuck(-770.72424316406, 4216.5859375, 46.903316497803)',
      'GMR.DefineUnstuck(-788.46893310547, 4199.25390625, 44.546203613281)',
      'GMR.DefineUnstuck(-812.63452148438, 4185.3754882812, 47.520290374756)',
      'GMR.DefineUnstuck(-832.44915771484, 4176.0971679688, 49.271575927734)',
      'GMR.DefineUnstuck(-850.14056396484, 4170.94140625, 47.825401306152)',
      'GMR.DefineUnstuck(-870.09326171875, 4175.7524414062, 47.132667541504)',
      'GMR.DefineUnstuck(-884.85040283203, 4190.4252929688, 46.577186584473)',
      'GMR.DefineUnstuck(-898.03594970703, 4206.8701171875, 43.584224700928)',
      'GMR.DefineUnstuck(-914.521484375, 4227.4306640625, 36.315299987793)',
      'GMR.DefineUnstuck(-926.88360595703, 4242.8486328125, 38.861320495605)',
      'GMR.DefineUnstuck(-937.37200927734, 4254.2314453125, 43.194038391113)',
      'GMR.DefineUnstuck(-947.88659667969, 4264.23828125, 47.466857910156)',
      'GMR.DefineUnstuck(-961.02252197266, 4269.5576171875, 51.514263153076)',
      'GMR.DefineUnstuck(-974.98468017578, 4276.4838867188, 56.505939483643)',
      'GMR.DefineUnstuck(-986.11889648438, 4285.7612304688, 60.879970550537)',
      'GMR.DefineUnstuck(-998.31726074219, 4299.8671875, 66.011489868164)',
      'GMR.DefineUnstuck(-1003.5551147461, 4305.3291015625, 67.954170227051)',  
      'GMR.DefineProfileCenter(-1003.979980, 4305.970215, 68.072304, 5)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Rampaging Ravagers / The Arakkoa Threat  |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9385, x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
      { questId = 9417, x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Avruu's Orb |r - FarmItemAndAcceptQuest",
    QuestID = 9418,
    QuestType = "Grinding",
    PickUp = { x = -1149.900024, y = 4214.390137, z = 54.475498, id = { 17084, 23580 } },
    TurnIn = { x = -1323.544434, y = 4043.031738, z = 116.494415, id = 17085 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9418, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand025454 then
        GMR.Print("Frame created")
        GMR.Frames.Rand025454 = CreateFrame("frame")
        GMR.Frames.Rand025454:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9418 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if GetItemCount(23580) >= 1 and not GMR.IsQuestActive(9418) then
                local itemName = GetItemInfo(23580)
                GMR.Use(itemName)
                if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() then
                  QuestFrameAcceptButton:Click()
                end
              elseif GMR.IsQuestActive(9418) then
                if GMR.IsPlayerPosition(-1323.776001, 4046.278076, 115.964401, 3) then 
                  GMR.MeshTo(-1323.776001, 4046.278076, 115.964401)
                else
                  local object1 = GMR.GetObjectWithInfo({ id = 181606, rawType = 8 })
                  local npc1 = GMR.GetObjectWithInfo({ id = 17085, rawType = 5 })
                  if not npc1 then
                    GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
                  elseif npc1 then
                    GossipTitleButton1:Click()
                    GMR.RunMacroText("/run SelectGossipOption(1)")
                    GMR.Questing.GossipWith(npc1, nil, nil, nil, nil, 5)
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand025454 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1149.900024, 4214.390137, 54.475498, 60)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Arakkoa Threat |r",
    QuestID = 9417,
    QuestType = "Custom",
    PickUp = { x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
    TurnIn = { x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9417, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 16966, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9417, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 16967, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-973.448, 4186.707, 24.37038, 80)',
      'GMR.DefineProfileCenter(-1085.533, 4194.947, 16.77508, 80)',
      'GMR.DefineProfileCenter(-1216.44, 4183.769, 39.42722, 80)',
      'GMR.DefineProfileCenter(-1144.984, 4216.39, 53.97621, 80)',
      'GMR.DefineProfileCenter(-1225.553, 4071.093, 71.88353, 80)',
      'GMR.DefineProfileCenter(-1284.431, 4067.691, 93.55517, 80)',
      'GMR.DefineProfileCenter(-1224.818, 4152.996, 50.43114, 80)',
      'GMR.DefineProfileCenter(-1073.366, 4199.294, 16.28948, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16967)',
      'GMR.DefineQuestEnemyId(16966)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Rampaging Ravagers |r",
    QuestID = 9385,
    QuestType = "Custom",
    PickUp = { x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
    TurnIn = { x = -1003.97998, y = 4305.970215, z = 68.072304, id = 16850 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9385, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-880.9067, 4352.188, 55.62366, 80)',
      'GMR.DefineProfileCenter(-841.3847, 4414.221, 78.05861, 80)',
      'GMR.DefineProfileCenter(-792.5474, 4410.002, 78.60834, 80)',
      'GMR.DefineProfileCenter(-707.0097, 4421.127, 85.81408, 80)',
      'GMR.DefineProfileCenter(-634.7007, 4381.628, 68.4485, 80)',
      'GMR.DefineProfileCenter(-725.1516, 4384.579, 72.89629, 80)',
      'GMR.DefineProfileCenter(-811.3876, 4355.233, 60.1034, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(19189)',
      'GMR.DefineQuestEnemyId(16934)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Heart of Darkness |r",
    QuestID = 10399,
    QuestType = "Custom",
    PickUp = { x = -809.40454101562, y = 2611.5910644531, z = 124.38919067383, id = 16839 },
    TurnIn = { x = -809.40454101562, y = 2611.5910644531, z = 124.38919067383, id = 16839 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10399, 1) then
          GMR.SetQuestingState("Idle")
      end
      ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(323.97836303711, 3050.6022949219, 22.966484069824, 150)',
      'GMR.DefineUnstuck(309.67529296875, 3056.7590332031, 23.54647064209)',
      'GMR.DefineUnstuck(295.30316162109, 3061.8044433594, 23.217712402344)',
      'GMR.DefineUnstuck(282.48980712891, 3066.302734375, 22.452085494995)',
      'GMR.DefineUnstuck(269.19689941406, 3072.7019042969, 21.709043502808)',
      'GMR.DefineUnstuck(244.60723876953, 3083.9760742188, 20.85195350647)',
      'GMR.DefineUnstuck(230.54006958008, 3092.5036621094, 21.902698516846)',
      'GMR.DefineUnstuck(219.19140625, 3101.7631835938, 23.778169631958)',
      'GMR.DefineUnstuck(209.36474609375, 3113.9282226562, 26.570053100586)',
      'GMR.DefineUnstuck(197.94067382812, 3121.4694824219, 27.617574691772)',
      'GMR.DefineUnstuck(183.54524230957, 3124.1018066406, 26.473207473755)',
      'GMR.DefineUnstuck(167.32028198242, 3122.0732421875, 24.17476272583)',
      'GMR.DefineUnstuck(154.03559875488, 3119.1896972656, 22.138004302979)',
      'GMR.DefineUnstuck(142.41398620605, 3111.2021484375, 19.141036987305)',
      'GMR.DefineUnstuck(131.75709533691, 3100.5036621094, 15.460600852966)',
      'GMR.DefineUnstuck(119.68088531494, 3096.1010742188, 13.501254081726)',
      'GMR.DefineUnstuck(106.62142944336, 3096.3610839844, 12.976165771484)',
      'GMR.DefineUnstuck(93.022613525391, 3100.8012695312, 11.93945980072)',
      'GMR.DefineUnstuck(80.766021728516, 3108.2409667969, 10.910000801086)',
      'GMR.DefineUnstuck(68.577323913574, 3114.8796386719, 5.3088622093201)',
      'GMR.DefineUnstuck(54.078468322754, 3116.17578125, 1.4641733169556)',
      'GMR.DefineUnstuck(38.900291442871, 3115.0756835938, -1.2142399549484)',
      'GMR.DefineUnstuck(20.817733764648, 3113.7648925781, -1.2221149206161)',
      'GMR.DefineUnstuck(2.3302383422852, 3112.4248046875, -1.2221149206161)',
      'GMR.DefineUnstuck(-9.2223615646362, 3122.2712402344, -1.2222893238068)',
      'GMR.DefineUnstuck(-21.50288772583, 3143.2709960938, -1.2222893238068)',
      'GMR.DefineUnstuck(-30.993766784668, 3161.4487304688, -0.92479354143143)',
      'GMR.DefineUnstuck(-41.090148925781, 3174.9812011719, -0.55073654651642)',
      'GMR.DefineUnstuck(-48.69909286499, 3188.0161132812, 0.86446410417557)',
      'GMR.DefineUnstuck(-52.710285186768, 3202.9987792969, 4.3451452255249)',
      'GMR.DefineUnstuck(-52.471530914307, 3220.212890625, 7.8655376434326)',
      'GMR.DefineUnstuck(-53.066345214844, 3233.3308105469, 11.034104347229)',
      'GMR.DefineUnstuck(-53.871673583984, 3246.8935546875, 15.165593147278)',
      'GMR.DefineUnstuck(-54.160293579102, 3262.1228027344, 19.979940414429)',
      'GMR.DefineUnstuck(-54.480289459229, 3279.0078125, 24.57439994812)',
      'GMR.DefineUnstuck(-53.240703582764, 3293.6870117188, 28.356273651123)',
      'GMR.DefineUnstuck(-51.604278564453, 3307.5263671875, 32.057514190674)',
      'GMR.DefineUnstuck(-51.599903106689, 3323.1208496094, 36.360397338867)',
      'GMR.DefineUnstuck(-52.008575439453, 3339.1311035156, 41.389205932617)',
      'GMR.DefineUnstuck(-52.461750030518, 3353.0925292969, 45.746170043945)',
      'GMR.DefineUnstuck(-51.729934692383, 3367.900390625, 49.397365570068)',
      'GMR.DefineUnstuck(-51.038890838623, 3381.8833007812, 53.562191009521)',
      'GMR.DefineUnstuck(-50.266304016113, 3397.5163574219, 58.652179718018)',
      'GMR.DefineUnstuck(-49.51513671875, 3412.7158203125, 62.496257781982)',
      'GMR.DefineUnstuck(-48.742343902588, 3428.3527832031, 66.44270324707)',
  
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(57.661842346191, 4332.140625, 95.38215637207, 20)',
      'GMR.DefineUnstuck(38.132480621338, 4320.1162109375, 91.179145812988)',
      'GMR.DefineUnstuck(25.801908493042, 4305.0122070312, 89.94995880127)',
      'GMR.DefineUnstuck(14.032560348511, 4286.1674804688, 87.447547912598)',
      'GMR.DefineUnstuck(0.93573379516602, 4265.197265625, 86.408828735352)',
      'GMR.DefineUnstuck(-7.6100721359253, 4243.4970703125, 84.644905090332)',
      'GMR.DefineUnstuck(-15.242209434509, 4216.9794921875, 82.454200744629)',
      'GMR.DefineUnstuck(-23.592479705811, 4197, 81.005111694336)',
      'GMR.DefineUnstuck(-37.659698486328, 4176.4360351562, 80.021522521973)',
      'GMR.DefineUnstuck(-43.763534545898, 4156.4897460938, 79.563125610352)',
      'GMR.DefineUnstuck(-47.656192779541, 4132.5234375, 81.088722229004)',
      'GMR.DefineUnstuck(-51.132457733154, 4108.072265625, 80.346389770508)',
      'GMR.DefineUnstuck(-54.261775970459, 4086.0615234375, 83.864036560059)',
      'GMR.DefineUnstuck(-60.642742156982, 4063.1691894531, 88.790176391602)',
      'GMR.DefineUnstuck(-69.077796936035, 4039.9584960938, 97.13028717041)',
      'GMR.DefineUnstuck(-79.482940673828, 4011.3264160156, 100.17750549316)',
      'GMR.DefineUnstuck(-92.513374328613, 3981.8325195312, 100.26639556885)',
      'GMR.DefineUnstuck(-105.67876434326, 3957.5705566406, 99.647956848145)',
      'GMR.DefineUnstuck(-112.5747756958, 3928.8544921875, 95.941940307617)',
      'GMR.DefineUnstuck(-119.40774536133, 3904.6857910156, 92.466499328613)',
      'GMR.DefineUnstuck(-126.24071502686, 3880.5173339844, 89.47135925293)',
      'GMR.DefineUnstuck(-133.52311706543, 3854.7587890625, 85.927879333496)',
      'GMR.DefineUnstuck(-139.23492431641, 3828.3662109375, 82.563049316406)',
      'GMR.DefineUnstuck(-129.4409942627, 3798.6533203125, 76.288719177246)',
      'GMR.DefineUnstuck(-115.35123443604, 3772.1022949219, 71.027168273926)',
      'GMR.DefineUnstuck(-99.975814819336, 3740.7397460938, 69.047592163086)',
      'GMR.DefineUnstuck(-89.480453491211, 3712.0961914062, 66.938804626465)',
      'GMR.DefineUnstuck(-84.237922668457, 3679.7724609375, 65.560272216797)',
      'GMR.DefineUnstuck(-77.482055664062, 3648.7302246094, 66.098205566406)',
      'GMR.DefineUnstuck(-59.923645019531, 3616.82421875, 72.00611114502)',
      'GMR.DefineUnstuck(-46.556621551514, 3585.2927246094, 73.814559936523)',
      'GMR.DefineUnstuck(-46.185981750488, 3562.5390625, 73.579414367676)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(204.87678527832, 4249.029296875, 120.71855926514, 25)',
      'GMR.DefineUnstuck(205.20516967773, 4259.6137695312, 120.23365783691)',
      'GMR.DefineUnstuck(202.68841552734, 4274.1928710938, 119.10501098633)',
      'GMR.DefineUnstuck(197.00305175781, 4288.1674804688, 118.21218109131)',
      'GMR.DefineUnstuck(189.90100097656, 4301.1977539062, 116.98760986328)',
      'GMR.DefineUnstuck(189.29832458496, 4314.7041015625, 116.51760101318)',
      'GMR.DefineUnstuck(189.74128723145, 4326.951171875, 116.52184295654)',
      'GMR.DefineUnstuck(179.39805603027, 4334.0766601562, 116.39238739014)',
      'GMR.DefineUnstuck(165.00849914551, 4334.1821289062, 111.64892578125)',
      'GMR.DefineUnstuck(148.92161560059, 4333.5322265625, 106.76062011719)',
      'GMR.DefineUnstuck(132.91865539551, 4332.8862304688, 106.45709228516)',
      'GMR.DefineUnstuck(118.10472106934, 4332.2875976562, 102.7107925415)',
      'GMR.DefineUnstuck(102.87113952637, 4331.6723632812, 101.46556091309)',
      'GMR.DefineUnstuck(88.50846862793, 4331.7661132812, 101.46556091309)',
      'GMR.DefineUnstuck(74.145240783691, 4331.7255859375, 101.50704193115)',
      'GMR.DefineUnstuck(63.315258026123, 4331.4130859375, 96.868797302246)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(193.95178222656, 3938.7644042969, 77.02375793457, 80)',
      'GMR.DefineUnstuck(174.50480651855, 3946.119140625, 77.100463867188)',
      'GMR.DefineUnstuck(149.97203063965, 3948.1752929688, 79.036109924316)',
      'GMR.DefineUnstuck(126.51438140869, 3948.7211914062, 77.960227966309)',
      'GMR.DefineUnstuck(101.89168548584, 3948.3588867188, 76.13111114502)',
      'GMR.DefineUnstuck(85.309768676758, 3944.1459960938, 77.409309387207)',
      'GMR.DefineUnstuck(75.616241455078, 3931.0593261719, 79.601829528809)',
      'GMR.DefineUnstuck(63.763820648193, 3924.2333984375, 81.067092895508)',
      'GMR.DefineUnstuck(39.008518218994, 3919.9184570312, 83.911102294922)',
      'GMR.DefineUnstuck(18.60675239563, 3915.7314453125, 85.498207092285)',
      'GMR.DefineUnstuck(3.2077100276947, 3902.2924804688, 85.96996307373)',
      'GMR.DefineUnstuck(-17.735523223877, 3894.1833496094, 84.662544250488)',
      'GMR.DefineUnstuck(-38.712661743164, 3890.3488769531, 87.04801940918)',
      'GMR.DefineUnstuck(-60.633201599121, 3879.3247070312, 88.521347045898)',
      'GMR.DefineUnstuck(-78.320625305176, 3858.5451660156, 85.082618713379)',
      'GMR.DefineUnstuck(-91.559494018555, 3835.5930175781, 81.819549560547)',
      'GMR.DefineUnstuck(-100.26367950439, 3811.5712890625, 78.540550231934)',
      'GMR.DefineUnstuck(-107.41409301758, 3785.912109375, 74.54549407959)',
      'GMR.DefineUnstuck(-109.73361206055, 3769.2023925781, 70.857536315918)',
      'GMR.DefineUnstuck(-107.43981933594, 3755.7180175781, 69.670532226562)',
      'GMR.DefineUnstuck(-101.1815032959, 3742.294921875, 69.33438873291)',
      'GMR.DefineUnstuck(-95.545623779297, 3728.1887207031, 67.61213684082)',
      'GMR.DefineUnstuck(-91.818840026855, 3713.0690917969, 66.706428527832)',
      'GMR.DefineUnstuck(-89.762908935547, 3697.9763183594, 66.145195007324)',
      'GMR.DefineUnstuck(-88.039558410645, 3685.3251953125, 65.239288330078)',
      'GMR.DefineUnstuck(-86.427696228027, 3673.4924316406, 64.54475402832)',
      'GMR.DefineUnstuck(-82.980583190918, 3660.4255371094, 64.707656860352)',
      'GMR.DefineUnstuck(-78.40274810791, 3647.5959472656, 66.149765014648)',
      'GMR.DefineUnstuck(-72.205978393555, 3636.0017089844, 68.437324523926)',
      'GMR.DefineUnstuck(-64.513168334961, 3624.3820800781, 71.124984741211)',
      'GMR.DefineUnstuck(-57.107879638672, 3612.5378417969, 72.459785461426)',
      'GMR.DefineUnstuck(-51.833656311035, 3599.2102050781, 73.394302368164)',
      'GMR.DefineUnstuck(-47.972721099854, 3585.7385253906, 73.691917419434)',
      'GMR.DefineUnstuck(-45.66983795166, 3572.0002441406, 73.608192443848)',
      'GMR.DefineUnstuck(-44.726146697998, 3558.8740234375, 73.503349304199)',
      'GMR.DefineProfileCenter(103.2847, 3616.754, 75.33942, 40)',
      'GMR.DefineProfileCenter(343.8433, 3507.732, 65.5368, 80)',
      'GMR.DefineProfileCenter(404.4964, 3531.009, 67.78629, 80)',
      'GMR.DefineProfileCenter(466.9339, 3530.229, 64.32405, 80)',
      'GMR.DefineProfileCenter(524.2275, 3478.08, 76.17835, 80)',
      'GMR.DefineProfileCenter(475.707, 3392.144, 73.53825, 80)',
      'GMR.DefineProfileCenter(347.7976, 3376.632, 62.03331, 80)',
      'GMR.DefineProfileCenter(296.9072, 3423.92, 68.68301, 80)',
      'GMR.DefineProfileCenter(211.4638, 3422.559, 69.02387, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16951)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE The Heart of Darkness |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10399, x = -809.40454101562, y = 2611.5910644531, z = 124.38919067383, id = 16839 }
    },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Profile = {
      'GMR.SkipTurnIn(false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(-49.148349761963, 3209.728515625, 5.5865983963013, 40)',
        'GMR.DefineUnstuck(-49.355632781982, 3188.83203125, 1.0132489204407)',
        'GMR.DefineUnstuck(-41.962795257568, 3177.2399902344, -0.3438001871109)',
        'GMR.DefineUnstuck(-34.341171264648, 3166.8220214844, -0.78004676103592)',
        'GMR.DefineUnstuck(-33.862018585205, 3153.4956054688, -1.5212464332581)',
        'GMR.DefineUnstuck(-35.696731567383, 3139.1928710938, -1.338996052742)',
        'GMR.DefineUnstuck(-40.31912612915, 3122.6491699219, -1.6359777450562)',
        'GMR.DefineUnstuck(-52.141719818115, 3107.4431152344, -2.4517805576324)',
        'GMR.DefineUnstuck(-63.287666320801, 3096.6452636719, -2.8852896690369)',
        'GMR.DefineUnstuck(-75.426025390625, 3086.7858886719, -3.3677616119385)',
        'GMR.DefineUnstuck(-87.586112976074, 3076.9089355469, -2.8270618915558)',
        'GMR.DefineUnstuck(-97.469482421875, 3066.7280273438, -1.9667536020279)',
        'GMR.DefineUnstuck(-105.55224609375, 3050.9702148438, -1.1784375905991)',
        'GMR.DefineUnstuck(-113.63063812256, 3035.220703125, 0.15347370505333)',
        'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
        'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
        'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
        'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
        'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Pools of Aggonar |r",
    QuestID = 9426,
    QuestType = "Custom",
    PickUp = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    TurnIn = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9426, 1) then
          GMR.SetQuestingState("Idle")
      end
      ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(323.97836303711, 3050.6022949219, 22.966484069824, 150)',
      'GMR.DefineUnstuck(309.67529296875, 3056.7590332031, 23.54647064209)',
      'GMR.DefineUnstuck(295.30316162109, 3061.8044433594, 23.217712402344)',
      'GMR.DefineUnstuck(282.48980712891, 3066.302734375, 22.452085494995)',
      'GMR.DefineUnstuck(269.19689941406, 3072.7019042969, 21.709043502808)',
      'GMR.DefineUnstuck(244.60723876953, 3083.9760742188, 20.85195350647)',
      'GMR.DefineUnstuck(230.54006958008, 3092.5036621094, 21.902698516846)',
      'GMR.DefineUnstuck(219.19140625, 3101.7631835938, 23.778169631958)',
      'GMR.DefineUnstuck(209.36474609375, 3113.9282226562, 26.570053100586)',
      'GMR.DefineUnstuck(197.94067382812, 3121.4694824219, 27.617574691772)',
      'GMR.DefineUnstuck(183.54524230957, 3124.1018066406, 26.473207473755)',
      'GMR.DefineUnstuck(167.32028198242, 3122.0732421875, 24.17476272583)',
      'GMR.DefineUnstuck(154.03559875488, 3119.1896972656, 22.138004302979)',
      'GMR.DefineUnstuck(142.41398620605, 3111.2021484375, 19.141036987305)',
      'GMR.DefineUnstuck(131.75709533691, 3100.5036621094, 15.460600852966)',
      'GMR.DefineUnstuck(119.68088531494, 3096.1010742188, 13.501254081726)',
      'GMR.DefineUnstuck(106.62142944336, 3096.3610839844, 12.976165771484)',
      'GMR.DefineUnstuck(93.022613525391, 3100.8012695312, 11.93945980072)',
      'GMR.DefineUnstuck(80.766021728516, 3108.2409667969, 10.910000801086)',
      'GMR.DefineUnstuck(68.577323913574, 3114.8796386719, 5.3088622093201)',
      'GMR.DefineUnstuck(54.078468322754, 3116.17578125, 1.4641733169556)',
      'GMR.DefineUnstuck(38.900291442871, 3115.0756835938, -1.2142399549484)',
      'GMR.DefineUnstuck(20.817733764648, 3113.7648925781, -1.2221149206161)',
      'GMR.DefineUnstuck(2.3302383422852, 3112.4248046875, -1.2221149206161)',
      'GMR.DefineUnstuck(-9.2223615646362, 3122.2712402344, -1.2222893238068)',
      'GMR.DefineUnstuck(-21.50288772583, 3143.2709960938, -1.2222893238068)',
      'GMR.DefineUnstuck(-30.993766784668, 3161.4487304688, -0.92479354143143)',
      'GMR.DefineUnstuck(-41.090148925781, 3174.9812011719, -0.55073654651642)',
      'GMR.DefineUnstuck(-48.69909286499, 3188.0161132812, 0.86446410417557)',
      'GMR.DefineUnstuck(-52.710285186768, 3202.9987792969, 4.3451452255249)',
      'GMR.DefineUnstuck(-52.471530914307, 3220.212890625, 7.8655376434326)',
      'GMR.DefineUnstuck(-53.066345214844, 3233.3308105469, 11.034104347229)',
      'GMR.DefineUnstuck(-53.871673583984, 3246.8935546875, 15.165593147278)',
      'GMR.DefineUnstuck(-54.160293579102, 3262.1228027344, 19.979940414429)',
      'GMR.DefineUnstuck(-54.480289459229, 3279.0078125, 24.57439994812)',
      'GMR.DefineUnstuck(-53.240703582764, 3293.6870117188, 28.356273651123)',
      'GMR.DefineUnstuck(-51.604278564453, 3307.5263671875, 32.057514190674)',
      'GMR.DefineUnstuck(-51.599903106689, 3323.1208496094, 36.360397338867)',
      'GMR.DefineUnstuck(-52.008575439453, 3339.1311035156, 41.389205932617)',
      'GMR.DefineUnstuck(-52.461750030518, 3353.0925292969, 45.746170043945)',
      'GMR.DefineUnstuck(-51.729934692383, 3367.900390625, 49.397365570068)',
      'GMR.DefineUnstuck(-51.038890838623, 3381.8833007812, 53.562191009521)',
      'GMR.DefineUnstuck(-50.266304016113, 3397.5163574219, 58.652179718018)',
      'GMR.DefineUnstuck(-49.51513671875, 3412.7158203125, 62.496257781982)',
      'GMR.DefineUnstuck(-48.742343902588, 3428.3527832031, 66.44270324707)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(323.97836303711, 3050.6022949219, 22.966484069824, 150)',
      'GMR.DefineUnstuck(309.67529296875, 3056.7590332031, 23.54647064209)',
      'GMR.DefineUnstuck(295.30316162109, 3061.8044433594, 23.217712402344)',
      'GMR.DefineUnstuck(282.48980712891, 3066.302734375, 22.452085494995)',
      'GMR.DefineUnstuck(269.19689941406, 3072.7019042969, 21.709043502808)',
      'GMR.DefineUnstuck(244.60723876953, 3083.9760742188, 20.85195350647)',
      'GMR.DefineUnstuck(230.54006958008, 3092.5036621094, 21.902698516846)',
      'GMR.DefineUnstuck(219.19140625, 3101.7631835938, 23.778169631958)',
      'GMR.DefineUnstuck(209.36474609375, 3113.9282226562, 26.570053100586)',
      'GMR.DefineUnstuck(197.94067382812, 3121.4694824219, 27.617574691772)',
      'GMR.DefineUnstuck(183.54524230957, 3124.1018066406, 26.473207473755)',
      'GMR.DefineUnstuck(167.32028198242, 3122.0732421875, 24.17476272583)',
      'GMR.DefineUnstuck(154.03559875488, 3119.1896972656, 22.138004302979)',
      'GMR.DefineUnstuck(142.41398620605, 3111.2021484375, 19.141036987305)',
      'GMR.DefineUnstuck(131.75709533691, 3100.5036621094, 15.460600852966)',
      'GMR.DefineUnstuck(119.68088531494, 3096.1010742188, 13.501254081726)',
      'GMR.DefineUnstuck(106.62142944336, 3096.3610839844, 12.976165771484)',
      'GMR.DefineUnstuck(93.022613525391, 3100.8012695312, 11.93945980072)',
      'GMR.DefineUnstuck(80.766021728516, 3108.2409667969, 10.910000801086)',
      'GMR.DefineUnstuck(68.577323913574, 3114.8796386719, 5.3088622093201)',
      'GMR.DefineUnstuck(54.078468322754, 3116.17578125, 1.4641733169556)',
      'GMR.DefineUnstuck(38.900291442871, 3115.0756835938, -1.2142399549484)',
      'GMR.DefineUnstuck(20.817733764648, 3113.7648925781, -1.2221149206161)',
      'GMR.DefineUnstuck(2.3302383422852, 3112.4248046875, -1.2221149206161)',
      'GMR.DefineUnstuck(-9.2223615646362, 3122.2712402344, -1.2222893238068)',
      'GMR.DefineUnstuck(-21.50288772583, 3143.2709960938, -1.2222893238068)',
      'GMR.DefineUnstuck(-30.993766784668, 3161.4487304688, -0.92479354143143)',
      'GMR.DefineUnstuck(-41.090148925781, 3174.9812011719, -0.55073654651642)',
      'GMR.DefineUnstuck(-48.69909286499, 3188.0161132812, 0.86446410417557)',
      'GMR.DefineUnstuck(-52.710285186768, 3202.9987792969, 4.3451452255249)',
      'GMR.DefineUnstuck(-52.471530914307, 3220.212890625, 7.8655376434326)',
      'GMR.DefineUnstuck(-53.066345214844, 3233.3308105469, 11.034104347229)',
      'GMR.DefineUnstuck(-53.871673583984, 3246.8935546875, 15.165593147278)',
      'GMR.DefineUnstuck(-54.160293579102, 3262.1228027344, 19.979940414429)',
      'GMR.DefineUnstuck(-54.480289459229, 3279.0078125, 24.57439994812)',
      'GMR.DefineUnstuck(-53.240703582764, 3293.6870117188, 28.356273651123)',
      'GMR.DefineUnstuck(-51.604278564453, 3307.5263671875, 32.057514190674)',
      'GMR.DefineUnstuck(-51.599903106689, 3323.1208496094, 36.360397338867)',
      'GMR.DefineUnstuck(-52.008575439453, 3339.1311035156, 41.389205932617)',
      'GMR.DefineUnstuck(-52.461750030518, 3353.0925292969, 45.746170043945)',
      'GMR.DefineUnstuck(-51.729934692383, 3367.900390625, 49.397365570068)',
      'GMR.DefineUnstuck(-51.038890838623, 3381.8833007812, 53.562191009521)',
      'GMR.DefineUnstuck(-50.266304016113, 3397.5163574219, 58.652179718018)',
      'GMR.DefineUnstuck(-49.51513671875, 3412.7158203125, 62.496257781982)',
      'GMR.DefineUnstuck(-48.742343902588, 3428.3527832031, 66.44270324707)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(57.661842346191, 4332.140625, 95.38215637207, 20)',
      'GMR.DefineUnstuck(38.132480621338, 4320.1162109375, 91.179145812988)',
      'GMR.DefineUnstuck(25.801908493042, 4305.0122070312, 89.94995880127)',
      'GMR.DefineUnstuck(14.032560348511, 4286.1674804688, 87.447547912598)',
      'GMR.DefineUnstuck(0.93573379516602, 4265.197265625, 86.408828735352)',
      'GMR.DefineUnstuck(-7.6100721359253, 4243.4970703125, 84.644905090332)',
      'GMR.DefineUnstuck(-15.242209434509, 4216.9794921875, 82.454200744629)',
      'GMR.DefineUnstuck(-23.592479705811, 4197, 81.005111694336)',
      'GMR.DefineUnstuck(-37.659698486328, 4176.4360351562, 80.021522521973)',
      'GMR.DefineUnstuck(-43.763534545898, 4156.4897460938, 79.563125610352)',
      'GMR.DefineUnstuck(-47.656192779541, 4132.5234375, 81.088722229004)',
      'GMR.DefineUnstuck(-51.132457733154, 4108.072265625, 80.346389770508)',
      'GMR.DefineUnstuck(-54.261775970459, 4086.0615234375, 83.864036560059)',
      'GMR.DefineUnstuck(-60.642742156982, 4063.1691894531, 88.790176391602)',
      'GMR.DefineUnstuck(-69.077796936035, 4039.9584960938, 97.13028717041)',
      'GMR.DefineUnstuck(-79.482940673828, 4011.3264160156, 100.17750549316)',
      'GMR.DefineUnstuck(-92.513374328613, 3981.8325195312, 100.26639556885)',
      'GMR.DefineUnstuck(-105.67876434326, 3957.5705566406, 99.647956848145)',
      'GMR.DefineUnstuck(-112.5747756958, 3928.8544921875, 95.941940307617)',
      'GMR.DefineUnstuck(-119.40774536133, 3904.6857910156, 92.466499328613)',
      'GMR.DefineUnstuck(-126.24071502686, 3880.5173339844, 89.47135925293)',
      'GMR.DefineUnstuck(-133.52311706543, 3854.7587890625, 85.927879333496)',
      'GMR.DefineUnstuck(-139.23492431641, 3828.3662109375, 82.563049316406)',
      'GMR.DefineUnstuck(-129.4409942627, 3798.6533203125, 76.288719177246)',
      'GMR.DefineUnstuck(-115.35123443604, 3772.1022949219, 71.027168273926)',
      'GMR.DefineUnstuck(-99.975814819336, 3740.7397460938, 69.047592163086)',
      'GMR.DefineUnstuck(-89.480453491211, 3712.0961914062, 66.938804626465)',
      'GMR.DefineUnstuck(-84.237922668457, 3679.7724609375, 65.560272216797)',
      'GMR.DefineUnstuck(-77.482055664062, 3648.7302246094, 66.098205566406)',
      'GMR.DefineUnstuck(-59.923645019531, 3616.82421875, 72.00611114502)',
      'GMR.DefineUnstuck(-46.556621551514, 3585.2927246094, 73.814559936523)',
      'GMR.DefineUnstuck(-46.185981750488, 3562.5390625, 73.579414367676)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(204.87678527832, 4249.029296875, 120.71855926514, 25)',
      'GMR.DefineUnstuck(205.20516967773, 4259.6137695312, 120.23365783691)',
      'GMR.DefineUnstuck(202.68841552734, 4274.1928710938, 119.10501098633)',
      'GMR.DefineUnstuck(197.00305175781, 4288.1674804688, 118.21218109131)',
      'GMR.DefineUnstuck(189.90100097656, 4301.1977539062, 116.98760986328)',
      'GMR.DefineUnstuck(189.29832458496, 4314.7041015625, 116.51760101318)',
      'GMR.DefineUnstuck(189.74128723145, 4326.951171875, 116.52184295654)',
      'GMR.DefineUnstuck(179.39805603027, 4334.0766601562, 116.39238739014)',
      'GMR.DefineUnstuck(165.00849914551, 4334.1821289062, 111.64892578125)',
      'GMR.DefineUnstuck(148.92161560059, 4333.5322265625, 106.76062011719)',
      'GMR.DefineUnstuck(132.91865539551, 4332.8862304688, 106.45709228516)',
      'GMR.DefineUnstuck(118.10472106934, 4332.2875976562, 102.7107925415)',
      'GMR.DefineUnstuck(102.87113952637, 4331.6723632812, 101.46556091309)',
      'GMR.DefineUnstuck(88.50846862793, 4331.7661132812, 101.46556091309)',
      'GMR.DefineUnstuck(74.145240783691, 4331.7255859375, 101.50704193115)',
      'GMR.DefineUnstuck(63.315258026123, 4331.4130859375, 96.868797302246)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(193.95178222656, 3938.7644042969, 77.02375793457, 80)',
      'GMR.DefineUnstuck(174.50480651855, 3946.119140625, 77.100463867188)',
      'GMR.DefineUnstuck(149.97203063965, 3948.1752929688, 79.036109924316)',
      'GMR.DefineUnstuck(126.51438140869, 3948.7211914062, 77.960227966309)',
      'GMR.DefineUnstuck(101.89168548584, 3948.3588867188, 76.13111114502)',
      'GMR.DefineUnstuck(85.309768676758, 3944.1459960938, 77.409309387207)',
      'GMR.DefineUnstuck(75.616241455078, 3931.0593261719, 79.601829528809)',
      'GMR.DefineUnstuck(63.763820648193, 3924.2333984375, 81.067092895508)',
      'GMR.DefineUnstuck(39.008518218994, 3919.9184570312, 83.911102294922)',
      'GMR.DefineUnstuck(18.60675239563, 3915.7314453125, 85.498207092285)',
      'GMR.DefineUnstuck(3.2077100276947, 3902.2924804688, 85.96996307373)',
      'GMR.DefineUnstuck(-17.735523223877, 3894.1833496094, 84.662544250488)',
      'GMR.DefineUnstuck(-38.712661743164, 3890.3488769531, 87.04801940918)',
      'GMR.DefineUnstuck(-60.633201599121, 3879.3247070312, 88.521347045898)',
      'GMR.DefineUnstuck(-78.320625305176, 3858.5451660156, 85.082618713379)',
      'GMR.DefineUnstuck(-91.559494018555, 3835.5930175781, 81.819549560547)',
      'GMR.DefineUnstuck(-100.26367950439, 3811.5712890625, 78.540550231934)',
      'GMR.DefineUnstuck(-107.41409301758, 3785.912109375, 74.54549407959)',
      'GMR.DefineUnstuck(-109.73361206055, 3769.2023925781, 70.857536315918)',
      'GMR.DefineUnstuck(-107.43981933594, 3755.7180175781, 69.670532226562)',
      'GMR.DefineUnstuck(-101.1815032959, 3742.294921875, 69.33438873291)',
      'GMR.DefineUnstuck(-95.545623779297, 3728.1887207031, 67.61213684082)',
      'GMR.DefineUnstuck(-91.818840026855, 3713.0690917969, 66.706428527832)',
      'GMR.DefineUnstuck(-89.762908935547, 3697.9763183594, 66.145195007324)',
      'GMR.DefineUnstuck(-88.039558410645, 3685.3251953125, 65.239288330078)',
      'GMR.DefineUnstuck(-86.427696228027, 3673.4924316406, 64.54475402832)',
      'GMR.DefineUnstuck(-82.980583190918, 3660.4255371094, 64.707656860352)',
      'GMR.DefineUnstuck(-78.40274810791, 3647.5959472656, 66.149765014648)',
      'GMR.DefineUnstuck(-72.205978393555, 3636.0017089844, 68.437324523926)',
      'GMR.DefineUnstuck(-64.513168334961, 3624.3820800781, 71.124984741211)',
      'GMR.DefineUnstuck(-57.107879638672, 3612.5378417969, 72.459785461426)',
      'GMR.DefineUnstuck(-51.833656311035, 3599.2102050781, 73.394302368164)',
      'GMR.DefineUnstuck(-47.972721099854, 3585.7385253906, 73.691917419434)',
      'GMR.DefineUnstuck(-45.66983795166, 3572.0002441406, 73.608192443848)',
      'GMR.DefineUnstuck(-44.726146697998, 3558.8740234375, 73.503349304199)',
      'GMR.DefineProfileCenter(386.5758, 3505.37, 63.69162, 80)',
      'GMR.DefineProfileCenter(324.9974, 3460.069, 62.12278, 80)',
      'GMR.DefineProfileCenter(238.2114, 3474.574, 62.74974, 80)',
      'GMR.DefineProfileCenter(192.5764, 3460.313, 62.09915, 80)',
      'GMR.DefineProfileCenter(137.0184, 3469.482, 63.11622, 80)',
      'GMR.DefineProfileCenter(87.94748, 3483.832, 63.16154, 80)',
      'GMR.DefineProfileCenter(45.89273, 3496.13, 63.04403, 80)',
      'GMR.DefineProfileCenter(31.05021, 3549.576, 63.54771, 80)',
      'GMR.DefineProfileCenter(16.94016, 3460.923, 64.94949, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16951)',
      'GMR.DefineQuestEnemyId(16901)',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE The Pools of Aggonar |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9426, x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 }
    },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Profile = {
      'GMR.SkipTurnIn(false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(-619.60986328125, 2747.0622558594, 83.949256896973, 30)',
        'GMR.DefineUnstuck(-608.38500976562, 2761.8305664062, 79.509010314941)',
        'GMR.DefineUnstuck(-602.98620605469, 2772.2893066406, 75.757293701172)',
        'GMR.DefineUnstuck(-597.69323730469, 2784.7348632812, 71.653022766113)',
        'GMR.DefineUnstuck(-588.19317626953, 2801.58984375, 66.737838745117)',
        'GMR.DefineUnstuck(-580.53948974609, 2817.0075683594, 62.959621429443)',
        'GMR.DefineUnstuck(-579.05651855469, 2830.6264648438, 61.322818756104)',
        'GMR.DefineUnstuck(-586.08673095703, 2841.9533691406, 59.624538421631)',
        'GMR.DefineUnstuck(-596.98828125, 2851.890625, 56.942493438721)',
        'GMR.DefineUnstuck(-609.59991455078, 2861.13671875, 54.614025115967)',
        'GMR.DefineUnstuck(-621.64739990234, 2867.1867675781, 52.294952392578)',
        'GMR.DefineUnstuck(-635.01409912109, 2874.2084960938, 50.727993011475)',
        'GMR.DefineUnstuck(-644.62133789062, 2883.0786132812, 50.267124176025)',
        'GMR.DefineUnstuck(-651.9990234375, 2895.3403320312, 47.170265197754)',
        'GMR.DefineUnstuck(-655.46942138672, 2911.2846679688, 44.159801483154)',
        'GMR.DefineUnstuck(-656.93225097656, 2925.6162109375, 41.347686767578)',
        'GMR.DefineUnstuck(-656.49920654297, 2940.3640136719, 38.085834503174)',
        'GMR.DefineUnstuck(-654.90142822266, 2955.1037597656, 34.887008666992)',
        'GMR.DefineUnstuck(-653.25982666016, 2970.2470703125, 30.970823287964)',
        'GMR.DefineUnstuck(-649.92474365234, 2985.4636230469, 27.190305709839)',
        'GMR.DefineUnstuck(-645.68139648438, 2998.7905273438, 24.768880844116)',
        'GMR.DefineUnstuck(-640.43133544922, 3015.2788085938, 22.707454681396)',
        'GMR.DefineUnstuck(-635.80572509766, 3029.8061523438, 18.901594161987)',
        'GMR.DefineUnstuck(-631.05688476562, 3044.7204589844, 15.343214988708)',
        'GMR.DefineUnstuck(-625.71301269531, 3058.529296875, 11.274188995361)',
        'GMR.DefineUnstuck(-619.52496337891, 3071.5075683594, 7.7397446632385)',
        'GMR.DefineUnstuck(-615.37969970703, 3084.390625, 5.9287514686584)',
        'GMR.DefineUnstuck(-612.63507080078, 3095.6098632812, 5.5691785812378)',
        'GMR.DefineUnstuck(-608.82360839844, 3111.1896972656, 5.7396326065063)',
        'GMR.DefineUnstuck(-605.98858642578, 3122.7780761719, 5.4450240135193)',
        'GMR.DefineUnstuck(-603.56127929688, 3139.7724609375, 5.355055809021)',
        'GMR.DefineUnstuck(-600.81683349609, 3155.6083984375, 5.0114398002625)',
        'GMR.DefineUnstuck(-597.14233398438, 3170.3527832031, 5.1042041778564)',
        'GMR.DefineUnstuck(-592.70263671875, 3184.0725097656, 4.899374961853)',
        'GMR.DefineUnstuck(-587.20495605469, 3197.3525390625, 4.9973797798157)',
        'GMR.DefineUnstuck(-579.67126464844, 3211.5068359375, 4.7982311248779)',
        'GMR.DefineUnstuck(-571.64819335938, 3225.4331054688, 4.9974040985107)',
        'GMR.DefineUnstuck(-563.63208007812, 3239.3471679688, 5.3012228012085)',
        'GMR.DefineUnstuck(-554.58862304688, 3255.0444335938, 6.6089043617249)',
        'GMR.DefineUnstuck(-547.23919677734, 3270.2009277344, 8.1004190444946)',
        'GMR.DefineUnstuck(-542.93298339844, 3286.8757324219, 11.329845428467)',
        'GMR.DefineUnstuck(-540.88360595703, 3303.5830078125, 14.080800056458)',
        'GMR.DefineUnstuck(-540.33123779297, 3318.3876953125, 17.109935760498)',
        'GMR.DefineUnstuck(-541.43823242188, 3333.0974121094, 20.61815071106)',
        'GMR.DefineUnstuck(-544.15338134766, 3348.9099121094, 24.437107086182)',
        'GMR.DefineUnstuck(-546.52496337891, 3362.7219238281, 27.895481109619)',
        'GMR.DefineUnstuck(-548.17498779297, 3379.0786132812, 32.540424346924)',
        'GMR.DefineUnstuck(-548.66015625, 3394.7062988281, 36.584690093994)',
        'GMR.DefineUnstuck(-549.17059326172, 3411.1494140625, 39.997108459473)',
        'GMR.DefineUnstuck(-549.75836181641, 3430.0822753906, 42.720767974854)',
        'GMR.DefineUnstuck(-549.04400634766, 3445.6818847656, 44.119613647461)',
        'GMR.DefineUnstuck(-544.75726318359, 3462.2863769531, 44.369258880615)',
        'GMR.DefineUnstuck(-538.43933105469, 3477.9382324219, 44.4440574646)',
        'GMR.DefineUnstuck(-533.64129638672, 3492.7709960938, 44.520210266113)',
        'GMR.DefineUnstuck(-527.15905761719, 3508.6315917969, 44.517665863037)',
        'GMR.DefineUnstuck(-519.69079589844, 3524.1352539062, 44.523765563965)',
        'GMR.DefineUnstuck(-513.98541259766, 3541.7661132812, 44.86075592041)',
        'GMR.DefineUnstuck(-507.97909545898, 3556.6181640625, 44.78800201416)',
        'GMR.DefineUnstuck(-500.47653198242, 3572.0712890625, 45.047721862793)',
        'GMR.DefineUnstuck(-492.38088989258, 3588.7458496094, 46.127136230469)',
        'GMR.DefineUnstuck(-483.05596923828, 3604.7338867188, 45.4040184021)',
        'GMR.DefineUnstuck(-474.92193603516, 3618.9641113281, 44.509792327881)',
        'GMR.DefineUnstuck(-466.0198059082, 3636.0739746094, 45.076969146729)',
        'GMR.DefineUnstuck(-457.22073364258, 3651.4304199219, 45.586544036865)',
        'GMR.DefineUnstuck(-448.21429443359, 3667.1489257812, 46.157962799072)',
        'GMR.DefineUnstuck(-441.3125, 3682.0068359375, 45.797065734863)',
        'GMR.DefineUnstuck(-438.24499511719, 3696.4860839844, 46.108203887939)',
        'GMR.DefineUnstuck(-435.10705566406, 3714.3422851562, 46.961719512939)',
        'GMR.DefineUnstuck(-431.16442871094, 3729.8559570312, 46.038135528564)',
        'GMR.DefineUnstuck(-425.58242797852, 3747.1052246094, 44.867568969727)',
        'GMR.DefineUnstuck(-418.59552001953, 3763.32421875, 47.736801147461)',
        'GMR.DefineUnstuck(-410.08715820312, 3777.3815917969, 50.333824157715)',
        'GMR.DefineUnstuck(-400.70477294922, 3791.435546875, 52.277103424072)',
        'GMR.DefineUnstuck(-392.52362060547, 3807.0810546875, 54.177703857422)',
        'GMR.DefineUnstuck(-383.2229309082, 3821.5712890625, 56.511127471924)',
        'GMR.DefineUnstuck(-370.38061523438, 3838.2221679688, 59.46704864502)',
        'GMR.DefineUnstuck(-359.06875610352, 3852.888671875, 62.138694763184)',
        'GMR.DefineUnstuck(-346.31988525391, 3865.5280761719, 64.875968933105)',
        'GMR.DefineUnstuck(-332.40829467773, 3875.7951660156, 67.490127563477)',
        'GMR.DefineUnstuck(-322.0680847168, 3890.8959960938, 70.073333740234)',
        'GMR.DefineUnstuck(-313.50073242188, 3905.9301757812, 73.248626708984)',
        'GMR.DefineUnstuck(-301.87655639648, 3926.3286132812, 79.323715209961)',
        'GMR.DefineUnstuck(-292.29721069336, 3943.138671875, 84.267761230469)',
        'GMR.DefineUnstuck(-281.49789428711, 3962.0895996094, 89.610244750977)',
        'GMR.DefineUnstuck(-275.99426269531, 3971.7475585938, 91.921592712402)',
        'GMR.DefineUnstuck(-267.83587646484, 3986.0642089844, 95.017562866211)',
        'GMR.DefineUnstuck(-259.48193359375, 4000.7238769531, 98.152557373047)',
        'GMR.DefineUnstuck(-249.00160217285, 4014.7736816406, 100.58375549316)',
        'GMR.DefineUnstuck(-240.36909484863, 4027.1799316406, 102.15671539307)',
        'GMR.DefineUnstuck(-233.19338989258, 4042.8852539062, 101.9426651001)',
        'GMR.DefineUnstuck(-226.31552124023, 4055.9189453125, 101.49783325195)',
        'GMR.DefineUnstuck(-217.01351928711, 4067.3654785156, 100.97541046143)',
        'GMR.DefineUnstuck(-206.41949462891, 4077.7375488281, 100.26557159424)',
        'GMR.DefineUnstuck(-193.55497741699, 4087.8996582031, 99.750747680664)',
        'GMR.DefineUnstuck(-178.73417663574, 4094.8850097656, 99.158134460449)',
        'GMR.DefineUnstuck(-163.77944946289, 4099.3559570312, 99.25463104248)',
        'GMR.DefineUnstuck(-148.69148254395, 4100.1030273438, 99.196769714355)',
        'GMR.DefineUnstuck(-132.65737915039, 4099.2275390625, 97.038543701172)',
        'GMR.DefineUnstuck(-116.62329101562, 4098.3515625, 94.923484802246)',
        'GMR.DefineUnstuck(-106.34860992432, 4097.7900390625, 94.583374023438)',
        'GMR.DefineUnstuck(-91.151626586914, 4097.8735351562, 92.087280273438)',
        'GMR.DefineUnstuck(-73.138122558594, 4104.7216796875, 86.395698547363)',
        'GMR.DefineUnstuck(-59.868946075439, 4115.6420898438, 83.176078796387)',
        'GMR.DefineUnstuck(-49.558525085449, 4130.33203125, 80.714179992676)',
        'GMR.DefineUnstuck(-43.555271148682, 4148.1567382812, 80.318161010742)',
        'GMR.DefineUnstuck(-39.046157836914, 4166.9716796875, 80.484169006348)',
        'GMR.DefineUnstuck(-30.869848251343, 4185.6884765625, 80.573905944824)',
        'GMR.DefineUnstuck(-22.415821075439, 4200.7705078125, 80.990684509277)',
        'GMR.DefineUnstuck(-17.501535415649, 4218.841796875, 82.727378845215)',
        'GMR.DefineUnstuck(-14.332887649536, 4237.9267578125, 84.289543151855)',
        'GMR.DefineUnstuck(-11.432181358337, 4255.3974609375, 84.830589294434)',
        'GMR.DefineUnstuck(-8.7332630157471, 4271.6528320312, 84.585868835449)',
        'GMR.DefineUnstuck(-4.0544624328613, 4286.0063476562, 84.308441162109)',
        'GMR.DefineUnstuck(3.9157149791718, 4298.3344726562, 85.701271057129)',
        'GMR.DefineUnstuck(14.888158798218, 4308.1953125, 87.674003601074)',
        'GMR.DefineUnstuck(27.404582977295, 4317.5703125, 89.346649169922)',
        'GMR.DefineUnstuck(39.671485900879, 4325.0107421875, 91.521896362305)',
        'GMR.DefineUnstuck(52.377326965332, 4330.9228515625, 94.179298400879)',
        'GMR.DefineUnstuck(64.169219970703, 4333.658203125, 97.279899597168)',
        'GMR.DefineUnstuck(79.411918640137, 4333.9760742188, 101.55618286133)',
        'GMR.DefineUnstuck(94.742141723633, 4331.6723632812, 101.46546936035)',
        'GMR.DefineUnstuck(109.64636993408, 4328.9848632812, 101.46624755859)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(365.69735717773, 3484.015625, 61.876659393311, 80)',
        'GMR.DefineUnstuck(350.80004882812, 3493.96875, 62.822025299072)',
        'GMR.DefineUnstuck(338.4797668457, 3496.8891601562, 63.489677429199)',
        'GMR.DefineUnstuck(324.50708007812, 3497.7629394531, 64.20703125)',
        'GMR.DefineUnstuck(310.92953491211, 3494.896484375, 64.528549194336)',
        'GMR.DefineUnstuck(299.99557495117, 3493.982421875, 65.742607116699)',
        'GMR.DefineUnstuck(290.86441040039, 3498.0861816406, 67.612030029297)',
        'GMR.DefineUnstuck(280.58612060547, 3503.0783691406, 65.749794006348)',
        'GMR.DefineUnstuck(272.03192138672, 3510.5593261719, 64.836486816406)',
        'GMR.DefineUnstuck(264.54418945312, 3518.7563476562, 64.996887207031)',
        'GMR.DefineUnstuck(256.42727661133, 3525.6228027344, 66.091133117676)',
        'GMR.DefineUnstuck(246.3337097168, 3528.3317871094, 66.50853729248)',
        'GMR.DefineUnstuck(233.65397644043, 3527.439453125, 65.87614440918)',
        'GMR.DefineUnstuck(221.01121520996, 3525.6555175781, 65.325843811035)',
        'GMR.DefineUnstuck(209.17361450195, 3525.0083007812, 67.411643981934)',
        'GMR.DefineUnstuck(199.40919494629, 3527.923828125, 67.677467346191)',
        'GMR.DefineUnstuck(189.18118286133, 3532.3835449219, 65.964660644531)',
        'GMR.DefineUnstuck(178.42373657227, 3534.8950195312, 64.883689880371)',
        'GMR.DefineUnstuck(167.08193969727, 3539.4133300781, 65.618423461914)',
        'GMR.DefineUnstuck(158.81777954102, 3548.3425292969, 66.696708679199)',
        'GMR.DefineUnstuck(155.04461669922, 3559.5686035156, 67.988861083984)',
        'GMR.DefineUnstuck(149.00552368164, 3570.0471191406, 67.586090087891)',
        'GMR.DefineUnstuck(140.78089904785, 3580.3383789062, 67.736358642578)',
        'GMR.DefineUnstuck(133.07945251465, 3588.8933105469, 67.17813873291)',
        'GMR.DefineUnstuck(123.06607818604, 3594.953125, 67.286895751953)',
        'GMR.DefineUnstuck(110.68665313721, 3597.2158203125, 68.620788574219)',
        'GMR.DefineUnstuck(97.305526733398, 3595.5993652344, 67.855392456055)',
        'GMR.DefineUnstuck(84.237907409668, 3591.8530273438, 66.050537109375)',
        'GMR.DefineUnstuck(71.183738708496, 3588.1108398438, 65.89786529541)',
        'GMR.DefineUnstuck(60.094429016113, 3584.931640625, 66.660697937012)',
        'GMR.DefineUnstuck(48.211093902588, 3581.5249023438, 67.037635803223)',
        'GMR.DefineUnstuck(35.560668945312, 3577.8984375, 66.054931640625)',
        'GMR.DefineUnstuck(24.861640930176, 3574.8310546875, 65.405738830566)',
        'GMR.DefineUnstuck(13.393696784973, 3575.1960449219, 64.605659484863)',
        'GMR.DefineUnstuck(6.1198263168335, 3578.0349121094, 66.918090820312)',
        'GMR.DefineUnstuck(2.5795881748199, 3583.0178222656, 71.497268676758)',
        'GMR.DefineUnstuck(-1.9568736553192, 3586.8657226562, 75.714546203613)',
        'GMR.DefineUnstuck(-8.4921245574951, 3587.3774414062, 78.982192993164)',
        'GMR.DefineUnstuck(-14.646240234375, 3586.8068847656, 78.884872436523)',
        'GMR.DefineUnstuck(-27.848180770874, 3584.2592773438, 75.081169128418)',
        'GMR.DefineUnstuck(-36.858417510986, 3588.9020996094, 75.249183654785)',
        'GMR.DefineUnstuck(-42.480381011963, 3600.7639160156, 74.462265014648)',
        'GMR.DefineUnstuck(-50.877674102783, 3614.9252929688, 73.185653686523)',
        'GMR.DefineUnstuck(-58.018230438232, 3626.9675292969, 72.223030090332)',
        'GMR.DefineUnstuck(-65.373001098633, 3639.3708496094, 69.385848999023)',
        'GMR.DefineUnstuck(-72.619338989258, 3653.1484375, 66.841499328613)',
        'GMR.DefineUnstuck(-77.563186645508, 3667.5705566406, 66.705718994141)',
        'GMR.DefineUnstuck(-82.507034301758, 3681.9926757812, 66.179763793945)',
        'GMR.DefineUnstuck(-87.446342468262, 3696.4016113281, 66.369049072266)',
        'GMR.DefineUnstuck(-92.382621765137, 3710.8015136719, 66.549736022949)',
        'GMR.DefineUnstuck(-97.321929931641, 3725.2104492188, 67.106353759766)',
        'GMR.DefineUnstuck(-100.97786712646, 3736.9948730469, 68.737045288086)',
        'GMR.DefineUnstuck(-104.15058898926, 3748.9428710938, 69.536239624023)',
        'GMR.DefineUnstuck(-107.31972503662, 3760.8771972656, 70.05069732666)',
        'GMR.DefineUnstuck(-111.33683013916, 3776.0048828125, 71.908721923828)',
        'GMR.DefineUnstuck(-115.67653656006, 3790.1284179688, 74.917045593262)',
        'GMR.DefineUnstuck(-121.86195373535, 3801.2175292969, 77.575050354004)',
        'GMR.DefineUnstuck(-129.13352966309, 3811.7124023438, 80.080764770508)',
        'GMR.DefineUnstuck(-134.26795959473, 3823.6728515625, 81.973480224609)',
        'GMR.DefineUnstuck(-133.70872497559, 3835.2189941406, 83.547080993652)',      
        'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
        'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
        'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
        'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
        'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Cleansing the Waters |r",
    QuestID = 9427,
    QuestType = "Custom",
    PickUp = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    TurnIn = { x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9427, 1) and not GMR.InCombat() then
      GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(415.357788, 3459.838379, 63.392170, 3) then 
              GMR.MeshTo(415.357788, 3459.838379, 63.392170)
          else 
              local itemName = GetItemInfo(23361)
              GMR.Use(itemName)
          end
      end
      ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(323.97836303711, 3050.6022949219, 22.966484069824, 150)',
      'GMR.DefineUnstuck(309.67529296875, 3056.7590332031, 23.54647064209)',
      'GMR.DefineUnstuck(295.30316162109, 3061.8044433594, 23.217712402344)',
      'GMR.DefineUnstuck(282.48980712891, 3066.302734375, 22.452085494995)',
      'GMR.DefineUnstuck(269.19689941406, 3072.7019042969, 21.709043502808)',
      'GMR.DefineUnstuck(244.60723876953, 3083.9760742188, 20.85195350647)',
      'GMR.DefineUnstuck(230.54006958008, 3092.5036621094, 21.902698516846)',
      'GMR.DefineUnstuck(219.19140625, 3101.7631835938, 23.778169631958)',
      'GMR.DefineUnstuck(209.36474609375, 3113.9282226562, 26.570053100586)',
      'GMR.DefineUnstuck(197.94067382812, 3121.4694824219, 27.617574691772)',
      'GMR.DefineUnstuck(183.54524230957, 3124.1018066406, 26.473207473755)',
      'GMR.DefineUnstuck(167.32028198242, 3122.0732421875, 24.17476272583)',
      'GMR.DefineUnstuck(154.03559875488, 3119.1896972656, 22.138004302979)',
      'GMR.DefineUnstuck(142.41398620605, 3111.2021484375, 19.141036987305)',
      'GMR.DefineUnstuck(131.75709533691, 3100.5036621094, 15.460600852966)',
      'GMR.DefineUnstuck(119.68088531494, 3096.1010742188, 13.501254081726)',
      'GMR.DefineUnstuck(106.62142944336, 3096.3610839844, 12.976165771484)',
      'GMR.DefineUnstuck(93.022613525391, 3100.8012695312, 11.93945980072)',
      'GMR.DefineUnstuck(80.766021728516, 3108.2409667969, 10.910000801086)',
      'GMR.DefineUnstuck(68.577323913574, 3114.8796386719, 5.3088622093201)',
      'GMR.DefineUnstuck(54.078468322754, 3116.17578125, 1.4641733169556)',
      'GMR.DefineUnstuck(38.900291442871, 3115.0756835938, -1.2142399549484)',
      'GMR.DefineUnstuck(20.817733764648, 3113.7648925781, -1.2221149206161)',
      'GMR.DefineUnstuck(2.3302383422852, 3112.4248046875, -1.2221149206161)',
      'GMR.DefineUnstuck(-9.2223615646362, 3122.2712402344, -1.2222893238068)',
      'GMR.DefineUnstuck(-21.50288772583, 3143.2709960938, -1.2222893238068)',
      'GMR.DefineUnstuck(-30.993766784668, 3161.4487304688, -0.92479354143143)',
      'GMR.DefineUnstuck(-41.090148925781, 3174.9812011719, -0.55073654651642)',
      'GMR.DefineUnstuck(-48.69909286499, 3188.0161132812, 0.86446410417557)',
      'GMR.DefineUnstuck(-52.710285186768, 3202.9987792969, 4.3451452255249)',
      'GMR.DefineUnstuck(-52.471530914307, 3220.212890625, 7.8655376434326)',
      'GMR.DefineUnstuck(-53.066345214844, 3233.3308105469, 11.034104347229)',
      'GMR.DefineUnstuck(-53.871673583984, 3246.8935546875, 15.165593147278)',
      'GMR.DefineUnstuck(-54.160293579102, 3262.1228027344, 19.979940414429)',
      'GMR.DefineUnstuck(-54.480289459229, 3279.0078125, 24.57439994812)',
      'GMR.DefineUnstuck(-53.240703582764, 3293.6870117188, 28.356273651123)',
      'GMR.DefineUnstuck(-51.604278564453, 3307.5263671875, 32.057514190674)',
      'GMR.DefineUnstuck(-51.599903106689, 3323.1208496094, 36.360397338867)',
      'GMR.DefineUnstuck(-52.008575439453, 3339.1311035156, 41.389205932617)',
      'GMR.DefineUnstuck(-52.461750030518, 3353.0925292969, 45.746170043945)',
      'GMR.DefineUnstuck(-51.729934692383, 3367.900390625, 49.397365570068)',
      'GMR.DefineUnstuck(-51.038890838623, 3381.8833007812, 53.562191009521)',
      'GMR.DefineUnstuck(-50.266304016113, 3397.5163574219, 58.652179718018)',
      'GMR.DefineUnstuck(-49.51513671875, 3412.7158203125, 62.496257781982)',
      'GMR.DefineUnstuck(-48.742343902588, 3428.3527832031, 66.44270324707)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(323.97836303711, 3050.6022949219, 22.966484069824, 150)',
      'GMR.DefineUnstuck(309.67529296875, 3056.7590332031, 23.54647064209)',
      'GMR.DefineUnstuck(295.30316162109, 3061.8044433594, 23.217712402344)',
      'GMR.DefineUnstuck(282.48980712891, 3066.302734375, 22.452085494995)',
      'GMR.DefineUnstuck(269.19689941406, 3072.7019042969, 21.709043502808)',
      'GMR.DefineUnstuck(244.60723876953, 3083.9760742188, 20.85195350647)',
      'GMR.DefineUnstuck(230.54006958008, 3092.5036621094, 21.902698516846)',
      'GMR.DefineUnstuck(219.19140625, 3101.7631835938, 23.778169631958)',
      'GMR.DefineUnstuck(209.36474609375, 3113.9282226562, 26.570053100586)',
      'GMR.DefineUnstuck(197.94067382812, 3121.4694824219, 27.617574691772)',
      'GMR.DefineUnstuck(183.54524230957, 3124.1018066406, 26.473207473755)',
      'GMR.DefineUnstuck(167.32028198242, 3122.0732421875, 24.17476272583)',
      'GMR.DefineUnstuck(154.03559875488, 3119.1896972656, 22.138004302979)',
      'GMR.DefineUnstuck(142.41398620605, 3111.2021484375, 19.141036987305)',
      'GMR.DefineUnstuck(131.75709533691, 3100.5036621094, 15.460600852966)',
      'GMR.DefineUnstuck(119.68088531494, 3096.1010742188, 13.501254081726)',
      'GMR.DefineUnstuck(106.62142944336, 3096.3610839844, 12.976165771484)',
      'GMR.DefineUnstuck(93.022613525391, 3100.8012695312, 11.93945980072)',
      'GMR.DefineUnstuck(80.766021728516, 3108.2409667969, 10.910000801086)',
      'GMR.DefineUnstuck(68.577323913574, 3114.8796386719, 5.3088622093201)',
      'GMR.DefineUnstuck(54.078468322754, 3116.17578125, 1.4641733169556)',
      'GMR.DefineUnstuck(38.900291442871, 3115.0756835938, -1.2142399549484)',
      'GMR.DefineUnstuck(20.817733764648, 3113.7648925781, -1.2221149206161)',
      'GMR.DefineUnstuck(2.3302383422852, 3112.4248046875, -1.2221149206161)',
      'GMR.DefineUnstuck(-9.2223615646362, 3122.2712402344, -1.2222893238068)',
      'GMR.DefineUnstuck(-21.50288772583, 3143.2709960938, -1.2222893238068)',
      'GMR.DefineUnstuck(-30.993766784668, 3161.4487304688, -0.92479354143143)',
      'GMR.DefineUnstuck(-41.090148925781, 3174.9812011719, -0.55073654651642)',
      'GMR.DefineUnstuck(-48.69909286499, 3188.0161132812, 0.86446410417557)',
      'GMR.DefineUnstuck(-52.710285186768, 3202.9987792969, 4.3451452255249)',
      'GMR.DefineUnstuck(-52.471530914307, 3220.212890625, 7.8655376434326)',
      'GMR.DefineUnstuck(-53.066345214844, 3233.3308105469, 11.034104347229)',
      'GMR.DefineUnstuck(-53.871673583984, 3246.8935546875, 15.165593147278)',
      'GMR.DefineUnstuck(-54.160293579102, 3262.1228027344, 19.979940414429)',
      'GMR.DefineUnstuck(-54.480289459229, 3279.0078125, 24.57439994812)',
      'GMR.DefineUnstuck(-53.240703582764, 3293.6870117188, 28.356273651123)',
      'GMR.DefineUnstuck(-51.604278564453, 3307.5263671875, 32.057514190674)',
      'GMR.DefineUnstuck(-51.599903106689, 3323.1208496094, 36.360397338867)',
      'GMR.DefineUnstuck(-52.008575439453, 3339.1311035156, 41.389205932617)',
      'GMR.DefineUnstuck(-52.461750030518, 3353.0925292969, 45.746170043945)',
      'GMR.DefineUnstuck(-51.729934692383, 3367.900390625, 49.397365570068)',
      'GMR.DefineUnstuck(-51.038890838623, 3381.8833007812, 53.562191009521)',
      'GMR.DefineUnstuck(-50.266304016113, 3397.5163574219, 58.652179718018)',
      'GMR.DefineUnstuck(-49.51513671875, 3412.7158203125, 62.496257781982)',
      'GMR.DefineUnstuck(-48.742343902588, 3428.3527832031, 66.44270324707)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(57.661842346191, 4332.140625, 95.38215637207, 20)',
      'GMR.DefineUnstuck(38.132480621338, 4320.1162109375, 91.179145812988)',
      'GMR.DefineUnstuck(25.801908493042, 4305.0122070312, 89.94995880127)',
      'GMR.DefineUnstuck(14.032560348511, 4286.1674804688, 87.447547912598)',
      'GMR.DefineUnstuck(0.93573379516602, 4265.197265625, 86.408828735352)',
      'GMR.DefineUnstuck(-7.6100721359253, 4243.4970703125, 84.644905090332)',
      'GMR.DefineUnstuck(-15.242209434509, 4216.9794921875, 82.454200744629)',
      'GMR.DefineUnstuck(-23.592479705811, 4197, 81.005111694336)',
      'GMR.DefineUnstuck(-37.659698486328, 4176.4360351562, 80.021522521973)',
      'GMR.DefineUnstuck(-43.763534545898, 4156.4897460938, 79.563125610352)',
      'GMR.DefineUnstuck(-47.656192779541, 4132.5234375, 81.088722229004)',
      'GMR.DefineUnstuck(-51.132457733154, 4108.072265625, 80.346389770508)',
      'GMR.DefineUnstuck(-54.261775970459, 4086.0615234375, 83.864036560059)',
      'GMR.DefineUnstuck(-60.642742156982, 4063.1691894531, 88.790176391602)',
      'GMR.DefineUnstuck(-69.077796936035, 4039.9584960938, 97.13028717041)',
      'GMR.DefineUnstuck(-79.482940673828, 4011.3264160156, 100.17750549316)',
      'GMR.DefineUnstuck(-92.513374328613, 3981.8325195312, 100.26639556885)',
      'GMR.DefineUnstuck(-105.67876434326, 3957.5705566406, 99.647956848145)',
      'GMR.DefineUnstuck(-112.5747756958, 3928.8544921875, 95.941940307617)',
      'GMR.DefineUnstuck(-119.40774536133, 3904.6857910156, 92.466499328613)',
      'GMR.DefineUnstuck(-126.24071502686, 3880.5173339844, 89.47135925293)',
      'GMR.DefineUnstuck(-133.52311706543, 3854.7587890625, 85.927879333496)',
      'GMR.DefineUnstuck(-139.23492431641, 3828.3662109375, 82.563049316406)',
      'GMR.DefineUnstuck(-129.4409942627, 3798.6533203125, 76.288719177246)',
      'GMR.DefineUnstuck(-115.35123443604, 3772.1022949219, 71.027168273926)',
      'GMR.DefineUnstuck(-99.975814819336, 3740.7397460938, 69.047592163086)',
      'GMR.DefineUnstuck(-89.480453491211, 3712.0961914062, 66.938804626465)',
      'GMR.DefineUnstuck(-84.237922668457, 3679.7724609375, 65.560272216797)',
      'GMR.DefineUnstuck(-77.482055664062, 3648.7302246094, 66.098205566406)',
      'GMR.DefineUnstuck(-59.923645019531, 3616.82421875, 72.00611114502)',
      'GMR.DefineUnstuck(-46.556621551514, 3585.2927246094, 73.814559936523)',
      'GMR.DefineUnstuck(-46.185981750488, 3562.5390625, 73.579414367676)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(204.87678527832, 4249.029296875, 120.71855926514, 25)',
      'GMR.DefineUnstuck(205.20516967773, 4259.6137695312, 120.23365783691)',
      'GMR.DefineUnstuck(202.68841552734, 4274.1928710938, 119.10501098633)',
      'GMR.DefineUnstuck(197.00305175781, 4288.1674804688, 118.21218109131)',
      'GMR.DefineUnstuck(189.90100097656, 4301.1977539062, 116.98760986328)',
      'GMR.DefineUnstuck(189.29832458496, 4314.7041015625, 116.51760101318)',
      'GMR.DefineUnstuck(189.74128723145, 4326.951171875, 116.52184295654)',
      'GMR.DefineUnstuck(179.39805603027, 4334.0766601562, 116.39238739014)',
      'GMR.DefineUnstuck(165.00849914551, 4334.1821289062, 111.64892578125)',
      'GMR.DefineUnstuck(148.92161560059, 4333.5322265625, 106.76062011719)',
      'GMR.DefineUnstuck(132.91865539551, 4332.8862304688, 106.45709228516)',
      'GMR.DefineUnstuck(118.10472106934, 4332.2875976562, 102.7107925415)',
      'GMR.DefineUnstuck(102.87113952637, 4331.6723632812, 101.46556091309)',
      'GMR.DefineUnstuck(88.50846862793, 4331.7661132812, 101.46556091309)',
      'GMR.DefineUnstuck(74.145240783691, 4331.7255859375, 101.50704193115)',
      'GMR.DefineUnstuck(63.315258026123, 4331.4130859375, 96.868797302246)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(193.95178222656, 3938.7644042969, 77.02375793457, 80)',
      'GMR.DefineUnstuck(174.50480651855, 3946.119140625, 77.100463867188)',
      'GMR.DefineUnstuck(149.97203063965, 3948.1752929688, 79.036109924316)',
      'GMR.DefineUnstuck(126.51438140869, 3948.7211914062, 77.960227966309)',
      'GMR.DefineUnstuck(101.89168548584, 3948.3588867188, 76.13111114502)',
      'GMR.DefineUnstuck(85.309768676758, 3944.1459960938, 77.409309387207)',
      'GMR.DefineUnstuck(75.616241455078, 3931.0593261719, 79.601829528809)',
      'GMR.DefineUnstuck(63.763820648193, 3924.2333984375, 81.067092895508)',
      'GMR.DefineUnstuck(39.008518218994, 3919.9184570312, 83.911102294922)',
      'GMR.DefineUnstuck(18.60675239563, 3915.7314453125, 85.498207092285)',
      'GMR.DefineUnstuck(3.2077100276947, 3902.2924804688, 85.96996307373)',
      'GMR.DefineUnstuck(-17.735523223877, 3894.1833496094, 84.662544250488)',
      'GMR.DefineUnstuck(-38.712661743164, 3890.3488769531, 87.04801940918)',
      'GMR.DefineUnstuck(-60.633201599121, 3879.3247070312, 88.521347045898)',
      'GMR.DefineUnstuck(-78.320625305176, 3858.5451660156, 85.082618713379)',
      'GMR.DefineUnstuck(-91.559494018555, 3835.5930175781, 81.819549560547)',
      'GMR.DefineUnstuck(-100.26367950439, 3811.5712890625, 78.540550231934)',
      'GMR.DefineUnstuck(-107.41409301758, 3785.912109375, 74.54549407959)',
      'GMR.DefineUnstuck(-109.73361206055, 3769.2023925781, 70.857536315918)',
      'GMR.DefineUnstuck(-107.43981933594, 3755.7180175781, 69.670532226562)',
      'GMR.DefineUnstuck(-101.1815032959, 3742.294921875, 69.33438873291)',
      'GMR.DefineUnstuck(-95.545623779297, 3728.1887207031, 67.61213684082)',
      'GMR.DefineUnstuck(-91.818840026855, 3713.0690917969, 66.706428527832)',
      'GMR.DefineUnstuck(-89.762908935547, 3697.9763183594, 66.145195007324)',
      'GMR.DefineUnstuck(-88.039558410645, 3685.3251953125, 65.239288330078)',
      'GMR.DefineUnstuck(-86.427696228027, 3673.4924316406, 64.54475402832)',
      'GMR.DefineUnstuck(-82.980583190918, 3660.4255371094, 64.707656860352)',
      'GMR.DefineUnstuck(-78.40274810791, 3647.5959472656, 66.149765014648)',
      'GMR.DefineUnstuck(-72.205978393555, 3636.0017089844, 68.437324523926)',
      'GMR.DefineUnstuck(-64.513168334961, 3624.3820800781, 71.124984741211)',
      'GMR.DefineUnstuck(-57.107879638672, 3612.5378417969, 72.459785461426)',
      'GMR.DefineUnstuck(-51.833656311035, 3599.2102050781, 73.394302368164)',
      'GMR.DefineUnstuck(-47.972721099854, 3585.7385253906, 73.691917419434)',
      'GMR.DefineUnstuck(-45.66983795166, 3572.0002441406, 73.608192443848)',
      'GMR.DefineUnstuck(-44.726146697998, 3558.8740234375, 73.503349304199)',
      'GMR.DefineProfileCenter(415.357788, 3459.838379, 63.392170, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(17000)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Cleansing the Waters  |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9427, x = 222.804001, y = 4330.040039, z = 119.083, id = 16796 }
    },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Profile = {
      'GMR.SkipTurnIn(false)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(299.61251831055, 3485.5444335938, 64.159118652344, 30)',
        'GMR.DefineUnstuck(284.37533569336, 3496.4848632812, 65.972480773926)',
        'GMR.DefineUnstuck(271.73678588867, 3509.353515625, 64.673385620117)',
        'GMR.DefineUnstuck(260.99487304688, 3522.9018554688, 65.601676940918)',
        'GMR.DefineUnstuck(251.24378967285, 3533.8635253906, 67.905899047852)',
        'GMR.DefineUnstuck(237.90377807617, 3536.7553710938, 68.313461303711)',
        'GMR.DefineUnstuck(224.59341430664, 3531.6489257812, 66.833602905273)',
        'GMR.DefineUnstuck(212.96789550781, 3525.4521484375, 66.615036010742)',
        'GMR.DefineUnstuck(202.1692199707, 3522.861328125, 66.693588256836)',
        'GMR.DefineUnstuck(190.33950805664, 3526.2075195312, 64.283935546875)',
        'GMR.DefineUnstuck(178.09840393066, 3531.9609375, 63.79337310791)',
        'GMR.DefineUnstuck(167.61378479004, 3539.8374023438, 65.89966583252)',
        'GMR.DefineUnstuck(159.00114440918, 3550.7885742188, 67.323066711426)',
        'GMR.DefineUnstuck(152.08540344238, 3562.4919433594, 67.435852050781)',
        'GMR.DefineUnstuck(147.00987243652, 3574.9929199219, 67.888542175293)',
        'GMR.DefineUnstuck(139.5676574707, 3583.5246582031, 67.985710144043)',
        'GMR.DefineUnstuck(128.50338745117, 3590.2893066406, 66.652854919434)',
        'GMR.DefineUnstuck(110.19927978516, 3590.2744140625, 66.299591064453)',
        'GMR.DefineUnstuck(89.417449951172, 3588.74609375, 65.21647644043)',
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(89.304885864258, 3588.9838867188, 65.272468566895, 50)',
        'GMR.DefineUnstuck(76.377113342285, 3584.7802734375, 64.00577545166)',
        'GMR.DefineUnstuck(59.062110900879, 3581.1911621094, 65.811798095703)',
        'GMR.DefineUnstuck(42.009689331055, 3578.169921875, 66.347938537598)',
        'GMR.DefineUnstuck(25.630359649658, 3577.3786621094, 65.630401611328)',
        'GMR.DefineUnstuck(12.46310043335, 3577.7998046875, 66.079299926758)',
        'GMR.DefineUnstuck(5.8942279815674, 3580.5100097656, 68.709945678711)',
        'GMR.DefineUnstuck(1.1985564231873, 3585.0979003906, 73.149703979492)',
        'GMR.DefineUnstuck(-4.7553176879883, 3587.7124023438, 77.52912902832)',
        'GMR.DefineUnstuck(-10.911392211914, 3587.1086425781, 79.263969421387)',
        'GMR.DefineUnstuck(-18.275016784668, 3586.3173828125, 77.811187744141)',
        'GMR.DefineUnstuck(-28.088541030884, 3585.2631835938, 75.179618835449)',
        'GMR.DefineUnstuck(-36.44849395752, 3588.2465820312, 75.24047088623)',
        'GMR.DefineUnstuck(-43.83744430542, 3598.4680175781, 74.312606811523)',
        'GMR.DefineUnstuck(-51.315200805664, 3613.07421875, 73.095764160156)',
        'GMR.DefineUnstuck(-61.077136993408, 3629.20703125, 71.4912109375)',
        'GMR.DefineUnstuck(-69.053436279297, 3641.7045898438, 68.129486083984)',
        'GMR.DefineUnstuck(-76.803779602051, 3653.8481445312, 65.852401733398)',
        'GMR.DefineUnstuck(-84.11727142334, 3665.3071289062, 64.555274963379)',
        'GMR.DefineUnstuck(-89.022193908691, 3676.5402832031, 64.543617248535)',
        'GMR.DefineUnstuck(-91.955917358398, 3691.0419921875, 65.344352722168)',
        'GMR.DefineUnstuck(-94.094619750977, 3704.8776855469, 66.112854003906)',
        'GMR.DefineUnstuck(-96.421119689941, 3719.9282226562, 66.831504821777)',
        'GMR.DefineUnstuck(-98.748039245605, 3734.9814453125, 68.369911193848)',
        'GMR.DefineUnstuck(-102.34461975098, 3746.3220214844, 69.502799987793)',
        'GMR.DefineUnstuck(-108.16034698486, 3761.7397460938, 70.094589233398)',
        'GMR.DefineUnstuck(-113.69937133789, 3776.423828125, 71.809661865234)',
        'GMR.DefineUnstuck(-119.20381164551, 3791.0161132812, 74.76602935791)',
        'GMR.DefineUnstuck(-125.16283416748, 3806.8134765625, 78.891120910645)',
        'GMR.DefineUnstuck(-127.11708068848, 3821.3159179688, 82.272926330566)',
        'GMR.DefineUnstuck(-125.15990447998, 3837.59765625, 84.893379211426)',
        'GMR.DefineUnstuck(-121.80643463135, 3854.5451660156, 87.467277526855)',
        'GMR.DefineUnstuck(-118.45024871826, 3871.5061035156, 89.146049499512)',
        'GMR.DefineUnstuck(-113.95496368408, 3888.1611328125, 91.537483215332)',
        'GMR.DefineUnstuck(-108.44258880615, 3904.1494140625, 93.821563720703)',
        'GMR.DefineUnstuck(-102.14077758789, 3922.427734375, 96.749374389648)',
        'GMR.DefineUnstuck(-99.086395263672, 3938.9631347656, 99.330917358398)',
        'GMR.DefineUnstuck(-96.88427734375, 3956.5356445312, 99.76025390625)',
        'GMR.DefineUnstuck(-95.080947875977, 3975.7883300781, 99.896186828613)',
        'GMR.DefineUnstuck(-94.107414245605, 3993.0510253906, 99.741508483887)',
        'GMR.DefineUnstuck(-92.994354248047, 4012.7875976562, 99.585556030273)',
        'GMR.DefineUnstuck(-89.802017211914, 4027.9592285156, 99.59008026123)',
        'GMR.DefineUnstuck(-85.26545715332, 4041.21875, 96.86548614502)',
        'GMR.DefineUnstuck(-79.939796447754, 4056.7844238281, 96.136642456055)',
        'GMR.DefineUnstuck(-73.807968139648, 4074.7065429688, 94.085487365723)',
        'GMR.DefineUnstuck(-66.87850189209, 4094.9599609375, 87.886154174805)',
        'GMR.DefineUnstuck(-60.87809753418, 4112.498046875, 83.80638885498)',
        'GMR.DefineUnstuck(-54.614837646484, 4130.8041992188, 81.661895751953)',
        'GMR.DefineUnstuck(-48.614437103271, 4148.341796875, 79.816535949707)',
        'GMR.DefineUnstuck(-43.280242919922, 4163.9326171875, 79.539436340332)',
        'GMR.DefineUnstuck(-36.701465606689, 4177.0927734375, 80.291854858398)',
        'GMR.DefineUnstuck(-30.609476089478, 4187.3798828125, 80.570037841797)',
        'GMR.DefineUnstuck(-21.617660522461, 4204.9155273438, 81.089317321777)',
        'GMR.DefineUnstuck(-16.189294815063, 4219.984375, 82.816604614258)',
        'GMR.DefineUnstuck(-11.665937423706, 4235.76953125, 84.139305114746)',
        'GMR.DefineUnstuck(-8.0867300033569, 4254.7978515625, 84.953323364258)',
        'GMR.DefineUnstuck(-1.6993227005005, 4270.1865234375, 85.67276763916)',
        'GMR.DefineUnstuck(7.4946465492249, 4285.7958984375, 86.393058776855)',
        'GMR.DefineUnstuck(18.531829833984, 4299.4453125, 88.818420410156)',
        'GMR.DefineUnstuck(33.298244476318, 4311.1552734375, 91.096458435059)',
        'GMR.DefineUnstuck(49.39217376709, 4321.14453125, 93.271575927734)',
        'GMR.DefineUnstuck(64.492813110352, 4329.3295898438, 97.412467956543)',
        'GMR.DefineUnstuck(78.364418029785, 4330.3100585938, 101.54004669189)',      
        'GMR.CreateTableEntry("Unstuck")',
        'GMR.DefineUnstuck(-49.148349761963, 3209.728515625, 5.5865983963013, 40)',
        'GMR.DefineUnstuck(-49.355632781982, 3188.83203125, 1.0132489204407)',
        'GMR.DefineUnstuck(-41.962795257568, 3177.2399902344, -0.3438001871109)',
        'GMR.DefineUnstuck(-34.341171264648, 3166.8220214844, -0.78004676103592)',
        'GMR.DefineUnstuck(-33.862018585205, 3153.4956054688, -1.5212464332581)',
        'GMR.DefineUnstuck(-35.696731567383, 3139.1928710938, -1.338996052742)',
        'GMR.DefineUnstuck(-40.31912612915, 3122.6491699219, -1.6359777450562)',
        'GMR.DefineUnstuck(-52.141719818115, 3107.4431152344, -2.4517805576324)',
        'GMR.DefineUnstuck(-63.287666320801, 3096.6452636719, -2.8852896690369)',
        'GMR.DefineUnstuck(-75.426025390625, 3086.7858886719, -3.3677616119385)',
        'GMR.DefineUnstuck(-87.586112976074, 3076.9089355469, -2.8270618915558)',
        'GMR.DefineUnstuck(-97.469482421875, 3066.7280273438, -1.9667536020279)',
        'GMR.DefineUnstuck(-105.55224609375, 3050.9702148438, -1.1784375905991)',
        'GMR.DefineUnstuck(-113.63063812256, 3035.220703125, 0.15347370505333)',
        'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
        'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
        'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
        'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
        'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Outland Sucks |r",
    QuestID = 10236,
    QuestType = "Custom",
    PickUp = { x = 430.101013, y = 2886.580078, z = 52.355801, id = 16915 },
    TurnIn = { x = 430.101013, y = 2886.580078, z = 52.355801, id = 16915 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10236, 1) then
          GMR.SetQuestingState("Idle")
      end
      ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      -- by FP
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(204.87678527832, 4249.029296875, 120.71855926514, 25)',
      'GMR.DefineUnstuck(205.20516967773, 4259.6137695312, 120.23365783691)',
      'GMR.DefineUnstuck(202.68841552734, 4274.1928710938, 119.10501098633)',
      'GMR.DefineUnstuck(197.00305175781, 4288.1674804688, 118.21218109131)',
      'GMR.DefineUnstuck(189.90100097656, 4301.1977539062, 116.98760986328)',
      'GMR.DefineUnstuck(189.29832458496, 4314.7041015625, 116.51760101318)',
      'GMR.DefineUnstuck(189.74128723145, 4326.951171875, 116.52184295654)',
      'GMR.DefineUnstuck(179.39805603027, 4334.0766601562, 116.39238739014)',
      'GMR.DefineUnstuck(165.00849914551, 4334.1821289062, 111.64892578125)',
      'GMR.DefineUnstuck(148.92161560059, 4333.5322265625, 106.76062011719)',
      'GMR.DefineUnstuck(132.91865539551, 4332.8862304688, 106.45709228516)',
      'GMR.DefineUnstuck(118.10472106934, 4332.2875976562, 102.7107925415)',
      'GMR.DefineUnstuck(102.87113952637, 4331.6723632812, 101.46556091309)',
      'GMR.DefineUnstuck(88.50846862793, 4331.7661132812, 101.46556091309)',
      'GMR.DefineUnstuck(74.145240783691, 4331.7255859375, 101.50704193115)',
      'GMR.DefineUnstuck(63.315258026123, 4331.4130859375, 96.868797302246)',
      -- From Telhamat
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(57.661842346191, 4332.140625, 95.38215637207, 20)',
      'GMR.DefineUnstuck(38.132480621338, 4320.1162109375, 91.179145812988)',
      'GMR.DefineUnstuck(25.801908493042, 4305.0122070312, 89.94995880127)',
      'GMR.DefineUnstuck(14.032560348511, 4286.1674804688, 87.447547912598)',
      'GMR.DefineUnstuck(0.93573379516602, 4265.197265625, 86.408828735352)',
      'GMR.DefineUnstuck(-7.6100721359253, 4243.4970703125, 84.644905090332)',
      'GMR.DefineUnstuck(-15.242209434509, 4216.9794921875, 82.454200744629)',
      'GMR.DefineUnstuck(-23.592479705811, 4197, 81.005111694336)',
      'GMR.DefineUnstuck(-37.659698486328, 4176.4360351562, 80.021522521973)',
      'GMR.DefineUnstuck(-43.763534545898, 4156.4897460938, 79.563125610352)',
      'GMR.DefineUnstuck(-47.656192779541, 4132.5234375, 81.088722229004)',
      'GMR.DefineUnstuck(-51.132457733154, 4108.072265625, 80.346389770508)',
      'GMR.DefineUnstuck(-54.261775970459, 4086.0615234375, 83.864036560059)',
      'GMR.DefineUnstuck(-60.642742156982, 4063.1691894531, 88.790176391602)',
      'GMR.DefineUnstuck(-69.077796936035, 4039.9584960938, 97.13028717041)',
      'GMR.DefineUnstuck(-79.482940673828, 4011.3264160156, 100.17750549316)',
      'GMR.DefineUnstuck(-92.513374328613, 3981.8325195312, 100.26639556885)',
      'GMR.DefineUnstuck(-105.67876434326, 3957.5705566406, 99.647956848145)',
      'GMR.DefineUnstuck(-112.5747756958, 3928.8544921875, 95.941940307617)',
      'GMR.DefineUnstuck(-119.40774536133, 3904.6857910156, 92.466499328613)',
      'GMR.DefineUnstuck(-126.24071502686, 3880.5173339844, 89.47135925293)',
      'GMR.DefineUnstuck(-133.52311706543, 3854.7587890625, 85.927879333496)',
      'GMR.DefineUnstuck(-139.23492431641, 3828.3662109375, 82.563049316406)',
      'GMR.DefineUnstuck(-129.4409942627, 3798.6533203125, 76.288719177246)',
      'GMR.DefineUnstuck(-115.35123443604, 3772.1022949219, 71.027168273926)',
      'GMR.DefineUnstuck(-99.975814819336, 3740.7397460938, 69.047592163086)',
      'GMR.DefineUnstuck(-89.480453491211, 3712.0961914062, 66.938804626465)',
      'GMR.DefineUnstuck(-84.237922668457, 3679.7724609375, 65.560272216797)',
      'GMR.DefineUnstuck(-77.482055664062, 3648.7302246094, 66.098205566406)',
      'GMR.DefineUnstuck(-59.923645019531, 3616.82421875, 72.00611114502)',
      'GMR.DefineUnstuck(-46.556621551514, 3585.2927246094, 73.814559936523)',
      'GMR.DefineUnstuck(-46.185981750488, 3562.5390625, 73.579414367676)',
      'GMR.DefineUnstuck(-49.204113006592, 3554.595703125, 73.915168762207)',
      'GMR.DefineUnstuck(-55.423389434814, 3539.8642578125, 73.682983398438)',
      'GMR.DefineUnstuck(-61.772792816162, 3525.6491699219, 73.601142883301)',
      'GMR.DefineUnstuck(-65.306518554688, 3508.7280273438, 73.621772766113)',
      'GMR.DefineUnstuck(-65.464691162109, 3491.90625, 73.561325073242)',
      'GMR.DefineUnstuck(-64.792419433594, 3474.6291503906, 73.215408325195)',
      'GMR.DefineUnstuck(-64.056465148926, 3455.7155761719, 71.898216247559)',
      'GMR.DefineUnstuck(-61.621246337891, 3439.5009765625, 68.879287719727)',
      'GMR.DefineUnstuck(-60.281181335449, 3422.4133300781, 64.728729248047)',
      'GMR.DefineUnstuck(-60.829933166504, 3406.3508300781, 61.520374298096)',
      'GMR.DefineUnstuck(-61.434135437012, 3388.6650390625, 56.367538452148)',
      'GMR.DefineUnstuck(-60.817451477051, 3371.0085449219, 50.616249084473)',
      'GMR.DefineUnstuck(-59.230827331543, 3356.2678222656, 46.848922729492)',
      'GMR.DefineUnstuck(-57.509365081787, 3340.2741699219, 42.060340881348)',
      'GMR.DefineUnstuck(-53.874046325684, 3326.0256347656, 37.411499023438)',
      'GMR.DefineUnstuck(-50.960403442383, 3309.8212890625, 32.568229675293)',
      'GMR.DefineUnstuck(-49.686546325684, 3294.673828125, 28.503187179565)',
      'GMR.DefineUnstuck(-51.783763885498, 3279.2919921875, 24.508863449097)',
      'GMR.DefineUnstuck(-55.645835876465, 3263.2873535156, 20.391611099243)',
      'GMR.DefineUnstuck(-58.101081848145, 3247.0295410156, 15.32635974884)',
      'GMR.DefineUnstuck(-59.791519165039, 3229.7944335938, 10.28054523468)',
      'GMR.DefineUnstuck(-60.07931137085, 3214.2478027344, 7.009672164917)',
      'GMR.DefineUnstuck(-58.792430877686, 3199.4858398438, 4.1057734489441)',
      'GMR.DefineUnstuck(-57.61491394043, 3183.9201660156, -0.20729729533195)',
      'GMR.DefineUnstuck(-50.893333435059, 3171.0844726562, -2.0457005500793)',
      'GMR.DefineUnstuck(-42.919219970703, 3158.1469726562, -2.3508536815643)',
      'GMR.DefineUnstuck(-37.692832946777, 3143.0769042969, -1.4169269800186)',
      'GMR.DefineUnstuck(-33.645462036133, 3127.1325683594, -1.3159115314484)',
      'GMR.DefineUnstuck(-29.485363006592, 3110.7443847656, -1.2559345960617)',
      'GMR.DefineUnstuck(-24.828302383423, 3092.3981933594, -1.1411733627319)',
      'GMR.DefineUnstuck(-20.574256896973, 3075.6396484375, 0.67513447999954)',
      'GMR.DefineUnstuck(-16.502775192261, 3059.6003417969, 0.60848861932755)',
      'GMR.DefineUnstuck(-12.265953063965, 3042.9096679688, 3.7346317768097)',
      'GMR.DefineUnstuck(-5.5566182136536, 3028.6589355469, 5.6574473381042)',
      'GMR.DefineUnstuck(7.229453086853, 3020.2736816406, 8.7598495483398)',
      'GMR.DefineUnstuck(22.631301879883, 3013.3562011719, 10.48907661438)',
      'GMR.DefineUnstuck(40.280849456787, 3005.4291992188, 12.338593482971)',
      'GMR.DefineUnstuck(55.969829559326, 2999.4479980469, 14.425075531006)',
      'GMR.DefineUnstuck(74.476554870605, 2995.4106445312, 16.157949447632)',
      'GMR.DefineUnstuck(92.186721801758, 2990.0866699219, 17.91241645813)',
      'GMR.DefineUnstuck(108.7989654541, 2983.9509277344, 19.224559783936)',
      'GMR.DefineUnstuck(125.77973175049, 2977.6791992188, 20.145227432251)',
      'GMR.DefineUnstuck(142.40591430664, 2971.5383300781, 20.481517791748)',
      'GMR.DefineUnstuck(156.5708770752, 2967.3229980469, 20.039489746094)',
      'GMR.DefineUnstuck(169.79611206055, 2961.8767089844, 19.65213394165)',
      'GMR.DefineUnstuck(183.21551513672, 2954.6701660156, 20.874994277954)',
      'GMR.DefineUnstuck(198.80429077148, 2950.4494628906, 22.238010406494)',
      'GMR.DefineUnstuck(213.96255493164, 2950.7365722656, 22.397804260254)',
      'GMR.DefineUnstuck(231.11363220215, 2952.9240722656, 22.30252456665)',
      'GMR.DefineUnstuck(247.09970092773, 2953.984375, 23.226480484009)',
      'GMR.DefineUnstuck(264.80841064453, 2953.7690429688, 23.693521499634)',
      'GMR.DefineUnstuck(281.22329711914, 2954.4260253906, 23.480575561523)',
      'GMR.DefineUnstuck(298.85992431641, 2955.2954101562, 22.397827148438)',
      'GMR.DefineUnstuck(312.84490966797, 2955.1252441406, 23.207004547119)',
      'GMR.DefineUnstuck(331.37954711914, 2954.8999023438, 27.069799423218)',
      'GMR.DefineUnstuck(348.26229858398, 2954.6945800781, 27.445980072021)',
      'GMR.DefineUnstuck(368.44027709961, 2954.44921875, 28.033048629761)',
      'GMR.DefineUnstuck(389.01876831055, 2954.1989746094, 28.662113189697)',
      'GMR.DefineUnstuck(414.66262817383, 2942.6728515625, 30.912595748901)',
      'GMR.DefineUnstuck(428.03140258789, 2931.1748046875, 34.591686248779)',
      'GMR.DefineUnstuck(440.02877807617, 2920.6403808594, 38.156635284424)',
      'GMR.DefineUnstuck(449.58547973633, 2909.8608398438, 44.109130859375)',
      'GMR.DefineUnstuck(458.314453125, 2897.5913085938, 49.333694458008)',
      'GMR.DefineUnstuck(455.31088256836, 2888.2736816406, 53.354049682617)',    
      'GMR.DefineProfileCenter(186.0473, 3006.551, -1.128675, 120)',
      'GMR.DefineProfileCenter(19.70798, 3069.375, -0.7891659, 120)',
      'GMR.DefineProfileCenter(-14.56947, 3095.883, -0.02396658, 120)',
      'GMR.DefineProfileCenter(8.430373, 3180.609, 9.531531, 120)',
      'GMR.DefineProfileCenter(185.4747, 3006.542, -1.113011, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(183934)',
      'GMR.DefineQuestEnemyId(181798)',
      'GMR.DefineCustomObjectId(183934)',
      'GMR.DefineCustomObjectId(181798)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE How To Serve Goblins |r",
    QuestID = 10238,
    QuestType = "Custom",
    PickUp = { x = 430.101013, y = 2886.580078, z = 52.355801, id = 16915 },
    TurnIn = { x = 430.101013, y = 2886.580078, z = 52.355801, id = 16915 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10238, 1) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(67.398277, 3209.460938, 32.005737, 5) then 
            GMR.MeshTo(67.398277, 3209.460938, 32.005737)
          else
            local object1 = GMR.GetObjectWithInfo({ id = 183936, rawType = 8 })
            if object1 then
              GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
            end
          end
        elseif not GMR.Questing.IsObjectiveCompleted(10238, 2) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(-73.447258, 3140.181396, -4.285191, 5) then 
            GMR.MeshTo(-73.447258, 3140.181396, -4.285191)
          else
            local object1 = GMR.GetObjectWithInfo({ id = 183940, rawType = 8 })
            if object1 then
              GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
            end
          end
        elseif not GMR.Questing.IsObjectiveCompleted(10238, 3) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(-125.551750, 3087.864746, 3.147377, 5) then 
            GMR.MeshTo(-125.551750, 3087.864746, 3.147377)
          else
            local object1 = GMR.GetObjectWithInfo({ id = 183941, rawType = 8 })
            if object1 then
              GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
            end
          end
        end
      ]],
    Profile = {
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Missing Missive - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 9373,
    QuestType = "Grinding",
    PickUp = { x = -768.5029296875, y = 2943.7661132812, z = 17.12494468689, id = { 16857, 23338 } },
    TurnIn = { x = -312.484985, y = 4728.77002, z = 17.3123, id = 16991 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
        if not GMR.Frames.Rand008 then
        GMR.Print("Frame created")
        GMR.Frames.Rand008 = CreateFrame("frame")
        GMR.Frames.Rand008:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9373 then
            if GMR.IsExecuting() and GetItemCount(23338) >= 1 and not GMR.IsQuestActive(9373) then
              local itemName = GetItemInfo(23338)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand008 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-768.5029296875, 2943.7661132812, 17.12494468689, 100)',
      'GMR.DefineProfileCenter(-778.78082275391, 2988.6823730469, 12.973566055298, 100)',
      'GMR.DefineProfileCenter(-768.77099609375, 3039.1943359375, 7.6164937019348, 100)',
      'GMR.DefineProfileCenter(-747.03527832031, 3050.0461425781, 8.1221218109131, 100)',
      'GMR.DefineProfileCenter(-707.62463378906, 3031.0146484375, 11.272943496704, 100)',
      'GMR.DefineProfileCenter(-705.63928222656, 3108.7399902344, -2.9489710330963, 100)',
      'GMR.DefineProfileCenter(-799.50708007812, 3156.0993652344, -1.3850182294846, 100)',
      'GMR.DefineProfileCenter(-788.42102050781, 3191.5014648438, 11.248539924622, 100)',
      'GMR.DefineProfileCenter(-714.01147460938, 3015.0627441406, 14.666929244995, 100)',
      'GMR.DefineProfileCenter(-854.25439453125, 2951.6286621094, 6.8976845741272, 100)',
      'GMR.DefineProfileCenter(-874.98364257812, 2983.9907226562, 10.158505439758, 100)',
      'GMR.DefineProfileCenter(-869.09735107422, 3009.1518554688, 7.9157338142395, 100)',
      'GMR.DefineProfileCenter(-854.25512695312, 3043.89453125, 8.8880434036255, 100)',
      'GMR.DefineProfileCenter(-867.38897705078, 2941.5170898438, 8.0647888183594, 100)',
      'GMR.DefineProfileCenter(-822.98089599609, 3024.5847167969, -2.8301634788513, 100)',
      'GMR.DefineProfileCenter(-819.81231689453, 3183.0966796875, 7.6558847427368, 100)',
      'GMR.DefineProfileCenter(-756.88482666016, 3009.3896484375, 12.673776626587, 100)',
      'GMR.DefineProfileCenter(-740.88714599609, 2915.3881835938, 24.829401016235, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineQuestEnemyId(16857)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Cenarion Expedition |r",
    QuestID = 9912,
    QuestType = "TalkTo",
    PickUp = { x = -314.819, y = 4711.439941, z = 18.420401, id = 16885 },
    TurnIn = { x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-143.451996, 5533.819824, 31.148800, 10)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineRepairVendor(105.50184631348, 4337.619140625, 101.46614074707, 19001)',
      'GMR.DefineGoodsVendor(95.0439453125, 4292.25390625, 101.88876342773, 16798)',
      'GMR.DefineAmmoVendor(-708.870972, 2739.129883, 94.816803, 16826)',
      'GMR.DefineProfileMailbox(194.474, 4326.633, 116.7475, 183038)',
      'GMR.DefineCustomObjectId(17841)'
    }
  })
  
  if not GMR.IsQuestCompleted(9728) and not GMR.IsQuestCompleted(9778) then
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Warden Hamoot |r",
    QuestID = 9778,
    QuestType = "TalkTo",
    PickUp = { x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
    TurnIn = { x = -252.358994, y = 5499.209961, z = 66.682503, id = 17858 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-252.358994, 5499.209961, 66.682503, 10)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineHearthstoneBindLocation(-174.47799682617, 5529.2114257812, 29.407695770264, 18907, 1945)',
      'GMR.DefineCustomObjectId(17858)'
    }
  })
  end
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Zangermarsh INITIAL Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9716, x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
      { questId = 9802, x = -215.542999, y = 5437.27002, z = 21.5109, id = 17909 },
      { questId = 9747, x = -215.167007, y = 5433.430176, z = 22.0385, id = 17956 },
      { questId = 9895, x = -181.380005, y = 5527.089844, z = 29.490801, id = 17834 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineBlacklistItem("Unidentified Plant Parts")',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineHearthstoneBindLocation(-174.47799682617, 5529.2114257812, 29.407695770264, 18907, 1945)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Plants of Zangermarsh |r",
    QuestID = 9802,
    QuestType = "Custom",
    PickUp = { x = -215.542999, y = 5437.27002, z = 21.5109, id = 17909 },
    TurnIn = { x = -215.542999, y = 5437.27002, z = 21.5109, id = 17909 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9802, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineBlacklistItem("Unidentified Plant Parts")',
      'GMR.DefineProfileCenter(-426.9558, 5344.063, 17.66883, 80)',
      'GMR.DefineProfileCenter(-581.963, 5354.705, 17.65896, 80)',
      'GMR.DefineProfileCenter(-685.9059, 5471.704, 17.91827, 80)',
      'GMR.DefineProfileCenter(-613.1077, 5536.559, 21.39618, 80)',
      'GMR.DefineProfileCenter(-415.3342, 5553.786, 18.3941, 80)',
      'GMR.DefineProfileCenter(-82.12082, 5404.4, 22.97149, 80)',
      'GMR.DefineProfileCenter(5.039988, 5446.448, 18.74086, 80)',
      'GMR.DefineProfileCenter(-44.01768, 5672.346, 17.37427, 80)',
      'GMR.DefineProfileCenter(34.41599, 5800.492, 21.69576, 80)',
      'GMR.DefineProfileCenter(-86.11473, 6010.584, 17.82948, 80)',
      'GMR.DefineProfileCenter(-321.7271, 5967.511, 17.79113, 80)',
      'GMR.DefineProfileCenter(-300.2227, 6203.853, 21.36229, 80)',
      'GMR.DefineProfileCenter(-165.1665, 6211.103, 17.76087, 80)',
      'GMR.DefineProfileCenter(39.75357, 5849.799, 23.79251, 80)',
      'GMR.DefineProfileCenter(-35.23071, 5708.679, 21.80742, 80)',
      'GMR.DefineProfileCenter(63.11029, 5544.502, 21.69208, 80)',
      'GMR.DefineProfileCenter(-31.64867, 5389.463, 21.87512, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineHearthstoneBindLocation(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineQuestEnemyId(18134)',
      'GMR.DefineQuestEnemyId(18132)',
      'GMR.DefineQuestEnemyId(20387)',
      'GMR.DefineQuestEnemyId(18130)',
      'GMR.DefineQuestEnemyId(18086)',
      'GMR.DefineQuestEnemyId(18087)'
    }
  })
  

    GMR.DefineQuest({
    QuestName = "|cFF1E90FE Disturbance at Umbrafen Lake |r",
    QuestID = 9716,
    QuestType = "Custom",
    PickUp = { x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
    TurnIn = { x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9716, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-760.695618, 5927.675293, 20.429270, 5) then 
          GMR.MeshTo(-760.695618, 5927.675293, 20.429270)
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-760.695618, 5927.675293, 20.429270, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineHearthstoneBindLocation(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineQuestEnemyId(18340)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE As The Crow Flies |r",
    QuestID = 9718,
    QuestType = "Custom",
    PickUp = { x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
    TurnIn = { x = -143.451996, y = 5533.819824, z = 31.1488, id = 17841 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9718, 1) and GetItemCooldown(25465) == 0 and not GMR.IsInVehicle() then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-143.451996, 5533.819824, 31.148800, 3) then 
          GMR.MeshTo(-143.451996, 5533.819824, 31.148800)
        else
          if not timer then 
            timer = GetTime()+6
          elseif timer < GetTime() then 
            GMR.SetQuestingState("Idle")
            timer = nil;
            print("Useing Item to get on Bird.")
            local itemName = GetItemInfo(25465)
            GMR.Use(itemName)
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-143.451996, 5533.819824, 31.148800, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(17841)'
    }
  })

  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Count of the Marshes |r - FarmItemAndAcceptQuest",
    QuestID = 9911,
    QuestType = "Grinding",
    PickUp = { x = -35.117576599121, y = 7847.4326171875, z = 22.631330490112, id = { 18285, 25459 } },
    TurnIn = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand036 then
        GMR.Print("Frame created")
        GMR.Frames.Rand036 = CreateFrame("frame")
        GMR.Frames.Rand036:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9911 then
            if GMR.IsExecuting() then
              if GetItemCount(25459) >= 1 and not GMR.IsQuestActive(9911) then
                local itemName = GetItemInfo(25459)
                GMR.Use(itemName)
                if QuestFrameDetailPanel:IsShown() then
                  QuestFrameAcceptButton:Click()
                elseif QuestFrame:IsShown() then
                  QuestFrameAcceptButton:Click()
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand036 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-205.8783416748, 5569.0390625, 23.4625415802, 40)',
      'GMR.DefineUnstuck(-192.21296691895, 5567.2685546875, 24.115159988403)',
      'GMR.DefineUnstuck(-173.72064208984, 5565.1875, 24.7721824646)',
      'GMR.DefineUnstuck(-159.65222167969, 5558.9555664062, 23.769044876099)',
      'GMR.DefineUnstuck(-144.63177490234, 5556.3032226562, 24.133529663086)',
      'GMR.DefineUnstuck(-126.97984313965, 5554.4443359375, 23.993967056274)',
      'GMR.DefineUnstuck(-109.55807495117, 5549.7421875, 23.395774841309)',
      'GMR.DefineUnstuck(-91.152961730957, 5546.0532226562, 23.123081207275)',
      'GMR.DefineUnstuck(-78.145484924316, 5546.1303710938, 23.135887145996)',
      'GMR.DefineUnstuck(-71.533843994141, 5550.3012695312, 23.28849029541)',
      'GMR.DefineUnstuck(-65.691917419434, 5557.8466796875, 23.422256469727)',
      'GMR.DefineUnstuck(-59.033882141113, 5568.3896484375, 23.419553756714)',
      'GMR.DefineUnstuck(-55.772327423096, 5580.7260742188, 23.36449432373)',
      'GMR.DefineUnstuck(-52.305763244629, 5600.5087890625, 22.457618713379)',
      'GMR.DefineUnstuck(-46.471099853516, 5620.4946289062, 21.781778335571)',
      'GMR.DefineUnstuck(-37.313682556152, 5636.4946289062, 22.223747253418)',
      'GMR.DefineUnstuck(-27.816896438599, 5653.0874023438, 19.651592254639)',
      'GMR.DefineUnstuck(-19.304840087891, 5667.9599609375, 17.521991729736)',
      'GMR.DefineUnstuck(-14.154106140137, 5686.462890625, 17.466091156006)',
      'GMR.DefineUnstuck(-9.8515977859497, 5704.6674804688, 22.773122787476)',
      'GMR.DefineUnstuck(-2.9551124572754, 5721.7763671875, 23.110158920288)',
      'GMR.DefineUnstuck(2.4067108631134, 5740.0151367188, 19.200393676758)',
      'GMR.DefineUnstuck(5.7933416366577, 5758.8198242188, 19.174844741821)',
      'GMR.DefineUnstuck(9.0628480911255, 5776.9741210938, 17.3743724823)',
      'GMR.DefineUnstuck(11.202169418335, 5795.5336914062, 18.859222412109)',
      'GMR.DefineUnstuck(7.5128407478333, 5810.2543945312, 23.670398712158)',
      'GMR.DefineUnstuck(5.5681028366089, 5821.5224609375, 23.803510665894)',
      'GMR.DefineUnstuck(4.9822783470154, 5841.6069335938, 21.867666244507)',
      'GMR.DefineUnstuck(4.4248633384705, 5860.7172851562, 23.934511184692)',
      'GMR.DefineUnstuck(0.88640069961548, 5878.4213867188, 21.293329238892)',
      'GMR.DefineUnstuck(-3.7427468299866, 5897.6279296875, 17.396814346313)',
      'GMR.DefineUnstuck(-6.0275187492371, 5916.9536132812, 20.129346847534)',
      'GMR.DefineUnstuck(0.55049562454224, 5936.9907226562, 21.395721435547)',
      'GMR.DefineUnstuck(2.7021706104279, 5954.197265625, 20.857707977295)',
      'GMR.DefineUnstuck(3.5632653236389, 5974.921875, 20.607107162476)',
      'GMR.DefineUnstuck(4.3973927497864, 5994.9975585938, 19.05287361145)',
      'GMR.DefineUnstuck(-5.9890561103821, 6013.0161132812, 20.048854827881)',
      'GMR.DefineUnstuck(-20.751279830933, 6029.86328125, 17.471134185791)',
      'GMR.DefineUnstuck(-27.767936706543, 6048.9638671875, 20.798049926758)',
      'GMR.DefineUnstuck(-29.782962799072, 6070.9228515625, 21.872703552246)',
      'GMR.DefineUnstuck(-33.035736083984, 6092.2729492188, 23.231439590454)',
      'GMR.DefineUnstuck(-38.173881530762, 6113.6245117188, 22.838489532471)',
      'GMR.DefineUnstuck(-42.092784881592, 6134.2265625, 23.840021133423)',
      'GMR.DefineUnstuck(-39.906703948975, 6157.3403320312, 22.877870559692)',
      'GMR.DefineUnstuck(-35.922637939453, 6180.3955078125, 18.753149032593)',
      'GMR.DefineUnstuck(-21.692266464233, 6200.7416992188, 17.375162124634)',
      'GMR.DefineUnstuck(-14.879969596863, 6206.1000976562, 18.972585678101)',
      'GMR.DefineUnstuck(-8.648211479187, 6225.09765625, 23.269346237183)',
      'GMR.DefineUnstuck(-7.8904309272766, 6250.56640625, 22.266103744507)',
      'GMR.DefineUnstuck(0.36637407541275, 6268.3022460938, 22.625537872314)',
      'GMR.DefineUnstuck(15.292573928833, 6280.66015625, 22.389339447021)',
      'GMR.DefineUnstuck(34.47274017334, 6295.1821289062, 23.137643814087)',
      'GMR.DefineUnstuck(49.088592529297, 6308.4555664062, 23.056690216064)',
      'GMR.DefineUnstuck(56.507850646973, 6326.1533203125, 23.544052124023)',
      'GMR.DefineUnstuck(62.465110778809, 6343.958984375, 22.218257904053)',
      'GMR.DefineUnstuck(65.656204223633, 6360.7587890625, 19.879985809326)',
      'GMR.DefineUnstuck(72.399154663086, 6375.6567382812, 17.827909469604)',
      'GMR.DefineUnstuck(83.22135925293, 6392.8901367188, 17.376943588257)',
      'GMR.DefineUnstuck(95.696426391602, 6409.0415039062, 17.489450454712)',
      'GMR.DefineUnstuck(107.91696929932, 6420.9516601562, 17.520536422729)',
      'GMR.DefineUnstuck(118.2691192627, 6439.4931640625, 17.375598907471)',
      'GMR.DefineUnstuck(125.61906433105, 6460.9609375, 17.449132919312)',
      'GMR.DefineUnstuck(131.53210449219, 6481.5732421875, 18.085210800171)',
      'GMR.DefineUnstuck(142.66752624512, 6497.701171875, 21.076856613159)',
      'GMR.DefineUnstuck(158.8695526123, 6514.111328125, 21.933235168457)',
      'GMR.DefineUnstuck(167.50085449219, 6532.1635742188, 23.037832260132)',
      'GMR.DefineUnstuck(171.951171875, 6555.37890625, 20.583652496338)',
      'GMR.DefineUnstuck(174.6927947998, 6574.560546875, 23.672510147095)',
      'GMR.DefineUnstuck(174.40010070801, 6595.9487304688, 22.692638397217)',
      'GMR.DefineUnstuck(173.4069519043, 6617.6879882812, 22.517755508423)',
      'GMR.DefineUnstuck(173.05651855469, 6640.3735351562, 22.240692138672)',
      'GMR.DefineUnstuck(172.43452453613, 6661.0141601562, 19.459949493408)',
      'GMR.DefineUnstuck(170.03713989258, 6681.9560546875, 20.22421836853)',
      'GMR.DefineUnstuck(167.41429138184, 6704.8671875, 22.070142745972)',
      'GMR.DefineUnstuck(163.16668701172, 6729.7622070312, 20.482460021973)',
      'GMR.DefineUnstuck(153.11212158203, 6750.73828125, 18.330654144287)',
      'GMR.DefineUnstuck(139.20378112793, 6774.677734375, 20.179636001587)',
      'GMR.DefineUnstuck(128.02778625488, 6796.2763671875, 21.803861618042)',
      'GMR.DefineUnstuck(119.66088104248, 6819.7900390625, 20.981588363647)',
      'GMR.DefineUnstuck(108.50313568115, 6842.5600585938, 17.483600616455)',
      'GMR.DefineUnstuck(96.182312011719, 6867.7036132812, 17.441343307495)',
      'GMR.DefineUnstuck(86.907180786133, 6886.6313476562, 17.698202133179)',
      'GMR.DefineUnstuck(77.279663085938, 6907.9409179688, 19.389169692993)',
      'GMR.DefineUnstuck(67.702606201172, 6931.4194335938, 17.890161514282)',
      'GMR.DefineUnstuck(61.666126251221, 6952.4399414062, 21.44379234314)',
      'GMR.DefineUnstuck(60.29076385498, 6978.3618164062, 23.955015182495)',
      'GMR.DefineUnstuck(60.976593017578, 7001.4125976562, 21.946557998657)',
      'GMR.DefineUnstuck(61.613216400146, 7022.8090820312, 23.040628433228)',
      'GMR.DefineUnstuck(62.965492248535, 7046.7827148438, 18.256303787231)',
      'GMR.DefineUnstuck(64.506065368652, 7068.3486328125, 17.605142593384)',
      'GMR.DefineUnstuck(68.030296325684, 7087.9545898438, 17.453151702881)',
      'GMR.DefineUnstuck(76.36678314209, 7111.828125, 18.128784179688)',
      'GMR.DefineUnstuck(89.405548095703, 7135.1162109375, 19.892854690552)',
      'GMR.DefineUnstuck(92.060913085938, 7155.4116210938, 21.353521347046)',
      'GMR.DefineUnstuck(77.599449157715, 7178.7202148438, 23.529531478882)',
      'GMR.DefineUnstuck(65.239639282227, 7202.8735351562, 22.210899353027)',
      'GMR.DefineUnstuck(68.10871887207, 7228.5947265625, 22.671073913574)',
      'GMR.DefineUnstuck(66.676490783691, 7251.6069335938, 23.131772994995)',
      'GMR.DefineUnstuck(61.337989807129, 7278.431640625, 17.810384750366)',
      'GMR.DefineUnstuck(58.327480316162, 7303.7534179688, 18.378379821777)',
      'GMR.DefineUnstuck(60.31441116333, 7328.0551757812, 17.387935638428)',
      'GMR.DefineUnstuck(62.219200134277, 7351.3515625, 17.601211547852)',
      'GMR.DefineUnstuck(64.313827514648, 7376.9702148438, 18.602397918701)',
      'GMR.DefineUnstuck(66.649406433105, 7405.5356445312, 22.048118591309)',
      'GMR.DefineUnstuck(68.340629577637, 7426.2202148438, 23.397790908813)',
      'GMR.DefineUnstuck(79.203559875488, 7448.5947265625, 21.711742401123)',
      'GMR.DefineUnstuck(86.145614624023, 7470.6752929688, 22.17279624939)',
      'GMR.DefineUnstuck(89.164825439453, 7497.765625, 21.693088531494)',
      'GMR.DefineUnstuck(89.535217285156, 7521.4956054688, 18.398778915405)',
      'GMR.DefineUnstuck(89.910331726074, 7545.5278320312, 18.861148834229)',
      'GMR.DefineUnstuck(86.556632995605, 7570.2368164062, 17.375009536743)',
      'GMR.DefineUnstuck(81.538421630859, 7595.7661132812, 19.009452819824)',
      'GMR.DefineUnstuck(77.630332946777, 7621.69921875, 22.07287979126)',
      'GMR.DefineUnstuck(75.599449157715, 7649.0439453125, 17.493499755859)',
      'GMR.DefineUnstuck(68.465072631836, 7675.9848632812, 22.410076141357)',
      'GMR.DefineUnstuck(64.867202758789, 7704.0239257812, 20.150554656982)',
      'GMR.DefineUnstuck(63.076854705811, 7733.931640625, 19.615259170532)',
      'GMR.DefineUnstuck(61.581066131592, 7758.9189453125, 17.797702789307)',
      'GMR.DefineUnstuck(57.90119934082, 7779.9482421875, 18.694175720215)',
      'GMR.DefineUnstuck(49.840843200684, 7800.2319335938, 18.368978500366)',
      'GMR.DefineUnstuck(33.910850524902, 7815.146484375, 19.169612884521)',    
      'GMR.DefineProfileCenter(-35.117576599121, 7847.4326171875, 22.631330490112, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(323.088013, 7839.830078, 22.167400, 19383)',
      'GMR.DefineRepairVendor(323.088013, 7839.830078, 22.167400, 19383)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(228.162994, 7933.879883, 25.161100, 18245)',
      'GMR.DefineProfileMailbox(258.980835, 7869.787598, 23.053631, 182567)',
      'GMR.DefineQuestEnemyId(18285)'
    }
  })
  

  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Observing the Sporelings / The Sporelings Plight / Natural Enemies |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9701, x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
      { questId = 9739, x = -205.360001, y = 8516.549805, z = 22.802799, id = 17923 },
      { questId = 9743, x = -205.360001, y = 8516.549805, z = 22.802799, id = 17923 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Sporelings' Plight |r",
    QuestID = 9739,
    QuestType = "Custom",
    PickUp = { x = -205.360001, y = 8516.549805, z = 22.802799, id = 17923 },
    TurnIn = { x = -205.360001, y = 8516.549805, z = 22.802799, id = 17923 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9739, 1) then
          GMR.SetQuestingState(nil)
        local npc1 = GMR.GetObjectWithInfo({ id = 182069, rawType = 8 })
        if npc1 then
          GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.DefineBlacklistItem("Bog Lord Tendril")',
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-62.647716522217, 8885.7470703125, 21.895029067993, 100)',
      'GMR.DefineProfileCenter(-69.728073120117, 8814.013671875, 18.435388565063, 100)',
      'GMR.DefineProfileCenter(-11.81653213501, 8851.0634765625, 21.225498199463, 100)',
      'GMR.DefineProfileCenter(-80.628868103027, 8658.8359375, 17.291139602661, 100)',
      'GMR.DefineProfileCenter(-132.67021179199, 8637.267578125, 23.645816802979, 100)',
      'GMR.DefineProfileCenter(-84.839202880859, 8568.1103515625, 21.727832794189, 100)',
      'GMR.DefineProfileCenter(-164.57652282715, 8689.1552734375, 19.320672988892, 100)',
      'GMR.DefineProfileCenter(-155.01155090332, 8779.9365234375, 22.475610733032, 100)',
      'GMR.DefineProfileCenter(-176.50311279297, 8910.6044921875, 21.32154083252, 100)',
      'GMR.DefineProfileCenter(-143.97854614258, 8960.333984375, 19.44734954834, 100)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineCustomObjectId(182069)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Natural Enemies |r",
    QuestID = 9743,
    QuestType = "Custom",
    PickUp = { x = -205.360001, y = 8516.549805, z = 22.802799, id = 17923 },
    TurnIn = { x = -205.360001, y = 8516.549805, z = 22.802799, id = 17923 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9743, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand0326 then
        GMR.Print("Frame created")
        GMR.Frames.Rand0326 = CreateFrame("frame")
        GMR.Frames.Rand0326:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9743 then
            if GMR.IsExecuting() and not GMR.IsQuestCompleted(9743) then
              if GossipFrameGreetingPanel:IsShown() then
                GossipTitleButton3:Click()
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand0326 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-151.4459, 8590.196, 18.03241, 120)',
      'GMR.DefineProfileCenter(-124.4017, 8712.889, 20.02133, 120)',
      'GMR.DefineProfileCenter(-65.71978, 8848.493, 18.08599, 120)',
      'GMR.DefineProfileCenter(-138.2027, 8932.474, 18.49195, 120)',
      'GMR.DefineProfileCenter(-172.1488, 8738.775, 17.85848, 120)',
      'GMR.DefineProfileCenter(-123.6163, 8563.26, 17.98658, 120)',
      'GMR.DefineProfileCenter(-173.9273, 8111.625, 19.06802, 120)',
      'GMR.DefineProfileCenter(-151.9731, 7965.103, 18.6653, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(19519)',
      'GMR.DefineQuestEnemyId(18125)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Observing the Sporelings |r",
    QuestID = 9701,
    QuestType = "Custom",
    PickUp = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    TurnIn = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9701, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-121.9265, 8716.961, 20.82617, 5) then 
          GMR.MeshTo(-121.9265, 8716.961, 20.82617)
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-121.9265, 8716.961, 20.82617, 5)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(999999)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Question of Gluttony |r",
    QuestID = 9702,
    QuestType = "Custom",
    PickUp = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    TurnIn = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9702, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-241.3073, 8120.8, 19.21942, 80)',
      'GMR.DefineProfileCenter(-89.98768, 8015.616, 20.28177, 80)',
      'GMR.DefineProfileCenter(-50.03925, 8003.353, 24.83806, 80)',
      'GMR.DefineProfileCenter(-113.6952, 7921.965, 20.22012, 80)',
      'GMR.DefineProfileCenter(-96.33974, 7878.312, 19.46282, 80)',
      'GMR.DefineProfileCenter(-247.6781, 7850.38, 18.9965, 80)',
      'GMR.DefineProfileCenter(-284.0545, 7810.01, 21.18522, 80)',
      'GMR.DefineProfileCenter(-328.1776, 7914.823, 19.26535, 80)',
      'GMR.DefineProfileCenter(-207.7209, 7962.169, 18.55782, 80)',
      'GMR.DefineProfileCenter(-264.6874, 8046.703, 18.50503, 80)',
      'GMR.DefineProfileCenter(-187.7131, 8118.909, 18.34168, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(184578)',
      'GMR.DefineQuestEnemyId(182031)',
      'GMR.DefineQuestEnemyId(182256)',
      'GMR.DefineCustomObjectId(184578)',
      'GMR.DefineCustomObjectId(182031)',
      'GMR.DefineCustomObjectId(182256)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Familiar Fungi |r",
    QuestID = 9708,
    QuestType = "Custom",
    PickUp = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    TurnIn = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9708, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(599.3102, 7869.055, 22.6534, 120)',
      'GMR.DefineProfileCenter(684.1514, 7796.445, 22.05787, 120)',
      'GMR.DefineProfileCenter(785.0506, 7654.917, 18.01879, 120)',
      'GMR.DefineProfileCenter(925.0681, 7659.662, 19.6307, 120)',
      'GMR.DefineProfileCenter(995.9791, 7562.776, 22.29417, 120)',
      'GMR.DefineProfileCenter(915.9423, 7841.169, 20.75157, 120)',
      'GMR.DefineProfileCenter(837.8776, 7953.279, 23.61477, 120)',
      'GMR.DefineProfileCenter(1028.121, 7999.846, 22.42566, 120)',
      'GMR.DefineProfileCenter(797.5071, 7918.987, 19.44754, 120)',
      'GMR.DefineProfileCenter(616.4899, 7874.306, 22.91147, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(18117)',
      'GMR.DefineQuestEnemyId(18118)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Stealing Back the Mushrooms (Familiar Fungi Followup) |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9709, x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.AllowSpeedUp()',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Orebor Harbor Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9848, x = 974.252991, y = 7403.089844, z = 29.6108, id = 18019 },
      { questId = 9835, x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
      { questId = 10115, x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
      { questId = 10116, x = 1021.708557, y = 7376.442871, z = 36.268482, id = 183284 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-120.27281951904, 8207.25390625, 23.581270217896, 100)',
      'GMR.DefineUnstuck(-95.122764587402, 8202.0078125, 23.589792251587)',
      'GMR.DefineUnstuck(-74.6552734375, 8199.8349609375, 22.111856460571)',
      'GMR.DefineUnstuck(-55.130065917969, 8193.7880859375, 22.424873352051)',
      'GMR.DefineUnstuck(-36.632720947266, 8184.7397460938, 22.423648834229)',
      'GMR.DefineUnstuck(-15.488718986511, 8175.8369140625, 22.235395431519)',
      'GMR.DefineUnstuck(4.9345092773438, 8163.4584960938, 22.735698699951)',
      'GMR.DefineUnstuck(28.052072525024, 8155.3530273438, 22.597057342529)',
      'GMR.DefineUnstuck(44.153602600098, 8139.5, 20.795946121216)',
      'GMR.DefineUnstuck(57.757293701172, 8119.224609375, 17.704959869385)',
      'GMR.DefineUnstuck(59.825824737549, 8098.0327148438, 22.48020362854)',
      'GMR.DefineUnstuck(65.253974914551, 8073.0576171875, 22.595539093018)',
      'GMR.DefineUnstuck(78.791397094727, 8056.8706054688, 21.118320465088)',
      'GMR.DefineUnstuck(108.14389801025, 8050.0815429688, 17.597116470337)',
      'GMR.DefineUnstuck(133.79432678223, 8045.6596679688, 22.256969451904)',
      'GMR.DefineUnstuck(164.05487060547, 8037.0727539062, 23.342859268188)',
      'GMR.DefineUnstuck(172.89596557617, 8065.6596679688, 22.644845962524)',
      'GMR.DefineUnstuck(184.10096740723, 8082.3999023438, 22.218124389648)',
      'GMR.DefineUnstuck(209.54225158691, 8077.0434570312, 22.701164245605)',
      'GMR.DefineUnstuck(237.90240478516, 8067.349609375, 17.379077911377)',
      'GMR.DefineUnstuck(263.46469116211, 8058.6118164062, 19.448392868042)',
      'GMR.DefineUnstuck(286.07559204102, 8049.0795898438, 19.124980926514)',
      'GMR.DefineUnstuck(305.92654418945, 8033.892578125, 17.374217987061)',
      'GMR.DefineUnstuck(324.2370300293, 8014.4741210938, 17.374217987061)',
      'GMR.DefineUnstuck(338.00286865234, 7994.5825195312, 21.05023765564)',
      'GMR.DefineUnstuck(349.81829833984, 7971.7768554688, 22.425077438354)',
      'GMR.DefineUnstuck(362.78448486328, 7954.267578125, 21.013795852661)',
      'GMR.DefineUnstuck(383.54693603516, 7936.6860351562, 18.368062973022)',
      'GMR.DefineUnstuck(414.59301757812, 7918.015625, 17.46626663208)',
      'GMR.DefineUnstuck(445.63409423828, 7899.3486328125, 17.374477386475)',
      'GMR.DefineUnstuck(474.48739624023, 7878.2192382812, 23.060771942139)',
      'GMR.DefineUnstuck(496.27142333984, 7853.8071289062, 22.077907562256)',
      'GMR.DefineUnstuck(504.77838134766, 7836.1049804688, 22.734144210815)',
      'GMR.DefineUnstuck(520.70361328125, 7827.7885742188, 18.22582244873)',
      'GMR.DefineUnstuck(544.68707275391, 7814.7490234375, 17.374858856201)',
      'GMR.DefineUnstuck(572.98168945312, 7794.3466796875, 21.021112442017)',
      'GMR.DefineUnstuck(593.97833251953, 7776.4033203125, 23.93000793457)',
      'GMR.DefineUnstuck(612.14215087891, 7760.111328125, 23.861566543579)',
      'GMR.DefineUnstuck(616.45782470703, 7731.0849609375, 23.469194412231)',
      'GMR.DefineUnstuck(631.00305175781, 7704.98046875, 17.782127380371)',
      'GMR.DefineUnstuck(650.16986083984, 7682.001953125, 17.374076843262)',
      'GMR.DefineUnstuck(669.39074707031, 7660.291015625, 17.374633789062)',
      'GMR.DefineUnstuck(693.15625, 7648.9096679688, 21.049798965454)',
      'GMR.DefineUnstuck(720.16821289062, 7638.5322265625, 23.221658706665)',
      'GMR.DefineUnstuck(738.44067382812, 7629.9047851562, 23.820198059082)',
      'GMR.DefineUnstuck(744.99523925781, 7612.6474609375, 22.95548248291)',
      'GMR.DefineUnstuck(749.19110107422, 7585.6767578125, 21.955892562866)',
      'GMR.DefineUnstuck(760.10339355469, 7560.6489257812, 18.900447845459)',
      'GMR.DefineUnstuck(781.00262451172, 7543.1723632812, 17.687076568604)',
      'GMR.DefineUnstuck(805.33392333984, 7528.0678710938, 17.686595916748)',
      'GMR.DefineUnstuck(821.8896484375, 7507.5874023438, 17.87813949585)',
      'GMR.DefineUnstuck(834.07592773438, 7482.4365234375, 17.779979705811)',
      'GMR.DefineUnstuck(852.36303710938, 7454.830078125, 23.347150802612)',
      'GMR.DefineUnstuck(877.41180419922, 7436.7387695312, 20.946203231812)',
      'GMR.DefineUnstuck(894.08673095703, 7417.6518554688, 20.476491928101)',
      'GMR.DefineUnstuck(912.51043701172, 7390.7529296875, 20.169757843018)',
      'GMR.DefineUnstuck(932.78662109375, 7381.4702148438, 19.888450622559)',
      'GMR.DefineUnstuck(959.36456298828, 7373.9448242188, 28.040189743042)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(326.42761230469, 8617.6689453125, 23.913902282715, 20)',
      'GMR.DefineUnstuck(316.47091674805, 8614.0322265625, 23.855766296387)',
      'GMR.DefineUnstuck(300.17395019531, 8610.16796875, 20.634092330933)',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Ango'rosh Encroachment |r",
    QuestID = 9835,
    QuestType = "Custom",
    PickUp = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    TurnIn = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9835, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18118, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9835, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18117, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(999.5164, 7644.563, 22.42755, 120)',
      'GMR.DefineProfileCenter(930.832, 7770.584, 21.42, 120)',
      'GMR.DefineProfileCenter(850.3536, 7665.783, 19.59588, 120)',
      'GMR.DefineProfileCenter(870.773, 7848.341, 22.0232, 120)',
      'GMR.DefineProfileCenter(684.1514, 7796.445, 22.05787, 120)',
      'GMR.DefineProfileCenter(785.0506, 7654.917, 18.01879, 120)',
      'GMR.DefineProfileCenter(925.0681, 7659.662, 19.6307, 120)',
      'GMR.DefineProfileCenter(995.9791, 7562.776, 22.29417, 120)',
      'GMR.DefineProfileCenter(915.9423, 7841.169, 20.75157, 120)',
      'GMR.DefineProfileCenter(837.8776, 7953.279, 23.61477, 120)',
      'GMR.DefineProfileCenter(1028.121, 7999.846, 22.42566, 120)',
      'GMR.DefineProfileCenter(797.5071, 7918.987, 19.44754, 120)',
      'GMR.DefineProfileCenter(616.4899, 7874.306, 22.91147, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(18117)',
      'GMR.DefineQuestEnemyId(18118)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Overlord Gorefist |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9839, x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Daggerfen Deviance |r",
    QuestID = 10115,
    QuestType = "Custom",
    PickUp = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    TurnIn = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10115, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18116, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10115, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18115, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(1162.592, 8043.966, 17.68847, 120)',
      'GMR.DefineProfileCenter(1186.152, 8120.619, 18.00709, 120)',
      'GMR.DefineProfileCenter(1204.365, 8243.709, 17.7778, 120)',
      'GMR.DefineProfileCenter(1085.306, 8204.828, 22.16326, 120)',
      'GMR.DefineProfileCenter(1162.609, 8284.99, 18.37883, 120)',
      'GMR.DefineProfileCenter(1174.122, 8222.207, 17.73498, 120)',
      'GMR.DefineProfileCenter(1186.659, 8142.507, 17.62689, 120)',
      'GMR.DefineProfileCenter(1154.078, 8062.206, 17.6776, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399,17904)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499,18907)',
      'GMR.DefineProfileMailbox(-199.935394, 5507.828125, 22.099514, 183039)',
      'GMR.DefineQuestEnemyId(18116)',
      'GMR.DefineQuestEnemyId(18115)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Secrets of the Daggerfen |r",
    QuestID = 9848,
    QuestType = "Custom",
    PickUp = { x = 974.252991, y = 7403.089844, z = 29.6108, id = 18019 },
    TurnIn = { x = 974.252991, y = 7403.089844, z = 29.6108, id = 18019 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9848, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(1030.4758300781, 8249.3388671875, 57.653644561768, 3) then 
          GMR.MeshTo(1030.4758300781, 8249.3388671875, 57.653644561768)
        else
          local object = GMR.GetObjectWithInfo({ id = 182184, rawType = 8 })
          if not GMR.GetDelay("CustomQuest") and object then
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
          end 
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9848, 2) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(1098.4698486328, 8199.783203125, 23.447790145874, 25) then 
          GMR.MeshTo(1098.4698486328, 8199.783203125, 23.447790145874)
        else
          local object = GMR.GetObjectWithInfo({ id = 182185, rawType = 8 })
          if not GMR.GetDelay("CustomQuest") and object then
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
          else
            GMR.SetQuestingState("Idle")
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1042.5466308594, 8256.2587890625, 57.653442382812, 3)',
      'GMR.DefineUnstuck(1039.4464111328, 8251.73046875, 57.653442382812)',
      'GMR.DefineUnstuck(1040.2691650391, 8250.2978515625, 57.653442382812)',
      'GMR.DefineUnstuck(1041.708984375, 8247.791015625, 56.628265380859)',
      'GMR.DefineUnstuck(1047.2349853516, 8238.1689453125, 22.481639862061)',
      'GMR.DefineUnstuck(1042.400390625, 8256.38671875, 57.653518676758)',
      'GMR.DefineUnstuck(1036.1859130859, 8253.73828125, 57.653518676758)',
      'GMR.DefineUnstuck(1028.0557861328, 8252.7939453125, 57.652034759521)',
      'GMR.DefineUnstuck(1021.8150634766, 8249.884765625, 57.644306182861)',
      'GMR.DefineUnstuck(1020.5618896484, 8247.0634765625, 56.037662506104)',
      'GMR.DefineUnstuck(1018.7260742188, 8242.9306640625, 48.348571777344)',
      'GMR.DefineUnstuck(1015.4650878906, 8235.58984375, 17.375244140625)',
      'GMR.DefineUnstuck(1011.4555053711, 8226.5634765625, 18.016710281372)',
      'GMR.DefineUnstuck(1009.2283325195, 8219.0927734375, 18.957168579102)',
      'GMR.DefineUnstuck(1008.2084350586, 8209.3076171875, 19.407968521118)',
      'GMR.DefineUnstuck(1003.151184082, 8201.875, 21.092136383057)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1038.7733154297, 8252.5537109375, 31.648931503296, 20)',
      'GMR.DefineUnstuck(1037.1790771484, 8258.7275390625, 32.386810302734)',
      'GMR.DefineUnstuck(1033.9133300781, 8265.7705078125, 32.839981079102)',
      'GMR.DefineUnstuck(1030.9704589844, 8268.3740234375, 34.521114349365)',
      'GMR.DefineUnstuck(1029.0718994141, 8269.1669921875, 35.290992736816)',
      'GMR.DefineUnstuck(1027.2203369141, 8267.244140625, 36.351757049561)',
      'GMR.DefineUnstuck(1027.1381835938, 8264.361328125, 37.838108062744)',
      'GMR.DefineUnstuck(1028.8742675781, 8262.3271484375, 38.942314147949)',
      'GMR.DefineUnstuck(1030.8287353516, 8261.705078125, 39.748119354248)',
      'GMR.DefineUnstuck(1032.7056884766, 8262.5654296875, 40.353576660156)',
      'GMR.DefineUnstuck(1033.4876708984, 8263.9814453125, 41.038421630859)',
      'GMR.DefineUnstuck(1033.8946533203, 8265.5712890625, 41.641204833984)',
      'GMR.DefineUnstuck(1033.2525634766, 8267.9443359375, 42.510650634766)',
      'GMR.DefineUnstuck(1030.9938964844, 8269.388671875, 43.313575744629)',
      'GMR.DefineUnstuck(1028.0992431641, 8268.31640625, 44.602939605713)',
      'GMR.DefineUnstuck(1026.4692382812, 8266.2236328125, 45.700462341309)',
      'GMR.DefineUnstuck(1026.5802001953, 8264.1689453125, 46.108085632324)',
      'GMR.DefineUnstuck(1029.1397705078, 8262.443359375, 46.431636810303)',
      'GMR.DefineUnstuck(1031.6586914062, 8262.2314453125, 47.419612884521)',
      'GMR.DefineUnstuck(1033.4458007812, 8263.2529296875, 48.114448547363)',
      'GMR.DefineUnstuck(1034.0867919922, 8265.2080078125, 48.901767730713)',
      'GMR.DefineUnstuck(1033.4108886719, 8266.7158203125, 49.496074676514)',
      'GMR.DefineUnstuck(1031.8428955078, 8269.1123046875, 50.459350585938)',
      'GMR.DefineUnstuck(1028.9560546875, 8268.95703125, 51.51184463501)',
      'GMR.DefineUnstuck(1026.8065185547, 8267.7373046875, 52.419330596924)',
      'GMR.DefineUnstuck(1025.8018798828, 8265.6806640625, 53.109348297119)',
      'GMR.DefineUnstuck(1025.7523193359, 8264.44921875, 53.523555755615)',
      'GMR.DefineUnstuck(1026.9300537109, 8262.7529296875, 54.347480773926)',
      'GMR.DefineUnstuck(1028.96875, 8261.3701171875, 55.291927337646)',
      'GMR.DefineUnstuck(1031.0081787109, 8261.09375, 55.802463531494)',
      'GMR.DefineUnstuck(1032.8695068359, 8262.376953125, 56.773700714111)',
      'GMR.DefineUnstuck(1034.4272460938, 8262.90625, 57.653972625732)',
      'GMR.DefineUnstuck(1035.2027587891, 8263.169921875, 57.653972625732)',
      'GMR.DefineUnstuck(1037.4371337891, 8261.3349609375, 57.653972625732)',
      'GMR.DefineUnstuck(1036.5922851562, 8257.7333984375, 57.653972625732)',
      'GMR.DefineUnstuck(1033.0200195312, 8253.10546875, 57.653972625732)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(986.03082275391, 7388.0278320312, 31.055667877197, 80)',
      'GMR.DefineUnstuck(994.01452636719, 7399.5283203125, 30.911027908325)',
      'GMR.DefineUnstuck(1003.8264770508, 7413.662109375, 29.964351654053)',
      'GMR.DefineUnstuck(1011.6549682617, 7426.2172851562, 28.001979827881)',
      'GMR.DefineUnstuck(1015.5232543945, 7438.7006835938, 27.688631057739)',
      'GMR.DefineUnstuck(1015.73046875, 7452.1806640625, 24.709602355957)',
      'GMR.DefineUnstuck(1013.612487793, 7466.01953125, 19.718742370605)',
      'GMR.DefineUnstuck(1013.7504882812, 7475.775390625, 17.661479949951)',
      'GMR.DefineUnstuck(1019.8723754883, 7489.9599609375, 17.566463470459)',
      'GMR.DefineUnstuck(1027.2272949219, 7498.2036132812, 17.671091079712)',
      'GMR.DefineUnstuck(1038.0423583984, 7508.365234375, 17.62261390686)',
      'GMR.DefineUnstuck(1047.5992431641, 7515.3286132812, 17.75297164917)',
      'GMR.DefineUnstuck(1060.6970214844, 7521.251953125, 19.003408432007)',
      'GMR.DefineUnstuck(1072.7591552734, 7525.4814453125, 19.68222618103)',
      'GMR.DefineUnstuck(1084.0153808594, 7529.427734375, 20.783929824829)',
      'GMR.DefineUnstuck(1096.4299316406, 7534.7490234375, 20.415958404541)',
      'GMR.DefineUnstuck(1103.4644775391, 7542.0048828125, 20.695924758911)',
      'GMR.DefineUnstuck(1109.0444335938, 7552.9765625, 21.756946563721)',
      'GMR.DefineUnstuck(1112.8959960938, 7564.265625, 20.831653594971)',
      'GMR.DefineUnstuck(1117.2856445312, 7577.1313476562, 19.611120223999)',
      'GMR.DefineUnstuck(1123.1311035156, 7592.1010742188, 22.047121047974)',
      'GMR.DefineUnstuck(1127.9676513672, 7604.35546875, 22.871692657471)',
      'GMR.DefineUnstuck(1132.2395019531, 7618.5180664062, 23.05023765564)',
      'GMR.DefineUnstuck(1137.3485107422, 7631.552734375, 24.504323959351)',
      'GMR.DefineUnstuck(1142.7591552734, 7645.3559570312, 23.592426300049)',
      'GMR.DefineUnstuck(1148.0163574219, 7658.7685546875, 24.267290115356)',
      'GMR.DefineUnstuck(1153.9190673828, 7671.8520507812, 25.239376068115)',
      'GMR.DefineUnstuck(1160.7032470703, 7682.1860351562, 26.077354431152)',
      'GMR.DefineUnstuck(1167.4796142578, 7692.5083007812, 25.974296569824)',
      'GMR.DefineUnstuck(1172.6650390625, 7702.2465820312, 25.173431396484)',
      'GMR.DefineUnstuck(1173.4421386719, 7711.9833984375, 23.982862472534)',
      'GMR.DefineUnstuck(1172.8337402344, 7725.9643554688, 25.093700408936)',
      'GMR.DefineUnstuck(1172.2611083984, 7739.1259765625, 24.440103530884)',
      'GMR.DefineUnstuck(1171.7420654297, 7751.056640625, 24.337102890015)',
      'GMR.DefineUnstuck(1171.2406005859, 7762.58203125, 23.691087722778)',
      'GMR.DefineUnstuck(1170.5969238281, 7777.376953125, 23.338146209717)',
      'GMR.DefineUnstuck(1170.0777587891, 7789.3076171875, 23.051712036133)',
      'GMR.DefineUnstuck(1169.1254882812, 7802.4541015625, 25.430051803589)',
      'GMR.DefineUnstuck(1167.0750732422, 7814.1684570312, 25.858287811279)',
      'GMR.DefineUnstuck(1163.5623779297, 7824.71484375, 26.523790359497)',
      'GMR.DefineUnstuck(1162.2944335938, 7833.1645507812, 26.939424514771)',
      'GMR.DefineUnstuck(1162.6090087891, 7847.1484375, 26.218355178833)',
      'GMR.DefineUnstuck(1162.849609375, 7857.8461914062, 26.210433959961)',
      'GMR.DefineUnstuck(1163.1082763672, 7869.34375, 24.87388420105)',
      'GMR.DefineUnstuck(1163.3491210938, 7880.0512695312, 25.966794967651)',
      'GMR.DefineUnstuck(1164.4202880859, 7891.8930664062, 27.291362762451)',
      'GMR.DefineUnstuck(1167.9323730469, 7901.5126953125, 27.003246307373)',
      'GMR.DefineUnstuck(1172.5739746094, 7912.5078125, 26.18879699707)',
      'GMR.DefineUnstuck(1176.2249755859, 7924.2890625, 25.793476104736)',
      'GMR.DefineUnstuck(1180.2532958984, 7937.287109375, 25.696840286255)',
      'GMR.DefineUnstuck(1183.9044189453, 7949.068359375, 25.709175109863)',
      'GMR.DefineUnstuck(1187.6839599609, 7961.2641601562, 26.317602157593)',
      'GMR.DefineUnstuck(1190.8543701172, 7971.494140625, 25.915618896484)',
      'GMR.DefineUnstuck(1193.4912109375, 7983.0581054688, 24.419673919678)',
      'GMR.DefineUnstuck(1191.4497070312, 7991.6669921875, 24.675842285156)',
      'GMR.DefineUnstuck(1185.3388671875, 7999.2788085938, 24.697587966919)',
      'GMR.DefineUnstuck(1177.8477783203, 8006.3540039062, 22.067802429199)',
      'GMR.DefineUnstuck(1168.8706054688, 8014.8325195312, 18.226131439209)',
      'GMR.DefineUnstuck(1163.0413818359, 8023.15234375, 17.800983428955)',
      'GMR.DefineUnstuck(1156.3898925781, 8036.4028320312, 17.697149276733)',
      'GMR.DefineUnstuck(1150.8521728516, 8047.4340820312, 17.681287765503)',
      'GMR.DefineUnstuck(1145.6767578125, 8057.744140625, 17.678693771362)',
      'GMR.DefineUnstuck(1139.4129638672, 8067.279296875, 17.695650100708)',
      'GMR.DefineUnstuck(1134.6395263672, 8078.6674804688, 17.660104751587)',
      'GMR.DefineUnstuck(1130.0177001953, 8089.6938476562, 17.525203704834)',
      'GMR.DefineUnstuck(1123.5288085938, 8099.1298828125, 17.644016265869)',
      'GMR.DefineProfileCenter(1098.4698486328, 8199.783203125, 23.447790145874, 100)',
      'GMR.DefineProfileCenter(1145.9256591797, 8152.580078125, 18.182762145996, 100)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineCustomObjectId(182185)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Wanted: Chieftain Mummaki |r",
    QuestID = 10116,
    QuestType = "Custom",
    PickUp = { x = 1021.708557, y = 7376.442871, z = 36.268482, id = 183284 },
    TurnIn = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10116, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1031.3544921875, 8250.43359375, 57.653549194336,5)',
      'GMR.DefineUnstuck(1029.3560791016, 8254.73046875, 57.653549194336)',
      'GMR.DefineUnstuck(1024.0500488281, 8254.849609375, 57.653549194336)',
      'GMR.DefineUnstuck(1021.5440673828, 8249.9326171875, 57.644718170166)',
      'GMR.DefineUnstuck(1016.9463500977, 8235.4638671875, 17.374956130981)',
      'GMR.DefineUnstuck(1007.2983398438, 8225.41015625, 18.882448196411)',
      'GMR.DefineUnstuck(996.43463134766, 8214.142578125, 22.533863067627)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1038.7733154297, 8252.5537109375, 31.648931503296, 20)',
      'GMR.DefineUnstuck(1037.1790771484, 8258.7275390625, 32.386810302734)',
      'GMR.DefineUnstuck(1033.9133300781, 8265.7705078125, 32.839981079102)',
      'GMR.DefineUnstuck(1030.9704589844, 8268.3740234375, 34.521114349365)',
      'GMR.DefineUnstuck(1029.0718994141, 8269.1669921875, 35.290992736816)',
      'GMR.DefineUnstuck(1027.2203369141, 8267.244140625, 36.351757049561)',
      'GMR.DefineUnstuck(1027.1381835938, 8264.361328125, 37.838108062744)',
      'GMR.DefineUnstuck(1028.8742675781, 8262.3271484375, 38.942314147949)',
      'GMR.DefineUnstuck(1030.8287353516, 8261.705078125, 39.748119354248)',
      'GMR.DefineUnstuck(1032.7056884766, 8262.5654296875, 40.353576660156)',
      'GMR.DefineUnstuck(1033.4876708984, 8263.9814453125, 41.038421630859)',
      'GMR.DefineUnstuck(1033.8946533203, 8265.5712890625, 41.641204833984)',
      'GMR.DefineUnstuck(1033.2525634766, 8267.9443359375, 42.510650634766)',
      'GMR.DefineUnstuck(1030.9938964844, 8269.388671875, 43.313575744629)',
      'GMR.DefineUnstuck(1028.0992431641, 8268.31640625, 44.602939605713)',
      'GMR.DefineUnstuck(1026.4692382812, 8266.2236328125, 45.700462341309)',
      'GMR.DefineUnstuck(1026.5802001953, 8264.1689453125, 46.108085632324)',
      'GMR.DefineUnstuck(1029.1397705078, 8262.443359375, 46.431636810303)',
      'GMR.DefineUnstuck(1031.6586914062, 8262.2314453125, 47.419612884521)',
      'GMR.DefineUnstuck(1033.4458007812, 8263.2529296875, 48.114448547363)',
      'GMR.DefineUnstuck(1034.0867919922, 8265.2080078125, 48.901767730713)',
      'GMR.DefineUnstuck(1033.4108886719, 8266.7158203125, 49.496074676514)',
      'GMR.DefineUnstuck(1031.8428955078, 8269.1123046875, 50.459350585938)',
      'GMR.DefineUnstuck(1028.9560546875, 8268.95703125, 51.51184463501)',
      'GMR.DefineUnstuck(1026.8065185547, 8267.7373046875, 52.419330596924)',
      'GMR.DefineUnstuck(1025.8018798828, 8265.6806640625, 53.109348297119)',
      'GMR.DefineUnstuck(1025.7523193359, 8264.44921875, 53.523555755615)',
      'GMR.DefineUnstuck(1026.9300537109, 8262.7529296875, 54.347480773926)',
      'GMR.DefineUnstuck(1028.96875, 8261.3701171875, 55.291927337646)',
      'GMR.DefineUnstuck(1031.0081787109, 8261.09375, 55.802463531494)',
      'GMR.DefineUnstuck(1032.8695068359, 8262.376953125, 56.773700714111)',
      'GMR.DefineUnstuck(1034.4272460938, 8262.90625, 57.653972625732)',
      'GMR.DefineUnstuck(1035.2027587891, 8263.169921875, 57.653972625732)',
      'GMR.DefineUnstuck(1037.4371337891, 8261.3349609375, 57.653972625732)',
      'GMR.DefineUnstuck(1036.5922851562, 8257.7333984375, 57.653972625732)',
      'GMR.DefineUnstuck(1033.0200195312, 8253.10546875, 57.653972625732)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1060.6970214844, 7521.251953125, 19.003408432007, 80)',
      'GMR.DefineUnstuck(1072.7591552734, 7525.4814453125, 19.68222618103)',
      'GMR.DefineUnstuck(1084.0153808594, 7529.427734375, 20.783929824829)',
      'GMR.DefineUnstuck(1096.4299316406, 7534.7490234375, 20.415958404541)',
      'GMR.DefineUnstuck(1103.4644775391, 7542.0048828125, 20.695924758911)',
      'GMR.DefineUnstuck(1109.0444335938, 7552.9765625, 21.756946563721)',
      'GMR.DefineUnstuck(1112.8959960938, 7564.265625, 20.831653594971)',
      'GMR.DefineUnstuck(1117.2856445312, 7577.1313476562, 19.611120223999)',
      'GMR.DefineUnstuck(1123.1311035156, 7592.1010742188, 22.047121047974)',
      'GMR.DefineUnstuck(1127.9676513672, 7604.35546875, 22.871692657471)',
      'GMR.DefineUnstuck(1132.2395019531, 7618.5180664062, 23.05023765564)',
      'GMR.DefineUnstuck(1137.3485107422, 7631.552734375, 24.504323959351)',
      'GMR.DefineUnstuck(1142.7591552734, 7645.3559570312, 23.592426300049)',
      'GMR.DefineUnstuck(1148.0163574219, 7658.7685546875, 24.267290115356)',
      'GMR.DefineUnstuck(1153.9190673828, 7671.8520507812, 25.239376068115)',
      'GMR.DefineUnstuck(1160.7032470703, 7682.1860351562, 26.077354431152)',
      'GMR.DefineUnstuck(1167.4796142578, 7692.5083007812, 25.974296569824)',
      'GMR.DefineUnstuck(1172.6650390625, 7702.2465820312, 25.173431396484)',
      'GMR.DefineUnstuck(1173.4421386719, 7711.9833984375, 23.982862472534)',
      'GMR.DefineUnstuck(1172.8337402344, 7725.9643554688, 25.093700408936)',
      'GMR.DefineUnstuck(1172.2611083984, 7739.1259765625, 24.440103530884)',
      'GMR.DefineUnstuck(1171.7420654297, 7751.056640625, 24.337102890015)',
      'GMR.DefineUnstuck(1171.2406005859, 7762.58203125, 23.691087722778)',
      'GMR.DefineUnstuck(1170.5969238281, 7777.376953125, 23.338146209717)',
      'GMR.DefineUnstuck(1170.0777587891, 7789.3076171875, 23.051712036133)',
      'GMR.DefineUnstuck(1169.1254882812, 7802.4541015625, 25.430051803589)',
      'GMR.DefineUnstuck(1167.0750732422, 7814.1684570312, 25.858287811279)',
      'GMR.DefineUnstuck(1163.5623779297, 7824.71484375, 26.523790359497)',
      'GMR.DefineUnstuck(1162.2944335938, 7833.1645507812, 26.939424514771)',
      'GMR.DefineUnstuck(1162.6090087891, 7847.1484375, 26.218355178833)',
      'GMR.DefineUnstuck(1162.849609375, 7857.8461914062, 26.210433959961)',
      'GMR.DefineUnstuck(1163.1082763672, 7869.34375, 24.87388420105)',
      'GMR.DefineUnstuck(1163.3491210938, 7880.0512695312, 25.966794967651)',
      'GMR.DefineUnstuck(1164.4202880859, 7891.8930664062, 27.291362762451)',
      'GMR.DefineUnstuck(1167.9323730469, 7901.5126953125, 27.003246307373)',
      'GMR.DefineUnstuck(1172.5739746094, 7912.5078125, 26.18879699707)',
      'GMR.DefineUnstuck(1176.2249755859, 7924.2890625, 25.793476104736)',
      'GMR.DefineUnstuck(1180.2532958984, 7937.287109375, 25.696840286255)',
      'GMR.DefineUnstuck(1183.9044189453, 7949.068359375, 25.709175109863)',
      'GMR.DefineUnstuck(1187.6839599609, 7961.2641601562, 26.317602157593)',
      'GMR.DefineUnstuck(1190.8543701172, 7971.494140625, 25.915618896484)',
      'GMR.DefineUnstuck(1193.4912109375, 7983.0581054688, 24.419673919678)',
      'GMR.DefineUnstuck(1191.4497070312, 7991.6669921875, 24.675842285156)',
      'GMR.DefineUnstuck(1185.3388671875, 7999.2788085938, 24.697587966919)',
      'GMR.DefineUnstuck(1177.8477783203, 8006.3540039062, 22.067802429199)',
      'GMR.DefineUnstuck(1168.8706054688, 8014.8325195312, 18.226131439209)',
      'GMR.DefineUnstuck(1163.0413818359, 8023.15234375, 17.800983428955)',
      'GMR.DefineUnstuck(1156.3898925781, 8036.4028320312, 17.697149276733)',
      'GMR.DefineUnstuck(1150.8521728516, 8047.4340820312, 17.681287765503)',
      'GMR.DefineUnstuck(1145.6767578125, 8057.744140625, 17.678693771362)',
      'GMR.DefineUnstuck(1139.4129638672, 8067.279296875, 17.695650100708)',
      'GMR.DefineUnstuck(1134.6395263672, 8078.6674804688, 17.660104751587)',
      'GMR.DefineUnstuck(1130.0177001953, 8089.6938476562, 17.525203704834)',
      'GMR.DefineUnstuck(1123.5288085938, 8099.1298828125, 17.644016265869)',
      'GMR.DefineProfileCenter(1034.914, 8264.906, 57.65381, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineQuestEnemyId(19174)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Wanted: Chieftain Mummaki / Secrets of the Daggerfen |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10116, x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
      { questId = 9848, x = 974.252991, y = 7403.089844, z = 29.6108, id = 18019 },
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1100.0096435547, 7936.20703125, 17.430311203003, 80)',
      'GMR.DefineUnstuck(1118.6278076172, 7928.9248046875, 19.73773765564)',
      'GMR.DefineUnstuck(1132.6677246094, 7921.1020507812, 23.715284347534)',
      'GMR.DefineUnstuck(1145.1201171875, 7912.4643554688, 24.758678436279)',
      'GMR.DefineUnstuck(1155.4724121094, 7901.5161132812, 24.182641983032)',
      'GMR.DefineUnstuck(1163.0966796875, 7887.8466796875, 27.005002975464)',
      'GMR.DefineUnstuck(1166.1451416016, 7871.4697265625, 25.445196151733)',
      'GMR.DefineUnstuck(1165.9522705078, 7855.8979492188, 26.209230422974)',
      'GMR.DefineUnstuck(1163.8541259766, 7840.4091796875, 26.025928497314)',
      'GMR.DefineUnstuck(1162.3861083984, 7824.8442382812, 26.364179611206)',
      'GMR.DefineUnstuck(1161.6469726562, 7809.2236328125, 25.589548110962)',
      'GMR.DefineUnstuck(1160.9077148438, 7793.6030273438, 24.433166503906)',
      'GMR.DefineUnstuck(1160.1672363281, 7777.9545898438, 25.477212905884)',
      'GMR.DefineUnstuck(1158.8676757812, 7762.3974609375, 25.861474990845)',
      'GMR.DefineUnstuck(1155.8562011719, 7746.2109375, 25.484834671021)',
      'GMR.DefineUnstuck(1151.8706054688, 7731.9638671875, 24.664863586426)',
      'GMR.DefineUnstuck(1146.6408691406, 7716.337890625, 23.674163818359)',
      'GMR.DefineUnstuck(1141.2867431641, 7700.3403320312, 18.866691589355)',
      'GMR.DefineUnstuck(1136.4479980469, 7685.8823242188, 17.912578582764)',
      'GMR.DefineUnstuck(1131.4848632812, 7671.052734375, 17.412677764893)',
      'GMR.DefineUnstuck(1126.5172119141, 7656.2099609375, 17.341722488403)',
      'GMR.DefineUnstuck(1121.6829833984, 7641.765625, 17.71887588501)',
      'GMR.DefineUnstuck(1116.0639648438, 7628.0844726562, 18.030975341797)',
      'GMR.DefineUnstuck(1109.0040283203, 7616.544921875, 18.054300308228)',
      'GMR.DefineUnstuck(1100.3825683594, 7606.6181640625, 18.478265762329)',
      'GMR.DefineUnstuck(1089.0487060547, 7595.84375, 21.357545852661)',
      'GMR.DefineUnstuck(1078.9019775391, 7586.1977539062, 22.138973236084)',
      'GMR.DefineUnstuck(1068.7451171875, 7576.5419921875, 22.310493469238)',
      'GMR.DefineUnstuck(1056.5183105469, 7564.9189453125, 22.013439178467)',
      'GMR.DefineUnstuck(1044.5808105469, 7553.5703125, 22.293025970459)',
      'GMR.DefineUnstuck(1035.5264892578, 7542.6479492188, 22.182224273682)',
      'GMR.DefineUnstuck(1031.4268798828, 7528.9140625, 20.836849212646)',
      'GMR.DefineUnstuck(1027.5852050781, 7514.6508789062, 20.037950515747)',
      'GMR.DefineUnstuck(1021.3061523438, 7499.431640625, 18.005235671997)',
      'GMR.DefineUnstuck(1015.6384277344, 7484.4013671875, 17.524303436279)',
      'GMR.DefineUnstuck(1011.0707397461, 7470.7534179688, 17.997776031494)',
      'GMR.DefineUnstuck(1011.0406494141, 7461.0961914062, 21.41880607605)',
      'GMR.DefineUnstuck(1015.042175293, 7446.8671875, 26.828685760498)',
      'GMR.DefineUnstuck(1012.4064331055, 7432.9487304688, 27.32159614563)',
      'GMR.DefineUnstuck(1007.7153320312, 7418.0014648438, 29.386177062988)',
      'GMR.DefineUnstuck(1001.7055053711, 7404.14453125, 31.040012359619)',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Overlord Gorefist |r",
    QuestID = 9839,
    QuestType = "Custom",
    PickUp = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    TurnIn = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9839, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18160, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9839, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18120, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1003.9430541992, 7376.9165039062, 36.244415283203, 80)',
      'GMR.DefineUnstuck(1002.6672363281, 7398.1596679688, 32.783836364746)',
      'GMR.DefineUnstuck(1004.3934326172, 7414.859375, 29.817132949829)',
      'GMR.DefineUnstuck(1010.6334228516, 7437.8403320312, 27.833965301514)',
      'GMR.DefineUnstuck(1012.3942260742, 7457.8193359375, 23.001859664917)',
      'GMR.DefineUnstuck(1014.6970825195, 7471.12890625, 17.957229614258)',
      'GMR.DefineUnstuck(1023.1956176758, 7482.7373046875, 17.527238845825)',
      'GMR.DefineUnstuck(1047.2114257812, 7500.62109375, 17.272783279419)',
      'GMR.DefineUnstuck(1061.20703125, 7508.6806640625, 17.427316665649)',
      'GMR.DefineUnstuck(1075.0377197266, 7516.6455078125, 17.803030014038)',
      'GMR.DefineUnstuck(1100.0063476562, 7536.9311523438, 20.157150268555)',
      'GMR.DefineUnstuck(1108.1326904297, 7546.7299804688, 20.699377059937)',
      'GMR.DefineUnstuck(1118.1877441406, 7562.5810546875, 19.719858169556)',
      'GMR.DefineUnstuck(1127.0838623047, 7577.5571289062, 19.293991088867)',
      'GMR.DefineUnstuck(1132.8559570312, 7592.9877929688, 22.563270568848)',
      'GMR.DefineUnstuck(1139.4416503906, 7610.59375, 24.801401138306)',
      'GMR.DefineUnstuck(1144.8646240234, 7625.0913085938, 25.53794670105)',
      'GMR.DefineUnstuck(1150.3603515625, 7638.1528320312, 24.073246002197)',
      'GMR.DefineUnstuck(1159.6431884766, 7655.9501953125, 26.152896881104)',
      'GMR.DefineUnstuck(1155.9001464844, 7663.2915039062, 25.280467987061)',
      'GMR.DefineUnstuck(1158.2233886719, 7673.9926757812, 25.134809494019)',
      'GMR.DefineUnstuck(1166.2238769531, 7685.9877929688, 25.669816970825)',
      'GMR.DefineUnstuck(1169.5559082031, 7701.546875, 25.536733627319)',
      'GMR.DefineUnstuck(1168.7548828125, 7716.8901367188, 25.428400039673)',
      'GMR.DefineUnstuck(1166.8544921875, 7731.466796875, 25.803379058838)',
      'GMR.DefineUnstuck(1167.3239746094, 7747.2963867188, 24.82693862915)',
      'GMR.DefineUnstuck(1167.8125, 7763.7646484375, 23.89457321167)',
      'GMR.DefineUnstuck(1168.3099365234, 7780.5346679688, 23.226400375366)',
      'GMR.DefineUnstuck(1167.6098632812, 7796.9560546875, 24.014196395874)',
      'GMR.DefineUnstuck(1165.1300048828, 7813.8857421875, 25.575281143188)',
      'GMR.DefineUnstuck(1163.9022216797, 7830.2275390625, 27.26488494873)',
      'GMR.DefineUnstuck(1169.6442871094, 7848.4462890625, 26.266647338867)',
      'GMR.DefineUnstuck(1178.4453125, 7862.2846679688, 25.350706100464)',
      'GMR.DefineUnstuck(1191.3131103516, 7879.4018554688, 25.329885482788)',
      'GMR.DefineUnstuck(1199.9578857422, 7896.6220703125, 24.496788024902)',
      'GMR.DefineUnstuck(1205.7504882812, 7916.787109375, 24.902544021606)',
      'GMR.DefineUnstuck(1206.8530273438, 7937.3833007812, 26.286600112915)',
      'GMR.DefineUnstuck(1203.1881103516, 7956.3471679688, 24.660709381104)',
      'GMR.DefineUnstuck(1202.59765625, 7972.0639648438, 24.541233062744)',
      'GMR.DefineUnstuck(1202.5703125, 7987.4428710938, 23.413007736206)',
      'GMR.DefineUnstuck(1206.3973388672, 8007.0659179688, 22.672267913818)',
      'GMR.DefineUnstuck(1207.2518310547, 8017.2329101562, 22.537286758423)',
      'GMR.DefineUnstuck(1208.1909179688, 8028.4072265625, 24.14129447937)',
      'GMR.DefineUnstuck(1212.5034179688, 8048.224609375, 22.084362030029)',
      'GMR.DefineUnstuck(1223.2268066406, 8063.52734375, 22.203615188599)',
      'GMR.DefineUnstuck(1240.5725097656, 8073.904296875, 22.208456039429)',
      'GMR.DefineUnstuck(1257.6091308594, 8078.7724609375, 21.832454681396)',
      'GMR.DefineUnstuck(1259.2453613281, 8085.3754882812, 24.250547409058)',
      'GMR.DefineUnstuck(1260.0104980469, 8101.5927734375, 23.683946609497)',
      'GMR.DefineUnstuck(1262.7398681641, 8110.3217773438, 20.923742294312)',
      'GMR.DefineUnstuck(1262.7940673828, 8118.7875976562, 19.544834136963)',
      'GMR.DefineUnstuck(1264.5521240234, 8128.8383789062, 24.055376052856)',
      'GMR.DefineUnstuck(1263.7705078125, 8147.8125, 23.136798858643)',
      'GMR.DefineUnstuck(1263.4309082031, 8163.5239257812, 23.089698791504)',
      'GMR.DefineUnstuck(1263.1528320312, 8177.9526367188, 24.712472915649)',
      'GMR.DefineUnstuck(1262.2186279297, 8192.388671875, 22.487808227539)',
      'GMR.DefineUnstuck(1261.7977294922, 8205.8466796875, 19.598106384277)',
      'GMR.DefineUnstuck(1265.9193115234, 8223.685546875, 22.009120941162)',
      'GMR.DefineUnstuck(1271.0498046875, 8239.3291015625, 21.593410491943)',
      'GMR.DefineUnstuck(1274.423828125, 8257.380859375, 20.354442596436)',
      'GMR.DefineUnstuck(1276.2639160156, 8273.4150390625, 13.874350547791)',
      'GMR.DefineUnstuck(1278.5546875, 8293.3759765625, 13.722117424011)',
      'GMR.DefineUnstuck(1280.7702636719, 8312.68359375, 12.604462623596)',
      'GMR.DefineUnstuck(1283.0610351562, 8332.6455078125, 13.651188850403)',
      'GMR.DefineUnstuck(1274.8283691406, 8351.3310546875, 17.500854492188)',
      'GMR.DefineUnstuck(1273.8187255859, 8370.2626953125, 19.601831436157)',
      'GMR.DefineUnstuck(1279.8695068359, 8388.9921875, 17.122764587402)',
      'GMR.DefineUnstuck(1285.9248046875, 8407.45703125, 15.956038475037)',
      'GMR.DefineUnstuck(1295.2734375, 8426.60546875, 20.162225723267)',
      'GMR.DefineUnstuck(1304.5725097656, 8444.015625, 20.909706115723)',
      'GMR.DefineUnstuck(1311.7893066406, 8461.6943359375, 21.54193687439)',
      'GMR.DefineUnstuck(1318.7661132812, 8480.537109375, 22.197853088379)',
      'GMR.DefineUnstuck(1324.9984130859, 8501.974609375, 24.809595108032)',
      'GMR.DefineUnstuck(1337.8454589844, 8515.7255859375, 24.411264419556)',
      'GMR.DefineUnstuck(1348.8870849609, 8532.373046875, 22.491973876953)',
      'GMR.DefineUnstuck(1361.2270507812, 8548.9306640625, 21.765983581543)',
      'GMR.DefineUnstuck(1373.2091064453, 8566.4638671875, 21.402151107788)',
      'GMR.DefineUnstuck(1379.7611083984, 8584.7880859375, 20.923566818237)',
      'GMR.DefineUnstuck(1380.3165283203, 8603.81640625, 17.628871917725)',
      'GMR.DefineUnstuck(1387.2069091797, 8623.56640625, 11.321084022522)',    
      'GMR.DefineProfileCenter(1674.280029, 8550.400391, 2.690260, 60)',
      'GMR.DefineProfileCenter(1455.337, 8617.583, -9.134803, 60)',
      'GMR.DefineProfileCenter(1602.701, 8602.078, -31.41921, 60)',
      'GMR.DefineProfileCenter(1623.536, 8520.208, -5.784812, 50)',
      'GMR.DefineProfileCenter(1676.945, 8555.545, 2.647837, 50)',
      'GMR.DefineProfileCenter(1692.336, 8620.589, 12.9754, 50)',
      'GMR.DefineProfileCenter(1777.766, 8591.378, -5.674274, 50)',
      'GMR.DefineProfileCenter(1834.192, 8456.184, -19.97444, 60)',
      'GMR.DefineProfileCenter(1764.124, 8466.23, -15.10515, 60)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)',
      'GMR.DefineQuestEnemyId(18120)',
      'GMR.DefineQuestEnemyId(18160)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Stealing Back the Mushrooms |r",
    QuestID = 9709,
    QuestType = "Custom",
    PickUp = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    TurnIn = { x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9709, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(1455.337, 8617.583, -9.134803, 60)',
      'GMR.DefineProfileCenter(1602.701, 8602.078, -31.41921, 60)',
      'GMR.DefineProfileCenter(1623.536, 8520.208, -5.784812, 50)',
      'GMR.DefineProfileCenter(1676.945, 8555.545, 2.647837, 50)',
      'GMR.DefineProfileCenter(1692.336, 8620.589, 12.9754, 50)',
      'GMR.DefineProfileCenter(1777.766, 8591.378, -5.674274, 50)',
      'GMR.DefineProfileCenter(1834.192, 8456.184, -19.97444, 60)',
      'GMR.DefineProfileCenter(1764.124, 8466.23, -15.10515, 60)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)',
      'GMR.DefineQuestEnemyId(18120)',
      'GMR.DefineQuestEnemyId(18121)',
      'GMR.DefineQuestEnemyId(18160)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE All Orebor NEUTRAL Quests |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9839, x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
      { questId = 9709, x = -283.860992, y = 8302.740234, z = 19.710899, id = 17831 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand076 then
        GMR.Print("Frame created")
        GMR.Frames.Rand076 = CreateFrame("frame")
        GMR.Frames.Rand076:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" then
            if GMR.IsExecuting() and GMR.IsQuestActive(9839) and GMR.Questing.IsObjectiveCompleted(9839, 1) then
              GossipTitleButton3:Click()
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand076 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1361.9884033203, 8598.080078125, 20.809959411621, 80)',
      'GMR.DefineUnstuck(1354.1184082031, 8566.2099609375, 22.029258728027)',
      'GMR.DefineUnstuck(1348.0661621094, 8545.755859375, 22.131860733032)',
      'GMR.DefineUnstuck(1340.8005371094, 8525.9580078125, 23.151575088501)',
      'GMR.DefineUnstuck(1335.244140625, 8510.8173828125, 25.19522857666)',
      'GMR.DefineUnstuck(1321.3349609375, 8501.0830078125, 24.043539047241)',
      'GMR.DefineUnstuck(1314.4436035156, 8479.57421875, 22.474924087524)',
      'GMR.DefineUnstuck(1312.6805419922, 8458.6982421875, 21.408739089966)',
      'GMR.DefineUnstuck(1311.1325683594, 8437.439453125, 20.773223876953)',
      'GMR.DefineUnstuck(1307.4981689453, 8417.2412109375, 19.531623840332)',
      'GMR.DefineUnstuck(1292.9223632812, 8408.029296875, 16.078046798706)',
      'GMR.DefineUnstuck(1280.685546875, 8397.337890625, 16.263254165649)',
      'GMR.DefineUnstuck(1274.6267089844, 8375.2021484375, 19.062986373901)',
      'GMR.DefineUnstuck(1274.7294921875, 8353.177734375, 17.099292755127)',
      'GMR.DefineUnstuck(1273.3149414062, 8332.70703125, 16.618909835815)',
      'GMR.DefineUnstuck(1271.3875732422, 8319.666015625, 16.212341308594)',
      'GMR.DefineUnstuck(1270.0628662109, 8295.072265625, 18.760236740112)',
      'GMR.DefineUnstuck(1269.8404541016, 8272.998046875, 16.272729873657)',
      'GMR.DefineUnstuck(1267.9486083984, 8259.009765625, 19.658855438232)',
      'GMR.DefineUnstuck(1269.1572265625, 8235.35546875, 21.537954330444)',
      'GMR.DefineUnstuck(1268.8299560547, 8218.51171875, 23.72046661377)',
      'GMR.DefineUnstuck(1265.9403076172, 8206.80859375, 19.900741577148)',
      'GMR.DefineUnstuck(1264.9838867188, 8183.4428710938, 24.590375900269)',
      'GMR.DefineUnstuck(1261.8211669922, 8154.0893554688, 22.660667419434)',
      'GMR.DefineUnstuck(1259.1728515625, 8137.50390625, 21.284469604492)',
      'GMR.DefineUnstuck(1256.2100830078, 8123.7592773438, 18.505428314209)',
      'GMR.DefineUnstuck(1246.9682617188, 8110.7993164062, 19.208084106445)',
      'GMR.DefineUnstuck(1235.6737060547, 8096.544921875, 19.18098449707)',
      'GMR.DefineUnstuck(1231.2115478516, 8074.7421875, 22.208456039429)',
      'GMR.DefineUnstuck(1221.1025390625, 8058.34375, 22.328193664551)',
      'GMR.DefineUnstuck(1209.2836914062, 8045.951171875, 21.304922103882)',
      'GMR.DefineUnstuck(1206.4267578125, 8021.1279296875, 22.718292236328)',
      'GMR.DefineUnstuck(1208.0744628906, 7999.1254882812, 23.968854904175)',
      'GMR.DefineUnstuck(1209.9437255859, 7974.1635742188, 25.185562133789)',
      'GMR.DefineUnstuck(1212.2806396484, 7942.9580078125, 23.2646484375)',
      'GMR.DefineUnstuck(1215.5036621094, 7899.9204101562, 20.965812683105)',
      'GMR.DefineUnstuck(1213.2537841797, 7874.2456054688, 24.966016769409)',
      'GMR.DefineUnstuck(1205.7377929688, 7837.783203125, 25.820444107056)',
      'GMR.DefineUnstuck(1199.5378417969, 7816.375, 25.046316146851)',
      'GMR.DefineUnstuck(1184.0522460938, 7781.0346679688, 25.758913040161)',
      'GMR.DefineUnstuck(1114.6558837891, 7762.892578125, 24.286575317383)',
      'GMR.DefineUnstuck(1093.5732421875, 7743.5141601562, 23.262958526611)',
      'GMR.DefineUnstuck(1087.1014404297, 7733.6577148438, 22.366342544556)',
      'GMR.DefineUnstuck(1099.9675292969, 7716.1186523438, 22.770015716553)',
      'GMR.DefineUnstuck(1104.1728515625, 7696.6875, 22.408954620361)',
      'GMR.DefineUnstuck(1093.4813232422, 7670.4052734375, 17.403072357178)',
      'GMR.DefineUnstuck(1077.3598632812, 7647.6323242188, 22.132968902588)',
      'GMR.DefineUnstuck(1070.984375, 7636.2368164062, 22.196006774902)',
      'GMR.DefineUnstuck(1065.6950683594, 7615.5786132812, 21.569080352783)',
      'GMR.DefineUnstuck(1058.5345458984, 7596.5190429688, 20.907613754272)',
      'GMR.DefineUnstuck(1048.9542236328, 7576.6928710938, 21.722106933594)',
      'GMR.DefineUnstuck(1045.5499267578, 7556.9897460938, 22.701347351074)',
      'GMR.DefineUnstuck(1038.5407714844, 7537.7290039062, 22.562274932861)',
      'GMR.DefineUnstuck(1029.8444824219, 7518.2353515625, 19.759538650513)',
      'GMR.DefineUnstuck(1024.419921875, 7502.6787109375, 18.126127243042)',
      'GMR.DefineUnstuck(1019.2129516602, 7487.74609375, 17.539152145386)',
      'GMR.DefineUnstuck(1013.1384887695, 7470.3256835938, 18.032995223999)',
      'GMR.DefineUnstuck(1012.3218994141, 7456.4282226562, 23.088548660278)',
      'GMR.DefineUnstuck(1011.8135986328, 7435.7973632812, 27.400110244751)',
      'GMR.DefineUnstuck(1006.8889770508, 7417.748046875, 29.433479309082)',
      'GMR.DefineUnstuck(1002.6071777344, 7402.302734375, 31.242462158203)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(848.16802978516, 7459.1337890625, 22.717321395874, 70)',
      'GMR.DefineUnstuck(838.85205078125, 7478.6440429688, 18.539608001709)',
      'GMR.DefineUnstuck(825.75531005859, 7494.978515625, 17.740966796875)',
      'GMR.DefineUnstuck(812.486328125, 7513.8359375, 18.054239273071)',
      'GMR.DefineUnstuck(799.93432617188, 7531.6748046875, 17.678037643433)',
      'GMR.DefineUnstuck(786.24395751953, 7541.599609375, 17.685291290283)',
      'GMR.DefineUnstuck(767.54516601562, 7547.654296875, 17.473413467407)',
      'GMR.DefineUnstuck(748.14880371094, 7551.4692382812, 17.558517456055)',
      'GMR.DefineUnstuck(729.13708496094, 7555.208984375, 17.738971710205)',
      'GMR.DefineUnstuck(711.37542724609, 7558.7026367188, 18.117177963257)',
      'GMR.DefineUnstuck(696.01593017578, 7563.1518554688, 19.46736907959)',
      'GMR.DefineUnstuck(681.21221923828, 7571.17578125, 21.490064620972)',
      'GMR.DefineUnstuck(667.09582519531, 7579.6484375, 22.053915023804)',
      'GMR.DefineUnstuck(651.61749267578, 7588.2333984375, 22.668983459473)',
      'GMR.DefineUnstuck(638.46478271484, 7597.2822265625, 21.57813835144)',
      'GMR.DefineUnstuck(624.36279296875, 7608.6767578125, 20.269340515137)',
      'GMR.DefineUnstuck(610.28112792969, 7620.0541992188, 21.883493423462)',
      'GMR.DefineUnstuck(596.83245849609, 7630.9208984375, 23.542665481567)',
      'GMR.DefineUnstuck(582.09527587891, 7641.38671875, 21.733171463013)',
      'GMR.DefineUnstuck(567.04974365234, 7647.91015625, 21.029767990112)',
      'GMR.DefineUnstuck(549.88391113281, 7651.8120117188, 18.897857666016)',
      'GMR.DefineUnstuck(531.90594482422, 7654.0444335938, 17.488565444946)',
      'GMR.DefineUnstuck(526.41125488281, 7656.748046875, 17.374614715576)',
      'GMR.DefineUnstuck(516.60736083984, 7660.7885742188, 17.37522315979)',
      'GMR.DefineUnstuck(498.35430908203, 7665.8837890625, 18.732473373413)',
      'GMR.DefineUnstuck(483.56747436523, 7672.0493164062, 18.824466705322)',
      'GMR.DefineUnstuck(466.54452514648, 7679.173828125, 18.34517288208)',
      'GMR.DefineUnstuck(451.4905090332, 7686.8188476562, 18.384023666382)',
      'GMR.DefineUnstuck(438.46347045898, 7698.537109375, 17.821481704712)',
      'GMR.DefineUnstuck(429.26596069336, 7711.1069335938, 17.37460899353)',
      'GMR.DefineUnstuck(420.69982910156, 7725.1669921875, 17.372455596924)',
      'GMR.DefineUnstuck(417.09921264648, 7739.7504882812, 17.803522109985)',
      'GMR.DefineUnstuck(419.60668945312, 7756.7104492188, 18.08740234375)',
      'GMR.DefineUnstuck(423.90686035156, 7772.5668945312, 17.471260070801)',
      'GMR.DefineUnstuck(430.35375976562, 7789.9458007812, 17.391757965088)',
      'GMR.DefineUnstuck(435.93878173828, 7805.0009765625, 18.327527999878)',
      'GMR.DefineUnstuck(444.12646484375, 7791.693359375, 21.401075363159)',
      'GMR.DefineUnstuck(455.52133178711, 7777.0913085938, 20.208972930908)',
      'GMR.DefineUnstuck(468.14254760742, 7768.703125, 21.191833496094)',
      'GMR.DefineUnstuck(482.93099975586, 7770.5864257812, 21.076049804688)',
      'GMR.DefineUnstuck(497.72189331055, 7783.19921875, 19.661670684814)',
      'GMR.DefineUnstuck(509.71301269531, 7793.7543945312, 17.510665893555)',
      'GMR.DefineUnstuck(519.83935546875, 7808.0913085938, 17.546119689941)',
      'GMR.DefineUnstuck(526.79595947266, 7821.6577148438, 17.374519348145)',
      'GMR.DefineUnstuck(521.81872558594, 7828.451171875, 17.814435958862)',
      'GMR.DefineUnstuck(512.78619384766, 7836.8706054688, 20.952760696411)',
      'GMR.DefineUnstuck(505.12295532227, 7844.3256835938, 21.687412261963)',
      'GMR.DefineUnstuck(500.93222045898, 7856.3671875, 22.420639038086)',
      'GMR.DefineUnstuck(503.00952148438, 7868.029296875, 22.867401123047)',
      'GMR.DefineUnstuck(507.27191162109, 7882.2294921875, 22.382518768311)',
      'GMR.DefineUnstuck(511.77178955078, 7897.220703125, 21.713787078857)',
      'GMR.DefineUnstuck(509.63195800781, 7913.0776367188, 21.270725250244)',
      'GMR.DefineUnstuck(500.51062011719, 7925.1831054688, 20.642923355103)',
      'GMR.DefineUnstuck(494.12072753906, 7942.4360351562, 17.374887466431)',
      'GMR.DefineUnstuck(487.34606933594, 7953.7348632812, 17.374887466431)',
      'GMR.DefineUnstuck(477.33706665039, 7960.0971679688, 17.374887466431)',
      'GMR.DefineUnstuck(451.88717651367, 7968.3139648438, 17.375225067139)',
      'GMR.DefineUnstuck(434.93817138672, 7971.7309570312, 20.364973068237)',
      'GMR.DefineUnstuck(417.99267578125, 7975.1469726562, 22.305994033813)',
      'GMR.DefineUnstuck(402.44174194336, 7980.1123046875, 23.297040939331)',
      'GMR.DefineUnstuck(389.09732055664, 7987.2534179688, 24.039642333984)',
      'GMR.DefineUnstuck(373.54824829102, 7998.095703125, 23.703632354736)',
      'GMR.DefineUnstuck(358.70803833008, 8008.4438476562, 24.050071716309)',
      'GMR.DefineUnstuck(344.80136108398, 8016.4038085938, 20.154279708862)',
      'GMR.DefineUnstuck(329.61651611328, 8021.173828125, 17.374620437622)',
      'GMR.DefineUnstuck(314.83685302734, 8028.0454101562, 17.374620437622)',
      'GMR.DefineUnstuck(298.87689208984, 8037.4721679688, 17.374620437622)',
      'GMR.DefineUnstuck(283.64016723633, 8046.4716796875, 18.644937515259)',
      'GMR.DefineUnstuck(269.10263061523, 8055.05859375, 20.216478347778)',
      'GMR.DefineUnstuck(254.2601776123, 8062.1025390625, 17.533332824707)',
      'GMR.DefineUnstuck(236.76019287109, 8069.3881835938, 17.379867553711)',
      'GMR.DefineUnstuck(220.80000305176, 8076.0322265625, 18.57092666626)',
      'GMR.DefineUnstuck(204.08656311035, 8082.9907226562, 22.288782119751)',
      'GMR.DefineUnstuck(185.42028808594, 8092.3671875, 22.71696472168)',
      'GMR.DefineUnstuck(170.60948181152, 8096.3432617188, 24.199243545532)',
      'GMR.DefineUnstuck(151.83790588379, 8100.3461914062, 23.283159255981)',
      'GMR.DefineUnstuck(137.69721984863, 8108.65625, 23.088491439819)',
      'GMR.DefineUnstuck(123.11051940918, 8118.6748046875, 17.649179458618)',
      'GMR.DefineUnstuck(108.18511199951, 8128.9262695312, 17.818063735962)',
      'GMR.DefineUnstuck(93.892845153809, 8139.9404296875, 18.404718399048)',
      'GMR.DefineUnstuck(87.331184387207, 8152.6420898438, 17.710186004639)',
      'GMR.DefineUnstuck(76.383773803711, 8159.677734375, 17.192720413208)',
      'GMR.DefineUnstuck(58.845481872559, 8164.0126953125, 21.360088348389)',
      'GMR.DefineUnstuck(42.289165496826, 8166.943359375, 23.244560241699)',
      'GMR.DefineUnstuck(24.305723190308, 8169.0473632812, 23.415437698364)',
      'GMR.DefineUnstuck(6.7157020568848, 8171.1049804688, 24.095703125)',
      'GMR.DefineUnstuck(-10.053913116455, 8173.0673828125, 22.336395263672)',
      'GMR.DefineUnstuck(-26.227857589722, 8177.3515625, 22.275077819824)',
      'GMR.DefineUnstuck(-45.227100372314, 8185.2612304688, 22.511779785156)',
      'GMR.DefineUnstuck(-60.814235687256, 8191.7504882812, 22.32592010498)',
      'GMR.DefineUnstuck(-76.260528564453, 8201.0732421875, 22.121648788452)',
      'GMR.DefineUnstuck(-92.492317199707, 8208.98046875, 22.492721557617)',
      'GMR.DefineUnstuck(-108.90061187744, 8214.095703125, 23.65754699707)',
      'GMR.DefineUnstuck(-124.0461807251, 8218.853515625, 24.461624145508)',
      'GMR.DefineUnstuck(-139.3161315918, 8227.82421875, 22.761432647705)',
      'GMR.DefineUnstuck(-155.28338623047, 8237.2041015625, 23.316263198853)',
      'GMR.DefineUnstuck(-166.7763671875, 8247.9599609375, 22.116996765137)',
      'GMR.DefineUnstuck(-176.5989074707, 8263.6630859375, 22.567029953003)',
      'GMR.DefineUnstuck(-185.12217712402, 8277.2890625, 22.011703491211)',
      'GMR.DefineUnstuck(-194.29136657715, 8291.947265625, 21.387369155884)',
      'GMR.DefineUnstuck(-204.94674682617, 8303.6552734375, 21.629020690918)',
      'GMR.DefineUnstuck(-219.72785949707, 8311.1103515625, 22.314653396606)',
      'GMR.DefineUnstuck(-234.71823120117, 8317.4541015625, 22.095781326294)',
      'GMR.DefineUnstuck(-246.17977905273, 8316.9287109375, 21.569046020508)',
      'GMR.DefineUnstuck(-257.84768676758, 8323.8720703125, 21.538389205933)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1008.8607177734, 7468.8129882812, 18.103704452515,20)',
      'GMR.DefineUnstuck(992.42291259766, 7479.5874023438, 17.404176712036)',
      'GMR.DefineUnstuck(976.41461181641, 7482.4213867188, 17.381511688232)',
      'GMR.DefineUnstuck(958.85040283203, 7477.3002929688, 17.597362518311)',
      'GMR.DefineUnstuck(943.24475097656, 7471.109375, 18.270345687866)',
      'GMR.DefineUnstuck(926.40014648438, 7464.4272460938, 21.962623596191)',
      'GMR.DefineUnstuck(907.42004394531, 7456.8979492188, 22.86083984375)',
      'GMR.DefineUnstuck(891.10540771484, 7452.4150390625, 22.730478286743)',
      'GMR.DefineUnstuck(875.65734863281, 7452.8471679688, 20.963634490967)',
      'GMR.DefineUnstuck(864.35974121094, 7454.9873046875, 22.056520462036)',
      'GMR.DefineUnstuck(849.03509521484, 7458.8471679688, 22.989366531372)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(936.31433105469, 7377.189453125, 21.850429534912, 25)',
      'GMR.DefineUnstuck(924.82995605469, 7381.3627929688, 20.095798492432)',
      'GMR.DefineUnstuck(910.38482666016, 7393.4125976562, 20.136741638184)',
      'GMR.DefineUnstuck(898.87017822266, 7406.103515625, 20.434532165527)',
      'GMR.DefineUnstuck(890.88885498047, 7417.3349609375, 20.464599609375)',
      'GMR.DefineUnstuck(881.46923828125, 7431.5659179688, 20.642747879028)',
      'GMR.DefineUnstuck(868.73889160156, 7444.248046875, 22.433034896851)',
      'GMR.DefineUnstuck(857.79772949219, 7452.1684570312, 22.625425338745)',
      'GMR.DefineUnstuck(848.73455810547, 7458.7294921875, 22.909940719604)',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Terror of Marshlight Lake |r",
    QuestID = 9902,
    QuestType = "Custom",
    PickUp = { x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
    TurnIn = { x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9902, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(931.64294433594, 7378.427734375, 19.848636627197, 20)',
      'GMR.DefineUnstuck(927.623046875, 7391.9584960938, 19.789836883545)',
      'GMR.DefineUnstuck(927.15850830078, 7410.07421875, 18.1328125)',
      'GMR.DefineUnstuck(934.68853759766, 7436.6430664062, 17.971942901611)',
      'GMR.DefineUnstuck(941.22259521484, 7449.0356445312, 18.011793136597)',
      'GMR.DefineUnstuck(951.02270507812, 7458.2534179688, 17.658437728882)',
      'GMR.DefineUnstuck(962.76092529297, 7466.734375, 17.397434234619)',
      'GMR.DefineUnstuck(982.42926025391, 7475.236328125, 17.342231750488)',
      'GMR.DefineUnstuck(992.49047851562, 7472.6328125, 17.554174423218)',
      'GMR.DefineUnstuck(1015.3972167969, 7473.4233398438, 17.798942565918)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1016.05078125, 7473.404296875, 17.799570083618, 30)',
      'GMR.DefineUnstuck(1021.5453491211, 7487.0249023438, 17.53546333313)',
      'GMR.DefineUnstuck(1029.4077148438, 7500.0302734375, 17.632564544678)',
      'GMR.DefineUnstuck(1039.9233398438, 7515.2739257812, 17.734052658081)',
      'GMR.DefineUnstuck(1045.1665039062, 7528.9555664062, 20.820096969604)',
      'GMR.DefineUnstuck(1049.3253173828, 7544.900390625, 23.246482849121)',
      'GMR.DefineUnstuck(1053.4840087891, 7560.8447265625, 22.407167434692)',
      'GMR.DefineUnstuck(1053.4854736328, 7573.8862304688, 22.073196411133)',
      'GMR.DefineUnstuck(1053.5010986328, 7589.87890625, 21.125173568726)',
      'GMR.DefineUnstuck(1055.4162597656, 7606.6401367188, 20.872617721558)',
      'GMR.DefineUnstuck(1057.3791503906, 7623.818359375, 22.490240097046)',
      'GMR.DefineUnstuck(1059.5804443359, 7643.0830078125, 21.71608543396)',
      'GMR.DefineUnstuck(1063.0283203125, 7659.431640625, 21.892705917358)',
      'GMR.DefineUnstuck(1070.84375, 7673.8408203125, 18.272304534912)',
      'GMR.DefineUnstuck(1079.6625976562, 7686.7548828125, 19.057710647583)',
      'GMR.DefineUnstuck(1091.0190429688, 7696.2211914062, 24.273368835449)',
      'GMR.DefineUnstuck(1082.7342529297, 7701.8198242188, 22.517747879028)',
      'GMR.DefineUnstuck(1092.8179931641, 7710.2319335938, 22.356595993042)',
      'GMR.DefineUnstuck(1100.8461914062, 7723.2016601562, 23.514640808105)',
      'GMR.DefineUnstuck(1105.2709960938, 7740.2543945312, 22.523927688599)',
      'GMR.DefineUnstuck(1107.3471679688, 7759.0795898438, 23.096622467041)',
      'GMR.DefineUnstuck(1105.6791992188, 7773.03515625, 23.581172943115)',
      'GMR.DefineUnstuck(1098.3813476562, 7789.0698242188, 17.625072479248)',
      'GMR.DefineUnstuck(1085.8684082031, 7805.4086914062, 18.756818771362)',
      'GMR.DefineUnstuck(1074.4140625, 7816.45703125, 21.526990890503)',
      'GMR.DefineUnstuck(1062.7939453125, 7828.0971679688, 19.870655059814)',
      'GMR.DefineUnstuck(1059.5941162109, 7844.853515625, 20.19705581665)',
      'GMR.DefineUnstuck(1059.1195068359, 7862.5463867188, 20.972280502319)',
      'GMR.DefineUnstuck(1056.5673828125, 7882.3896484375, 19.472858428955)',
      'GMR.DefineUnstuck(1052.7680664062, 7894.5649414062, 18.308534622192)',
      'GMR.DefineUnstuck(1042.2133789062, 7904.5239257812, 17.862197875977)',
      'GMR.DefineUnstuck(1026.3864746094, 7914.0053710938, 17.676958084106)',
      'GMR.DefineUnstuck(1013.5872802734, 7924.3671875, 17.501466751099)',
      'GMR.DefineUnstuck(998.67657470703, 7937.3032226562, 19.623008728027)',
      'GMR.DefineUnstuck(987.16046142578, 7947.2939453125, 21.547296524048)',
      'GMR.DefineUnstuck(975.74450683594, 7959.046875, 23.396718978882)',
      'GMR.DefineUnstuck(965.97979736328, 7970.6372070312, 22.643602371216)',
      'GMR.DefineUnstuck(956.95947265625, 7984.4604492188, 23.763118743896)',
      'GMR.DefineUnstuck(948.39056396484, 7997.591796875, 20.604789733887)',
      'GMR.DefineUnstuck(932.74694824219, 8003.9096679688, 17.857946395874)',
      'GMR.DefineUnstuck(934.49224853516, 8021.0107421875, 17.885131835938)',
      'GMR.DefineUnstuck(931.99212646484, 8035.3969726562, 22.414726257324)',
      'GMR.DefineUnstuck(919.98120117188, 8049.759765625, 22.750150680542)',
      'GMR.DefineUnstuck(908.56512451172, 8063.603515625, 24.107797622681)',
      'GMR.DefineUnstuck(893.81683349609, 8072.9384765625, 22.962099075317)',
      'GMR.DefineUnstuck(879.0537109375, 8080.2265625, 17.391044616699)',
      'GMR.DefineUnstuck(865.06396484375, 8080.2416992188, 17.416952133179)',
      'GMR.DefineUnstuck(856.82299804688, 8071.0102539062, 18.31263923645)',
      'GMR.DefineUnstuck(846.05426025391, 8058.5004882812, 18.014696121216)',
      'GMR.DefineUnstuck(829.88781738281, 8049.4609375, 17.426031112671)',
      'GMR.DefineUnstuck(813.92175292969, 8044.8344726562, 18.395559310913)',
      'GMR.DefineUnstuck(801.94287109375, 8071.7431640625, 19.432977676392)',
      'GMR.DefineUnstuck(808.15472412109, 8083.3510742188, 22.201700210571)',
      'GMR.DefineUnstuck(804.45660400391, 8097.365234375, 22.582096099854)',
      'GMR.DefineUnstuck(793.15173339844, 8112.0546875, 23.465984344482)',
      'GMR.DefineUnstuck(790.1923828125, 8120.1494140625, 23.172832489014)',
      'GMR.DefineUnstuck(790.41705322266, 8133.1630859375, 20.543340682983)',
      'GMR.DefineUnstuck(797.58758544922, 8151.6015625, 17.528282165527)',
      'GMR.DefineUnstuck(796.76062011719, 8167.642578125, 20.40202331543)',
      'GMR.DefineUnstuck(792.37493896484, 8185.65234375, 17.333618164062)',
      'GMR.DefineUnstuck(789.57940673828, 8200.4658203125, 16.932096481323)',
      'GMR.DefineUnstuck(785.65301513672, 8220.02734375, 16.932096481323)',
      'GMR.DefineUnstuck(783.26049804688, 8231.4775390625, 16.932096481323)',
      'GMR.DefineUnstuck(781.92864990234, 8246.6640625, 16.932096481323)',
      'GMR.DefineUnstuck(779.99670410156, 8259.6845703125, 16.932096481323)',
      'GMR.DefineUnstuck(773.45776367188, 8271.7041015625, 16.932096481323)',
      'GMR.DefineUnstuck(764.01824951172, 8282.18359375, 16.932096481323)',
      'GMR.DefineUnstuck(752.71228027344, 8291.4150390625, 16.932096481323)',
      'GMR.DefineUnstuck(741.64404296875, 8300.453125, 16.932096481323)',
      'GMR.DefineUnstuck(735.1845703125, 8305.7275390625, 16.932096481323)',
      'GMR.DefineUnstuck(727.55090332031, 8311.9609375, 16.932096481323)',
      'GMR.DefineUnstuck(719.69781494141, 8318.373046875, 16.932096481323)',
      'GMR.DefineUnstuck(710.32360839844, 8325.0341796875, 16.932096481323)',
      'GMR.DefineUnstuck(699.71343994141, 8328.8134765625, 16.932096481323)',
      'GMR.DefineUnstuck(689.88659667969, 8331.251953125, 16.932096481323)',
      'GMR.DefineUnstuck(681.06665039062, 8333.693359375, 16.932096481323)',
      'GMR.DefineUnstuck(669.72406005859, 8335.822265625, 16.932096481323)',
      'GMR.DefineUnstuck(657.92242431641, 8333.5439453125, 16.932096481323)',
      'GMR.DefineUnstuck(647.90228271484, 8331.3232421875, 16.932096481323)',
      'GMR.DefineUnstuck(638.06286621094, 8329.146484375, 16.932096481323)',
      'GMR.DefineUnstuck(628.67065429688, 8327.0693359375, 16.932096481323)',
      'GMR.DefineUnstuck(613.01702880859, 8323.607421875, 16.932096481323)',
      'GMR.DefineUnstuck(598.73278808594, 8320.4482421875, 16.932096481323)',
      'GMR.DefineUnstuck(583.48950195312, 8317.0771484375, 16.932096481323)',
      'GMR.DefineUnstuck(571.39129638672, 8315.421875, 16.932096481323)',
      'GMR.DefineUnstuck(559.98333740234, 8313.705078125, 16.932096481323)',
      'GMR.DefineUnstuck(547.04705810547, 8313.1240234375, 16.932096481323)',
      'GMR.DefineUnstuck(533.45092773438, 8316.39453125, 16.932096481323)',
      'GMR.DefineUnstuck(521.92840576172, 8315.515625, 16.932096481323)',
      'GMR.DefineUnstuck(510.87911987305, 8314.7412109375, 16.932096481323)',
      'GMR.DefineUnstuck(498.24887084961, 8313.30859375, 16.932096481323)',
      'GMR.DefineUnstuck(486.04125976562, 8310.578125, 16.932096481323)',
      'GMR.DefineUnstuck(472.97036743164, 8311.37109375, 16.932096481323)',
      'GMR.DefineUnstuck(459.23434448242, 8321.0556640625, 16.932096481323)',
      'GMR.DefineUnstuck(447.42422485352, 8329.380859375, 16.932096481323)',
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(400.720001, 8353.379883, 22.668200, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)',
      'GMR.DefineQuestEnemyId(20477)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE The Terror of Marshlight Lake |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9902, x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(456.36709594727, 8256.916015625, 15.951809883118, 60)',
      'GMR.DefineUnstuck(464.60015869141, 8267.705078125, 15.951809883118)',
      'GMR.DefineUnstuck(472.69537353516, 8278.109375, 15.951809883118)',
      'GMR.DefineUnstuck(481.03167724609, 8288.5126953125, 15.951809883118)',
      'GMR.DefineUnstuck(488.14443969727, 8297.92578125, 15.951809883118)',
      'GMR.DefineUnstuck(494.07553100586, 8309.908203125, 15.951809883118)',
      'GMR.DefineUnstuck(494.12042236328, 8310.0380859375, 15.951809883118)',
      'GMR.DefineUnstuck(498.63891601562, 8316.6044921875, 17.061517715454)',
      'GMR.DefineUnstuck(509.25143432617, 8313.181640625, 17.061515808105)',
      'GMR.DefineUnstuck(520.52001953125, 8310.2138671875, 17.061668395996)',
      'GMR.DefineUnstuck(529.84838867188, 8306.8076171875, 17.061544418335)',
      'GMR.DefineUnstuck(539.03863525391, 8301.03515625, 17.061538696289)',
      'GMR.DefineUnstuck(550.38928222656, 8294.5546875, 17.061658859253)',
      'GMR.DefineUnstuck(560.29284667969, 8288.8193359375, 17.061666488647)',
      'GMR.DefineUnstuck(571.3681640625, 8285.9375, 17.061416625977)',
      'GMR.DefineUnstuck(582.76904296875, 8283.9287109375, 17.061668395996)',
      'GMR.DefineUnstuck(592.80059814453, 8282.1591796875, 17.061422348022)',
      'GMR.DefineUnstuck(607.15954589844, 8284.3115234375, 17.061666488647)',
      'GMR.DefineUnstuck(621.73889160156, 8286.919921875, 17.061666488647)',
      'GMR.DefineUnstuck(636.64788818359, 8287.3720703125, 17.059669494629)',
      'GMR.DefineUnstuck(649.85858154297, 8287.84375, 17.060668945312)',
      'GMR.DefineUnstuck(662.37622070312, 8290.392578125, 17.061664581299)',
      'GMR.DefineUnstuck(676.04211425781, 8293.65234375, 17.06166267395)',
      'GMR.DefineUnstuck(687.26831054688, 8292.748046875, 17.061666488647)',
      'GMR.DefineUnstuck(697.27569580078, 8287.142578125, 17.061666488647)',
      'GMR.DefineUnstuck(708.50244140625, 8278.46875, 17.06166267395)',
      'GMR.DefineUnstuck(717.75677490234, 8268.1083984375, 17.061666488647)',
      'GMR.DefineUnstuck(726.13232421875, 8256.2451171875, 17.061668395996)',
      'GMR.DefineUnstuck(735.05816650391, 8246.1171875, 17.06166267395)',
      'GMR.DefineUnstuck(745.13073730469, 8234.69140625, 17.06166267395)',
      'GMR.DefineUnstuck(757.16217041016, 8228.201171875, 17.061563491821)',
      'GMR.DefineUnstuck(767.99493408203, 8220.716796875, 17.061668395996)',
      'GMR.DefineUnstuck(778.24615478516, 8210.8046875, 17.061660766602)',
      'GMR.DefineUnstuck(790.04418945312, 8196.951171875, 17.061525344849)',
      'GMR.DefineUnstuck(797.53509521484, 8182.4252929688, 19.698608398438)',
      'GMR.DefineUnstuck(808.15087890625, 8180.60546875, 21.071237564087)',
      'GMR.DefineUnstuck(816.78186035156, 8161.8359375, 20.692939758301)',
      'GMR.DefineUnstuck(828.77197265625, 8149.6948242188, 21.490404129028)',
      'GMR.DefineUnstuck(844.48828125, 8138.482421875, 18.972055435181)',
      'GMR.DefineUnstuck(857.19769287109, 8126.8198242188, 20.655563354492)',
      'GMR.DefineUnstuck(868.95367431641, 8113.068359375, 18.244331359863)',
      'GMR.DefineUnstuck(882.2197265625, 8098.4057617188, 18.552223205566)',
      'GMR.DefineUnstuck(894.81121826172, 8082.703125, 21.901037216187)',
      'GMR.DefineUnstuck(906.60650634766, 8065.3286132812, 24.172761917114)',
      'GMR.DefineUnstuck(918.02966308594, 8050.2290039062, 22.969856262207)',
      'GMR.DefineUnstuck(929.70556640625, 8035.3315429688, 21.990926742554)',
      'GMR.DefineUnstuck(940.88116455078, 8018.6357421875, 18.145460128784)',
      'GMR.DefineUnstuck(956.69622802734, 8008.572265625, 17.44811630249)',
      'GMR.DefineUnstuck(974.51739501953, 7999.2934570312, 17.661451339722)',
      'GMR.DefineUnstuck(991.82061767578, 7986.6674804688, 16.93214225769)',
      'GMR.DefineUnstuck(996.85736083984, 7975.5581054688, 18.125604629517)',
      'GMR.DefineUnstuck(987.29815673828, 7959.8505859375, 19.703769683838)',
      'GMR.DefineUnstuck(984.99285888672, 7949.6435546875, 21.899267196655)',
      'GMR.DefineUnstuck(987.69995117188, 7939.4829101562, 21.655130386353)',
      'GMR.DefineUnstuck(996.33331298828, 7925.5512695312, 20.522531509399)',
      'GMR.DefineUnstuck(999.95190429688, 7909.5883789062, 17.943225860596)',
      'GMR.DefineUnstuck(1013.3481445312, 7899.1225585938, 18.49619102478)',
      'GMR.DefineUnstuck(1031.5936279297, 7896.4438476562, 19.136650085449)',
      'GMR.DefineUnstuck(1045.1026611328, 7887.8017578125, 21.190883636475)',
      'GMR.DefineUnstuck(1053.1030273438, 7874.2182617188, 21.283706665039)',
      'GMR.DefineUnstuck(1058.8093261719, 7854.0083007812, 20.806772232056)',
      [[
          if not GMR.Frames.Rand032634 then
        GMR.Print("Frame created")
        GMR.Frames.Rand032634 = CreateFrame("frame")
        GMR.Frames.Rand032634:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" then
            if GMR.IsExecuting() and not GMR.IsQuestCompleted(9902) then
              if GossipFrameGreetingPanel:IsShown() then
                GossipTitleButton3:Click()
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand032634 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Stinger Venom |r",
    QuestID = 9830,
    QuestType = "Custom",
    PickUp = { x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
    TurnIn = { x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9830, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(1079.1357421875, 7590.865234375, 22.151782989502, 100)',
      'GMR.DefineProfileCenter(1104.3991699219, 7872.9008789062, 22.612428665161, 100)',
      'GMR.DefineProfileCenter(1022.0399169922, 8080.8647460938, 22.016725540161, 100)',
      'GMR.DefineProfileCenter(864.40417480469, 8120.083984375, 19.16961479187, 100)',
      'GMR.DefineProfileCenter(900.73065185547, 8202.5458984375, 21.227628707886, 100)',
      'GMR.DefineProfileCenter(900.85784912109, 8305.30859375, 20.147672653198, 100)',
      'GMR.DefineProfileCenter(878.89721679688, 8404.9560546875, 23.436069488525, 100)',
      'GMR.DefineProfileCenter(806.66143798828, 8499.318359375, 23.155363082886, 100)',
      'GMR.DefineProfileCenter(903.44818115234, 8614.9228515625, 23.315433502197, 100)',
      'GMR.DefineProfileCenter(815.16491699219, 8583.150390625, 17.374843597412, 100)',
      'GMR.DefineProfileCenter(653.95697021484, 8684.2998046875, 23.285511016846, 100)',
      'GMR.DefineProfileCenter(547.57800292969, 8692.4169921875, 21.608369827271, 100)',
      'GMR.DefineProfileCenter(428.38739013672, 8685.421875, 23.114067077637, 100)',
      'GMR.DefineProfileCenter(509.99069213867, 8712.82421875, 20.825407028198, 100)',
      'GMR.DefineProfileCenter(694.26837158203, 8651.6982421875, 22.211349487305, 100)',
      'GMR.DefineProfileCenter(799.5693359375, 8584.67578125, 17.414920806885, 100)',
      'GMR.DefineProfileCenter(877.20153808594, 8409.8232421875, 23.301959991455, 100)',
      'GMR.DefineAreaBlacklist(886.42474365234, 8052.0634765625, 22.365432739258, 45)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)',
      'GMR.DefineQuestEnemyId(18133)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Lines of Communication |r",
    QuestID = 9833,
    QuestType = "Custom",
    PickUp = { x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
    TurnIn = { x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9833, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(689.78717041016, 8380.2099609375, 16.673601150513, 100)',
      'GMR.DefineUnstuck(666.24041748047, 8382.0595703125, 16.673601150513)',
      'GMR.DefineUnstuck(644.3955078125, 8375.640625, 16.673601150513)',
      'GMR.DefineUnstuck(627.17395019531, 8366.3076171875, 16.673601150513)',
      'GMR.DefineUnstuck(615.35101318359, 8361.1923828125, 16.673601150513)',
      'GMR.DefineUnstuck(604.42498779297, 8357.1103515625, 16.673601150513)',
      'GMR.DefineUnstuck(587.64392089844, 8352.4716796875, 16.673601150513)',
      'GMR.DefineUnstuck(570.60943603516, 8348.2294921875, 16.673601150513)',
      'GMR.DefineUnstuck(551.32720947266, 8340.4169921875, 16.673601150513)',
      'GMR.DefineUnstuck(539.12121582031, 8328.1240234375, 16.673601150513)',
      'GMR.DefineUnstuck(516.70379638672, 8322.8935546875, 16.673601150513)',
      'GMR.DefineUnstuck(495.9241027832, 8321.384765625, 16.673601150513)',
      'GMR.DefineUnstuck(480.27340698242, 8320.248046875, 16.673601150513)',
      'GMR.DefineUnstuck(465.26443481445, 8317.8837890625, 16.673601150513)',
      'GMR.DefineUnstuck(446.0432434082, 8311.2314453125, 16.673601150513)',
      'GMR.DefineUnstuck(424.56991577148, 8298.328125, 16.673601150513)',
      'GMR.DefineUnstuck(409.68759155273, 8284.619140625, 16.673601150513)',
      'GMR.DefineUnstuck(391.91098022461, 8271.7607421875, 16.673601150513)',
      'GMR.DefineUnstuck(368.82162475586, 8268.5361328125, 16.673601150513)',
      'GMR.DefineUnstuck(351.94387817383, 8271.6748046875, 16.673601150513)',
      'GMR.DefineUnstuck(324.21212768555, 8273.3251953125, 16.673601150513)',
      'GMR.DefineUnstuck(308.96273803711, 8274.2333984375, 16.673601150513)',
      'GMR.DefineUnstuck(291.88220214844, 8275.3203125, 16.673601150513)',
      'GMR.DefineUnstuck(274.33483886719, 8264.6552734375, 16.673601150513)',
      'GMR.DefineUnstuck(261.98791503906, 8255.6357421875, 16.673601150513)',
      'GMR.DefineUnstuck(249.06471252441, 8247.244140625, 16.673601150513)',
      'GMR.DefineUnstuck(236.9796295166, 8239.583984375, 16.673601150513)',
      'GMR.DefineUnstuck(225.96723937988, 8234.6455078125, 19.759748458862)',    
      'GMR.DefineProfileCenter(-51.03501, 8445.608, 17.3796, 120)',
      'GMR.DefineProfileCenter(-413.0525, 8523.473, 21.96765, 120)',
      'GMR.DefineProfileCenter(-268.8546, 8467.185, 18.71252, 120)',
      'GMR.DefineProfileCenter(22.13921, 8322.996, 23.67723, 120)',
      'GMR.DefineProfileCenter(76.4875, 8198.185, 19.7697, 120)',
      'GMR.DefineProfileCenter(15.87519, 7925.042, 22.01186, 44)',
      'GMR.DefineProfileCenter(-29.35393, 7859.28, 22.3481, 120)',
      'GMR.DefineProfileCenter(-84.69428, 7770.395, 18.82963, 120)',
      'GMR.DefineProfileCenter(-145.1233, 7607.079, 22.76108, 120)',
      'GMR.DefineProfileCenter(63.99788, 7822.994, 19.2502, 120)',
      'GMR.DefineProfileCenter(-30.54944, 7837.27, 22.89113, 120)',
      'GMR.DefineProfileCenter(81.32755, 8103.419, 23.13074, 120)',
      'GMR.DefineProfileCenter(-8.629436, 8383.05, 20.91106, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineRepairVendor(1020.8350219727, 7383.4516601562, 36.241580963135, 18010)',
      'GMR.DefineGoodsVendor(1056.3791503906, 7370.9897460938, 39.58411026001, 18908)',
      'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500,21172)',
      'GMR.DefineProfileMailbox(1029.1604003906, 7361.6962890625, 36.417465209961)',
      'GMR.DefineQuestEnemyId(18131)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE All Orebor NEUTRAL Quests |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9830, x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 },
      { questId = 9833, x = 974.862976, y = 7421.649902, z = 31.9895, id = 18009 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand078 then
        GMR.Print("Frame created")
        GMR.Frames.Rand078 = CreateFrame("frame")
        GMR.Frames.Rand078:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" then
            if GMR.IsQuestActive(9830) and GMR.IsQuestCompletable(9830) and GossipFrameGreetingPanel:IsShown() then
              GossipTitleButton1:Click()
            elseif GMR.IsQuestActive(9833) and GMR.IsQuestCompletable(9833) and GossipFrameGreetingPanel:IsShown() then
              GossipTitleButton1:Click()
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand078 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Grinding: Zangermarsh - Basilisks North East - Level 65 |r",
    QuestType = "GrindTo",
    Level = 65,
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
    'GMR.DefineProfileCenter(944.2978, 5763.103, -2.307649, 50)',
    'GMR.DefineProfileCenter(950.4807, 5700.603, 3.086858, 50)',
    'GMR.DefineProfileCenter(926.4785, 5639.189, 9.370758, 50)',
    'GMR.DefineProfileCenter(944.3994, 5425.919, -2.012255, 50)',
    'GMR.DefineProfileCenter(956.6505, 5380.755, -4.420358, 50)',
    'GMR.DefineProfileCenter(925.7418, 5355.655, 5.356632, 50)',
    'GMR.DefineProfileCenter(951.0486, 5315.957, 0.8030996, 50)',
    'GMR.DefineProfileCenter(997.4387, 5314.704, -22.08152, 50)',
    'GMR.DefineProfileCenter(968.5944, 5262.867, -8.313138, 50)',
    'GMR.DefineProfileCenter(977.7519, 5213.058, -3.703802, 50)',
    'GMR.DefineProfileCenter(994.2451, 5194.44, -12.73074, 50)',
    'GMR.DefineProfileCenter(978.1465, 5157.187, 0.1549854, 50)',
    'GMR.DefineProfileCenter(950.2549, 5142.234, 0.9077697, 50)',
    'GMR.DefineProfileCenter(912.4099, 5107.036, 4.413258, 50)',
    'GMR.DefineProfileCenter(954.7795, 5066.373, -12.97722, 80)',
    'GMR.DefineProfileCenter(874.9865, 5059.625, -3.494663, 80)',
    'GMR.DefineProfileCenter(827.4617, 5027.637, -9.203645, 50)',
    'GMR.DefineProfileCenter(788.3972, 5035.414, -4.245362, 50)',
    'GMR.DefineProfileCenter(793.1356, 5081.69, 8.719297, 50)',
    'GMR.DefineProfileCenter(755.8484, 5059.354, 9.599715, 50)',
    'GMR.DefineProfileCenter(717.4992, 5053.115, 8.292552, 50)',
    'GMR.DefineProfileCenter(684.2441, 5054.82, 11.39761, 50)',
    'GMR.DefineProfileCenter(812.5356, 5063.405, 7.526284, 50)',
    'GMR.DefineProfileCenter(891.1047, 5073.956, -2.060335, 50)',
    'GMR.DefineProfileCenter(928.2288, 5093.234, -0.8988408, 50)',
    'GMR.DefineProfileCenter(986.4312, 5102.128, -8.625931, 50)',
    'GMR.DefineProfileCenter(956.0099, 5202.039, 3.253895, 50)',
    'GMR.DefineProfileCenter(961.1015, 5283.047, -4.90785, 50)',
    'GMR.DefineProfileCenter(940.9755, 5401.549, 0.1530382, 50)',
    'GMR.DefineProfileCenter(898.7562, 5563.975, 13.46296, 50)',
    'GMR.DefineProfileCenter(918.5533, 5631.975, 12.31872, 50)',
    'GMR.DefineProfileCenter(919.3856, 5700.295, 13.42035, 50)',
    'GMR.DefineProfileCenter(936.1949, 5734.59, 10.43738, 50)',
    'GMR.DefineProfileCenter(969.4677, 5709.43, -6.776454, 50)',
    'GMR.DefineAreaBlacklist(964.5533, 5579.34, -9.69371, 20)',
    'GMR.DefineQuestEnemyId(19706)',
    'GMR.DefineQuestEnemyId(18132)',
    'GMR.BlacklistId(18138)',
    'GMR.BlacklistId(18124)',
    'GMR.BlacklistId(19402)',
    'GMR.BlacklistId(20324)',
    'GMR.DefineSellVendor(-198.970001, 5490.689941, 21.929399, 17904)',
    'GMR.DefineRepairVendor(-198.970001, 5490.689941, 21.929399, 17904)',
    'GMR.DefineAmmoVendor(-202.912994, 5477.600098, 22.791500, 21172)',
    'GMR.DefineGoodsVendor(-174.477997, 5529.209961, 29.407499, 18907)',
    'GMR.DefineProfileMailbox(-199.9225, 5507.861, 22.09435, 183039)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A'dal |r",
    QuestID = 10210,
    QuestType = "TalkTo",
    PickUp = { x = -1637.0427246094, y = 5353.7841796875, z = 15.979079246521, id = 19684 },
    TurnIn = { x = -1861.068481, y = 5429.248047, z = -9.705236, id = 18481 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(995.62139892578, 7385.7309570312, 35.174076080322, 80)',
      'GMR.DefineUnstuck(976.07818603516, 7382.9873046875, 30.126070022583)',
      'GMR.DefineUnstuck(957.18878173828, 7379.1904296875, 27.901525497437)',
      'GMR.DefineUnstuck(940.37860107422, 7375.3110351562, 23.161891937256)',
      'GMR.DefineUnstuck(926.50335693359, 7373.3442382812, 20.053003311157)',
      'GMR.DefineUnstuck(915.94329833984, 7363.9819335938, 18.660467147827)',
      'GMR.DefineUnstuck(908.24371337891, 7347.171875, 18.222274780273)',
      'GMR.DefineUnstuck(908.33746337891, 7328.2299804688, 18.679460525513)',
      'GMR.DefineUnstuck(907.76300048828, 7310.9458007812, 18.479164123535)',
      'GMR.DefineUnstuck(906.71929931641, 7294.5004882812, 18.194747924805)',
      'GMR.DefineUnstuck(907.33221435547, 7277.7158203125, 18.025447845459)',
      'GMR.DefineUnstuck(909.91223144531, 7260.208984375, 17.654226303101)',
      'GMR.DefineUnstuck(912.31268310547, 7243.9208984375, 18.405401229858)',
      'GMR.DefineUnstuck(916.28210449219, 7225.8432617188, 20.217578887939)',
      'GMR.DefineUnstuck(914.81402587891, 7211.5, 21.180019378662)',
      'GMR.DefineUnstuck(906.25061035156, 7196.5693359375, 21.948238372803)',
      'GMR.DefineUnstuck(896.78692626953, 7183.0971679688, 21.642278671265)',
      'GMR.DefineUnstuck(889.65936279297, 7169.3911132812, 20.737325668335)',
      'GMR.DefineUnstuck(890.23370361328, 7159.0224609375, 20.913431167603)',
      'GMR.DefineUnstuck(897.43078613281, 7146.7104492188, 20.229469299316)',
      'GMR.DefineUnstuck(905.01495361328, 7133.662109375, 21.322147369385)',
      'GMR.DefineUnstuck(908.50524902344, 7116.0493164062, 19.334453582764)',
      'GMR.DefineUnstuck(910.88659667969, 7101.0805664062, 19.653871536255)',
      'GMR.DefineUnstuck(915.52087402344, 7085.2822265625, 21.775917053223)',
      'GMR.DefineUnstuck(920.50982666016, 7068.2749023438, 21.768308639526)',
      'GMR.DefineUnstuck(925.26232910156, 7052.0737304688, 20.803827285767)',
      'GMR.DefineUnstuck(926.61798095703, 7036.6083984375, 19.595769882202)',
      'GMR.DefineUnstuck(926.72619628906, 7021.7963867188, 19.644577026367)',
      'GMR.DefineUnstuck(925.78326416016, 7004.1953125, 19.113290786743)',
      'GMR.DefineUnstuck(922.38928222656, 6987.6557617188, 18.309329986572)',
      'GMR.DefineUnstuck(918.98052978516, 6973.6391601562, 18.039577484131)',
      'GMR.DefineUnstuck(913.29931640625, 6958.7211914062, 18.452796936035)',
      'GMR.DefineUnstuck(905.91156005859, 6944.4633789062, 18.171049118042)',
      'GMR.DefineUnstuck(897.76379394531, 6928.7387695312, 17.678398132324)',
      'GMR.DefineUnstuck(886.67883300781, 6915.7192382812, 17.464656829834)',
      'GMR.DefineUnstuck(873.21325683594, 6904.8740234375, 17.50414276123)',
      'GMR.DefineUnstuck(860.84533691406, 6891.939453125, 17.601280212402)',
      'GMR.DefineUnstuck(854.47039794922, 6877.2963867188, 19.313547134399)',
      'GMR.DefineUnstuck(850.99914550781, 6862.4750976562, 21.009515762329)',
      'GMR.DefineUnstuck(846.22216796875, 6846.806640625, 18.867719650269)',
      'GMR.DefineUnstuck(842.98840332031, 6834.142578125, 17.521272659302)',
      'GMR.DefineUnstuck(841.31927490234, 6817.3876953125, 17.757820129395)',
      'GMR.DefineUnstuck(839.89178466797, 6801.8505859375, 17.512701034546)',
      'GMR.DefineUnstuck(836.5869140625, 6785.7465820312, 17.517478942871)',
      'GMR.DefineUnstuck(831.73504638672, 6770.0239257812, 17.536500930786)',
      'GMR.DefineUnstuck(825.85650634766, 6755.5517578125, 17.468196868896)',
      'GMR.DefineUnstuck(818.12408447266, 6740.5424804688, 17.439750671387)',
      'GMR.DefineUnstuck(810.583984375, 6725.9067382812, 17.521043777466)',
      'GMR.DefineUnstuck(806.08557128906, 6711.0434570312, 17.522369384766)',
      'GMR.DefineUnstuck(803.05224609375, 6694.0302734375, 17.474159240723)',
      'GMR.DefineUnstuck(799.48620605469, 6678.8349609375, 17.598543167114)',
      'GMR.DefineUnstuck(797.21691894531, 6663.8349609375, 17.61693572998)',
      'GMR.DefineUnstuck(796.17407226562, 6647.404296875, 17.735685348511)',
      'GMR.DefineUnstuck(795.15698242188, 6631.3784179688, 19.231962203979)',
      'GMR.DefineUnstuck(797.54522705078, 6614.8891601562, 20.894144058228)',
      'GMR.DefineUnstuck(802.83416748047, 6596.7006835938, 20.972723007202)',
      'GMR.DefineUnstuck(805.41784667969, 6577.2822265625, 21.475664138794)',
      'GMR.DefineUnstuck(805.52380371094, 6555.8764648438, 20.708255767822)',
      'GMR.DefineUnstuck(800.45428466797, 6540.43359375, 19.87614440918)',
      'GMR.DefineUnstuck(798.65777587891, 6524.458984375, 17.388696670532)',
      'GMR.DefineUnstuck(799.99597167969, 6506.7998046875, 17.384037017822)',
      'GMR.DefineUnstuck(803.60986328125, 6489.97265625, 17.399301528931)',
      'GMR.DefineUnstuck(807.21240234375, 6472.6748046875, 17.511213302612)',
      'GMR.DefineUnstuck(807.38842773438, 6458.7412109375, 17.43843460083)',
      'GMR.DefineUnstuck(809.82275390625, 6442.603515625, 17.431135177612)',
      'GMR.DefineUnstuck(811.43572998047, 6428.4760742188, 17.4251537323)',
      'GMR.DefineUnstuck(811.47979736328, 6411.4799804688, 17.528945922852)',
      'GMR.DefineUnstuck(812.60888671875, 6394.6733398438, 17.458507537842)',
      'GMR.DefineUnstuck(813.42077636719, 6377.841796875, 17.373273849487)',
      'GMR.DefineUnstuck(811.6806640625, 6360.728515625, 17.543188095093)',
      'GMR.DefineUnstuck(808.53942871094, 6345.4208984375, 17.630443572998)',
      'GMR.DefineUnstuck(804.00897216797, 6332.2885742188, 17.411800384521)',
      'GMR.DefineUnstuck(795.10754394531, 6315.1098632812, 17.390180587769)',
      'GMR.DefineUnstuck(786.3994140625, 6298.3041992188, 17.329261779785)',
      'GMR.DefineUnstuck(779.32641601562, 6285.2763671875, 19.751340866089)',
      'GMR.DefineUnstuck(774.43536376953, 6274.2548828125, 23.837726593018)',
      'GMR.DefineUnstuck(769.14862060547, 6259.6401367188, 26.02032661438)',
      'GMR.DefineUnstuck(760.06610107422, 6243.6625976562, 24.140007019043)',
      'GMR.DefineUnstuck(746.83538818359, 6230.2592773438, 24.876626968384)',
      'GMR.DefineUnstuck(735.47528076172, 6218.4516601562, 23.994110107422)',
      'GMR.DefineUnstuck(721.78186035156, 6204.328125, 24.309564590454)',
      'GMR.DefineUnstuck(707.16918945312, 6195.2241210938, 20.237451553345)',
      'GMR.DefineUnstuck(689.88403320312, 6186.53125, 17.375679016113)',
      'GMR.DefineUnstuck(678.97790527344, 6174.7553710938, 17.376413345337)',
      'GMR.DefineUnstuck(674.3916015625, 6156.9047851562, 17.633424758911)',
      'GMR.DefineUnstuck(670.74993896484, 6139.1586914062, 20.322351455688)',
      'GMR.DefineUnstuck(665.17156982422, 6123.2915039062, 23.403003692627)',
      'GMR.DefineUnstuck(657.36846923828, 6108.3188476562, 21.053323745728)',
      'GMR.DefineUnstuck(648.15338134766, 6093.810546875, 17.747159957886)',
      'GMR.DefineUnstuck(633.76110839844, 6085.3286132812, 20.762271881104)',
      'GMR.DefineUnstuck(619.28283691406, 6079.7265625, 21.139795303345)',
      'GMR.DefineUnstuck(602.53564453125, 6079.5336914062, 21.5774974823)',
      'GMR.DefineUnstuck(584.07739257812, 6081.0693359375, 21.95817565918)',
      'GMR.DefineUnstuck(567.64410400391, 6081.796875, 22.314382553101)',
      'GMR.DefineUnstuck(551.57989501953, 6082.0708007812, 22.797389984131)',
      'GMR.DefineUnstuck(532.6513671875, 6081.7958984375, 22.047327041626)',
      'GMR.DefineUnstuck(514.56140136719, 6080.82421875, 21.988693237305)',
      'GMR.DefineUnstuck(496.89953613281, 6080.5048828125, 22.08455657959)',
      'GMR.DefineUnstuck(478.50271606445, 6082.677734375, 22.146181106567)',
      'GMR.DefineUnstuck(463.32763671875, 6086.359375, 22.95569229126)',
      'GMR.DefineUnstuck(447.09805297852, 6086.6245117188, 21.979024887085)',
      'GMR.DefineUnstuck(432.65078735352, 6083.7182617188, 18.81862449646)',
      'GMR.DefineUnstuck(366.36239624023, 6055.7875976562, 17.374500274658)',
      'GMR.DefineUnstuck(355.12396240234, 6044.3095703125, 17.374500274658)',
      'GMR.DefineUnstuck(342.27581787109, 6030.982421875, 17.622301101685)',
      'GMR.DefineUnstuck(330.30142211914, 6018.5615234375, 20.498516082764)',
      'GMR.DefineUnstuck(318.28186035156, 6006.09375, 21.840364456177)',
      'GMR.DefineUnstuck(305.72598266602, 5995.57421875, 23.111724853516)',
      'GMR.DefineUnstuck(289.36175537109, 5988.4370117188, 22.398555755615)',
      'GMR.DefineUnstuck(274.99661254883, 5984.76953125, 21.116598129272)',
      'GMR.DefineUnstuck(273.20309448242, 5986.7250976562, 21.095035552979)',
      'GMR.DefineUnstuck(266.61236572266, 5994.1064453125, 20.988985061646)',
      'GMR.DefineUnstuck(254.64054870605, 6010.28125, 21.159065246582)',
      'GMR.DefineUnstuck(240.41716003418, 6020.3618164062, 21.68009185791)',
      'GMR.DefineUnstuck(223.2884979248, 6029.3896484375, 21.82262802124)',
      'GMR.DefineUnstuck(206.56645202637, 6040.3759765625, 22.054149627686)',
      'GMR.DefineUnstuck(196.05934143066, 6053.3979492188, 22.030513763428)',
      'GMR.DefineUnstuck(189.94749450684, 6069.0971679688, 21.934797286987)',
      'GMR.DefineUnstuck(185.87651062012, 6084.173828125, 21.823635101318)',
      'GMR.DefineUnstuck(178.26295471191, 6094.28125, 21.480541229248)',
      'GMR.DefineUnstuck(166.68370056152, 6101.31640625, 21.383207321167)',
      'GMR.DefineUnstuck(150.98466491699, 6109.54296875, 20.935068130493)',
      'GMR.DefineUnstuck(137.01344299316, 6117.439453125, 21.019094467163)',
      'GMR.DefineUnstuck(124.0428237915, 6125.4521484375, 20.954126358032)',
      'GMR.DefineUnstuck(110.16457366943, 6132.5512695312, 20.922946929932)',
      'GMR.DefineUnstuck(95.800643920898, 6137.4326171875, 21.047986984253)',
      'GMR.DefineUnstuck(86.943893432617, 6135.4150390625, 21.448408126831)',
      'GMR.DefineUnstuck(78.817436218262, 6125.5854492188, 21.967964172363)',
      'GMR.DefineUnstuck(72.762390136719, 6123.1630859375, 21.967964172363)',
      'GMR.DefineUnstuck(66.100227355957, 6126.4926757812, 21.967964172363)',
      'GMR.DefineUnstuck(51.90161895752, 6136.935546875, 21.407566070557)',
      'GMR.DefineUnstuck(39.646587371826, 6142.5444335938, 21.645265579224)',
      'GMR.DefineUnstuck(26.401945114136, 6144.9467773438, 21.729520797729)',
      'GMR.DefineUnstuck(12.841068267822, 6145.6669921875, 22.053646087646)',
      'GMR.DefineUnstuck(-3.6049032211304, 6146.541015625, 22.59302520752)',
      'GMR.DefineUnstuck(-18.829431533813, 6147.3500976562, 22.82675743103)',
      'GMR.DefineUnstuck(-33.201168060303, 6148.11328125, 23.055849075317)',
      'GMR.DefineUnstuck(-49.191345214844, 6147.220703125, 22.619522094727)',
      'GMR.DefineUnstuck(-68.891326904297, 6145.8803710938, 22.489423751831)',
      'GMR.DefineUnstuck(-92.868797302246, 6139.22265625, 22.546043395996)',
      'GMR.DefineUnstuck(-115.08135223389, 6129.3940429688, 21.010766983032)',
      'GMR.DefineUnstuck(-137.68951416016, 6120.5131835938, 21.199699401855)',
      'GMR.DefineUnstuck(-161.28457641602, 6111.9467773438, 21.645360946655)',
      'GMR.DefineUnstuck(-177.8687286377, 6104.779296875, 21.761890411377)',
      'GMR.DefineUnstuck(-194.30497741699, 6092.5419921875, 21.264308929443)',
      'GMR.DefineUnstuck(-209.66323852539, 6081.4311523438, 21.418371200562)',
      'GMR.DefineUnstuck(-225.66011047363, 6069.8579101562, 22.304996490479)',
      'GMR.DefineUnstuck(-243.25003051758, 6059.408203125, 22.63116645813)',
      'GMR.DefineUnstuck(-262.44116210938, 6050.9165039062, 22.433723449707)',
      'GMR.DefineUnstuck(-285.46176147461, 6051.9228515625, 21.895948410034)',
      'GMR.DefineUnstuck(-304.72915649414, 6053.3002929688, 21.416759490967)',
      'GMR.DefineUnstuck(-321.51968383789, 6051.9375, 21.500925064087)',
      'GMR.DefineUnstuck(-341.16464233398, 6050.3505859375, 21.780963897705)',
      'GMR.DefineUnstuck(-363.41064453125, 6050.3608398438, 21.941640853882)',
      'GMR.DefineUnstuck(-386.84588623047, 6049.513671875, 21.959177017212)',
      'GMR.DefineUnstuck(-405.81091308594, 6046.3149414062, 21.67883682251)',
      'GMR.DefineUnstuck(-423.03958129883, 6034.8569335938, 22.600645065308)',
      'GMR.DefineUnstuck(-439.76861572266, 6023.0444335938, 22.075555801392)',
      'GMR.DefineUnstuck(-459.84631347656, 6012.5258789062, 22.498037338257)',
      'GMR.DefineUnstuck(-480.95663452148, 6011.181640625, 21.261949539185)',
      'GMR.DefineUnstuck(-499.18295288086, 6009.3833007812, 24.294982910156)',
      'GMR.DefineUnstuck(-518.8876953125, 5998.3823242188, 23.054056167603)',
      'GMR.DefineUnstuck(-533.85693359375, 5991.8452148438, 21.888929367065)',
      'GMR.DefineUnstuck(-549.66198730469, 5989.947265625, 22.590921401978)',
      'GMR.DefineUnstuck(-569.416015625, 5989.9560546875, 23.276697158813)',
      'GMR.DefineUnstuck(-589.59002685547, 5989.9653320312, 23.636266708374)',
      'GMR.DefineUnstuck(-610.17852783203, 5989.974609375, 23.099523544312)',
      'GMR.DefineUnstuck(-632.30377197266, 5990.1630859375, 22.326410293579)',
      'GMR.DefineUnstuck(-648.08129882812, 5996.9282226562, 21.771730422974)',
      'GMR.DefineUnstuck(-664.98571777344, 6006.9985351562, 17.564878463745)',
      'GMR.DefineUnstuck(-680.2412109375, 6018.1552734375, 17.374263763428)',
      'GMR.DefineUnstuck(-694.884765625, 6024.4233398438, 20.406543731689)',
      'GMR.DefineUnstuck(-717.07867431641, 6028.2026367188, 23.369071960449)',
      'GMR.DefineUnstuck(-740.90618896484, 6029.8442382812, 23.241622924805)',
      'GMR.DefineUnstuck(-763.75244140625, 6032.4868164062, 23.550132751465)',
      'GMR.DefineUnstuck(-779.95159912109, 6035.3725585938, 19.71314239502)',
      'GMR.DefineUnstuck(-797.15991210938, 6036.9536132812, 18.092256546021)',
      'GMR.DefineUnstuck(-817.21130371094, 6035.9501953125, 22.394622802734)',
      'GMR.DefineUnstuck(-838.00317382812, 6033.0009765625, 22.557363510132)',
      'GMR.DefineUnstuck(-858.36474609375, 6034.21875, 21.498128890991)',
      'GMR.DefineUnstuck(-876.42340087891, 6033.8754882812, 20.739288330078)',
      'GMR.DefineUnstuck(-893.73950195312, 6030.6162109375, 20.239686965942)',
      'GMR.DefineUnstuck(-910.58001708984, 6027.15625, 19.570676803589)',
      'GMR.DefineUnstuck(-910.08282470703, 6018.0703125, 19.194816589355)',
      'GMR.DefineUnstuck(-910.63562011719, 5996.6689453125, 19.322988510132)',
      'GMR.DefineUnstuck(-911.57940673828, 5976.52734375, 19.824869155884)',
      'GMR.DefineUnstuck(-911.60803222656, 5957.5849609375, 20.247066497803)',
      'GMR.DefineUnstuck(-911.63787841797, 5937.8168945312, 20.314893722534)',
      'GMR.DefineUnstuck(-911.66271972656, 5921.3530273438, 20.51296043396)',
      'GMR.DefineUnstuck(-911.69256591797, 5901.6108398438, 20.151103973389)',
      'GMR.DefineUnstuck(-911.95526123047, 5881.0107421875, 20.473098754883)',
      'GMR.DefineUnstuck(-913.91741943359, 5863.8530273438, 21.165071487427)',
      'GMR.DefineUnstuck(-915.91369628906, 5845.0649414062, 21.286069869995)',
      'GMR.DefineUnstuck(-915.48406982422, 5825.345703125, 20.971694946289)',
      'GMR.DefineUnstuck(-915.79919433594, 5803.6044921875, 21.808965682983)',
      'GMR.DefineUnstuck(-914.97583007812, 5784.7368164062, 22.036748886108)',
      'GMR.DefineUnstuck(-913.59588623047, 5766.6733398438, 22.053758621216)',
      'GMR.DefineUnstuck(-914.17803955078, 5751.4565429688, 22.154066085815)',
      'GMR.DefineUnstuck(-914.47845458984, 5730.8891601562, 22.651031494141)',
      'GMR.DefineUnstuck(-914.3251953125, 5711.5278320312, 22.349454879761)',
      'GMR.DefineUnstuck(-916.03070068359, 5694.4272460938, 22.146169662476)',
      'GMR.DefineUnstuck(-919.33880615234, 5678.255859375, 21.877849578857)',
      'GMR.DefineUnstuck(-923.84741210938, 5666.255859375, 22.74555015564)',
      'GMR.DefineUnstuck(-933.87634277344, 5650.236328125, 22.5986328125)',
      'GMR.DefineUnstuck(-944.23498535156, 5635.4799804688, 22.332149505615)',
      'GMR.DefineUnstuck(-948.90539550781, 5623.033203125, 21.761039733887)',
      'GMR.DefineUnstuck(-951.68286132812, 5603.0991210938, 21.551404953003)',
      'GMR.DefineUnstuck(-952.79400634766, 5586.6586914062, 21.452117919922)',
      'GMR.DefineUnstuck(-953.35180664062, 5567.3315429688, 21.651945114136)',
      'GMR.DefineUnstuck(-953.38354492188, 5546.3315429688, 22.323652267456)',
      'GMR.DefineUnstuck(-953.41278076172, 5526.9833984375, 22.236452102661)',
      'GMR.DefineUnstuck(-953.43951416016, 5509.2734375, 22.182363510132)',
      'GMR.DefineUnstuck(-954.92529296875, 5491.6645507812, 22.106992721558)',
      'GMR.DefineUnstuck(-957.24877929688, 5474.955078125, 21.970628738403)',
      'GMR.DefineUnstuck(-956.74926757812, 5455.7797851562, 21.256074905396)',
      'GMR.DefineUnstuck(-951.11688232422, 5440.0849609375, 21.800092697144)',
      'GMR.DefineUnstuck(-940.04437255859, 5427.240234375, 22.471662521362)',
      'GMR.DefineUnstuck(-930.38372802734, 5419.5512695312, 23.010572433472)',
      'GMR.DefineUnstuck(-931.69201660156, 5408.9213867188, 23.610330581665)',
      'GMR.DefineUnstuck(-948.85815429688, 5396.3701171875, 22.414079666138)',
      'GMR.DefineUnstuck(-965.57336425781, 5385.2553710938, 21.650758743286)',
      'GMR.DefineUnstuck(-978.44787597656, 5381.0361328125, 21.577220916748)',
      'GMR.DefineUnstuck(-991.23822021484, 5383.2426757812, 22.014015197754)',
      'GMR.DefineUnstuck(-1007.0693969727, 5387.8139648438, 22.339803695679)',
      'GMR.DefineUnstuck(-1019.1177368164, 5387.4526367188, 22.460868835449)',
      'GMR.DefineUnstuck(-1037.3482666016, 5382.3100585938, 22.047538757324)',
      'GMR.DefineUnstuck(-1052.5563964844, 5379.4033203125, 22.383560180664)',
      'GMR.DefineUnstuck(-1069.8464355469, 5377.2124023438, 23.040899276733)',
      'GMR.DefineUnstuck(-1085.7572021484, 5368.6557617188, 23.529348373413)',
      'GMR.DefineUnstuck(-1101.4727783203, 5358.8530273438, 23.93869972229)',
      'GMR.DefineUnstuck(-1118.5026855469, 5350.7768554688, 24.922330856323)',
      'GMR.DefineUnstuck(-1138.3950195312, 5348.2021484375, 26.383703231812)',
      'GMR.DefineUnstuck(-1158.3969726562, 5344.5791015625, 28.243322372437)',
      'GMR.DefineUnstuck(-1173.7866210938, 5333.2412109375, 29.871667861938)',
      'GMR.DefineUnstuck(-1190.1416015625, 5317.6127929688, 32.407817840576)',
      'GMR.DefineUnstuck(-1203.2305908203, 5305.10546875, 34.535682678223)',
      'GMR.DefineUnstuck(-1216.9177246094, 5292.0263671875, 36.487575531006)',
      'GMR.DefineUnstuck(-1232.1979980469, 5279.5571289062, 38.610946655273)',
      'GMR.DefineUnstuck(-1248.9150390625, 5267.5297851562, 39.910060882568)',
      'GMR.DefineUnstuck(-1262.9569091797, 5255.603515625, 42.025611877441)',
      'GMR.DefineUnstuck(-1276.83984375, 5240.392578125, 44.540363311768)',
      'GMR.DefineUnstuck(-1289.6447753906, 5227.5922851562, 46.273963928223)',
      'GMR.DefineUnstuck(-1306.3527832031, 5211.7016601562, 48.724903106689)',
      'GMR.DefineUnstuck(-1321.8787841797, 5198.9033203125, 51.389797210693)',
      'GMR.DefineUnstuck(-1338.2540283203, 5188.65625, 54.486148834229)',
      'GMR.DefineUnstuck(-1354.8560791016, 5181.5473632812, 57.816787719727)',
      'GMR.DefineUnstuck(-1374.3001708984, 5176.57421875, 60.544261932373)',
      'GMR.DefineUnstuck(-1394.7894287109, 5175.0498046875, 61.981803894043)',
      'GMR.DefineUnstuck(-1414.9633789062, 5175.064453125, 62.292278289795)',
      'GMR.DefineUnstuck(-1435.3618164062, 5176.8354492188, 61.109359741211)',
      'GMR.DefineUnstuck(-1452.9713134766, 5182.3090820312, 56.927871704102)',
      'GMR.DefineUnstuck(-1471.5119628906, 5192.0952148438, 48.764869689941)',
      'GMR.DefineUnstuck(-1486.9582519531, 5204.2421875, 39.925930023193)',
      'GMR.DefineUnstuck(-1498.5120849609, 5218.0747070312, 32.629104614258)',
      'GMR.DefineUnstuck(-1508.3460693359, 5235.1176757812, 25.788000106812)',
      'GMR.DefineUnstuck(-1514.8780517578, 5254.15625, 18.738821029663)',
      'GMR.DefineUnstuck(-1520.3405761719, 5275.70703125, 12.987067222595)',
      'GMR.DefineUnstuck(-1523.4302978516, 5293.9404296875, 10.311326026917)',
      'GMR.DefineUnstuck(-1528.9637451172, 5313.24609375, 8.7905607223511)',
      'GMR.DefineUnstuck(-1543.1544189453, 5323.1704101562, 6.9584856033325)',
      'GMR.DefineUnstuck(-1560.2718505859, 5328.9887695312, 11.709146499634)',
      'GMR.DefineUnstuck(-1577.2867431641, 5333.9018554688, 15.845409393311)',
      'GMR.DefineUnstuck(-1594.576171875, 5340.4033203125, 17.205135345459)',
      'GMR.DefineUnstuck(-1612.5264892578, 5346.3447265625, 17.01683807373)',
      'GMR.DefineUnstuck(-1625.9812011719, 5350.2641601562, 16.59789276123)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-416.95178222656, 5482.0942382812, 21.337900161743, 60)',
      'GMR.DefineUnstuck(-430.10443115234, 5475.2216796875, 21.223207473755)',
      'GMR.DefineUnstuck(-445.95498657227, 5465.6547851562, 21.680736541748)',
      'GMR.DefineUnstuck(-461.71917724609, 5455.9423828125, 22.675765991211)',
      'GMR.DefineUnstuck(-475.54373168945, 5448.755859375, 22.539398193359)',
      'GMR.DefineUnstuck(-492.23254394531, 5446.8046875, 22.265491485596)',
      'GMR.DefineUnstuck(-511.12924194336, 5445.4956054688, 21.642070770264)',
      'GMR.DefineUnstuck(-527.36041259766, 5443.2236328125, 20.944425582886)',
      'GMR.DefineUnstuck(-546.17785644531, 5434.3588867188, 21.156587600708)',
      'GMR.DefineUnstuck(-561.92169189453, 5424.607421875, 21.284009933472)',
      'GMR.DefineUnstuck(-576.12579345703, 5419.4047851562, 21.236791610718)',
      'GMR.DefineUnstuck(-594.34820556641, 5414.2333984375, 21.424983978271)',
      'GMR.DefineUnstuck(-610.57775878906, 5409.6279296875, 21.673406600952)',
      'GMR.DefineUnstuck(-626.39453125, 5403.8334960938, 22.771352767944)',
      'GMR.DefineUnstuck(-641.07318115234, 5397.3569335938, 22.903734207153)',
      'GMR.DefineUnstuck(-655.47265625, 5387.8642578125, 22.132955551147)',
      'GMR.DefineUnstuck(-672.70819091797, 5382.416015625, 22.185956954956)',
      'GMR.DefineUnstuck(-691.52972412109, 5381.4350585938, 22.189556121826)',
      'GMR.DefineUnstuck(-708.79431152344, 5382.6015625, 22.327648162842)',
      'GMR.DefineUnstuck(-725.22088623047, 5383.7119140625, 22.510093688965)',
      'GMR.DefineUnstuck(-741.66137695312, 5384.8232421875, 22.654886245728)',
      'GMR.DefineUnstuck(-752.71362304688, 5385.9506835938, 22.790578842163)',
      'GMR.DefineUnstuck(-761.15936279297, 5387.6708984375, 22.960832595825)',
      'GMR.DefineUnstuck(-769.53265380859, 5390.0102539062, 22.981981277466)',
      'GMR.DefineUnstuck(-783.36676025391, 5393.8759765625, 23.035861968994)',
      'GMR.DefineUnstuck(-797.25476074219, 5397.7568359375, 23.020328521729)',
      'GMR.DefineUnstuck(-812.72033691406, 5402.078125, 23.136545181274)',
      'GMR.DefineUnstuck(-828.59045410156, 5406.5126953125, 23.30899810791)',
      'GMR.DefineUnstuck(-847.61572265625, 5411.8286132812, 23.462368011475)',
      'GMR.DefineUnstuck(-865.85888671875, 5416.9262695312, 23.790676116943)',
      'GMR.DefineUnstuck(-884.01129150391, 5420.083984375, 24.061454772949)',
      'GMR.DefineUnstuck(-901.28125, 5421.1694335938, 24.225717544556)',
      'GMR.DefineUnstuck(-919.98413085938, 5419.7368164062, 23.280658721924)',
      'GMR.DefineUnstuck(-931.69201660156, 5408.9213867188, 23.610330581665)',
      'GMR.DefineUnstuck(-948.85815429688, 5396.3701171875, 22.414079666138)',
      'GMR.DefineUnstuck(-965.57336425781, 5385.2553710938, 21.650758743286)',
      'GMR.DefineUnstuck(-978.44787597656, 5381.0361328125, 21.577220916748)',
      'GMR.DefineUnstuck(-991.23822021484, 5383.2426757812, 22.014015197754)',
      'GMR.DefineUnstuck(-1007.0693969727, 5387.8139648438, 22.339803695679)',
      'GMR.DefineUnstuck(-1019.1177368164, 5387.4526367188, 22.460868835449)',
      'GMR.DefineUnstuck(-1037.3482666016, 5382.3100585938, 22.047538757324)',
      'GMR.DefineUnstuck(-1052.5563964844, 5379.4033203125, 22.383560180664)',
      'GMR.DefineUnstuck(-1069.8464355469, 5377.2124023438, 23.040899276733)',
      'GMR.DefineUnstuck(-1085.7572021484, 5368.6557617188, 23.529348373413)',
      'GMR.DefineUnstuck(-1101.4727783203, 5358.8530273438, 23.93869972229)',
      'GMR.DefineUnstuck(-1118.5026855469, 5350.7768554688, 24.922330856323)',
      'GMR.DefineUnstuck(-1138.3950195312, 5348.2021484375, 26.383703231812)',
      'GMR.DefineUnstuck(-1158.3969726562, 5344.5791015625, 28.243322372437)',
      'GMR.DefineUnstuck(-1173.7866210938, 5333.2412109375, 29.871667861938)',
      'GMR.DefineUnstuck(-1190.1416015625, 5317.6127929688, 32.407817840576)',
      'GMR.DefineUnstuck(-1203.2305908203, 5305.10546875, 34.535682678223)',
      'GMR.DefineUnstuck(-1216.9177246094, 5292.0263671875, 36.487575531006)',
      'GMR.DefineUnstuck(-1232.1979980469, 5279.5571289062, 38.610946655273)',
      'GMR.DefineUnstuck(-1248.9150390625, 5267.5297851562, 39.910060882568)',
      'GMR.DefineUnstuck(-1262.9569091797, 5255.603515625, 42.025611877441)',
      'GMR.DefineUnstuck(-1276.83984375, 5240.392578125, 44.540363311768)',
      'GMR.DefineUnstuck(-1289.6447753906, 5227.5922851562, 46.273963928223)',
      'GMR.DefineUnstuck(-1306.3527832031, 5211.7016601562, 48.724903106689)',
      'GMR.DefineUnstuck(-1321.8787841797, 5198.9033203125, 51.389797210693)',
      'GMR.DefineUnstuck(-1338.2540283203, 5188.65625, 54.486148834229)',
      'GMR.DefineUnstuck(-1354.8560791016, 5181.5473632812, 57.816787719727)',
      'GMR.DefineUnstuck(-1374.3001708984, 5176.57421875, 60.544261932373)',
      'GMR.DefineUnstuck(-1394.7894287109, 5175.0498046875, 61.981803894043)',
      'GMR.DefineUnstuck(-1414.9633789062, 5175.064453125, 62.292278289795)',
      'GMR.DefineUnstuck(-1435.3618164062, 5176.8354492188, 61.109359741211)',
      'GMR.DefineUnstuck(-1452.9713134766, 5182.3090820312, 56.927871704102)',
      'GMR.DefineUnstuck(-1471.5119628906, 5192.0952148438, 48.764869689941)',
      'GMR.DefineUnstuck(-1486.9582519531, 5204.2421875, 39.925930023193)',
      'GMR.DefineUnstuck(-1498.5120849609, 5218.0747070312, 32.629104614258)',
      'GMR.DefineUnstuck(-1508.3460693359, 5235.1176757812, 25.788000106812)',
      'GMR.DefineUnstuck(-1514.8780517578, 5254.15625, 18.738821029663)',
      'GMR.DefineUnstuck(-1520.3405761719, 5275.70703125, 12.987067222595)',
      'GMR.DefineUnstuck(-1523.4302978516, 5293.9404296875, 10.311326026917)',
      'GMR.DefineUnstuck(-1528.9637451172, 5313.24609375, 8.7905607223511)',
      'GMR.DefineUnstuck(-1543.1544189453, 5323.1704101562, 6.9584856033325)',
      'GMR.DefineUnstuck(-1560.2718505859, 5328.9887695312, 11.709146499634)',
      'GMR.DefineUnstuck(-1577.2867431641, 5333.9018554688, 15.845409393311)',
      'GMR.DefineUnstuck(-1594.576171875, 5340.4033203125, 17.205135345459)',
      'GMR.DefineUnstuck(-1612.5264892578, 5346.3447265625, 17.01683807373)',
      'GMR.DefineUnstuck(-1625.9812011719, 5350.2641601562, 16.59789276123)',
      'GMR.DefineProfileCenter(-1861.068481, 5429.248047, -9.705236, 60)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE City of Light (ESCORT Q) |r",
    QuestID = 10211,
    QuestType = "Custom",
    PickUp = { x = -1859.869995, y = 5420.069824, z = -10.3805, id = 18166 },
    TurnIn = { x = -1859.869995, y = 5420.069824, z = -10.3805, id = 18166 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10211, 1) then
        GMR.SetQuestingState(nil);
        local npcToFollow = GMR.FindObject(19685, 5, nil, nil)
        if npcToFollow then 
          local x, y, z = GMR.ObjectPosition(npcToFollow)
          if not GMR.IsPlayerPosition(x, y, z) then
            GMR.MeshTo(x, y, z)
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("Mount", false)',
      'GMR.AllowSpeedUp()',
      'Dismount();',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineQuestEnemyId(19685)',
      'GMR.DefineCustomObjectId(19685)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE What's Wrong At Cenarion Thicket? |r",
    QuestID = 9957,
    QuestType = "TalkTo",
    PickUp = { x = -181.380005, y = 5527.089844, z = 29.490801, id = 17834 },
    TurnIn = { x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineRepairVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineAmmoVendor(-2092.229980, 5329.310059, -35.305099,19197)',
      'GMR.DefineGoodsVendor(-1769.020020, 5152.540039, -37.121601,19182)',
      'GMR.DefineProfileMailbox(-1897.249023, 5159.242676, -40.208351, 184139)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Strange Energy / Clues in the Thicket / It's Watching You! |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9968, x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
      { questId = 9971, x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
      { questId = 9951, x = -1827.3127441406, y = 4721.1186523438, z = 10.033330917358, id = 18424 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.AllowSpeedUp()',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineRepairVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineAmmoVendor(-2092.229980, 5329.310059, -35.305099,19197)',
      'GMR.DefineGoodsVendor(-1769.020020, 5152.540039, -37.121601,19182)',
      'GMR.DefineProfileMailbox(-1897.249023, 5159.242676, -40.208351, 184139)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Clues in the Thicket |r",
    QuestID = 9971,
    QuestType = "Custom",
    PickUp = { x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
    TurnIn = { x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9971, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1784.603760, 4647.821777, 14.627082, 3) then 
          GMR.MeshTo(-1784.603760, 4647.821777, 14.627082)
        else 
          local object = GMR.GetObjectWithInfo({ id = 183789, rawType = 8 })
          if not GMR.GetDelay("CustomQuest") and object then
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1784.603760, 4647.821777, 14.627082, 100)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineRepairVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineAmmoVendor(-2092.229980, 5329.310059, -35.305099,19197)',
      'GMR.DefineGoodsVendor(-1769.020020, 5152.540039, -37.121601,19182)',
      'GMR.DefineProfileMailbox(-1897.249023, 5159.242676, -40.208351, 184139)',
      'GMR.DefineQuestEnemyId(183789)',
      'GMR.DefineCustomObjectId(183789)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE It's Watching You |r",
    QuestID = 9951,
    QuestType = "Custom",
    PickUp = { x = -1923.77002, y = 4681.97998, z = -1.452, id = 18424 },
    TurnIn = { x = -1840.7947998047, y = 4702.0009765625, z = 9.3594560623169, id = 18424 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9951, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1792.582031, 4728.883789, 66.831192, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineRepairVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineAmmoVendor(-2092.229980, 5329.310059, -35.305099,19197)',
      'GMR.DefineGoodsVendor(-1769.020020, 5152.540039, -37.121601,19182)',
      'GMR.DefineProfileMailbox(-1897.249023, 5159.242676, -40.208351, 184139)',
      'GMR.DefineQuestEnemyId(18438)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Strange Energy |r",
    QuestID = 9968,
    QuestType = "Custom",
    PickUp = { x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
    TurnIn = { x = -1947.199951, y = 4689.5, z = -2.10715, id = 18446 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9968, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18468, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9968, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18437, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-1795.4045410156, 4605.86328125, 12.151285171509, 100)',
      'GMR.DefineProfileCenter(-2086.6782226562, 4682.0063476562, -9.0649337768555, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineRepairVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineAmmoVendor(-2092.229980, 5329.310059, -35.305099,19197)',
      'GMR.DefineGoodsVendor(-1769.020020, 5152.540039, -37.121601,19182)',
      'GMR.DefineProfileMailbox(-1897.249023, 5159.242676, -40.208351, 184139)',
      'GMR.DefineQuestEnemyId(18468)',
      'GMR.DefineQuestEnemyId(18437)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE By Any Means Necessary |r",
    QuestID = 9978,
    QuestType = "TalkTo",
    PickUp = { x = -1947.2028808594, y = 4689.5014648438, z = -2.1905765533447, id = 18446 },
    TurnIn = { x = -1988.2485351562, y = 4474.2778320312, z = 10.476064682007, id = 18482 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand032634 then
        GMR.Print("Frame created")
        GMR.Frames.Rand032634 = CreateFrame("frame")
        GMR.Frames.Rand032634:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9978 then
            if GMR.IsExecuting() then
              if GossipFrameGreetingPanel:IsShown() then
                GossipTitleButton1:Click()
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand032634 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineSellVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineRepairVendor(-1848.020020, 5506.640137, -12.427800, 27667)',
      'GMR.DefineAmmoVendor(-2092.229980, 5329.310059, -35.305099,19197)',
      'GMR.DefineGoodsVendor(-1769.020020, 5152.540039, -37.121601,19182)',
      'GMR.DefineProfileMailbox(-1897.249023, 5159.242676, -40.208351, 184139)'
    }
  })
  

  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Allerian Hold Initial Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10026, x = -2928.189941, y = 3922.560059, z = 3.36487, id = 18252 },
      { questId = 9986, x = -2953.120117, y = 3960.909912, z = 1.06123, id = 18389 },
      { questId = 9998, x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
      { questId = 10016, x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
      { questId = 10033, x = -2969.458252, y = 3973.215576, z = 1.897426, id = 182587 },
      { questId = 9992, x = -2995.949951, y = 3983.48999, z = 3.18814, id = 18390 },
      { questId = 10038, x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
      { questId = 10869, x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
      { questId = 10920, x = -3742.689941, y = 4398.950195, z = -0.295119, id = 22462 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE For the Fallen |r",
    QuestID = 10920,
    QuestType = "Custom",
    PickUp = { x = -3742.689941, y = 4398.950195, z = -0.295119, id = 22462 },
    TurnIn = { x = -3742.689941, y = 4398.950195, z = -0.295119, id = 22462 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10920, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-3789.721, 4232.918, 5.012229, 120)',
      'GMR.DefineProfileCenter(-3545.247, 4180.817, -3.078313, 120)',
      'GMR.DefineProfileCenter(-3425.886, 4164.292, 1.202092, 120)',
      'GMR.DefineProfileCenter(-3321.117, 4132.14, -1.884215, 120)',
      'GMR.DefineProfileCenter(-3218.421, 4069.312, 1.599098, 120)',
      'GMR.DefineProfileCenter(-3139.407, 4024.446, -0.3958226, 120)',
      'GMR.DefineProfileCenter(-3103.842, 4115.291, 5.027544, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(18467)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Timber Worg Tails |r",
    QuestID = 10016,
    QuestType = "Custom",
    PickUp = { x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
    TurnIn = { x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10016, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-2768.081, 3824.491, -4.213726, 150)',
      'GMR.DefineProfileCenter(-2541.919, 3773.131, 1.381312, 150)',
      'GMR.DefineProfileCenter(-2536.564, 3914.632, 8.895326, 150)',
      'GMR.DefineProfileCenter(-2483.64, 4041.572, 4.348158, 150)',
      'GMR.DefineProfileCenter(-2310.131, 4056.003, -29.67371, 150)',
      'GMR.DefineProfileCenter(-2197.134, 3781.594, -16.70876, 150)',
      'GMR.DefineProfileCenter(-2258.1, 3661.784, -11.75576, 150)',
      'GMR.DefineProfileCenter(-2365.163, 3568.794, -13.59411, 150)',
      'GMR.DefineProfileCenter(-2252.671, 3489.873, -22.88033, 150)',
      'GMR.DefineProfileCenter(-2437.138, 3382.853, -17.32899, 150)',
      'GMR.DefineProfileCenter(-2473.36, 3676.098, -1.179567, 150)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineHearthstoneBindLocation(-2917.8811035156, 4021.4836425781, 0.42797529697418, 19296, 1945)',
      'GMR.DefineQuestEnemyId(18477)',
      'GMR.DefineQuestEnemyId(18476)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Elusive Ironjaw |r",
    QuestID = 10022,
    QuestType = "Custom",
    PickUp = { x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
    TurnIn = { x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10022, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2257.219971, 3547.439941, -21.610100, 300)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineHearthstoneBindLocation(-2917.8811035156, 4021.4836425781, 0.42797529697418, 19296, 1945)',
      'GMR.DefineQuestEnemyId(18670)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Olemba Seeds |r",
    QuestID = 9992,
    QuestType = "Custom",
    PickUp = { x = -2995.949951, y = 3983.48999, z = 3.18814, id = 18390 },
    TurnIn = { x = -2995.949951, y = 3983.48999, z = 3.18814, id = 18390 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9992, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2321.056885, 4387.631348, 2.825834, 120)',
      'GMR.DefineProfileCenter(-2188.033691, 4506.437988, -4.913927, 120)',
      'GMR.DefineProfileCenter(-2256.928, 4508.854, 2.198949, 120)',
      'GMR.DefineProfileCenter(-2225.633, 4598.754, 0.002285673, 120)',
      'GMR.DefineProfileCenter(-2190.498, 4692.533, -3.489186, 120)',
      'GMR.DefineProfileCenter(-2261.94, 4755.289, -1.478886, 120)',
      'GMR.DefineProfileCenter(-2308.55, 4903.192, 0.3251835, 120)',
      'GMR.DefineProfileCenter(-2552.909, 4019.194, 2.765196, 120)',
      'GMR.DefineProfileCenter(-2545.096, 3943.64, 11.49634, 120)',
      'GMR.DefineProfileCenter(-2481.628, 3869.224, 5.988023, 120)',
      'GMR.DefineProfileCenter(-2275.808, 3674.652, -9.520188, 120)',
      'GMR.DefineProfileCenter(-2197.486, 3710.016, -21.1491, 120)',
      'GMR.DefineProfileCenter(-2143.426, 3766.142, -18.77806, 120)',
      'GMR.DefineProfileCenter(-2031.149, 3623.989, -84.80574, 120)',
      'GMR.DefineProfileCenter(-2220.903, 3371.541, -21.09275, 120)',
      'GMR.DefineProfileCenter(-2440.74, 3459.279, -18.43487, 120)',
      'GMR.DefineProfileCenter(-2510.757, 3785.469, 8.346402, 120)',
      'GMR.DefineProfileCenter(-2749.236, 3890.009, -16.54146, 40)',
      'GMR.DefineProfileCenter(-2481.713, 3868.302, 5.776385, 120)',
      'GMR.DefineProfileCenter(-2290.352, 4046.362, -29.45428, 120)',
      'GMR.DefineProfileCenter(-2219.429, 4086.548, -0.4167691, 120)',
      'GMR.DefineProfileCenter(-2149.718, 4068.301, 2.669401, 120)',
      'GMR.DefineProfileCenter(-2025.993, 3964.706, 2.455972, 120)',
      'GMR.DefineProfileCenter(-2545.636, 3944.179, 11.25057, 120)',
      'GMR.DefineAreaBlacklist(-2933.145, 3979.355, -0.7534243,50)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineHearthstoneBindLocation(-2917.8811035156, 4021.4836425781, 0.42797529697418, 19296, 1945)',
      'GMR.DefineCustomObjectId(182541)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Vessels of Power |r",
    QuestID = 10028,
    QuestType = "Custom",
    PickUp = { x = -2995.949951, y = 3983.48999, z = 3.18814, id = 18390 },
    TurnIn = { x = -2995.949951, y = 3983.48999, z = 3.18814, id = 18390 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10028, 1) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = { 182581, 182583 }, rawType = 8 })
        if not GMR.GetDelay("CustomQuest") and object then
          GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 2)
        elseif not object then
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2242.256, 4199.305, 0.9727687, 120)',
      'GMR.DefineProfileCenter(-2174.726, 4265.765, 3.358314, 120)',
      'GMR.DefineProfileCenter(-2164.449, 4282.506, 11.82193, 120)',
      'GMR.DefineProfileCenter(-2194.729, 4210.407, 10.55477, 120)',
      'GMR.DefineProfileCenter(-2178.827, 4183.729, 12.98945, 120)',
      'GMR.DefineProfileCenter(-2172.018, 4162.924, 11.27925, 120)',
      'GMR.DefineProfileCenter(-2126.143, 4155.164, 12.9065, 120)',
      'GMR.DefineProfileCenter(-2143.931, 4153.305, 9.772337, 120)',
      'GMR.DefineProfileCenter(-2096.385, 4252.974, 8.564473, 120)',
      'GMR.DefineProfileCenter(-2079.057, 4260.274, 9.430939, 120)',
      'GMR.DefineProfileCenter(-2021.891, 4260.073, 4.415779, 120)',
      'GMR.DefineProfileCenter(-2047.943, 4242.699, 7.794044, 120)',
      'GMR.DefineProfileCenter(-2009.338, 4312.287, 2.939979, 120)',
      'GMR.DefineProfileCenter(-1971.112, 4281.042, 1.392343, 120)',
      'GMR.DefineProfileCenter(-1970.524, 4212.374, 2.607379, 120)',
      'GMR.DefineProfileCenter(-1972.673, 4196.252, 2.534155, 120)',
      'GMR.DefineProfileCenter(-1986.611, 4149.472, 3.806302, 120)',
      'GMR.DefineProfileCenter(-2012.557, 4115.497, 1.201632, 120)',
      'GMR.DefineProfileCenter(-2080.975, 4021.98, 1.095935, 120)',
      'GMR.DefineProfileCenter(-2125.965, 4155.482, 12.8715, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineCustomObjectId(182581)',
      'GMR.DefineCustomObjectId(182583)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Magical Disturbances |r",
    QuestID = 10026,
    QuestType = "Custom",
    PickUp = { x = -2928.189941, y = 3922.560059, z = 3.36487, id = 18252 },
    TurnIn = { x = -2928.189941, y = 3922.560059, z = 3.36487, id = 18252 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10026, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2289.743896, 4113.594238, -15.472625, 120)',
      'GMR.DefineProfileCenter(-2307.270752, 3939.348145, -8.608910, 120)',
      'GMR.DefineProfileCenter(-2530.979004, 4022.451660, 2.465660, 120)',
      'GMR.DefineProfileCenter(-2632.587158, 3818.475098, -15.905037, 120)',
      'GMR.DefineProfileCenter(-2302.913330, 3489.533203, -24.204762, 120)',
      'GMR.DefineProfileCenter(-2195.091797, 3309.647461, -27.302782, 120)',
      'GMR.DefineProfileCenter(-2487.876709, 3383.822021, -9.629047, 120)',
      'GMR.DefineProfileCenter(-2220.229004, 3604.647217, -25.718586, 120)',
      'GMR.DefineAreaBlacklist(-2354.909912, 3208.290039, -3.292260,40)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineHearthstoneBindLocation(-2917.8811035156, 4021.4836425781, 0.42797529697418, 19296, 1945)',
      'GMR.DefineQuestEnemyId(18464)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Unruly Neighbors |r",
    QuestID = 9998,
    QuestType = "Custom",
    PickUp = { x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
    TurnIn = { x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9998, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2896.929443, 3650.500977, -16.476896, 60)',
      'GMR.DefineProfileCenter(-2858.367, 3551.713, -26.35714, 60)',
      'GMR.DefineProfileCenter(-2837.91, 3488.656, -36.28082, 60)',
      'GMR.DefineProfileCenter(-2782.6376953125, 3543.7463378906, -20.563829421997, 60)',
      'GMR.DefineProfileCenter(-2772.1569824219, 3520.1960449219, -20.847450256348, 60)',
      'GMR.DefineProfileCenter(-2783.072265625, 3484.5817871094, -28.795965194702, 60)',
      'GMR.DefineProfileCenter(-2835.8913574219, 3445.0183105469, -37.191867828369, 60)',
      'GMR.DefineProfileCenter(-2832.369140625, 3396.6428222656, -38.935920715332, 60)',
      'GMR.DefineProfileCenter(-2924.3759765625, 3359.8957519531, -4.3286094665527, 60)',
      'GMR.DefineProfileCenter(-2956.2834472656, 3373.3369140625, -3.9316701889038, 60)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineHearthstoneBindLocation(-2917.8811035156, 4021.4836425781, 0.42797529697418, 19296, 1945)',
      'GMR.DefineQuestEnemyId(18595)'
    }
  })
  
  -- Maybe remove this chain (down to Thin The Flock)
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Firewing Liasion / Thinning the Ranks |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10002, x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
      { questId = 10007, x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Firewing Liaison |r",
    QuestID = 10002,
    QuestType = "Custom",
    PickUp = { x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
    TurnIn = { x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10002, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-2918.129883, 3419.439941, 1.959390, 120)',
      'GMR.DefineAreaBlacklist(-2976.129395, 3389.703125, -1.725883,20)',
      'GMR.DefineAreaBlacklist(-2948.969971, 3386.080078, 0.147990,10)',
      'GMR.DefineAreaBlacklist(-2968.459961, 3386.590088, -3.111640,10)',
      'GMR.DefineAreaBlacklist(-2979.921387, 3406.797119, 0.228686,10)',
      'GMR.DefineAreaBlacklist(-2987.197266, 3380.662598, -1.415797,15)',
      'GMR.DefineAreaBlacklist(-2968.459961, 3386.590088, -3.111640,10)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(18583)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Fel Orc Plans |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10012, x = -2928.169189, y = 3415.461182, z = 1.893931, id = 182549 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Thinning the Ranks |r",
    QuestID = 10007,
    QuestType = "Custom",
    PickUp = { x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
    TurnIn = { x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10007, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 16772, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10007, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 16810, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-2886.368896, 3653.831787, -17.856758, 40)',
      'GMR.DefineProfileCenter(-2968.346, 3612.903, -8.444069, 40)',
      'GMR.DefineProfileCenter(-2959.899, 3558.599, -7.982112, 40)',
      'GMR.DefineProfileCenter(-2950.854, 3495.43, -2.732383, 40)',
      'GMR.DefineProfileCenter(-2919.302, 3421.378, 1.422278, 40)',
      'GMR.DefineProfileCenter(-2978.585, 3473.977, 0.08203412, 40)',
      'GMR.DefineProfileCenter(-2985.432, 3539.789, -4.396559, 40)',
      'GMR.DefineProfileCenter(-2998.744, 3594.051, -2.513482, 40)',
      'GMR.DefineProfileCenter(-2918.857, 3519.867, -22.04997, 50)',
      'GMR.DefineProfileCenter(-2884.401, 3501.318, -30.88938, 50)',
      'GMR.DefineProfileCenter(-2854.605, 3532.996, -30.92262, 40)',
      'GMR.DefineProfileCenter(-2841.768, 3426.215, -38.73447, 40)',
      'GMR.DefineProfileCenter(-2898.826, 3361.479, -12.86949, 40)',
      'GMR.DefineAreaBlacklist(-2984.197754, 3383.991943, -0.986128,20)',
      'GMR.DefineAreaBlacklist(-2961.748047, 3385.517578, -4.013443,10)',
      'GMR.DefineAreaBlacklist(-2982.086426, 3419.332764, 0.230784,10)',
      'GMR.DefineAreaBlacklist(-2932.556885, 3381.549805, 0.228896,7)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "Herbs", "Ores", "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(16810)',
      'GMR.DefineQuestEnemyId(16772)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE The Firewing Liasion / Thinning the Ranks |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10002, x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 },
      { questId = 10007, x = -2592.439941, y = 3317.340088, z = 0.68057, id = 18565 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Fel Orc Plans |r",
    QuestID = 10012,
    QuestType = "TalkTo",
    PickUp = { x = -2928.169189, y = 3415.461182, z = 1.893931, id = 182549 },
    TurnIn = { x = -2971.169922, y = 3935.179932, z = 3.86593, id = 18387 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineProfileCenter(-2928.169189, 3415.461182, 1.893931, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "Herbs", "Ores", "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(182549)',
      'GMR.DefineCustomObjectId(182549)'
    }
  })
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Thin the Flock |r",
    QuestID = 10869,
    QuestType = "Custom",
    PickUp = { x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
    TurnIn = { x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10869, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18449, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10869, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18450, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-1968.86, 3854.592, -2.054244, 120)',
      'GMR.DefineProfileCenter(-1900.909, 3926.826, -0.6663453, 120)',
      'GMR.DefineProfileCenter(-1861.399, 3884.465, 4.468848, 120)',
      'GMR.DefineProfileCenter(-1805.801, 3920.545, 9.616444, 120)',
      'GMR.DefineProfileCenter(-1912.195, 3994.879, -5.248832, 120)',
      'GMR.DefineProfileCenter(-1703.356, 4403.708, 17.69036, 120)',
      'GMR.DefineProfileCenter(-1620.636, 4445.997, 24.19338, 120)',
      'GMR.DefineProfileCenter(-1545.25, 4419.634, 39.27677, 120)',
      'GMR.DefineProfileCenter(-1513.191, 4473.46, 40.12996, 120)',
      'GMR.DefineProfileCenter(-1649.698, 4417.342, 20.70639, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(18450)',
      'GMR.DefineQuestEnemyId(18449)',
      'GMR.DefineQuestEnemyId(18540)',
      'GMR.DefineQuestEnemyId(18541)',
      'GMR.DefineQuestEnemyId(18539)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Stymying the Arakkoa |r",
    QuestID = 9986,
    QuestType = "Custom",
    PickUp = { x = -2953.120117, y = 3960.909912, z = 1.06123, id = 18389 },
    TurnIn = { x = -2953.120117, y = 3960.909912, z = 1.06123, id = 18389 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9986, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1607.959961, 4431.229980, 52.711899, 4) then 
          GMR.MeshTo(-1607.959961, 4431.229980, 52.711899)
        else 
          local object = GMR.GetObjectWithInfo({ id = 18539, rawType = 5, isAlive = true })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end 
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9986, 2) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1845.660034, 3874.659912, 6.330900, 4) then 
          GMR.MeshTo(-1845.660034, 3874.659912, 6.330900)
        else 
          local object = GMR.GetObjectWithInfo({ id = 18540, rawType = 5, isAlive = true })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end 
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9986, 3) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-2515.270020, 5379.049805, 28.0072000, 4) then 
          GMR.MeshTo(-2515.270020, 5379.049805, 28.0072000)
        else 
          local object = GMR.GetObjectWithInfo({ id = 18541, rawType = 5, isAlive = true })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-2515.8972167969, 5385.3139648438, 0.047374684363604, 20)',
      'GMR.DefineUnstuck(-2510.9748535156, 5391.6181640625, 0.047374684363604)',
      'GMR.DefineUnstuck(-2522.4309082031, 5395.6455078125, 0.047374684363604)',
      'GMR.DefineUnstuck(-2533.2932128906, 5396.4750976562, 0.047374684363604)',
      'GMR.DefineUnstuck(-2544.02734375, 5397.294921875, 0.047374684363604)',
      'GMR.DefineUnstuck(-2556.3508300781, 5402.2822265625, 0.058835912495852)',
      'GMR.DefineUnstuck(-2559.2016601562, 5409.8286132812, 0.31162738800049)',
      'GMR.DefineUnstuck(-2555.2397460938, 5419.5600585938, 0.55655723810196)',
      'GMR.DefineUnstuck(-2552.3044433594, 5430.6938476562, 0.059669725596905)',
      'GMR.DefineUnstuck(-2546.5498046875, 5437.6826171875, 0.048345007002354)',
      'GMR.DefineUnstuck(-2540.3479003906, 5444.1215820312, 0.21641391515732)',
      'GMR.DefineUnstuck(-2530.1616210938, 5450.7509765625, 0.89600253105164)',
      'GMR.DefineUnstuck(-2527.5573730469, 5460.73828125, 3.836597442627)',
      'GMR.DefineUnstuck(-2533.26171875, 5469.7163085938, 7.2105107307434)',
      'GMR.DefineUnstuck(-2541.9108886719, 5473.9052734375, 8.2513961791992)',
      'GMR.DefineUnstuck(-2547.478515625, 5467.9140625, 8.843879699707)',
      'GMR.DefineUnstuck(-2555.0148925781, 5458.9799804688, 18.083490371704)',
      'GMR.DefineUnstuck(-2560.6911621094, 5452.39453125, 26.081758499146)',
      'GMR.DefineUnstuck(-2564.9030761719, 5446.8403320312, 27.238645553589)',
      'GMR.DefineProfileCenter(-1607.959961, 4431.229980, 52.711899, 120)',
      'GMR.DefineProfileCenter(-1845.660034, 3874.659912, 6.330900, 120)',
      'GMR.DefineProfileCenter(-2515.270020, 5379.049805, 28.0072000, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(18539)',
      'GMR.DefineQuestEnemyId(18540)',
      'GMR.DefineQuestEnemyId(18541)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Thin the Flock / Styming the Arakkoa |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10869, x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
      { questId = 9986, x = -2953.120117, y = 3960.909912, z = 1.06123, id = 18389 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Speak with Private Weeks |r",
    QuestID = 10038,
    QuestType = "TalkTo",
    PickUp = { x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
    TurnIn = { x = -2301.669922, y = 4898.390137, z = 1.22768, id = 18715 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2301.669922, 4898.390137, 1.227680, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(182549)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Who Are They? |r",
    QuestID = 10040,
    QuestType = "Custom",
    PickUp = { x = -2301.669922, y = 4898.390137, z = 1.22768, id = 18715 },
    TurnIn = { x = -2301.669922, y = 4898.390137, z = 1.22768, id = 18715 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      local buff1 = GetSpellInfo(32756)
      if not GMR.HasBuff("player", buff1) then
        if not GMR.Questing.IsObjectiveCompleted(10040, 1) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(-2301.669922, 4898.390137, 1.227680, 3) then 
            GMR.MeshTo(-2301.669922, 4898.390137, 1.227680)
          else 
            local object = GMR.GetObjectWithInfo({ id = 18715, rawType = 5 })
            if not GMR.GetDelay("CustomQuest") and object then 
              GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
            end 
          end
        elseif not GMR.Questing.IsObjectiveCompleted(10040, 2) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(-2301.669922, 4898.390137, 1.227680, 3) then 
            GMR.MeshTo(-2301.669922, 4898.390137, 1.227680)
          else 
            local object = GMR.GetObjectWithInfo({ id = 18715, rawType = 5 })
            if not GMR.GetDelay("CustomQuest") and object then 
              GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
            end 
          end
        elseif not GMR.Questing.IsObjectiveCompleted(10040, 3) then
          GMR.SetQuestingState(nil);
          if not GMR.IsPlayerPosition(-2301.669922, 4898.390137, 1.227680, 3) then 
            GMR.MeshTo(-2301.669922, 4898.390137, 1.227680)
          else 
            local object = GMR.GetObjectWithInfo({ id = 18715, rawType = 5 })
            if not GMR.GetDelay("CustomQuest") and object then 
              GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 3)
            end 
          end
        end
      elseif GMR.HasBuff("player", buff1) then
        if not GMR.Questing.IsObjectiveCompleted(10040, 1) then
          GMR.SetQuestingState(nil);
          local object = GMR.GetObjectWithInfo({ id = 18716, rawType = 5 })
          if object then 
            GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5)
          else
            GMR.SetQuestingState("Idle")
          end
  
        elseif not GMR.Questing.IsObjectiveCompleted(10040, 2) then
          GMR.SetQuestingState(nil);
          local object = GMR.GetObjectWithInfo({ id = 18717, rawType = 5 })
          if object then 
            GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5)
          else
            GMR.SetQuestingState("Idle")
          end
        elseif not GMR.Questing.IsObjectiveCompleted(10040, 3) then
          GMR.SetQuestingState(nil);
          local object = GMR.GetObjectWithInfo({ id = 18719, rawType = 5 })
          if object then 
            GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5)
          else
            GMR.SetQuestingState("Idle")
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("Mount", false)',
      'GMR.DenySpeedUp()',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-2460.0705566406, 4979.0004882812, 29.106071472168, 10)',
      'GMR.DefineUnstuck(-2462.2495117188, 4971.2768554688, 30.196853637695)',
      'GMR.DefineUnstuck(-2464.9333496094, 4961.7641601562, 31.950836181641)',
      'GMR.DefineUnstuck(-2464.28125, 4952.017578125, 34.304752349854)',
      'GMR.DefineUnstuck(-2460.0703125, 4945.2705078125, 34.682647705078)',
      'GMR.DefineUnstuck(-2452.6047363281, 4943.0727539062, 34.654804229736)',
      'GMR.DefineUnstuck(-2445.8952636719, 4943.8115234375, 34.41410446167)',
      'GMR.DefineUnstuck(-2437.8334960938, 4948.9619140625, 34.861045837402)',
      'GMR.DefineUnstuck(-2432.044921875, 4953.7421875, 34.715000152588)',
      'GMR.DefineUnstuck(-2422.9519042969, 4961.3251953125, 33.37329864502)',
      'GMR.DefineProfileCenter(-2427.967, 4975.493, 31.14033,120)',
      'GMR.DefineProfileCenter(-2461.742, 5017.048, 26.41973,120)',
      'GMR.DefineProfileCenter(-2402.209, 4906.547, 35.31567,120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(99999999)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Kill the Shadow Council (Huge Melee / Caster Weapon Upgrade! Turn off GMR to Turn-In.) |r",
    QuestID = 10042,
    QuestType = "Custom",
    PickUp = { x = -2301.669922, y = 4898.390137, z = 1.22768, id = 18715 },
    TurnIn = { x = -3007.899902, y = 3978.219971, z = 3.10527, id = 18713 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      local buff1 = GetSpellInfo(32756)
      if GMR.HasBuff("player", buff1) and GMR.IsQuestActive(10042) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-2416.282, 5260.091, 0.7866665, 3) then 
          GMR.MeshTo(-2416.282, 5260.091, 0.7866665)
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10042, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 16519, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10042, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 17088, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10042, 3) then
        local enemy = GMR.GetObjectWithInfo({ id = 18720, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.AllowSpeedUp()',
      'GMR.DefineProfileCenter(-2412.549, 5035.285, 27.60207, 120)',
      'GMR.DefineProfileCenter(-2405.597, 4970.307, 31.65, 120)',
      'GMR.DefineProfileCenter(-2409.683, 4909.032, 36.14859, 120)',
      'GMR.DefineProfileCenter(-2458.739, 4941.972, 34.65333, 120)',
      'GMR.DefineProfileCenter(-2467.73, 4899.961, 37.25699, 120)',
      'GMR.DefineProfileCenter(-2519.113, 4909.569, 39.2748, 120)',
      'GMR.DefineProfileCenter(-2473.451, 4988.966, 29.73491, 120)',
      'GMR.DefineProfileCenter(-2475.305, 5024.768, 23.89579, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(16519)',
      'GMR.DefineQuestEnemyId(17088)',
      'GMR.DefineQuestEnemyId(18720)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Wanted: Bonelashers Dead |r",
    QuestID = 10033,
    QuestType = "Custom",
    PickUp = { x = -2969.458252, y = 3973.215576, z = 1.897426, id = 182587 },
    TurnIn = { x = -2923.090088, y = 3945.709961, z = 0.770363, id = 18704 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10033, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2811.770020, 4694.549805, -9.765470, 120)',
      'GMR.DefineProfileCenter(-2826.239990, 4815.540039, -8.343090, 120)',
      'GMR.DefineProfileCenter(-2858.697754, 4949.097168, -20.013674, 120)',
      'GMR.DefineProfileCenter(-2959.289551, 5188.062012, -18.233027, 120)',
      'GMR.DefineProfileCenter(-2957.805908, 5186.143555, -18.071909, 120)',
      'GMR.DefineProfileCenter(-2882.396729, 4977.095703, -20.542873, 120)',
      'GMR.DefineProfileCenter(-2784.306885, 4949.300293, -12.231000, 120)',
      'GMR.DefineProfileCenter(-2889.172607, 4825.264160, -15.430741, 120)',
      'GMR.DefineProfileCenter(-3016.291504, 4644.579102, -21.434692, 120)',
      'GMR.DefineProfileCenter(-2944.593994, 4575.592773, -20.931089, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(18470)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Infested Protectors |r",
    QuestID = 10896,
    QuestType = "Custom",
    PickUp = { x = -2860.590088, y = 5038.810059, z = -19.9867, id = 22420 },
    TurnIn = { x = -2860.590088, y = 5038.810059, z = -19.9867, id = 22420 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10896, 1) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = 22419, rawType = 5, isAlive = true })
        if object then
          GMR.RunMacroText("/startattack")
          GMR.Questing.InteractWith(object, nil, nil, nil, nil, 2)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2727.369141, 4926.481934, -6.340390, 120)',
      'GMR.DefineProfileCenter(-2758.926025, 5212.602051, -5.734582, 120)',
      'GMR.DefineProfileCenter(-2860.062744, 4615.881836, -15.520354, 120)',
      'GMR.DefineProfileCenter(-3086.968750, 4309.070801, -11.469718, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2844.340088, 5050.810059, -17.986700, 20890)',
      'GMR.DefineRepairVendor(-2844.340088, 5050.810059, -17.986700, 20890)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(22095)',
      'GMR.DefineQuestEnemyId(22419)',
      'GMR.DefineQuestEnemyId(22307)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Before Darkness Falls |r",
    QuestID = 10878,
    QuestType = "Custom",
    PickUp = { x = -2820.896484, y = 5072.438477, z = -13.884975, id = 22370 },
    TurnIn = { x = -2820.896484, y = 5072.438477, z = -13.884975, id = 22370 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10878, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 21661, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10878, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 21907, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10878, 3) then
        local enemy = GMR.GetObjectWithInfo({ id = 21902, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-3050.166, 4857.62, -21.62432, 120)',
      'GMR.DefineProfileCenter(-3108.536, 4851.954, -22.1324, 120)',
      'GMR.DefineProfileCenter(-3148.628, 4927.665, -9.001003, 120)',
      'GMR.DefineProfileCenter(-3109.729, 4976.366, -22.38007, 120)',
      'GMR.DefineProfileCenter(-3100.761, 5047.039, -21.7364, 120)',
      'GMR.DefineProfileCenter(-3148.293, 4976.82, -9.13225, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2844.340088, 5050.810059, -17.986700, 20890)',
      'GMR.DefineRepairVendor(-2844.340088, 5050.810059, -17.986700, 20890)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(21661)',
      'GMR.DefineQuestEnemyId(21907)',
      'GMR.DefineQuestEnemyId(21902)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Taken in the Night / An Improper Burial |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10873, x = -3723.719971, y = 5385.680176, z = -1.763027, id = 22364 },
      { questId = 10913, x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
      { questId = 10877, x = -3755.22998, y = 5405.950195, z = -3.31487, id = 22456 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineRepairVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE An Improper Burial |r",
    QuestID = 10913,
    QuestType = "Custom",
    PickUp = { x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
    TurnIn = { x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      local debuff = GetSpellInfo(39189)
      if not GMR.Questing.IsObjectiveCompleted(10913, 1) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = 21859, rawType = 5, isAlive = false })
        local object2 = GMR.GetObjectWithInfo({ id = 21846, rawType = 5, isAlive = false })
        local itemName = GetItemInfo(31769)
        GMR.Use(itemName)
        if not GMR.GetDelay("CustomQuest") then
          if object or object2 then
            GMR.Use(itemName)
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 15)
            GMR.Questing.InteractWith(object2, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 6)
          else
            GMR.SetQuestingState("Idle")
          end
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10913, 2) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = 21846, rawType = 5, isAlive = false })
        local itemName = GetItemInfo(31769)
        if object and not GMR.HasBuff("target", debuff)then
          GMR.TargetUnit(object)
          GMR.Use(itemName)
          GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(-3740.265, 5271.712, -16.08285, 120)',
      'GMR.DefineProfileCenter(-3753.375, 5167.119, -22.31171, 120)',
      'GMR.DefineProfileCenter(-3757.124, 5086.656, -18.71628, 120)',
      'GMR.DefineProfileCenter(-3752.112, 5177.23, -22.4522, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineRepairVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Hero Is Needed |r",
    QuestID = 10914,
    QuestType = "Custom",
    PickUp = { x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
    TurnIn = { x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10914, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 21285, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10914, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 21284, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-3670.435, 5316.637, -18.70917, 120)',
      'GMR.DefineProfileCenter(-3673.875, 5130.546, -22.29036, 120)',
      'GMR.DefineProfileCenter(-3661.647, 5041.041, -21.80944, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineRepairVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(21285)',
      'GMR.DefineQuestEnemyId(21284)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Fallen Exarch |r",
    QuestID = 10915,
    QuestType = "Custom",
    PickUp = { x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
    TurnIn = { x = -3740.01001, y = 5409.069824, z = -3.24949, id = 22446 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10915, 1) then
        if not GMR.IsPlayerPosition(-3365.373, 5145.716, -8.134784, 5) then 
          GMR.MeshTo(-3365.373, 5145.716, -8.134784)
        else 
          local object = GMR.GetObjectWithInfo({ id = 184999, rawType = 8 })
          local npc = GMR.GetObjectWithInfo({ id = 184999, rawType = 5 })
          if npc then
            GMR.Questing.InteractWith(npc, nil, nil, nil, nil, 10)
          elseif object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-3365.373, 5145.716, -8.134784, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineRepairVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineCustomObjectId(184999)',
      'GMR.DefineQuestEnemyId(22452)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Dead Relic |r",
    QuestID = 10877,
    QuestType = "Custom",
    PickUp = { x = -3755.22998, y = 5405.950195, z = -3.31487, id = 22456 },
    TurnIn = { x = -3755.22998, y = 5405.950195, z = -3.31487, id = 22456 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10877, 1) then
        if not GMR.IsPlayerPosition(-3751.735, 4710.657, -17.48213, 5) then 
          GMR.MeshTo(-3751.735, 4710.657, -17.48213)
        else 
          local object = GMR.GetObjectWithInfo({ id = 185220, rawType = 8 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-3751.735, 4710.657, -17.48213, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineRepairVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineCustomObjectId(185220)',
      'GMR.DefineQuestEnemyId(185220)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Taken in the Night |r",
    QuestID = 10873,
    QuestType = "Custom",
    PickUp = { x = -3723.719971, y = 5385.680176, z = -1.763027, id = 22364 },
    TurnIn = { x = -3723.719971, y = 5385.680176, z = -1.763027, id = 22364 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10873, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-3816.058, 4224.468, 3.821037, 120)',
      'GMR.DefineProfileCenter(-3901.141, 4249.95, -11.30132, 120)',
      'GMR.DefineProfileCenter(-3913.661, 4323.757, -7.303356, 120)',
      'GMR.DefineProfileCenter(-3942.174, 4337.71, -11.93079, 120)',
      'GMR.DefineProfileCenter(-3832.674, 4268.02, 7.37453, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineRepairVendor(-2978.949951, 4033.120117, 3.657580, 19056)',
      'GMR.DefineAmmoVendor(-2925.260010, 3964.770020, 0.366130, 19053)',
      'GMR.DefineGoodsVendor(-2917.879883, 4021.479980, 0.510790, 19296)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(22355)',
      'GMR.DefineQuestEnemyId(22355)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Cabals Orders - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 10880,
    QuestType = "Grinding",
    PickUp = { x = -3098.7482910156, y = 4945.8779296875, z = -21.13009262085, id = { 21661, 31707 } },
    TurnIn = { x = -2825.0583496094, y = 5073.7397460938, z = -15.405363082886, id = 22370 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand002318 then
        GMR.Print("Frame created")
        GMR.Frames.Rand002318 = CreateFrame("frame")
        GMR.Frames.Rand002318:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10880 then
            if GMR.IsExecuting() and GetItemCount(31707) >= 1 and not GMR.IsQuestActive(10880) then
              local itemName = GetItemInfo(31707)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand002318 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-3098.7482910156, 4945.8779296875, -21.13009262085 290)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineRepairVendor(-3753.330078, 5410.810059, -3.232180, 22476)',
      'GMR.DefineGoodsVendor(-2843.919922, 5046.500000, -17.973801, 20893)',
      'GMR.DefineAmmoVendor(-2841.870117, 5049.399902, -17.666000, 20892)',
      'GMR.DefineProfileMailbox(-2930.108887, 4008.547852, -1.366403, 182939)',
      'GMR.DefineQuestEnemyId(21661)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Message to Telaar (We should be FRIENDLY with Kurenai) |r",
    QuestID = 9792,
    QuestType = "TalkTo",
    PickUp = { x = 1024.030029, y = 7366.399902, z = 36.418598, id = 18008 },
    TurnIn = { x = -2574.310059, y = 7270.189941, z = 15.5622, id = 18097 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2574.310059, 7270.189941, 15.562200, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(182549)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE ALL Telaar Quests (Nagrand Initial Quests) |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10476, x = -2569.780029, y = 7271.870117, z = 15.4773, id = 18408 },
      { questId = 9936, x = -2563.839, y = 7274.782, z = 15.48017, id = 182393 },
      { questId = 9940, x = -2563.839, y = 7274.782, z = 15.48017, id = 182393 },
      { questId = 9917, x = -2556.340088, y = 7254.189941, z = 15.2034, id = 18353 },
      { questId = 10113, x = -2551.050049, y = 7256.629883, z = 16.6978, id = 19137 },
      { questId = 9869, x = -2491.860107, y = 7228.569824, z = 16.3211, id = 18233 },
      { questId = 9956, x = -2604.96582, y = 7287.297363, z = 20.336363, id = 18416 },
      { questId = 9874, x = -2613.370117, y = 7285.740234, z = 20.815001, id = 18222 },
      { questId = 9878, x = -2621.600098, y = 7286.529785, z = 20.880301, id = 18224 },
      { questId = 9913, x = -2528.360107, y = 7472.060059, z = -4.50013, id = 18335 },
      { questId = 10109, x = -2428.169922, y = 6885.75, z = 4.1961, id = 19035 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Nesingwary Safari |r",
    QuestID = 10113,
    QuestType = "TalkTo",
    PickUp = { x = -2551.050049, y = 7256.629883, z = 16.6978, id = 19137 },
    TurnIn = { x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
        if not GMR.Frames.Rand0326 then
      GMR.Print("Frame created")
      GMR.Frames.Rand0326 = CreateFrame("frame")
      GMR.Frames.Rand0326:SetScript("OnUpdate", function(self)
        if GMR.GetProfileType() == "Questing" and GMR.IsQuestCompletable(10113) then
          if GMR.IsExecuting() then
            if GossipFrameGreetingPanel:IsShown() then
              GossipTitleButton1:Click()
            end
          end
        else 
          self:SetScript("OnUpdate", nil); GMR.Frames.Rand0326 = nil; GMR.Print("Frame deleted")
        end
      end)
    end
    ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1461.780029, 6344.299805, 37.273899, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(18180)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Mastery Quests (Part 1) Nagrand |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9857, x = -1454.640015, y = 6352.279785, z = 37.323799, id = 18218 },
      { questId = 9789, x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
      { questId = 9854, x = -1450.880005, y = 6341.370117, z = 37.398899, id = 18200 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
        if not GMR.Frames.Rand03263 then
      GMR.Print("Frame created")
      GMR.Frames.Rand03263 = CreateFrame("frame")
      GMR.Frames.Rand03263:SetScript("OnUpdate", function(self)
        if GMR.GetProfileType() == "Questing" then
          if GMR.IsExecuting() and not GMR.IsQuestCompleted(9789) then
            if GossipFrameGreetingPanel:IsShown() or QuestFrame:IsShown() then
              GossipTitleButton1:Click()
            end
          end
        else 
          self:SetScript("OnUpdate", nil); GMR.Frames.Rand03263 = nil; GMR.Print("Frame deleted")
        end
      end)
    end
    ]],
    'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
    'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
    'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
    'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
    'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Throne of the Elements |r",
    QuestID = 9869,
    QuestType = "TalkTo",
    PickUp = { x = -2491.860107, y = 7228.569824, z = 16.3211, id = 18233 },
    TurnIn = { x = -772.432983, y = 6944.549805, z = 32.341599, id = 18072 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-1316.6378173828, 6828.693359375, 35.177291870117, 100)',
      'GMR.DefineUnstuck(-1298.8005371094, 6839.802734375, 33.769729614258)',
      'GMR.DefineUnstuck(-1277.8497314453, 6852.8515625, 34.137966156006)',
      'GMR.DefineUnstuck(-1260.7135009766, 6863.5239257812, 34.905986785889)',
      'GMR.DefineUnstuck(-1241.5163574219, 6869.7060546875, 33.546703338623)',
      'GMR.DefineUnstuck(-1229.1279296875, 6859.8388671875, 32.349624633789)',
      'GMR.DefineUnstuck(-1221.2547607422, 6850.046875, 33.149444580078)',
      'GMR.DefineUnstuck(-1215.73046875, 6848.6528320312, 31.901044845581)',
      'GMR.DefineUnstuck(-1208.1842041016, 6854.4248046875, 31.345815658569)',
      'GMR.DefineUnstuck(-1194.7478027344, 6864.9697265625, 31.345815658569)',
      'GMR.DefineUnstuck(-1188.2746582031, 6866.4448242188, 31.345815658569)',
      'GMR.DefineUnstuck(-1179.95703125, 6863.0766601562, 31.345815658569)',
      'GMR.DefineUnstuck(-1167.9831542969, 6854.3203125, 31.345815658569)',
      'GMR.DefineUnstuck(-1159.7309570312, 6851.5361328125, 31.345815658569)',
      'GMR.DefineUnstuck(-1151.5330810547, 6849.4780273438, 31.345815658569)',
      'GMR.DefineUnstuck(-1142.208984375, 6847.349609375, 31.345815658569)',
      'GMR.DefineUnstuck(-1135.015625, 6844.83984375, 31.345815658569)',
      'GMR.DefineUnstuck(-1126.7935791016, 6841.5913085938, 31.345815658569)',
      'GMR.DefineUnstuck(-1119.3957519531, 6839.9497070312, 31.345815658569)',
      'GMR.DefineUnstuck(-1110.9322509766, 6838.3764648438, 31.345815658569)',
      'GMR.DefineUnstuck(-1102.4758300781, 6836.130859375, 31.345815658569)',
      'GMR.DefineUnstuck(-1092.6187744141, 6834.6547851562, 31.345815658569)',
      'GMR.DefineUnstuck(-1083.9040527344, 6834.04296875, 31.345815658569)',
      'GMR.DefineUnstuck(-1075.3267822266, 6834.4829101562, 31.345815658569)',
      'GMR.DefineUnstuck(-1067.5977783203, 6835.3500976562, 31.345815658569)',
      'GMR.DefineUnstuck(-1059.1788330078, 6836.2944335938, 31.345815658569)',
      'GMR.DefineUnstuck(-1048.4089355469, 6837.5024414062, 31.345815658569)',
      'GMR.DefineUnstuck(-1038.2561035156, 6839.56640625, 31.345815658569)',
      'GMR.DefineUnstuck(-1029.1467285156, 6842.5517578125, 31.345815658569)',
      'GMR.DefineUnstuck(-1022.5050048828, 6847.2690429688, 31.345815658569)',
      'GMR.DefineUnstuck(-1013.4623413086, 6855.2490234375, 31.345815658569)',
      'GMR.DefineUnstuck(-1006.6320800781, 6858.9848632812, 31.345815658569)',
      'GMR.DefineUnstuck(-996.49200439453, 6862.0712890625, 31.672790527344)',
      'GMR.DefineUnstuck(-981.52770996094, 6865.7114257812, 31.960706710815)',
      'GMR.DefineUnstuck(-967.87005615234, 6865.1313476562, 31.980066299438)',
      'GMR.DefineUnstuck(-952.15466308594, 6860.5034179688, 32.266487121582)',
      'GMR.DefineUnstuck(-935.60876464844, 6862.3251953125, 31.971834182739)',
      'GMR.DefineUnstuck(-920.63220214844, 6867.3071289062, 31.966329574585)',
      'GMR.DefineUnstuck(-904.65673828125, 6876.091796875, 33.335536956787)',
      'GMR.DefineUnstuck(-883.47747802734, 6890.345703125, 32.967041015625)',    
      'GMR.DefineProfileCenter(-772.432983, 6944.549805, 32.341599, 25)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE A Rare Bean / Muck Diving / The Underneath /  |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9800, x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
      { questId = 9815, x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
      { questId = 9818, x = -792.767029, y = 6944.640137, z = 32.177399, id = 18071 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Underneath |r",
    QuestID = 9818,
    QuestType = "TalkTo",
    PickUp = { x = -792.767029, y = 6944.640137, z = 32.177399, id = 18071 },
    TurnIn = { x = -865.58783, y = 6979.794434, z = 35.165417, id = 18099 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
      if not GMR.Frames.Rand067 then
      GMR.Print("Frame created")
      GMR.Frames.Rand067 = CreateFrame("frame")
      GMR.Frames.Rand067:SetScript("OnUpdate", function(self)
        if GMR.GetProfileType() == "Questing" and GMR.IsQuestCompletable(9818) then
          if GMR.IsExecuting() and GMR.IsQuestActive(9818) then
            if QuestFrame:IsShown() then
              GossipTitleButton1:Click()
            end
          end
        else 
          self:SetScript("OnUpdate", nil); GMR.Frames.Rand067 = nil; GMR.Print("Frame deleted")
        end
      end)
    end
    ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-865.587830, 6979.794434, 35.165417, 25)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Tortured Earth |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9819, x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Grinding: Level 66 (Around the Ring of Trials) |r",
    QuestType = "GrindTo",
    Level = 66,
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
      if not GMR.Frames.Rand070 then
      GMR.Print("Frame created")
      GMR.Frames.Rand070 = CreateFrame("frame")
      GMR.Frames.Rand070:SetScript("OnUpdate", function(self)
        if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9861 then
          if GMR.IsExecuting() and GetItemCount(24504) >= 1 and not GMR.IsQuestActive(9861) then
            local itemName = GetItemInfo(24504)
            GMR.Use(itemName)
            if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() then
              QuestFrameAcceptButton:Click()
            end
          end
        else 
          self:SetScript("OnUpdate", nil); GMR.Frames.Rand070 = nil; GMR.Print("Frame deleted")
        end
      end)
    end
    ]],
      'GMR.DefineProfileCenter(-1429.361, 6591.828, 35.40303, 80)',
      'GMR.DefineProfileCenter(-1532.797, 6777.391, 13.77376, 80)',
      'GMR.DefineProfileCenter(-1585.703, 6869.497, -11.2667, 80)',
      'GMR.DefineProfileCenter(-1562.262, 7003.144, 0.8189923, 80)',
      'GMR.DefineProfileCenter(-1621.28, 7164.765, 2.886883, 80)',
      'GMR.DefineProfileCenter(-1651.351, 7392.816, -0.7571673, 80)',
      'GMR.DefineProfileCenter(-1759.905, 7522.404, -8.044071, 80)',
      'GMR.DefineProfileCenter(-1868.573, 7505.46, -7.685871, 80)',
      'GMR.DefineProfileCenter(-1897.23, 7339.741, -20.85601, 80)',
      'GMR.DefineProfileCenter(-2030.114, 7360.323, -32.95383, 80)',
      'GMR.DefineProfileCenter(-1635.145, 7106.332, 4.890052, 80)',
      'GMR.DefineProfileCenter(-1758.936, 6819.458, -28.50351, 80)',
      'GMR.DefineProfileCenter(-1721.51, 6678.866, -17.42623, 80)',
      'GMR.DefineProfileCenter(-1829.358, 6612.281, -2.68595, 80)',
      'GMR.DefineProfileCenter(-1865.269, 6478.015, 23.64894, 80)',
      'GMR.DefineProfileCenter(-1700.874, 6381.083, 42.27262, 120)',
      'GMR.DefineProfileCenter(-2087.211, 6429.951, 22.83161, 120)',
      'GMR.DefineProfileCenter(-2261.05, 6536.031, 8.711577, 80)',
      'GMR.DefineProfileCenter(-2343.52, 6648.872, 1.044387, 80)',
      'GMR.DefineProfileCenter(-2563.307, 6602.932, 4.366075, 80)',
      'GMR.DefineProfileCenter(-2694.205, 6562.12, 26.17357, 80)',
      'GMR.DefineProfileCenter(-2648.344, 6482.102, 28.55662, 80)',
      'GMR.DefineProfileCenter(-2578.549, 6424.449, 25.51998, 80)',
      'GMR.DefineProfileCenter(-2392.057, 6409.718, 16.66008, 80)',
      'GMR.DefineQuestEnemyId(17130)',
      'GMR.DefineQuestEnemyId(17158)',
      'GMR.DefineQuestEnemyId(18205)',
      'GMR.DefineQuestEnemyId(17128)',
      'GMR.DefineQuestEnemyId(18334)',
      'GMR.DefineQuestEnemyId(17132)',
      'GMR.DefineQuestEnemyId(17131)',
      'GMR.DefineQuestEnemyId(17159)',
      'GMR.BlacklistId(17141)',
      'GMR.BlacklistId(17139)',
      'GMR.BlacklistId(18489)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Rare Bean |r",
    QuestID = 9800,
    QuestType = "Gathering",
    PickUp = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    TurnIn = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-779.1695, 7120.506, 37.19664, 120)',
      'GMR.DefineProfileCenter(-749.5271, 7148.748, 41.20521, 120)',
      'GMR.DefineProfileCenter(-737.4833, 7197.45, 43.73923, 120)',
      'GMR.DefineProfileCenter(-783.6563, 7216.423, 36.0111, 120)',
      'GMR.DefineProfileCenter(-774.6091, 7297.262, 43.27713, 120)',
      'GMR.DefineProfileCenter(-826.779, 7318.851, 37.49517, 120)',
      'GMR.DefineProfileCenter(-897.655, 7351.825, 32.89663, 120)',
      'GMR.DefineProfileCenter(-904.6204, 7390.053, 33.55742, 120)',
      'GMR.DefineProfileCenter(-963.9168, 7377.697, 34.05997, 120)',
      'GMR.DefineProfileCenter(-989.8351, 7375.604, 34.58364, 120)',
      'GMR.DefineProfileCenter(-1075.948, 7381.365, 34.46489, 120)',
      'GMR.DefineProfileCenter(-1084.333, 7482.875, 33.29073, 120)',
      'GMR.DefineProfileCenter(-951.3693, 7507.022, 35.63064, 120)',
      'GMR.DefineProfileCenter(-790.0552, 7450.219, 54.41154, 120)',
      'GMR.DefineProfileCenter(-783.7583, 7216.703, 36.01326, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(182128)',
      'GMR.DefineCustomObjectId(182128)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Agitated Spirits of Skysong |r",
    QuestID = 9804,
    QuestType = "Custom",
    PickUp = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    TurnIn = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9804, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand038 then
        GMR.Print("Frame created")
        GMR.Frames.Rand038 = CreateFrame("frame")
        GMR.Frames.Rand038:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9804 then
            if GMR.IsExecuting() and not GMR.Questing.IsObjectiveCompleted(9804, 1) then
              local waterBreathingFromCherrry = GetSpellInfo(31920)
              local itemName = GetItemInfo(24421)
              if GetItemCount(24421) >= 1 and not GMR.HasBuff("player", waterBreathingFromCherrry) then
                GMR.Use(itemName)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand038 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DenySpeedUp()',
      'GMR.SetChecked("Mount", false)',
      'GMR.SetChecked("Looting", false)',
      'GMR.DefineProfileCenter(-957.6921, 6922.276, 31.39305, 120)',
      'GMR.DefineProfileCenter(-895.5426, 7019.707, 31.78151, 120)',
      'GMR.DefineProfileCenter(-1037.948, 7094.992, 30.88355, 120)',
      'GMR.DefineProfileCenter(-918.5526, 7280.242, 30.88574, 120)',
      'GMR.DefineProfileCenter(-1068.116, 7195.686, 18.33716, 120)',
      'GMR.DefineProfileCenter(-1099.485, 6914.663, 30.88472, 120)',
      'GMR.DefineProfileCenter(-1188.592, 6897.404, 18.6132, 120)',
      'GMR.DefineProfileCenter(-914.9445, 7010.217, 30.9803, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17153)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Blessings of Incineratus |r",
    QuestID = 9805,
    QuestType = "Custom",
    PickUp = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    TurnIn = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9805, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1809.318237, 6294.043945, 59.271889, 3) then 
          GMR.MeshTo(-1809.318237, 6294.043945, 59.271889)
        else
          local itemName = GetItemInfo(24467)
          GMR.Use(itemName)
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9805, 2) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1839.834106, 6389.762207, 52.898720, 3) then 
          GMR.MeshTo(-1839.834106, 6389.762207, 52.898720)
        else
          local itemName = GetItemInfo(24467)
          GMR.Use(itemName)
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9805, 3) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1920.002197, 6361.214844, 56.021667, 3) then 
          GMR.MeshTo(-1920.002197, 6361.214844, 56.021667)
        else
          local itemName = GetItemInfo(24467)
          GMR.Use(itemName)
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9805, 4) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-1974.910278, 6274.426758, 56.929790, 3) then 
          GMR.MeshTo(-1974.910278, 6274.426758, 56.929790)
        else
          local itemName = GetItemInfo(24467)
          GMR.Use(itemName)
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.SetChecked("Looting", true)',
      'GMR.DefineUnstuck(-957.84558105469, 6911.5737304688, 31.69122505188, 50)',
      'GMR.DefineUnstuck(-973.78442382812, 6904.8427734375, 31.211032867432)',
      'GMR.DefineUnstuck(-987.16857910156, 6899.21875, 31.180669784546)',
      'GMR.DefineUnstuck(-998.00286865234, 6891.0634765625, 31.180669784546)',
      'GMR.DefineUnstuck(-1012.9665527344, 6876.5922851562, 31.180669784546)',
      'GMR.DefineUnstuck(-1024.4678955078, 6866.1616210938, 31.180669784546)',
      'GMR.DefineUnstuck(-1036.3041992188, 6855.9809570312, 31.180669784546)',
      'GMR.DefineUnstuck(-1047.9934082031, 6843.833984375, 31.180669784546)',
      'GMR.DefineUnstuck(-1056.9833984375, 6839.1552734375, 31.180669784546)',
      'GMR.DefineUnstuck(-1070.6389160156, 6840.359375, 31.180669784546)',
      'GMR.DefineUnstuck(-1082.9104003906, 6841.8583984375, 31.180669784546)',
      'GMR.DefineUnstuck(-1095.3178710938, 6843.3745117188, 31.180669784546)',
      'GMR.DefineUnstuck(-1109.0736083984, 6845.279296875, 31.180669784546)',
      'GMR.DefineUnstuck(-1119.4451904297, 6848.3115234375, 31.180669784546)',
      'GMR.DefineUnstuck(-1137.3914794922, 6853.3491210938, 31.180669784546)',
      'GMR.DefineUnstuck(-1148.8645019531, 6854.3022460938, 31.180669784546)',
      'GMR.DefineUnstuck(-1173.1553955078, 6859.765625, 31.180669784546)',
      'GMR.DefineUnstuck(-1184.5524902344, 6864.560546875, 31.180669784546)',
      'GMR.DefineUnstuck(-1196.8431396484, 6866.8627929688, 31.180669784546)',
      'GMR.DefineUnstuck(-1206.3508300781, 6865.5190429688, 31.180669784546)',
      'GMR.DefineUnstuck(-1215.1363525391, 6857.5971679688, 31.180669784546)',
      'GMR.DefineUnstuck(-1218.2788085938, 6854.8564453125, 31.180669784546)',
      'GMR.DefineUnstuck(-1223.2689208984, 6851.6264648438, 33.299674987793)',
      'GMR.DefineUnstuck(-1230.4285888672, 6849.9306640625, 35.632949829102)',
      'GMR.DefineUnstuck(-1239.2126464844, 6849.9150390625, 36.685436248779)',
      'GMR.DefineUnstuck(-1251.8989257812, 6850.4067382812, 36.493419647217)',
      'GMR.DefineUnstuck(-1260.5191650391, 6846.8310546875, 36.232822418213)',
      'GMR.DefineUnstuck(-1268.7587890625, 6826.8344726562, 35.689014434814)',
      'GMR.DefineUnstuck(-1277.2352294922, 6806.7368164062, 32.986698150635)',
      'GMR.DefineUnstuck(-1282.7954101562, 6781.701171875, 34.898899078369)',
      'GMR.DefineUnstuck(-1288.9989013672, 6760.0424804688, 38.179447174072)',
      'GMR.DefineUnstuck(-1297.8453369141, 6739.2045898438, 39.653591156006)',
      'GMR.DefineUnstuck(-1302.9322509766, 6718.5532226562, 40.218994140625)',
      'GMR.DefineUnstuck(-1307.6374511719, 6697.7841796875, 40.619430541992)',
      'GMR.DefineUnstuck(-1318.0383300781, 6679.4731445312, 42.451366424561)',
      'GMR.DefineUnstuck(-1333.6697998047, 6662.541015625, 46.397674560547)',
      'GMR.DefineUnstuck(-1350.8607177734, 6647.353515625, 43.499240875244)',
      'GMR.DefineUnstuck(-1368.2864990234, 6632.3046875, 41.273258209229)',
      'GMR.DefineUnstuck(-1388.7924804688, 6609.3227539062, 41.262172698975)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Blessings of Incineratus |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9805, x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.SetChecked("Looting", true)',
      'GMR.DefineUnstuck(-1316.6378173828, 6828.693359375, 35.177291870117, 100)',
      'GMR.DefineUnstuck(-1298.8005371094, 6839.802734375, 33.769729614258)',
      'GMR.DefineUnstuck(-1277.8497314453, 6852.8515625, 34.137966156006)',
      'GMR.DefineUnstuck(-1260.7135009766, 6863.5239257812, 34.905986785889)',
      'GMR.DefineUnstuck(-1241.5163574219, 6869.7060546875, 33.546703338623)',
      'GMR.DefineUnstuck(-1229.1279296875, 6859.8388671875, 32.349624633789)',
      'GMR.DefineUnstuck(-1221.2547607422, 6850.046875, 33.149444580078)',
      'GMR.DefineUnstuck(-1215.73046875, 6848.6528320312, 31.901044845581)',
      'GMR.DefineUnstuck(-1208.1842041016, 6854.4248046875, 31.345815658569)',
      'GMR.DefineUnstuck(-1194.7478027344, 6864.9697265625, 31.345815658569)',
      'GMR.DefineUnstuck(-1188.2746582031, 6866.4448242188, 31.345815658569)',
      'GMR.DefineUnstuck(-1179.95703125, 6863.0766601562, 31.345815658569)',
      'GMR.DefineUnstuck(-1167.9831542969, 6854.3203125, 31.345815658569)',
      'GMR.DefineUnstuck(-1159.7309570312, 6851.5361328125, 31.345815658569)',
      'GMR.DefineUnstuck(-1151.5330810547, 6849.4780273438, 31.345815658569)',
      'GMR.DefineUnstuck(-1142.208984375, 6847.349609375, 31.345815658569)',
      'GMR.DefineUnstuck(-1135.015625, 6844.83984375, 31.345815658569)',
      'GMR.DefineUnstuck(-1126.7935791016, 6841.5913085938, 31.345815658569)',
      'GMR.DefineUnstuck(-1119.3957519531, 6839.9497070312, 31.345815658569)',
      'GMR.DefineUnstuck(-1110.9322509766, 6838.3764648438, 31.345815658569)',
      'GMR.DefineUnstuck(-1102.4758300781, 6836.130859375, 31.345815658569)',
      'GMR.DefineUnstuck(-1092.6187744141, 6834.6547851562, 31.345815658569)',
      'GMR.DefineUnstuck(-1083.9040527344, 6834.04296875, 31.345815658569)',
      'GMR.DefineUnstuck(-1075.3267822266, 6834.4829101562, 31.345815658569)',
      'GMR.DefineUnstuck(-1067.5977783203, 6835.3500976562, 31.345815658569)',
      'GMR.DefineUnstuck(-1059.1788330078, 6836.2944335938, 31.345815658569)',
      'GMR.DefineUnstuck(-1048.4089355469, 6837.5024414062, 31.345815658569)',
      'GMR.DefineUnstuck(-1038.2561035156, 6839.56640625, 31.345815658569)',
      'GMR.DefineUnstuck(-1029.1467285156, 6842.5517578125, 31.345815658569)',
      'GMR.DefineUnstuck(-1022.5050048828, 6847.2690429688, 31.345815658569)',
      'GMR.DefineUnstuck(-1013.4623413086, 6855.2490234375, 31.345815658569)',
      'GMR.DefineUnstuck(-1006.6320800781, 6858.9848632812, 31.345815658569)',
      'GMR.DefineUnstuck(-996.49200439453, 6862.0712890625, 31.672790527344)',
      'GMR.DefineUnstuck(-981.52770996094, 6865.7114257812, 31.960706710815)',
      'GMR.DefineUnstuck(-967.87005615234, 6865.1313476562, 31.980066299438)',
      'GMR.DefineUnstuck(-952.15466308594, 6860.5034179688, 32.266487121582)',
      'GMR.DefineUnstuck(-935.60876464844, 6862.3251953125, 31.971834182739)',
      'GMR.DefineUnstuck(-920.63220214844, 6867.3071289062, 31.966329574585)',
      'GMR.DefineUnstuck(-904.65673828125, 6876.091796875, 33.335536956787)',
      'GMR.DefineUnstuck(-883.47747802734, 6890.345703125, 32.967041015625)',  
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")'
    }
  })
  
  -- The Spirit Polluted
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Muck Diving |r",
    QuestID = 9815,
    QuestType = "Custom",
    PickUp = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    TurnIn = { x = -782.630005, y = 6936.529785, z = 32.4767, id = 18073 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9815, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
        if not GMR.Frames.Rand068 then
        GMR.Print("Frame created")
        GMR.Frames.Rand068 = CreateFrame("frame")
        GMR.Frames.Rand068:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9815 then
            if GMR.Questing.IsObjectiveCompleted(9815, 1) and GossipFrameGreetingPanel:IsShown() then
              GossipTitleButton3:Click()
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand068 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1488.265, 7819.156, -97.6593, 30)',
      'GMR.DefineProfileCenter(-1665.406, 7813.777, -103.1075, 30)',
      'GMR.DefineProfileCenter(-1744.435, 7904.725, -103.3071, 30)',
      'GMR.DefineProfileCenter(-1665.926, 8070.989, -103.3709, 30)',
      'GMR.DefineProfileCenter(-1582.058, 8095.898, -101.5796, 30)',
      'GMR.DefineProfileCenter(-1477.043, 8024.105, -103.9477, 30)',
      'GMR.DefineProfileCenter(-1384.929, 8092.663, -102.4033, 30)',
      'GMR.DefineProfileCenter(-1350.127, 8022.916, -103.6107, 30)',
      'GMR.DefineProfileCenter(-1442.543, 7879.875, -103.4078, 30)',
      'GMR.DefineProfileCenter(-1511.887, 7816.322, -100.8269, 30)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17154)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Talbuk Mastery Part 1 |r",
    QuestID = 9857,
    QuestType = "Custom",
    PickUp = { x = -1454.640015, y = 6352.279785, z = 37.323799, id = 18218 },
    TurnIn = { x = -1454.640015, y = 6352.279785, z = 37.323799, id = 18218 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9857, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-957.84558105469, 6911.5737304688, 31.69122505188, 90)',
      'GMR.DefineUnstuck(-973.78442382812, 6904.8427734375, 31.211032867432)',
      'GMR.DefineUnstuck(-987.16857910156, 6899.21875, 31.180669784546)',
      'GMR.DefineUnstuck(-998.00286865234, 6891.0634765625, 31.180669784546)',
      'GMR.DefineUnstuck(-1012.9665527344, 6876.5922851562, 31.180669784546)',
      'GMR.DefineUnstuck(-1024.4678955078, 6866.1616210938, 31.180669784546)',
      'GMR.DefineUnstuck(-1036.3041992188, 6855.9809570312, 31.180669784546)',
      'GMR.DefineUnstuck(-1047.9934082031, 6843.833984375, 31.180669784546)',
      'GMR.DefineUnstuck(-1056.9833984375, 6839.1552734375, 31.180669784546)',
      'GMR.DefineUnstuck(-1070.6389160156, 6840.359375, 31.180669784546)',
      'GMR.DefineUnstuck(-1082.9104003906, 6841.8583984375, 31.180669784546)',
      'GMR.DefineUnstuck(-1095.3178710938, 6843.3745117188, 31.180669784546)',
      'GMR.DefineUnstuck(-1109.0736083984, 6845.279296875, 31.180669784546)',
      'GMR.DefineUnstuck(-1119.4451904297, 6848.3115234375, 31.180669784546)',
      'GMR.DefineUnstuck(-1137.3914794922, 6853.3491210938, 31.180669784546)',
      'GMR.DefineUnstuck(-1148.8645019531, 6854.3022460938, 31.180669784546)',
      'GMR.DefineUnstuck(-1173.1553955078, 6859.765625, 31.180669784546)',
      'GMR.DefineUnstuck(-1184.5524902344, 6864.560546875, 31.180669784546)',
      'GMR.DefineUnstuck(-1196.8431396484, 6866.8627929688, 31.180669784546)',
      'GMR.DefineUnstuck(-1206.3508300781, 6865.5190429688, 31.180669784546)',
      'GMR.DefineUnstuck(-1215.1363525391, 6857.5971679688, 31.180669784546)',
      'GMR.DefineUnstuck(-1218.2788085938, 6854.8564453125, 31.180669784546)',
      'GMR.DefineUnstuck(-1223.2689208984, 6851.6264648438, 33.299674987793)',
      'GMR.DefineUnstuck(-1230.4285888672, 6849.9306640625, 35.632949829102)',
      'GMR.DefineUnstuck(-1239.2126464844, 6849.9150390625, 36.685436248779)',
      'GMR.DefineUnstuck(-1251.8989257812, 6850.4067382812, 36.493419647217)',
      'GMR.DefineUnstuck(-1260.5191650391, 6846.8310546875, 36.232822418213)',
      'GMR.DefineUnstuck(-1268.7587890625, 6826.8344726562, 35.689014434814)',
      'GMR.DefineUnstuck(-1277.2352294922, 6806.7368164062, 32.986698150635)',
      'GMR.DefineUnstuck(-1282.7954101562, 6781.701171875, 34.898899078369)',
      'GMR.DefineUnstuck(-1288.9989013672, 6760.0424804688, 38.179447174072)',
      'GMR.DefineUnstuck(-1297.8453369141, 6739.2045898438, 39.653591156006)',
      'GMR.DefineUnstuck(-1302.9322509766, 6718.5532226562, 40.218994140625)',
      'GMR.DefineUnstuck(-1307.6374511719, 6697.7841796875, 40.619430541992)',
      'GMR.DefineUnstuck(-1318.0383300781, 6679.4731445312, 42.451366424561)',
      'GMR.DefineUnstuck(-1333.6697998047, 6662.541015625, 46.397674560547)',
      'GMR.DefineUnstuck(-1350.8607177734, 6647.353515625, 43.499240875244)',
      'GMR.DefineUnstuck(-1368.2864990234, 6632.3046875, 41.273258209229)',
      'GMR.DefineUnstuck(-1388.7924804688, 6609.3227539062, 41.262172698975)',
      'GMR.DefineProfileCenter(-1420.267578, 6277.637207, 49.182907, 120)',
      'GMR.DefineProfileCenter(-1528.180176, 6812.600098, 5.785893, 120)',
      'GMR.DefineProfileCenter(-1506.380615, 6994.165527, 1.277812, 120)',
      'GMR.DefineProfileCenter(-1847.383545, 6824.973633, -38.407272, 120)',
      'GMR.DefineProfileCenter(-2002.916, 6449.141, 25.7015, 120)',
      'GMR.DefineProfileCenter(-2179.108, 6417.757, 18.49316, 120)',
      'GMR.DefineProfileCenter(-2321.336, 6317.555, 31.88615, 120)',
      'GMR.DefineProfileCenter(-2290.101074, 6587.216309, 3.360609, 120)',
      'GMR.DefineProfileCenter(-2249.467529, 6718.008301, -3.215649, 120)',
      'GMR.DefineProfileCenter(-2171.264160, 6948.765137, -12.056262, 120)',
      'GMR.DefineProfileCenter(-2350.835205, 7070.778809, -13.576412, 120)',
      'GMR.DefineProfileCenter(-2460.775879, 6675.786621, 3.123056, 120)',
      'GMR.DefineProfileCenter(-2381.282715, 6447.215332, 13.606682, 120)',
      'GMR.DefineProfileCenter(-2256.353760, 6680.537598, -3.714794, 120)',
      'GMR.BlacklistId(18489)',
      'GMR.DefineAreaBlacklist(-1307.3516845703, 7230.451171875, 35.376834869385, 250)',
      'GMR.DefineAreaBlacklist(-1440.4166259766, 7166.970703125, 38.230239868164, 150)',
      'GMR.DefineAreaBlacklist(-1345.8602294922, 7080.7880859375, 32.657543182373, 150)',
      'GMR.DefineAreaBlacklist(-1258.2010498047, 7024.78125, 34.515872955322, 150)',
      'GMR.DefineAreaBlacklist(-1322.6470947266, 7145.0590820312, 37.305263519287, 150)',
      'GMR.DefineAreaBlacklist(-1322.6470947266, 7145.0590820312, 37.305263519287, 150)',
      'GMR.DefineAreaBlacklist(-1322.6470947266, 7145.0590820312, 37.305263519287, 150)',
      'GMR.DefineAreaBlacklist(-1279.5494384766, 7266.724609375, 39.886413574219, 150)',
      'GMR.DefineAreaBlacklist(-1291.0895996094, 7367.939453125, 36.979976654053, 150)',
      'GMR.DefineAreaBlacklist(-1410.9422607422, 7274.8774414062, 25.503326416016, 150)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17130)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Clefthoof Mastery Part 1 |r",
    QuestID = 9789,
    QuestType = "Custom",
    PickUp = { x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
    TurnIn = { x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9789, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1504.838, 6283.292, 44.7704, 120)',
      'GMR.DefineProfileCenter(-1642.462, 6381.155, 31.29251, 120)',
      'GMR.DefineProfileCenter(-1763.957, 6640.588, -13.49881, 120)',
      'GMR.DefineProfileCenter(-1450.35, 6897.025, 7.638575, 120)',
      'GMR.DefineProfileCenter(-1668.348, 6892.327, -16.62292, 120)',
      'GMR.DefineProfileCenter(-1907.422, 6472.861, 26.02942, 120)',
      'GMR.DefineProfileCenter(-1673.589, 6418.266, 35.2528, 120)',
      'GMR.DefineProfileCenter(-2089.183, 6448.543, 19.30292, 120)',
      'GMR.DefineProfileCenter(-2186.898, 6316.8, 47.14403, 120)',
      'GMR.DefineProfileCenter(-2289.229, 6509.487, 11.59151, 120)',
      'GMR.DefineProfileCenter(-2299.827, 6658.629, -0.6772129, 120)',
      'GMR.DefineProfileCenter(-2353.027, 7015.909, -11.49926, 120)',
      'GMR.DefineProfileCenter(-2477.715, 7078.103, -10.81952, 120)',
      'GMR.DefineProfileCenter(-2312.724, 6653.055, 0.03855126, 120)',
      'GMR.DefineProfileCenter(-2574.26, 6610.789, 5.947642, 120)',
      'GMR.DefineProfileCenter(-2644.593, 6471.147, 28.34287, 120)',
      'GMR.DefineProfileCenter(-2224.633, 6351.2, 33.9952, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(18205)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Windroc Mastery Part 1 |r",
    QuestID = 9854,
    QuestType = "Custom",
    PickUp = { x = -1450.880005, y = 6341.370117, z = 37.398899, id = 18200 },
    TurnIn = { x = -1450.880005, y = 6341.370117, z = 37.398899, id = 18200 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9854, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1425.502, 6542.586, 34.4258, 120)',
      'GMR.DefineProfileCenter(-1306.644, 6806.491, 36.6082, 120)',
      'GMR.DefineProfileCenter(-1686.367, 6768.826, -10.35641, 120)',
      'GMR.DefineProfileCenter(-1420.267578, 6277.637207, 49.182907, 120)',
      'GMR.DefineProfileCenter(-1528.180176, 6812.600098, 5.785893, 120)',
      'GMR.DefineProfileCenter(-1506.380615, 6994.165527, 1.277812, 120)',
      'GMR.DefineProfileCenter(-1847.383545, 6824.973633, -38.407272, 120)',
      'GMR.DefineProfileCenter(-2002.916, 6449.141, 25.7015, 120)',
      'GMR.DefineProfileCenter(-2179.108, 6417.757, 18.49316, 120)',
      'GMR.DefineProfileCenter(-2321.336, 6317.555, 31.88615, 120)',
      'GMR.DefineProfileCenter(-2290.101074, 6587.216309, 3.360609, 120)',
      'GMR.DefineProfileCenter(-2249.467529, 6718.008301, -3.215649, 120)',
      'GMR.DefineProfileCenter(-2171.264160, 6948.765137, -12.056262, 120)',
      'GMR.DefineProfileCenter(-2350.835205, 7070.778809, -13.576412, 120)',
      'GMR.DefineProfileCenter(-2460.775879, 6675.786621, 3.123056, 120)',
      'GMR.DefineProfileCenter(-2381.282715, 6447.215332, 13.606682, 120)',
      'GMR.DefineProfileCenter(-2256.353760, 6680.537598, -3.714794, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17128)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Nagrand - Mastery Quests Part 2 |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9858, x = -1454.640015, y = 6352.279785, z = 37.323799, id = 18218 },
      { questId = 9850, x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
      { questId = 9855, x = -1450.880005, y = 6341.370117, z = 37.398899, id = 18200 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Talbuk Mastery Part 2 |r",
    QuestID = 9858,
    QuestType = "Custom",
    PickUp = { x = -1454.640015, y = 6352.279785, z = 37.323799, id = 18218 },
    TurnIn = { x = -1454.640015, y = 6352.279785, z = 37.323799, id = 18218 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9858, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1543.225, 7032.207, 4.750511, 120)',
      'GMR.DefineProfileCenter(-1639.681, 7158.083, 2.897733, 120)',
      'GMR.DefineProfileCenter(-1544.744, 7317.908, 2.697032, 120)',
      'GMR.DefineProfileCenter(-1483.885, 7427.261, 3.988211, 120)',
      'GMR.DefineProfileCenter(-1662.848, 7434.529, -5.218494, 120)',
      'GMR.DefineProfileCenter(-1876.721, 7396.169, -14.18575, 120)',
      'GMR.DefineProfileCenter(-1805.851, 7518.276, -4.582766, 120)',
      'GMR.DefineProfileCenter(-1671.065, 7610.144, -8.335993, 120)',
      'GMR.DefineProfileCenter(-1458.109, 7498.188, 3.237084, 120)',
      'GMR.DefineProfileCenter(-1161.756, 7647.345, 15.20294, 120)',
      'GMR.DefineProfileCenter(-974.1083, 7661.885, 33.81585, 120)',
      'GMR.DefineProfileCenter(-870.2684, 7407.728, 37.86253, 120)',
      'GMR.DefineProfileCenter(-1026.111, 7389.123, 36.40653, 120)',
      'GMR.DefineProfileCenter(-1036.764, 7365.994, 33.98855, 120)',
      'GMR.DefineProfileCenter(-1301.077, 7582.986, 7.242975, 120)',
      'GMR.DefineProfileCenter(-1480.292, 7449.204, 3.497086, 120)',
      'GMR.DefineProfileCenter(-1669.771, 7403.682, -3.089611, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17131)',
      'GMR.DefineQuestEnemyId(17132)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Clefthoof Mastery Part 2 |r",
    QuestID = 9850,
    QuestType = "Custom",
    PickUp = { x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
    TurnIn = { x = -1461.780029, y = 6344.299805, z = 37.273899, id = 18180 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9850, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1532.576, 7032.619, 6.242494, 120)',
      'GMR.DefineProfileCenter(-1601.344, 7158.359, 7.699172, 120)',
      'GMR.DefineProfileCenter(-1786.675, 7285.002, -8.954493, 120)',
      'GMR.DefineProfileCenter(-1967.79, 7358.446, -33.06135, 120)',
      'GMR.DefineProfileCenter(-1847.329, 7520.036, -6.637434, 120)',
      'GMR.DefineProfileCenter(-1428.186, 7508.593, 3.058138, 120)',
      'GMR.DefineProfileCenter(-1365.391, 7674.909, -3.225324, 120)',
      'GMR.DefineProfileCenter(-1091.039, 7768.639, 20.81655, 120)',
      'GMR.DefineProfileCenter(-958.0085, 7864.809, 39.62293, 120)',
      'GMR.DefineProfileCenter(-969.6696, 7831.23, 37.73372, 120)',
      'GMR.DefineProfileCenter(-1101.589, 7761.787, 19.70712, 120)',
      'GMR.DefineProfileCenter(-1069.911, 7508.219, 28.97816, 120)',
      'GMR.DefineProfileCenter(-1102.692, 7409.549, 32.78846, 120)',
      'GMR.DefineProfileCenter(-1355.932, 7579.174, 3.470371, 120)',
      'GMR.DefineProfileCenter(-1937.462, 7354.653, -27.33706, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17132)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Windroc Mastery Part 2 |r",
    QuestID = 9855,
    QuestType = "Custom",
    PickUp = { x = -1450.880005, y = 6341.370117, z = 37.398899, id = 18200 },
    TurnIn = { x = -1450.880005, y = 6341.370117, z = 37.398899, id = 18200 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9855, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2177.291, 7421.424, -34.01646, 120)',
      'GMR.DefineProfileCenter(-2167.02, 7498.843, -33.45203, 120)',
      'GMR.DefineProfileCenter(-2274.907, 7525.154, -11.98602, 120)',
      'GMR.DefineProfileCenter(-2159.378, 7688.301, -29.2113, 120)',
      'GMR.DefineProfileCenter(-2116.066, 7804.508, -20.01805, 120)',
      'GMR.DefineProfileCenter(-2028.509, 7908.896, -24.92805, 120)',
      'GMR.DefineProfileCenter(-2219.054, 7593.438, -20.08274, 120)',
      'GMR.DefineProfileCenter(-2270.632, 7535.772, -12.86956, 120)',
      'GMR.DefineProfileCenter(-2142.574, 7410.353, -33.39037, 120)',
      'GMR.DefineAreaBlacklist(-2297.144, 7415.558, -16.51208,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(18220)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE I Must Have Them |r",
    QuestID = 10109,
    QuestType = "Custom",
    PickUp = { x = -2428.169922, y = 6885.75, z = 4.1961, id = 19035 },
    TurnIn = { x = -2428.169922, y = 6885.75, z = 4.1961, id = 19035 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10109, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2271.969, 6941.226, -6.149992, 120)',
      'GMR.DefineProfileCenter(-2478.415, 7044.429, -6.404522, 120)',
      'GMR.DefineProfileCenter(-2776.405, 7026.689, -9.231313, 120)',
      'GMR.DefineProfileCenter(-2271.969, 6941.226, -6.149992, 120)',
      'GMR.DefineProfileCenter(-2280.407, 6509.271, 12.04979, 120)',
      'GMR.DefineProfileCenter(-2351.462, 6391.185, 25.08642, 120)',
      'GMR.DefineProfileCenter(-2143.241, 6283.107, 58.66661, 120)',
      'GMR.DefineProfileCenter(-2049.378, 6443.203, 25.29984, 120)',
      'GMR.DefineProfileCenter(-1818.804, 6608.967, -2.477396, 120)',
      'GMR.DefineProfileCenter(-1742.663, 6791.982, -20.03279, 120)',
      'GMR.DefineProfileCenter(-1790.525, 6980.943, -34.01535, 120)',
      'GMR.DefineProfileCenter(-1594.042, 7130.033, 6.969659, 120)',
      'GMR.DefineProfileCenter(-1370.073, 7556.914, 6.512423, 120)',
      'GMR.DefineProfileCenter(-1108.877, 7460.255, 29.50806, 120)',
      'GMR.DefineProfileCenter(-842.0035, 7346.816, 39.32826, 120)',
      'GMR.DefineAreaBlacklist(-2297.144, 7415.558, -16.51208,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17158)',
      'GMR.DefineQuestEnemyId(17159)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Bring Me the Egg! (MANUALLY Complete this. PRE-BIS Cloak!) |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10111, x = -2428.169922, y = 6885.75, z = 4.1961, id = 19035 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Tortured Earth |r",
    QuestID = 9819,
    QuestType = "Custom",
    PickUp = { x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 },
    TurnIn = { x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9819, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1338.103, 6603.411, 47.72727, 120)',
      'GMR.DefineProfileCenter(-1319.149, 6527.68, 67.3331, 120)',
      'GMR.DefineProfileCenter(-1536.532, 6331.591, 58.97576, 120)',
      'GMR.DefineProfileCenter(-1565.826, 6252.296, 70.58604, 120)',
      'GMR.DefineProfileCenter(-1630.624, 6245.641, 71.11691, 120)',
      'GMR.DefineProfileCenter(-1752.575, 6456.037, 44.53074, 120)',
      'GMR.DefineProfileCenter(-2076.041, 6403.473, 30.28071, 120)',
      'GMR.DefineProfileCenter(-2136.945, 6369.055, 50.54307, 120)',
      'GMR.DefineProfileCenter(-2467.223, 6682.398, 3.928613, 120)',
      'GMR.DefineProfileCenter(-2595.661, 6683.431, -0.05528382, 120)',
      'GMR.DefineProfileCenter(-2626.891, 6752.713, 0.6640762, 120)',
      'GMR.DefineProfileCenter(-2376.188, 6495.968, 28.57557, 120)',
      'GMR.DefineProfileCenter(-2044.228, 6358.469, 43.47907, 120)',
      'GMR.DefineProfileCenter(-1768.402, 6465.912, 40.76157, 120)',
      'GMR.DefineProfileCenter(-1554.952, 6316.8, 50.29421, 120)',
      'GMR.DefineProfileCenter(-1889.057, 6755.933, -66.33111, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17156)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Eating Damnation |r",
    QuestID = 9821,
    QuestType = "Custom",
    PickUp = { x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 },
    TurnIn = { x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9821, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-636.5695, 7159.422, 60.77795, 60)',
      'GMR.DefineProfileCenter(-680.7469, 7235.249, 62.13017, 60)',
      'GMR.DefineProfileCenter(-746.8832, 7300.467, 52.03811, 60)',
      'GMR.DefineProfileCenter(-666.6315, 7435.329, 74.47784, 60)',
      'GMR.DefineProfileCenter(-691.6931, 7524.501, 74.65585, 120)',
      'GMR.DefineProfileCenter(-652.7147, 7586.662, 102.8451, 120)',
      'GMR.DefineProfileCenter(-2021.73, 6964.968, -88.78839, 120)',
      'GMR.DefineProfileCenter(-1922.434, 7090.815, -92.86915, 120)',
      'GMR.DefineProfileCenter(-1898.042, 7167.685, -91.45884, 120)',
      'GMR.DefineProfileCenter(-2076.485, 7196.729, -112.6715, 120)',
      'GMR.DefineProfileCenter(-2397.035, 7381.294, -168.6461, 60)',
      'GMR.DefineProfileCenter(-2450.386, 7493.359, -9.04382, 60)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(18062)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Shattering the Veil |r",
    QuestID = 9849,
    QuestType = "Custom",
    PickUp = { x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 },
    TurnIn = { x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9849, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand040 then
        GMR.Print("Frame created")
        GMR.Frames.Rand040 = CreateFrame("frame")
        GMR.Frames.Rand040:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9849 then
            if GMR.IsExecuting() and GMR.IsQuestActive(9849) then
              if not GMR.Questing.IsObjectiveCompleted(9849, 1) then
                local npc1 = GMR.GetObjectWithInfo({ id = 17157, rawType = 5 })
                local npc2 = GMR.GetObjectWithInfo({ id = 18181, rawType = 5 })
                local itemName = GetItemInfo(24501)
                if UnitName("target") == "Shattered Rumbler" then
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand040 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-2084.86328125, 7091.6450195312, -2.1599838733673, 30)',
      'GMR.DefineUnstuck(-2104.1987304688, 7069.1528320312, -8.432186126709)',
      'GMR.DefineUnstuck(-2115.3564453125, 7052.39453125, -8.0296363830566)',
      'GMR.DefineUnstuck(-2126.1159667969, 7033.9379882812, -8.9893426895142)',
      'GMR.DefineUnstuck(-2137.7456054688, 7016.0869140625, -10.787483215332)',
      'GMR.DefineUnstuck(-2151.1662597656, 6996.037109375, -11.277011871338)',
      'GMR.DefineUnstuck(-2160.7963867188, 6977.6752929688, -11.157797813416)',
      'GMR.DefineUnstuck(-2165.3583984375, 6958.2900390625, -14.771615982056)',
      'GMR.DefineUnstuck(-2163.1552734375, 6936.6162109375, -13.330167770386)',
      'GMR.DefineUnstuck(-2161.2673339844, 6912.1928710938, -6.5479774475098)',
      'GMR.DefineUnstuck(-2160.9431152344, 6892.525390625, -5.8986558914185)',
      'GMR.DefineUnstuck(-2159.7416992188, 6868.8515625, -9.3271017074585)',
      'GMR.DefineUnstuck(-2157.2543945312, 6847.2172851562, -8.9423017501831)',
      'GMR.DefineUnstuck(-2155.275390625, 6822.236328125, -7.7550330162048)',
      'GMR.DefineUnstuck(-2152.2514648438, 6802.0161132812, -5.627073764801)',
      'GMR.DefineUnstuck(-2151.4523925781, 6779.7456054688, -3.9436008930206)',
      'GMR.DefineUnstuck(-2158.3583984375, 6760.5112304688, -3.8069503307343)',
      'GMR.DefineUnstuck(-2138.9370117188, 6744.8813476562, -4.2849011421204)',
      'GMR.DefineUnstuck(-2123.9743652344, 6738.5009765625, -3.0476231575012)',
      'GMR.DefineUnstuck(-2104.7292480469, 6739.0151367188, -3.007031917572)',
      'GMR.DefineUnstuck(-2086.7163085938, 6728.4897460938, 3.5920789241791)',
      'GMR.DefineUnstuck(-2070.4650878906, 6707.29296875, 11.94176864624)',
      'GMR.DefineUnstuck(-2059.4787597656, 6688.2749023438, 13.058800697327)',
      'GMR.DefineUnstuck(-2045.84765625, 6668.2431640625, 13.059482574463)',
      'GMR.DefineUnstuck(-2034.5992431641, 6646.2348632812, 13.053116798401)',
      'GMR.DefineUnstuck(-2024.8937988281, 6624.9184570312, 13.053884506226)',
      'GMR.DefineUnstuck(-2015.7163085938, 6604.7612304688, 12.306483268738)',
      'GMR.DefineUnstuck(-2006.3620605469, 6586.6416015625, 11.907034873962)',
      'GMR.DefineUnstuck(-1990.3525390625, 6572.4990234375, 10.667199134827)',
      'GMR.DefineUnstuck(-1974.7578125, 6557.4399414062, 11.118050575256)',
      'GMR.DefineUnstuck(-1958.1401367188, 6543.3369140625, 12.939060211182)',
      'GMR.DefineUnstuck(-1939.3715820312, 6531.7133789062, 15.015730857849)',
      'GMR.DefineUnstuck(-1920.8337402344, 6520.232421875, 17.238201141357)',
      'GMR.DefineUnstuck(-1901.4996337891, 6513.3627929688, 17.498336791992)',
      'GMR.DefineUnstuck(-1878.5056152344, 6513.7568359375, 18.403619766235)',
      'GMR.DefineUnstuck(-1858.0369873047, 6519.7934570312, 18.926561355591)',
      'GMR.DefineUnstuck(-1838.6779785156, 6527.12890625, 17.866937637329)',
      'GMR.DefineUnstuck(-1817.1923828125, 6533.0795898438, 17.134124755859)',
      'GMR.DefineUnstuck(-1796.6593017578, 6537.4833984375, 17.945579528809)',
      'GMR.DefineUnstuck(-1773.4295654297, 6542.4653320312, 18.585006713867)',
      'GMR.DefineProfileCenter(-2883.958, 6798.826, -38.46298, 80)',
      'GMR.DefineProfileCenter(-2925.843, 6834.145, -47.90835, 80)',
      'GMR.DefineProfileCenter(-2918.503, 6906.223, -38.26945, 80)',
      'GMR.DefineProfileCenter(-2943.037, 6950.058, -43.29736, 80)',
      'GMR.DefineProfileCenter(-2950.127, 7009.614, -41.08219, 80)',
      'GMR.DefineProfileCenter(-2909.184, 7053.088, -31.49191, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(18181)',
      'GMR.DefineQuestEnemyId(17157)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Gurok the Usurper (PRE-BIS/BIS Necklace! Do Manually!) |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9853, x = -812.87487792969, y = 6917.4892578125, z = 34.925029754639, id = 18099 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE HELP! |r",
    QuestID = 9923,
    QuestType = "Custom",
    PickUp = { x = -2563.889893, y = 6288.290039, z = 15.295, id = 18445 },
    TurnIn = { x = -2488.780029, y = 7230.660156, z = 16.320999, id = 18183 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9923, 1) then
        GMR.SetQuestingState(nil)
        if GetItemCount(25490) >= 1 then
          if not GMR.IsPlayerPosition(-2561.699, 6285.897, 15.11925, 3) then 
            GMR.MeshTo(-2561.699, 6285.897, 15.11925)
          else 
            local object = GMR.GetObjectWithInfo({ id = 182349, rawType = 8 })
            if object then
              GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
            end
          end
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand11124 then
        GMR.Print("Frame created")
        GMR.Frames.Rand11124 = CreateFrame("frame")
        GMR.Frames.Rand11124:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 9923 and not GMR.InCombat() then
            if GMR.IsExecuting() and not GMR.IsQuestActive(9923) and GMR.IsPlayerPosition(-2561.699, 6285.897, 15.11925, 35) then
              if QuestFrameDetailPanel:IsShown() then
                QuestFrameAcceptButton:Click()
              end
              local object = GMR.GetObjectWithInfo({ id = 18369, rawType = 5 })
              if object then
                GossipTitleButton1:Click()
                GMR.Questing.InteractWith(object, nil, nil, nil, nil, 3)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand11124 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2553.079, 6281.922, 14.60528, 150)',
      'GMR.DefineProfileCenter(-2471.445, 6264.321, 30.94912, 150)',
      'GMR.DefineProfileCenter(-2583.294, 6188.135, 21.14407, 150)',
      'GMR.DefineProfileCenter(-2510.466, 6176.848, 59.9393, 150)',
      'GMR.DefineProfileCenter(-2461.328, 6113.081, 89.64058, 150)',
      'GMR.DefineProfileCenter(-2359.482, 6156.528, 57.87469, 150)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2530.6628417969, 7354.0532226562, 9.2834606170654, 19012)',
      'GMR.DefineRepairVendor(-2530.6628417969, 7354.0532226562, 9.2834606170654, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineQuestEnemyId(17135)',
      'GMR.DefineQuestEnemyId(17134)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Corki's Gone Missing Again (Next 2 quests give you a BIS Item! Manual Completion Req.) |r",
    QuestID = 9924,
    QuestType = "Custom",
    PickUp = { x = -2488.780029, y = 7230.660156, z = 16.320999, id = 18183 },
    TurnIn = { x = -2488.780029, y = 7230.660156, z = 16.320999, id = 18183 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9924, 1) then
        GMR.SetQuestingState(nil)
        if GetItemCount(25509) >= 1 then
          if not GMR.IsPlayerPosition(-969.9119, 8126.845, -93.74339, 3) then 
            GMR.MeshTo(-969.9119, 8126.845, -93.74339)
          else 
            local object = GMR.GetObjectWithInfo({ id = 182350, rawType = 8 })
            if object then
              GossipTitleButton1:Click()
              GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
            end
          end
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-1047.413, 8098.228, -92.02065, 50)',
      'GMR.DefineProfileCenter(-1096.147, 8084.815, -87.28273, 50)',
      'GMR.DefineProfileCenter(-1147.079, 8037.168, -85.50981, 50)',
      'GMR.DefineProfileCenter(-1226.451, 8026.415, -93.73019, 50)',
      'GMR.DefineProfileCenter(-1235.401, 8034.168, -94.43332, 50)',
      'GMR.DefineProfileCenter(-1146.972, 8036.115, -85.37576, 50)',
      'GMR.DefineProfileCenter(-1091.512, 8068.234, -87.77105, 50)',
      'GMR.DefineProfileCenter(-1038.673, 8081.654, -93.10189, 50)',
      'GMR.DefineProfileCenter(-1100.8, 8110.913, -87.65846, 50)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineQuestEnemyId(17136)',
      'GMR.DefineQuestEnemyId(17137)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Ravaged Caravan (Caster Bracer, Melee Ring / Cloak) |r",
    QuestID = 9956,
    QuestType = "Gathering",
    PickUp = { x = -2604.96582, y = 7287.297363, z = 20.336363, id = 18416 },
    TurnIn = { x = -2604.96582, y = 7287.297363, z = 20.336363, id = 18416 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-1097.1188964844, 8695.8984375, 61.216041564941, 100)',
      'GMR.DefineProfileCenter(-1056.3804931641, 8760.5673828125, 86.4990234375, 100)',
      'GMR.DefineProfileCenter(-1074.8372802734, 8857.8115234375, 102.41965484619, 100)',
      'GMR.DefineProfileCenter(-1070.0528564453, 8944.8330078125, 101.05841064453, 100)',
      'GMR.DefineProfileCenter(-1104.3402099609, 8961.814453125, 104.67350769043, 100)',
      'GMR.DefineProfileCenter(-1039.8881835938, 9020.861328125, 94.544532775879, 100)',
      'GMR.DefineProfileCenter(-1040.5270996094, 9014.904296875, 93.564765930176, 60)',
      'GMR.DefineProfileCenter(-974.64312744141, 8930.8193359375, 102.82528686523, 100)',
      'GMR.DefineProfileCenter(-1022.1720581055, 8925.3505859375, 101.33786773682, 100)',
      'GMR.DefineProfileCenter(-1074.1802978516, 8856.75, 102.46434020996, 100)',
      'GMR.DefineProfileCenter(-1058.5280761719, 8877.8857421875, 122.3358001709, 100)',
      'GMR.DefineProfileCenter(-988.06701660156, 8861.5625, 142.27337646484, 100)',
      'GMR.DefineProfileCenter(-945.45111083984, 8891.9931640625, 148.21092224121, 100)',
      'GMR.DefineProfileCenter(-888.64190673828, 8939.599609375, 154.49256896973, 100)',
      'GMR.DefineProfileCenter(-814.39459228516, 8873.423828125, 183.13432312012, 100)',
      'GMR.DefineProfileCenter(-848.19256591797, 8856.4306640625, 185.5384979248, 100)',
      'GMR.DefineProfileCenter(-882.91064453125, 8895.08203125, 185.63206481934, 100)',
      'GMR.DefineProfileCenter(-816.87939453125, 8800.71484375, 185.50846862793, 100)',
      'GMR.DefineProfileCenter(-833.83142089844, 8774.974609375, 179.22595214844, 100)',
      'GMR.DefineProfileCenter(-818.474609375, 8750.4501953125, 180.10638427734, 100)',
      'GMR.DefineProfileCenter(-807.49694824219, 8728.142578125, 178.75337219238, 100)',
      'GMR.DefineProfileCenter(-836.90881347656, 8682.92578125, 180.33895874023, 100)',
      'GMR.DefineProfileCenter(-841.25604248047, 8723.3505859375, 177.2663269043, 100)',
      'GMR.DefineProfileCenter(-879.005859375, 8717.478515625, 174.55183410645, 100)',
      'GMR.DefineProfileCenter(-879.15704345703, 8694.0634765625, 172.5530090332, 100)',
      'GMR.DefineProfileCenter(-916.70062255859, 8690.1748046875, 173.42134094238, 100)',
      'GMR.DefineAreaBlacklist(-1031.2471923828, 8890.314453125, 115.77124023438,35)',
      'GMR.DefineAreaBlacklist(-1077.1956787109, 8983.9609375, 101.76161956787,35)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineCustomObjectId(182520)',
      'GMR.DefineCustomObjectId(181800)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Stopping the Spread |r",
    QuestID = 9874,
    QuestType = "Custom",
    PickUp = { x = -2613.370117, y = 7285.740234, z = 20.815001, id = 18222 },
    TurnIn = { x = -2613.370117, y = 7285.740234, z = 20.815001, id = 18222 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9874, 1) then
        GMR.SetQuestingState(nil);
        local object = GMR.GetObjectWithInfo({ id = 18240, rawType = 5, isAlive = false })
        local itemName = GetItemInfo(24560)
        if object then
          GMR.Use(itemName)
          GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-1417.724, 8541.23, 14.03838, 30)',
      'GMR.DefineProfileCenter(-1441.314, 8542.02, 13.23606, 30)',
      'GMR.DefineProfileCenter(-1500.247, 8563.777, 7.259974, 30)',
      'GMR.DefineProfileCenter(-1530.732, 8601.465, 7.259974, 30)',
      'GMR.DefineProfileCenter(-1528.583, 8614.77, 7.259974, 30)',
      'GMR.DefineProfileCenter(-1518.576, 8617.964, 7.259974, 30)',
      'GMR.DefineProfileCenter(-1504.94, 8606.152, 7.259974, 30)',
      'GMR.DefineProfileCenter(-1541.93, 8501.666, 0.6186859, 30)',
      'GMR.DefineProfileCenter(-1491.759, 8480.164, -3.983176, 30)',
      'GMR.DefineProfileCenter(-1456.694, 8450.362, -0.03034093, 30)',
      'GMR.DefineProfileCenter(-1443.866, 8429.44, -0.02933023, 30)',
      'GMR.DefineProfileCenter(-1468.452, 8427.965, 0.09403748, 30)',
      'GMR.DefineProfileCenter(-1577.633, 8462.652, -10.0882, 30)',
      'GMR.DefineProfileCenter(-1663.698, 8434.487, -23.19268, 30)',
      'GMR.DefineProfileCenter(-1665.629, 8464.189, -20.01911, 30)',
      'GMR.DefineProfileCenter(-1624.086, 8510.609, -13.39372, 120)',
      'GMR.DefineAreaBlacklist(-1518.2631835938, 8467.7919921875, 3.4463467597961,30)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Solving the Problem |r",
    QuestID = 9878,
    QuestType = "Custom",
    PickUp = { x = -2621.600098, y = 7286.529785, z = 20.880301, id = 18224 },
    TurnIn = { x = -2621.600098, y = 7286.529785, z = 20.880301, id = 18224 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9878, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18207, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9878, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18203, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(-1425.983, 8536.916, 13.95975, 120)',
      'GMR.DefineProfileCenter(-1513.852, 8447.851, -4.066858, 120)',
      'GMR.DefineProfileCenter(-1617.031, 8451.27, -15.49094, 120)',
      'GMR.DefineProfileCenter(-1659.06, 8471.397, -18.88197, 120)',
      'GMR.DefineProfileCenter(-1632.313, 8517.923, -13.39508, 120)',
      'GMR.DefineProfileCenter(-1610.166, 8576.537, -7.493187, 120)',
      'GMR.DefineProfileCenter(-1481.656, 8565.169, 7.260244, 120)',
      'GMR.DefineProfileCenter(-1616.287, 8520.732, -13.39331, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineQuestEnemyId(18203)',
      'GMR.DefineQuestEnemyId(18207)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Stopping the Spread / Solving the Problem |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 9956, x = -2604.96582, y = 7287.297363, z = 20.336363, id = 18416 },
      { questId = 9874, x = -2613.370117, y = 7285.740234, z = 20.815001, id = 18222 },
      { questId = 9878, x = -2621.600098, y = 7286.529785, z = 20.880301, id = 18224 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Do My Eyes Deceive Me |r",
    QuestID = 9917,
    QuestType = "Custom",
    PickUp = { x = -2556.340088, y = 7254.189941, z = 15.2034, id = 18353 },
    TurnIn = { x = -2556.340088, y = 7254.189941, z = 15.2034, id = 18353 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9917, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2629.085, 6864.987, 3.725688, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineQuestEnemyId(18352)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Not On My Watch! |r",
    QuestID = 9918,
    QuestType = "Custom",
    PickUp = { x = -2556.340088, y = 7254.189941, z = 15.2034, id = 18353 },
    TurnIn = { x = -2556.340088, y = 7254.189941, z = 15.2034, id = 18353 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9918, 1) then
        if not GMR.IsPlayerPosition(-2589.833, 6833.116, 33.46893, 25) then 
          GMR.MeshTo(-2589.833, 6833.116, 33.46893)
        else 
          local object = GMR.GetObjectWithInfo({ id = 18351, rawType = 5 })
          if object then
            GossipTitleButton1:Click()
            GMR.Questing.GossipWith(object, nil, nil, nil, nil, 5)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2589.833, 6833.116, 33.46893, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineCustomObjectId(18351)',
      'GMR.DefineQuestEnemyId(18351)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Mo'mor the Breaker |r",
    QuestID = 9920,
    QuestType = "TalkTo",
    PickUp = { x = -2556.340088, y = 7254.189941, z = 15.2034, id = 18353 },
    TurnIn = { x = -2618.179932, y = 7278.459961, z = 20.807699, id = 18223 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2589.833, 6833.116, 33.46893, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineCustomObjectId(18351)',
      'GMR.DefineQuestEnemyId(18351)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Ruins of Burning Blade |r",
    QuestID = 9921,
    QuestType = "Custom",
    PickUp = { x = -2618.179932, y = 7278.459961, z = 20.807699, id = 18223 },
    TurnIn = { x = -2618.179932, y = 7278.459961, z = 20.807699, id = 18223 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9921, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 17134, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9921, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 17135, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-2534.938, 6283.211, 15.12494, 50)',
      'GMR.DefineProfileCenter(-2569.143, 6231.627, 17.38416, 50)',
      'GMR.DefineProfileCenter(-2579.135, 6181.795, 21.0755, 50)',
      'GMR.DefineProfileCenter(-2466.324, 6266.53, 31.01806, 50)',
      'GMR.DefineProfileCenter(-2513.986, 6172.593, 59.9392, 50)',
      'GMR.DefineProfileCenter(-2485.597, 6110.187, 92.10863, 50)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-2402.7155761719, 6144.0419921875, 72.334327697754, 20)',
      'GMR.DefineUnstuck(-2411.8955078125, 6136.9868164062, 75.009811401367)',
      'GMR.DefineUnstuck(-2423.4289550781, 6128.123046875, 78.994697570801)',
      'GMR.DefineUnstuck(-2433.5581054688, 6122.6782226562, 83.082405090332)',
      'GMR.DefineUnstuck(-2444.009765625, 6120.5649414062, 84.392066955566)',
      'GMR.DefineUnstuck(-2451.1857910156, 6124.7963867188, 83.939018249512)',
      'GMR.DefineUnstuck(-2450.1506347656, 6134.0893554688, 79.621719360352)',
      'GMR.DefineUnstuck(-2455.1911621094, 6142.134765625, 74.542289733887)',
      'GMR.DefineUnstuck(-2465.3068847656, 6148.74609375, 68.882888793945)',
      'GMR.DefineUnstuck(-2474.1831054688, 6153.2607421875, 63.503574371338)',
      'GMR.DefineUnstuck(-2482.0358886719, 6159.4609375, 58.816974639893)',
      'GMR.DefineUnstuck(-2486.361328125, 6165.7783203125, 59.018653869629)',
      'GMR.DefineUnstuck(-2483.9760742188, 6172.6015625, 59.755294799805)',
      'GMR.DefineUnstuck(-2472.8952636719, 6174.3911132812, 59.398483276367)',
      'GMR.DefineUnstuck(-2460.8530273438, 6169.1850585938, 57.168823242188)',
      'GMR.DefineUnstuck(-2449.6284179688, 6166.7353515625, 53.583213806152)',
      'GMR.DefineUnstuck(-2443.6584472656, 6172.1704101562, 51.222927093506)',
      'GMR.DefineUnstuck(-2448.857421875, 6185.7783203125, 40.50260925293)',
      'GMR.DefineUnstuck(-2456.9377441406, 6196.623046875, 33.401279449463)',
      'GMR.DefineUnstuck(-2461.771484375, 6208.2236328125, 30.547855377197)',
      'GMR.DefineUnstuck(-2465.7553710938, 6218.5859375, 30.047719955444)',
      'GMR.DefineUnstuck(-2470.1760253906, 6230.0854492188, 30.325616836548)',
      'GMR.DefineUnstuck(-2475.1450195312, 6239.5615234375, 30.983085632324)',
      'GMR.DefineUnstuck(-2482.0715332031, 6249.8344726562, 31.31502532959)',
      'GMR.DefineUnstuck(-2487.0256347656, 6257.1826171875, 30.814002990723)',
      'GMR.DefineUnstuck(-2494.9802246094, 6265.0854492188, 28.282678604126)',
      'GMR.DefineUnstuck(-2503.1560058594, 6264.15234375, 26.741092681885)',
      'GMR.DefineUnstuck(-2517.3852539062, 6258.404296875, 22.189018249512)',
      'GMR.DefineUnstuck(-2527.7055664062, 6259.2622070312, 18.873268127441)',
      'GMR.DefineUnstuck(-2531.8549804688, 6267.59375, 17.697546005249)',
      'GMR.DefineUnstuck(-2529.4719238281, 6275.1879882812, 17.302909851074)',
      'GMR.DefineUnstuck(-2523.2578125, 6284.62109375, 18.496912002563)',
      'GMR.DefineUnstuck(-2514.1579589844, 6297.2358398438, 20.326623916626)',
      'GMR.DefineUnstuck(-2505.0190429688, 6309.2045898438, 21.138648986816)',
      'GMR.DefineUnstuck(-2492.9165039062, 6321.7841796875, 22.198091506958)',
      'GMR.DefineUnstuck(-2479.482421875, 6333.5590820312, 24.379072189331)',
      'GMR.DefineUnstuck(-2466.1384277344, 6343.2978515625, 27.954887390137)',
      'GMR.DefineUnstuck(-2448.8884277344, 6347.0537109375, 29.815814971924)',
      'GMR.DefineUnstuck(-2427.4909667969, 6349.9155273438, 29.15488243103)',
      'GMR.DefineUnstuck(-2405.9460449219, 6355.5532226562, 28.688188552856)',
      'GMR.DefineUnstuck(-2387.4860839844, 6360.6547851562, 28.40020942688)',
      'GMR.DefineUnstuck(-2368.640625, 6367.6279296875, 28.183582305908)',
      'GMR.DefineUnstuck(-2350.232421875, 6376.5439453125, 27.527942657471)',
      'GMR.DefineUnstuck(-2332.3251953125, 6387.44140625, 25.46551322937)',
      'GMR.DefineUnstuck(-2316.0478515625, 6400.154296875, 23.629539489746)',
      'GMR.DefineUnstuck(-2300.1452636719, 6413.8466796875, 22.249601364136)',
      'GMR.DefineUnstuck(-2282.83203125, 6427.9711914062, 20.947914123535)',
      'GMR.DefineUnstuck(-2266.1481933594, 6441.5830078125, 19.447200775146)',
      'GMR.DefineUnstuck(-2248.4118652344, 6456.052734375, 18.668241500854)',
      'GMR.DefineUnstuck(-2228.9064941406, 6468.4848632812, 17.509063720703)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-2566.8720703125, 6140.8676757812, 33.521472930908, 10)',
      'GMR.DefineUnstuck(-2577.1420898438, 6153.78125, 23.222688674927)',
      'GMR.DefineUnstuck(-2574.4709472656, 6164.4228515625, 22.062097549438)',
      'GMR.DefineUnstuck(-2576.8034667969, 6182.248046875, 21.009986877441)',
      'GMR.DefineUnstuck(-2582.8103027344, 6201.0659179688, 20.156328201294)',
      'GMR.DefineUnstuck(-2579.6306152344, 6216.3984375, 18.360677719116)',
      'GMR.DefineUnstuck(-2573.9704589844, 6232.1362304688, 17.817632675171)',
      'GMR.DefineUnstuck(-2569.3117675781, 6247.7954101562, 18.508815765381)',
      'GMR.DefineUnstuck(-2560.8032226562, 6258.0551757812, 17.959928512573)',
      'GMR.DefineUnstuck(-2549.0607910156, 6267.7353515625, 15.301821708679)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineQuestEnemyId(17134)',
      'GMR.DefineQuestEnemyId(17135)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE The Twin Clefts of Nagrand (Tank Shield / Druid Staff / Plate Bracers) |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 9922, x = -2618.179932, y = 7278.459961, z = 20.807699, id = 18223 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Twin Clefts of Nagrand |r",
    QuestID = 9922,
    QuestType = "Custom",
    PickUp = { x = -2618.179932, y = 7278.459961, z = 20.807699, id = 18223 },
    TurnIn = { x = -2618.179932, y = 7278.459961, z = 20.807699, id = 18223 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9907, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 17136, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9907, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 17137, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineProfileCenter(-1246.187, 8025.359, -94.83561, 80)',
      'GMR.DefineProfileCenter(-1186.195, 8035.278, -93.10305, 80)',
      'GMR.DefineProfileCenter(-1107.905, 8045.499, -83.60225, 80)',
      'GMR.DefineProfileCenter(-1096.573, 8113.59, -88.37022, 80)',
      'GMR.DefineProfileCenter(-1051.676, 8108.02, -89.92238, 80)',
      'GMR.DefineProfileCenter(-1040.027, 8080.093, -92.75692, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17137)',
      'GMR.DefineQuestEnemyId(17136)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Fierce Enemies |r",
    QuestID = 10476,
    QuestType = "Custom",
    PickUp = { x = -2569.780029, y = 7271.870117, z = 15.4773, id = 18408 },
    TurnIn = { x = -2569.780029, y = 7271.870117, z = 15.4773, id = 18408 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10476, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineBlacklistItem("Obsidian Warbeads")',
      'GMR.DefineProfileCenter(-1246.187, 8025.359, -94.83561, 80)',
      'GMR.DefineProfileCenter(-1186.195, 8035.278, -93.10305, 80)',
      'GMR.DefineProfileCenter(-1107.905, 8045.499, -83.60225, 80)',
      'GMR.DefineProfileCenter(-1096.573, 8113.59, -88.37022, 80)',
      'GMR.DefineProfileCenter(-1051.676, 8108.02, -89.92238, 80)',
      'GMR.DefineProfileCenter(-1040.027, 8080.093, -92.75692, 80)',
      'GMR.DefineProfileCenter(-1196.232, 8029.674, -93.88453, 80)',
      'GMR.DefineProfileCenter(-1956.558, 7621.436, -91.54218, 80)',
      'GMR.DefineProfileCenter(-1977.822, 7569.164, -91.86027, 80)',
      'GMR.DefineProfileCenter(-2043.664, 7539.93, -91.80052, 80)',
      'GMR.DefineProfileCenter(-2010.098, 7437.795, -93.32138, 80)',
      'GMR.DefineProfileCenter(-2042.101, 7439.557, -99.572, 80)',
      'GMR.DefineProfileCenter(-2072.416, 7386.307, -105.0262, 80)',
      'GMR.DefineProfileCenter(-2070.198, 7452.997, -101.4397, 80)',
      'GMR.DefineProfileCenter(-2025.964, 7577.35, -91.18821, 80)',
      'GMR.DefineProfileCenter(-1995.992, 7604.142, -89.91391, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17137)',
      'GMR.DefineQuestEnemyId(17136)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE WANTED: Zorobo the Advisor |r",
    QuestID = 9940,
    QuestType = "Custom",
    PickUp = { x = -2563.839, y = 7274.782, z = 15.48017, id = 182393 },
    TurnIn = { x = -2569.780029, y = 7271.870117, z = 15.4773, id = 18408 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(9940, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(-643.667053, 7736.977051, 85.035904, 25) then 
          GMR.MeshTo(-643.667053, 7736.977051, 85.035904)
        else 
          local object = GMR.GetObjectWithInfo({ id = 18413, rawType = 5 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end 
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9940, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18064, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(9940, 3) then
        local enemy = GMR.GetObjectWithInfo({ id = 17138, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(-798.6829, 7799.243, 36.02028, 80)',
      'GMR.DefineProfileCenter(-832.2325, 7721.354, 34.91744, 80)',
      'GMR.DefineProfileCenter(-782.6651, 7582.529, 52.11555, 80)',
      'GMR.DefineProfileCenter(-827.491, 7699.067, 38.0503, 80)',
      'GMR.DefineProfileCenter(-768.4526, 7849.579, 46.11158, 80)',
      'GMR.DefineProfileCenter(-790.1309, 7932.257, 62.06879, 80)',
      'GMR.DefineProfileCenter(-671.4139, 7748.877, 84.60271, 80)',
      'GMR.DefineProfileCenter(-781.8985, 7788.393, 39.26274, 80)',
      'GMR.DefineAreaBlacklist(-708.9102, 7877.038, 45.17037,15)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(17138)',
      'GMR.DefineQuestEnemyId(18064)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Grinding - Level 68 (By Telaar) |r",
    QuestType = "GrindTo",
    Level = 68,
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineProfileCenter(-2419.267, 7045.136, -7.641199, 100)',
      'GMR.DefineProfileCenter(-2573.363, 7030.855, -4.470068, 100)',
      'GMR.DefineProfileCenter(-2726.59, 6988.891, -6.253442, 100)',
      'GMR.DefineProfileCenter(-2866.019, 7113.78, -8.425311, 100)',
      'GMR.DefineProfileCenter(-2827.52, 7053.946, -13.68875, 100)',
      'GMR.DefineProfileCenter(-2749.172, 6940.525, -10.89431, 100)',
      'GMR.DefineProfileCenter(-2644.34, 6901.118, -1.533603, 100)',
      'GMR.DefineProfileCenter(-2721.888, 6750.134, -1.412069, 100)',
      'GMR.DefineProfileCenter(-2697.453, 6572.35, 24.6473, 100)',
      'GMR.DefineProfileCenter(-2530.878, 6608.521, 2.022742, 100)',
      'GMR.DefineProfileCenter(-2344.634, 6584.458, 2.999377, 100)',
      'GMR.DefineProfileCenter(-2293.374, 6697.678, -1.611979, 100)',
      'GMR.DefineProfileCenter(-2194.225, 6874.441, -6.361836, 100)',
      'GMR.DefineProfileCenter(-2295.004, 6946.894, -5.775948, 100)',
      'GMR.DefineProfileCenter(-2417.589, 7034.582, -7.731668, 100)',
      'GMR.DefineProfileCenter(-2577.304, 7039.873, -5.472426, 100)',
      'GMR.DefineProfileCenter(-2705.805, 7049.581, -7.502296, 100)',
      'GMR.BlacklistId(17156)',
      'GMR.BlacklistId(17147)',
      'GMR.BlacklistId(18033)',
      'GMR.BlacklistId(17148)',
      'GMR.BlacklistId(17146)',
      'GMR.BlacklistId(17135)',
      'GMR.BlacklistId(17134)',
      'GMR.BlacklistId(17141)',
      'GMR.BlacklistId(17139)',
      'GMR.DefineQuestEnemyId(17131)',
      'GMR.DefineQuestEnemyId(17132)',
      'GMR.DefineQuestEnemyId(17159)',
      'GMR.DefineQuestEnemyId(18205)',
      'GMR.DefineQuestEnemyId(17130)',
      'GMR.DefineQuestEnemyId(17158)',
      'GMR.DefineQuestEnemyId(18352)',
      -- Telaar Vendors _ NAGRAND -- MASTERY Repair / Sell Vendor..
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineBlacklistItem("Mote of FIre")',
      'GMR.DefineBlacklistItem("Mote of Air")',
      'GMR.DefineBlacklistItem("Mote of Water")',
      'GMR.DefineBlacklistItem("Mote of Earth")',
      'GMR.DefineBlacklistItem("Mote of Shadow")',
      'GMR.DefineBlacklistItem("Mote of Life")',
      'GMR.DefineBlacklistItem("Mote of Mana")',
      'GMR.DefineBlacklistItem("Primal Air")',
      'GMR.DefineBlacklistItem("Primal Earth")',
      'GMR.DefineBlacklistItem("Primal Fire")',
      'GMR.DefineBlacklistItem("Primal Life")',
      'GMR.DefineBlacklistItem("Primal Mana")',
      'GMR.DefineBlacklistItem("Primal Shadow")',
      'GMR.DefineBlacklistItem("Primal Water")',
      'GMR.DefineBlacklistItem("Knothide Leather")'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Killing the Crawlers |r",
    QuestID = 10927,
    QuestType = "Custom",
    PickUp = { x = 1113.339966, y = 7098.970215, z = 122.612, id = 22488 },
    TurnIn = { x = 2014.719971, y = 6881.02002, z = 179.016006, id = 21158 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10927, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand081 then
        GMR.Print("Frame created")
        GMR.Frames.Rand081 = CreateFrame("frame")
        GMR.Frames.Rand081:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10927 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10927) and GMR.Questing.IsObjectiveCompleted(10927, 1) then
              GossipTitleButton3:Click()
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand081 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(932.64752197266, 7053.3666992188, 18.817440032959, 80)',
      'GMR.DefineUnstuck(924.61871337891, 7081.1362304688, 19.911615371704)',
      'GMR.DefineUnstuck(920.91772460938, 7105.1567382812, 18.231462478638)',
      'GMR.DefineUnstuck(920.61248779297, 7121.8745117188, 17.789844512939)',
      'GMR.DefineUnstuck(926.18646240234, 7133.3862304688, 17.517589569092)',
      'GMR.DefineUnstuck(938.81475830078, 7149.1904296875, 17.717668533325)',
      'GMR.DefineUnstuck(945.56170654297, 7163.0859375, 17.4996509552)',
      'GMR.DefineUnstuck(946.38702392578, 7177.1206054688, 17.477685928345)',
      'GMR.DefineUnstuck(943.01617431641, 7198.6611328125, 17.47279548645)',
      'GMR.DefineUnstuck(938.46014404297, 7216.5317382812, 17.511793136597)',
      'GMR.DefineUnstuck(930.87017822266, 7236.5395507812, 17.752244949341)',
      'GMR.DefineUnstuck(926.70288085938, 7255.765625, 17.402082443237)',
      'GMR.DefineUnstuck(923.31713867188, 7273.5512695312, 17.566509246826)',
      'GMR.DefineUnstuck(917.36883544922, 7290.15234375, 17.640226364136)',
      'GMR.DefineUnstuck(913.85443115234, 7303.8662109375, 17.519512176514)',
      'GMR.DefineUnstuck(912.15209960938, 7323.40625, 17.783802032471)',
      'GMR.DefineUnstuck(912.65356445312, 7340.6015625, 17.940250396729)',
      'GMR.DefineUnstuck(916.13391113281, 7355.3120117188, 18.463186264038)',
      'GMR.DefineUnstuck(922.16497802734, 7371.7890625, 19.880012512207)',
      'GMR.DefineUnstuck(938.35760498047, 7383.5205078125, 22.03434753418)',
      'GMR.DefineUnstuck(950.74548339844, 7382.0302734375, 26.394887924194)',
      'GMR.DefineUnstuck(975.53698730469, 7375.9677734375, 30.270292282104)',
      'GMR.DefineUnstuck(997.93505859375, 7370.4912109375, 36.109050750732)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(-725.93914794922, 7171.4619140625, 48.213863372803, 150)',
      'GMR.DefineUnstuck(-740.70806884766, 7186.29296875, 45.381084442139)',
      'GMR.DefineUnstuck(-753.8857421875, 7202.1552734375, 40.635147094727)',
      'GMR.DefineUnstuck(-765.34423828125, 7219.142578125, 40.344200134277)',
      'GMR.DefineUnstuck(-771.60455322266, 7238.9956054688, 41.46036529541)',
      'GMR.DefineUnstuck(-779.43859863281, 7257.4365234375, 39.145629882812)',
      'GMR.DefineUnstuck(-789.62420654297, 7276.724609375, 40.740726470947)',
      'GMR.DefineUnstuck(-800.39166259766, 7297.1137695312, 40.99006652832)',
      'GMR.DefineUnstuck(-813.91845703125, 7313.5258789062, 37.769672393799)',
      'GMR.DefineUnstuck(-828.02856445312, 7328.2563476562, 39.27379989624)',
      'GMR.DefineUnstuck(-843.69781494141, 7344.6142578125, 38.919910430908)',
      'GMR.DefineUnstuck(-858.02850341797, 7357.5483398438, 37.318981170654)',
      'GMR.DefineUnstuck(-873.60552978516, 7368.9658203125, 35.719360351562)',
      'GMR.DefineUnstuck(-887.47955322266, 7377.1616210938, 34.482616424561)',
      'GMR.DefineUnstuck(-903.390625, 7386.5610351562, 33.363506317139)',
      'GMR.DefineUnstuck(-917.21643066406, 7394.728515625, 34.374759674072)',
      'GMR.DefineUnstuck(-939.20269775391, 7407.716796875, 35.698616027832)',
      'GMR.DefineUnstuck(-961.86785888672, 7421.10546875, 37.66862487793)',
      'GMR.DefineUnstuck(-978.50372314453, 7440.2451171875, 35.977378845215)',
      'GMR.DefineUnstuck(-997.39093017578, 7456.0576171875, 33.967456817627)',
      'GMR.DefineUnstuck(-1012.9488525391, 7472.2685546875, 33.593402862549)',
      'GMR.DefineUnstuck(-1023.785949707, 7494.3203125, 31.350933074951)',
      'GMR.DefineUnstuck(-1032.8218994141, 7516.8081054688, 31.429798126221)',
      'GMR.DefineUnstuck(-1039.0101318359, 7523.7104492188, 31.634769439697)',
      'GMR.DefineUnstuck(-1052.970703125, 7531.5756835938, 31.071582794189)',
      'GMR.DefineUnstuck(-1064.1865234375, 7541.2880859375, 30.188899993896)',
      'GMR.DefineUnstuck(-1067.8154296875, 7549.9760742188, 29.572311401367)',
      'GMR.DefineUnstuck(-1071.0718994141, 7569.25390625, 27.86873626709)',
      'GMR.DefineUnstuck(-1073.9224853516, 7587.4931640625, 29.090198516846)',
      'GMR.DefineUnstuck(-1077.6555175781, 7604.791015625, 29.172971725464)',
      'GMR.DefineUnstuck(-1078.1405029297, 7619.9057617188, 27.674814224243)',
      'GMR.DefineUnstuck(-1080.2286376953, 7631.9487304688, 25.330703735352)',
      'GMR.DefineUnstuck(-1084.6838378906, 7647.390625, 23.701879501343)',
      'GMR.DefineUnstuck(-1084.0050048828, 7657.6977539062, 23.89109992981)',
      'GMR.DefineUnstuck(-1078.9215087891, 7669.41015625, 24.790874481201)',
      'GMR.DefineUnstuck(-1071.2182617188, 7687.158203125, 24.136350631714)',
      'GMR.DefineUnstuck(-1067.7333984375, 7700.5859375, 25.250221252441)',
      'GMR.DefineUnstuck(-1069.6593017578, 7716.7153320312, 24.51958656311)',
      'GMR.DefineUnstuck(-1074.1903076172, 7726.3618164062, 22.693674087524)',
      'GMR.DefineUnstuck(-1077.8089599609, 7739.37109375, 22.583793640137)',
      'GMR.DefineUnstuck(-1078.3419189453, 7754.068359375, 23.10765838623)',
      'GMR.DefineUnstuck(-1075.3045654297, 7765.951171875, 22.837409973145)',
      'GMR.DefineUnstuck(-1071.3787841797, 7782.6352539062, 22.368698120117)',
      'GMR.DefineUnstuck(-1069.4926757812, 7796.4584960938, 23.508069992065)',
      'GMR.DefineUnstuck(-1066.6119384766, 7806.75390625, 25.092672348022)',
      'GMR.DefineUnstuck(-1065.0710449219, 7816.8413085938, 27.157161712646)',
      'GMR.DefineUnstuck(-1065.1168212891, 7832.8935546875, 27.136905670166)',
      'GMR.DefineUnstuck(-1068.0734863281, 7848.9560546875, 22.540681838989)',
      'GMR.DefineUnstuck(-1073.2817382812, 7863.6938476562, 21.321552276611)',
      'GMR.DefineUnstuck(-1080.9219970703, 7881.0268554688, 19.279426574707)',
      'GMR.DefineUnstuck(-1089.0592041016, 7899.4868164062, 17.515781402588)',
      'GMR.DefineUnstuck(-1087.2994384766, 7913.6245117188, 18.117383956909)',
      'GMR.DefineUnstuck(-1076.0368652344, 7924.8046875, 19.523832321167)',
      'GMR.DefineUnstuck(-1060.2393798828, 7937.3515625, 20.32487487793)',
      'GMR.DefineUnstuck(-1041.21875, 7952.4584960938, 21.56481552124)',
      'GMR.DefineUnstuck(-1023.7548217773, 7966.3291015625, 22.790714263916)',
      'GMR.DefineUnstuck(-1007.6913452148, 7979.0874023438, 23.362628936768)',
      'GMR.DefineUnstuck(-989.95336914062, 7993.1752929688, 24.221055984497)',
      'GMR.DefineUnstuck(-971.6484375, 8003.6943359375, 24.784206390381)',
      'GMR.DefineUnstuck(-955.23150634766, 8019.1767578125, 27.239994049072)',
      'GMR.DefineUnstuck(-944.48400878906, 8037.5336914062, 25.164148330688)',
      'GMR.DefineUnstuck(-936.85479736328, 8048.7358398438, 23.891040802002)',
      'GMR.DefineUnstuck(-926.26831054688, 8065.9086914062, 22.12455368042)',
      'GMR.DefineUnstuck(-927.232421875, 8092.365234375, 19.858551025391)',
      'GMR.DefineUnstuck(-930.08209228516, 8102.671875, 18.949300765991)',
      'GMR.DefineUnstuck(-932.77587890625, 8107.2729492188, 18.12260055542)',
      'GMR.DefineUnstuck(-938.70751953125, 8117.1669921875, 16.295049667358)',
      'GMR.DefineUnstuck(-940.96441650391, 8119.0966796875, 15.827108383179)',
      'GMR.DefineUnstuck(-950.47998046875, 8136.2758789062, 8.5300312042236)',
      'GMR.DefineUnstuck(-954.68353271484, 8145.2524414062, 8.0138292312622)',
      'GMR.DefineUnstuck(-960.09832763672, 8156.8154296875, 10.685276031494)',
      'GMR.DefineUnstuck(-965.32904052734, 8167.9853515625, 15.016516685486)',
      'GMR.DefineUnstuck(-970.73718261719, 8179.5341796875, 15.061229705811)',
      'GMR.DefineUnstuck(-975.37341308594, 8188.2456054688, 15.941531181335)',
      'GMR.DefineUnstuck(-981.22369384766, 8197.6826171875, 16.130014419556)',
      'GMR.DefineUnstuck(-975.92413330078, 8212.0166015625, 17.491813659668)',
      'GMR.DefineUnstuck(-967.21002197266, 8237.2607421875, 18.60458946228)',
      'GMR.DefineUnstuck(-958.81103515625, 8256.4697265625, 19.445663452148)',
      'GMR.DefineUnstuck(-952.12030029297, 8270.6201171875, 19.726091384888)',
      'GMR.DefineUnstuck(-945.72509765625, 8282.0419921875, 20.314041137695)',
      'GMR.DefineUnstuck(-935.71154785156, 8293.51953125, 21.039171218872)',
      'GMR.DefineUnstuck(-924.96380615234, 8300.2734375, 21.822727203369)',
      'GMR.DefineUnstuck(-906.2587890625, 8303.2607421875, 21.141536712646)',
      'GMR.DefineUnstuck(-891.21740722656, 8305.662109375, 22.266216278076)',
      'GMR.DefineUnstuck(-866.15509033203, 8308.0986328125, 26.207563400269)',
      'GMR.DefineUnstuck(-847.85247802734, 8309.8564453125, 29.942594528198)',
      'GMR.DefineUnstuck(-832.74542236328, 8306.0244140625, 32.899909973145)',
      'GMR.DefineUnstuck(-813.82214355469, 8297.125, 34.80456161499)',
      'GMR.DefineUnstuck(-800.91857910156, 8288.291015625, 36.266372680664)',
      'GMR.DefineUnstuck(-787.65686035156, 8279.2109375, 38.290046691895)',
      'GMR.DefineUnstuck(-767.64837646484, 8278.5712890625, 39.433307647705)',
      'GMR.DefineUnstuck(-750.94805908203, 8284.9375, 38.717174530029)',
      'GMR.DefineUnstuck(-734.02014160156, 8294.3935546875, 42.750358581543)',
      'GMR.DefineUnstuck(-718.25207519531, 8303.2021484375, 45.228221893311)',
      'GMR.DefineUnstuck(-700.21197509766, 8314.5947265625, 43.892890930176)',
      'GMR.DefineUnstuck(-689.57623291016, 8325.828125, 44.627338409424)',
      'GMR.DefineUnstuck(-687.50610351562, 8336.548828125, 45.384841918945)',
      'GMR.DefineUnstuck(-687.07556152344, 8355.8642578125, 44.981986999512)',
      'GMR.DefineUnstuck(-685.94036865234, 8373.4892578125, 43.457981109619)',
      'GMR.DefineUnstuck(-683.2939453125, 8389.7529296875, 42.118083953857)',
      'GMR.DefineUnstuck(-679.31011962891, 8403.5263671875, 39.710296630859)',
      'GMR.DefineUnstuck(-672.81744384766, 8420.451171875, 39.650512695312)',
      'GMR.DefineUnstuck(-666.47161865234, 8429.716796875, 42.50182723999)',
      'GMR.DefineUnstuck(-651.48815917969, 8437.9306640625, 45.912437438965)',
      'GMR.DefineUnstuck(-632.05261230469, 8442.58203125, 51.520126342773)',
      'GMR.DefineUnstuck(-613.99560546875, 8443.9384765625, 57.841129302979)',
      'GMR.DefineUnstuck(-595.48986816406, 8443.162109375, 63.682975769043)',
      'GMR.DefineUnstuck(-576.15887451172, 8442.3505859375, 63.154140472412)',
      'GMR.DefineUnstuck(-562.99645996094, 8441.798828125, 58.722927093506)',
      'GMR.DefineUnstuck(-546.12725830078, 8441.091796875, 53.434585571289)',
      'GMR.DefineUnstuck(-534.19580078125, 8440.5908203125, 49.977386474609)',
      'GMR.DefineUnstuck(-518.55749511719, 8439.9345703125, 45.25004196167)',
      'GMR.DefineUnstuck(-501.01651000977, 8438.1240234375, 40.605457305908)',
      'GMR.DefineUnstuck(-490.33972167969, 8426.2353515625, 35.358089447021)',
      'GMR.DefineUnstuck(-467.923828125, 8419.8876953125, 28.042377471924)',
      'GMR.DefineUnstuck(-447.58941650391, 8416.6279296875, 23.66381072998)',
      'GMR.DefineUnstuck(-434.86614990234, 8415.576171875, 22.807331085205)',
      'GMR.DefineUnstuck(-417.59356689453, 8415.2314453125, 22.884654998779)',
      'GMR.DefineUnstuck(-400.71292114258, 8414.89453125, 23.350427627563)',
      'GMR.DefineUnstuck(-384.66619873047, 8414.5751953125, 23.472717285156)',
      'GMR.DefineUnstuck(-373.5244140625, 8414.353515625, 23.394208908081)',
      'GMR.DefineUnstuck(-357.48358154297, 8414.033203125, 23.245971679688)',
      'GMR.DefineUnstuck(-345.13806152344, 8413.787109375, 22.477313995361)',
      'GMR.DefineUnstuck(-326.80111694336, 8412.3798828125, 22.311229705811)',
      'GMR.DefineUnstuck(-315.61532592773, 8401.9345703125, 22.366024017334)',
      'GMR.DefineUnstuck(-307.34808349609, 8390.1513671875, 22.167814254761)',
      'GMR.DefineUnstuck(-296.48309326172, 8371.228515625, 22.243064880371)',
      'GMR.DefineUnstuck(-288.94235229492, 8357.080078125, 22.451856613159)',
      'GMR.DefineUnstuck(-285.9172668457, 8345.599609375, 22.406467437744)',
      'GMR.DefineUnstuck(-282.07678222656, 8333.5380859375, 22.283874511719)',
      'GMR.DefineUnstuck(-269.90896606445, 8319.7919921875, 21.500894546509)',
      'GMR.DefineUnstuck(-256.44711303711, 8314.5107421875, 21.699798583984)',
      'GMR.DefineUnstuck(-239.25062561035, 8313.4453125, 21.768432617188)',
      'GMR.DefineUnstuck(-224.69207763672, 8311.4287109375, 21.925270080566)',
      'GMR.DefineUnstuck(-214.09201049805, 8306.3642578125, 22.038370132446)',
      'GMR.DefineUnstuck(-202.98138427734, 8299.259765625, 21.449541091919)',
      'GMR.DefineUnstuck(-193.6580657959, 8290.6005859375, 21.459489822388)',
      'GMR.DefineUnstuck(-186.39723205566, 8278.8154296875, 21.893690109253)',
      'GMR.DefineUnstuck(-176.45487976074, 8258.01171875, 22.046308517456)',
      'GMR.DefineUnstuck(-155.34504699707, 8244.6728515625, 22.404397964478)',
      'GMR.DefineUnstuck(-111.37886047363, 8233.1435546875, 23.066198348999)',
      'GMR.DefineUnstuck(-95.678741455078, 8223.0322265625, 22.765674591064)',
      'GMR.DefineUnstuck(-77.729377746582, 8206.126953125, 22.381187438965)',
      'GMR.DefineUnstuck(-60.131114959717, 8194.6982421875, 22.156925201416)',
      'GMR.DefineUnstuck(-42.178939819336, 8183.0390625, 22.466339111328)',
      'GMR.DefineUnstuck(-24.001403808594, 8176.9926757812, 22.238027572632)',
      'GMR.DefineUnstuck(-1.7133626937866, 8169.82421875, 22.807147979736)',
      'GMR.DefineUnstuck(18.543712615967, 8162.0537109375, 23.880939483643)',
      'GMR.DefineUnstuck(29.125820159912, 8153.6508789062, 22.315843582153)',
      'GMR.DefineUnstuck(41.92794418335, 8141.4311523438, 21.851137161255)',
      'GMR.DefineUnstuck(62.182163238525, 8122.09765625, 18.430303573608)',
      'GMR.DefineUnstuck(65.400009155273, 8108.3979492188, 22.30806350708)',
      'GMR.DefineUnstuck(62.784408569336, 8089.0434570312, 22.836790084839)',
      'GMR.DefineUnstuck(62.26212310791, 8066.4116210938, 21.918243408203)',
      'GMR.DefineUnstuck(63.395076751709, 8046.8232421875, 21.74084854126)',
      'GMR.DefineUnstuck(74.037239074707, 8033.6845703125, 19.716955184937)',
      'GMR.DefineUnstuck(91.044525146484, 8025.099609375, 17.403638839722)',
      'GMR.DefineUnstuck(107.66453552246, 8019.125, 18.169242858887)',
      'GMR.DefineUnstuck(121.74238586426, 8016.6801757812, 17.506134033203)',
      'GMR.DefineUnstuck(134.89863586426, 8015.7651367188, 17.376848220825)',
      'GMR.DefineUnstuck(152.15783691406, 8014.5654296875, 17.385633468628)',
      'GMR.DefineUnstuck(168.10577392578, 8015.3051757812, 20.01082611084)',
      'GMR.DefineUnstuck(183.74096679688, 8022.3203125, 22.117630004883)',
      'GMR.DefineUnstuck(200.94581604004, 8025.736328125, 21.908922195435)',
      'GMR.DefineUnstuck(228.05877685547, 8027.7553710938, 17.719106674194)',
      'GMR.DefineUnstuck(255.74342346191, 8031.7280273438, 17.959735870361)',
      'GMR.DefineUnstuck(276.80816650391, 8039.705078125, 17.484664916992)',
      'GMR.DefineUnstuck(296.50830078125, 8040.7353515625, 17.377986907959)',
      'GMR.DefineUnstuck(313.6291809082, 8033.876953125, 17.374927520752)',
      'GMR.DefineUnstuck(326.99603271484, 8017.8569335938, 17.374927520752)',
      'GMR.DefineUnstuck(333.51431274414, 7996.9443359375, 19.734157562256)',
      'GMR.DefineUnstuck(339.62911987305, 7979.291015625, 22.658855438232)',
      'GMR.DefineUnstuck(348.8049621582, 7964.69140625, 21.976919174194)',
      'GMR.DefineUnstuck(361.62295532227, 7952.634765625, 20.920370101929)',
      'GMR.DefineUnstuck(378.15209960938, 7941.2275390625, 19.571020126343)',
      'GMR.DefineUnstuck(396.34469604492, 7931.6064453125, 17.436414718628)',
      'GMR.DefineUnstuck(417.08676147461, 7920.6372070312, 17.382413864136)',
      'GMR.DefineUnstuck(432.50854492188, 7908.51171875, 17.406589508057)',
      'GMR.DefineUnstuck(446.59323120117, 7892.0043945312, 18.022483825684)',
      'GMR.DefineUnstuck(459.59301757812, 7872.9599609375, 22.343591690063)',
      'GMR.DefineUnstuck(467.66711425781, 7859.1069335938, 22.849094390869)',
      'GMR.DefineUnstuck(485.31497192383, 7851.6904296875, 22.388185501099)',
      'GMR.DefineUnstuck(507.9485168457, 7851.2392578125, 21.862382888794)',
      'GMR.DefineUnstuck(526.88671875, 7850.8618164062, 21.172843933105)',
      'GMR.DefineUnstuck(549.12493896484, 7850.4184570312, 22.029916763306)',
      'GMR.DefineUnstuck(562.31164550781, 7848.5190429688, 21.824901580811)',
      'GMR.DefineUnstuck(572.27886962891, 7839.2802734375, 23.392677307129)',
      'GMR.DefineUnstuck(583.38366699219, 7828.8676757812, 19.438039779663)',
      'GMR.DefineUnstuck(596.9462890625, 7820.3896484375, 17.414178848267)',
      'GMR.DefineUnstuck(609.19384765625, 7810.8637695312, 17.669553756714)',
      'GMR.DefineUnstuck(620.36206054688, 7799.8974609375, 17.926322937012)',
      'GMR.DefineUnstuck(639.17224121094, 7781.427734375, 18.162746429443)',
      'GMR.DefineUnstuck(655.3486328125, 7769.5234375, 17.49423789978)',
      'GMR.DefineUnstuck(669.21929931641, 7760.6538085938, 17.791709899902)',
      'GMR.DefineUnstuck(679.63403320312, 7753.994140625, 21.356698989868)',
      'GMR.DefineUnstuck(695.15240478516, 7742.6157226562, 19.584794998169)',
      'GMR.DefineUnstuck(704.10687255859, 7729.822265625, 17.387756347656)',
      'GMR.DefineUnstuck(708.5498046875, 7717.5776367188, 17.47606086731)',
      'GMR.DefineUnstuck(711.66021728516, 7705.1650390625, 17.453433990479)',
      'GMR.DefineUnstuck(716.47857666016, 7693.7900390625, 17.515617370605)',
      'GMR.DefineUnstuck(725.53143310547, 7679.638671875, 21.798070907593)',
      'GMR.DefineUnstuck(737.24279785156, 7670.7250976562, 21.0915184021)',
      'GMR.DefineUnstuck(750.77984619141, 7662.1137695312, 19.591461181641)',
      'GMR.DefineUnstuck(764.27703857422, 7652.7172851562, 17.83685874939)',
      'GMR.DefineUnstuck(776.21002197266, 7640.8276367188, 17.432355880737)',
      'GMR.DefineUnstuck(786.84576416016, 7626.6997070312, 17.500583648682)',
      'GMR.DefineUnstuck(789.38745117188, 7612.5317382812, 17.713760375977)',
      'GMR.DefineUnstuck(787.45227050781, 7600.7749023438, 19.938592910767)',
      'GMR.DefineUnstuck(784.56079101562, 7589.1879882812, 21.440855026245)',
      'GMR.DefineUnstuck(783.947265625, 7576.6162109375, 19.055822372437)',
      'GMR.DefineUnstuck(791.93395996094, 7559.2470703125, 20.096181869507)',
      'GMR.DefineUnstuck(799.68853759766, 7545.6669921875, 19.109031677246)',
      'GMR.DefineUnstuck(807.97766113281, 7533.4594726562, 17.806901931763)',
      'GMR.DefineUnstuck(814.52416992188, 7523.5766601562, 17.690700531006)',
      'GMR.DefineUnstuck(817.27191162109, 7512.0346679688, 17.711713790894)',
      'GMR.DefineUnstuck(822.15179443359, 7494.767578125, 17.866012573242)',
      'GMR.DefineUnstuck(831.14038085938, 7483.685546875, 17.706562042236)',
      'GMR.DefineUnstuck(840.896484375, 7477.8110351562, 18.989435195923)',
      'GMR.DefineUnstuck(855.94720458984, 7470.1196289062, 22.897710800171)',
      'GMR.DefineUnstuck(869.47094726562, 7464.4760742188, 21.208799362183)',
      'GMR.DefineUnstuck(881.18591308594, 7447.37109375, 21.043695449829)',
      'GMR.DefineUnstuck(881.49188232422, 7428.0434570312, 20.712055206299)',
      'GMR.DefineUnstuck(892.12353515625, 7413.6875, 20.494800567627)',
      'GMR.DefineUnstuck(899.8857421875, 7404.0390625, 20.368368148804)',
      'GMR.DefineUnstuck(910.37530517578, 7393.6674804688, 20.134582519531)',
      'GMR.DefineUnstuck(922.54705810547, 7384.5864257812, 19.932222366333)',
      'GMR.DefineUnstuck(932.98022460938, 7379.8852539062, 20.140092849731)',
      'GMR.DefineUnstuck(955.82977294922, 7378.3442382812, 27.894546508789)',
      'GMR.DefineUnstuck(969.21697998047, 7375.982421875, 29.450576782227)',
      'GMR.DefineUnstuck(982.66351318359, 7372.3466796875, 33.018753051758)',
      'GMR.DefineUnstuck(997.93927001953, 7366.16796875, 36.202354431152)',
      'GMR.DefineUnstuck(1006.6964111328, 7353.4624023438, 36.599727630615)',
      'GMR.DefineUnstuck(1011.0823364258, 7338.4375, 36.444835662842)',
      'GMR.DefineUnstuck(1014.3960571289, 7319.9057617188, 40.888481140137)',
      'GMR.DefineUnstuck(1017.6171875, 7300.1298828125, 46.723125457764)',
      'GMR.DefineUnstuck(1017.9018554688, 7287.8598632812, 51.272617340088)',
      'GMR.DefineUnstuck(1017.0381469727, 7273.4799804688, 53.725517272949)',
      'GMR.DefineUnstuck(1016.2970581055, 7261.1401367188, 56.935218811035)',
      'GMR.DefineUnstuck(1013.1016845703, 7236.7485351562, 64.995140075684)',
      'GMR.DefineUnstuck(1012.5844726562, 7217.8383789062, 71.343887329102)',
      'GMR.DefineUnstuck(1015.8775634766, 7198.8505859375, 78.036567687988)',
      'GMR.DefineUnstuck(1018.3940429688, 7179.2915039062, 84.533638000488)',
      'GMR.DefineUnstuck(1016.0765991211, 7149.0249023438, 94.860359191895)',
      'GMR.DefineUnstuck(1020.8219604492, 7138.4428710938, 98.84871673584)',
      'GMR.DefineUnstuck(1030.6335449219, 7129.1850585938, 103.25421905518)',
      'GMR.DefineUnstuck(1044.4528808594, 7113.5307617188, 109.93940734863)',
      'GMR.DefineUnstuck(1054.2534179688, 7104.1796875, 114.80433654785)',
      'GMR.DefineUnstuck(1067.4244384766, 7092.3193359375, 118.06591796875)',
      'GMR.DefineUnstuck(1090.1801757812, 7091.5610351562, 120.03672027588)',
      'GMR.DefineProfileCenter(1162.197, 7033.508, 109.8138, 80)',
      'GMR.DefineProfileCenter(1193.362, 6992.488, 112.0561, 80)',
      'GMR.DefineProfileCenter(1279.329, 6957.621, 97.53181, 80)',
      'GMR.DefineProfileCenter(1327.42, 6940.761, 93.32112, 80)',
      'GMR.DefineProfileCenter(1343.532, 6874.486, 100.2537, 80)',
      'GMR.DefineProfileCenter(1397.519, 6851.468, 108.9071, 80)',
      'GMR.DefineProfileCenter(1446.728, 6839.419, 108.6355, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      -- Telaar Vendors _ NAGRAND -- MASTERY Repair / Sell Vendor..
      'GMR.DefineSellVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineRepairVendor(-2541.899902, 7364.770020, 7.032160, 19012)',
      'GMR.DefineAmmoVendor(-2555.969971, 7241.520020, 15.226700, 19021)',
      'GMR.DefineGoodsVendor(-2761.409912, 7300.419922, 43.553501, 18914)',
      'GMR.DefineProfileMailbox(-2672.534, 7288.578, 36.73249, 182955)',
      'GMR.DefineQuestEnemyId(22044)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE No Time for Curiosity |r",
    QuestID = 9794,
    QuestType = "TalkTo",
    PickUp = { x = 974.252991, y = 7403.089844, z = 29.6108, id = 18019 },
    TurnIn = { x = 1957.849976, y = 6894.799805, z = 161.876007, id = 18098 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(896.91076660156, 7370.4716796875, 19.079843521118, 60)',
      'GMR.DefineUnstuck(907.51788330078, 7371.6557617188, 17.771284103394)',
      'GMR.DefineUnstuck(918.21179199219, 7372.5932617188, 19.417602539062)',
      'GMR.DefineUnstuck(927.53570556641, 7371.611328125, 20.003631591797)',
      'GMR.DefineUnstuck(937.716796875, 7370.0317382812, 23.135673522949)',
      'GMR.DefineUnstuck(947.06884765625, 7368.580078125, 26.260143280029)',
      'GMR.DefineUnstuck(955.61853027344, 7367.2529296875, 27.914279937744)',
      'GMR.DefineUnstuck(964.97058105469, 7365.8017578125, 29.197172164917)',
      'GMR.DefineUnstuck(974.73760986328, 7364.2861328125, 30.933588027954)',
      'GMR.DefineUnstuck(983.24188232422, 7361.3740234375, 34.545467376709)',
      'GMR.DefineUnstuck(991.59020996094, 7356.9296875, 35.946212768555)',
      'GMR.DefineUnstuck(999.60040283203, 7351.8369140625, 36.445556640625)',
      'GMR.DefineUnstuck(1007.5870361328, 7346.759765625, 36.817512512207)',
      'GMR.DefineUnstuck(1013.4798583984, 7341.2329101562, 36.635387420654)',
      'GMR.DefineUnstuck(1017.2897338867, 7333.0991210938, 38.1930809021)',
      'GMR.DefineUnstuck(1018.8720703125, 7323.7377929688, 40.765190124512)',
      'GMR.DefineUnstuck(1018.2396850586, 7313.9497070312, 42.41495513916)',
      'GMR.DefineUnstuck(1018.0438232422, 7304.5112304688, 45.356090545654)',
      'GMR.DefineUnstuck(1018.4076538086, 7295.0541992188, 48.19775390625)',
      'GMR.DefineUnstuck(1018.5346069336, 7291.7529296875, 49.524295806885)',
      'GMR.DefineUnstuck(1017.9018554688, 7287.8598632812, 51.272617340088)',
      'GMR.DefineUnstuck(1017.0381469727, 7273.4799804688, 53.725517272949)',
      'GMR.DefineUnstuck(1016.2970581055, 7261.1401367188, 56.935218811035)',
      'GMR.DefineUnstuck(1013.1016845703, 7236.7485351562, 64.995140075684)',
      'GMR.DefineUnstuck(1012.5844726562, 7217.8383789062, 71.343887329102)',
      'GMR.DefineUnstuck(1015.8775634766, 7198.8505859375, 78.036567687988)',
      'GMR.DefineUnstuck(1018.3940429688, 7179.2915039062, 84.533638000488)',
      'GMR.DefineUnstuck(1016.0765991211, 7149.0249023438, 94.860359191895)',
      'GMR.DefineUnstuck(1020.8219604492, 7138.4428710938, 98.84871673584)',
      'GMR.DefineUnstuck(1030.6335449219, 7129.1850585938, 103.25421905518)',
      'GMR.DefineUnstuck(1044.4528808594, 7113.5307617188, 109.93940734863)',
      'GMR.DefineUnstuck(1054.2534179688, 7104.1796875, 114.80433654785)',
      'GMR.DefineUnstuck(1067.4244384766, 7092.3193359375, 118.06591796875)',
      'GMR.DefineUnstuck(1090.1801757812, 7091.5610351562, 120.03672027588)',
      'GMR.DefineUnstuck(1096.6461181641, 7092.1489257812, 121.09351348877)',
      'GMR.DefineUnstuck(1111.9173583984, 7088.9501953125, 121.91905212402)',
      'GMR.DefineUnstuck(1121.7120361328, 7083.3540039062, 120.06630706787)',
      'GMR.DefineUnstuck(1128.8515625, 7074.9077148438, 118.40924072266)',
      'GMR.DefineUnstuck(1135.53515625, 7067.1333007812, 117.08613586426)',
      'GMR.DefineUnstuck(1143.8665771484, 7058.5776367188, 115.31108856201)',
      'GMR.DefineUnstuck(1152.2957763672, 7049.921875, 113.4965057373)',
      'GMR.DefineUnstuck(1160.5393066406, 7041.4565429688, 111.08377075195)',
      'GMR.DefineUnstuck(1169.1539306641, 7032.6103515625, 108.74908447266)',
      'GMR.DefineUnstuck(1178.9149169922, 7022.5869140625, 106.88568878174)',
      'GMR.DefineUnstuck(1185.517578125, 7015.806640625, 105.53492736816)',
      'GMR.DefineUnstuck(1192.8530273438, 7009.8110351562, 104.05239868164)',
      'GMR.DefineUnstuck(1201.1616210938, 7003.2026367188, 100.45346832275)',
      'GMR.DefineUnstuck(1210.0578613281, 6995.2358398438, 97.816398620605)',
      'GMR.DefineUnstuck(1217.7337646484, 6988.3618164062, 94.047920227051)',
      'GMR.DefineUnstuck(1225.2528076172, 6980.7885742188, 90.211624145508)',
      'GMR.DefineUnstuck(1233.2340087891, 6975.2158203125, 87.09073638916)',
      'GMR.DefineUnstuck(1244.0853271484, 6971.6577148438, 86.854141235352)',
      'GMR.DefineUnstuck(1256.330078125, 6971.1533203125, 87.139442443848)',
      'GMR.DefineUnstuck(1268.2163085938, 6972.3061523438, 88.0478515625)',
      'GMR.DefineUnstuck(1280.4206542969, 6971.2143554688, 89.995780944824)',
      'GMR.DefineUnstuck(1290.6729736328, 6968.2329101562, 90.829055786133)',
      'GMR.DefineUnstuck(1298.2901611328, 6963.8198242188, 92.968139648438)',
      'GMR.DefineUnstuck(1306.5151367188, 6956.3837890625, 92.87663269043)',
      'GMR.DefineUnstuck(1315.0379638672, 6948.05078125, 93.030395507812)',
      'GMR.DefineUnstuck(1321.8823242188, 6939.8315429688, 94.122444152832)',
      'GMR.DefineUnstuck(1329.5330810547, 6930.6435546875, 93.366561889648)',
      'GMR.DefineUnstuck(1335.3491210938, 6921.8090820312, 94.194427490234)',
      'GMR.DefineUnstuck(1339.3060302734, 6910.9814453125, 94.601005554199)',
      'GMR.DefineUnstuck(1342.8409423828, 6901.30859375, 95.013999938965)',
      'GMR.DefineUnstuck(1347.6900634766, 6892.9086914062, 94.146194458008)',
      'GMR.DefineUnstuck(1356.5231933594, 6883.7084960938, 95.273086547852)',
      'GMR.DefineUnstuck(1364.798828125, 6875.08984375, 96.369682312012)',
      'GMR.DefineUnstuck(1371.994140625, 6869.0581054688, 97.37141418457)',
      'GMR.DefineUnstuck(1381.3145751953, 6864.8989257812, 99.469505310059)',
      'GMR.DefineUnstuck(1391.6799316406, 6862.2036132812, 102.32139587402)',
      'GMR.DefineUnstuck(1401.2458496094, 6859.7163085938, 106.40198516846)',
      'GMR.DefineUnstuck(1409.3829345703, 6856.9360351562, 108.73628997803)',
      'GMR.DefineUnstuck(1419.3356933594, 6851.9541015625, 110.43120574951)',
      'GMR.DefineUnstuck(1428.5498046875, 6847.3422851562, 111.14836120605)',
      'GMR.DefineUnstuck(1437.7388916016, 6842.7421875, 109.78805541992)',
      'GMR.DefineUnstuck(1447.0646972656, 6838.8486328125, 108.49286651611)',
      'GMR.DefineUnstuck(1457.8704833984, 6836.01171875, 109.79217529297)',
      'GMR.DefineUnstuck(1468.9742431641, 6833.0966796875, 108.81558990479)',
      'GMR.DefineUnstuck(1478.5341796875, 6830.5864257812, 106.97119140625)',
      'GMR.DefineUnstuck(1487.6623535156, 6828.1899414062, 107.33791351318)',
      'GMR.DefineUnstuck(1499.6019287109, 6825.0551757812, 109.90090179443)',
      'GMR.DefineUnstuck(1511.5032958984, 6821.7963867188, 113.77821350098)',
      'GMR.DefineUnstuck(1522.17578125, 6816.4228515625, 117.65431213379)',
      'GMR.DefineUnstuck(1531.7166748047, 6811.6186523438, 119.9734954834)',
      'GMR.DefineUnstuck(1541.2824707031, 6806.8022460938, 122.52602386475)',
      'GMR.DefineUnstuck(1552.2561035156, 6806.8959960938, 124.8849029541)',
      'GMR.DefineUnstuck(1561.3171386719, 6809.5991210938, 126.08660888672)',
      'GMR.DefineUnstuck(1571.1844482422, 6812.5424804688, 127.07359313965)',
      'GMR.DefineUnstuck(1581.1540527344, 6816.3491210938, 128.28051757812)',
      'GMR.DefineUnstuck(1591.6450195312, 6821.146484375, 129.13020324707)',
      'GMR.DefineUnstuck(1600.2518310547, 6825.0825195312, 129.80766296387)',
      'GMR.DefineUnstuck(1609.3342285156, 6826.8081054688, 130.4842376709)',
      'GMR.DefineUnstuck(1621.8187255859, 6824.4790039062, 131.21374511719)',
      'GMR.DefineUnstuck(1635.3564453125, 6820.8564453125, 132.2731628418)',
      'GMR.DefineUnstuck(1647.6770019531, 6817.5600585938, 132.84048461914)',
      'GMR.DefineUnstuck(1660.5389404297, 6815.1694335938, 133.67301940918)',
      'GMR.DefineUnstuck(1672.8891601562, 6815.6645507812, 134.68064880371)',
      'GMR.DefineUnstuck(1688.3325195312, 6818.1240234375, 135.8025970459)',
      'GMR.DefineUnstuck(1704.1937255859, 6820.650390625, 136.26504516602)',
      'GMR.DefineUnstuck(1718.0513916016, 6822.3950195312, 136.07235717773)',
      'GMR.DefineUnstuck(1731.1911621094, 6821.9702148438, 136.45509338379)',
      'GMR.DefineUnstuck(1745.1220703125, 6820.322265625, 136.72880554199)',
      'GMR.DefineUnstuck(1756.8598632812, 6820.978515625, 138.14393615723)',
      'GMR.DefineUnstuck(1768.7261962891, 6822.4541015625, 138.87356567383)',
      'GMR.DefineUnstuck(1782.6422119141, 6821.83203125, 137.80027770996)',
      'GMR.DefineUnstuck(1795.7611083984, 6821.2451171875, 138.89605712891)',
      'GMR.DefineUnstuck(1805.8497314453, 6822.71875, 138.95448303223)',
      'GMR.DefineUnstuck(1817.6228027344, 6826.4428710938, 140.21351623535)',
      'GMR.DefineUnstuck(1831.4354248047, 6828.9702148438, 141.19906616211)',
      'GMR.DefineUnstuck(1846.6102294922, 6830.4418945312, 141.0161895752)',
      'GMR.DefineUnstuck(1860.9350585938, 6831.8315429688, 143.15776062012)',
      'GMR.DefineUnstuck(1874.0614013672, 6833.1044921875, 144.42855834961)',
      'GMR.DefineUnstuck(1888.4140625, 6834.4965820312, 145.5302734375)',
      'GMR.DefineUnstuck(1901.5544433594, 6835.7709960938, 146.6224822998)',
      'GMR.DefineUnstuck(1915.0710449219, 6837.0815429688, 148.09591674805)',
      'GMR.DefineUnstuck(1927.8193359375, 6837.8940429688, 150.93099975586)',
      'GMR.DefineUnstuck(1941.8146972656, 6838.2578125, 154.80233764648)',
      'GMR.DefineUnstuck(1954.5922851562, 6838.5903320312, 157.92153930664)',
      'GMR.DefineUnstuck(1962.5250244141, 6844.8452148438, 159.56916809082)',
      'GMR.DefineUnstuck(1959.5500488281, 6854.7690429688, 159.46890258789)',
      'GMR.DefineUnstuck(1955.8688964844, 6867.8505859375, 160.92454528809)',
      'GMR.DefineProfileCenter(1957.849976, 6894.799805, 161.876007, 2)',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)'
    }
  })
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Start of Blades Edge Mountains (BEM)
  
  
  
  
  
  
  
  
  
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Sylvanaar Initial Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10690, x = 1973.87, y = 6859.317, z = 162.5544, id = 185035 },
      { questId = 10455, x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
      { questId = 10555, x = 2035.280029, y = 6834.910156, z = 173.778, id = 21469 },
      { questId = 10510, x = 2055.929932, y = 6816.509766, z = 175.598007, id = 21197 },
      { questId = 10502, x = 2014.719971, y = 6881.02002, z = 179.016006, id = 21158 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Encroaching Wilderness |r",
    QuestID = 10455,
    QuestType = "Custom",
    PickUp = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    TurnIn = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10455, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(1877.357, 6768.025, 145.7043, 80)',
      'GMR.DefineProfileCenter(1924.969, 6682.536, 146.7292, 80)',
      'GMR.DefineProfileCenter(1999.233, 6683.085, 147.7011, 80)',
      'GMR.DefineProfileCenter(2074.984, 6670.617, 146.4758, 80)',
      'GMR.DefineProfileCenter(1925.664, 6615.351, 144.9612, 80)',
      'GMR.DefineProfileCenter(1872.785, 6677.098, 143.1858, 80)',
      'GMR.DefineProfileCenter(1810.814, 6747.727, 140.7052, 80)',
      'GMR.DefineProfileCenter(1587.615, 6770.902, 137.7738, 80)',
      'GMR.DefineProfileCenter(1549.037, 6820.701, 126.2375, 80)',
      'GMR.DefineProfileCenter(1766.318, 6796.24, 138.988, 80)',
      'GMR.DefineProfileCenter(1887.098, 6782.363, 143.2243, 80)',
      'GMR.DefineProfileCenter(1916.019, 6690.512, 146.1002, 80)',
      'GMR.DefineProfileCenter(1992.149, 6664.585, 144.4873, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(21022)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Marauding Wolves |r",
    QuestID = 10456,
    QuestType = "Custom",
    PickUp = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    TurnIn = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10456, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(2046.954, 6181.663, 141.2827, 80)',
      'GMR.DefineProfileCenter(2176.094, 6116.437, 144.8193, 80)',
      'GMR.DefineProfileCenter(2125.043, 5982.709, 137.7899, 80)',
      'GMR.DefineProfileCenter(1971.687, 5942.623, 137.4179, 80)',
      'GMR.DefineProfileCenter(1904.236, 6126.666, 141.4538, 80)',
      'GMR.DefineProfileCenter(1959.701, 5954.22, 138.9495, 80)',
      'GMR.DefineProfileCenter(1936.977, 5876.082, 139.4526, 80)',
      'GMR.DefineProfileCenter(1823.827, 5959.228, 141.4352, 80)',
      'GMR.DefineProfileCenter(1765.596, 6030.261, 139.1176, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(20748)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Protecting Our Own |r",
    QuestID = 10457,
    QuestType = "Gathering",
    PickUp = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    TurnIn = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10457, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(1909.623, 6773.668, 144.7982, 80)',
      'GMR.DefineProfileCenter(1912.323, 6689.994, 146.9664, 80)',
      'GMR.DefineProfileCenter(1966.684, 6699.868, 147.9955, 80)',
      'GMR.DefineProfileCenter(2086.319, 6664.629, 144.4704, 80)',
      'GMR.DefineProfileCenter(2066.099, 6598.841, 139.8643, 80)',
      'GMR.DefineProfileCenter(1997.769, 6593.641, 138.5898, 80)',
      'GMR.DefineProfileCenter(1906.558, 6618.17, 144.6331, 80)',
      'GMR.DefineProfileCenter(1808.097, 6728.297, 137.6452, 80)',
      'GMR.DefineProfileCenter(1745.072, 6737.774, 136.3965, 80)',
      'GMR.DefineProfileCenter(1733.01, 6798.84, 138.5743, 80)',
      'GMR.DefineProfileCenter(1615.798, 6768.697, 133.3398, 80)',
      'GMR.DefineProfileCenter(1551.129, 6755.295, 134.0925, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(184631)',
      'GMR.DefineCustomObjectId(184631)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE A Dire Situation |r",
    QuestID = 10506,
    QuestType = "Custom",
    PickUp = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    TurnIn = { x = 1980.619995, y = 6881.189941, z = 161.373001, id = 21066 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10506, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand043411 then
        GMR.Print("Frame created")
        GMR.Frames.Rand043411 = CreateFrame("frame")
        GMR.Frames.Rand043411:SetScript("OnUpdate", function(self)
          local buffOnDog = GetSpellInfo(36310)
          local itemName = GetItemInfo(30251)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10506 then
            if GMR.IsQuestActive(10506) then
              if GMR.IsExecuting() and not GMR.HasBuff("target", buffOnDog) and not GMR.HasDebuff("target", buffOnDog) and not UnitIsDead("target") and UnitName("target") == "Bloodmaul Dire Wolf" then
                if not GMR.Questing.IsObjectiveCompleted(10506, 1) then
                  local npc1 = GMR.GetObjectWithInfo({ id = 20058, rawType = 5, isAlive = true })
                  if npc1 then 
                    if not timer then 
                      timer = GetTime()+5
                    elseif timer < GetTime() then 
                      GMR.SetQuestingState("Idle")
                      timer = nil;
                      print("|cFFE25FFF CryptoQuester |r |cFF1E90FE  Useing Item on Wolf. |r")
                      GMR.Use(itemName)
                      GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 10)
                    end
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand043411 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(1485.576, 6581.937, -10.08036, 120)',
      'GMR.DefineProfileCenter(1646.046, 6481.948, 2.876792, 120)',
      'GMR.DefineProfileCenter(1727.682, 6504.325, 1.893835, 120)',
      'GMR.DefineProfileCenter(1812.052, 6477.068, 1.9682, 120)',
      'GMR.DefineProfileCenter(1843.841, 6409.54, -10.33799, 120)',
      'GMR.DefineProfileCenter(1982.375, 6384.954, -10.33819, 120)',
      'GMR.DefineProfileCenter(1633.887, 6356.184, -4.129035, 120)',
      'GMR.DefineProfileCenter(1709.554, 6480.003, -0.1807683, 120)',
      'GMR.DefineProfileCenter(1451.349, 6517.056, -10.33909, 120)',
      'GMR.DefineProfileCenter(1465.6, 6544.111, -10.33865, 120)',
      'GMR.DefineProfileCenter(1848.051, 6409.442, -10.33859, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(20058)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Malaise |r",
    QuestID = 10555,
    QuestType = "Custom",
    PickUp = { x = 2035.280029, y = 6834.910156, z = 173.778, id = 21469 },
    TurnIn = { x = 1609.844, y = 6985.815, z = 189.9971, id = 184825 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10555, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(1793.733, 6878.384, 142.7902, 80)',
      'GMR.DefineProfileCenter(1656.835, 6869.7, 143.8089, 30)',
      'GMR.DefineProfileCenter(1622.359, 6913.112, 154.1787, 80)',
      'GMR.DefineProfileCenter(1589.071, 6921.078, 157.6817, 80)',
      'GMR.DefineProfileCenter(1546.531, 6949.248, 159.0518, 80)',
      'GMR.DefineProfileCenter(1690.016, 6905.269, 162.5248, 80)',
      'GMR.DefineProfileCenter(1666.439, 6938.563, 167.8212, 80)',
      'GMR.DefineProfileCenter(1655.937, 6985.315, 156.891, 80)',
      'GMR.DefineProfileCenter(1735.773, 6921.734, 143.6865, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(184631)',
      'GMR.DefineQuestEnemyId(19944)',
      'GMR.DefineQuestEnemyId(19945)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Scratches |r",
    QuestID = 10556,
    QuestType = "Custom",
    PickUp = { x = 1609.844, y = 6985.815, z = 189.9971, id = 184825 },
    TurnIn = { x = 2035.280029, y = 6834.910156, z = 173.778, id = 21469 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10556, 1) then
        GMR.SetQuestingState(nil);
        local spiritBuff = GetSpellInfo(36904)
        if not GMR.HasBuff("player", spiritBuff) then
          if not GMR.IsPlayerPosition(1612.486, 6935.542, 159.1053, 3) then 
            GMR.MeshTo(1612.486, 6935.542, 159.1053)
          else 
            local itemName = GetItemInfo(30530)
            GMR.Use(itemName)
          end
        elseif GMR.HasBuff("player", spiritBuff) then
          if not GMR.IsPlayerPosition(2035.280029, 6834.910156, 173.778000, 3) then 
            GMR.MeshTo(2035.280029, 6834.910156, 173.778000)
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1540.7529296875, 6233.4013671875, 2.5979883670807, 60)',
      'GMR.DefineUnstuck(1545.7053222656, 6255.4399414062, 0.079686924815178)',
      'GMR.DefineUnstuck(1548.1832275391, 6273.1455078125, 2.8410902023315)',
      'GMR.DefineUnstuck(1551.4313964844, 6292.611328125, 3.6392033100128)',
      'GMR.DefineUnstuck(1556.7231445312, 6308.9467773438, 3.5528297424316)',
      'GMR.DefineUnstuck(1566.5872802734, 6318.2270507812, 1.2902408838272)',
      'GMR.DefineUnstuck(1578.6467285156, 6328.04296875, 1.4573941230774)',
      'GMR.DefineUnstuck(1592.9129638672, 6334.1430664062, 2.6005916595459)',
      'GMR.DefineUnstuck(1608.7083740234, 6340.5869140625, -1.066574215889)',
      'GMR.DefineUnstuck(1620.3983154297, 6350.0268554688, -3.180570602417)',
      'GMR.DefineUnstuck(1633.3959960938, 6359.1142578125, -4.4510455131531)',
      'GMR.DefineUnstuck(1646.2019042969, 6365.474609375, -6.0637307167053)',
      'GMR.DefineUnstuck(1656.7489013672, 6375.728515625, -8.7670679092407)',
      'GMR.DefineUnstuck(1664.3795166016, 6389.29296875, -9.9989433288574)',
      'GMR.DefineUnstuck(1670.7111816406, 6402.1635742188, -9.2906799316406)',
      'GMR.DefineUnstuck(1682.2452392578, 6411.2841796875, -8.4913053512573)',
      'GMR.DefineUnstuck(1690.6381835938, 6439.748046875, -6.3804936408997)',
      'GMR.DefineUnstuck(1693.4061279297, 6460.40234375, -3.5629057884216)',
      'GMR.DefineUnstuck(1690.5751953125, 6473.5927734375, -1.7789362668991)',
      'GMR.DefineUnstuck(1676.7257080078, 6487.1743164062, 0.90570384263992)',
      'GMR.DefineUnstuck(1673.5474853516, 6502.236328125, 3.4608473777771)',
      'GMR.DefineUnstuck(1679.697265625, 6514.525390625, 4.544367313385)',
      'GMR.DefineUnstuck(1690.5257568359, 6523.9228515625, 5.5348501205444)',
      'GMR.DefineUnstuck(1702.1174316406, 6532.421875, 8.2176361083984)',
      'GMR.DefineUnstuck(1708.7574462891, 6544.5634765625, 11.675038337708)',
      'GMR.DefineUnstuck(1706.833984375, 6556.3374023438, 14.902338981628)',
      'GMR.DefineUnstuck(1698.4077148438, 6564.0151367188, 18.453796386719)',
      'GMR.DefineUnstuck(1683.78125, 6571.6040039062, 23.902009963989)',
      'GMR.DefineUnstuck(1672.9547119141, 6580.173828125, 27.089233398438)',
      'GMR.DefineUnstuck(1661.8260498047, 6590.345703125, 29.795789718628)',
      'GMR.DefineUnstuck(1652.2292480469, 6600.373046875, 32.302062988281)',
      'GMR.DefineUnstuck(1647.5063476562, 6616.0297851562, 35.757392883301)',
      'GMR.DefineUnstuck(1649.2492675781, 6623.2353515625, 38.105560302734)',
      'GMR.DefineUnstuck(1656.3403320312, 6627.0122070312, 41.979904174805)',
      'GMR.DefineUnstuck(1671.0590820312, 6628.6220703125, 46.869380950928)',
      'GMR.DefineUnstuck(1679.8100585938, 6625.0185546875, 49.933414459229)',
      'GMR.DefineUnstuck(1688.1329345703, 6624.373046875, 53.738582611084)',
      'GMR.DefineUnstuck(1700.5330810547, 6627.3564453125, 57.799354553223)',
      'GMR.DefineUnstuck(1714.1229248047, 6630.6259765625, 61.232414245605)',
      'GMR.DefineUnstuck(1726.1575927734, 6634.6977539062, 65.02571105957)',
      'GMR.DefineUnstuck(1736.2364501953, 6639.4853515625, 67.620208740234)',
      'GMR.DefineUnstuck(1750.3366699219, 6646.1826171875, 72.03833770752)',
      'GMR.DefineUnstuck(1757.1062011719, 6653.7646484375, 77.037147521973)',
      'GMR.DefineUnstuck(1754.8436279297, 6660.3212890625, 79.611938476562)',
      'GMR.DefineUnstuck(1740.4256591797, 6668.2958984375, 82.695495605469)',
      'GMR.DefineUnstuck(1722.1912841797, 6676.38671875, 87.697563171387)',
      'GMR.DefineUnstuck(1705.8892822266, 6682.189453125, 90.91943359375)',
      'GMR.DefineUnstuck(1694.5887451172, 6688.732421875, 93.746566772461)',
      'GMR.DefineUnstuck(1680.1337890625, 6692.54296875, 96.705772399902)',
      'GMR.DefineUnstuck(1667.3327636719, 6690.3979492188, 100.00956726074)',
      'GMR.DefineUnstuck(1648.7648925781, 6687.6953125, 106.19145202637)',
      'GMR.DefineUnstuck(1636.0083007812, 6694.5366210938, 108.39639282227)',
      'GMR.DefineUnstuck(1632.5270996094, 6707.3989257812, 110.58550262451)',
      'GMR.DefineUnstuck(1637.0968017578, 6724.7260742188, 113.92662811279)',
      'GMR.DefineUnstuck(1647.2313232422, 6756.8461914062, 123.25807189941)',
      'GMR.DefineUnstuck(1647.2934570312, 6772.3403320312, 126.8498916626)',
      'GMR.DefineUnstuck(1650.6259765625, 6788.4448242188, 131.3168182373)',
      'GMR.DefineUnstuck(1650.0358886719, 6813.06640625, 132.57312011719)',    
      'GMR.DefineProfileCenter(1612.486, 6935.542, 159.1053, 80)',
      'GMR.DefineProfileCenter(2035.280029, 6834.910156, 173.778000, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Into the Draenethyst Mine |r",
    QuestID = 10510,
    QuestType = "Custom",
    PickUp = { x = 2055.929932, y = 6816.509766, z = 175.598007, id = 21197 },
    TurnIn = { x = 2055.929932, y = 6816.509766, z = 175.598007, id = 21197 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10510, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      -- Back by the Cave to Path Down
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1531.0010986328, 6769.58203125, 126.16089630127, 50)',
      'GMR.DefineUnstuck(1550.1419677734, 6757.4916992188, 132.0287322998)',
      'GMR.DefineUnstuck(1578.6588134766, 6755.7802734375, 136.98008728027)',
      'GMR.DefineUnstuck(1596.5432128906, 6760.2084960938, 137.26704406738)',
      'GMR.DefineUnstuck(1611.2568359375, 6768.3046875, 132.5834197998)',
      'GMR.DefineUnstuck(1624.4990234375, 6776.5961914062, 128.92774963379)',
      'GMR.DefineUnstuck(1639.4898681641, 6783.3012695312, 127.33657073975)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1636.4721679688, 6783.4106445312, 127.35332489014, 60)',
      'GMR.DefineUnstuck(1640.7576904297, 6762.4379882812, 123.8233795166)',
      'GMR.DefineUnstuck(1644.8806152344, 6742.2607421875, 119.01109313965)',
      'GMR.DefineUnstuck(1643.5590820312, 6727.9790039062, 115.44709777832)',
      'GMR.DefineUnstuck(1635.0340576172, 6709.349609375, 110.97146606445)',
      'GMR.DefineUnstuck(1636.5559082031, 6687.9868164062, 107.79581451416)',
      'GMR.DefineUnstuck(1659.4827880859, 6681.2314453125, 103.31798553467)',
      'GMR.DefineUnstuck(1675.9547119141, 6677.6162109375, 99.513519287109)',
      'GMR.DefineUnstuck(1686.6195068359, 6674.5620117188, 94.30330657959)',
      'GMR.DefineUnstuck(1695.2062988281, 6672.8793945312, 91.995445251465)',
      'GMR.DefineUnstuck(1718.5432128906, 6669.8862304688, 88.39852142334)',
      'GMR.DefineUnstuck(1741.8282470703, 6663.193359375, 81.040687561035)',
      'GMR.DefineUnstuck(1759.7016601562, 6658.357421875, 78.674293518066)',
      'GMR.DefineUnstuck(1750.7736816406, 6643.8520507812, 71.491287231445)',
      'GMR.DefineUnstuck(1731.7238769531, 6635.5053710938, 66.595863342285)',
      'GMR.DefineUnstuck(1716.6456298828, 6628.1337890625, 62.051990509033)',
      'GMR.DefineUnstuck(1703.6890869141, 6625.9243164062, 58.432411193848)',
      'GMR.DefineUnstuck(1689.3978271484, 6625.2885742188, 54.344665527344)',
      'GMR.DefineUnstuck(1670.1934814453, 6627.9658203125, 46.64501953125)',
      'GMR.DefineUnstuck(1645.2863769531, 6629.8515625, 38.300365447998)',
      'GMR.DefineUnstuck(1611.0017089844, 6619.4580078125, 31.362140655518)',
      'GMR.DefineUnstuck(1588.5648193359, 6605.64453125, 19.923065185547)',
      'GMR.DefineUnstuck(1568.9296875, 6593.5561523438, 12.322212219238)',
      'GMR.DefineUnstuck(1550.9372558594, 6569.3217773438, 6.0698165893555)',
      'GMR.DefineUnstuck(1535.9250488281, 6534.6723632812, -7.7117972373962)',
      'GMR.DefineUnstuck(1526.1654052734, 6525.908203125, -10.172226905823)',
      'GMR.DefineUnstuck(1513.1954345703, 6522.0336914062, -10.337920188904)',
      'GMR.DefineUnstuck(1499.7457275391, 6521.6015625, -8.471866607666)',
      'GMR.DefineUnstuck(1488.7766113281, 6529.5263671875, -10.338403701782)',
      'GMR.DefineUnstuck(1475.4735107422, 6536.140625, -10.338403701782)',
      'GMR.DefineUnstuck(1455.3389892578, 6537.02734375, -10.338403701782)',
      'GMR.DefineUnstuck(1442.0074462891, 6539.111328125, -10.251690864563)',
      'GMR.DefineUnstuck(1424.7462158203, 6536.83984375, -1.0525606870651)',
      'GMR.DefineUnstuck(1416.2790527344, 6528.0385742188, 4.0357618331909)',
      'GMR.DefineUnstuck(1406.2934570312, 6532.791015625, 5.4347457885742)',
      'GMR.DefineUnstuck(1395.0124511719, 6538.94921875, 9.9321641921997)',
      'GMR.DefineUnstuck(1387.4138183594, 6538.4428710938, 12.320508003235)',
      'GMR.DefineProfileCenter(1389.727, 6546.989, 11.28795, 80)',
      'GMR.DefineProfileCenter(1345.294, 6540.011, 5.869967, 80)',
      'GMR.DefineProfileCenter(1351.527, 6583.794, -7.335249, 80)',
      'GMR.DefineProfileCenter(1359.722, 6612.744, -12.4143, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(184689)',
      'GMR.DefineCustomObjectId(184689)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Bloodmaul Ogres |r",
    QuestID = 10502,
    QuestType = "Custom",
    PickUp = { x = 2014.719971, y = 6881.02002, z = 179.016006, id = 21158 },
    TurnIn = { x = 2014.719971, y = 6881.02002, z = 179.016006, id = 21158 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10502, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(1633.312, 6697.953, 108.9732, 80)',
      'GMR.DefineProfileCenter(1705.97, 6637.809, 58.52225, 80)',
      'GMR.DefineProfileCenter(1557.247, 6575.904, 7.914494, 80)',
      'GMR.DefineProfileCenter(1514.354, 6519.413, -10.33811, 80)',
      'GMR.DefineProfileCenter(1557.498, 6462.176, -11.31442, 80)',
      'GMR.DefineProfileCenter(1684.328, 6497.11, 2.465522, 80)',
      'GMR.DefineProfileCenter(1812.29, 6497.209, 1.831017, 80)',
      'GMR.DefineProfileCenter(1926.687, 6459.925, 1.925921, 80)',
      'GMR.DefineProfileCenter(2048.868, 6442.067, -0.7564406, 80)',
      'GMR.DefineProfileCenter(1979.484, 6336.665, -0.7478974, 80)',
      'GMR.DefineProfileCenter(1824.733, 6316.594, 0.06995571, 80)',
      'GMR.DefineProfileCenter(1672.513, 6482.495, -0.02329192, 80)',
      'GMR.DefineProfileCenter(2204.602, 6324.933, 2.090701, 80)',
      'GMR.DefineAreaBlacklist(1710.822, 6323.467, 33.36446, 25)',
      'GMR.DefineAreaBlacklist(1697.156, 6292.858, -0.7506068, 80)',
      'GMR.DefineAreaBlacklist(1650.937, 6315.469, -0.4350412, 80)',
      'GMR.DefineAreaBlacklist(1644.998, 6287.315, 3.643017, 80)',
      'GMR.DefineAreaBlacklist(1597.587, 6300.477, 2.642248, 80)',
      'GMR.DefineAreaBlacklist(1562.206, 6318.305, 1.413823, 80)',
      'GMR.DefineAreaBlacklist(1549.407, 6356.4, 2.794475, 80)',
      'GMR.DefineAreaBlacklist(1517.35, 6351.114, 1.417237, 80)',
      'GMR.DefineAreaBlacklist(1490.051, 6383.325, 34.95801, 80)',
      'GMR.DefineAreaBlacklist(1530.323, 6363.655, 2.022915, 80)',
      'GMR.DefineAreaBlacklist(1619.026, 6342.906, -2.214493, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(19948)',
      'GMR.DefineQuestEnemyId(19991)',
      'GMR.DefineQuestEnemyId(19992)',
      'GMR.DefineQuestEnemyId(21238)',
      'GMR.DefineQuestEnemyId(19952)',
      'GMR.DefineQuestEnemyId(19957)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFF1E90FE Into the Draenethyst Mine / The Bloodmaul Ogres |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10502, x = 2014.719971, y = 6881.02002, z = 179.016006, id = 21158 },
      { questId = 10510, x = 2055.929932, y = 6816.509766, z = 175.598007, id = 21197 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
      if not GMR.Frames.Rand082 then
          GMR.Print("Frame created")
          GMR.Frames.Rand082 = CreateFrame("frame")
          GMR.Frames.Rand082:SetScript("OnUpdate", function(self)
              if GMR.GetProfileType() == "Questing" and GMR.IsQuestActive(10502) and GMR.Questing.IsObjectiveCompleted(10502, 1)  then
                  if GMR.IsExecuting() then
                      if GossipFrameGreetingPanel:IsShown() then
                          GossipTitleButton2:Click()
                      end
                  end
              else 
                  self:SetScript("OnUpdate", nil); GMR.Frames.Rand082 = nil; GMR.Print("Frame deleted")
              end
          end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Den Mother |r",
    QuestID = 10690,
    QuestType = "Custom",
    PickUp = { x = 1973.87, y = 6859.317, z = 162.5544, id = 185035 },
    TurnIn = { x = 2012.6407470703, y = 6877.3471679688, z = 178.99063110352, id = 21158 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10690, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
      if not GMR.Frames.Rand083 then
          GMR.Print("Frame created")
          GMR.Frames.Rand083 = CreateFrame("frame")
          GMR.Frames.Rand083:SetScript("OnUpdate", function(self)
              if GMR.GetProfileType() == "Questing" and GMR.IsQuestActive(10690) and GMR.Questing.IsObjectiveCompleted(10690, 1)  then
                  if GMR.IsExecuting() then
                      if GossipFrameGreetingPanel:IsShown() then
                          GossipTitleButton3:Click()
                      end
                  end
              else 
                  self:SetScript("OnUpdate", nil); GMR.Frames.Rand083 = nil; GMR.Print("Frame deleted")
              end
          end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2296.352, 7023.231, 364.1174, 60)',
      'GMR.DefineUnstuck(2290.371, 7036.544, 363.4784)',
      'GMR.DefineUnstuck(2277.42, 7043.14, 364.032)',
      'GMR.DefineUnstuck(2265.435, 7049.313, 365.0082)',
      'GMR.DefineUnstuck(2251.713, 7056.382, 365.0527)',
      'GMR.DefineUnstuck(2235.957, 7064.499, 365.1271)',
      'GMR.DefineUnstuck(2224.196, 7071.085, 366.2628)',
      'GMR.DefineUnstuck(2211.318, 7078.559, 363.6794)',
      'GMR.DefineUnstuck(2201.011, 7084.541, 364.5184)',
      'GMR.DefineUnstuck(2190.573, 7092.497, 365.0562)',
      'GMR.DefineUnstuck(2177.864, 7102.185, 364.4355)',
      'GMR.DefineUnstuck(2160.464, 7099.013, 364.6405)',
      'GMR.DefineUnstuck(2143.608, 7096.438, 364.8031)',
      'GMR.DefineUnstuck(2131.678, 7095.844, 364.7713)',
      'GMR.DefineUnstuck(2121.746, 7095.685, 364.7713)',
      'GMR.DefineUnstuck(2111.101, 7095.514, 364.7713)',
      'GMR.DefineUnstuck(2097.284, 7095.292, 364.7713)',
      'GMR.DefineUnstuck(2085.6, 7098.336, 364.7713)',
      'GMR.DefineUnstuck(2074.195, 7102.169, 364.7713)',
      'GMR.DefineUnstuck(2064.172, 7105.823, 364.7713)',
      'GMR.DefineUnstuck(2057.91, 7106.463, 364.3334)',
      'GMR.DefineUnstuck(2044.348, 7099.16, 360.1485)',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2041.619, 7088.303, 347.5758, 10)',
      'GMR.DefineUnstuck(2040.029, 7083.587, 345.0586)',
      'GMR.DefineUnstuck(2038.359, 7078.536, 341.7549)',
      'GMR.DefineUnstuck(2036.648, 7073.358, 337.2088)',
      'GMR.DefineUnstuck(2031.505, 7065.884, 329.9679)',
      'GMR.DefineUnstuck(2031.75, 7063.231, 327.3927)',
      'GMR.DefineUnstuck(2034.012, 7058.702, 323.3557)',
      'GMR.DefineUnstuck(2033.066, 7054.059, 318.8923)',
      'GMR.DefineUnstuck(2031.39, 7049.878, 314.534)',
      'GMR.DefineUnstuck(2027.922, 7047.349, 310.5313)',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.892, 7012.389, 236.29, 10)',
      'GMR.DefineUnstuck(2028.368, 7007.634, 232.937)',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2024.973, 6970.768, 177.3201, 10)',
      'GMR.DefineUnstuck(2028.125, 6959.382, 176.4363)',
      'GMR.DefineUnstuck(2029.87, 6949.371, 178.8213)',
      'GMR.DefineProfileCenter(1696.609985, 6045.540039, 144.660004, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineRepairVendor(2100.729980, 6797.839844, 175.695999, 19499)',
      'GMR.DefineAmmoVendor(2020.520020, 6797.609863, 175.695999, 19498)',
      'GMR.DefineGoodsVendor(2098.179932, 6903.919922, 183.313004, 19495)',
      'GMR.DefineProfileMailbox(2082.809, 6878.939, 180.1238, 184147)',
      'GMR.DefineQuestEnemyId(21956)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Toshleys Station - Initial Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10608, x = 1914.839966, y = 5578.609863, z = 263.959991, id = 21755 },
      { questId = 10557, x = 1935.01001, y = 5575.740234, z = 263.752991, id = 21460 },
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.AllowSpeedUp()',
      'GMR.SetChecked("Mount", true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2036.609, 6577.254, 134.3875, 150)',
      'GMR.DefineUnstuck(2036.406, 6560.81, 133.4612)',
      'GMR.DefineUnstuck(2036.181, 6542.578, 133.6518)',
      'GMR.DefineUnstuck(2035.897, 6519.588, 133.8407)',
      'GMR.DefineUnstuck(2035.6, 6495.521, 133.9897)',
      'GMR.DefineUnstuck(2035.357, 6475.792, 134.0894)',
      'GMR.DefineUnstuck(2035.098, 6454.783, 134.193)',
      'GMR.DefineUnstuck(2034.805, 6431.045, 134.263)',
      'GMR.DefineUnstuck(2034.444, 6401.826, 134.3086)',
      'GMR.DefineUnstuck(2034.118, 6375.383, 134.2922)',
      'GMR.DefineUnstuck(2033.634, 6336.118, 134.2143)',
      'GMR.DefineUnstuck(2033.21, 6301.804, 134.0487)',
      'GMR.DefineUnstuck(2032.878, 6274.854, 133.8646)',
      'GMR.DefineUnstuck(2032.496, 6243.921, 133.6162)',
      'GMR.DefineUnstuck(2032.251, 6223.999, 133.4205)',
      'GMR.DefineUnstuck(2031.783, 6195.144, 138.1633)',
      'GMR.DefineUnstuck(2038.263, 6169.438, 140.7652)',
      'GMR.DefineUnstuck(2044.542, 6144.168, 144.2491)',
      'GMR.DefineUnstuck(2047.92, 6117.778, 146.5236)',
      'GMR.DefineUnstuck(2024.728, 6089.148, 148.5797)',
      'GMR.DefineUnstuck(2009.65, 6069.89, 148.7188)',
      'GMR.DefineUnstuck(1998.479, 6051.133, 147.1455)',
      'GMR.DefineUnstuck(1985.962, 6030.114, 145.4041)',
      'GMR.DefineUnstuck(1967.125, 6013.851, 142.9005)',
      'GMR.DefineUnstuck(1944.371, 6004.994, 141.8868)',
      'GMR.DefineUnstuck(1924.365, 5997.896, 140.7344)',
      'GMR.DefineUnstuck(1903.373, 5990.447, 140.275)',
      'GMR.DefineUnstuck(1881.69, 5980.541, 139.9472)',
      'GMR.DefineUnstuck(1863.754, 5968.783, 141.2516)',
      'GMR.DefineUnstuck(1834.499, 5960.808, 141.4755)',
      'GMR.DefineUnstuck(1815.194, 5957.94, 142.0824)',
      'GMR.DefineUnstuck(1801.762, 5940.235, 143.8348)',
      'GMR.DefineUnstuck(1784.79, 5919.909, 147.0615)',
      'GMR.DefineUnstuck(1778.173, 5900.866, 150.2464)',
      'GMR.DefineUnstuck(1771.497, 5881.655, 154.0739)',
      'GMR.DefineUnstuck(1763.105, 5857.505, 157.7424)',
      'GMR.DefineUnstuck(1772.17, 5836.292, 164.8222)',
      'GMR.DefineUnstuck(1785.011, 5820.337, 172.7566)',
      'GMR.DefineUnstuck(1796.678, 5803.907, 182.1048)',
      'GMR.DefineUnstuck(1811.186, 5791.48, 191.5759)',
      'GMR.DefineUnstuck(1837.031, 5787.599, 203.4736)',
      'GMR.DefineUnstuck(1860.582, 5784.061, 213.7244)',
      'GMR.DefineUnstuck(1867.875, 5770.946, 219.8653)',
      'GMR.DefineUnstuck(1868.913, 5750.993, 228.8338)',
      'GMR.DefineUnstuck(1869.886, 5732.292, 236.9085)',
      'GMR.DefineUnstuck(1870.894, 5712.914, 244.4165)',
      'GMR.DefineUnstuck(1878.472, 5695.483, 250.4543)',
      'GMR.DefineUnstuck(1896.086, 5676.554, 256.5716)',
      'GMR.DefineUnstuck(1901.47, 5657.629, 259.3373)',
      'GMR.DefineUnstuck(1901.713, 5643.986, 253.6871)',
      'GMR.DefineUnstuck(1901.916, 5632.565, 252.2889)',
      'GMR.DefineUnstuck(1902.223, 5615.352, 252.3963)',
      'GMR.DefineUnstuck(1888.334, 5601.665, 255.106)',
      'GMR.DefineUnstuck(1887.74, 5583.369, 258.1555)',
      'GMR.DefineUnstuck(1895.979, 5567.649, 260.6675)',
      'GMR.DefineSellVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineRepairVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineGoodsVendor(1944.849976, 5537.870117, 266.734009, 21110)',
      'GMR.DefineAmmoVendor(1913.000000, 5522.930176, 267.704010, 21111)',
      'GMR.DefineProfileMailbox(1935.499, 5545.311, 266.7737, 184944)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Crystal Clear |r",
    QuestID = 10608,
    QuestType = "Custom",
    PickUp = { x = 1914.839966, y = 5578.609863, z = 263.959991, id = 21755 },
    TurnIn = { x = 1914.839966, y = 5578.609863, z = 263.959991, id = 21755 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10608, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(1784.72, 5550.9, 265.3363, 80)',
      'GMR.DefineProfileCenter(1745.8, 5556.966, 266.5698, 80)',
      'GMR.DefineProfileCenter(1695.885, 5657.255, 266.5506, 80)',
      'GMR.DefineProfileCenter(1646.039, 5612.969, 266.4218, 80)',
      'GMR.DefineProfileCenter(1670.248, 5527.412, 265.3463, 80)',
      'GMR.DefineProfileCenter(1649.581, 5483.292, 265.9429, 80)',
      'GMR.DefineProfileCenter(1550.868, 5495.347, 269.239, 80)',
      'GMR.DefineProfileCenter(1579.93, 5392.673, 266.1784, 80)',
      'GMR.DefineProfileCenter(1663.189, 5304.572, 265.1374, 80)',
      'GMR.DefineProfileCenter(1692.639, 5231.473, 265.265, 80)',
      'GMR.DefineProfileCenter(1630.585, 5208.854, 265.1877, 80)',
      'GMR.DefineProfileCenter(1601.318, 5376.03, 266.5841, 80)',
      'GMR.DefineProfileCenter(1552.441, 5499.155, 269.0866, 80)',
      'GMR.DefineProfileCenter(1584.54, 5594.364, 265.1855, 80)',
      'GMR.DefineProfileCenter(1677.691, 5640.333, 265.3408, 80)',
      'GMR.DefineProfileCenter(1743.729, 5659.223, 266.1317, 80)',
      'GMR.DefineProfileCenter(1723.439, 5558.362, 265.2115, 80)',
      'GMR.BlacklistId(21796)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineRepairVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineGoodsVendor(1944.849976, 5537.870117, 266.734009, 21110)',
      'GMR.DefineAmmoVendor(1913.000000, 5522.930176, 267.704010, 21111)',
      'GMR.DefineProfileMailbox(1935.499, 5545.311, 266.7737, 184944)',
      'GMR.DefineQuestEnemyId(21189)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Test Flight: The Zephyrium Capacitorium |r",
    QuestID = 10557,
    QuestType = "Custom",
    PickUp = { x = 1935.01001, y = 5575.740234, z = 263.752991, id = 21460 },
    TurnIn = { x = 1935.01001, y = 5575.740234, z = 263.752991, id = 21460 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10557, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(1920.310059, 5581.299805, 263.977997, 3) then 
          GMR.MeshTo(1920.310059, 5581.299805, 263.977997)
        else 
          local npc = GMR.GetObjectWithInfo({ id = 21461, rawType = 5 })
          if npc then
            GMR.RunMacroText("/run SelectGossipOption(1)")
            GMR.Questing.GossipWith(npc, nil, nil, nil, nil, 5)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(1760.527, 5907.059, 149.4332, 80)',
      'GMR.DefineUnstuck(1763.301, 5886.455, 152.818)',
      'GMR.DefineUnstuck(1770.372, 5863.521, 157.4362)',
      'GMR.DefineUnstuck(1778.468, 5838.34, 165.9168)',
      'GMR.DefineUnstuck(1786.216, 5812.315, 175.781)',
      'GMR.DefineUnstuck(1801.794, 5796.553, 186.3764)',
      'GMR.DefineUnstuck(1822.841, 5790.845, 196.9913)',
      'GMR.DefineUnstuck(1843.414, 5791.425, 205.4459)',
      'GMR.DefineUnstuck(1855.966, 5786.146, 211.6106)',
      'GMR.DefineUnstuck(1866.602, 5775.526, 217.6131)',
      'GMR.DefineUnstuck(1872.46, 5762.041, 224.8008)',
      'GMR.DefineUnstuck(1869.944, 5740.228, 233.7146)',
      'GMR.DefineUnstuck(1873.999, 5723.889, 240.2751)',
      'GMR.DefineUnstuck(1881.12, 5705.02, 248.0597)',
      'GMR.DefineUnstuck(1888.202, 5691.301, 252.3233)',
      'GMR.DefineUnstuck(1897.214, 5673.845, 257.2349)',
      'GMR.DefineUnstuck(1901.736, 5658.055, 259.3613)',
      'GMR.DefineUnstuck(1906.492, 5643.604, 254.5999)',
      'GMR.DefineUnstuck(1914.247, 5621.436, 251.7434)',
      'GMR.DefineUnstuck(1905.703, 5612.196, 252.915)',
      'GMR.DefineUnstuck(1892.082, 5600.369, 255.1808)',
      'GMR.DefineUnstuck(1886.149, 5586.066, 257.3965)',
      'GMR.DefineUnstuck(1896.457, 5570.771, 260.6483)',
      'GMR.DefineProfileCenter(1920.310059, 5581.299805, 263.977997, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineRepairVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineGoodsVendor(1944.849976, 5537.870117, 266.734009, 21110)',
      'GMR.DefineAmmoVendor(1913.000000, 5522.930176, 267.704010, 21111)',
      'GMR.DefineProfileMailbox(1935.499, 5545.311, 266.7737, 184944)',
      'GMR.DefineQuestEnemyId(9999999)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Test Flight: The Singing Ridge |r",
    QuestID = 10710,
    QuestType = "Custom",
    PickUp = { x = 1935.01001, y = 5575.740234, z = 263.752991, id = 21460 },
    TurnIn = { x = 1935.01001, y = 5575.740234, z = 263.752991, id = 21460 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10710, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(1920.310059, 5581.299805, 263.977997, 3) then 
          GMR.MeshTo(1920.310059, 5581.299805, 263.977997)
        else 
          local npc = GMR.GetObjectWithInfo({ id = 21461, rawType = 5 })
          if npc then
            local itemName = GetItemInfo(30540)
            GMR.Use(itemName)
            GMR.Questing.GossipWith(npc, nil, nil, nil, nil, 5)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(1920.310059, 5581.299805, 263.977997, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineRepairVendor(1910.420044, 5548.359863, 264.015991, 21112)',
      'GMR.DefineGoodsVendor(1944.849976, 5537.870117, 266.734009, 21110)',
      'GMR.DefineAmmoVendor(1913.000000, 5522.930176, 267.704010, 21111)',
      'GMR.DefineProfileMailbox(1935.499, 5545.311, 266.7737, 184944)',
      'GMR.DefineQuestEnemyId(9999999)'
    }
  })
  
  
  
  
  
  
  
  
  -- Start of Netherstorm
  
  
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFF1E90FE Netherstorm Initial Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10190, x = 3352.8420410156, y = 3726.3911132812, z = 141.42945861816, id = 19578 },
      { questId = 10186, x = 3041.7978515625, y = 3658.8542480469, z = 143.32344055176, id = 19570 },
      { questId = 10342, x = 3080.5122070312, y = 3684.4033203125, z = 142.44961547852, id = 19617 },
      { questId = 10173, x = 2997.6091308594, y = 3734.9680175781, z = 143.99571228027, id = 19217 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineHearthstoneBindLocation(3062.149902, 3701.820068, 142.561996, 19571)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Recharging the Batteries |r",
    QuestID = 10190,
    QuestType = "Custom",
    PickUp = { x = 3352.8420410156, y = 3726.3911132812, z = 141.42945861816, id = 19578 },
    TurnIn = { x = 3352.8420410156, y = 3726.3911132812, z = 141.42945861816, id = 19578 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10190, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand04045 then
        GMR.Print("Frame created")
        GMR.Frames.Rand04045 = CreateFrame("frame")
        GMR.Frames.Rand04045:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10190 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10190) then
              if not GMR.Questing.IsObjectiveCompleted(10190, 1) then
                local npc1 = GMR.GetObjectWithInfo({ id = 18879, rawType = 5, isAlive = true })
                local itemName = GetItemInfo(28369)
                if npc1 then
                  if GMR.UnitHealth("target") < 2000 and UnitName("target") == "Phase Hunter" and UnitExists("target") and not UnitIsDead("target") then
                    print("|cFF1E90FE CryptoQuester |r - Using Quest Item on Mob")
                    GMR.Use(itemName)
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand04045 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3500.6330566406, 3889.3696289062, 138.09989929199, 100)',
      'GMR.DefineProfileCenter(3559.7253417969, 3947.4560546875, 124.69414520264, 100)',
      'GMR.DefineProfileCenter(3492.8959960938, 4093.0610351562, 120.40926361084, 100)',
      'GMR.DefineProfileCenter(3535.3037109375, 3922.8933105469, 128.77668762207, 100)',
      'GMR.DefineProfileCenter(3523.6560058594, 3811.6108398438, 131.81944274902, 100)',
      'GMR.DefineProfileCenter(3554.0944824219, 3696.5966796875, 129.74461364746, 100)',
      'GMR.DefineProfileCenter(3605.2880859375, 3600.25, 125.32276916504, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18879)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE You're Hired! |r",
    QuestID = 10186,
    QuestType = "Custom",
    PickUp = { x = 3041.800049, y = 3658.850098, z = 143.406006, id = 19570 },
    TurnIn = { x = 3041.800049, y = 3658.850098, z = 143.406006, id = 19570 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10186, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3405.38, 3683.012, 147.349, 80)',
      'GMR.DefineProfileCenter(3420.494, 3719.714, 139.305, 80)',
      'GMR.DefineProfileCenter(3440.69, 3703.538, 146.6557, 80)',
      'GMR.DefineProfileCenter(3436.005, 3662.99, 152.6678, 80)',
      'GMR.DefineProfileCenter(3408.101, 3614.768, 141.6498, 80)',
      'GMR.DefineProfileCenter(3469.451, 3633.051, 140.6865, 80)',
      'GMR.DefineProfileCenter(3547.728, 3624.199, 125.9213, 80)',
      'GMR.DefineProfileCenter(3572.685, 3627.85, 133.7225, 80)',
      'GMR.DefineProfileCenter(3551.646, 3570.509, 128.2684, 80)',
      'GMR.DefineProfileCenter(3522.594, 3584.737, 144.1414, 80)',
      'GMR.DefineProfileCenter(3510.166, 3579.612, 149.836, 80)',
      'GMR.DefineProfileCenter(3490.055, 3576.005, 158.7035, 80)',
      'GMR.DefineProfileCenter(3499.84, 3624.739, 154.4875, 80)',
      'GMR.DefineProfileCenter(3370.44, 3658.173, 143.7627, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(183767)',
      'GMR.DefineQuestEnemyId(183768)',
      'GMR.DefineCustomObjectId(183767)',
      'GMR.DefineCustomObjectId(183768)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Invaluable Asset Zapping |r",
    QuestID = 10203,
    QuestType = "Custom",
    PickUp = { x = 3041.800049, y = 3658.850098, z = 143.406006, id = 19570 },
    TurnIn = { x = 2923.389893, y = 3577.389893, z = 129.389008, id = 19634 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10203, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2718.104, 3193.008, 147.5978, 3) then 
          GMR.MeshTo(2718.104, 3193.008, 147.5978)
        else
          local object = GMR.GetObjectWithInfo({ id = 183805, rawType = 8 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10203, 2) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2785.722, 3172.982, 146.0239, 3) then 
          GMR.MeshTo(2785.722, 3172.982, 146.0239)
        else
          local object = GMR.GetObjectWithInfo({ id = 183806, rawType = 8 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10203, 3) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2747.715, 3238.774, 147.5974, 3) then 
          GMR.MeshTo(2747.715, 3238.774, 147.5974)
        else
          local object = GMR.GetObjectWithInfo({ id = 183807, rawType = 8 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10203, 4) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2772.285, 3115.489, 154.2348, 3) then 
          GMR.MeshTo(2772.285, 3115.489, 154.2348)
        else
          local object = GMR.GetObjectWithInfo({ id = 183808, rawType = 8 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineCustomObjectId(183805)',
      'GMR.DefineCustomObjectId(183806)',
      'GMR.DefineCustomObjectId(183807)',
      'GMR.DefineCustomObjectId(183808)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Report to Engineering |r",
    QuestID = 10225,
    QuestType = "TalkTo",
    PickUp = { x = 3041.800049, y = 3658.850098, z = 143.406006, id = 19570 },
    TurnIn = { x = 2974.389893, y = 3675.300049, z = 143.457001, id = 19709 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SetChecked("GryphonMasters", false)',
      'GMR.SetChecked("Mount", true)',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Essence For the Engines |r",
    QuestID = 10224,
    QuestType = "Custom",
    PickUp = { x = 2974.389893, y = 3675.300049, z = 143.457001, id = 19709 },
    TurnIn = { x = 2974.389893, y = 3675.300049, z = 143.457001, id = 19709 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10224, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3270.657, 3616.502, 127.4035, 80)',
      'GMR.DefineProfileCenter(3300.521, 3563.51, 125.1808, 80)',
      'GMR.DefineProfileCenter(3308.196, 3466.835, 110.342, 80)',
      'GMR.DefineProfileCenter(3317.546, 3422.343, 120.0963, 30)',
      'GMR.DefineProfileCenter(3184.149, 3486.262, 102.7712, 80)',
      'GMR.DefineProfileCenter(3157.635, 3393.939, 104.5837, 80)',
      'GMR.DefineProfileCenter(3247.882, 3849.952, 139.8129, 80)',
      'GMR.DefineAreaBlacklist(3261.567, 3527.807, 125.6536,15)',
      'GMR.DefineAreaBlacklist(3294.712, 3387.063, 110.0738,20)',
      'GMR.DefineAreaBlacklist(3260.509, 3392.659, 111.6965,15)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18864)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Elemental Power Extraction |r",
    QuestID = 10226,
    QuestType = "Custom",
    PickUp = { x = 2974.389893, y = 3675.300049, z = 143.457001, id = 19709 },
    TurnIn = { x = 2974.389893, y = 3675.300049, z = 143.457001, id = 19709 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10226, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand048 then
        GMR.Print("Frame created")
        GMR.Frames.Rand048 = CreateFrame("frame")
        GMR.Frames.Rand048:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10226 then
            if GMR.IsExecuting() and GMR.IsQuestActive(10226) then
              if not GMR.Questing.IsObjectiveCompleted(10226, 1) then
                local npc1 = GMR.GetObjectWithInfo({ id = 18881, rawType = 5, isAlive = true })
                local npc2 = GMR.GetObjectWithInfo({ id = 18865, rawType = 5, isAlive = true })
                local object1 = GMR.GetObjectWithInfo({ id = 183933, rawType = 5 })
                local itemName = GetItemInfo(28547)
                if object1 then
                  GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
                elseif npc1 and UnitName("target") == "Sundered Rumbler" and not UnitIsDead("target") and GMR.UnitHealth("target") < 3000 and not GMR.GetDelay("CustomQuest") then
                  GMR.Use(itemName); GMR.SetDelay("CustomQuest", 3)
                elseif npc2 and UnitName("target") == "Warp Aberration" and not UnitIsDead("target") and GMR.UnitHealth("target") < 3000 and not GMR.GetDelay("CustomQuest") then
                  GMR.Use(itemName); GMR.SetDelay("CustomQuest", 3)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand048 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(2640.933, 3507.549, 141.4406, 80)',
      'GMR.DefineProfileCenter(2572.397, 3548.674, 138.5618, 80)',
      'GMR.DefineProfileCenter(2588.554, 3595.84, 138.8191, 80)',
      'GMR.DefineProfileCenter(3293.247, 3527.901, 118.3056, 80)',
      'GMR.DefineProfileCenter(3265.919, 3434.099, 93.30744, 80)',
      'GMR.BlacklistId(20772)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18881)',
      'GMR.DefineQuestEnemyId(18865)',
      'GMR.DefineCustomObjectId(183933)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Securing the Shaleskin Shale |r",
    QuestID = 10342,
    QuestType = "Custom",
    PickUp = { x = 3080.51001, y = 3684.399902, z = 142.533005, id = 19617 },
    TurnIn = { x = 3080.51001, y = 3684.399902, z = 142.533005, id = 19617 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10342, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3160.561, 3744.838, 137.1864, 120)',
      'GMR.DefineProfileCenter(3115.245, 3866.98, 143.439, 120)',
      'GMR.DefineProfileCenter(2947.201, 3808.284, 154.7321, 120)',
      'GMR.DefineProfileCenter(2904.684, 3770.878, 153.1888, 120)',
      'GMR.DefineProfileCenter(2737.778, 3612.975, 140.3762, 120)',
      'GMR.DefineProfileCenter(2789.392, 3543.679, 143.863, 120)',
      'GMR.DefineProfileCenter(2678.526, 3518.311, 137.3837, 120)',
      'GMR.DefineProfileCenter(2900.498, 3775.436, 155.1407, 120)',
      'GMR.DefineProfileCenter(3124.734, 3957.557, 170.3251, 120)',
      'GMR.DefineProfileCenter(3214.412, 3968.849, 159.914, 120)',
      'GMR.DefineProfileCenter(3325.958, 3944.423, 182.9826, 120)',
      'GMR.DefineProfileCenter(3369.162, 4042.61, 166.6949, 120)',
      'GMR.DefineProfileCenter(3424.857, 3840.448, 145.127, 120)',
      'GMR.DefineProfileCenter(3168.183, 3780.47, 139.3202, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20210)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE That Little Extra Kick |r",
    QuestID = 10199,
    QuestType = "Custom",
    PickUp = { x = 3080.51001, y = 3684.399902, z = 142.533005, id = 19617 },
    TurnIn = { x = 3080.51001, y = 3684.399902, z = 142.533005, id = 19617 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10199, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3505.6042480469, 4054.4572753906, 124.10459136963, 100)',
      'GMR.DefineProfileCenter(3497.8540039062, 3787.1342773438, 142.10227966309, 100)',
      'GMR.DefineProfileCenter(3175.9445800781, 3782.5786132812, 137.79083251953, 100)',
      'GMR.DefineProfileCenter(3129.5278320312, 3576.140625, 143.35781860352, 100)',
      'GMR.DefineProfileCenter(3120.6459960938, 3342.1196289062, 107.48588562012, 100)',
      'GMR.DefineProfileCenter(2708.8012695312, 3569.4692382812, 135.38012695312, 100)',
      'GMR.DefineProfileCenter(3102.4892578125, 3848.951171875, 144.2268371582, 100)',
      'GMR.DefineProfileCenter(3373.5893554688, 4096.630859375, 149.8257598877, 100)',
      'GMR.DefineProfileCenter(3336.8627929688, 4025.5637207031, 164.46528625488, 100)',
      'GMR.DefineProfileCenter(3266.8078613281, 3939.4606933594, 175.12074279785, 100)',
      'GMR.DefineProfileCenter(3151.8044433594, 4055.4494628906, 135.06872558594, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18880)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE The Archmage's Staff |r",
    QuestID = 10173,
    QuestType = "Custom",
    PickUp = { x = 2997.610107, y = 3734.969971, z = 144.078003, id = 19217 },
    TurnIn = { x = 2997.610107, y = 3734.969971, z = 144.078003, id = 19217 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10173, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2760.220703, 3206.056152, 150.734634, 3) then 
          GMR.MeshTo(2760.220703, 3206.056152, 150.734634)
        else
          local npc1 = GMR.GetObjectWithInfo({ id = 19493, rawType = 5 })
          if npc1 then
            GMR.Questing.InteractWith(npc1, nil, nil, nil, nil, 5)
          else
            local itemName = GetItemInfo(29207)
            GMR.Use(itemName)
          end
        end
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(2760.220703, 3206.056152, 150.734634, 80)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19493)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Rebuilding the Staff |r",
    QuestID = 10300,
    QuestType = "Custom",
    PickUp = { x = 2997.610107, y = 3734.969971, z = 144.078003, id = 19217 },
    TurnIn = { x = 2997.610107, y = 3734.969971, z = 144.078003, id = 19217 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10300, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(3339.364, 3660.819, 136.9932, 80)',
      'GMR.DefineProfileCenter(3377.939, 3635.542, 140.7003, 80)',
      'GMR.DefineProfileCenter(3403.14, 3698.08, 145.0441, 80)',
      'GMR.DefineProfileCenter(3464.379, 3753.433, 149.146, 80)',
      'GMR.DefineProfileCenter(3498.523, 3714.123, 139.0821, 80)',
      'GMR.DefineProfileCenter(3429.647, 3607.55, 141.4064, 80)',
      'GMR.DefineProfileCenter(3450.708, 3677.134, 149.9592, 80)',
      'GMR.DefineProfileCenter(3410.063, 3659.37, 150.2314, 80)',
      'GMR.DefineAreaBlacklist(3492.101, 3581.442, 157.6862,15)',
      'GMR.DefineAreaBlacklist(3486.745, 3595.601, 159.5798,15)',
      'GMR.DefineAreaBlacklist(3473.485, 3578.425, 166.673,15)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18872)',
      'GMR.DefineQuestEnemyId(18873)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFF1E90FE Curse of the Violet Tower |r",
    QuestID = 10174,
    QuestType = "Custom",
    PickUp = { x = 2997.610107, y = 3734.969971, z = 144.078003, id = 19217 },
    TurnIn = { x = 2987.530762, y = 3714.925537, z = 142.84671, id = 19644 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10174, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2987.295654, 3715.135254, 142.812515, 3) then 
          GMR.MeshTo(2987.295654, 3715.135254, 142.812515)
        else
          local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
          if npc1 then
            GMR.Questing.GossipWith(npc1, nil, nil, nil, nil, 5)
          else
            local itemName = GetItemInfo(28455)
            GMR.Use(itemName)
          end
        end
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand04349 then
        GMR.Print("Frame created")
        GMR.Frames.Rand04349 = CreateFrame("frame")
        GMR.Frames.Rand04349:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10174 then
            if GMR.IsExecuting() then
              if not GMR.IsQuestActive(10174) then
                local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
                local itemName = GetItemInfo(28455)
                GMR.Use(itemName)
              else
                if not GMR.Questing.IsObjectiveCompleted(10174, 1) and GMR.IsQuestActive(10174) then
                  if not GMR.IsPlayerPosition(2987.530762, 3714.925537, 142.846710, 3) then 
                    GMR.MeshTo(2987.530762, 3714.925537, 142.846710)
                  else
                    local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
                    local itemName = GetItemInfo(28455)
                    GMR.Use(itemName)
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand04349 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.DefineProfileCenter(2987.295654, 3715.135254, 142.812515, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999, 19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19644)'
    }
  })
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 Netherstorm - Town Square Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10343, x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
      { questId = 10184, x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
      { questId = 10185, x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
      { questId = 10331, x = 2290.120117, y = 2266.040039, z = 93.719498, id = 20463 },
      { questId = 10334, x = 2299.379883, y = 2265.280029, z = 95.441902, id = 20464 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2523.951, 2944.325, 118.4967, 60)',
      'GMR.DefineUnstuck(2517.601, 2936.458, 121.1372)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineUnstuck(2482.893, 2442.448, 132.1952)',
      'GMR.DefineUnstuck(2472.164, 2426.935, 136.2992)',
      'GMR.DefineUnstuck(2452.702, 2414.204, 141.9097)',
      'GMR.DefineUnstuck(2431.788, 2400.524, 142.3157)',
      'GMR.DefineUnstuck(2416.352, 2390.137, 143.2151)',
      'GMR.DefineUnstuck(2393.837, 2387.336, 141.1623)',
      'GMR.DefineUnstuck(2375.145, 2385.696, 132.8179)',
      'GMR.DefineUnstuck(2355.827, 2384, 123.1692)',
      'GMR.DefineUnstuck(2342.662, 2365.953, 116.7492)',
      'GMR.DefineUnstuck(2334.171, 2344.017, 110.9792)',
      'GMR.DefineUnstuck(2326.613, 2324.491, 105.6802)',
      'GMR.DefineUnstuck(2320.031, 2307.487, 101.8017)',
      'GMR.DefineUnstuck(2306.556, 2294.463, 98.15685)',
      'GMR.DefineUnstuck(2293.304, 2285.872, 95.86809)',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Indispensable Tools |r",
    QuestID = 10331,
    QuestType = "Custom",
    PickUp = { x = 2290.120117, y = 2266.040039, z = 93.719498, id = 20463 },
    TurnIn = { x = 2290.120117, y = 2266.040039, z = 93.719498, id = 20463 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10331, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.SetChecked("DisplayUnstuck", true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2304.5, 2087.881, 76.74524, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20409)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Master Smith Rhonsus |r",
    QuestID = 10332,
    QuestType = "Custom",
    PickUp = { x = 2290.120117, y = 2266.040039, z = 93.719498, id = 20463 },
    TurnIn = { x = 2290.120117, y = 2266.040039, z = 93.719498, id = 20463 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10332, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2295.739990, 2100.040039, 78.142998, 120)',
      'GMR.DefineAreaBlacklist(2293.083, 2058.405, 72.03351,15)',
      'GMR.DefineAreaBlacklist(2300.166, 2141.037, 87.46978,15)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20410)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Unending Invasion |r",
    QuestID = 10343,
    QuestType = "Custom",
    PickUp = { x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
    TurnIn = { x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10343, 1) then
        GMR.SetQuestingState(nil);
        GMR.Questing.InteractWith(2217.363, 2380.094, 103.7534, 184433)
        end
      end
    ]],
    Profile = {
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2217.363, 2380.094, 103.7534, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(184433)',
      'GMR.DefineCustomObjectId(184433)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Potential Energy Source |r",
    QuestID = 10239,
    QuestType = "Gathering",
    PickUp = { x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
    TurnIn = { x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2523.951, 2944.325, 118.4967, 60)',
      'GMR.DefineProfileCenter(2315.012, 2705.926, 127.3617, 40)',
      'GMR.DefineProfileCenter(2351.681, 2685.871, 126.8605, 40)',
      'GMR.DefineProfileCenter(2374.884, 2670.387, 131.4475, 40)',
      'GMR.DefineProfileCenter(2376.053, 2702.329, 133.0845, 40)',
      'GMR.DefineProfileCenter(2405.891, 2720.258, 134.4934, 40)',
      'GMR.DefineProfileCenter(2382.09, 2726.12, 135.1858, 40)',
      'GMR.DefineProfileCenter(2380.235, 2734.597, 135.1797, 40)',
      'GMR.DefineProfileCenter(2364.206, 2746.371, 135.1859, 40)',
      'GMR.DefineProfileCenter(2363.726, 2766.715, 135.1862, 40)',
      'GMR.DefineProfileCenter(2384.069, 2790.723, 135.1846, 40)',
      'GMR.DefineProfileCenter(2395.414, 2805.436, 134.4926, 40)',
      'GMR.DefineProfileCenter(2418.741, 2806.928, 135.1859, 40)',
      'GMR.DefineProfileCenter(2434.068, 2794.674, 134.4927, 40)',
      'GMR.DefineProfileCenter(2454.048, 2773.163, 135.1857, 40)',
      'GMR.DefineProfileCenter(2453.444, 2754.109, 134.4929, 40)',
      'GMR.DefineProfileCenter(2442.408, 2735.943, 134.9884, 40)',
      'GMR.DefineProfileCenter(2405.817, 2721.662, 134.4927, 40)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(183945)',
      'GMR.DefineCustomObjectId(183945)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Building a Perimeter |r",
    QuestID = 10240,
    QuestType = "Custom",
    PickUp = { x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
    TurnIn = { x = 2248.72998, y = 2273.100098, z = 97.913902, id = 19489 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10240, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2283.821, 2181.041, 92.41496, 3) then 
          GMR.MeshTo(2283.821, 2181.041, 92.41496)
        else
          local itemName = GetItemInfo(28725)
          GMR.Use(itemName)
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10240, 2) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2376.416, 2285.157, 135.5208, 3) then 
          GMR.MeshTo(2376.416, 2285.157, 135.5208)
        else
          local itemName = GetItemInfo(28725)
          GMR.Use(itemName)
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10240, 3) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2257.126, 2456.163, 99.25587, 3) then 
          GMR.MeshTo(2257.126, 2456.163, 99.25587)
        else
          local itemName = GetItemInfo(28725)
          GMR.Use(itemName)
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2283.821, 2181.041, 92.41496, 120)',
      'GMR.DefineProfileCenter(2376.416, 2285.157, 135.5208, 120)',
      'GMR.DefineProfileCenter(2257.126, 2456.163, 99.25587, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(99999)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Fate Worse Than Death |r",
    QuestID = 10185,
    QuestType = "Custom",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10185, 1) then
        local enemy = GMR.GetObjectWithInfo({ id = 18867, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10185, 2) then
        local enemy = GMR.GetObjectWithInfo({ id = 18866, canAttack = true, isAlive = true })
        if enemy then
          GMR.SetQuestingState(nil)
          GMR.Questing.KillEnemy(enemy)
        else
          GMR.SetQuestingState("Idle")
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2261.776, 2301.74, 92.00246, 120)',
      'GMR.DefineProfileCenter(2181.527, 2328.17, 84.76455, 120)',
      'GMR.DefineProfileCenter(2138.346, 2370.636, 78.79906, 120)',
      'GMR.DefineProfileCenter(2132.102, 2427.883, 89.31176, 120)',
      'GMR.DefineProfileCenter(2213.572, 2386.593, 104.124, 120)',
      'GMR.DefineProfileCenter(2163.273, 2414.094, 96.45605, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18867)',
      'GMR.DefineQuestEnemyId(18866)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Malevolent Remnants |r",
    QuestID = 10184,
    QuestType = "Custom",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10184, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2161.556, 2241.119, 74.92451, 120)',
      'GMR.DefineProfileCenter(2173.115, 2188.941, 71.28829, 120)',
      'GMR.DefineProfileCenter(2221.925, 2140.031, 73.4385, 120)',
      'GMR.DefineProfileCenter(2304.839, 2200.62, 95.67525, 120)',
      'GMR.DefineProfileCenter(2307.761, 2155.04, 89.83915, 120)',
      'GMR.DefineProfileCenter(2333.146, 2102.68, 84.29382, 120)',
      'GMR.DefineProfileCenter(2304.21, 2185.656, 92.95027, 120)',
      'GMR.DefineProfileCenter(2177.037, 2176.252, 71.27239, 120)',
      'GMR.DefineProfileCenter(2138.492, 2265.262, 73.89286, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19881)',
      'GMR.DefineQuestEnemyId(20934)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Annals of Kirin'Var |r",
    QuestID = 10312,
    QuestType = "Custom",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10312, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2197.854004, 2110.575439, 71.427010, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19543)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Battle-Mage Dathric - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 10182,
    QuestType = "Grinding",
    PickUp = { x = 2196.315918, y = 2107.440186, z = 71.008163, id = { 19543, 29233 } },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand054 then
        GMR.Print("Frame created")
        GMR.Frames.Rand054 = CreateFrame("frame")
        GMR.Frames.Rand054:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10182 then
            if GMR.IsQuestActive(10182) and GMR.Questing.IsObjectiveCompleted(10182, 1) and not GMR.InCombat() then
              if GossipFrame:IsShown() then
                GossipTitleButton3:Click()
              end
            elseif GMR.IsExecuting() and GetItemCount(29233) >= 1 and not GMR.IsQuestActive(10182) then
              local itemName = GetItemInfo(29233)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            elseif GMR.IsExecuting() and GMR.IsQuestActive(10182) and not GMR.Questing.IsObjectiveCompleted(10182, 1) and not GMR.InCombat() then
              if not GMR.IsPlayerPosition(2228.7556152344, 2312.099609375, 89.249847412109, 3) then 
                GMR.MeshTo(2228.7556152344, 2312.099609375, 89.249847412109)
              else
                local itemName = GetItemInfo(28351)
                GMR.Use(itemName)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand054 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2228.633, 2312.514, 89.34054, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19543)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Cohlin Frostweaver - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 10307,
    QuestType = "Grinding",
    PickUp = { x = 2180.318115, y = 2211.72876, z = 76.806007, id = { 19545, 29236 } },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand054 then
        GMR.Print("Frame created")
        GMR.Frames.Rand054 = CreateFrame("frame")
        GMR.Frames.Rand054:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10307 then
            if GMR.IsQuestActive(10307) and GMR.Questing.IsObjectiveCompleted(10307, 1) and not GMR.InCombat() then
              if GossipFrame:IsShown() then
                GossipTitleButton3:Click()
              end
            elseif GMR.IsExecuting() and GetItemCount(29236) >= 1 and not GMR.IsQuestActive(10307) then
              local itemName = GetItemInfo(29236)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            elseif GMR.IsExecuting() and GMR.IsQuestActive(10307) and not GMR.Questing.IsObjectiveCompleted(10307, 1) and not GMR.InCombat() then
              if not GMR.IsPlayerPosition(2202.637207, 2412.487549, 109.223251, 3) then 
                GMR.MeshTo(2202.637207, 2412.487549, 109.223251)
              else
                local itemName2 = GetItemInfo(28353)
                GMR.Use(itemName2)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand054 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2202.637207, 2412.487549, 109.223251, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19545)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Conjurer Luminrath - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 10306,
    QuestType = "Grinding",
    PickUp = { x = 2278.3776855469, y = 2144.5764160156, z = 83.678695678711, id = { 19544, 29235 } },
    TurnIn = { x = 2247.1135253906, y = 2277.4270019531, z = 97.82836151123, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand056 then
        GMR.Print("Frame created")
        GMR.Frames.Rand056 = CreateFrame("frame")
        GMR.Frames.Rand056:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10306 then
            if GMR.IsQuestActive(10306) and GMR.Questing.IsObjectiveCompleted(10306, 1) and not GMR.InCombat() then
              if GossipFrame:IsShown() then
                GossipTitleButton3:Click()
              end
            elseif GMR.IsExecuting() and GetItemCount(29235) >= 1 and not GMR.IsQuestActive(10306) then
              local itemName = GetItemInfo(29235)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            elseif GMR.IsExecuting() and GMR.IsQuestActive(10306) and not GMR.Questing.IsObjectiveCompleted(10306, 1) and not GMR.InCombat() then
              if not GMR.IsPlayerPosition(2193.0107421875, 2338.6037597656, 89.80696105957, 3) then 
                GMR.MeshTo(2193.0107421875, 2338.6037597656, 89.80696105957)
              else
                local itemName = GetItemInfo(28352)
                GMR.Use(itemName)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand056 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2193.0107421875, 2338.6037597656, 89.80696105957, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19544)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Abjurist Belmara - FARMITEM |r - FarmItemAndAcceptQuest",
    QuestID = 10305,
    QuestType = "Grinding",
    PickUp = { x = 2278.3776855469, y = 2144.5764160156, z = 83.678695678711, id = { 19546, 29234 } },
    TurnIn = { x = 2247.1135253906, y = 2277.4270019531, z = 97.82836151123, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
          if not GMR.Frames.Rand056 then
        GMR.Print("Frame created")
        GMR.Frames.Rand056 = CreateFrame("frame")
        GMR.Frames.Rand056:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10305 then
            if GMR.IsQuestActive(10305) and GMR.Questing.IsObjectiveCompleted(10305, 1) and not GMR.InCombat() then
              if GossipFrame:IsShown() then
                GossipTitleButton3:Click()
              end
            elseif GMR.IsExecuting() and GetItemCount(29234) >= 1 and not GMR.IsQuestActive(10305) then
              local itemName = GetItemInfo(29234)
              GMR.Use(itemName)
              if QuestFrameDetailPanel:IsShown() or QuestFrame:IsShown() or GossipFrameGreetingPanel:IsShown() then
                if QuestFrameAcceptButton:IsShown() then
                  QuestFrameAcceptButton:Click()
                else
                  QuestFrameAcceptButton:Click()
                end
              end
            elseif GMR.IsExecuting() and GMR.IsQuestActive(10305) and not GMR.Questing.IsObjectiveCompleted(10305, 1) and not GMR.InCombat() then
              if not GMR.IsPlayerPosition(2240.2922363281, 2390.2658691406, 112.42930603027, 3) then 
                GMR.MeshTo(2240.2922363281, 2390.2658691406, 112.42930603027)
              else
                local itemName = GetItemInfo(28336)
                GMR.Use(itemName)
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand056 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2170.1044921875, 2199.1298828125, 72.086639404297, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19546)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Searching for Evidence |r",
    QuestID = 10316,
    QuestType = "TalkTo",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2558.278, y = 2121.487, z = 99.49392, id = 184300 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2558.278, 2121.487, 99.49392, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 A Lingering Suspicion |r",
    QuestID = 10314,
    QuestType = "Custom",
    PickUp = { x = 2558.278, y = 2121.487, z = 99.49392, id = 184300 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10314, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2518.459, 2148.099, 98.29564, 60)',
      'GMR.DefineProfileCenter(2436.823, 2195.767, 99.40038, 60)',
      'GMR.DefineProfileCenter(2436.229, 2176.905, 94.892, 60)',
      'GMR.DefineProfileCenter(2398.842, 2158.474, 86.65745, 60)',
      'GMR.DefineProfileCenter(2419.647, 2138.562, 87.9066, 60)',
      'GMR.DefineProfileCenter(2445, 2155.076, 92.30861, 60)',
      'GMR.DefineProfileCenter(2426.468, 2129.95, 87.53622, 60)',
      'GMR.DefineProfileCenter(2426.998, 2095.067, 83.38469, 60)',
      'GMR.DefineProfileCenter(2455.764, 2128.676, 90.66583, 60)',
      'GMR.DefineProfileCenter(2495.482, 2121.886, 96.9781, 60)',
      'GMR.DefineProfileCenter(2499.959, 2037.248, 85.32538, 60)',
      'GMR.DefineProfileCenter(2488.785, 1994.763, 88.11781, 60)',
      'GMR.DefineProfileCenter(2509.917, 2067.802, 92.77258, 60)',
      'GMR.DefineProfileCenter(2523.191, 2108.042, 96.26222, 60)',
      'GMR.DefineAreaBlacklist(2526.222, 1993.508, 90.25967,20)',
      'GMR.DefineAreaBlacklist(2527.292, 2015.2, 90.10416,15)',
      'GMR.BlacklistId(20483)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(21065)',
      'GMR.DefineQuestEnemyId(20480)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Capturing the Phylactery |r",
    QuestID = 10319,
    QuestType = "Custom",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10319, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(2468.783203, 2144.723633, 94.011230, 3) then 
          GMR.MeshTo(2468.783203, 2144.723633, 94.011230)
        else 
          local object = GMR.GetObjectWithInfo({ id = 184310, rawType = 8 })
          if not GMR.GetDelay("CustomQuest") and object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5); GMR.SetDelay("CustomQuest", 2)
          end 
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2468.783203, 2144.723633, 94.011230, 120)',
      'GMR.DefineAreaBlacklist(2526.222, 1993.508, 90.25967,20)',
      'GMR.DefineAreaBlacklist(2527.292, 2015.2, 90.10416,15)',
      'GMR.BlacklistId(20483)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(184310)',
      'GMR.DefineCustomObjectId(184310)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Needs More Cowbell |r",
    QuestID = 10334,
    QuestType = "TalkTo",
    PickUp = { x = 2299.379883, y = 2265.280029, z = 95.441902, id = 20464 },
    TurnIn = { x = 2528, y = 2185.679932, z = 102.599998, id = 20415 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2528.000000, 2185.679932, 102.599998, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Sigil of Krasus |r",
    QuestID = 10188,
    QuestType = "Custom",
    PickUp = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    TurnIn = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10188, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
        if not GMR.Frames.Rand085 then
        GMR.Print("Frame created")
        GMR.Frames.Rand085 = CreateFrame("frame")
        GMR.Frames.Rand085:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10188 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10188) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              elseif GMR.Questing.IsObjectiveCompleted(10188, 1) and GMR.IsQuestActive(10188) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand085 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2437.3544921875, 2593.1271972656, 134.32272338867, 100)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19593)'
    }
  })
  
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Krasus's Compendium |r",
    QuestID = 10192,
    QuestType = "Custom",
    PickUp = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    TurnIn = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10192, 1) then
        GMR.SetQuestingState(nil)
        if not GMR.IsPlayerPosition(2140.802246, 2216.196533, 75.192543, 3) then 
          GMR.MeshTo(2140.802246, 2216.196533, 75.192543)
        else
          local object1 = GMR.GetObjectWithInfo({ id = 184121, rawType = 8 })
          if object1 then
            GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
          end
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10192, 2) then
        GMR.SetQuestingState(nil)
        if not GMR.IsPlayerPosition(2127.024, 2274.891, 73.17466, 3) then 
          GMR.MeshTo(2127.024, 2274.891, 73.17466)
        else
          local object1 = GMR.GetObjectWithInfo({ id = 184122, rawType = 8 })
          if object1 then
            GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
          end
        end
      elseif not GMR.Questing.IsObjectiveCompleted(10192, 3) then
        GMR.SetQuestingState(nil)
        if not GMR.IsPlayerPosition(2187.451, 2204.063, 77.55714, 3) then 
          GMR.MeshTo(2187.451, 2204.063, 77.55714)
        else
          local object1 = GMR.GetObjectWithInfo({ id = 184123, rawType = 8 })
          if object1 then
            GMR.Questing.InteractWith(object1, nil, nil, nil, nil, 5)
          end
        end
      elseif GMR.Questing.IsObjectiveCompleted(10192, 1) and GMR.Questing.IsObjectiveCompleted(10192, 2) and GMR.Questing.IsObjectiveCompleted(10192, 3) then
        if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
          GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
        else
          local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
            GMR.Use(28455)
        end
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand089 then
        GMR.Print("Frame created")
        GMR.Frames.Rand089 = CreateFrame("frame")
        GMR.Frames.Rand089:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10192 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10192) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local npc1 = GMR.GetObjectWithInfo({ id = 19644, rawType = 5 })
                    local itemName = GetItemInfo(28455)
                    GMR.Use(itemName)
                  end
                end
              elseif GMR.Questing.IsObjectiveCompleted(10192, 1) and GMR.Questing.IsObjectiveCompleted(10192, 2) and GMR.Questing.IsObjectiveCompleted(10192, 3) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                    local itemName = GetItemInfo(28455)
                    GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand089 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2140.802246, 2216.196533, 75.192543, 80)',
      'GMR.DefineProfileCenter(2127.024, 2274.891, 73.17466, 80)',
      'GMR.DefineProfileCenter(2187.451, 2204.063, 77.55714, 80)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(184121)',
      'GMR.DefineQuestEnemyId(184122)',
      'GMR.DefineQuestEnemyId(184123)',
      'GMR.DefineCustomObjectId(184121)',
      'GMR.DefineCustomObjectId(184122)',
      'GMR.DefineCustomObjectId(184123)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 Unlocking the Compendium |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10301, x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      [[
        if not GMR.Frames.Rand088 then
        GMR.Print("Frame created")
        GMR.Frames.Rand088 = CreateFrame("frame")
        GMR.Frames.Rand088:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10301 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10301) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand088 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 The Sunfury Garrison |r",
    QuestID = 10222,
    QuestType = "Custom",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10222, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SetChecked("DisplayUnstuck", true)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2493.14, 2414.378, 134.0273, 60)',
      'GMR.DefineProfileCenter(2525.762, 2377.117, 128.2126, 60)',
      'GMR.DefineProfileCenter(2532.621, 2313.193, 116.0152, 60)',
      'GMR.DefineProfileCenter(2563.685, 2325.799, 115.594, 60)',
      'GMR.DefineProfileCenter(2584.571, 2371.52, 109.4796, 60)',
      'GMR.DefineProfileCenter(2575.594, 2386.21, 109.478, 60)',
      'GMR.BlacklistId(20483)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19707)',
      'GMR.DefineQuestEnemyId(20221)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Unlocking the Compendium |r",
    QuestID = 10301,
    QuestType = "Custom",
    PickUp = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    TurnIn = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10301, 1) then
        GMR.SetQuestingState("Idle")
      elseif GMR.Questing.IsObjectiveCompleted(10301, 1) and GMR.IsQuestActive(10301) then
        if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
          GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
        else
            GMR.Use(28455)
        end
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand090 then
        GMR.Print("Frame created")
        GMR.Frames.Rand090 = CreateFrame("frame")
        GMR.Frames.Rand090:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10301 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10301) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              elseif GMR.Questing.IsObjectiveCompleted(10301, 1) and GMR.IsQuestActive(10301) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand090 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2578.288, 2386.533, 109.4778, 120)',
      'GMR.DefineProfileCenter(2520.591, 2356.194, 128.4029, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19926)'
    }
  })
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 Summoner Kathins Prize / Down with Dalis |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10209, x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
      { questId = 10223, x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      [[
        if not GMR.Frames.Rand095 then
        GMR.Print("Frame created")
        GMR.Frames.Rand095 = CreateFrame("frame")
        GMR.Frames.Rand095:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10209 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10209) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand095 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Summoner Kathins Prize |r",
    QuestID = 10209,
    QuestType = "Custom",
    PickUp = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    TurnIn = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10209, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
        if not GMR.Frames.Rand096 then
        GMR.Print("Frame created")
        GMR.Frames.Rand096 = CreateFrame("frame")
        GMR.Frames.Rand096:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10209 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10209) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              elseif GMR.IsQuestActive(10209) and GMR.IsPlayerPosition(2965.7177734375, 2325.0131835938, 152.61526489258, 80) then
                GMR.RunMacroText("/dismount")
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand096 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(true)',
      -- Run To Area
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2331.5349121094, 2334.2236328125, 108.6305770874, 50)',
      'GMR.DefineUnstuck(2334.2390136719, 2346.0461425781, 111.38324737549)',
      'GMR.DefineUnstuck(2338.6486816406, 2358.4689941406, 114.67204284668)',
      'GMR.DefineUnstuck(2343.9392089844, 2369.3176269531, 117.62169647217)',
      'GMR.DefineUnstuck(2349.849609375, 2376.4321289062, 120.550758361825)',
      'GMR.DefineUnstuck(2364.333984375, 2378.4755859375, 127.25338745117)',
      'GMR.DefineUnstuck(2376.7609863281, 2380.1022949219, 134.51481628418)',
      'GMR.DefineUnstuck(2386.26953125, 2380.0661621094, 141.11051940918)',
      'GMR.DefineUnstuck(2396.01171875, 2377.5361328125, 145.41529846191)',
      'GMR.DefineUnstuck(2409.6018066406, 2375.146484375, 144.61755371094)',
      'GMR.DefineUnstuck(2421.5327148438, 2378.0844726562, 143.13664245605)',
      'GMR.DefineUnstuck(2433.9946289062, 2389.6125488281, 142.87600708008)',
      'GMR.DefineUnstuck(2445.2846679688, 2401.611328125, 141.47927856445)',
      'GMR.DefineUnstuck(2455.0036621094, 2410.3547363281, 140.94586181641)',
      'GMR.DefineUnstuck(2468.7377929688, 2420.4484863281, 137.65483093262)',
      'GMR.DefineUnstuck(2479.0588378906, 2426.7451171875, 134.7092590332)',
      'GMR.DefineUnstuck(2485.0339355469, 2436.5126953125, 132.61557006836)',
      'GMR.DefineUnstuck(2489.2966308594, 2457.1555175781, 129.97064208984)',
      'GMR.DefineUnstuck(2491.5637207031, 2479.6083984375, 129.13096618652)',
      'GMR.DefineUnstuck(2497.0515136719, 2499.2863769531, 129.05114746094)',
      'GMR.DefineUnstuck(2501.2934570312, 2514.4978027344, 128.99543762207)',
      'GMR.DefineUnstuck(2505.0993652344, 2528.1442871094, 129.15853881836)',
      'GMR.DefineUnstuck(2515.5437011719, 2536.2209472656, 129.27755737305)',
      'GMR.DefineUnstuck(2529.5952148438, 2539.8015136719, 128.76745605469)',
      'GMR.DefineUnstuck(2543.8295898438, 2537.7885742188, 123.38076782227)',
      'GMR.DefineUnstuck(2558.6772460938, 2537.8017578125, 116.36971282959)',
      'GMR.DefineUnstuck(2572.7502441406, 2534.3464355469, 112.40103912354)',
      'GMR.DefineUnstuck(2588.4853515625, 2528.7165527344, 114.14424133301)',
      'GMR.DefineUnstuck(2600.1606445312, 2520.9919433594, 114.49829101562)',
      'GMR.DefineUnstuck(2613.8820800781, 2519.9133300781, 113.39822387695)',
      'GMR.DefineUnstuck(2625.5014648438, 2525.8920898438, 110.82959747314)',
      'GMR.DefineUnstuck(2635.4802246094, 2536.9245605469, 113.52500915527)',
      'GMR.DefineUnstuck(2640.6218261719, 2550.6923828125, 113.33195495605)',
      'GMR.DefineUnstuck(2641.3857421875, 2565.04296875, 116.41799163818)',
      'GMR.DefineUnstuck(2643.912109375, 2579.7897949219, 117.81134796143)',
      'GMR.DefineUnstuck(2648.7731933594, 2587.8942871094, 117.29885101318)',
      'GMR.DefineUnstuck(2659.8618164062, 2597.0793457031, 111.41330718994)',
      'GMR.DefineUnstuck(2669.759765625, 2605.7858886719, 109.28276062012)',
      'GMR.DefineUnstuck(2680.9775390625, 2611.70703125, 105.44639587402)',
      'GMR.DefineUnstuck(2694.98046875, 2612.2236328125, 102.85198974609)',
      'GMR.DefineUnstuck(2710.4592285156, 2611.9016113281, 102.47798156738)',
      'GMR.DefineUnstuck(2724.6091308594, 2612.3415527344, 102.48071289062)',
      'GMR.DefineUnstuck(2738.1098632812, 2612.7612304688, 102.35719299316)',
      'GMR.DefineUnstuck(2753.8947753906, 2615.8239746094, 102.34451293945)',
      'GMR.DefineUnstuck(2771.3518066406, 2619.2353515625, 102.33707427979)',
      'GMR.DefineUnstuck(2788.0815429688, 2620.3649902344, 102.47158050537)',
      'GMR.DefineUnstuck(2804.8061523438, 2621.7412109375, 102.47158050537)',
      'GMR.DefineUnstuck(2821.3591308594, 2624.1743164062, 103.69455718994)',
      'GMR.DefineUnstuck(2839.794921875, 2624.6604003906, 108.46675872803)',
      'GMR.DefineUnstuck(2856.6491699219, 2623.0463867188, 111.46959686279)',
      'GMR.DefineUnstuck(2875.6708984375, 2613.6494140625, 113.16915130615)',
      'GMR.DefineUnstuck(2884.6872558594, 2598.8681640625, 115.85272979736)',
      'GMR.DefineUnstuck(2887.7060546875, 2584.7556152344, 116.66623687744)',
      'GMR.DefineUnstuck(2889.9721679688, 2567.822265625, 119.76039886475)',
      'GMR.DefineUnstuck(2892.7702636719, 2553.6135253906, 120.06839752197)',
      'GMR.DefineUnstuck(2893.4379882812, 2540.6508789062, 118.34586334229)',
      'GMR.DefineUnstuck(2895.501953125, 2526.4035644531, 114.95951843262)',
      'GMR.DefineUnstuck(2898.0268554688, 2517.1896972656, 114.59183502197)',
      'GMR.DefineUnstuck(2901.1325683594, 2504.7487792969, 116.83312988281)',
      'GMR.DefineUnstuck(2904.6384277344, 2493.1042480469, 113.68416595459)',
      'GMR.DefineUnstuck(2907.8051757812, 2482.7414550781, 117.16180419922)',
      'GMR.DefineUnstuck(2910.2536621094, 2471.5725097656, 120.68955993652)',
      'GMR.DefineUnstuck(2911.2165527344, 2457.4506835938, 120.18236541748)',
      'GMR.DefineUnstuck(2911.5832519531, 2437.9228515625, 121.06943511963)',
      'GMR.DefineUnstuck(2907.1264648438, 2422.4846191406, 115.1909866333)',
      'GMR.DefineUnstuck(2907.2678222656, 2413.3686523438, 119.86721038818)',
      'GMR.DefineUnstuck(2915.1667480469, 2398.2546386719, 125.90251922607)',
      'GMR.DefineUnstuck(2920.16796875, 2386.5930175781, 125.79898834229)',
      'GMR.DefineUnstuck(2929.0168457031, 2370.447265625, 132.88516235352)',
      'GMR.DefineUnstuck(2940.0544433594, 2356.2407226562, 136.10488891602)',
      'GMR.DefineUnstuck(2951.6853027344, 2344.8820800781, 140.32388305664)',
      'GMR.DefineUnstuck(2956.5412597656, 2333.6069335938, 143.25381469727)',
      'GMR.DefineUnstuck(2965.7177734375, 2325.0131835938, 152.61526489258)',
  
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(3011.389893, 2290.219971, 160.904999, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19657)',
      'GMR.DefineQuestEnemyId(19705)'
    }
  })
  
  
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Down With Daellis (TEST) |r",
    QuestID = 10223,
    QuestType = "Custom",
    PickUp = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    TurnIn = { x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10223, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(true)',
      -- Run To Area
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2331.5349121094, 2334.2236328125, 108.6305770874, 50)',
      'GMR.DefineUnstuck(2334.2390136719, 2346.0461425781, 111.38324737549)',
      'GMR.DefineUnstuck(2338.6486816406, 2358.4689941406, 114.67204284668)',
      'GMR.DefineUnstuck(2343.9392089844, 2369.3176269531, 117.62169647217)',
      'GMR.DefineUnstuck(2349.849609375, 2376.4321289062, 120.550758361825)',
      'GMR.DefineUnstuck(2364.333984375, 2378.4755859375, 127.25338745117)',
      'GMR.DefineUnstuck(2376.7609863281, 2380.1022949219, 134.51481628418)',
      'GMR.DefineUnstuck(2386.26953125, 2380.0661621094, 141.11051940918)',
      'GMR.DefineUnstuck(2396.01171875, 2377.5361328125, 145.41529846191)',
      'GMR.DefineUnstuck(2409.6018066406, 2375.146484375, 144.61755371094)',
      'GMR.DefineUnstuck(2421.5327148438, 2378.0844726562, 143.13664245605)',
      'GMR.DefineUnstuck(2433.9946289062, 2389.6125488281, 142.87600708008)',
      'GMR.DefineUnstuck(2445.2846679688, 2401.611328125, 141.47927856445)',
      'GMR.DefineUnstuck(2455.0036621094, 2410.3547363281, 140.94586181641)',
      'GMR.DefineUnstuck(2468.7377929688, 2420.4484863281, 137.65483093262)',
      'GMR.DefineUnstuck(2479.0588378906, 2426.7451171875, 134.7092590332)',
      'GMR.DefineUnstuck(2485.0339355469, 2436.5126953125, 132.61557006836)',
      'GMR.DefineUnstuck(2489.2966308594, 2457.1555175781, 129.97064208984)',
      'GMR.DefineUnstuck(2491.5637207031, 2479.6083984375, 129.13096618652)',
      'GMR.DefineUnstuck(2497.0515136719, 2499.2863769531, 129.05114746094)',
      'GMR.DefineUnstuck(2501.2934570312, 2514.4978027344, 128.99543762207)',
      'GMR.DefineUnstuck(2505.0993652344, 2528.1442871094, 129.15853881836)',
      'GMR.DefineUnstuck(2515.5437011719, 2536.2209472656, 129.27755737305)',
      'GMR.DefineUnstuck(2529.5952148438, 2539.8015136719, 128.76745605469)',
      'GMR.DefineUnstuck(2543.8295898438, 2537.7885742188, 123.38076782227)',
      'GMR.DefineUnstuck(2558.6772460938, 2537.8017578125, 116.36971282959)',
      'GMR.DefineUnstuck(2572.7502441406, 2534.3464355469, 112.40103912354)',
      'GMR.DefineUnstuck(2588.4853515625, 2528.7165527344, 114.14424133301)',
      'GMR.DefineUnstuck(2600.1606445312, 2520.9919433594, 114.49829101562)',
      'GMR.DefineUnstuck(2613.8820800781, 2519.9133300781, 113.39822387695)',
      'GMR.DefineUnstuck(2625.5014648438, 2525.8920898438, 110.82959747314)',
      'GMR.DefineUnstuck(2635.4802246094, 2536.9245605469, 113.52500915527)',
      'GMR.DefineUnstuck(2640.6218261719, 2550.6923828125, 113.33195495605)',
      'GMR.DefineUnstuck(2641.3857421875, 2565.04296875, 116.41799163818)',
      'GMR.DefineUnstuck(2643.912109375, 2579.7897949219, 117.81134796143)',
      'GMR.DefineUnstuck(2648.7731933594, 2587.8942871094, 117.29885101318)',
      'GMR.DefineUnstuck(2659.8618164062, 2597.0793457031, 111.41330718994)',
      'GMR.DefineUnstuck(2669.759765625, 2605.7858886719, 109.28276062012)',
      'GMR.DefineUnstuck(2680.9775390625, 2611.70703125, 105.44639587402)',
      'GMR.DefineUnstuck(2694.98046875, 2612.2236328125, 102.85198974609)',
      'GMR.DefineUnstuck(2710.4592285156, 2611.9016113281, 102.47798156738)',
      'GMR.DefineUnstuck(2724.6091308594, 2612.3415527344, 102.48071289062)',
      'GMR.DefineUnstuck(2738.1098632812, 2612.7612304688, 102.35719299316)',
      'GMR.DefineUnstuck(2753.8947753906, 2615.8239746094, 102.34451293945)',
      'GMR.DefineUnstuck(2771.3518066406, 2619.2353515625, 102.33707427979)',
      'GMR.DefineUnstuck(2788.0815429688, 2620.3649902344, 102.47158050537)',
      'GMR.DefineUnstuck(2804.8061523438, 2621.7412109375, 102.47158050537)',
      'GMR.DefineUnstuck(2821.3591308594, 2624.1743164062, 103.69455718994)',
      'GMR.DefineUnstuck(2839.794921875, 2624.6604003906, 108.46675872803)',
      'GMR.DefineUnstuck(2856.6491699219, 2623.0463867188, 111.46959686279)',
      'GMR.DefineUnstuck(2875.6708984375, 2613.6494140625, 113.16915130615)',
      'GMR.DefineUnstuck(2884.6872558594, 2598.8681640625, 115.85272979736)',
      'GMR.DefineUnstuck(2887.7060546875, 2584.7556152344, 116.66623687744)',
      'GMR.DefineUnstuck(2889.9721679688, 2567.822265625, 119.76039886475)',
      'GMR.DefineUnstuck(2892.7702636719, 2553.6135253906, 120.06839752197)',
      'GMR.DefineUnstuck(2893.4379882812, 2540.6508789062, 118.34586334229)',
      'GMR.DefineUnstuck(2895.501953125, 2526.4035644531, 114.95951843262)',
      'GMR.DefineUnstuck(2898.0268554688, 2517.1896972656, 114.59183502197)',
      'GMR.DefineUnstuck(2901.1325683594, 2504.7487792969, 116.83312988281)',
      'GMR.DefineUnstuck(2904.6384277344, 2493.1042480469, 113.68416595459)',
      'GMR.DefineUnstuck(2907.8051757812, 2482.7414550781, 117.16180419922)',
      'GMR.DefineUnstuck(2910.2536621094, 2471.5725097656, 120.68955993652)',
      'GMR.DefineUnstuck(2911.2165527344, 2457.4506835938, 120.18236541748)',
      'GMR.DefineUnstuck(2911.5832519531, 2437.9228515625, 121.06943511963)',
      'GMR.DefineUnstuck(2907.1264648438, 2422.4846191406, 115.1909866333)',
      'GMR.DefineUnstuck(2907.2678222656, 2413.3686523438, 119.86721038818)',
      'GMR.DefineUnstuck(2915.1667480469, 2398.2546386719, 125.90251922607)',
      'GMR.DefineUnstuck(2920.16796875, 2386.5930175781, 125.79898834229)',
      'GMR.DefineUnstuck(2929.0168457031, 2370.447265625, 132.88516235352)',
      'GMR.DefineUnstuck(2940.0544433594, 2356.2407226562, 136.10488891602)',
      'GMR.DefineUnstuck(2951.6853027344, 2344.8820800781, 140.32388305664)',
      'GMR.DefineUnstuck(2956.5412597656, 2333.6069335938, 143.25381469727)',
      'GMR.DefineUnstuck(2965.7177734375, 2325.0131835938, 152.61526489258)',
  
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineProfileCenter(2985.623, 2356.748, 154.4116, 60)',
      'GMR.BlacklistId(20483)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19705)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFFFFAB48 Summoner Kathins Prize / Down With Daellis (TEST) |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10209, x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
      { questId = 10223, x = 2247.110107, y = 2277.429932, z = 97.911598, id = 19488 },
    },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Profile = {
      [[
        if not GMR.Frames.Rand096 then
        GMR.Print("Frame created")
        GMR.Frames.Rand096 = CreateFrame("frame")
        GMR.Frames.Rand096:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if GMR.Questing.IsObjectiveCompleted(10209, 1) and GMR.IsQuestActive(10209) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand096 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      -- From the Kill Spot
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2980.8039550781, 2323.6301269531, 160.73789978027, 60)',
      'GMR.DefineUnstuck(2967.19140625, 2330.2934570312, 151.63272094727)',
      'GMR.DefineUnstuck(2951.2817382812, 2337.4609375, 140.15060424805)',
      'GMR.DefineUnstuck(2937.7741699219, 2344.8625488281, 131.65374755859)',
      'GMR.DefineUnstuck(2924.2802734375, 2352.8549804688, 126.52300262451)',
      'GMR.DefineUnstuck(2911.728515625, 2360.7985839844, 119.4594039917)',
      'GMR.DefineUnstuck(2908.3146972656, 2373.7829589844, 113.52942657471)',
      -- Near edge of Cliff
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2877.0783691406, 2283.4060058594, 108.61948394775, 60)',
      'GMR.DefineUnstuck(2890.6669921875, 2293.5512695312, 114.45021057129)',
      'GMR.DefineUnstuck(2901.0007324219, 2297.4633789062, 120.55264282227)',
      'GMR.DefineUnstuck(2908.8908691406, 2307.197265625, 127.1762008667)',
      'GMR.DefineUnstuck(2902.9284667969, 2316.4914550781, 124.43029022217)',
      'GMR.DefineUnstuck(2895.7766113281, 2325.8100585938, 119.73014831543)',
      'GMR.DefineUnstuck(2895.0480957031, 2333.3317871094, 118.16465759277)',
      'GMR.DefineUnstuck(2897.2409667969, 2345.9252929688, 117.09908294678)',
      'GMR.DefineUnstuck(2900.9619140625, 2355.3830566406, 117.44046783447)',
      'GMR.DefineUnstuck(2906.8703613281, 2370.9536132812, 112.74907684326)',
      'GMR.DefineUnstuck(2908.3146972656, 2373.7829589844, 113.52942657471)',
      -- Path Back - where both end
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2909.9287109375, 2372.9460449219, 114.78315734863, 60)',
      'GMR.DefineUnstuck(2913.138671875, 2386.5100097656, 123.08698272705)',
      'GMR.DefineUnstuck(2912.2741699219, 2401.9755859375, 124.55992889404)',
      'GMR.DefineUnstuck(2913.3146972656, 2414.7192382812, 123.52727508545)',
      'GMR.DefineUnstuck(2913.1242675781, 2426.1896972656, 119.31376647949)',
      'GMR.DefineUnstuck(2913.384765625, 2441.3227539062, 122.26094055176)',
      'GMR.DefineUnstuck(2913.6735839844, 2458.1201171875, 121.75466156006)',
      'GMR.DefineUnstuck(2911.9382324219, 2471.1369628906, 121.6700668335)',
      'GMR.DefineUnstuck(2910.1489257812, 2480.8034667969, 119.38805389404)',
      'GMR.DefineUnstuck(2906.3642578125, 2492.732421875, 114.34936523438)',
      'GMR.DefineUnstuck(2901.6472167969, 2506.4362792969, 117.49712371826)',
      'GMR.DefineUnstuck(2895.900390625, 2520.3928222656, 114.34984588623)',
      'GMR.DefineUnstuck(2894.9821777344, 2534.828125, 115.07752990723)',
      'GMR.DefineUnstuck(2898.9060058594, 2546.6455078125, 120.65982818604)',
      'GMR.DefineUnstuck(2895.75, 2559.9323730469, 120.77318572998)',
      'GMR.DefineUnstuck(2890.6630859375, 2573.4389648438, 119.87815093994)',
      'GMR.DefineUnstuck(2883.228515625, 2588.1413574219, 116.15458679199)',
      'GMR.DefineUnstuck(2876.2436523438, 2601.9541015625, 114.57075500488)',
      'GMR.DefineUnstuck(2868.2858886719, 2613.1188964844, 112.82530975342)',
      'GMR.DefineUnstuck(2857.0998535156, 2620.4289550781, 112.2244644165)',
      'GMR.DefineUnstuck(2843.9504394531, 2625.5029296875, 109.23028564453)',
      'GMR.DefineUnstuck(2828.44921875, 2625.82421875, 105.38124084473)',
      'GMR.DefineUnstuck(2813.4521484375, 2623.6525878906, 102.47234344482)',
      'GMR.DefineUnstuck(2797.4682617188, 2621.3381347656, 102.47234344482)',
      'GMR.DefineUnstuck(2782.138671875, 2619.1181640625, 102.47234344482)',
      'GMR.DefineUnstuck(2761.6103515625, 2616.1457519531, 102.33832550049)',
      'GMR.DefineUnstuck(2741.7250976562, 2613.2663574219, 102.34702301025)',
      'GMR.DefineUnstuck(2721.5080566406, 2610.3388671875, 102.48072052002)',
      'GMR.DefineUnstuck(2701.5297851562, 2608.6918945312, 102.48072052002)',
      'GMR.DefineUnstuck(2679.7907714844, 2608.587890625, 105.87683868408)',
      'GMR.DefineUnstuck(2660.7060546875, 2607.9223632812, 111.87064361572)',
      'GMR.DefineUnstuck(2646.6628417969, 2603.8046875, 117.00584411621)',
      'GMR.DefineUnstuck(2634.6005859375, 2591.2556152344, 123.70936584473)',
      'GMR.DefineUnstuck(2627.7846679688, 2584.35546875, 120.93711853027)',
      'GMR.DefineUnstuck(2618.3405761719, 2575.5300292969, 121.38091278076)',
      'GMR.DefineUnstuck(2614.6774902344, 2590.8813476562, 129.8702545166)',
      'GMR.DefineUnstuck(2605.1328125, 2610.826171875, 130.29595947266)',
      'GMR.DefineUnstuck(2590.3173828125, 2621.3083496094, 131.50187683105)',
      'GMR.DefineUnstuck(2571.2971191406, 2627.7854003906, 131.04319763184)',
      'GMR.DefineUnstuck(2555.7785644531, 2629.0554199219, 128.70407104492)',
      'GMR.DefineUnstuck(2537.6525878906, 2622.7236328125, 127.50916290283)',
      'GMR.DefineUnstuck(2523.9777832031, 2611.4375, 129.64077758789)',
      'GMR.DefineUnstuck(2519.1801757812, 2591.7292480469, 129.26095581055)',
      'GMR.DefineUnstuck(2515.37109375, 2570.7058105469, 129.4041595459)',
      'GMR.DefineUnstuck(2513.0559082031, 2548.4370117188, 129.00804138184)',
      'GMR.DefineUnstuck(2508.5725097656, 2530.2839355469, 129.09680175781)',
      'GMR.DefineUnstuck(2499.7966308594, 2509.3466796875, 129.08477783203)',
      'GMR.DefineUnstuck(2492.4555664062, 2490.3579101562, 128.98017883301)',
      'GMR.DefineUnstuck(2490.2043457031, 2467.18359375, 129.40113830566)',
      'GMR.DefineUnstuck(2486.041015625, 2441.2297363281, 131.86555480957)',
      'GMR.DefineUnstuck(2480.73828125, 2427.5925292969, 134.39520263672)',
      'GMR.DefineUnstuck(2474.5356445312, 2421.9812011719, 136.05500793457)',
      'GMR.DefineUnstuck(2456.0695800781, 2414.1540527344, 141.37886047363)',
      'GMR.DefineUnstuck(2435.8823242188, 2404.0207519531, 142.12799072266)',
      'GMR.DefineUnstuck(2422.2856445312, 2386.2312011719, 143.45629882812)',
      'GMR.DefineUnstuck(2403.2570800781, 2377.56640625, 145.22808837891)',
      'GMR.DefineUnstuck(2381.8540039062, 2382.5051269531, 136.94410705566)',
      'GMR.DefineUnstuck(2361.5678710938, 2378.3764648438, 125.85802459717)',
      'GMR.DefineUnstuck(2343.2048339844, 2369.8903808594, 117.54447937012)',
      'GMR.DefineUnstuck(2333.1723632812, 2353.7292480469, 112.81436157227)',
      'GMR.DefineUnstuck(2327.4130859375, 2333.865234375, 107.81427764893)',
      'GMR.DefineUnstuck(2322.7570800781, 2314.6647949219, 103.32287597656)',
      'GMR.DefineUnstuck(2316.4482421875, 2302.2734375, 100.5454788208)',
      'GMR.DefineUnstuck(2302.3479003906, 2291.7185058594, 97.375343322754)',
      'GMR.DefineUnstuck(2276.791015625, 2279.21875, 94.659049987793)',
      
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Ar'kelos the Guardian |r",
    QuestID = 10176,
    QuestType = "Custom",
    PickUp = { x = 2270.4221191406, y = 2278.5473632812, z = 94.919761657715, id = 19644 },
    TurnIn = { x = 2245.080078, y = 2230.72998, z = 136.723007, id = 19481 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10176, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand092 then
        GMR.Print("Frame created")
        GMR.Frames.Rand092 = CreateFrame("frame")
        GMR.Frames.Rand092:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10176 then
            if GMR.IsExecuting() and not GMR.InCombat() then
              if not GMR.IsQuestActive(10176) then
                if not GMR.IsPlayerPosition(2270.4221191406, 2278.5473632812, 94.919761657715, 3) then 
                  GMR.MeshTo(2270.4221191406, 2278.5473632812, 94.919761657715)
                else
                  local itemName = GetItemInfo(28455)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand092 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(true)',
      'GMR.DefineProfileCenter(2241.94, 2244.117, 101.3442, 120)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(19494)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF TURNIN |r |cFFFFAB48 Ar'kelos the Guardian |r",
    QuestType = "MassTurnIn",
    MassTurnIn = {
      { questId = 10176, x = 2245.080078, y = 2230.72998, z = 136.723007, id = 19481 },
    },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Profile = {
      'GMR.SkipTurnIn(false)',
      -- From the Kill Spot
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2231.7963867188, 2248.7358398438, 103.08937072754, 6)',
      'GMR.DefineUnstuck(2228.6594238281, 2245.7604980469, 104.49468231201)',
      'GMR.DefineUnstuck(2225.4611816406, 2241.138671875, 105.03253173828)',
      'GMR.DefineUnstuck(2229.1311035156, 2239.1418457031, 105.36301422119)',
      'GMR.DefineUnstuck(2231.8386230469, 2243.0913085938, 106.38646697998)',
      'GMR.DefineUnstuck(2234.1708984375, 2244.6711425781, 107.9031829834)',
      'GMR.DefineUnstuck(2236.4309082031, 2244.6005859375, 109.27019500732)',
      'GMR.DefineUnstuck(2238.8981933594, 2243.5517578125, 110.90245056152)',
      'GMR.DefineUnstuck(2240.4987792969, 2241.6784667969, 111.8064956665)',
      'GMR.DefineUnstuck(2241.8640136719, 2240.615234375, 112.02794647217)',
      'GMR.DefineUnstuck(2239.4738769531, 2237.5219726562, 112.18487548828)',
      
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2243.4841308594, 2225.8366699219, 107.30429077148, 6)',
      'GMR.DefineUnstuck(2238.5358886719, 2224.4274902344, 108.8256072998)',
      'GMR.DefineUnstuck(2233.4670410156, 2225.1770019531, 109.01722717285)',
      'GMR.DefineUnstuck(2228.572265625, 2227.318359375, 108.59825134277)',
      'GMR.DefineUnstuck(2231.4604492188, 2229.9304199219, 108.96799468994)',
      'GMR.DefineUnstuck(2234.4174804688, 2233.6118164062, 110.75358581543)',
      'GMR.DefineUnstuck(2238.1271972656, 2237.73828125, 111.91938018799)',
      
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2238.1271972656, 2237.73828125, 111.91938018799, 5)',
      'GMR.DefineUnstuck(2240.3544921875, 2236.6843261719, 112.58206176758)',
      'GMR.DefineUnstuck(2244.4428710938, 2234.3081054688, 114.79290008545)',
      'GMR.DefineUnstuck(2248.4228515625, 2233.4470214844, 115.52928924561)',
      'GMR.DefineUnstuck(2249.2990722656, 2237.4555664062, 115.54716491699)',
      'GMR.DefineUnstuck(2248.5952148438, 2241.8020019531, 116.2901763916)',
      'GMR.DefineUnstuck(2245.6682128906, 2246.0964355469, 117.61869812012)',
      'GMR.DefineUnstuck(2241.2797851562, 2248.7478027344, 118.66967773438)',
      'GMR.DefineUnstuck(2236.408203125, 2250.2646484375, 119.82738494873)',
      'GMR.DefineUnstuck(2231.9782714844, 2249.8227539062, 120.27005004883)',
      'GMR.DefineUnstuck(2228.0532226562, 2248.0915527344, 120.23635101318)',
      'GMR.DefineUnstuck(2228.0874023438, 2245.4360351562, 120.34746551514)',
      'GMR.DefineUnstuck(2231.6694335938, 2243.0466308594, 120.80843353271)',
      'GMR.DefineUnstuck(2235.3833007812, 2240.0915527344, 121.34057617188)',
      'GMR.DefineUnstuck(2238.9885253906, 2236.7485351562, 121.96389770508)',
      'GMR.DefineUnstuck(2241.0349121094, 2233.2995605469, 123.44506835938)',
      'GMR.DefineUnstuck(2239.8793945312, 2229.1999511719, 125.55192565918)',
      'GMR.DefineUnstuck(2236.7204589844, 2225.2065429688, 127.23300933838)',
      'GMR.DefineUnstuck(2229.0412597656, 2226.0432128906, 126.97169494629)',
      'GMR.DefineUnstuck(2224.5563964844, 2228.7739257812, 126.56700897217)',
      'GMR.DefineUnstuck(2223.2321777344, 2233.6638183594, 126.253074646)',
      'GMR.DefineUnstuck(2222.4187011719, 2239.7839355469, 125.96155548096)',
      'GMR.DefineUnstuck(2224.896484375, 2245.0004882812, 125.90047454834)',
      'GMR.DefineUnstuck(2228.5981445312, 2246.5729980469, 126.04154968262)',
      'GMR.DefineUnstuck(2229.7790527344, 2243.5190429688, 126.22197723389)',
      'GMR.DefineUnstuck(2229.267578125, 2240.056640625, 127.99237823486)',
      'GMR.DefineUnstuck(2229.9682617188, 2237.9069824219, 128.80361938477)',
      'GMR.DefineUnstuck(2231.3950195312, 2236.96484375, 129.77369689941)',
      'GMR.DefineUnstuck(2232.8078613281, 2237.2868652344, 130.37603759766)',
      'GMR.DefineUnstuck(2234.5803222656, 2239.0185546875, 131.16505432129)',
      'GMR.DefineUnstuck(2234.8950195312, 2241.0593261719, 132.06420898438)',
      'GMR.DefineUnstuck(2233.2211914062, 2243.14453125, 132.88967895508)',
      'GMR.DefineUnstuck(2231.0852050781, 2244.3869628906, 133.7341003418)',
      'GMR.DefineUnstuck(2227.6232910156, 2244.3081054688, 134.83679199219)',
      'GMR.DefineUnstuck(2222.2998046875, 2242.1960449219, 134.62063598633)',
      'GMR.DefineUnstuck(2219.7485351562, 2234.1550292969, 134.80368041992)',
      'GMR.DefineUnstuck(2222.8845214844, 2228.5327148438, 135.23835754395)',
      'GMR.DefineUnstuck(2227.8881835938, 2224.7741699219, 135.71774291992)',
      'GMR.DefineUnstuck(2234.0981445312, 2222.9431152344, 136.24519348145)',
      'GMR.DefineUnstuck(2239.8742675781, 2225.2612304688, 136.52494812012)',
      
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  
  GMR.DefineQuest({
    QuestName = "|cFFE25FFF PICKUP |r |cFFFFAB48 Netherstorm - Midrealm Post (Eco-Dome) Quests |r",
    QuestType = "MassPickUp",
    MassPickUp = {
      { questId = 10348, x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
      { questId = 10417, x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
      { questId = 10433, x = 3373.580078, y = 2892.22998, z = 144.048996, id = 20921 },
      { questId = 10311, x = 3339.7687988281, y = 2881.7463378906, z = 143.99859619141, id = 20066 }
    },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2244.8366699219, 2230.6203613281, 136.62580871582, 25)',
      'GMR.DefineUnstuck(2238.7546386719, 2226.8041992188, 136.36697387695)',
      'GMR.DefineUnstuck(2231.7802734375, 2223.0310058594, 136.08874511719)',
      'GMR.DefineUnstuck(2227.5751953125, 2223.9147949219, 135.75819396973)',
      'GMR.DefineUnstuck(2223.6005859375, 2227.4321289062, 135.33879089355)',
      'GMR.DefineUnstuck(2220.6984863281, 2232.3349609375, 134.93270874023)',
      'GMR.DefineUnstuck(2219.3947753906, 2238.4235839844, 134.59823608398)',
      'GMR.DefineUnstuck(2221.8469238281, 2243.7409667969, 134.53788757324)',
      'GMR.DefineUnstuck(2226.4943847656, 2243.0952148438, 134.87966918945)',
      'GMR.DefineUnstuck(2230.771484375, 2244.462890625, 133.8060760498)',
      'GMR.DefineUnstuck(2232.994140625, 2244.0112304688, 133.08502197266)',
      'GMR.DefineUnstuck(2234.3608398438, 2242.47265625, 132.64965820312)',
      'GMR.DefineUnstuck(2234.0114746094, 2239.1943359375, 130.93421936035)',
      'GMR.DefineUnstuck(2232.8452148438, 2238.3344726562, 130.39212036133)',
      'GMR.DefineUnstuck(2230.9982910156, 2237.4426269531, 129.60679626465)',
      'GMR.DefineUnstuck(2228.6328125, 2238.9519042969, 128.12882995605)',
      'GMR.DefineUnstuck(2229.3601074219, 2242.58984375, 126.31398773193)',
      'GMR.DefineUnstuck(2231.0017089844, 2247.6323242188, 126.15351867676)',
      'GMR.DefineUnstuck(2235.1237792969, 2250.7170410156, 126.35540771484)',
      'GMR.DefineUnstuck(2240.8181152344, 2250.0998535156, 126.77676391602)',
      'GMR.DefineUnstuck(2245.9038085938, 2246.1071777344, 127.27852630615)',
      'GMR.DefineUnstuck(2248.8056640625, 2238.8872070312, 127.775390625)',
      'GMR.DefineUnstuck(2248.7885742188, 2232.9951171875, 128.02926635742)',
      'GMR.DefineUnstuck(2244.3212890625, 2225.4230957031, 128.04222106934)',
      'GMR.DefineUnstuck(2239.724609375, 2222.9621582031, 127.83277130127)',
      'GMR.DefineUnstuck(2234.619140625, 2225.1213378906, 127.36158752441)',
      'GMR.DefineUnstuck(2238.6479492188, 2227.9094238281, 126.37690734863)',
      'GMR.DefineUnstuck(2240.8088378906, 2231.4858398438, 124.24298858643)',
      'GMR.DefineUnstuck(2240.5725097656, 2234.9169921875, 123.65084075928)',
      'GMR.DefineUnstuck(2236.9694824219, 2237.9645996094, 121.58150482178)',
      'GMR.DefineUnstuck(2235.2346191406, 2233.8173828125, 120.91757965088)',
      'GMR.DefineUnstuck(2232.8374023438, 2229.9750976562, 118.943359375)',
      'GMR.DefineUnstuck(2231.6437988281, 2226.31640625, 117.90929412842)',
      'GMR.DefineUnstuck(2233.9528808594, 2224.3566894531, 117.59765625)',
      'GMR.DefineUnstuck(2239.2368164062, 2224.9748535156, 116.39747619629)',
      'GMR.DefineUnstuck(2243.8442382812, 2227.1647949219, 115.61470031738)',
      'GMR.DefineUnstuck(2247.27734375, 2231.7763671875, 115.52040100098)',
      'GMR.DefineUnstuck(2243.7800292969, 2233.9467773438, 114.64434051514)',
      'GMR.DefineUnstuck(2238.7067871094, 2237.0954589844, 112.05598449707)',
      'GMR.DefineUnstuck(2235.5737304688, 2235.3068847656, 111.33194732666)',
      'GMR.DefineUnstuck(2231.7370605469, 2230.05859375, 109.10118103027)',
      'GMR.DefineUnstuck(2231.0695800781, 2225.6555175781, 108.83422088623)',
      'GMR.DefineUnstuck(2235.3117675781, 2224.1633300781, 109.18272399902)',
      'GMR.DefineUnstuck(2240.5344238281, 2225.7055664062, 108.03691864014)',
      'GMR.DefineUnstuck(2246.5036621094, 2228.5073242188, 105.97608947754)',
      'GMR.DefineUnstuck(2248.2553710938, 2233.1853027344, 104.02428436279)',
      'GMR.DefineUnstuck(2248.4067382812, 2239.357421875, 101.97899627686)',
      'GMR.DefineUnstuck(2246.8203125, 2243.5373535156, 101.69985198975)',
      'GMR.DefineUnstuck(2247.4309082031, 2249.8205566406, 101.48323822021)',
      'GMR.DefineUnstuck(2249.4245605469, 2253.3444824219, 101.47266387939)',
      'GMR.DefineUnstuck(2254.9868164062, 2259.9389648438, 101.56574249268)',
      'GMR.DefineUnstuck(2258.3454589844, 2264.3674316406, 100.75856781006)',
      'GMR.DefineUnstuck(2264.4455566406, 2272.4096679688, 97.261123657227)',
      
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2468.8713378906, 2864.1088867188, 132.34320068359, 40)',
      'GMR.DefineUnstuck(2475.2646484375, 2873.2880859375, 132.95600891113)',
      'GMR.DefineUnstuck(2484.166015625, 2885.4252929688, 133.15266418457)',
      'GMR.DefineUnstuck(2488.37890625, 2899.1140136719, 131.38914489746)',
      'GMR.DefineUnstuck(2490.3061523438, 2912.3791503906, 129.24787902832)',
      'GMR.DefineUnstuck(2496.81640625, 2921.1052246094, 128.05313110352)',
      'GMR.DefineUnstuck(2507.2856445312, 2927.16015625, 124.9895401001)',
      'GMR.DefineUnstuck(2513.0676269531, 2921.7634277344, 125.69010925293)',
      'GMR.DefineUnstuck(2516.314, 2919.263, 127.1798)',
      'GMR.DefineUnstuck(2517.433, 2897.34, 129.9766)',
      'GMR.DefineUnstuck(2523.4, 2872.198, 130.2247)',
      'GMR.DefineUnstuck(2527.669, 2847.251, 127.7692)',
      'GMR.DefineUnstuck(2531.246, 2826.352, 126.803)',
      'GMR.DefineUnstuck(2541.571, 2805.002, 124.4264)',
      'GMR.DefineUnstuck(2552.138, 2783.152, 121.903)',
      'GMR.DefineUnstuck(2555.133, 2757.778, 119.4403)',
      'GMR.DefineUnstuck(2553.938, 2736.202, 119.6497)',
      'GMR.DefineUnstuck(2548.327, 2712.561, 123.1689)',
      'GMR.DefineUnstuck(2539.452, 2692.383, 126.9399)',
      'GMR.DefineUnstuck(2534.601, 2670.223, 130.1371)',
      'GMR.DefineUnstuck(2530.442, 2651.505, 130.5672)',
      'GMR.DefineUnstuck(2525.653, 2629.958, 130.3068)',
      'GMR.DefineUnstuck(2520.366, 2606.171, 129.4293)',
      'GMR.DefineUnstuck(2515.877, 2585.97, 129.2839)',
      'GMR.DefineUnstuck(2514.518, 2562.755, 129.2681)',
      'GMR.DefineUnstuck(2510.305, 2541.26, 129.4725)',
      'GMR.DefineUnstuck(2503.907, 2522.825, 128.9725)',
      'GMR.DefineUnstuck(2497.906, 2505.533, 129.1446)',
      'GMR.DefineUnstuck(2490.906, 2485.364, 129.0528)',
      'GMR.DefineUnstuck(2486.949, 2463.142, 129.7418)',
      -- TO BRIDGE
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2330.6669921875, 2332.5061035156, 108.07167816162, 50)',
      'GMR.DefineUnstuck(2333.9697265625, 2345.2565917969, 111.17642211914)',
      'GMR.DefineUnstuck(2337.6020507812, 2359.2788085938, 114.45449066162)',
      'GMR.DefineUnstuck(2345.3903808594, 2369.8454589844, 118.24346923828)',
      'GMR.DefineUnstuck(2356.2565917969, 2378.2165527344, 123.52463531494)',
      'GMR.DefineUnstuck(2368.7216796875, 2380.7785644531, 129.70573425293)',
      'GMR.DefineUnstuck(2382.8244628906, 2379.0661621094, 138.95855712891)',
      'GMR.DefineUnstuck(2395.3972167969, 2375.865234375, 145.70155334473)',
      'GMR.DefineUnstuck(2412.8427734375, 2376.595703125, 144.13597106934)',
      'GMR.DefineUnstuck(2430.3874511719, 2388.2214355469, 143.0791015625)',
      'GMR.DefineUnstuck(2447.6203613281, 2409.6728515625, 141.60542297363)',
      'GMR.DefineUnstuck(2459.705078125, 2419.7722167969, 141.13284301758)',
      'GMR.DefineUnstuck(2474.2275390625, 2437.123046875, 135.56672668457)',
      'GMR.DefineUnstuck(2484.708984375, 2457.0759277344, 130.56532287598)',
      'GMR.DefineUnstuck(2489.3984375, 2481.8552246094, 129.17514038086)',
      'GMR.DefineUnstuck(2492.9685058594, 2500.8713378906, 129.08992004395)',
      'GMR.DefineUnstuck(2501.5852050781, 2521.7951660156, 129.08915710449)',
      'GMR.DefineUnstuck(2510.8962402344, 2541.0795898438, 129.34568786621)',
      'GMR.DefineUnstuck(2514.1408691406, 2560.7709960938, 129.24418640137)',
      'GMR.DefineUnstuck(2519.3481445312, 2589.9680175781, 129.19026184082)',
      'GMR.DefineUnstuck(2522.9873046875, 2610.3737792969, 129.50634765625)',
      'GMR.DefineUnstuck(2526.5720214844, 2630.4741210938, 130.23397827148)',
      'GMR.DefineUnstuck(2531.7556152344, 2643.8056640625, 129.24255371094)',
      'GMR.DefineUnstuck(2551.4714355469, 2639.0002441406, 128.83294677734)',
      'GMR.DefineUnstuck(2573.3493652344, 2634.5942382812, 129.83790588379)',
      'GMR.DefineUnstuck(2594.5949707031, 2629.0832519531, 130.10510253906)',
      'GMR.DefineUnstuck(2611.2570800781, 2627.759765625, 125.45476531982)',
      'GMR.DefineUnstuck(2626.1879882812, 2621.2761230469, 120.35432434082)',
      'GMR.DefineUnstuck(2641.0354003906, 2612.7653808594, 116.7406463623)',
      'GMR.DefineUnstuck(2658.5493164062, 2609.1613769531, 112.45146942139)',
      'GMR.DefineUnstuck(2677.8830566406, 2610.34765625, 106.20301055908)',
      'GMR.DefineUnstuck(2696.8857421875, 2610.2119140625, 102.47732543945)',
      -- START OF BRIDGE
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(2696.4216308594, 2608.0187988281, 102.47899627686, 50)',
      'GMR.DefineUnstuck(2719.5095214844, 2610.3635253906, 102.48058319092)',
      'GMR.DefineUnstuck(2737.4035644531, 2613.1606445312, 102.3744430542)',
      'GMR.DefineUnstuck(2754.3928222656, 2615.310546875, 102.3450088501)',
      'GMR.DefineUnstuck(2770.2395019531, 2618.2546386719, 102.33714294434)',
      'GMR.DefineUnstuck(2788.0202636719, 2621.7521972656, 102.47416687012)',
      'GMR.DefineUnstuck(2812.7624511719, 2625.1474609375, 102.47416687012)',
      'GMR.DefineUnstuck(2832.5925292969, 2628.30859375, 106.45975494385)',
      'GMR.DefineUnstuck(2852.6215820312, 2629.4104003906, 109.79319000244)',
      'GMR.DefineUnstuck(2876.6567382812, 2629.4724121094, 112.06031036377)',
      'GMR.DefineUnstuck(2903.6708984375, 2629.5419921875, 114.2710723877)',
      'GMR.DefineUnstuck(2925.74609375, 2629.5988769531, 115.70223236084)',
      'GMR.DefineUnstuck(2947.4904785156, 2626.396484375, 115.83511352539)',
      'GMR.DefineUnstuck(2969.0827636719, 2621.8591308594, 115.40603637695)',
      'GMR.DefineUnstuck(2995.7976074219, 2613.6784667969, 114.17042541504)',
      'GMR.DefineUnstuck(3020.001953125, 2610.1276855469, 112.27923583984)',
      'GMR.DefineUnstuck(3037.900390625, 2614.7373046875, 110.72264862061)',
      'GMR.DefineUnstuck(3045.9985351562, 2628.9389648438, 111.85604858398)',
      'GMR.DefineUnstuck(3054.6833496094, 2649.8037109375, 114.01689910889)',
      'GMR.DefineUnstuck(3063.6906738281, 2675.5515136719, 115.06311798096)',
      'GMR.DefineUnstuck(3072.8288574219, 2693.8427734375, 115.70408630371)',
      'GMR.DefineUnstuck(3080.9177246094, 2710.6577148438, 116.0101776123)',
      'GMR.DefineUnstuck(3090.0158691406, 2736.3884277344, 115.83509063721)',
      'GMR.DefineUnstuck(3095.8571777344, 2753.8369140625, 116.04095458984)',
      'GMR.DefineUnstuck(3101.9892578125, 2768.3312988281, 116.2233581543)',
      'GMR.DefineUnstuck(3110.5180664062, 2781.5532226562, 117.74705505371)',
      'GMR.DefineUnstuck(3125.3654785156, 2800.4565429688, 120.54769897461)',
      'GMR.DefineUnstuck(3141.7141113281, 2815.6213378906, 124.74708557129)',
      'GMR.DefineUnstuck(3159.8659667969, 2828.6704101562, 129.50762939453)',
      'GMR.DefineUnstuck(3187.7250976562, 2835.9609375, 133.6886138916)',
      'GMR.DefineUnstuck(3200.54296875, 2833.3793945312, 135.4393157959)',
      'GMR.DefineUnstuck(3237.5895996094, 2842.3068847656, 138.08923339844)',
      'GMR.DefineUnstuck(3258.2829589844, 2847.8166503906, 139.1784362793)',
      'GMR.DefineUnstuck(3275.9304199219, 2851.6975097656, 139.78833007812)',
      'GMR.DefineUnstuck(3298.8305664062, 2857.8527832031, 140.46192932129)',
      'GMR.DefineUnstuck(3320.3732910156, 2860.0949707031, 141.00537109375)',
      
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Run a Diagnostic! |r",
    QuestID = 10417,
    QuestType = "Custom",
    PickUp = { x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
    TurnIn = { x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10417, 1) then
        GMR.SetQuestingState(nil);
        if not GMR.IsPlayerPosition(3412.706, 2795.254, 150.5205, 5) then 
          GMR.MeshTo(3412.706, 2795.254, 150.5205)
        else
          local object = GMR.GetObjectWithInfo({ id = 184589, rawType = 8 })
          if object then 
            GMR.Questing.InteractWith(object, nil, nil, nil, nil, 5)
          end
        end
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3412.706, 2795.254, 150.5205, 120)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(184589)',
      'GMR.DefineCustomObjectId(184589)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Deal With the Saboteurs |r",
    QuestID = 10418,
    QuestType = "Custom",
    PickUp = { x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
    TurnIn = { x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10418, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3439.945, 2848.594, 142.3733, 120)',
      'GMR.DefineProfileCenter(3502.421, 2860.879, 146.9052, 120)',
      'GMR.DefineProfileCenter(3558.516, 2910.618, 141.0787, 120)',
      'GMR.DefineProfileCenter(3514.144, 2956.038, 141.4131, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20773)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 New Opportunities |r",
    QuestID = 10348,
    QuestType = "Gathering",
    PickUp = { x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
    TurnIn = { x = 3359.649902, y = 2893.790039, z = 144.007004, id = 20810 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3448.164, 2821.673, 147.4595, 60)',
      'GMR.DefineProfileCenter(3451.648, 2795.489, 150.6707, 60)',
      'GMR.DefineProfileCenter(3474.01, 2787.444, 148.7227, 60)',
      'GMR.DefineProfileCenter(3574.213, 2790.836, 147.6733, 60)',
      'GMR.DefineProfileCenter(3512.859, 2773.302, 151.9575, 60)',
      'GMR.DefineProfileCenter(3474.782, 2786.858, 148.7443, 60)',
      'GMR.DefineProfileCenter(3407.744, 2824.039, 146.3648, 60)',
      'GMR.DefineProfileCenter(3389.52, 2817.605, 148.0191, 60)',
      'GMR.DefineProfileCenter(3332.785, 2851.354, 142.4682, 60)',
      'GMR.DefineProfileCenter(3310.387, 2884.876, 141.1472, 60)',
      'GMR.DefineProfileCenter(3325.425, 2913.274, 140.715, 60)',
      'GMR.DefineProfileCenter(3379.353, 2932.5, 148.3748, 60)',
      'GMR.DefineProfileCenter(3330.17, 2969.172, 142.8589, 60)',
      'GMR.DefineProfileCenter(3350.26, 3012.128, 142.1085, 60)',
      'GMR.DefineProfileCenter(3344.656, 3046.211, 140.5665, 60)',
      'GMR.DefineProfileCenter(3385.484, 3092.589, 132.2725, 60)',
      'GMR.DefineProfileCenter(3411.394, 3093.524, 134.787, 60)',
      'GMR.DefineProfileCenter(3435.148, 3082.791, 138.4841, 60)',
      'GMR.DefineProfileCenter(3433.843, 3111.281, 132.421, 60)',
      'GMR.DefineProfileCenter(3492.758, 3106.325, 136.1924, 60)',
      'GMR.DefineProfileCenter(3512.597, 3080.912, 135.8887, 60)',
      'GMR.DefineProfileCenter(3538.281, 3051.504, 140.3221, 60)',
      'GMR.DefineProfileCenter(3553.887, 3098.239, 129.5786, 60)',
      'GMR.DefineProfileCenter(3590.341, 3052.676, 128.2457, 60)',
      'GMR.DefineProfileCenter(3652.103, 3034.692, 124.3098, 60)',
      'GMR.DefineProfileCenter(3656.521, 2984.437, 136.8311, 60)',
      'GMR.DefineProfileCenter(3633.011, 2948.454, 146.4706, 60)',
      'GMR.DefineSettings("Disable", { "Grinding" }); GMR.DefineSettings("Enable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(184443)',
      'GMR.DefineCustomObjectId(184443)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Keeping Up Appearances |r",
    QuestID = 10433,
    QuestType = "Custom",
    PickUp = { x = 3373.580078, y = 2892.22998, z = 144.048996, id = 20921 },
    TurnIn = { x = 3373.580078, y = 2892.22998, z = 144.048996, id = 20921 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10433, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(3314.743, 2964.885, 142.9411, 120)',
      'GMR.DefineProfileCenter(3370.028, 3023.52, 141.8263, 120)',
      'GMR.DefineProfileCenter(3466.482, 3078.764, 140.8526, 120)',
      'GMR.DefineProfileCenter(3574, 3064.145, 129.8488, 120)',
      'GMR.DefineProfileCenter(3647.558, 2966.839, 142.093, 120)',
      'GMR.DefineProfileCenter(3608.995, 2848.281, 163.0799, 120)',
      'GMR.DefineProfileCenter(3435.157, 3063.251, 142.4482, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20671)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Drijiya Needs Your Help |r",
    QuestID = 10311,
    QuestType = "TalkTo",
    PickUp = { x = 3339.7687988281, y = 2881.7463378906, z = 143.99859619141, id = 20066 },
    TurnIn = { x = 3096.1687011719, y = 2801.1457519531, z = 118.09504699707, id = 20281 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineProfileCenter(2528.000000, 2185.679932, 102.599998, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineRepairVendor(2290.120117, 2266.040039, 93.719498, 20463)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(2953.860107, 3636.459961, 144.363998, 19574)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Flora of the Eco-Dome |r",
    QuestID = 10426,
    QuestType = "Custom",
    PickUp = { x = 4245.919922, y = 3124.080078, z = 180.188995, id = 20871 },
    TurnIn = { x = 4245.919922, y = 3124.080078, z = 180.188995, id = 20871 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10426, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand055 then
        GMR.Print("Frame created")
        GMR.Frames.Rand055 = CreateFrame("frame")
        GMR.Frames.Rand055:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10426 then
            if GMR.IsExecuting() then
              if not GMR.Questing.IsObjectiveCompleted(10426, 1) then
                local npc1 = GMR.GetObjectWithInfo({ id = 20774, rawType = 5 })
                local plantDEBUFF = GetSpellInfo(3019)
                if npc1 and GMR.UnitHealth("target") < 2000 and not GMR.HasBuff("target", plantDEBUFF) and UnitName("target") == "Farahlon Lasher" then
                  local itemName = GetItemInfo(29818)
                  GMR.Use(itemName)
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand055 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(4312.62, 3155.075, 158.4771, 120)',
      'GMR.DefineProfileCenter(4285.843, 3016.888, 130.8236, 120)',
      'GMR.DefineProfileCenter(4471.188, 2877.655, 141.2665, 120)',
      'GMR.DefineProfileCenter(4175.676, 3178.329, 177.2607, 120)',
      'GMR.DefineProfileCenter(4139.928, 3364.613, 148.6971, 120)',
      'GMR.DefineProfileCenter(4095.328, 3297.976, 162.3471, 120)',
      'GMR.DefineProfileCenter(4092.084, 3176.881, 172.9917, 120)',
      'GMR.DefineProfileCenter(4015.779, 3096.152, 143.6463, 120)',
      'GMR.DefineProfileCenter(3976.558, 2991.179, 133.8395, 120)',
      'GMR.DefineProfileCenter(4009.041, 2847.705, 128.3751, 120)',
      'GMR.DefineProfileCenter(4003.958, 3094.97, 140.9009, 120)',
      'GMR.DefineProfileCenter(4181.108, 3160.784, 177.2251, 120)',
      'GMR.DefineAreaBlacklist(4225.792, 2847.718, 140.3706,25)',
      'GMR.DefineAreaBlacklist(4177.927, 2824.511, 130.4793,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20774)',
      'GMR.DefineQuestEnemyId(20983)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Creatures of the Eco-Dome |r",
    QuestID = 10427,
    QuestType = "Custom",
    PickUp = { x = 4245.916015625, y = 3124.0791015625, z = 180.10537719727, id = 20871 },
    TurnIn = { x = 4245.916015625, y = 3124.0791015625, z = 180.10537719727, id = 20871 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10427, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand056 then
        GMR.Print("Frame created")
        GMR.Frames.Rand056 = CreateFrame("frame")
        GMR.Frames.Rand056:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10427 then
            if GMR.IsExecuting() then
              if not GMR.Questing.IsObjectiveCompleted(10427, 1) then
                local npc1 = GMR.GetObjectWithInfo({ id = 20610, rawType = 5 })
                if npc1 then 
                  if npc1 and GMR.UnitHealth("target") < 800 and UnitName("target") == "Talbuk Doe" then
                    local itemName = GetItemInfo(29817)
                    GMR.Use(itemName)
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand056 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      'GMR.DefineProfileCenter(4312.62, 3155.075, 158.4771, 120)',
      'GMR.DefineProfileCenter(4285.843, 3016.888, 130.8236, 120)',
      'GMR.DefineProfileCenter(4471.188, 2877.655, 141.2665, 120)',
      'GMR.DefineProfileCenter(4175.676, 3178.329, 177.2607, 120)',
      'GMR.DefineProfileCenter(4139.928, 3364.613, 148.6971, 120)',
      'GMR.DefineProfileCenter(4095.328, 3297.976, 162.3471, 120)',
      'GMR.DefineProfileCenter(4092.084, 3176.881, 172.9917, 120)',
      'GMR.DefineProfileCenter(4015.779, 3096.152, 143.6463, 120)',
      'GMR.DefineProfileCenter(3976.558, 2991.179, 133.8395, 120)',
      'GMR.DefineProfileCenter(4009.041, 2847.705, 128.3751, 120)',
      'GMR.DefineProfileCenter(4003.958, 3094.97, 140.9009, 120)',
      'GMR.DefineProfileCenter(4181.108, 3160.784, 177.2251, 120)',
      'GMR.DefineAreaBlacklist(4225.792, 2847.718, 140.3706,25)',
      'GMR.DefineAreaBlacklist(4177.927, 2824.511, 130.4793,20)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20610)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 When Nature Goes Too Far |r",
    QuestID = 10429,
    QuestType = "Custom",
    PickUp = { x = 4245.919922, y = 3124.080078, z = 180.188995, id = 20871 },
    TurnIn = { x = 4245.919922, y = 3124.080078, z = 180.188995, id = 20871 },
    Faction = "Alliance",
    PreQuest = 99553311064,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10429, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      'GMR.DefineProfileCenter(4400.109863, 2999.189941, 108.192001, 120)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(20775)'
    }
  })
  
  GMR.DefineQuest({
    QuestName = "|cFFFFAB48 Bloody Imp-ossible! |r",
    QuestID = 10924,
    QuestType = "Custom",
    PickUp = { x = 2954.9060058594, y = 1782.12109375, z = 139.12092590332, id = 22479 },
    TurnIn = { x = 2954.9060058594, y = 1782.12109375, z = 139.12092590332, id = 22479 },
    Faction = "Alliance",
    PreQuest = 123456789,
    Lua = [[
      if not GMR.Questing.IsObjectiveCompleted(10924, 1) then
        GMR.SetQuestingState("Idle")
      end
    ]],
    Profile = {
      [[
          if not GMR.Frames.Rand098 then
        GMR.Print("Frame created")
        GMR.Frames.Rand098 = CreateFrame("frame")
        GMR.Frames.Rand098:SetScript("OnUpdate", function(self)
          if GMR.GetProfileType() == "Questing" and GMR.GetNextQuest() == 10924 then
            if GMR.IsExecuting() then
              if not GMR.Questing.IsObjectiveCompleted(10924, 1) then
                local npc1 = GMR.GetObjectWithInfo({ id = 22484, rawType = 5, isAlive = true })
                local itemName = GetItemInfo(31815)
                if GetItemCount(31815) >= 1 and GetItemCooldown(31815) == 0 then
                  if not GMR.GetDelay("CustomQuest") and not npc1 then
                    GMR.Use(itemName)
                  end
                end
              end
            end
          else 
            self:SetScript("OnUpdate", nil); GMR.Frames.Rand098 = nil; GMR.Print("Frame deleted")
          end
        end)
      end
      ]],
      'GMR.SkipTurnIn(false)',
      -- From Stormspire Bridge
      'GMR.CreateTableEntry("Unstuck")',
      'GMR.DefineUnstuck(3701.5998535156, 3056.0700683594, 115.63380432129, 60)',
      'GMR.DefineUnstuck(3684.7954101562, 3048.7810058594, 117.22547912598)',
      'GMR.DefineUnstuck(3671.1640625, 3039.8630371094, 119.63354492188)',
      'GMR.DefineUnstuck(3654.1003417969, 3035.6284179688, 123.61891937256)',
      'GMR.DefineUnstuck(3637.8283691406, 3038.3249511719, 125.85442352295)',
      'GMR.DefineUnstuck(3620.0817871094, 3046.2131347656, 125.43544006348)',
      'GMR.DefineUnstuck(3604.0822753906, 3047.8684082031, 125.92511749268)',
      'GMR.DefineUnstuck(3586.787109375, 3038.3935546875, 128.90893554688)',
      'GMR.DefineUnstuck(3569.6708984375, 3026.091796875, 133.8885345459)',
      'GMR.DefineUnstuck(3557.5463867188, 3016.0324707031, 138.01617431641)',
      'GMR.DefineUnstuck(3546.4143066406, 3006.275390625, 141.86125183105)',
      'GMR.DefineUnstuck(3535.2104492188, 2998.1828613281, 143.32476806641)',
      'GMR.DefineUnstuck(3523.6604003906, 2992.7868652344, 143.39862060547)',
      'GMR.DefineUnstuck(3509.3537597656, 2989.1105957031, 143.10794067383)',
      'GMR.DefineUnstuck(3495.5288085938, 2986.3669433594, 142.87022399902)',
      'GMR.DefineUnstuck(3479.4350585938, 2981.5476074219, 142.83000183105)',
      'GMR.DefineUnstuck(3466.7543945312, 2974.7548828125, 143.03314208984)',
      'GMR.DefineUnstuck(3458.8449707031, 2965.4313964844, 143.21606445312)',
      'GMR.DefineUnstuck(3456.1042480469, 2951.9392089844, 143.49473571777)',
      'GMR.DefineUnstuck(3453.1867675781, 2939.5493164062, 143.78314208984)',
      'GMR.DefineUnstuck(3445.8569335938, 2930.3330078125, 143.77467346191)',
      'GMR.DefineUnstuck(3435.2387695312, 2923.8198242188, 143.79347229004)',
      'GMR.DefineUnstuck(3426.2219238281, 2916.4309082031, 144.34548950195)',
      'GMR.DefineUnstuck(3420.2954101562, 2904.8132324219, 144.16630554199)',
      'GMR.DefineUnstuck(3414.2600097656, 2889.4831542969, 143.60885620117)',
      'GMR.DefineUnstuck(3407.4860839844, 2879.1611328125, 142.65641784668)',
      'GMR.DefineUnstuck(3397.3178710938, 2872.0988769531, 142.06645202637)',
      'GMR.DefineUnstuck(3384.1706542969, 2868.4753417969, 142.00028991699)',
      'GMR.DefineUnstuck(3370.3784179688, 2867.2868652344, 142.12213134766)',
      'GMR.DefineUnstuck(3355.3474121094, 2863.845703125, 141.79713439941)',
      'GMR.DefineUnstuck(3341.1171875, 2861.2170410156, 141.50091552734)',
      'GMR.DefineUnstuck(3326.1435546875, 2858.8881835938, 141.19866943359)',
      'GMR.DefineUnstuck(3310.0512695312, 2858.4240722656, 140.71562194824)',
      'GMR.DefineUnstuck(3294.4421386719, 2857.3581542969, 140.32568359375)',
      'GMR.DefineUnstuck(3279.3239746094, 2853.9357910156, 139.87074279785)',
      'GMR.DefineUnstuck(3264.4870605469, 2848.7326660156, 139.3775177002)',
      'GMR.DefineUnstuck(3248.9177246094, 2842.4841308594, 138.4776763916)',
      'GMR.DefineUnstuck(3234.9921875, 2840.5883789062, 137.96606445312)',
      'GMR.DefineUnstuck(3218.8381347656, 2840.9521484375, 136.71356201172)',
      'GMR.DefineUnstuck(3201.1337890625, 2840.111328125, 134.97996520996)',
      'GMR.DefineUnstuck(3187.0825195312, 2838.2976074219, 133.43202209473)',
      'GMR.DefineUnstuck(3170.8415527344, 2834.572265625, 131.53570556641)',
      'GMR.DefineUnstuck(3155.3942871094, 2827.9675292969, 128.66761779785)',
      'GMR.DefineUnstuck(3140.2202148438, 2820.0368652344, 125.10961914062)',
      'GMR.DefineUnstuck(3125.4033203125, 2809.1103515625, 121.77109527588)',
      'GMR.DefineUnstuck(3111.2600097656, 2793.1804199219, 118.22956085205)',
      'GMR.DefineUnstuck(3101.5617675781, 2779.4626464844, 116.4927520752)',
      'GMR.DefineUnstuck(3096.5166015625, 2764.1245117188, 115.81381988525)',
      'GMR.DefineUnstuck(3093.7197265625, 2750.2802734375, 115.95725250244)',
      'GMR.DefineUnstuck(3089.44140625, 2735.0439453125, 115.82997894287)',
      'GMR.DefineUnstuck(3086.6552734375, 2721.2316894531, 116.25479125977)',
      'GMR.DefineUnstuck(3083.3781738281, 2707.9755859375, 116.27617645264)',
      'GMR.DefineUnstuck(3076.3332519531, 2695.7790527344, 115.89102935791)',
      'GMR.DefineUnstuck(3067.9389648438, 2683.96484375, 115.26461791992)',
      'GMR.DefineUnstuck(3062.5993652344, 2671.3061523438, 114.98162841797)',
      'GMR.DefineUnstuck(3057.603515625, 2659.8244628906, 114.69273376465)',
      'GMR.DefineUnstuck(3054.1784667969, 2645.8791503906, 113.69683837891)',
      'GMR.DefineUnstuck(3050.5205078125, 2633.193359375, 112.24643707275)',
      'GMR.DefineUnstuck(3048.669921875, 2622.0881347656, 110.62424468994)',
      'GMR.DefineUnstuck(3049.91796875, 2607.3344726562, 108.95063018799)',
      'GMR.DefineUnstuck(3052.6357421875, 2592.783203125, 107.97069549561)',
      'GMR.DefineUnstuck(3055.8869628906, 2578.3151855469, 107.37976074219)',
      'GMR.DefineUnstuck(3059.2094726562, 2563.5302734375, 107.99937438965)',
      'GMR.DefineUnstuck(3060.4516601562, 2549.484375, 109.35676574707)',
      'GMR.DefineUnstuck(3060.6726074219, 2533.3566894531, 111.62958526611)',
      'GMR.DefineUnstuck(3064.6159667969, 2518.6936035156, 113.16975402832)',
      'GMR.DefineUnstuck(3071.6364746094, 2505.2995605469, 114.00959014893)',
      'GMR.DefineUnstuck(3077.5971679688, 2492.8757324219, 114.16750335693)',
      'GMR.DefineUnstuck(3087.791015625, 2478.3034667969, 115.03563690186)',
      'GMR.DefineUnstuck(3100.9750976562, 2469.0559082031, 116.51902008057)',
      'GMR.DefineUnstuck(3111.2395019531, 2457.9553222656, 118.86698150635)',
      'GMR.DefineUnstuck(3120.5632324219, 2444.0568847656, 122.03615570068)',
      'GMR.DefineUnstuck(3129.2741699219, 2428.166015625, 127.14929962158)',
      'GMR.DefineUnstuck(3141.1857910156, 2419.7734375, 130.66090393066)',
      'GMR.DefineUnstuck(3156.6298828125, 2407.0437011719, 131.3424987793)',
      'GMR.DefineUnstuck(3165.0568847656, 2392.5278320312, 128.95002746582)',
      'GMR.DefineUnstuck(3165.669921875, 2377.7236328125, 128.73860168457)',
      'GMR.DefineUnstuck(3160.703125, 2354.9265136719, 131.24317932129)',
      'GMR.DefineUnstuck(3159.8349609375, 2333.703125, 134.24794006348)',
      'GMR.DefineUnstuck(3163.6384277344, 2315.2724609375, 136.03741455078)',
      'GMR.DefineUnstuck(3168.0324707031, 2301.1264648438, 136.80252075195)',
      'GMR.DefineUnstuck(3172.8298339844, 2285.6813964844, 137.21618652344)',
      'GMR.DefineUnstuck(3177.4619140625, 2267.2236328125, 137.73861694336)',
      'GMR.DefineUnstuck(3179.2214355469, 2248.5236816406, 138.4815826416)',
      'GMR.DefineUnstuck(3183.1479492188, 2233.6662597656, 138.31605529785)',
      'GMR.DefineUnstuck(3189.9270019531, 2216.162109375, 138.16389465332)',
      'GMR.DefineUnstuck(3194.8728027344, 2198.4953613281, 136.88374328613)',
      'GMR.DefineUnstuck(3199.1203613281, 2180.1997070312, 135.2532043457)',
      'GMR.DefineUnstuck(3203.3647460938, 2161.9184570312, 135.12756347656)',
      'GMR.DefineUnstuck(3201.8142089844, 2145.0854492188, 138.25663757324)',
      'GMR.DefineUnstuck(3202.2346191406, 2132.1772460938, 140.11866760254)',
      'GMR.DefineUnstuck(3206.2976074219, 2114.8618164062, 141.01393127441)',
      'GMR.DefineUnstuck(3209.2917480469, 2097.4377441406, 140.23252868652)',
      'GMR.DefineUnstuck(3207.501953125, 2082.1262207031, 139.76672363281)',
      'GMR.DefineUnstuck(3205.1381835938, 2068.2565917969, 139.86688232422)',
      'GMR.DefineUnstuck(3199.8706054688, 2054.4875488281, 140.52984619141)',
      'GMR.DefineUnstuck(3193.1572265625, 2038.05078125, 141.06590270996)',
      'GMR.DefineUnstuck(3187.5541992188, 2021.1926269531, 142.1915435791)',
      'GMR.DefineUnstuck(3182.2521972656, 2005.2395019531, 143.19638061523)',
      'GMR.DefineUnstuck(3176.3317871094, 1987.4262695312, 143.47425842285)',
      'GMR.DefineUnstuck(3169.9909667969, 1968.3483886719, 143.66943359375)',
      'GMR.DefineUnstuck(3163.6604003906, 1949.3005371094, 143.77159118652)',
      'GMR.DefineUnstuck(3160.8037109375, 1931.1271972656, 143.8452911377)',
      'GMR.DefineUnstuck(3157.8208007812, 1916.6452636719, 143.87390136719)',
      'GMR.DefineUnstuck(3153.5803222656, 1902.1182861328, 143.87466430664)',
      'GMR.DefineUnstuck(3147.0913085938, 1882.7467041016, 143.9525604248)',
      'GMR.DefineUnstuck(3141.7736816406, 1865.4597167969, 143.8371887207)',
      'GMR.DefineUnstuck(3137.6372070312, 1849.4143066406, 143.87789916992)',
      'GMR.DefineUnstuck(3121.0024414062, 1840.8299560547, 143.8948059082)',
      'GMR.DefineUnstuck(3104.1318359375, 1833.3803710938, 143.71830749512)',
      'GMR.DefineUnstuck(3086.3288574219, 1828.2915039062, 142.97233581543)',
      'GMR.DefineUnstuck(3064.5954589844, 1825.0971679688, 140.94758605957)',
      'GMR.DefineUnstuck(3046.6491699219, 1823.8634033203, 139.83685302734)',
      'GMR.DefineUnstuck(3029.5131835938, 1823.8842773438, 139.63786315918)',
      'GMR.DefineUnstuck(3012.6684570312, 1823.9047851562, 140.14462280273)',
      'GMR.DefineUnstuck(2999.3271484375, 1820.4377441406, 139.54100036621)',    
      'GMR.DefineProfileCenter(3075.0666503906, 1884.6551513672, 144.44325256348, 100)',
      'GMR.DefineProfileCenter(3070.4711914062, 1929.1993408203, 144.49433898926, 100)',
      'GMR.DefineProfileCenter(3041.0637207031, 1958.7486572266, 144.44438171387, 100)',
      'GMR.DefineProfileCenter(3187.9519042969, 1825.1052246094, 144.44467163086, 100)',
      'GMR.DefineProfileCenter(3237.0029296875, 1743.9489746094, 118.72552490234, 100)',
      'GMR.DefineProfileCenter(3157.291015625, 1723.1365966797, 140.96145629883, 100)',
      'GMR.DefineProfileCenter(3110.2124023438, 1674.5115966797, 140.74238586426, 100)',
      'GMR.DefineProfileCenter(3036.330078125, 1710.4432373047, 130.66268920898, 100)',
      'GMR.DefineProfileCenter(2944.2451171875, 1714.9898681641, 124.4769744873, 100)',
      'GMR.DefineProfileCenter(3101.9235839844, 1912.3842773438, 144.4449005127, 100)',
      'GMR.DefineProfileCenter(2905.8493652344, 1865.3363037109, 118.37317657471, 100)',
      'GMR.DefineSettings("Enable", { "Grinding" }); GMR.DefineSettings("Disable", { "CustomObjects" })',
      'GMR.DefineSellVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996, 19572)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000, 20194)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)',
      'GMR.DefineQuestEnemyId(18884)'
    }
  })
  
  
  
  GMR.DefineQuest({
    QuestName = "Grinding: Netherstorm - Level 70",
    QuestType = "GrindTo",
    Level = 71,
    Faction = "Alliance",
    PreQuest = 123456789,
    Profile = {
      'GMR.DefineProfileCenter(3208.184, 3604.479, 128.7734, 80)',
      'GMR.DefineProfileCenter(3104.162, 3817.406, 143.9626, 80)',
      'GMR.DefineProfileCenter(3202.659, 3820.93, 141.5334, 80)',
      'GMR.DefineProfileCenter(3344.64, 3793.434, 140.2714, 80)',
      'GMR.DefineProfileCenter(3445.961, 3845.999, 145.0156, 80)',
      'GMR.DefineProfileCenter(3529.391, 3966.775, 131.0453, 80)',
      'GMR.DefineProfileCenter(3497.788, 4020.217, 131.8559, 80)',
      'GMR.DefineProfileCenter(3507.501, 3800.821, 138.3905, 80)',
      'GMR.DefineProfileCenter(3346.786, 4026.341, 166.7049, 80)',
      'GMR.DefineProfileCenter(3330.128, 3944.887, 183.9524, 80)',
      'GMR.DefineProfileCenter(3243.747, 3937.382, 173.4608, 80)',
      'GMR.DefineProfileCenter(3150.173, 3992.616, 155.0334, 80)',
      'GMR.DefineProfileCenter(3116.383, 4088.36, 137.9027, 80)',
      'GMR.DefineProfileCenter(3167.236, 3988.667, 154.545, 80)',
      'GMR.DefineProfileCenter(3118.906, 3791.267, 142.6406, 80)',
      'GMR.DefineProfileCenter(2913.697, 3778.021, 152.871, 80)',
      'GMR.DefineProfileCenter(2771.26, 3763.129, 125.3621, 80)',
      'GMR.DefineProfileCenter(2737.959, 3619.781, 140.854, 80)',
      'GMR.DefineProfileCenter(2799.923, 3548.419, 147.6059, 80)',
      'GMR.DefineProfileCenter(2636.244, 3564.793, 144.2362, 80)',
      'GMR.DefineProfileCenter(2773.655, 3221.854, 148.8143, 120)',
      'GMR.DefineProfileCenter(3188.298, 3601.2, 132.4702, 80)',
      'GMR.DefineQuestEnemyId(20210)',
      'GMR.DefineQuestEnemyId(18880)',
      'GMR.DefineQuestEnemyId(18879)',
      'GMR.DefineQuestEnemyId(18864)',
      'GMR.DefineQuestEnemyId(18865)',
      'GMR.DefineQuestEnemyId(19852)',
      'GMR.DefineQuestEnemyId(19853)',
      'GMR.BlacklistId(20284)',
      'GMR.BlacklistId(19692)',
      'GMR.BlacklistId(18873)',
      'GMR.BlacklistId(18872)',
      'GMR.BlacklistId(21058)',
      'GMR.DefineSellVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineRepairVendor(2975.459961, 3663.070068, 143.167999,19575)',
      'GMR.DefineAmmoVendor(3351.219971, 2877.949951, 144.250000,20194)',
      'GMR.DefineGoodsVendor(3073.639893, 3656.379883, 143.173996,19572)',
      'GMR.DefineProfileMailbox(3055.719, 3686.681, 142.7672, 184085)'
    }
  })
  
  
  end)