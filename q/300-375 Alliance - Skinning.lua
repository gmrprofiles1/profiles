local quester = "Skinning 300-375 [Alliance][Quantum]"
if not GMR.Tables.Questers[quester] then
GMR.DefineQuester(quester, function()

	GMR.DefineQuest({
		QuestName = "300-330 Hellfire Peninsula [Alliance][Quantum]",
		QuestType = "SkinTo",
		-- mining und herbing
		Level = 330,
		Race = "Alliance", --statt Race = auch Faction = "Alliance"
		Profile = function()
				GMR.DefineProfileName("300-330 Hellfire Peninsula [Alliance][Quantum]")
				GMR.DefineProfileType("Gathering")
				GMR.DefineProfileContinent("Outland")
				GMR.DefineProfileCenter(-961.01818847656, 3146.9291992188, 27.702646255493, 100)
				GMR.DefineProfileCenter(-981.16552734375, 3241.1306152344, 48.832626342773, 100)
				GMR.DefineProfileCenter(-1114.9763183594, 3314.3815917969, 87.230972290039, 100)
				GMR.BlacklistId(19414)
				GMR.BlacklistId(16878)
				GMR.BlacklistId(22454)
				GMR.BlacklistId(16878)
				GMR.BlacklistId(16975)
				GMR.BlacklistId(19414)
				GMR.BlacklistId(19414)
				GMR.DefineProfileCenter(-1216.5048828125, 3287.2741699219, 82.774925231934, 100)
				GMR.DefineProfileCenter(-1371.8492431641, 3369.8244628906, 43.984245300293, 100)
				GMR.DefineProfileCenter(-1423.3422851562, 3421.1103515625, 38.850082397461, 100)
				GMR.DefineProfileCenter(-1432.3039550781, 3455.8024902344, 42.98267364502, 100)
				GMR.DefineProfileCenter(-1477.7327880859, 3443.6333007812, 32.144111633301, 100)
				GMR.DefineProfileCenter(-1514.0354003906, 3473.7163085938, 29.528770446777, 100)
				GMR.DefineProfileCenter(-1533.5980224609, 3552.0439453125, 34.80623626709, 100)
				GMR.DefineProfileCenter(-1622.1072998047, 3601.7414550781, 26.285083770752, 100)
				GMR.DefineProfileCenter(-1610.9272460938, 3646.2736816406, 33.168869018555, 100)
				GMR.DefineProfileCenter(-1657.8354492188, 3799.5795898438, 32.990715026855, 100)
				GMR.DefineGoodsVendor(-708.87133789062, 2739.130859375, 94.733436584473, 16826)
		        GMR.DefineAmmoVendor(-708.87133789062, 2739.130859375, 94.733436584473, 16826)
			    GMR.DefineHearthstoneBindLocation(-708.87133789062, 2739.130859375, 94.733436584473, 16826, 1945)
                GMR.DefineVendorPath("Goods", -630.6767578125, 2561.5927734375, 77.546363830566)
                GMR.DefineVendorPath("Goods", -643.04235839844, 2578.6567382812, 82.013130187988)
                GMR.DefineVendorPath("Goods", -662.84741210938, 2608.6774902344, 85.245277404785)
                GMR.DefineVendorPath("Goods", -669.86431884766, 2622.0107421875, 86.532722473145)
                GMR.DefineVendorPath("Goods", -674.04345703125, 2632.1643066406, 87.531982421875)
                GMR.DefineVendorPath("Goods", -677.23431396484, 2640.5798339844, 88.668434143066)
                GMR.DefineVendorPath("Goods", -700.40655517578, 2682.7326660156, 93.808464050293)
                GMR.DefineVendorPath("Goods", -705.70489501953, 2689.708984375, 93.946617126465)
                GMR.DefineVendorPath("Goods", -709.75811767578, 2701.3610839844, 94.39315032959)
                GMR.DefineVendorPath("Goods", -709.91839599609, 2708.2407226562, 94.720573425293)
                GMR.DefineVendorPath("Goods", -709.35894775391, 2713.2114257812, 94.721145629883)
                GMR.DefineVendorPath("Goods", -699.69378662109, 2713.0659179688, 94.736480712891)
                GMR.DefineVendorPath("Goods", -700.26220703125, 2716.2446289062, 94.736480712891)
                GMR.DefineVendorPath("Goods", -703.03137207031, 2717.9992675781, 94.736480712891)
                GMR.DefineVendorPath("Goods", -702.94720458984, 2723.0100097656, 94.110397338867)
                GMR.DefineVendorPath("Goods", -702.74627685547, 2728.6025390625, 94.110397338867)
                GMR.DefineVendorPath("Goods", -703.89678955078, 2732.7719726562, 94.733444213867)
                GMR.DefineVendorPath("Goods", -704.95947265625, 2737.4265136719, 94.733444213867)
                GMR.DefineVendorPath("Goods", -705.54553222656, 2739.6784667969, 94.733444213867)
                GMR.DefineVendorPath("Goods", -706.55657958984, 2739.0893554688, 95.42374420166)
                GMR.DefineProfileMailbox(-706.82678222656, 2700.1274414062, 94.468849182129)
                GMR.DefineMailboxPath(-632.03448486328, 2564.3156738281, 78.34757232666)
                GMR.DefineMailboxPath(-641.32751464844, 2576.5659179688, 81.665641784668)
                GMR.DefineMailboxPath(-648.8486328125, 2588.0170898438, 82.933059692383)
                GMR.DefineMailboxPath(-660.88067626953, 2607.0480957031, 84.996910095215)
                GMR.DefineMailboxPath(-669.43621826172, 2622.8654785156, 86.531242370605)
                GMR.DefineMailboxPath(-676.99291992188, 2639.7055664062, 88.578674316406)
                GMR.DefineMailboxPath(-680.56793212891, 2648.7473144531, 89.953407287598)
                GMR.DefineMailboxPath(-687.25854492188, 2661.1166992188, 92.025245666504)
                GMR.DefineMailboxPath(-697.52990722656, 2680.1062011719, 93.512100219726)
                GMR.DefineMailboxPath(-707.47277832031, 2700.0812988281, 94.434387207031)
                GMR.DefineVendorPath("Ammo", -630.6767578125, 2561.5927734375, 77.546363830566)
                GMR.DefineVendorPath("Ammo", -643.04235839844, 2578.6567382812, 82.013130187988)
                GMR.DefineVendorPath("Ammo", -662.84741210938, 2608.6774902344, 85.245277404785)
                GMR.DefineVendorPath("Ammo", -669.86431884766, 2622.0107421875, 86.532722473145)
                GMR.DefineVendorPath("Ammo", -674.04345703125, 2632.1643066406, 87.531982421875)
                GMR.DefineVendorPath("Ammo", -677.23431396484, 2640.5798339844, 88.668434143066)
                GMR.DefineVendorPath("Ammo", -700.40655517578, 2682.7326660156, 93.808464050293)
                GMR.DefineVendorPath("Ammo", -705.70489501953, 2689.708984375, 93.946617126465)
                GMR.DefineVendorPath("Ammo", -709.75811767578, 2701.3610839844, 94.39315032959)
                GMR.DefineVendorPath("Ammo", -709.91839599609, 2708.2407226562, 94.720573425293)
                GMR.DefineVendorPath("Ammo", -709.35894775391, 2713.2114257812, 94.721145629883)
                GMR.DefineVendorPath("Ammo", -699.69378662109, 2713.0659179688, 94.736480712891)
                GMR.DefineVendorPath("Ammo", -700.26220703125, 2716.2446289062, 94.736480712891)
                GMR.DefineVendorPath("Ammo", -703.03137207031, 2717.9992675781, 94.736480712891)
                GMR.DefineVendorPath("Ammo", -702.94720458984, 2723.0100097656, 94.110397338867)
                GMR.DefineVendorPath("Ammo", -702.74627685547, 2728.6025390625, 94.110397338867)
                GMR.DefineVendorPath("Ammo", -703.89678955078, 2732.7719726562, 94.733444213867)
                GMR.DefineVendorPath("Ammo", -704.95947265625, 2737.4265136719, 94.733444213867)
                GMR.DefineVendorPath("Ammo", -705.54553222656, 2739.6784667969, 94.733444213867)
                GMR.DefineVendorPath("Ammo", -706.55657958984, 2739.0893554688, 95.42374420166)
                GMR.DefineSellVendor(-707.80706787109, 2716.1213378906, 94.736930847168, 22227)
                GMR.DefineRepairVendor(-707.80706787109, 2716.1213378906, 94.736930847168, 22227)
                GMR.DefineVendorPath("Sell", -630.39215087891, 2560.9970703125, 77.35359954834)
                GMR.DefineVendorPath("Sell", -651.3837890625, 2593.7434082031, 83.375991821289)
                GMR.DefineVendorPath("Sell", -661.47241210938, 2607.8537597656, 85.088096618652)
                GMR.DefineVendorPath("Sell", -676.37359619141, 2638.0822753906, 88.366119384766)
                GMR.DefineVendorPath("Sell", -696.93682861328, 2678.0681152344, 93.314674377441)
                GMR.DefineVendorPath("Sell", -704.0361328125, 2687.4560546875, 93.927093505859)
                GMR.DefineVendorPath("Sell", -709.21142578125, 2697.6467285156, 94.320426940918)
                GMR.DefineVendorPath("Sell", -709.21496582031, 2704.6296386719, 94.720634460449)
                GMR.DefineVendorPath("Sell", -709.29016113281, 2713.0791015625, 94.721031188965)
                GMR.DefineVendorPath("Sell", -700.36853027344, 2713.3056640625, 94.735382080078)
                GMR.DefineVendorPath("Sell", -701.13708496094, 2717.4348144531, 94.735382080078)
                GMR.DefineVendorPath("Sell", -707.03686523438, 2717.099609375, 94.735382080078)
                GMR.DefineVendorPath("Repair", -630.39215087891, 2560.9970703125, 77.35359954834)
                GMR.DefineVendorPath("Repair", -633.63366699219, 2569.7036132812, 79.88330078125)
                GMR.DefineVendorPath("Repair", -651.3837890625, 2593.7434082031, 83.375991821289)
                GMR.DefineVendorPath("Repair", -661.47241210938, 2607.8537597656, 85.088096618652)
                GMR.DefineVendorPath("Repair", -676.37359619141, 2638.0822753906, 88.366119384766)
                GMR.DefineVendorPath("Repair", -696.93682861328, 2678.0681152344, 93.314674377441)
                GMR.DefineVendorPath("Repair", -704.0361328125, 2687.4560546875, 93.927093505859)
                GMR.DefineVendorPath("Repair", -709.21142578125, 2697.6467285156, 94.320426940918)
                GMR.DefineVendorPath("Repair", -709.21496582031, 2704.6296386719, 94.720634460449)
                GMR.DefineVendorPath("Repair", -709.29016113281, 2713.0791015625, 94.721031188965)
                GMR.DefineVendorPath("Repair", -700.36853027344, 2713.3056640625, 94.735382080078)
                GMR.DefineVendorPath("Repair", -701.13708496094, 2717.4348144531, 94.735382080078)
                GMR.DefineVendorPath("Repair", -707.03686523438, 2717.099609375, 94.735382080078)
		end 
	})
	
		GMR.DefineQuest({
		QuestName = "330-375 Nagrand [Alliance][Quantum]",
		QuestType = "SkinTo",
		-- Herb bis 375 / mining bis 375
		Level = 375,
		Race = "Alliance", --statt Race = auch Faction = "Alliance"
		Profile = function()
				GMR.DefineProfileName("330-375 Nagrand [Alliance][Quantum]")
				GMR.DefineProfileType("Gathering")
				GMR.DefineProfileContinent("Outland")
				GMR.DefineProfileCenter(-1851.3626708984, 6608.1684570312, 2.8318862915039, 100)
				GMR.DefineProfileCenter(-1705.9506835938, 6683.5151367188, -13.526927947998, 100)
				GMR.DefineProfileCenter(-1718.2493896484, 6826.4765625, -21.044979095459, 100)
				GMR.DefineProfileCenter(-1616.7989501953, 6873.42578125, -12.096256256104, 100)
				GMR.DefineProfileCenter(-1498.5114746094, 6975.8735351562, -0.73805999755859, 100)
				GMR.DefineProfileCenter(-1479.5915527344, 6825.0859375, 10.6149559021, 100)
				GMR.BlacklistId(17158)
				GMR.DefineProfileCenter(-1376.0981445312, 6821.3100585938, 30.506427764893, 100)
				GMR.DefineProfileCenter(-1351.7218017578, 6656.4599609375, 42.944297790527, 100)
				GMR.DefineProfileCenter(-1433.5266113281, 6556.4731445312, 36.098987579346, 100)
				GMR.DefineProfileCenter(-1563.4299316406, 6425.7221679688, 29.95133972168, 100)
				GMR.DefineProfileCenter(-1679.4464111328, 6406.9125976562, 38.86092376709, 100)
				GMR.DefineProfileCenter(-1736.5422363281, 6355.9243164062, 43.717937469482, 100)
				GMR.BlacklistId(17141)
				GMR.BlacklistId(17139)
				GMR.BlacklistId(17158)
				GMR.BlacklistId(17141)
				GMR.BlacklistId(17156)
				GMR.BlacklistId(17158)
				GMR.DefineSellVendor(-1480.1221923828, 6357.9765625, 34.26566696167, 18278)
				GMR.DefineRepairVendor(-1480.1221923828, 6357.9765625, 34.26566696167, 18278)
				GMR.DefineVendorPath("Sell", -1455.5153808594, 6400.2880859375, 31.317724227905)
				GMR.DefineVendorPath("Sell", -1459.9294433594, 6387.9858398438, 31.220609664917)
				GMR.DefineVendorPath("Sell", -1464.0939941406, 6376.37890625, 33.73900604248)
				GMR.DefineVendorPath("Sell", -1467.9262695312, 6367.0981445312, 35.732921600342)
				GMR.DefineVendorPath("Sell", -1477.0159912109, 6358.5122070312, 35.31706237793)
				GMR.DefineVendorPath("Sell", -1478.7951660156, 6358.0288085938, 34.743968963623)
				GMR.DefineVendorPath("Repair", -1460.5622558594, 6404.4731445312, 30.287046432495)
				GMR.DefineVendorPath("Repair", -1464.6029052734, 6395.6899414062, 29.929462432861)
				GMR.DefineVendorPath("Repair", -1467.9916992188, 6381.5068359375, 32.182758331299)
				GMR.DefineVendorPath("Repair", -1470.1242675781, 6370.33203125, 34.697532653809)
				GMR.DefineVendorPath("Repair", -1472.0773925781, 6361.0961914062, 36.298946380615)
				GMR.DefineVendorPath("Repair", -1478.6536865234, 6357.8173828125, 34.842723846436)	
		end 
	})
GMR.DefineQuest({
	QuestName = "300-375 Skinning Completed",
	QuestType = "SkinTo",
	Level = 500,
	Race = "Alliance",
	Profile = function()
		GMR.Print("Please load 375-450 now and move your character to Northrend (Borean Tundra)")
		GMR.Stop()
	end 
})
end)
end 
GMR.LoadQuester(quester)