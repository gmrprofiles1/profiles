GMR.DefineProfileName("80-105 Westfall [Alliance][Quantum]")
GMR.DefineProfileType("Gathering")
GMR.DefineProfileContinent("Eastern Kingdoms")
GMR.DefineProfileCenter(-10482.6015625, 820.62298583984, 44.798286437988, 70)
GMR.DefineProfileCenter(-10592.86328125, 786.12640380859, 49.772834777832, 70)
GMR.DefineProfileCenter(-10614.673828125, 873.84466552734, 40.666999816895, 70)
GMR.DefineProfileCenter(-10716.104492188, 773.1494140625, 41.800384521484, 70)
GMR.BlacklistId(115)
GMR.DefineProfileCenter(-10843.16015625, 775.73852539062, 32.729545593262, 100)
GMR.DefineProfileCenter(-10935.583007812, 741.55377197266, 42.035293579102, 100)
GMR.DefineProfileCenter(-11017.475585938, 695.54547119141, 36.641860961914, 100)
GMR.BlacklistId(452)
GMR.BlacklistId(98)
GMR.DefineProfileCenter(-10947.1640625, 895.31036376953, 35.02725982666, 100)
GMR.DefineProfileCenter(-10973.850585938, 989.1357421875, 36.482353210449, 100)
GMR.BlacklistId(124)
GMR.DefineProfileCenter(-10961.471679688, 1098.0944824219, 37.298488616943, 100)
GMR.DefineProfileCenter(-10824.922851562, 1087.7277832031, 36.377510070801, 100)
GMR.DefineProfileCenter(-10775.7578125, 1157.3165283203, 45.204364776611, 100)
GMR.DefineProfileCenter(-10708.80859375, 1019.5918579102, 36.481391906738, 100)
GMR.DefineProfileCenter(-10587.130859375, 950.20355224609, 41.029640197754, 100)
GMR.DefineSellVendor(-9653.9453125, 659.65069580078, 38.652896881104, 1249)
                GMR.DefineRepairVendor(-9653.9453125, 659.65069580078, 38.652896881104, 1249)
                GMR.DefineVendorPath("Sell", -9669.134765625, 690.63854980469, 36.777492523193)
                GMR.DefineVendorPath("Sell", -9632.349609375, 674.55718994141, 37.152236938476)
                GMR.DefineVendorPath("Sell", -9631.259765625, 674.0166015625, 37.152236938476)
                GMR.DefineVendorPath("Sell", -9630.326171875, 673.02758789062, 37.152236938476)
                GMR.DefineVendorPath("Sell", -9629.9462890625, 671.8935546875, 37.152236938476)
                GMR.DefineVendorPath("Sell", -9630.04296875, 670.70855712891, 37.152236938476)
                GMR.DefineVendorPath("Sell", -9630.4296875, 669.71752929688, 37.152236938476)
                GMR.DefineVendorPath("Sell", -9634.583984375, 659.50506591797, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9635.1669921875, 658.19635009766, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9635.9072265625, 657.40124511719, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9637.419921875, 655.150390625, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9639.9697265625, 652.89636230469, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9645.2685546875, 655.02783203125, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9649.177734375, 656.779296875, 38.652923583984)
                GMR.DefineVendorPath("Sell", -9654.2216796875, 659.65631103516, 38.652923583984)
                GMR.DefineVendorPath("Repair", -9669.1240234375, 690.5986328125, 36.780002593994)
                GMR.DefineVendorPath("Repair", -9633.7705078125, 675.00787353516, 37.179462432861)
                GMR.DefineVendorPath("Repair", -9631.8203125, 674.19293212891, 37.152984619141)
                GMR.DefineVendorPath("Repair", -9630.6806640625, 673.34466552734, 37.152984619141)
                GMR.DefineVendorPath("Repair", -9630.0830078125, 672.14587402344, 37.152984619141)
                GMR.DefineVendorPath("Repair", -9630.044921875, 670.95074462891, 37.152984619141)
                GMR.DefineVendorPath("Repair", -9630.416015625, 669.89825439453, 37.152984619141)
                GMR.DefineVendorPath("Repair", -9632.4365234375, 664.78039550781, 38.414531707764)
                GMR.DefineVendorPath("Repair", -9637.552734375, 653.51153564453, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9638.4541015625, 652.54858398438, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9639.6552734375, 652.07495117188, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9640.876953125, 652.13726806641, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9641.837890625, 652.58184814453, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9643.892578125, 654.25024414062, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9647.01953125, 656.22058105469, 38.653778076172)
                GMR.DefineVendorPath("Repair", -9654.8642578125, 659.76501464844, 38.653778076172)
                GMR.DefineGoodsVendor(-6225.2368164062, 320.3720703125, 383.11685180664, 829)
                GMR.DefineAmmoVendor(-6225.2368164062, 320.3720703125, 383.11685180664, 829)
                GMR.DefineVendorPath("Goods", -6266.146484375, 364.45050048828, 382.80017089844)
                GMR.DefineVendorPath("Goods", -6257.2446289062, 354.74459838867, 383.60217285156)
                GMR.DefineVendorPath("Goods", -6255.5698242188, 352.16357421875, 383.32293701172)
                GMR.DefineVendorPath("Goods", -6254.1923828125, 349.2809753418, 382.98986816406)
                GMR.DefineVendorPath("Goods", -6253.0981445312, 346.26010131836, 382.7018737793)
                GMR.DefineVendorPath("Goods", -6251.197265625, 341.67578125, 382.53802490234)
                GMR.DefineVendorPath("Goods", -6249.2607421875, 339.38073730469, 382.42776489258)
                GMR.DefineVendorPath("Goods", -6229.5424804688, 325.6946105957, 382.98965454102)
                GMR.DefineVendorPath("Goods", -6225.1123046875, 320.34048461914, 383.11599731445)
                GMR.DefineVendorPath("Ammo", -6266.3505859375, 364.87893676758, 382.76211547852)
                GMR.DefineVendorPath("Ammo", -6257.75390625, 354.80282592773, 383.56307983398)
                GMR.DefineVendorPath("Ammo", -6255.8032226562, 352.39242553711, 383.34252929688)
                GMR.DefineVendorPath("Ammo", -6254.2026367188, 349.2878112793, 382.99044799805)
                GMR.DefineVendorPath("Ammo", -6252.8447265625, 345.56600952148, 382.65225219726)
                GMR.DefineVendorPath("Ammo", -6251.7216796875, 342.46618652344, 382.55108642578)
                GMR.DefineVendorPath("Ammo", -6250.2875976562, 340.20376586914, 382.5573425293)
                GMR.DefineVendorPath("Ammo", -6248.70703125, 338.84643554688, 382.32257080078)
                GMR.DefineVendorPath("Ammo", -6227.724609375, 325.21597290039, 383.07897949219)
                GMR.DefineVendorPath("Ammo", -6225.1489257812, 320.50149536133, 383.11639404297)