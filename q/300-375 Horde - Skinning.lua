local quester = "Skinning 300-375 [Horde][Quantum]"
if not GMR.Tables.Questers[quester] then
GMR.DefineQuester(quester, function()

	GMR.DefineQuest({
		QuestName = "300-330 Hellfire Peninsula [Horde][Quantum]",
		QuestType = "SkinTo",
		-- mining und herbing
		Level = 330,
		Race = "Horde", --statt Race = auch Faction = "Alliance"
		Profile = function()
				GMR.DefineProfileName("300-330 Hellfire Peninsula [Horde][Quantum]")
				GMR.DefineProfileType("Gathering")
				GMR.DefineProfileContinent("Outland")
				GMR.DefineProfileCenter(408.4150390625, 2957.2939453125, 28.001731872559, 60)
                GMR.DefineProfileCenter(62.521392822266, 2960.8369140625, 21.647449493408, 60)
                GMR.DefineProfileCenter(70.212753295898, 3053.7280273438, -1.222526550293, 60)
                GMR.DefineProfileCenter(178.65173339844, 3104.3671875, 21.23225402832, 60)
                GMR.DefineProfileCenter(389.94839477539, 3059.0083007812, 16.136781692505, 60)
                GMR.DefineProfileCenter(466.72796630859, 3139.8073730469, 22.052696228027, 60)
                GMR.DefineSellVendor(167.27842712402, 2795.6691894531, 113.36517333984, 16583)
                GMR.DefineRepairVendor(167.27842712402, 2795.6691894531, 113.36517333984, 16583)
                GMR.DefineGoodsVendor(190.87579345703, 2610.9265136719, 87.283699035645, 16602)
                GMR.DefineProfileMailbox(171.84330749512, 2623.9724121094, 87.115280151367)
                GMR.DefineHearthstoneBindLocation(190.87579345703, 2610.9265136719, 87.283699035645, 16602, 101)
                GMR.BlacklistId(19414)
                GMR.BlacklistId(18677)
                GMR.BlacklistId(19413)
                GMR.BlacklistId(19415)
                GMR.BlacklistId(18952)
                GMR.BlacklistId(16876)
       	        GMR.BlacklistId(19701)
                GMR.DefineMeshAreaBlacklist(43.060009002686, 2897.0107421875, 37.754043579102, 70)
                GMR.DefineMeshAreaBlacklist(-88.81298828125, 3103.0290527344, -2.4134483337402, 70)
                GMR.DefineMeshAreaBlacklist(-35.074337005615, 3159.4343261719, -1.4760360717773, 70)
                GMR.DefineVendorPath("Sell", 322.77346801758, 2905.783203125, 39.46558380127)
                GMR.DefineVendorPath("Sell", 293.03671264648, 2790.6672363281, 78.610687255859)
                GMR.DefineVendorPath("Sell", 282.67395019531, 2757.8337402344, 84.286109924316)
                GMR.DefineVendorPath("Sell", 251.65299987793, 2746.7312011719, 86.425880432129)
                GMR.DefineVendorPath("Sell", 213.1918182373, 2737.6103515625, 87.01993560791)
                GMR.DefineVendorPath("Sell", 178.77458190918, 2727.0373535156, 87.43675994873)
                GMR.DefineVendorPath("Sell", 151.64447021484, 2747.2719726562, 100.10920715332)
                GMR.DefineVendorPath("Sell", 159.38032531738, 2775.8759765625, 109.20319366455)
                GMR.DefineVendorPath("Sell", 166.77369689941, 2794.2453613281, 113.34271240234)
                GMR.DefineVendorPath("Repair", 321.58511352539, 2908.576171875, 38.636745452881)
                GMR.DefineVendorPath("Repair", 291.79553222656, 2790.6223144531, 78.954734802246)
                GMR.DefineVendorPath("Repair", 281.32015991211, 2755.9318847656, 84.39136505127)
                GMR.DefineVendorPath("Repair", 242.84951782226, 2745.6508789062, 86.533767700195)
                GMR.DefineVendorPath("Repair", 195.67109680176, 2731.3464355469, 87.041488647461)
                GMR.DefineVendorPath("Repair", 178.99465942383, 2725.3359375, 87.291931152344)
                GMR.DefineVendorPath("Repair", 151.67599487305, 2748.220703125, 100.42353057861)
                GMR.DefineVendorPath("Repair", 161.24269104004, 2777.560546875, 109.71867370605)
                GMR.DefineVendorPath("Repair", 166.47953796387, 2793.87890625, 113.32540893555)
                GMR.DefineMailboxPath(324.71899414062, 2909.5595703125, 38.421661376953)
                GMR.DefineMailboxPath(279.39517211914, 2758.7468261719, 84.301109313965)
                GMR.DefineMailboxPath(261.01486206055, 2750.2644042969, 85.728439331055)
                GMR.DefineMailboxPath(207.71691894531, 2734.0380859375, 86.765953063965)
                GMR.DefineMailboxPath(176.73626708984, 2719.2565917969, 87.013023376465)
                GMR.DefineMailboxPath(161.76205444336, 2702.640625, 87.309066772461)
                GMR.DefineMailboxPath(172.52549743652, 2623.986328125, 87.109031677246)
                GMR.DefineVendorPath("Goods", 323.47750854492, 2911.1818847656, 38.140869140625)
                GMR.DefineVendorPath("Goods", 290.18786621094, 2785.7746582031, 79.971549987793)
                GMR.DefineVendorPath("Goods", 282.25305175781, 2753.2531738281, 84.523048400879)
                GMR.DefineVendorPath("Goods", 238.22538757324, 2743.427734375, 86.705718994141)
                GMR.DefineVendorPath("Goods", 196.4141998291, 2731.1584472656, 86.917793273926)
                GMR.DefineVendorPath("Goods", 161.2589263916, 2704.5205078125, 87.662803649902)
                GMR.DefineVendorPath("Goods", 172.56608581543, 2634.7170410156, 86.662551879883)
                GMR.DefineVendorPath("Goods", 190.14152526855, 2611.4760742188, 87.283683776855)
		end 
	})
	
		GMR.DefineQuest({
		QuestName = "330-375 Nagrand [Horde][Quantum]",
		QuestType = "SkinTo",
		-- Herb bis 375 / mining bis 375
		Level = 375,
		Race = "Horde", --statt Race = auch Faction = "Alliance"
		Profile = function()
				GMR.DefineProfileName("330-375 Nagrand [Horde][Quantum]")
				GMR.DefineProfileType("Gathering")
				GMR.DefineProfileContinent("Outland")
				GMR.DefineProfileCenter(-1851.3626708984, 6608.1684570312, 2.8318862915039, 100)
				GMR.DefineProfileCenter(-1705.9506835938, 6683.5151367188, -13.526927947998, 100)
				GMR.DefineProfileCenter(-1718.2493896484, 6826.4765625, -21.044979095459, 100)
				GMR.DefineProfileCenter(-1616.7989501953, 6873.42578125, -12.096256256104, 100)
				GMR.DefineProfileCenter(-1498.5114746094, 6975.8735351562, -0.73805999755859, 100)
				GMR.DefineProfileCenter(-1479.5915527344, 6825.0859375, 10.6149559021, 100)
				GMR.BlacklistId(17158)
				GMR.DefineProfileCenter(-1376.0981445312, 6821.3100585938, 30.506427764893, 100)
				GMR.DefineProfileCenter(-1351.7218017578, 6656.4599609375, 42.944297790527, 100)
				GMR.DefineProfileCenter(-1433.5266113281, 6556.4731445312, 36.098987579346, 100)
				GMR.DefineProfileCenter(-1563.4299316406, 6425.7221679688, 29.95133972168, 100)
				GMR.DefineProfileCenter(-1679.4464111328, 6406.9125976562, 38.86092376709, 100)
				GMR.DefineProfileCenter(-1736.5422363281, 6355.9243164062, 43.717937469482, 100)
				GMR.BlacklistId(17141)
				GMR.BlacklistId(17141)
				GMR.BlacklistId(17158)
				GMR.BlacklistId(17141)
				GMR.BlacklistId(17156)
				GMR.BlacklistId(17158)
				GMR.DefineSellVendor(-1480.1221923828, 6357.9765625, 34.26566696167, 18278)
				GMR.DefineRepairVendor(-1480.1221923828, 6357.9765625, 34.26566696167, 18278)
				GMR.DefineVendorPath("Sell", -1455.5153808594, 6400.2880859375, 31.317724227905)
				GMR.DefineVendorPath("Sell", -1459.9294433594, 6387.9858398438, 31.220609664917)
				GMR.DefineVendorPath("Sell", -1464.0939941406, 6376.37890625, 33.73900604248)
				GMR.DefineVendorPath("Sell", -1467.9262695312, 6367.0981445312, 35.732921600342)
				GMR.DefineVendorPath("Sell", -1477.0159912109, 6358.5122070312, 35.31706237793)
				GMR.DefineVendorPath("Sell", -1478.7951660156, 6358.0288085938, 34.743968963623)
				GMR.DefineVendorPath("Repair", -1460.5622558594, 6404.4731445312, 30.287046432495)
				GMR.DefineVendorPath("Repair", -1464.6029052734, 6395.6899414062, 29.929462432861)
				GMR.DefineVendorPath("Repair", -1467.9916992188, 6381.5068359375, 32.182758331299)
				GMR.DefineVendorPath("Repair", -1470.1242675781, 6370.33203125, 34.697532653809)
				GMR.DefineVendorPath("Repair", -1472.0773925781, 6361.0961914062, 36.298946380615)
				GMR.DefineVendorPath("Repair", -1478.6536865234, 6357.8173828125, 34.842723846436)	
		end 
	})
GMR.DefineQuest({
	QuestName = "300-375 Skinning Completed",
	QuestType = "SkinTo",
	Level = 500,
	Race = "Horde",
	Profile = function()
		GMR.Print("Please load 375-450 now and move your character to Northrend (Borean Tundra)")
		GMR.Stop()
	end 
})
end)
end 
GMR.LoadQuester(quester)